# FROM ubuntu:latest
# If this image can not be pull we have a local copy in:
FROM registry.gitlab.inria.fr/inria-ci/docker/ubuntu:latest

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && apt-get install -y --no-install-recommends \
    cmake make g++ curl ca-certificates jq doxygen texlive-font-utils \
    ghostscript
  