# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 14:08:29 2024

@author: chopi
"""

"""
The Staircase classe. 
"""
import json
import pickle
import sys
import random
from datetime import datetime
import math


class Staircase():
    """

    """
    def __init__(self,
                 n_reps = 10,
                 n_up = 1, 
                 n_down = 1,
                 n_reversals = 10,
                 n_last_reversals_for_analysis=6,
                 n_current_up =0,
                 n_current_down = 0,
                 n_current_reversals = 0,
                 n_current_rep = 0,
                 staircase_id = 1,
                 start_val  = 1,
                 max_val = 99999,
                 min_val = 99999,
                 current_vi_val = 1,
                 step = 0.1,
                 step_sizes_list=[],
                 step_sizes_list_index=0,
                 staircase_finished=False,
                 responses=[],
                 keyboard_responses=[],
                 list_vi_val=[],
                 trial_number=[],
                 current_direction="",
                 reversal_trial_list=[],
                 reversal_values_list=[],
                 date="",
                 step_type="lin"): # "lin" , "log10" "dec"

        
        self.n_reps = n_reps
        self.n_up = n_up
        self.n_down = n_down
        self.n_reversals = n_reversals
        
        self.n_current_up = n_current_up
        self.n_current_down = n_current_down
        self.n_current_reversals = n_current_reversals
        self.n_current_rep = n_current_rep
        
        self.staircase_id = staircase_id
        self.start_val = start_val
        self.step = step
        self.staircase_finished = staircase_finished
        self.responses=responses
        self.keyboard_responses=keyboard_responses
        self.list_vi_val=list_vi_val
        self.trial_number=trial_number
        self.current_direction=current_direction
        self.reversal_trial_list=reversal_trial_list
        self.reversal_values_list=reversal_values_list
        self.n_last_reversals_for_analysis=n_last_reversals_for_analysis
        self.step_sizes_list=step_sizes_list
        self.step_sizes_list_index=step_sizes_list_index
        self.max_val=max_val
        self.min_val=min_val
        self.date=datetime.today().strftime('%Y-%m-%d %H:%M').replace(" ", "-").replace(":", "-")
        self.step_type=step_type
        # if (n_down > 1) and (n_down == n_up):
        #     print ("Warning n_down = {} and n_up = {} will not converge".format(n_down, n_up))
    

def from_dict(cls, dict):
    return cls(n_reps=dict["n_reps"],
               n_up =dict["n_up"],
               n_down=dict["n_down"],
               n_reversals=dict["n_reversals"],
               n_current_up=dict["n_current_up"],
               n_current_down=dict["n_current_down"],
               n_current_reversals=dict["n_current_reversals"],
               n_current_rep=dict["n_current_rep"],
               staircase_id=dict["staircase_id"],
               start_val=dict["start_val"],
               step=dict["step"],
               staircase_finished=dict["staircase_finished"],
               responses=dict["responses"],
               list_vi_val=dict["list_vi_val"],
               trial_number=dict["trial_number"],
               current_direction=dict["current_direction"],
               reversal_trial_list=dict["reversal_trial_list"],
               reversal_values_list=dict["reversal_values_list"],
               n_last_reversals_for_analysis=dict["n_last_reversals_for_analysis"],
               step_sizes_list=dict["step_sizes_list"],
               step_sizes_list_index=dict["step_sizes_list_index"],
               max_val=dict["max_val"],
               min_val=dict["min_val"],
               keyboard_responses=dict["keyboard_responses"],
               date=dict["date"],
               step_type = dict["step_type"]
               )


class StaircaseHandler():
    """

    """
    def __init__(self,
                 staircases = [],
                 path_to_json="",
                 responses = {}):

        
        self.staircases = []
        self.path_to_files = path_to_json
        self.objects_in_scene = None
        self.staircases_ids = []
        self.staircase_file_name=""
        self.get_staircases_and_experiment_data()
        self.responses = responses
        self.current_staircase=0
        self.current_trial=0
        
        
    
    def get_staircases_and_experiment_data(self):
        
        if (len(sys.argv) >= 2):
            self.staircase_file_name = sys.argv[1:][0].split(";")[2]
            
        else :
            self.staircase_file_name = "staircases_2024-05-28-11-06.json"#random.choice(["left", "right"])

        # load staircases from json file
        json_file = open(self.path_to_files +"\\"+ self.staircase_file_name, "r") 
        job = json.load(json_file)
        for j in job :
            self.staircases.append(from_dict(Staircase, j))
            self.staircases_ids.append(self.staircases[-1].staircase_id)
        json_file.close()
        
        # load properties from objects in scene
        with open(self.path_to_files + "\\experiment_data.pickle", 'rb') as handle:
            self.objects_in_scene = pickle.load(handle)
    
    
    def process_data_from_unity_and_prepare_instruction(self):
        if (len(sys.argv) >= 2):
            answer_from_unity = sys.argv[1:][0].split(";")[0]
            self.current_staircase = int(sys.argv[1:][0].split(";")[1])
        else :
            answer_from_unity = "left"#random.choice(["left", "right"])
            self.current_staircase = random.choice(self.staircases_ids)
        
        
        self.update_staircases(staircase_id=self.current_staircase,
                               direction=self.responses[answer_from_unity],
                               answer_from_unity=answer_from_unity)
                               
        
    def update_staircases(self, staircase_id, direction="up", answer_from_unity=""):

        list_staircases = []
        total_trials = []
        current_staircase_index = 0
        
        for i in range(len(self.staircases)):
            
            total_trials.append(len(self.staircases[i].responses))
        
        for i in range(len(self.staircases)):
            

            if (self.staircases[i].staircase_id == staircase_id):
                
                current_staircase_index = i
                
                # append vi_value before changing for next step
                self.staircases[i].list_vi_val.append(round(self.staircases[i].start_val, 3))
                self.staircases[i].keyboard_responses.append(answer_from_unity)
                
                if (direction == "up"):
                    
                        self.staircases[i].n_current_up += 1
                        self.staircases[i].n_current_down = 0
                        
                        if (self.staircases[i].n_current_up == self.staircases[i].n_up):
                            
                            # Calculate reversal
                            if (self.staircases[i].current_direction == "down"):
                                self.staircases[i].n_current_reversals += 1
                                self.staircases[i].reversal_values_list.append(round(self.staircases[i].start_val, 3))
                                self.staircases[i].current_direction = "up"
                                self.staircases[i].reversal_trial_list.append(sum(total_trials) + 1)
                                
                                if (self.staircases[i].step_sizes_list_index < len(self.staircases[i].step_sizes_list) - 1):
                                
                                    self.staircases[i].step_sizes_list_index += 1

                            if (self.staircases[i].current_direction == ""):
                                self.staircases[i].current_direction = "up"
                                
                            
                            if (len(self.staircases[i].step_sizes_list) > 0):
                                self.staircases[i].step = self.staircases[i].step_sizes_list[self.staircases[i].step_sizes_list_index]
                                
                            # increase vi_val
                            if (self.staircases[i].step_type == "log10"):
                                
                                self.staircases[i].start_val *= 10**(self.staircases[i].step)
                            else:
                                self.staircases[i].start_val += self.staircases[i].step
                            
                            # Finish staircase if value out of bounds
                            if (self.staircases[i].max_val != 99999) and (self.staircases[i].start_val >= self.staircases[i].max_val):
                                self.staircases[i].staircase_finished = True
                                
                            self.staircases[i].n_current_up = 0
                            
                elif (direction == "down"):
                    
                        self.staircases[i].n_current_down += 1
                        self.staircases[i].n_current_up = 0
                        
                        if (self.staircases[i].n_current_down == self.staircases[i].n_down):
                            

                            # Calculate reversal
                            if (self.staircases[i].current_direction == "up"):
                                self.staircases[i].n_current_reversals += 1
                                self.staircases[i].current_direction = "down"
                                self.staircases[i].reversal_values_list.append(round(self.staircases[i].start_val, 3))
                                self.staircases[i].reversal_trial_list.append(sum(total_trials) + 1)
                                
                                if (self.staircases[i].step_sizes_list_index < len(self.staircases[i].step_sizes_list) - 1):
                                
                                    self.staircases[i].step_sizes_list_index += 1
                                
                            if (self.staircases[i].current_direction == ""):
                                self.staircases[i].current_direction = "down"

                                
                            if (len(self.staircases[i].step_sizes_list) > 0):
                                self.staircases[i].step = self.staircases[i].step_sizes_list[self.staircases[i].step_sizes_list_index]
                                
                            # decrease vi_val
                            if (self.staircases[i].step_type == "log10"):
                                
                                self.staircases[i].start_val /= 10**(self.staircases[i].step)
                            else:
                                self.staircases[i].start_val -= self.staircases[i].step
                            
                            # Finish staircase if value out of bounds
                            if (self.staircases[i].min_val != 99999) and (self.staircases[i].start_val <= self.staircases[i].min_val):
                                self.staircases[i].staircase_finished = True
                            
                            self.staircases[i].n_current_down = 0
                            
                            #self.staircases[i].n_current_reversals += 1
                                
                
                self.staircases[i].start_val = round(self.staircases[i].start_val, 3)
                
                
                self.staircases[i].responses.append(direction)
                
                
                self.staircases[i].n_current_rep += 1
                
                
                # define next instruction to unity
                self.instruction_to_unity(staircase_id, self.staircases[i].start_val)
                
            
        self.staircases[current_staircase_index].trial_number.append(sum(total_trials) + 1)
        
        for i in range(len(self.staircases)):
            list_staircases.append(self.staircases[i].__dict__)

        # updating json file
        json_file = open(self.path_to_files +"\\"+ self.staircase_file_name, "w")
        json.dump(list_staircases, json_file, indent=6)
        json_file.close()
    
    
    def instruction_to_unity(self, staircase_id, current_vi_val):
        answer_to_unity = ""
        for obj in self.objects_in_scene[staircase_id].keys():
            for key in self.objects_in_scene[staircase_id][obj].keys():
                
                if key == "attribute_dimensions":
                    pass
                else:
                    # action over attribute dimension
                    self.objects_in_scene[staircase_id][obj][key] = [round(current_vi_val, 3) for item in self.objects_in_scene[staircase_id][obj]["attribute_dimensions"]]
        
        # Next staircase
        next_staircase_id = random.choice(self.staircases_ids)
        
        
        next_staircase = None
        
        for i in range(len(self.staircases)):
            
            if (self.staircases[i].staircase_id == next_staircase_id):
                
                next_staircase=self.staircases[i]
        
        
        #print (self.objects_in_scene[next_staircase_id])
        for obj in self.objects_in_scene[next_staircase_id].keys():
            
            answer_to_unity += str(obj) + ";"
            
            for key in self.objects_in_scene[next_staircase_id][obj].keys():
                
                if key == "attribute_dimensions":
                    
                    pass
                
                else:

                    answer_to_unity += key + ";" + "_".join([str(self.objects_in_scene[next_staircase_id][obj][key][i]) for i in range(len(self.objects_in_scene[next_staircase_id][obj][key]))])
                    
                    answer_to_unity += ";" + str(next_staircase_id) # next staircase (random for the moment)
                    
                    answer_to_unity += ";" + str(next_staircase.staircase_finished)
                    
                    answer_to_unity += ":" # separation with next object in array
                    
        print (answer_to_unity)
                    
        with open(self.path_to_files + "\\experiment_data.pickle", 'wb') as handle:
            pickle.dump(self.objects_in_scene, handle, protocol=pickle.HIGHEST_PROTOCOL)
        