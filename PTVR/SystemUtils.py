# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 14:52:07 2020

@author: Vive VR
"""
import os
import sys
import subprocess
import json
from pathlib import Path
# defining where is the users ptvr python script
PWD = pathname = os.path.dirname(sys.argv[0])
PWD = os.path.abspath(PWD)
PWD = PWD.replace("/mnt/c/", "C:\\")
PWD = PWD.replace("/", "\\")

# giving a parameter that locate the Unity's executable path.
UnityPTVRRoot = __file__[0:-36]

UnityPTVRPath = UnityPTVRRoot + "\PTVR_Researchers\PTVR_Operators\PTVR.exe"


# test AssetBundles path

def check_path_to_asset_bundles(paths_to_asset_bundles):

    return os.path.isfile(paths_to_asset_bundles)


def check_path_to_prefabs_in_asset_bundle(paths_to_asset_bundles, paths_to_prefabs_in_asset_bundles):

    with open(paths_to_asset_bundles.replace(".com", "") + ".manifest") as f:
        f_read = f.read()

    f_split = (f_read.split("Assets:\n")[1]).split(
        "Dependencies")[:-1][0].split("- ")[1:]

    return paths_to_prefabs_in_asset_bundles + "\n" in f_split


# function to execute the experiment

def test_environment():
    value_argv = sys.argv[0]
    value_getcwd = os.getcwd()
    path_argv = Path(value_argv)
    path_getcwd = Path(value_getcwd)
    parent_path_argv = path_argv.parent
    parent_path_getcwd = path_getcwd.parent

    print("argv={}".format(value_argv))
    print("path={}".format(path_argv))
    print("parent={}".format(parent_path_argv))

    print()

    print("getcwd={}".format(value_getcwd))
    print("path={}".format(path_getcwd))
    print("parent={}".format(parent_path_getcwd))


def get_ptvr_app_name():
    """ returns the filename of the main python script (without the .py extension)."""
    return Path(sys.argv[0]).resolve().stem


def get_ptvr_app_category():
    """ returns the 'category' of the ptvr application (Demos, Experiments, Debugs or Externals)."""
    app_category = None

    # path to the directory containing the python script being executed.
    # do NOT use os.getcwd as this returns different paths based on the IDE being used.
    # parent directoy of the script being executed.
    app_path = Path(sys.argv[0]).parent.resolve()

    # use '==' for string comparison here.
    # when using str1 in str2, we get True when str1 is an empty string
    # and that case happens when we reach the root level of a path
    for folder in app_path.parents:
        if folder.name == "Demos":
            app_category = "Demos"
        elif folder.name == "Experiments":
            app_category = "Experiments"
        elif folder.name == "Debugs":
            app_category = "Debugs"

        if app_category is not None:
            break

    if not app_category:
        app_category = "Externals"

    return app_category


def get_path_to_app_json_directory():
    # path to current ptvr app, i.e the parent directory containing the script being executed.
    # (os.getcwd() didn't return the expected path when using other environment as Visual Studio)
    # app_path = Path(os.getcwd()) # os.getcwd() is working only in spyder
    app_path = Path(sys.argv[0]).parent.resolve()

    # path to PTVR_Researchers/Python_Scripts/<app_category>
    app_category_path = Path(UnityPTVRRoot) / "PTVR_Researchers" / \
        "Python_Scripts" / get_ptvr_app_category()
    app_category_path = app_category_path.resolve()

    # get the ptvr app's folder hierarchy
    try:
        folder_hierarchy = app_path.relative_to(app_category_path)
    except ValueError:
        folder_hierarchy = app_path.name

    # attached folder hierarchy to PTVR_Researchers/PTVR_Operators/<app_category>
    return Path(UnityPTVRRoot) / "PTVR_Researchers" / "PTVR_Operators" / "JSON_Files" / get_ptvr_app_category() / folder_hierarchy

# JSON Encoders


class CustomEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, "to_json"):
            return obj.to_json()
        return super().default(obj)

# SV: as I don't know the details for folder structures
# for all experiments, I prefer to keep a backup
# of the old code in the case is needed.


def OriginalLaunchThe3DWorld(jsonFileCategory="Externals"):
    path = os.path.abspath(sys.argv[0]).split("\\")
    nameScript = path[-1].replace(".py", "")
    # DO NOT use os.getcwd for getting getting the path
    # the current working directory CWD is set different based on from where
    # the python script gets executed.
    # Use proper libraries to deal with paths!
    # the_3D_world_path = os.getcwd()
    # the_3D_world_path = str(Path(sys.argv[0]).parent)
    the_3D_world_path = os.getcwd()

    value = the_3D_world_path.split("\\")

    category = []
    fullpath = ""
    jsonFilename = nameScript+".json"

    hasFindPathToCopy = False
    for i in range(len(value)):
        if hasFindPathToCopy:
            category.append(value[i])
        if not hasFindPathToCopy:
            if "Experiments" in jsonFileCategory:
                jsonFileCategory = "Experiments"
            elif "Demos" in jsonFileCategory:
                jsonFileCategory = "Demos"
            elif "Debugs" in jsonFileCategory:
                jsonFileCategory = "Debugs"
            else:
                jsonFileCategory = "Externals"
        if "Demos" in value[i]:
            jsonFileCategory = "Demos"
            hasFindPathToCopy = True
        elif "Debugs" in value[i]:
            jsonFileCategory = "Debugs"
            hasFindPathToCopy = True
        elif "Experiments" in value[i]:
            jsonFileCategory = "Experiments"
            hasFindPathToCopy = True

    for i in range(len(category)):
        fullpath = fullpath + category[i] + "\\"
       # To do add externals
    print(UnityPTVRRoot+"PTVR_Researchers\PTVR_Operators\JSON_Files\\" +
          jsonFileCategory + "\\" + fullpath + jsonFilename)
    subprocess.call([UnityPTVRPath, UnityPTVRRoot+"PTVR_Researchers\PTVR_Operators\JSON_Files\\" +
                    jsonFileCategory + "\\" + fullpath + jsonFilename])


# SV: Follow Python's PEP8 coding style!
# function and method names should use lower case with underscores
def LaunchThe3DWorld(jsonFileCategory="Externals"):
    path_to_json_directory = get_path_to_app_json_directory()
    path_to_json_file = path_to_json_directory / \
        (get_ptvr_app_name() + ".json")
    print(f"Launching PTVR.exe {path_to_json_file}")
    print("\nDo not forget: many errors that appear in the Python console might be solved by looking at the docstrings of objects (see FAQ about TypeError messages in the PTVR documentation). It is also useful to look at the Changelog in the PTVR documentation after releases that might have created some compatibility issues with previous code.")
    # had to be casted to string as python < 3.7.9 fails
    subprocess.call([UnityPTVRPath, str(path_to_json_file)])
