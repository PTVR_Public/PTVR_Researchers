import os
import subprocess
import sys      
from PTVR.Stimuli.Utils import MetaData


class Counting(MetaData):
    def __init__(self):
        self.counter = 0
        
    def UpdateCounter(self):
        self.counter += 1
    
def SetId():
    counter = 1
    Counting().UpdateCounter()