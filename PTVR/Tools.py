# -*- coding: utf-8 -*-
import numpy as np
from scipy.spatial.transform import Rotation as R
import PTVR.LogManager as log
import math

def scalar_product(u,v):
    product_a = u[0]*v[0] + u[1]*v[1] + u[2]*v[2]
    return product_a

def norm(u):
    norm_u = np.sqrt(np.square(u[0])+np.square(u[1])+np.square(u[2]))
    return norm_u

def normalize(u):
    norm_u = norm(u)
    if(norm_u==0):
        log.SetError("Operation impossible : You are attemting to normalize a null vector ")
        return u
    else:
        w = u / norm(u)
        return w

def vector_product(u=np.array([0.0,0.0,0.0]),v=np.array([0.0,0.0,0.0])):
    x = u[1]*v[2] - u[2]*v[1]
    y = u[2]*v[0] - u[0]*v[2]
    z = u[0]*v[1] - u[1]*v[0]
    w = - np.array([x,y,z]) # le produit vectoriel de 2 vecteurs dans un espace orienté à gauche et l'opposé du produit vectoriel des memes vecteurs dans le même espace mais orienté à droite.
    return w

## Conversion
def deg_to_rad(deg):
    rad = deg * np.pi/180.0
    return rad
    
def rad_to_deg(rad):
    deg = rad * 180.0/np.pi
    return deg
    
def points_to_vec(point_a,point_b):
    vec_x = point_b[0] - point_a[0]
    vec_y = point_b[1] - point_a[1]
    vec_z = point_b[2] - point_a[2]      
    vec = np.array([vec_x,vec_y,vec_z])
    return  vec

def distance_between_two_points(point_a,point_b):
    vec = points_to_vec(point_a, point_b)
    norm_val = norm(vec)
    return norm_val

def angle_between_two_vectors_rad(a_vec,b_vec):
    """
    Parameters
    ----------
    a_vec : Vector of reference, for example x_axis

    b_vec : Vector 

    NB : the order of vectors does not matter.
    
    Returns
    -------
    angle_rad : Angle between two vector
    The angle of the ray intersecting the unit circle at the given x-coordinate in radians [0, pi].
    Par convention, l’angle entre deux vecteurs réfère au plus petit angle positif entre ces deux vecteurs, qui est celui entre 0∘ et 180∘.
    """
    angle_rad = np.arccos((scalar_product(u=a_vec, v=b_vec))/(norm(a_vec)*(norm(b_vec))))
    return angle_rad

def angle_between_two_vectors_deg(a_vec,b_vec):
    """
    Parameters
    ----------
    a_vec : Vector

    b_vec : Vector


    Returns
    -------
    angle_deg: Angle classique entre deux vecteurs en degrée.
    Par convention, l’angle entre deux vecteurs réfère au plus petit angle positif entre ces deux vecteurs, qui est celui entre 0∘ et 180∘.
    """
    angle_rad = angle_between_two_vectors_rad(a_vec,b_vec)
    angle_deg = rad_to_deg(angle_rad)
    return angle_deg

def global_to_local_cartesian_coordinates(point_in_global_cs,local_cs_origin,local_cs_axis_X,local_cs_axis_Y,local_cs_axis_Z):
    p_vec = point_in_global_cs - local_cs_origin
    point_in_local_cs_x = scalar_product(p_vec, local_cs_axis_X)
    point_in_local_cs_y = scalar_product(p_vec, local_cs_axis_Y)
    point_in_local_cs_z = scalar_product(p_vec, local_cs_axis_Z)
    point_in_local_cs = np.array([point_in_local_cs_x,point_in_local_cs_y,point_in_local_cs_z])
    return point_in_local_cs

def local_to_global_cartesian_coordinates(point_in_local_cs,local_cs_origin,local_cs_axis_X,local_cs_axis_Y,local_cs_axis_Z):
    point_in_global_cs = local_cs_origin + point_in_local_cs[0] * local_cs_axis_X + point_in_local_cs[1] * local_cs_axis_Y + point_in_local_cs[2] * local_cs_axis_Z
    return point_in_global_cs 

def intersection_between_line_and_tangent_screen(origin, line_vector_eccentricity_deg, line_vector_half_meridian_deg,tangent_screen_origin):
    g_vec = set_spherical_to_cartesian_vec(radial_distance=1.0, eccentricity_deg = line_vector_eccentricity_deg , half_meridian_deg = line_vector_half_meridian_deg )
  
    Z_tangent_screen = normalize(tangent_screen_origin-origin)
    sc = scalar_product(g_vec, Z_tangent_screen)
    
    if(sc == 0.0):
        log.SetError("Line doesn't intersect tangent screen.")
        quit()  # PK@JTM Error ?
        return None
    else :
        d = norm(tangent_screen_origin-origin)
        a = d/sc
        intersection = origin + a * g_vec 
        return intersection

def capoc_ray(origin, line_vector_eccentricity_deg, line_vector_half_meridian_deg):
    g_vec = set_spherical_to_cartesian_vec(radial_distance=1.0, eccentricity_deg = line_vector_eccentricity_deg , half_meridian_deg = line_vector_half_meridian_deg )
    point = origin + 400.0 * g_vec
    return point

# PK@JTM New !
def projected_segment_orientation_deg(original_screen_X_axis,
                                      original_screen_Y_axis,
                                      capoc_on_original_screen_wrt_current_cs, 
                                      segment_orientation_on_original_screen_deg,
                                      destination_screen_X_axis,
                                      destination_screen_Y_axis,
                                      destination_screen_distance_from_current_cs_origin): 
    """
    
    Cette fonction a été developpée pour les segments
    
    Un segment ayant une orientation_origin sur un screen_origin est projeté sur un screen_destination. 
    Cette fonction retourne son orientation dans ce screen_origin

    Les notations dans cette fonction se basent sur une figure de PK qu'il faudra mettre quelquepart. 
    On pourrait mettre ca dans une technote ou dans notre Wiki et y faire reference ici
        
    Remarques :
        1) Le capoc appartient au original screen ce qui permet de le positionner
        2) Pour l'ecran destination, il est positionné de sorte que le capoc projet 
           sur l'écran destination soit a une distance de destination_screen_distance_from_current_cs_origin
           par rapport a l'origine du current_cs'
    
    """
    
    print("[Enter function projected_segment_orientation_deg]")
    print("[parameter] original_screen_X_axis = " + str(original_screen_X_axis))
    print("[parameter] original_screen_Y_axis = " + str(original_screen_Y_axis))
    print("[parameter] capoc_on_original_screen_wrt_current_cs = " + str(capoc_on_original_screen_wrt_current_cs))
    print("[parameter] segment_orientation_on_original_screen_deg = " + str(segment_orientation_on_original_screen_deg))
    print("[parameter] destination_screen_X_axis = " + str(destination_screen_X_axis))
    print("[parameter] destination_screen_Y_axis = " + str(destination_screen_Y_axis))
    
    #Etape 0 : Conversion en radians pour les operations tangente and g_vec computation
    segment_orientation_on_original_screen_rad = deg_to_rad(segment_orientation_on_original_screen_deg)
    g_vec = normalize(capoc_on_original_screen_wrt_current_cs)
    
    # Etape 1 : Calculer les coordonnées de M' par rapport au current coordinate system
    ## D'abord on calcule en local 
    point_M_on_original_screen_wrt_current_cs = capoc_on_original_screen_wrt_current_cs + np.tan(segment_orientation_on_original_screen_rad) * original_screen_Y_axis + original_screen_X_axis
    
    # Etape 2 : Calculer le projeté de K' sur l'écran destination -> K"
    capoc_on_destination_screen_wrt_current_cs = destination_screen_distance_from_current_cs_origin * capoc_on_original_screen_wrt_current_cs / scalar_product(u=g_vec, v=capoc_on_original_screen_wrt_current_cs)

    # Etape 3 : Calculer le projeté de M' sur l'écran destination -> m'
    projection_of_point_M_on_original_screen_to_destination_screen_wrt_current_cs = destination_screen_distance_from_current_cs_origin * point_M_on_original_screen_wrt_current_cs / scalar_product(u=g_vec, v=point_M_on_original_screen_wrt_current_cs)
    
    # Etape 4 (PK : en fait cette étape est inutile je pense et peut etre sautée): Calculer M" comme l'intersection de (K"m') avec (x"=1) -> M" et cakcul de l'orientation
    # On doit résoudre un systeme de 3 équations à 2 inconnues...
    # Source (Méthode) : https://homeomath2.imingo.net/geoesp8.htm
    # Source (AX=B) : https://homeomath2.imingo.net/geoesp8.htm
    # Méthode : 
    #  - Resoudre les deux premieres pour en déduire les parametres
    #  - Verifier si la 3eme est verifiee. Si oui, c'est qu'elles s'intersectent. 
    #  - Si non, elles ne s'intersectent pas et dans notre cas ca voudra dire un angle de +/-90 degres
    #gamma_vec = projection_of_point_M_on_original_screen_to_destination_screen_wrt_current_cs - capoc_on_destination_screen_wrt_current_cs
    # On va prendre les deux premieres équation et resoudre un systeme du type AX=B
    #A_11 = destination_screen_Y_axis[0]
    #A_21 = destination_screen_Y_axis[1]
    #A_12 = -gamma_vec[0]
    #A_22 = -gamma_vec[1]
    #A_determinant = A_11 * A_22 - A_21 * A_12
    #if (A_determinant == 0.0):
    #    segment_orientation_on_destination_screen_deg = 90.0
    #else:
    #    B_1 = -destination_screen_X_axis[0]
    #    B_2 = -destination_screen_X_axis[1]
    #    X_1 = ( A_22*B_1 - A_12*B_2 ) / A_determinant
    #    X_2 = ( A_11*B_2 - A_21*B_1 ) / A_determinant
    #    # Dans notre cas precis, c'est sur que la 3eme equation sera verifiée donc pas besoin de verifier
    #    point_M_on_destination_screen_wrt_current_cs = capoc_on_destination_screen_wrt_current_cs + X_2 * gamma_vec
    #    # Passer TODO @PK@JTM
    #    capoc_on_destination_screen_wrt_local_cs = ... 
    #    point_M_on_destination_screen_wrt_local_cs = ...
    #    segment_orientation_on_destination_screen_deg = segment_orientation_from_local_cartesian_coordinates_deg(point_a = capoc_on_destination_screen_wrt_local_cs, point_b = point_M_on_destination_screen_wrt_local_cs)
 
    # Etape 4 : On calcule l'orientation en local de K"m" par rapport aux axes du destination_screen
    destination_screen_Z_axis = vector_product(u = destination_screen_X_axis, v = destination_screen_Y_axis)
    capoc_on_destination_screen_wrt_local_cs = global_to_local_cartesian_coordinates(point_in_global_cs = capoc_on_destination_screen_wrt_current_cs,
                                                                                        local_cs_origin = capoc_on_destination_screen_wrt_current_cs,
                                                                                        local_cs_axis_X = destination_screen_X_axis,
                                                                                        local_cs_axis_Y = destination_screen_Y_axis,
                                                                                        local_cs_axis_Z = destination_screen_Z_axis)
    print("[variable] capoc_on_destination_screen_wrt_local_cs = " + str(capoc_on_destination_screen_wrt_local_cs) + " (pour verification, doit etre [0,0,0])")
    projection_of_point_M_on_original_screen_to_destination_screen_wrt_local_cs = global_to_local_cartesian_coordinates(point_in_global_cs = projection_of_point_M_on_original_screen_to_destination_screen_wrt_current_cs,
                                                                                        local_cs_origin = capoc_on_destination_screen_wrt_current_cs,
                                                                                        local_cs_axis_X = destination_screen_X_axis,
                                                                                        local_cs_axis_Y = destination_screen_Y_axis,
                                                                                        local_cs_axis_Z = destination_screen_Z_axis)
    print("[variable] projection_of_point_M_on_original_screen_to_destination_screen_wrt_local_cs = " + str(projection_of_point_M_on_original_screen_to_destination_screen_wrt_local_cs))
    
    segment_orientation_on_destination_screen_deg = segment_orientation_from_local_cartesian_coordinates_deg(point_a = capoc_on_destination_screen_wrt_local_cs, point_b = projection_of_point_M_on_original_screen_to_destination_screen_wrt_local_cs)
    print("[variable] segment_orientation_on_destination_screen_deg = " + str(segment_orientation_on_destination_screen_deg))

    return segment_orientation_on_destination_screen_deg
    

def segment_orientation_to_segment_projected_orientation_deg(orientation_deg, tangent_screen_X_axis,tangent_screen_Z_axis,  capoc_AB_pos_in_current_cs):
    """
    
    PK : WARNING THIS FUNCTION IS WRONG AND IS GOING TO BE REMOVED

    
    Parameters
    ----------
    orientation_deg : 
    segment orientation on tangent_screen
    tangent_screen_X_axis : X axis tangent_screen
    tangent_screen_Z_axis : Z axis tangent_screen
    capoc_AB_pos_in_current_cs : 

    Returns
    -------
    segment_projected_orientation : deg
    """
    print("[Inside function segment_orientation_to_segment_projected_orientation_deg]")
    print("[value given] orientation_deg = " + str(orientation_deg))
    print("[value given] tangent_screen_X_axis = " + str(tangent_screen_X_axis))
    print("[value given] tangent_screen_Z_axis = " + str(tangent_screen_Z_axis))
    print("[value given] capoc_AB_pos_in_current_cs = " + str(capoc_AB_pos_in_current_cs))
        
    orientation_rad = deg_to_rad(orientation_deg)
    
    gamma_vec = normalize(capoc_AB_pos_in_current_cs)
    print("[variable] gamma_vec = " + str(gamma_vec) + " (la direction qui vise le capoc depuis l'origine du current)")
    
    phi_alpha_rad = angle_between_two_vectors_rad(tangent_screen_X_axis, gamma_vec)
    print("[variable] phi_alpha_deg = " + str(rad_to_deg(phi_alpha_rad)))
    
    beta_vec = rotate_about_vector(tangent_screen_X_axis, -tangent_screen_Z_axis,  orientation_deg)
    print("[variable] beta_vec = " + str( beta_vec))
    
    phi_beta_rad = angle_between_two_vectors_rad(beta_vec, gamma_vec)
    print("[variable] phi_beta_deg = " + str(rad_to_deg(phi_beta_rad)))
    
    projected_orientation_rad = np.arccos((np.cos(orientation_rad)-np.cos(phi_alpha_rad)*np.cos(phi_beta_rad))/(np.sin(phi_alpha_rad)*np.sin(phi_beta_rad)))
    projected_orientation_deg = rad_to_deg(projected_orientation_rad)
    print("[variable] projected_orientation_deg = " + str(projected_orientation_deg))
    print("[Remark] if capoc_eccentricity_deg = 0, we should have projected_orientation_deg = orientation_deg")
    print("[Exit function segment_orientation_to_segment_projected_orientation_deg]")
    
    return projected_orientation_deg
 
def segment_projected_orientation_to_segment_orientation_deg(projected_orientation_deg, tangent_screen_X_axis,tangent_screen_Z_axis,  capoc_eccentricity_deg, capoc_half_meridian_deg ):
    """

    PK : WARNING THIS FUNCTION IS WRONG AND IS GOING TO BE REMOVED


    Parameters
    ----------
    projected_orientation_deg : 
    tangent_screen_X_axis : X axis tangent_screen
    tangent_screen_Z_axis : Z axis tangent_screen
    capoc_eccentricity_deg : 
    capoc_half_meridian_deg : 

    Returns
    -------
    orientation_deg : 
       segment orientation on tangent_screen

    """
   
    projected_orientation_rad = deg_to_rad(projected_orientation_deg)
    
    gamma_vec = set_spherical_to_cartesian_vec(radial_distance= 1.0, 
                                           eccentricity_deg = capoc_eccentricity_deg ,
                                           half_meridian_deg = capoc_half_meridian_deg )
 
    na_vec = normalize(vector_product(gamma_vec,tangent_screen_X_axis))
    
    nb_vec = rotate_about_vector(na_vec, -gamma_vec, projected_orientation_deg)
  
    beta_vec = vector_product(nb_vec, tangent_screen_Z_axis) 
    orientation_deg = angle_between_two_vectors_deg(tangent_screen_X_axis, beta_vec)
    
    return orientation_deg

def segment_orientation_from_local_cartesian_coordinates_deg(point_a,point_b):
    """
    on donne les extremités d'un segment sur un ecran tangent 
    on veut connaitre son orientation.

    Parameters
    ----------
    point_a : TYPE
        DESCRIPTION.
    point_b : TYPE
        DESCRIPTION.
    origin : TYPE
        DESCRIPTION.
    x_axis : TYPE
        DESCRIPTION.
    y_axis : TYPE
        DESCRIPTION.
    z_axis : TYPE
        DESCRIPTION.

    Returns
    -------
    orientation_deg : TYPE
        DESCRIPTION.
    orientation between -90 and 90 deg 

    """
    print("[Enter function segment_orientation_from_local_cartesian_coordinates_deg]")
    print("[parameter] point_a : " +str(point_a))
    print("[parameter] point_b : " +str(point_b))
    ab_vec = points_to_vec(point_a=point_a, point_b=point_b)
    point_c = (point_a + point_b)/2 # centre segment ab
    print ("[parameter] point_c : "+ str(point_c))
    beta = scalar_product(u = ab_vec , v = np.array([1.0,0.0,0.0]))
    
    if(beta == 0.0):
        orientation_deg = 90.0
    else:
        alpha = 1.0/beta
        point_m = point_c + alpha * ab_vec
        print("     point m : " + str(point_m))
        orientation_rad = np.arctan(point_m[1]-point_c[1])
        orientation_deg = rad_to_deg(orientation_rad)
    print("[variable] orientation_deg :"+ str(orientation_deg))
    print("[Exit function segment_orientation_from_local_cartesian_coordinates_deg]")
    return orientation_deg


def cartesian_to_spherical (x_input = 0.0, y_input = 0.0 , z_input = 0.0,
                               azimuth_reference_cardinal_direction  = "east",
                               azimuth_rotation_direction = "counterclockwise",
                               CS_spatial_configuration = "left-handed"):

    """
    For Explanations and terminology see this permanent link:
    (Date retrieved: 31 October 2024 16:11 UTC)
    https://en.wikipedia.org/w/index.php?title=Spherical_coordinate_system&oldid=1251776067    

    Following the conventions used in the link above, the current function uses
    the PHYSICS convention: radial distance, polar angle, azimuthal angle (rho, theta, phi).
    Polar angle is the angle between the radial line and the polar axis (here Z).
    Azimuthal angle is the angle of rotation of the radial line around the polar axis.
    
    The spatial configuration of the cartesian Coordinate System (CS) is LEFT-handed
    (as in Unity) by default. This means that the x and y axis have been exchanded !!!
    
    The right-handed spatial configuration is not yet implemented.

    Parameters
    ----------
    x , y and z: cartesian coordinates in meters
    
    azimuth_reference_cardinal_direction: "east", "west", "north" or "south". The default is "east". 
    Cardinal direction of the azimuth reference axis as seen 
    from the negative Z axis side. 
    
    azimuth_rotation_direction: "clockwise" or "counterclockwise". The default is "counterclockwise"
    Direction of rotation of the azimuth in the reference plane (here xy plane) as seen 
    from the negative Z axis side. 
    
    CS_spatial_configuration : Coordinate System spatial configuration. 
    The Default is "left-handed" (as in Unity)
    
    Returns
    -------
    rho : radial distance in meters.
    theta : polar angle in degrees in the interval [0°, 360°). 
    phi : azimuthal angle (aka azimuth) in degrees in the 
        half-open interval (−180°, +180°].
    
    Created by EC : oct 2024
    """
    z = z_input 
    
    #@EC: on pourrait simplifier les if ci-dessous j'ai l'impression
 
    
    # the following only concerns x and y as they are in 
    # the reference plane (ie xy plane)
    # See https://en.wikipedia.org/wiki/Atan2   
    # sections East-counterclockwise, north-clockwise, etc.   
    if azimuth_reference_cardinal_direction == "east":    # default
        x_prov = x_input; y_prov = y_input; 
        if azimuth_rotation_direction == "counterclockwise":
            x = x_prov ; y = y_prov
        if azimuth_rotation_direction == "clockwise":
            x = x_prov ; y = -y_prov
            
    elif azimuth_reference_cardinal_direction == "west":
        x_prov = -x_input; y_prov = -y_input; 
        if azimuth_rotation_direction == "counterclockwise":
            x = x_prov ; y = y_prov
        if azimuth_rotation_direction == "clockwise":
            x = x_prov ; y = -y_prov
            
    elif azimuth_reference_cardinal_direction == "north":
        x_prov = y_input; y_prov = x_input; # Exchange x and y
        if azimuth_rotation_direction == "counterclockwise":
            x = x_prov ; y = -y_prov
        if azimuth_rotation_direction == "clockwise":
            x = x_prov ; y = y_prov

    elif azimuth_reference_cardinal_direction == "south":
        x_prov = y_input; y_prov = x_input; # Exchange x and y
        if azimuth_rotation_direction == "counterclockwise":
            x = -x_prov ; y = y_prov
        if azimuth_rotation_direction == "clockwise":
            x = -x_prov ; y = -y_prov
    
    rho = math.sqrt(x**2 + y**2 + z**2) # radial distance
    theta = math.degrees (math.atan2 ( math.sqrt(x**2 + y**2), z ) ) # polar angle in physics convention 
    phi_prov =  math.degrees (math.atan2(y, x) ) # azimuthal angle in physics convention     
    # !! atan2 returns values in the half-open interval (−180°, +180°] !!!
    #print (f"phi without rounding: {phi_prov}")
    
    phi = round (phi_prov, 10)
    #print (f"round(phi): {phi}")
    # This rounding is to obtain 0 instead of -3.508354649267438e-15
    # The latter value causes an issue for ex. in cartesian_to_perimetric ()
    # when getting phi modulo 360:
    # this produces:  phi % 360 = 360
    # whereas this should produce 0 (and 0%360 DOES result in 0 !! ie not 360)
         
    return rho, theta, phi 
# END of set_cartesian_to_spherical ()    
# ----------------------------------------


def cartesian_to_perimetric (x = 0.0, y = 0.0 , z = 0.0,
                               half_meridian_reference_cardinal_direction  = "east",
                               half_meridian_rotation_direction = "counterclockwise"): 
    """
    Same as cartesian_to_spherical () except that:
        a/ the returned values are called:
            'radial distance', 'eccentricity', 'half_meridian'
            (instead of 'rho', 'theta', 'phi').
        b/  The half-meridian values are in the interval [0°, 360°)
            (whereas the corresponding phi was in (−180°, +180°]).
        
                                                          
    Created by EC : oct 2024                                                      
    """
    
    rho, theta, phi = cartesian_to_spherical (
            x , y , z,
            azimuth_reference_cardinal_direction = half_meridian_reference_cardinal_direction,
            azimuth_rotation_direction = half_meridian_rotation_direction)
    
    radial_distance = rho
    eccentricity = theta    
    
    # Modulo operator
    half_meridian = phi % 360  # In PTVR, HM needs to be in [0°, 360°) whereas 
    # phi was in (−180°, +180°]
    
    half_meridian = half_meridian
    
    return radial_distance, eccentricity, half_meridian
# END of cartesian_to_perimetric ()    
# ----------------------------------------

    
# EC: TODO : pourquoi mettre set_ devant ?
def set_spherical_to_cartesian (eccentricity_deg = 0.0, half_meridian_deg = 0.0 , radial_distance = 0.0):
    """
    """

    perimetricCoordinates = np.array([0.0,0.0,0.0])
    eccentricity_rad = eccentricity_deg*np.pi/180.0
    half_meridian_rad =  half_meridian_deg*np.pi/180.0
    theta = np.arctan(np.abs(np.sin(eccentricity_rad))/np.cos(eccentricity_rad)) ## To Remove eccentricity negative
    if(theta < 0.0):
        theta = theta+np.pi
    perimetricCoordinates[0] = float(theta)
    perimetricCoordinates[1] = float(half_meridian_rad)
    perimetricCoordinates[2] = float(radial_distance)
  
    x = float(perimetricCoordinates[2]*np.cos(perimetricCoordinates[1])*np.sin(perimetricCoordinates[0]))
    y = float(perimetricCoordinates[2]*np.sin(perimetricCoordinates[1])*np.sin(perimetricCoordinates[0]))
    z = float(perimetricCoordinates[2]*np.cos(perimetricCoordinates[0]))
                  
    return x,y,z

def set_spherical_to_cartesian_vec(eccentricity_deg = 0.0, half_meridian_deg = 0.0 , radial_distance = 0.0):
    x,y,z = set_spherical_to_cartesian(eccentricity_deg=eccentricity_deg,half_meridian_deg=half_meridian_deg,radial_distance=radial_distance)
    vec = np.array([x,y,z])
    return vec

def rotate_about_vector(u_vec,v_vec,theta_deg):
        """
        Methode pour retourner la rotation d'un vecteur 
           @param u est le vecteur que je veux tourner 
           @param v est l'axe de rotation
           @param theta est l'angle de rotation
        """
        # u est le vecteur que je veux tourner
        # v est l'axe de rotation
        # theta est l'angle de rotation
        r = R.from_rotvec(v_vec*theta_deg,degrees=True)  # we use minus sign to have left hand rotation -v_vec
        u_vec = r.apply(u_vec)
        
        return u_vec


def translate_along_axis(u,axis_cs_x,axis_cs_y,axis_cs_z,epsilon):
          """
          Methode pour retourner la translation d'un vecteur le long d'un vecteur
           @param epsilon : indique le vecteur 3 de translation 
           @param axis : indiquer le vecteur d'axe sur les quels appliquer epsilon
           @param u: indique la position de l'objet sur l'axe.
          """
          x = u[0]
          y = u[1]
          z = u[2]
          PosVector_x_x = epsilon[0] *  axis_cs_x[0]
          PosVector_x_y = epsilon[0] *  axis_cs_x[1]
          PosVector_x_z = epsilon[0] *  axis_cs_x[2]

          PosVector_y_x = epsilon[1]  * axis_cs_y[0]
          PosVector_y_y = epsilon[1]  * axis_cs_y[1]
          PosVector_y_z = epsilon[1]  * axis_cs_y[2]

          PosVector_z_x = epsilon[2]  * axis_cs_z[0]
          PosVector_z_y = epsilon[2]  * axis_cs_z[1]
          PosVector_z_z = epsilon[2]  * axis_cs_z[2]
           
          x += PosVector_x_x + PosVector_y_x + PosVector_z_x # X
          y += PosVector_x_y + PosVector_y_y + PosVector_z_y # Y
          z += PosVector_x_z + PosVector_y_z + PosVector_z_z # Z
          return x,y,z
      


def eccentricity_to_size_on_perpendicular_plane(eccentricity_in_deg,viewing_distance_in_m ):
    """ returns size in meters on perpendicular plane of the segment 
        from the point with an eccentricity of zero degree
        to the point with an eccentricity of "eccentricity_in_deg """
    return float(viewing_distance_in_m  * np.tan(deg_to_rad(eccentricity_in_deg)))

    
def visual_angle_to_size_on_perpendicular_plane(visual_angle_of_centered_object_in_deg,viewing_distance_in_m):
    """  returns size in meters on perpendicular plane of the object (centered on the 0° eccentricity point)
         subtending a visual angle of 'visual_angle_of_object' degrees """
    size_on_perpendicular_plane_in_m = 2 * eccentricity_to_size_on_perpendicular_plane((visual_angle_of_centered_object_in_deg / 2),viewing_distance_in_m)
    return float( size_on_perpendicular_plane_in_m)


## From Psychophysics of Reading in Normal and Low Vision, Gordon E Legge
def visual_angle_in_degrees_to_logmar(_visual_angle_of_centered_x_height_deg) :
    """ Converts degrees into logMar """
    
    return round( np.log10(_visual_angle_of_centered_x_height_deg*12) , 6 )

def logmar_to_visual_angle_in_degrees(_logmar_value) : 
    """Converts logMAR into degrees """
    return  round( ( (1/12) * np.exp(_logmar_value * np.log(10) ) ) , 6 )

def logmar_to_size_on_tangent_screen(_logmar,_distance) : 
    return (2 * _distance * np.tan(np.pi / 4320 * np.exp(_logmar*np.log(10)))) 

def eccentricity_in_deg(size_on_tangent_screen,radial_distance):
    #return np.degrees( 2 * ( np.atan(size_on_tangent_screen/2*radial_distance) ))
    return np.degrees( 2 * ( np.atan(size_on_tangent_screen/2*radial_distance) ))

def angle_to_height(angle, distanceXP): # EC: 11 nov enlevé le " distanceXP = 500"
    return distanceXP*np.tan(angle/180*np.pi)
        
          