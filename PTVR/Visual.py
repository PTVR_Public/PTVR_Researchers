"""
Interface for The 3DWorld, which holds all information and structures related to the World being designed.
"""
import datetime
import json
import logging
import PTVR.SystemUtils
import PTVR.Stimuli.Scenes
import os
import os.path
import sys
import shutil
import re
import copy
from PTVR.CalculatorManager import CalculatorManager
import numpy as np
import PTVR.Stimuli.Color as color
from pathlib import Path
from PTVR.Pointing.PointingCursor import MaskedEye

from PTVR.SystemUtils import get_ptvr_app_category, get_ptvr_app_name


class InvalidExperimentFile(Exception):
    pass


class OriginCurrentCS():
    def __init__(self):
        self.id = -4


class OriginGlobalCS():
    def __init__(self):
        self.id = -5

# TODO : add id and buton to use


class Headset():
    def __init__(self):
        self.id = -3


class Eye():
    def __init__(self, id_eye):
        self.id = id_eye

# TODO : Choose beetween hand or controller (or both?)
# add id each button


class HandController():
    def __init__(self, id_controller):
        self.id = id_controller

# This class is ultimately written to file


class The3DWorld():
    custom_data = {}
    id_scene = []
    coordinate_system = []
    """
    @jsonReleaseCategory indicate the folder Sub directory(in the Release directory) where the json file are store.
    """

    def __init__(self, name_of_subject="DummyUser", detailsThe3DWorld="",
                 resultsPath="", output_headset=False,
                 output_raw_sranipall_gaze_data=False,
                 output_hand_controller=False,
                 hand_controller_save_sampling_period=10,
                 # 10 ms is the defaullt value, otherwise there is a risk of skipping frames
                 hand_controller_events_are_recorded=False,
                 headset_save_sampling_period=10, operatorMode=False):
        self.calculatorManager = CalculatorManager()
        # Save a data file of head position in the scene.
        # name of the experiment. Contained in the output data files
        self.created = datetime.datetime.now()

        self.details3DWorld = detailsThe3DWorld
        self.details3DWorld = re.sub(r"\n|\r", " ", self.details3DWorld)
        self.details3DWorld = re.sub(r"\"", "\\\"", self.details3DWorld)

        self.handControllerLeft = HandController(id_controller=-1)
        self.handControllerRight = HandController(id_controller=-2)
        # self.idHandControllerLeft = -1
        # self.idHandControllerRight = -2

        self.headset = Headset()
        self.eyeLeft = Eye(id_eye=-6)
        self.eyeRight = Eye(id_eye=-7)
        self.eyeCombined = Eye(id_eye=-8)
        self.originCurrentCS = OriginCurrentCS()
        self.originGlobalCS = OriginGlobalCS()

        self.coordinate_system = PTVR.Stimuli.Utils.CoordinateSystem()
        self.last_coordinate_system_index = 0

        self.provided_results_path = resultsPath

        default_path_to_results = Path(PTVR.SystemUtils.UnityPTVRRoot) / \
            "PTVR_Researchers" / "PTVR_Operators" / "Results" / get_ptvr_app_name()
        self.results_path = resultsPath
        if resultsPath != "":
            self.results_path = resultsPath + get_ptvr_app_name()
        else:
            self.results_path = default_path_to_results

        self.user = name_of_subject
        self.scenes = {}
        self.leftBlind = False
        self.rightBlind = False
        # dt between every head position saves
        if (headset_save_sampling_period < 10):
            PTVR.LogManager.SetError(
                "To obtain a 90 Hz sampling rate, set the sampling period to 10 ms. " +
                "BEWARE ! if you set the sampling period to 11 ms, the sampling rate will "
                "not be exactly 90 Hz.")
            sys.exit()
        if (hand_controller_save_sampling_period < 10):
            PTVR.LogManager.SetError(
                "To obtain a 90 Hz sampling rate, set the sampling period to 10 ms. " +
                "BEWARE ! if you set the sampling period to 11 ms, the sampling rate will "
                "not be exactly 90 Hz.")
            sys.exit()
        self.headSaveSamplingPeriod = headset_save_sampling_period
        self.handControllerSaveSamplingPeriod = hand_controller_save_sampling_period
        self.hasOperator = operatorMode
        self.origin = PTVR.Stimuli.Objects.VirtualPoint()
        self.outputHead = output_headset
        self.outputGaze = output_raw_sranipall_gaze_data
        self.outputLaser = output_hand_controller
        self.hand_controller_events_are_recorded = hand_controller_events_are_recorded
        self.countValue = -1
        self.leftBackgroundColor = color.RGB255Color()
        self.rightBackgroundColor = color.RGB255Color()
        self.translation_vector = np.array([0, 0, 0])

    def set_monocular(self, masked_eye=MaskedEye.LEFT_EYE, masked_eye_background_color=color.RGB255Color()):
        """
        masked_eye : MaskedEye.LEFT_EYE , MaskedEye.RIGHT_EYE
        masked_eye_background_color :  default is Black
        """
        if (masked_eye == MaskedEye.LEFT_EYE):
            self.leftBlind = True
            self.leftBackgroundColor = masked_eye_background_color
        if (masked_eye == MaskedEye.RIGHT_EYE):
            self.rightBlind = True
            self.rightBackgroundColor = masked_eye_background_color
            print(self.rightBackgroundColor)

    def __add_a_coordinate_system(self, vt):
        self.last_coordinate_system_index += 1
        self.coordinate_system.append(copy.deepcopy(vt))
        self.coordinate_system[-1].generate_new_id()

    # def set_coordinate_system_from_a_virtual_point(self,origin=PTVR.Stimuli.Utils.VirtualPoint()):
    #     """
    #     This method set the room calibration origin from a VirtualPoint

    #     Parameters
    #     ----------
    #     translation : TYPE, optional
    #         The virtual point defining the room calibration origin (position and rotation). The default is np.array([0,0,0]).

    #     Returns
    #     -------
    #     None.

    #     """
    #     emobj = PTVR.Stimuli.World.EmptyVisualObject()
    #     emobj.set_virtual_point_data(origin)
    #     self.__add_a_coordinate_system(emobj)

    def translate_coordinate_system_along_global(self, translation=np.array([0.0, 0.0, 0.0])):
        """
        Built-in method to move the room calibration coordinate system in xyz.

        Parameters
        ----------
        translation : TYPE, optional
            translation in XYZ coordinates. Must be a 3d np.array. The default is np.array([0,0,0]).

        Returns
        -------
        None.

        """

        # vt = copy.deepcopy(self.coordinate_system[-1])
        # # note in destination to the developers:
        # # the current coordinate system is managed by the last virtual point in self.coordinate_system
        # # it has as parent the global coordinate system.
        # # Thus a translation along the current coordinate system is equivalent
        # # to a global translation in this specific case.
        # vt.translate_along_current_vector(translation)
        # self.__add_a_coordinate_system(vt)
        self.coordinate_system.translation_global(vector=translation)
        self.translation_vector = translation

    def translate_coordinate_system_along_current(self, translation=np.array([0.0, 0.0, 0.0])):
        """
        Built-in method to move the room calibration coordinate system in xyz
        along the current CS axis.

        Parameters
        ----------
        translation : TYPE, optional
            translation in XYZ coordinates. Must be a 3d np.array. The default is np.array([0,0,0]).

        Returns
        -------
        None.

        """
        self.coordinate_system.translation_current(vector=translation)
        # vt = copy.deepcopy(self.coordinate_system[-1])
        # vt.translate_along_local_xyz(translation[0],translation[1],translation[2])
        # self.__add_a_coordinate_system(vt)

    def rotate_coordinate_system_about_current_x(self, rotation_deg):
        """
        Sequential rotation of the room calibration origin around the current X axis.
        Note that it's in direct rotation.

        Parameters
        ----------
        rotation_deg : float
            rotation in degrees.

        Returns
        -------
        None.

        """
        # vt = copy.deepcopy(self.coordinate_system[-1])
        # vt.rotate_about_object_x(rotation_deg)
        # self.__add_a_coordinate_system (vt)
        self.coordinate_system.rotation_current_x(angle=rotation_deg)

    def rotate_coordinate_system_about_current_y(self, rotation_deg):
        """
        Sequential rotation of the room calibration origin around the current Y axis.
        Note that it's in direct rotation.

        Parameters
        ----------
        rotation_deg : float
            rotation in degrees.

        Returns
        -------
        None.

        """
        # vt = copy.deepcopy(self.coordinate_system[-1])
        # vt.rotate_about_object_y(rotation_deg)
        # self.__add_a_coordinate_system(vt)
        self.coordinate_system.rotation_current_y(angle=rotation_deg)

    def rotate_coordinate_system_about_current_z(self, rotation_deg):
        """
        Sequential rotation of the room calibration origin around the current Z axis.
        Note that it's in direct rotation.

        Parameters
        ----------
        rotation_deg : float
            rotation in degrees.

        Returns
        -------
        None.

        """
        # vt = copy.deepcopy(self.coordinate_system[-1])
        # vt.rotate_about_object_z(rotation_deg)
        # self.__add_a_coordinate_system(vt)
        self.coordinate_system.rotation_current_z(angle=rotation_deg)

    def rotate_coordinate_system_about_global_x(self, rotation_deg):
        """
        Sequential rotation of the room calibration origin around the global X axis.
        Note that it's in direct rotation.

        Parameters
        ----------
        rotation_deg : float
            rotation in degrees.

        Returns
        -------
        None.

        """
        # vt = copy.deepcopy(self.coordinate_system[-1])
        # vt.rotate_about_global_x(rotation_deg)
        # self.__add_a_coordinate_system (vt)
        self.coordinate_system.rotation_global_x(angle=rotation_deg)

    def rotate_coordinate_system_about_global_y(self, rotation_deg):
        """
        Sequential rotation of the room calibration origin around the global Y axis.
        Note that it's in direct rotation.

        Parameters
        ----------
        rotation_deg : float
            rotation in degrees.

        Returns
        -------
        None.

        """
        # vt = copy.deepcopy(self.coordinate_system[-1])
        # vt.rotate_about_global_y(rotation_deg)
        # self.__add_a_coordinate_system(vt)
        self.coordinate_system.rotation_global_y(angle=rotation_deg)

    def rotate_coordinate_system_about_global_z(self, rotation_deg):
        """
        Sequential rotation of the room calibration origin around the Z global axis.
        Note that it's in direct rotation.

        Parameters
        ----------
        rotation_deg : float
            rotation in degrees.

        Returns
        -------
        None.

        """
        # vt = copy.deepcopy(self.coordinate_system[-1])
        # vt.rotate_about_global_z(rotation_deg)
        # self.__add_a_coordinate_system(vt)
        self.coordinate_system.rotation_global_z(angle=rotation_deg)

    def get_axes(self):
        """
        Returns
        -------
        axis_x,axis_y,axis_z in Global cartesian Coordinate system

        """
        x_pos, y_pos, z_pos, axis_x, axis_y, axis_z = self.coordinate_system.GetCoordinates()
        return axis_x, axis_y, axis_z

    def get_rightward_axis(self):
        """
        Returns
        -------
        rightward_axis (x-axis) : np.array([]) in global cartesian coordinate system
        """
        axis_x, axis_y, axis_z = self.get_axes()
        return axis_x

    def get_upward_axis(self):
        """
        Returns
        -------
        upward_axis (y-axis) : np.array([]) in global cartesian coordinate system
        """
        axis_x, axis_y, axis_z = self.get_axes()
        return axis_y

    def get_forward_axis(self):
        """
        Returns
        -------
        forward_axis (z-axis) : np.array([]) in global cartesian coordinate system
        """
        axis_x, axis_y, axis_z = self.get_axes()
        return axis_z

    def get_position(self):
        """
        Returns 
        -------
        np.array([x,y,z]) returns the position in GLOBAL coordinates of the CURRENT coordinate system, i.e. the position of its origin.

        """
        x, y, z, axis_x, axis_y, axis_z = self.coordinate_system.GetCoordinates()

        return np.array([x, y, z])

    def get_coordinate_system(self):
        """
        Returns
        -------
        x_pos : TYPE
            DESCRIPTION.
        y_pos : TYPE
            DESCRIPTION.
        z_pos : TYPE
            DESCRIPTION.
        axis_x : TYPE
            DESCRIPTION.
        axis_y : TYPE
            DESCRIPTION.
        axis_z : TYPE
            DESCRIPTION.

        """
        x_pos, y_pos, z_pos = self.get_position()
        axis_x, axis_y, axis_z = self.get_axes()
        return x_pos, y_pos, z_pos, axis_x, axis_y, axis_z

    def reset_coordinate_system(self):
        # self.__add_a_coordinate_system (PTVR.Stimuli.World.EmptyVisualObject())
        self.coordinate_system.reset()

    def SetIdScene(self, count):
        self.countValue = count

    def add_scene(self, individual_scene=PTVR.Stimuli.Scenes.Scene()):
        """ Add a particular Scene object """
        self.id_scene.append(individual_scene.id)

        if individual_scene.originIsSet is False:
            individual_scene.origin = self.origin
        self.scenes[individual_scene.id] = individual_scene
        if (self.leftBlind == True):
            self.scenes[individual_scene.id].leftBackgroundColor = self.leftBackgroundColor
        if (self.rightBlind == True):
            self.scenes[individual_scene.id].rightBackgroundColor = self.rightBackgroundColor

    def write_calculators(self, fp):
        fp.write('"calculatorManager" : \n')
        self.calculatorManager.write(fp)
        fp.write(',\n')

    def write_metadata(self, fp):
        """
        Writes the headers of the JSON file
        """
        fp.write('"name" : "%s", "created" : "%s", \n' %
                 (get_ptvr_app_name(), self.created))
        fp.write('"user" : "%s",\n' % self.user)
        fp.write('"details3DWorld" : "%s",\n' % self.details3DWorld)
        fp.write('"outputHead" : "%s",\n' % self.outputHead)
        fp.write('"headSaveSamplingPeriod" : %d,\n' %
                 self.headSaveSamplingPeriod)
        fp.write('"handControllerSaveSamplingPeriod" : %d,\n' %
                 self.handControllerSaveSamplingPeriod)
        fp.write('"hand_controller_events_are_recorded" : %d,\n' %
                 self.hand_controller_events_are_recorded)

        fp.write('"outputGaze" : "%s",\n' % self.outputGaze)
        fp.write('"outputLaser" : "%s",\n' % self.outputLaser)

        # according to jeremy, results_path should be an empty string
        # if researchers don't provide a path.
        # i.e. we are not going to write there the default path going into the results folder
        fp.write('"pathResult" : "%s",\n' % self.provided_results_path)

        fp.write('"id_scene" : %s,\n' % self.id_scene)
        fp.write('"leftBlind" : "%s",\n' % self.leftBlind)
        fp.write('"rightBlind" : "%s",\n' % self.rightBlind)
        # fp.write('"translation_vector" : "%s",\n' % self.translation_vector)

        for k, v in self.custom_data.items():
            fp.write('"'+k+'" : '+v+',\n')

    def write_scenes(self, fp):
        """
        write every scene in the JSON file
        """
        fp.write('"scenes" : { \n')
        isFirstLine = True
        for sceneid in self.scenes:
            if not isFirstLine:
                fp.write(", \n")
                fp.write("    ")

            fp.write(str(sceneid)+" : ")
            self.scenes[sceneid].write(fp)
            isFirstLine = False
        fp.write("}\n")

    def _create_folder_description_files(self, path_to_json_directory):
        root_directory = Path(PTVR.SystemUtils.UnityPTVRRoot) / "PTVR_Researchers" / \
            "PTVR_Operators" / "JSON_Files" / get_ptvr_app_category()
        root_directory = root_directory.resolve()

        path_to_json_directory = path_to_json_directory.resolve()
        folder_hierarchy = path_to_json_directory.relative_to(root_directory)

        for component in folder_hierarchy.parts:
            file_name = f"{component}.txt"
            file_path = root_directory / file_name

            if not file_path.exists():
                with open(file_path, 'w') as file:
                    file.write("Default Value")

            # Update the root directory to the current component
            root_directory = root_directory / component

    def write(self):
        """
        The method to write the full JSON once everything is ready
        """
        path_to_json_directory = PTVR.SystemUtils.get_path_to_app_json_directory()
        path_to_json_directory.mkdir(parents=True, exist_ok=True)
        path_to_json_file = path_to_json_directory / \
            (PTVR.SystemUtils.get_ptvr_app_name() + ".json")

        # SV: I want to get rid of of the current "menu" to load jsons
        # from the unity editor. A right menu should be the classical
        # file > open json
        # where you can navigate to the file using the file explorer window.
        # That's a standard way of selecting a file.
        # Not the current way of scrolling a circular menu.
        # Plus, the current menu ENFORCES you to have a silly txt file
        # with the same name as the folder. WHY ADDING THESE CONSTRAINTS!!
        self._create_folder_description_files(path_to_json_directory)

        print(f"Writing JSON file into {path_to_json_file}")
        print(f"Results are going to be placed into {self.results_path}")
        with open(path_to_json_file, "w") as file:
            file.write("{")
            self.write_metadata(file)
            self.write_calculators(file)
            self.write_scenes(file)
            file.write("}")


def the_3D_world_from_file(filename):
    """
    reload an the_3D_world from a file
    """
    e = The3DWorld("")
    try:
        with open(filename, "r") as fp:
            json_data = json.load(fp)
            logging.info("Read the_3D_world data ")
            # now fill in the fields for e
            name = json_data.get('name', None)
            if name is not None:
                e.name = name
            else:
                raise InvalidExperimentFile(
                    "Did not find field name in experiment file")
            created = json_data.get('created', None)
            if created is not None:
                e.created = datetime.datetime.strptime(
                    created, "%Y-%m-%d %H:%M:%S.%f")
            else:
                raise InvalidExperimentFile(
                    "Did not find field created in experiment")
            ofile = json_data.get('output_file', None)
            if ofile is not None:
                e.output_file = ofile
            else:
                raise InvalidExperimentFile(
                    "Did not find field output_file in experiment")
            scenes = json_data.get('scenes', None)
            if scenes is not None:
                pass
            else:
                raise InvalidExperimentFile(
                    "Did not find scenes in experiment")
        return e
    except Exception as e:
        logging.error("Error opening file %s" % filename)
        logging.error(e)
        return None
