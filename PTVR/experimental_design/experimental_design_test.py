import pytest

from PTVR.experimental_design.experimental_design import FactorialDesign, PresentationMode

from PTVR.experimental_design.independent_variables.discrete_variable import DiscreteBalancedVariable, DiscreteRandomVariable

from PTVR.experimental_design.independent_variables.continuous_variable import ContinuousVariable, RandomPointInsideSphereRandomVariable

def test_factorial_design_instantiation():

    color_variable = DiscreteBalancedVariable(
            name="color",
            values=["red", "green"])

    shape_variable = DiscreteBalancedVariable(
            name="shape",
            values=["circle", "triangle", "square"],
            presentation_mode=PresentationMode.RANDOMIZED)

    coin_flip_random_variable = DiscreteRandomVariable(
            name="coin flip",
            values=["head", "tail"])

    random_3d_position_random_variable = RandomPointInsideSphereRandomVariable(
            name="random point inside sphere")

    experimental_procedure = FactorialDesign(
        independent_variables=[
            color_variable,
            shape_variable,
            coin_flip_random_variable,
            random_3d_position_random_variable],
        nb_repetitions_per_condition=2)

def test_factorial_design_number_of_trials():

    color_variable = DiscreteBalancedVariable(
            name="color",
            values=["red", "green"])

    shape_variable = DiscreteBalancedVariable(
            name="shape",
            values=["circle", "triangle", "square"],
            presentation_mode=PresentationMode.RANDOMIZED)

    coin_flip_random_variable = DiscreteRandomVariable(
            name="coin flip",
            values=["head", "tail"])

    random_3d_position_random_variable = RandomPointInsideSphereRandomVariable(
            name="random point inside sphere")

    experimental_procedure = FactorialDesign(
        independent_variables=[
            color_variable,
            shape_variable,
            coin_flip_random_variable,
            random_3d_position_random_variable],
        nb_repetitions_per_condition=2)

    # check total nr of trials
    # random variables should not create new trials
    # nr trials 2 colors * 3 shapes * 2 repetitions
    assert experimental_procedure._nr_trials == (2 * 3 * 2)

def test_balanced_variables():

    color_variable = DiscreteBalancedVariable(
            name="color",
            values=["red", "green"])

    shape_variable = DiscreteBalancedVariable(
            name="shape",
            values=["circle", "triangle", "square"],
            presentation_mode=PresentationMode.RANDOMIZED)

    coin_flip_random_variable = DiscreteRandomVariable(
            name="coin flip",
            values=["head", "tail"])

    random_3d_position_random_variable = RandomPointInsideSphereRandomVariable(
            name="random point inside sphere")

    experimental_procedure = FactorialDesign(
        independent_variables=[
            color_variable,
            shape_variable,
            coin_flip_random_variable,
            random_3d_position_random_variable],
        nb_repetitions_per_condition=2)

    # check if balanced variables are actually balanced
    color_frequencies = {"red":0, "green": 0}
    shape_frequencies = {"circle":0, "triangle": 0, "square":0}

    trial_table, headers = experimental_procedure._get_example_trial_table()
    color_column_idx = headers.index("color")
    shape_column_idx = headers.index("shape")
    for row in trial_table:
        color = row[color_column_idx]
        shape = row[shape_column_idx]
        color_frequencies[color] += 1
        shape_frequencies[shape] += 1

    assert all(freq == list(color_frequencies.values())[0] for freq in color_frequencies.values())
    assert all(freq == list(shape_frequencies.values())[0] for freq in shape_frequencies.values())

    # check if the combination of balanced variables is also balanced
    frequency_table = [0] * len(color_variable._values) * len(shape_variable._values)
    color_to_row = {"red" : 0, "green" : 1}
    shape_to_col = {"circle" : 0, "triangle" : 1, "square":2}
    for row in trial_table:
        color = row[color_column_idx]
        shape = row[shape_column_idx]
        frequency_table_row = color_to_row[color]
        frequency_table_col = shape_to_col[shape]
        frequency_table[frequency_table_row*3+frequency_table_col] += 1
    assert all(freq == frequency_table[0] for freq in frequency_table)


def test_invalid_independent_variables():

    # missing independent variables
    with pytest.raises(ValueError):
        experimental_design = FactorialDesign(
            independent_variables=[])

    # invalid continuous independent variable in a factorial design
    with pytest.raises(ValueError):
        light_intensity = ContinuousVariable(
                name="light intensity",
                min_value = 1.0,
                max_value = 10.0)
        experimental_design = FactorialDesign(
            independent_variables=[light_intensity])

    # factorial design needs to have at least one discrete non-random independent variable
    with pytest.raises(ValueError):
        coin  = DiscreteRandomVariable(
                name="coin flip",
                values=["head", "tail"])
        experimental_design = FactorialDesign(
            independent_variables=[coin])

def test_invalid_block_variables():

    # design with no valid block variable
    # and trying to group by block using an independent variable
    with pytest.raises(ValueError):
        color_variable = DiscreteBalancedVariable(
                name="color",
                values=["red", "green"],
                presentation_mode=PresentationMode.RANDOMIZED) # variable is not presented as block
        experimental_design = FactorialDesign(
            independent_variables=[color_variable])
        experimental_design.group_trials_by_variable(color_variable)




