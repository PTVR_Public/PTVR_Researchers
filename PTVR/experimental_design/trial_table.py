# definition of constants
# used in the trial table

TRIAL_ID_COLUMN_NAME = "trial"
BLOCK_ID_COLUMN_NAME = "block"
TRIAL_IN_BLOCK_ID_COLUMN_NAME = "trial in block"
REPETITION_ID_COLUMN_NAME = "rep"
