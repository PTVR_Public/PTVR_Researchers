from ast import Import
import pytest

# discrete variables
from PTVR.experimental_design.independent_variables.discrete_variable import \
    DiscreteBalancedVariable, DiscreteRandomVariable

# continuous variables
from PTVR.experimental_design.independent_variables.continuous_variable import \
    ContinuousVariable,\
    RandomPointInsideCircleRandomVariable,\
    RandomPointInsideSphereRandomVariable, RandomPointOnSphereRandomVariable,\
    RandomColorRandomVariable,\
    RandomRotationRandomVariable,\
    RandomUniformVariable,\
    RandomNormalVariable,\
    RandomExponentialVariable,\
    RandomPoissonVariable,\
    RandomBinomialVariable,\
    RandomGammaVariable

from PTVR.core.ptvr_data_types import Color

def test_independent_variables_instantiation():

    color = DiscreteBalancedVariable(
            name="color",
            values=[Color.RED, Color.GREEN])

    coin  = DiscreteRandomVariable(
            name="coin flip",
            values=["head", "tail"]) # uniform distribution

    unfair_coin  = DiscreteRandomVariable(
            name="unfair coin flip",
            values=["head", "tail"],
            weights=[0.6]) # 0.4 it's calculated automatically to make sure prob adds to 1.0

    light_intensity = ContinuousVariable(
            name="light intensity",
            min_value = 1.0,
            max_value = 10.0)

    random_color = RandomColorRandomVariable(
            name="random color")

    random_position_inside_circle = RandomPointInsideCircleRandomVariable(
            name="random position inside circle")

    random_position_inside_sphere = RandomPointInsideSphereRandomVariable(
            name="random position inside sphere")

    random_position_on_sphere = RandomPointOnSphereRandomVariable(
            name="random position on sphere")

    random_pose = RandomRotationRandomVariable(
            name="random pose")

    random_uniform_value = RandomUniformVariable(
            name="random uniform value", min_inclusive=-1, max_inclusive=1)

    random_normal_value = RandomNormalVariable(
            name="random normal value", mean=0, standard_deviation=2)

    random_exponential_value = RandomExponentialVariable(
            name="random exponential value", scale=1.0)

    random_poisson_value = RandomPoissonVariable(
            name="random poisson value", lambda_=1.0)

    random_binomial_value = RandomBinomialVariable(
            name="random binomail value", n=10, p=0.5)

    random_gamma_value = RandomGammaVariable(
            name="random gamma value", shape=2.0, scale=2.0)

    assert color is not None

    assert coin is not None
    assert unfair_coin is not None

    assert light_intensity is not None

    assert random_color is not None

    assert random_position_inside_circle is not None
    assert random_position_inside_sphere is not None
    assert random_position_on_sphere is not None
    assert random_pose is not None

    assert random_uniform_value is not None
    assert random_normal_value is not None
    assert random_exponential_value is not None
    assert random_poisson_value is not None
    assert random_binomial_value is not None
    assert random_gamma_value is not None

def test_invalid_probability_weights():

    # negative probability
    with pytest.raises(ValueError):
        coin  = DiscreteRandomVariable(
                name="coin flip",
                values=["head", "tail"],
                weights=[-1.0])

    # probability higher than for a single value
    with pytest.raises(ValueError):
        coin  = DiscreteRandomVariable(
                name="coin flip",
                values=["head", "tail"],
                weights=[1.2])

    # missing probabilities
    with pytest.raises(ValueError):
        dice  = DiscreteRandomVariable(
                name="dice",
                values=[1, 2, 3, 4, 5, 6],
                weights=[0.1, 0.2, 0.1, 0.2])

    # too many probabilities
    with pytest.raises(ValueError):
        coin  = DiscreteRandomVariable(
                name="coin flip",
                values=["head", "tail"],
                weights=[0.2, 0.1, 0.7])

    # right amount of weights but total probability is greater than 1
    with pytest.raises(ValueError):
        coin  = DiscreteRandomVariable(
                name="coin flip",
                values=["head", "tail"],
                weights=[0.5, 0.51])

def test_calculation_of_prob():
    coin  = DiscreteRandomVariable(
            name="coin flip",
            values=["head", "tail"],
            weights=[0.2])
    assert coin._weights[-1] == pytest.approx(0.8)

