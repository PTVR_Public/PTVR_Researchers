########################################################################
# standard library imports
########################################################################
from abc import ABC, abstractmethod # for abstrat base classes

class IRandomVariable(ABC):
    """
    Common interface for all Random Variables.
    """

    @abstractmethod
    def get_random_value(self):
        pass

class IndependentVariable(ABC):
    """
    Base class of all Independent Variables.

    Args:
        name (str): an unique identifier for the independent variable that can be used to retrieve one of its values.
    """
    def __init__(self, name : str):
        self._name = name
        
    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = {}
        result["name"] = self._name
        result["independentVariableType"] = type(self).__name__
        return result
