import random
from enum import Enum, auto
from abc import ABC

from PTVR.experimental_design.independent_variable import IndependentVariable, IRandomVariable

class SequenceOrder(Enum):
    """
    Ordering strategy

    IN_ORDER:
    The different levels of the block variable are grouped together and presented in order.

    RANDOM:
    The different levels of the block variable are grouped together but presented in random order.

    AT_RUNTIME:
    The different levels of the block variable are grouped together but the order is decided at runtime.
    """
    GIVEN_ORDER = auto()
    RANDOM_ORDER = auto()
    AT_RUNTIME_ORDER = auto()

class PresentationMode(ABC):

    IN_SEQUENCE_AT_GIVEN_ORDER = None
    IN_SEQUENCE_AT_RANDOM_ORDER = None
    IN_SEQUENCE_AT_RUNTIME_ORDER = None
    RANDOMIZED = None

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = {}
        result["presentationMode"] = type(self).__name__
        return result

class InSequencePresentationMode(PresentationMode):

    def __init__(self, sequence_order : SequenceOrder):
        self._sequence_order = sequence_order

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["sequenceOrder"] = self._sequence_order.name
        return result

class RandomizedPresentationMode(PresentationMode):
    pass

PresentationMode.IN_SEQUENCE_AT_GIVEN_ORDER = InSequencePresentationMode(SequenceOrder.GIVEN_ORDER)
PresentationMode.IN_SEQUENCE_AT_RANDOM_ORDER = InSequencePresentationMode(SequenceOrder.RANDOM_ORDER)
PresentationMode.IN_SEQUENCE_AT_RUNTIME_ORDER = InSequencePresentationMode(SequenceOrder.AT_RUNTIME_ORDER)
PresentationMode.RANDOMIZED = RandomizedPresentationMode()

# potential name for variables whose list of values is a finite set
# and the actual values are known in advance:
# - nominal/qualitative/categorial
# -> my problem with these names is that they suggest no inherent order between values.
# and that is not the case of discrete variables
# - discrete or ordinal variable
# -> my problem with these names is that it is only for numerical values with inherent order
# between values.
# According to chatGPT, 'categorical variables' is a good name candidate
# for all ordinal, discrete, qualitative, nominal variables.
# This class is equivalent to a Factor in R
# https://www.guru99.com/r-factor-categorical-continuous.html
class _DiscreteVariable(IndependentVariable):
    """
    Base class of Independent Variables whose list of values
    is discrete (a finite set of values).

    Args:
        name (str): an unique identifier for the independent variable that can be used to retrieve one of its values.

        values (list): the list of values/levels the variable takes.
    """
    def __init__(self, name : str, values : list):
        super().__init__(name)
        self._values = values
        
    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["values"] = self._values
        return result

class DiscreteBalancedVariable(_DiscreteVariable):
    """
    Independent Variables whose list of values is discrete (a finite set of values)
    and all the values are presented the same number of times along the experiment.

    Args:
        name (str): an unique identifier for the independent variable that can be used to retrieve one of its values.

        values (list): the list of values/levels the variable takes.

        presentation_mode (PresentationMode): mechanism to determing the order of presentation of the different levels.
    """
    def __init__(self,
                 name : str,
                 values : list,
                 presentation_mode : PresentationMode = PresentationMode.IN_SEQUENCE_AT_GIVEN_ORDER):
        super().__init__(name, values)
        self._presentation_mode = presentation_mode

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["presentationMode"] = self._presentation_mode.to_json()
        return result

class DiscreteRandomVariable(_DiscreteVariable, IRandomVariable):
    """
    Independent Variables whose list of values is discrete (a finite set of values)
    and the selection of each values in a trial is random.

    Args:
        name (str): an unique identifier for the independent variable that can be used to retrieve one of its values.

        values (list): the list of values/levels the variable takes.

        weights (list): list of weights for each value. The length of the list must be one less than the length of the list of values
        as the last one is calculated automatically to ensure that the sum of all weights is 1. If weights is None, then all values are
        equally likely to be selected (uniform distrubution).
    """
    def __init__(self, name : str, values : list, weights : list = None):
        super().__init__(name, values)
        # check weights
        self._weights = []
        if weights is not None:
            if len(weights) > len(values):
                raise ValueError("Too many weights")

            if len(weights) < len(values)-1:
                raise ValueError("Missing weights")

            accum = 0.0
            for weight in weights:
                self._weights.append(weight)
                accum += weight
                if not 0.0 <= weight <= 1.0:
                    raise ValueError("Wrong weight value {}. It must be a float between [0, 1]".format(weight))

            # in the case all weights were explicit, they should up to 1
            if len(weights) == len(values) and accum > 1:
                raise ValueError("The sum of all weights is greater than 1.0")

            # completely the probability of the last one if missing
            if len(weights) < len(values):
                last_weight = 1.0 - accum
                self._weights.append(last_weight)
                if (last_weight < 0.0):
                    raise ValueError("The sum of all weights is greater than 1.0")
        else:
            self._weights = [1.0 / len(values)] * len(values)

    def get_random_value(self):
        return random.choices(self._values, self._weights)[0]

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["weights"] = self._weights
        return result
