import random
import numpy as np

from PTVR.experimental_design.independent_variable import IndependentVariable, IRandomVariable
from PTVR.core.ptvr_data_types import Vector3, Color

class ContinuousVariable(IndependentVariable):
    """
    Base class of Independent Variables whose list of values
    is continuous (an infinite set of values).

    Args:
        name (str): an unique identifier for the independent variable that can be used to retrieve one of its values.
        min_value (float): the minimum value the variable can take.
        max_value (float): the maximum value the variable can take.
    """
    def __init__(self, name : str, min_value : float, max_value : float):
        super().__init__(name)
        self._min_value = min_value
        self._max_value = max_value

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["minValue"] = self._min_value
        result["maxValue"] = self._max_value
        return result

class _ContinuousRandomVariable(IndependentVariable, IRandomVariable):

    def __init__(self, name: str):
        super().__init__(name)

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        return super().to_json()

class _RandomContinuousScalarVariable(_ContinuousRandomVariable):

    def __init__(self, name: str):
        super().__init__(name)
    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["distribution"] = type(self).__name__
        return result

class RandomUniformVariable(_RandomContinuousScalarVariable):

    def __init__(self, name: str, min_inclusive:float = 0.0, max_inclusive:float = 1.0):
        super().__init__(name)
        self._min_inclusive = min_inclusive
        self._max_inclusive = max_inclusive

    def get_random_value(self):
        return random.uniform(self._min_inclusive, self._max_inclusive)

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["minInclusive"] = self._min_inclusive
        result["maxInclusive"] = self._max_inclusive
        return result

class RandomNormalVariable(_RandomContinuousScalarVariable):
    def __init__(self, name: str, mean:float=1.0, standard_deviation:float = 1.0):
        super().__init__(name)
        self._mean = mean
        self._standard_deviation = standard_deviation

    def get_random_value(self):
        return np.random.normal(self._mean, self._standard_deviation, 1)

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["mean"] = self._mean
        result["standardDeviation"] = self._standard_deviation
        return result

class RandomExponentialVariable(_RandomContinuousScalarVariable):
    def __init__(self, name: str, scale:float=1.0):
        super().__init__(name)
        self._scale = scale

    def get_random_value(self):
        return np.random.exponential(self._scale, 1)

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["scale"] = self._scale
        return result

class RandomPoissonVariable(_RandomContinuousScalarVariable):
    def __init__(self, name: str, lambda_:float=2.0):
        super().__init__(name)
        self._lambda = lambda_

    def get_random_value(self):
        return np.random.poisson(self._lambda, 1)

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["lambda"] = self._lambda
        return result

class RandomBinomialVariable(_RandomContinuousScalarVariable):
    def __init__(self, name: str, n:int=10, p:float = 0.5):
        super().__init__(name)
        self._n = n
        self._p = p

    def get_random_value(self):
        return np.random.binomial(self._n, self._p, 1)

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["n"] = self._n
        result["p"] = self._p
        return result

class RandomGammaVariable(_RandomContinuousScalarVariable):
    def __init__(self, name: str, shape, scale:float = 1.0):
        super().__init__(name)
        self._shape = shape
        self._scale = scale

    def get_random_value(self):
        return np.random.gamma(self._shape, self._scale, 1)

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["shape"] = self._shape
        result["scale"] = self._scale
        return result


class RandomPointInsideCircleRandomVariable(_ContinuousRandomVariable):
    """
    Random Point inside a circle laying in the XY plane placed at 'origin' with radius 'radius'.
    """ 

    def __init__(self, name:str, origin:Vector3 = Vector3.ZERO, radius:float = 1):
        super().__init__(name)
        self._radius = radius
        self._origin = origin

    def get_random_value(self):
        return self._origin + Vector3.get_random_inside_xy_circle(self._radius)

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["origin"] = self._origin
        result["radius"] = self._radius
        return result

class RandomPointInsideSphereRandomVariable(_ContinuousRandomVariable):
    """
    Random Point inside a sphere placed at 'origin' and with radius 'radius'.
    """ 

    def __init__(self, name: str, origin:Vector3 = Vector3.ZERO, radius:float = 1):
        super().__init__(name)
        self._radius = radius
        self._origin = origin

    def get_random_value(self):
        return self._origin + Vector3.get_random_inside_sphere(self._radius)

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["origin"] = self._origin
        result["radius"] = self._radius
        return result

class RandomPointOnSphereRandomVariable(_ContinuousRandomVariable):
    """
    Random Point on a sphere placed at 'origin' and with radius 'radius'.
    """ 

    def __init__(self, name: str, origin:Vector3 = Vector3.ZERO, radius:float = 1):
        super().__init__(name)
        self._radius = radius
        self._origin = origin

    def get_random_value(self):
        return self._origin + Vector3.get_random_on_sphere(self._radius)

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["origin"] = self._origin
        result["radius"] = self._radius
        return result


class RandomColorRandomVariable(_ContinuousRandomVariable):
    """
    Random color
    """ 

    def __init__(self, name: str):
        super().__init__(name)

    def get_random_value(self):
        return Color.get_random()

class RandomRotationRandomVariable(_ContinuousRandomVariable):
    """
    Random rotation
    """ 

    def __init__(self, name: str):
        super().__init__(name)

    def get_random_value(self):
        return "implemented in Unity side"
