# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 14:08:29 2024

@author: chopi
"""

"""
The Staircase classe. 
"""
import json

class Staircase():
    """

    """
    def __init__(self,
                 n_reps = 10,
                 n_up = 1, 
                 n_down = 1,
                 n_reversals = 5,
                 n_actual_up =0,
                 n_actual_down = 0,
                 n_actual_reversals = 0,
                 n_actual_rep = 0,
                 staircase_id = 1,
                 initial_vi_val = 1,
                 current_vi_val = 1,
                 step_up = +0.1,
                 step_down = -0.1,
                 staircase_finished=False):

        
        self.n_reps = n_reps
        self.n_up = n_up
        self.n_down = n_down
        self.n_reversals = n_reversals
        
        self.n_actual_up = n_actual_up
        self.n_actual_down = 0
        self.n_actual_reversals = 0
        self.n_actual_rep = 0
        
        self.staircase_id = staircase_id
        self.initial_vi_val = initial_vi_val
        self.current_vi_val = initial_vi_val
        self.step_up = step_up
        self.step_down = step_down
        self.staircase_finished = False
        
    
    

def from_dict(cls, dict):
    return cls(n_reps=dict["n_reps"],
               n_up =dict["n_up"],
               n_down=dict["n_down"],
               n_reversals=dict["n_reversals"],
               n_actual_up=dict["n_actual_up"],
               n_actual_down=dict["n_actual_down"],
               n_actual_reversals=dict["n_actual_reversals"],
               n_actual_rep=dict["n_actual_rep"],
               staircase_id=dict["staircase_id"],
               initial_vi_val=dict["initial_vi_val"],
               current_vi_val=dict["current_vi_val"],
               step_up=dict["step_up"],
               step_down=dict["step_down"],
               staircase_finished=dict["staircase_finished"]
               )


class StaircaseHandler():
    """

    """
    def __init__(self,
                 staircases = []):

        
        self.staircases = []
        
    def get_staircases(self, path_to_pickle):
        
        json_file = open(path_to_pickle + "\\staircases.json", "r") 
        job = json.load(json_file)
        for j in job :
            self.staircases.append(from_dict(Staircase, j))
            
        json_file.close()
        
    def update_staircases(self, staircase_id, direction="up"):
        
        for i in range(len(self.staircases)):
            if (self.staircases.id == staircase_id):
                
                if (direction == "up"):
                        self.staircases.current_vi_val += self.staircases.step_up
                elif (direction == "down"):
                        self.staircases.current_vi_val += self.staircases.step_down
        
                self.staircases.n_actual_rep += 1


