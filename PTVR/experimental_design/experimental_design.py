# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 18:33:02 2023

@author: svizcay
"""

########################################################################
# standard library imports
########################################################################
from pathlib import Path
import json
import random
import csv
from math import ceil
import uuid

# prod function was not introduced until python 3.8
try:
    from math import prod
except ImportError:
    def prod(iterable, *, start=1):
        result = start
        for val in iterable:
            result *= val
        return result

from abc import ABC # for abstrat base classes
from typing import List
from enum import Enum, auto
import itertools
########################################################################


########################################################################
# third party library imports
########################################################################
# for printing data in table format
# from prettytable import PrettyTable # requires pip install prettytable
# from tabulate import tabulate # requires pip install tabulate
########################################################################


########################################################################
# local imports
########################################################################
from PTVR.ExtraTools import print_as_table, get_permutation_matrix
from PTVR.SystemUtils import CustomEncoder, get_ptvr_app_name, get_path_to_app_json_directory
from PTVR.core.ptvr_event import PTVREvent
from PTVR.experimental_design.independent_variable import IndependentVariable
from PTVR.experimental_design.independent_variables.discrete_variable import PresentationMode, InSequencePresentationMode, RandomizedPresentationMode
from PTVR.experimental_design.independent_variables.discrete_variable import _DiscreteVariable, DiscreteBalancedVariable, IRandomVariable
from PTVR.experimental_design.independent_variables.continuous_variable import ContinuousVariable

from PTVR.experimental_design.trial_table import TRIAL_ID_COLUMN_NAME, BLOCK_ID_COLUMN_NAME, TRIAL_IN_BLOCK_ID_COLUMN_NAME, REPETITION_ID_COLUMN_NAME

class ShufflingMode(Enum):
    """
    The strategy to determine the level of randomization of randomized variables

    SHUFFLE_ONCE:
    The different levels of the variables are shuffled once
    and that randomization is kept among different experimental blocks.

    SHUFFLE_EVERY_TIME
    The different levels of the variables are shuffled every time.
    """
    SHUFFLE_ONCE = auto()
    SHUFFLE_EVERY_TIME = auto()

class TrialGroupingMechanism(Enum):
    """
    The mechanism that is used to group trials into blocks

    NOT_GROUPED:
    Trials are not grouped into blocks at all.

    BY_BLOCK_SIZE
    Blocks are specified giving the nr of trials per block

    BY_NR_BLOCKS
    Blocks are specified giving the total nr of blocks along the experiment

    BY_VARIABLE
    Blocks are specified by the levels of an IN_SEQUENCE Independent Variable,
    i.e. we start a new block every time we see a new value of the Independent Variable.
    """
    NOT_GROUPED = auto()
    BY_BLOCK_SIZE = auto()
    BY_NR_BLOCKS = auto()
    BY_VARIABLE = auto()


class ExperimentalProcedure(ABC):
    """
    Base class of all Experimental Procedures

    Args:
        experimental_procedure_type (ExperimentalProcedure.Type): the type of experimental procedure.
    """

    def __init__(self):
        self._trial_grouping_mechanism = TrialGroupingMechanism.NOT_GROUPED
    
    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = {}
        result["experimentalProcedureType"] = type(self).__name__
        result["trialGroupingMechanism"] = self._trial_grouping_mechanism.name
        return result

class StairCase(ExperimentalProcedure):
    def __init__(self,
                 independent_variables : list):
        super().__init__()
        self._independent_variables = independent_variables

    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        result["independentVariables"] = self._independent_variables
        return result
    

class FactorialDesign(ExperimentalProcedure):
    """
    In a Experimental Procedure following a Factorial design,
    the trial table presenting the different combination of conditions (stimuli)
    follows a well-known structure and can be determined in advance.

    Args:
        independent_variables (list of IndependentVariable): the list of independent variables.

        nb_repetitions_per_condition (int) : the number of times the different combination of trials is going to be repeated along the experiment.

        trial_repetition_density (int) : value that control how blocky is the presentation of the same level of a variable in a row.
        a value of 0, indicates they values are looped as much as possible.
        default value (MAX), presents all levels of the variables grouped together.

        shuffling_mode (IndependentVariable.ShufflingMode) : Whether we shuffle randomized variables every time or if we re-use the randomization between blocks.
    """
    MAX_TRIAL_REPETITION_DENSITY = None
    MIN_TRIAL_REPETITION_DENSITY = 0
    
    def __init__(self,
                 independent_variables : List[IndependentVariable],
                 nb_repetitions_per_condition : int = 1,
                 trial_repetition_density : int = None, # None -> MAX_DENSITY (default) repetition column placed extreme right
                 shuffling_mode : ShufflingMode = ShufflingMode.SHUFFLE_EVERY_TIME):
        super().__init__()

        # Check if list of indepent variables are valid.
        # For this type of experimental procedure, all discrete variables are accepted
        # and continuous random variables which have a well defined behaviour to determined
        # a value prior to the experimental run.
        # In other words, Continuous Variables are the only one excluded for now
        # and these are meant to be used in adaptive procedures.

        for iv in independent_variables:
            if type(iv) == ContinuousVariable:
                raise ValueError(f"var {iv._name} can not be used in a procedure following a factorial design.")

        # discrete balanced variables: either presented in sequence or in random order
        self._discrete_balanced_variables = [iv for iv in independent_variables \
            if type(iv) == DiscreteBalancedVariable]

        if len(self._discrete_balanced_variables) == 0:
            raise ValueError("Wrong design. At least one variable needs to be discrete and not a random variable.")

        self._in_sequence_variables = [iv for iv in independent_variables \
            if type(iv) == DiscreteBalancedVariable and type(iv._presentation_mode) == InSequencePresentationMode]

        # Check if trial repetition density is valid
        nr_in_sequence_variables = len(self._in_sequence_variables)

        self._randomized_variables = [iv for iv in independent_variables \
            if type(iv) == DiscreteBalancedVariable and type(iv._presentation_mode) == RandomizedPresentationMode]

        nr_randomized_variables = len(self._randomized_variables)

        max_repetition_density = nr_in_sequence_variables+1 \
            if (nr_randomized_variables > 0) \
            else nr_in_sequence_variables

        self._trial_repetition_density = trial_repetition_density \
            if trial_repetition_density is not None \
            else max_repetition_density

        if not 0 <= self._trial_repetition_density <= max_repetition_density:
            raise ValueError("Trial repetition density for this experiment needs to be between min=0 and max={}".
                             format(max_repetition_density))

        self._independent_variables = independent_variables
        self._nr_trial_repetitions = nb_repetitions_per_condition
        self._shuffling_mode = shuffling_mode


        self._nr_trials = nb_repetitions_per_condition * prod(len(x._values) for x in self._discrete_balanced_variables)

        self._nr_blocks = 0
        self._block_size = 0
        self._block_sizes = []

        # data members to iterate throw the trial table from python
        data, headers = self._get_example_trial_table()
        self._trial_table = data
        self._headers = headers

        self._trial_pointer = 0
        self._block_pointer = 0


    def has_finished(self):
        """
        To be used in an experiment being developed and handled completely
        in Python, i.e. all values are known in advance and trials
        are achieved using VisualScenes.

        This method is used to figure out if there are still trials to execute.
        """
        return self._trial_pointer >= len(self._trial_table)

    def there_are_blocks_to_run(self):
        """
        To be used in an experiment being developed and handled completely
        in Python, i.e. all values are known in advance and trials
        are achieved using VisualScenes.

        This method is used to figure out if there are still blocks to execute.
        """
        return self._block_pointer < self._nr_blocks

    def there_are_trials_in_block_to_run(self):
        """
        To be used in an experiment being developed and handled completely
        in Python, i.e. all values are known in advance and trials
        are achieved using VisualScenes.

        This method is used to figure out if there are trials in the current
        block that still need to be executed.
        """
        return self._trial_pointer_within_block < self._block_sizes[self._block_pointer]

    @property
    def _trial_pointer_within_block(self):
        remaining_trials = self._trial_pointer
        for block_id in range(self._block_pointer-1, 0, -1):
            remaining_trials -= self._block_sizes[block_id]
        return remaining_trials

    def finish_trial(self):
        """
        To be used in an experiment being developed and handled completely
        in Python, i.e. all values are known in advance and trials
        are achieved using VisualScenes.

        This method is used to indicate the experimental procedure that
        it needs to advance to the next trial
        """
        self._trial_pointer += 1

    def group_trials_by_block_size(self, nr_trials_per_block : int):
        """
        It divides the experiment into blocks by specifying the number of trials in a single block.
        The last block will contain all remaining trials in the case the total number of trials
        is not a multiple of nr_trials_per_block

        Args:
            nr_trials_per_block (int): the number of trials within a single block
        """
        if not 1 <= nr_trials_per_block <= self._nr_trials:
            raise ValueError("Wrong block size. Needs to be between 1 and {}".format(self._nr_trials))
        self._trial_grouping_mechanism = TrialGroupingMechanism.BY_BLOCK_SIZE
        self._block_size = nr_trials_per_block

        self._nr_blocks = self._nr_trials // nr_trials_per_block
        remaining_trials = self._nr_trials % nr_trials_per_block
        self._block_sizes = [nr_trials_per_block] * self._nr_blocks
        if remaining_trials > 0:
            self._nr_blocks += 1
            self._block_sizes.append(remaining_trials)

    def group_trials_by_nr_blocks(self, nr_blocks : int):
        """
        It divides the experiment into blocks by specifying the total number of blocks.
        The number of trials per block will be set equal to ceil(nr_trials/nr_blocks),
        i.e. the last block might have less trials assigned to it of the number of trials
        is not divisible by nr_blocks.

        Args:
            nr_blocks (int): the number of blocks the experiment is going to be divided into.
        """
        if not 1 <= nr_blocks <= self._nr_trials:
            raise ValueError("Wrong number of blocks. Needs to be between 1 and {}".format(self._nr_trials))
        self._trial_grouping_mechanism = TrialGroupingMechanism.BY_NR_BLOCKS
        self._nr_blocks = nr_blocks

        self._block_size = ceil(self._nr_trials/nr_blocks)
        last_block_size = self._nr_trials - (nr_blocks-1) * self._block_size
        self._block_sizes = [self._block_size] * (nr_blocks-1) + [last_block_size]

    def group_trials_by_variable(self, in_sequence_independent_variable : DiscreteBalancedVariable):
        """
        It divides the experiment into blocks following the presentation order 
        of an in-sequence independent variable, i.e. whenever the value of that variable
        changes, we will have a new block.

        Args:
            in_sequence_independent_variable (InSequenceIndependentVariable): the independent variable that is going to be used to divide the experiment into blocks.
        """

        if type(in_sequence_independent_variable._presentation_mode) != InSequencePresentationMode:
            raise ValueError(f"var {in_sequence_independent_variable._name} is not a variable presented in sequence.")

        if (in_sequence_independent_variable not in self._in_sequence_variables):
            raise ValueError(f"var {in_sequence_independent_variable._name} is not a variable used in the experiment")

        self._trial_grouping_mechanism = TrialGroupingMechanism.BY_VARIABLE
        self._grouping_variable_index = self._independent_variables.index(in_sequence_independent_variable)

        # whenever i see a new value -> increase the nr of blocks
        # if i start with none as initial value, -> start with nr_blocks = 0
        var_idx_in_table = self._headers.index(in_sequence_independent_variable._name)
        current_value = None#self.trial_table[0][var_idx_in_table]
        nr_blocks = 0
        for trial_pointer, row in enumerate(self._trial_table):
            value = row[var_idx_in_table]
            if value != current_value: # this gets executed at the first trial of every block
                nr_blocks += 1
                current_value = value
        self._nr_blocks = nr_blocks
        self._block_size = ceil(self._nr_trials/nr_blocks)
        last_block_size = self._nr_trials - (nr_blocks-1) * self._block_size
        self._block_sizes = [self._block_size] * (nr_blocks-1) + [last_block_size]

    def export_trial_table_as_csv(self, filename : str, verbose=True):
        """
        Exports the trial table as a csv file.

        Args:
            filename (str): path and filename of the csv where we are going to write the trial table.

            verbose (bool): whether we also display the trial table to the standard output (the console).
        """
        data, headers = self._get_example_trial_table()
        with open(filename, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(headers)
            for row in data:
                writer.writerow(row)

        if (verbose):
            print_as_table(data, headers)

    def _reserve_space_trial_table(self):
        """
        This method will reserve memory just enough
        to hold the permutation of the independent variables.
        trial id, block id and trial in block are added later.
        """
        # get nr columns
        # for now, we are going to add only columns (IVs that creates trials)
        # nr_columns = len(self._independent_variables)
        nr_columns = len(self._in_sequence_variables) + len(self._randomized_variables)
        if self._nr_trial_repetitions > 1:
            nr_columns += 1 # extra column for repetition column
        # if self.trial_grouping_mechanism != ExperimentalProcedure.TrialGroupingMechanism.NOT_GROUPED:
        #     nr_columns += 2 # extra columns: block and trial in block
        
        trial_table = []
        for i in range(self._nr_trials):
            trial_table.append([0] * nr_columns)

        return trial_table

    def _add_trial_info_columns(self, partial_table, partial_headers):
        # order of decorators from left to right:
        # trial id (global id), block id, trial in block
        if len(self._block_sizes) > 0:
            table = []
            block_id = 1
            trial_in_block = 1
            for trial_pointer, conditions in enumerate(partial_table):
                trial_id = trial_pointer + 1
                row = [trial_id, block_id, trial_in_block] + conditions
                table.append(row)
                trial_in_block += 1
                if trial_in_block > self._block_sizes[block_id-1]:
                    trial_in_block = 1
                    block_id += 1
            headers = [TRIAL_ID_COLUMN_NAME, BLOCK_ID_COLUMN_NAME, TRIAL_IN_BLOCK_ID_COLUMN_NAME] + partial_headers
        else:
            headers = [TRIAL_ID_COLUMN_NAME] + partial_headers
            table = [[trial_id+1] + lst for trial_id, lst in enumerate(partial_table)]

        return table, headers

    def _get_example_trial_table(self):

        # shuffle IVs who are supposed to be presented randomly
        for iv in self._randomized_variables:
            random.shuffle(iv._values)

        has_repetitions = self._nr_trial_repetitions > 1
        has_randomization = len(self._randomized_variables) > 0
        nr_in_sequence_variables = len(self._in_sequence_variables)

        insertion_idx = self._trial_repetition_density
        repetition_in_random = insertion_idx > nr_in_sequence_variables

        trial_table = self._reserve_space_trial_table()

        # temporal copies of ivs
        in_sequence_variables = list(self._in_sequence_variables)
        randomized_variables = list(self._randomized_variables)

        if has_repetitions:
            repetitions = [x+1 for x in range(self._nr_trial_repetitions)]
            if repetition_in_random:
                randomized_variables.append(
                    DiscreteBalancedVariable(
                        REPETITION_ID_COLUMN_NAME,
                        repetitions,
                        PresentationMode.RANDOMIZED))
            else:
                in_sequence_variables.insert(
                    insertion_idx,
                    DiscreteBalancedVariable(
                        REPETITION_ID_COLUMN_NAME,
                        repetitions))

        if has_randomization:
            in_sequence_ivs_values = [x._values for x in in_sequence_variables]
            randomized_ivs_values = [x._values for x in randomized_variables]
            random_vars_combinations = get_permutation_matrix(randomized_ivs_values)
            col_offset = len(in_sequence_ivs_values)
            in_sequence_ivs_values.append(random_vars_combinations)
            nr_pattern_repetition = prod(len(x._values) for x in in_sequence_variables)
            # fill the randomized part of the table
            if self._shuffling_mode == ShufflingMode.SHUFFLE_ONCE:
                # single shuffle operations is repeated
                random.shuffle(random_vars_combinations)
                random_entries = random_vars_combinations * nr_pattern_repetition
                for row, entry in enumerate(random_entries):
                    for col, cell in enumerate(entry):
                        trial_table[row][col_offset+col] = cell
            elif self._shuffling_mode == ShufflingMode.SHUFFLE_EVERY_TIME:
                nr_combinations = len(random_vars_combinations)
                for i in range(nr_pattern_repetition):
                    random.shuffle(random_vars_combinations) 
                    for row, entry in enumerate(random_vars_combinations):
                        for col, cell in enumerate(entry):
                            trial_table[(i*nr_combinations)+row][col_offset+col] = cell
            # add the in sequence variables
            combinations = list(itertools.product(*in_sequence_ivs_values))
            for row, entry in enumerate(combinations):
                for col, cell in enumerate(entry):
                    if (col < col_offset):
                        trial_table[row][col] = cell
        else:
            in_sequence_ivs_values = [x._values for x in in_sequence_variables]
            combinations = list(itertools.product(*in_sequence_ivs_values))
            for row, entry in enumerate(combinations):
                for col, cell in enumerate(entry):
                    trial_table[row][col] = cell

        random_variables = [iv for iv in self._independent_variables \
            if isinstance(iv, IRandomVariable)]

        for random_iv in random_variables:
            for row in trial_table:
                row.append(random_iv.get_random_value())

        # generate the headers
        headers = [x._name for x in in_sequence_variables] + \
            [x._name for x in randomized_variables] + \
            [x._name for x in random_variables]

        # add additional columns to the table such as:
        # trial id, block id and trial in block
        trial_table, headers = self._add_trial_info_columns(trial_table, headers)

        return trial_table, headers

        
    def print_trial_table(self):
        """
        It prints to the standard output the trial table of the experimental design.
        """
        data, headers = self._get_example_trial_table()
        print_as_table(data, headers)

    def get_value(self, variable_name:str):
        """
        It retrieves the value of an independent variable named 'variable_name' for the current trial.

        Args:
            variable_name (str): identifier of the independent variable whose value we want to retrieve for the current trial.

        Returns:
            The value of the independent variable specifed by variable_name for the current trial.
        """
        variable_idx = self._headers.index(variable_name)
        return self._trial_table[self._trial_pointer][variable_idx]
        
    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = super().to_json()
        # serialize list of condition variables
        # result["inSequenceIndependentVariables"] = self._in_sequence_variables
        result["independentVariables"] = self._independent_variables
        result["nrTrialRepetitions"] = self._nr_trial_repetitions
        result["trialRepetitionDensity"] = self._trial_repetition_density
        #result["shufflingMode"] = str(self._shuffling_mode)
        result["shufflingMode"] = self._shuffling_mode.name
        # when a grouping mechanism has been especified,
        # all related data members (nr_blocks, block_size, block_sizes) are calculated
        if (self._trial_grouping_mechanism != TrialGroupingMechanism.NOT_GROUPED):
            result["nrBlocks"] = self._nr_blocks
            result["blockSize"] = self._block_size
            result["blockSizes"] = self._block_sizes
        return result

# suggestion: we either call:
# this container class 'Experiment' and we call the other 'Experimental Design' or 'Experimental Procedure'
# or we call this container class 'Experimental Design' and the other class 'Experimental Procedure'
class Experiment:
    """
    Class that holds all the information needed to run a full experiment.
    Custom functionality might be attached to any of the public Events such as:
    on_experiment_start_event, on_block_start_event, on_trial_start_event, etc.

    Args:
        experimental_procedure (ExperimentalProcedure): the ExperimentalProcedure to follow along the experiment.
    """
    
    def __init__(
        self,
        experimental_procedure:ExperimentalProcedure):
        self.uuid = uuid.uuid4()
        self._experimental_procedure = experimental_procedure
        self.on_trial_start_event = PTVREvent()
        self.on_trial_end_event = PTVREvent()
        self.on_block_start_event = PTVREvent()
        self.on_block_end_event = PTVREvent()
        self.on_experiment_start_event = PTVREvent()
        self.on_experiment_end_event = PTVREvent()
        
    def to_json(self):
        """
        It returns a dictionary of the data that needs to be serialized.

        Returns:
            The dictionary of the serialized data.
        """
        result = {}
        result["uuid"] = str(self.uuid)

        result["onTrialStartEventUUID"] = str(self.on_trial_start_event.uuid)
        result["onTrialEndEventUUID"] = str(self.on_trial_end_event.uuid)
        result["onBlockStartEventUUID"] = str(self.on_block_start_event.uuid)
        result["onBlockEndEventUUID"] = str(self.on_block_end_event.uuid)
        result["onExperimentStartEventUUID"] = str(self.on_experiment_start_event.uuid)
        result["onExperimentEndEventUUID"] = str(self.on_experiment_end_event.uuid)

        result["experimentalProcedure"] = self._experimental_procedure.to_json()

        return result
        
    def write(self):
        """
        Outputs the serialized data into a JSON file.
        """
        path_to_json_directory = get_path_to_app_json_directory()
        # create the directory if it doesn't exist
        path_to_json_directory.mkdir(exist_ok=True)
        
        path_to_json_file = path_to_json_directory / (get_ptvr_app_name() + "_XP_Design.json")
            
        # get path where to write <ptvr_app>_XP_Design.json file
        #path_to_json = json_directory + PTVR.SystemUtils.get_ptvr_app_name() + "_XP_Design.json"

        with open (path_to_json_file, "w") as file:
            json.dump(self, file, indent=4, cls=CustomEncoder)

