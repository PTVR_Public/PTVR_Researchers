# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\PTVR\Blocks.py

The Blocks.py file contains "convenience functions".

    A "convenience function" is a non-essential subroutine in a programming
    library or framework which is intended to EASE commonly performed tasks
    (https://en.wikipedia.org/wiki/Convenience_function).


Created on Wed Nov  6 09:50:37 2024
@author: chopi
"""

from PTVR.Stimuli.Objects import Sprite
import PTVR.Pointing.ImageFromDrawing as IFD
import PTVR.Stimuli.Color as color
import numpy as np
import PTVR.Data.Event
import PTVR.Data.Callbacks.CounterCallback
import PTVR.Data.Callbacks.RandomCallback
import PTVR.Data.Callbacks.MovementCallback
import PTVR.Data.Events.CounterEvent


def DrawCircle(my_scene, my_world, image_file="Circle.png", circle_diameter=np.array([0, 0, 0]),
               circle_color=color.RGBColor(r=0, g=0, b=0, a=1),
               position_in_current_CS=np.array([0, 0, 0]),
               rotation_in_current_CS=np.array([0, 0, 0]),
               line_width_as_ratio=0.02,
               image_resolution_pix=2048):

    my_image = IFD.ImageFromDrawing(
        image_resolution_pix=image_resolution_pix,
        line_width_as_ratio=line_width_as_ratio,
        shape="ellipse",
        image_file=image_file,
        color=circle_color)

    circle = Sprite(
        image_file=image_file,
        path_to_image_file=my_image.image_path,  # !!

        size_in_meters=circle_diameter,
        position_in_current_CS=position_in_current_CS,
        rotation_in_current_CS=rotation_in_current_CS)

    my_scene.place(circle, my_world)

    return circle


point_and_click_event_number = 0


def PointAndClickEvent(
    the_object_id, my_scene, my_world,
    the_activation_cone_origin_id,
    the_activation_cone_radius_deg=0.1,
):
    """
       This function is a convenience function returning an event
    (called object_trigger_counter_has_reached_its_goal) that is triggered
    when an object (see the 'the_object_id' argument ) has been pointed at AND
    "clicked".
    When the pointer is the handcontroller with a laser beam, "clicked" means
    that the handcontroller's trigger has been pressed.
    Returning this event is the main objective of the present function.
       A second event is returned (called object_is_pointed_at). This event
    can be used to create interactions when the object is pointed at.
       A third event is returned (called object_is_not_pointed_at). This event
    can be used to create interactions when the object is not pointed at.

    Parameters
    ----------
    the_object_id :
        Identification number of the object that can be pointed at and clicked.
    my_scene :
    my_world :
    the_activation_cone_origin :
        This defines whether the pointer is the handcontroller, or the headset,
    or gaze, etc...

    -1 -> my_world.handControllerLeft.id

    -2 -> my_world.handControllerRight.id

    -3 -> my_world.headset.id (default)

    -6 -> my_world.eyeLeft.id

    -7 -> my_world.eyeRight.id

    -8 -> my_world.eyeCombined.id

    the_activation_cone_radius_deg : Radius in degrees of visual angle
    of the activation cone. The default is 0.1.

    Returns
    -------
    Three events:
    object_trigger_counter_has_reached_its_goal :
    object_is_pointed_at :
    object_is_not_pointed_at :

    """

    global point_and_click_event_number

    object_is_pointed_at = PTVR.Data.Event.PointedAt(
        target_id=the_object_id,
        activation_cone_origin_id=the_activation_cone_origin_id,
        activation_cone_radius_deg=the_activation_cone_radius_deg,
        event_name="pointed_at_in_point_and_click"
    )
    object_is_not_pointed_at = PTVR.Data.Event.PointedAt(
        target_id=the_object_id,
        activation_cone_origin_id=the_activation_cone_origin_id,
        activation_cone_radius_deg=the_activation_cone_radius_deg,
        mode="release"
    )

    trigger_button = PTVR.Data.Event.HandController(
        valid_responses=['right_trigger'],
        mode="press", event_name="trigger_in_point_and_click"
    )

    #   The following event initially creates a COUNTER that contains a continuous
    # value in a GV (Global Variable) called "PointAndClickCounterxxx"
    # (xxx referring to the object identified by the 'the_object_id' argument).
    # Note: The 'the_object_id' argument allows you to use ONE COUNTER for
    # each of several objects.
    #   This event is triggered (and returned by the present function)
    # when the continuous value contained in the GV is greater than
    # (or equal to) the value indicated by the 'value_to_be_reached' parameter.
    # ("triggered" here must not be confused with the handcontroller's trigger)
    #   In the present function, the role of this COUNTER is to count how
    # many times a given object has been pointed at AND clicked.
    object_trigger_counter_has_reached_its_goal = \
        PTVR.Data.Events.CounterEvent.TriggerWhenValueIsReached(
            value_to_be_reached=1,
            label_of_updated_GV_to_use="PointAndClickCounter" +
            str(the_object_id) + "_%d" % point_and_click_event_number,
            global_variables_to_reset=np.array(
                ["PointAndClickCounter" +
                 str(the_object_id) + "_%d" % point_and_click_event_number]))

    ################################## Callbacks #####################################

    #   The following callback initially creates a boolean COUNTER that contains
    # a boolean value in a GV (Global Variable) called "PointedAtxxx"
    # (xxx referring to the object identified by the 'the_object_id' argument).
    #   Thanks to an interaction defined below,
    # this boolean counter is set to True
    # when the object identified by 'the_object_id' argument is pointed at.
    object_pointed_at_counter_update = \
        PTVR.Data.Callbacks.CounterCallback.BooleanGVupdateFromObject(
            boolean_value_of_updated_GV="True",
            label_of_updated_GV="PointedAt" +
            str(the_object_id) + "_%d" % point_and_click_event_number,
            object_id=the_object_id)
    #   The following callback USES the "PointedAtxxx" counter defined above.
    # Thanks to an interaction defined below,
    # the "PointedAtxxx" counter is set to False
    # when the relevant object is NOT pointed at.
    object_not_pointed_at_counter_update = \
        PTVR.Data.Callbacks.CounterCallback.BooleanGVupdateFromObject(
            boolean_value_of_updated_GV="False",
            label_of_updated_GV="PointedAt" +
            str(the_object_id) + "_%d" % point_and_click_event_number,
            object_id=the_object_id)

    #   The following callback USES a boolean COUNTER that has been created
    # above by an Event Callback, namely by TriggerWhenValueIsReached().
    # Reminder: this boolean COUNTER contains a boolean value in a
    # GV (Global Variable) called "PointAndClickCounterxxx" (xxx referring
    # to the object identified by the 'the_object_id' argument).
    #   Thanks to an interaction defined below,
    # this boolean COUNTER is set to True
    # IF and only IF...
    # the "PointedAt..." counter defined above is True (i.e. if the
    # relevant object is pointed at).
    object_trigger_counter_update = \
        PTVR.Data.Callbacks.CounterCallback.BooleanGVupdateFromObject(
            boolean_value_of_updated_GV="True",
            label_of_updated_GV="PointAndClickCounter" +
            str(the_object_id) + "_%d" % point_and_click_event_number,
            object_id=the_object_id,
            update_if_this_GV_is_true="PointedAt" + str(the_object_id) +
            "_%d" % point_and_click_event_number)

    my_scene.AddInteraction(events=[object_is_pointed_at],
                            callbacks=[object_pointed_at_counter_update])

    my_scene.AddInteraction(events=[object_is_not_pointed_at],
                            callbacks=[object_not_pointed_at_counter_update])

    my_scene.AddInteraction(events=[trigger_button],
                            callbacks=[object_trigger_counter_update])

    point_and_click_event_number += 1

    return object_trigger_counter_has_reached_its_goal, object_is_pointed_at, \
        object_is_not_pointed_at


def MenuWithPointAndClick(
        my_scene, my_world, valid_responses=["right_trigger"],
        button_names=[],
        canvas_text="Texte",
        expected_answer=-1,
        object_list_to_change=[],
        callbacks_list=[],
        canvas_position_in_current_CS=np.array([0, 0, 0]),
        canvas_rotation_in_current_CS=np.array([0, 0, 0]),
        canvas_text_fontsize_in_postscript_points=100,
        canvas_size_in_meters=np.array([1.2, 1.5, 1]),
        canvas_color=color.RGBColor(r=1, g=1, b=1, a=1),
        button_position_in_canvas_list=[np.array([0, 0])],
        button_size_in_meters_list=np.array([0.4, 0.18]),
        button_text_fontsize_in_postscript_points=100,
        reset_GV_when_button_is_pushed=False,
        button_sprite_names=np.array([])
):

    canvas = PTVR.Stimuli.Objects.Canvas(
        position_in_current_CS=canvas_position_in_current_CS,
        rotation_in_current_CS=canvas_rotation_in_current_CS,
        size_in_meters=canvas_size_in_meters,
        fontsize_in_postscript_points=canvas_text_fontsize_in_postscript_points,
        color=canvas_color,
        canvas_text=canvas_text)

    my_scene.place(canvas, my_world)

    for i in range(len(button_names)):

        if (len(button_sprite_names) > i):
            sprite_name = button_sprite_names[i]
            button_name = ""
        else:
            sprite_name = ""
            button_name = button_names[i]

        # BUTTON
        button_i = PTVR.Stimuli.Objects.Button(
            text=button_name,
            position_in_canvas=button_position_in_canvas_list[i],
            size_in_meters=button_size_in_meters_list[i],
            button_text_fontsize_in_postscript_points=button_text_fontsize_in_postscript_points,
            canvas_id=canvas.id,
            sprite_name=sprite_name
        )

        my_scene.place(button_i, my_world)

        ############ GV names ###################
        triggered_GV = "Triggered" + str(button_i.id)
        pointed_at_GV = "PointedAt" + str(button_i.id)

    ################################## Events #####################################

        button_is_pointed_at = PTVR.Data.Event.PointedAt(
            target_id=button_i.id,
            activation_cone_origin_id=my_world.handControllerRight.id,
            activation_cone_radius_deg=0.01
        )
        button_is_not_pointed_at = PTVR.Data.Event.PointedAt(
            target_id=button_i.id,
            activation_cone_origin_id=my_world.handControllerRight.id,
            activation_cone_radius_deg=0.01, mode="release")

        trigger_button = PTVR.Data.Event.HandController(
            valid_responses=['right_trigger'],
            mode="press")

        button_trigger_counter_has_reached_its_goal = PTVR.Data.Events.CounterEvent.TriggerWhenValueIsReached(
            value_to_be_reached=1,
            # note reset_the_counter is False by default
            global_variables_to_reset=np.array(
                []),
            label_of_updated_GV_to_use="Triggered" + str(button_i.id))

    ################################## Callbacks #####################################

        button_highlighted = PTVR.Data.Callback.HighlightButton(
            object_id=button_i.id
        )
        button_not_highlighted = PTVR.Data.Callback.HighlightButton(
            object_id=button_i.id,
            effect="deactivate")

        button_pushed = PTVR.Data.Callback.ProcessButton(
            object_id=button_i.id)

        button_pointed_at_counter_update = PTVR.Data.Callbacks.CounterCallback.BooleanGVupdateFromObject(
            boolean_value_of_updated_GV="True",
            label_of_updated_GV="PointedAt" + str(button_i.id),
            object_id=button_i.id
        )

        button_not_pointed_at_counter_update = PTVR.Data.Callbacks.CounterCallback.BooleanGVupdateFromObject(
            boolean_value_of_updated_GV="False",
            label_of_updated_GV="PointedAt" + str(button_i.id),
            object_id=button_i.id)

        button_trigger_counter_update = PTVR.Data.Callbacks.CounterCallback.BooleanGVupdateFromObject(
            boolean_value_of_updated_GV="True",
            label_of_updated_GV="Triggered" + str(button_i.id),
            object_id=button_i.id,
            update_if_this_GV_is_true="PointedAt" + str(button_i.id))

        resetGV = PTVR.Data.Callbacks.CounterCallback.ResetGlobalVariables(
            label_of_updated_GV_list=np.array([pointed_at_GV, triggered_GV]),
            global_variable_initial_value_list=np.array([0, 0]))

    ################################## Interactions #####################################
        my_scene.AddInteraction(events=[button_is_pointed_at],
                                callbacks=[button_highlighted,
                                           button_pointed_at_counter_update]
                                )
        my_scene.AddInteraction(events=[button_is_not_pointed_at],
                                callbacks=[button_not_highlighted,
                                           button_not_pointed_at_counter_update]
                                )
        my_scene.AddInteraction(events=[trigger_button],
                                callbacks=[button_trigger_counter_update]
                                )
        my_scene.AddInteraction(events=[button_trigger_counter_has_reached_its_goal],
                                callbacks=[button_pushed]
                                )
        if (reset_GV_when_button_is_pushed):
            callback_i = callbacks_list[i]
            callback_i.append(resetGV)
        else:
            callback_i = callbacks_list[i]

        my_scene.AddInteraction(
            events=[button_trigger_counter_has_reached_its_goal],
            callbacks=callback_i)
