# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 10:27:17 2021

@author: Eric
"""

from PTVR.Stimuli.Utils import Frame, MetaData
import json
from PTVR.Stimuli.Utils import ComplexEncoder

class Calculator(Frame):
    """
    An object that can be created to calculate various values such as angles
    distances etc. The result can be used by other GO
    """
    def __init__(self):
        super().__init__()
        self.calculation = ""
        self.idsForCalculation = []
        
    def StringIntegration (self, is_freezed= False):
        if (is_freezed == True) :
            return "%"+str(self.id)+"i"
        else :
            return "%"+str(self.id)
        
class CalculatorManager(MetaData):

    def __init__(self):
        self.calculatorList = {}
    
    def to_json(self):
        return dict(self.__dict__)

    def write(self, fp):
        #fp.write(str(self.to_json()))
        fp.write(json.dumps(self.to_json(), sort_keys=True, indent=4, cls=ComplexEncoder))

    def CleanCalculations (self):
        self.calculatorList.clear()

    def AddPositionCalculation(self, virtual_point1, virtual_point2):
        aCalc = Calculator()
        aCalc.calculation = virtual_point1._get_reference_position() + "+" + virtual_point2._get_reference_position()
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
    
    def SubtractPositionCalculation(self, virtual_point1, virtual_point2):
        aCalc = Calculator()
        aCalc.calculation = virtual_point1._get_reference_position() + "-" + virtual_point2._get_reference_position()
        self.calculatorList[aCalc.id] = aCalc
        return aCalc

    def AddAngleAtOriginBetweenTwoVisualObjects(self, visualObject1, visualObject2):
        aCalc = Calculator()
        aCalc.calculation = "AngleAtOriginBetweenTwoVisualObjects"
        aCalc.idsForCalculation.append(visualObject1.id)
        aCalc.idsForCalculation.append(visualObject2.id)
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
        
    def AddRadialDistanceFromOrigin (self, visualObject):
        aCalc = Calculator()
        aCalc.calculation = "RadialDistanceFromOrigin"
        aCalc.idsForCalculation.append(visualObject.id)
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
        
    def AddAngleAtHeadPOVBetweenTwoVisualObjects (self, visualObject1, visualObject2):
        aCalc = Calculator()
        aCalc.calculation = "AngleAtHeadPOVBetweenTwoVisualObjects"
        aCalc.idsForCalculation.append(visualObject1.id)
        aCalc.idsForCalculation.append(visualObject2.id)
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
        
    def AddRadialDistanceFromHeadPOV (self, visualObject):
        aCalc = Calculator()
        aCalc.calculation = "RadialDistanceFromHeadPOV"
        aCalc.idsForCalculation.append(visualObject.id)
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
        
    def AddDistanceBetweenTwoVisualObjects (self,visualObject1, visualObject2):
        aCalc = Calculator()
        aCalc.calculation = "DistanceBetweenTwoVisualObjects"
        aCalc.idsForCalculation.append(visualObject1.id)
        aCalc.idsForCalculation.append(visualObject2.id)
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
        
    def AddAngleAtApexBetweenTwoVisualObjects (self, Apex, visualObject1, visualObject2):
        aCalc = Calculator()
        aCalc.calculation = "AngleAtApexBetweenTwoVisualObjects"
        aCalc.idsForCalculation.append(Apex.id)
        aCalc.idsForCalculation.append(visualObject1.id)
        aCalc.idsForCalculation.append(visualObject2.id)
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
    def AddElaspedTimeCurrentScene(self):
        aCalc = Calculator()
        aCalc.calculation = "ElaspedTimeCurrentScene"
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
    def AddReadingAccessibilityIndex(self):
        aCalc = Calculator()
        aCalc.calculation = "ReadingAccessibilityIndex"
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
    def AddReadingAcuity(self):
        aCalc = Calculator()
        aCalc.calculation = "ReadingAcuity"
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
    def AddCPS(self):
        aCalc = Calculator()
        aCalc.calculation = "CPS"
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
    def AddMRS(self):
        aCalc = Calculator()
        aCalc.calculation = "MRS"
        self.calculatorList[aCalc.id] = aCalc
        return aCalc
    def AddData(self, result_file_data) :
        aCalc = Calculator()
        aCalc.calculation = "Data"
        self.calculatorList[aCalc.id] = aCalc
        self.resultFileData = result_file_data
        return aCalc
        