# -*- coding: utf-8 -*-
"""
Interfaces for creating UIObject for the 3D World
"""
from .Objects import ColoratedObject
from .Utils import Resizable
from .Utils import VirtualPoint
from .Utils import VirtualPoint2D
from .Utils import MetaData
from .Utils import ComplexEncoder
from .Utils import VisualObject
from .Utils import EmptyVisualObject
#from .Utils import InfiniteCircularCone
from .Utils import Button
from .Utils import Screen
from  PTVR.LogManager import *
import PTVR.Pointing.PointingCursor
import PTVR.Tools as tools
import PTVR.Visual

import random
import json
import numpy as np

import PTVR.Stimuli.Color as color
from PTVR.Stimuli.Texture import Texture
import PTVR.Data.Interaction as Interaction
import PTVR.Stimuli.Objects as Objects
import copy

PI = np.pi
TWOPI = 2 * PI


# Some predefined textures
diffuse_texture = Texture()
PSCounter=0

class UIObject (Resizable):
    """

    """
    def __init__(self,is_active=True, position_in_current_CS=np.array([0.0,0.0,0.0]),
                     rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                     scale = 1) :#scale=np.array([1.0, 1.0, 1.0])): #TODO: 3d scale
            VirtualPoint.__init__(self, position_in_current_CS,rotation_in_current_CS)
            self.isActive= is_active
            if type(scale) is not np.ndarray:
                ## define the size of the visual object. Can be a simple float or 3d table
                self.scale =  np.array([scale,scale,scale])
            else:
                if scale.size == 2:
                    self.scale = np.array([scale[0],scale[1],1])
                else:
                    self.scale = np.array([scale[0],scale[1],scale[2]])
            Resizable.__init__(self, self.scale[0],self.scale[1],self.scale[2], position_in_current_CS, rotation_in_current_CS)

class ButtonUI(UIObject, Button):
    def __init__(self, is_active=True,
                 color=color.RGBColor(r=0, g=0, b=0, a=1),
                 texture = diffuse_texture,
                 position_in_current_CS=np.array([0.0,0.0,0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 width =160.0 , 
                 height= 160.0,
                 name_button=""

                 ):
        """ Initialization with parameters
        

        """
        UIObject.__init__(self,is_active, position_in_current_CS, rotation_in_current_CS)
        PTVR.Stimuli.Objects.ColoratedObject.__init__(self,is_visible=True, color=color,texture=diffuse_texture)
        Button.__init__(self,name_button=name_button)
        self.width = width
        self.height = height
        self.UIObjects = []
        ## identifier for serialization
        self.type = "buttonUI"
        
    def placeUIObject (self, o):
        self.UIObjects.append(o) 
        
  
class GraphUI (UIObject,ColoratedObject) :
    def __init__(self, is_active=True,
                 position_in_current_CS=np.array([0.0,0.0,0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size = np.array([1000, 600]),
                 color = color.RGBColor(r=0.2, g=0.3, b=0.5, a=1),
                 dot_color = color.RGBColor(r=1.0, g=1.0, b=1.0, a=1),
                 dot_connection_color = color.RGBColor(r=1.0, g=1.0, b=1.0, a=1),
                 frame_color = color.RGBColor(r=0.0, g=0.0, b=0.0, a=1),
                 text_color = color.RGBColor(r=1.0, g=1.0, b=1.0, a=1),
                 fontsize_in_points = 12,
                 grid_color = color.RGBColor(r=1.0, g=1.0, b=1.0, a=1),
                 minimum_y_value = 0,
                 maximum_y_value = 100,
                 minimum_x_value = 100,
                 maximum_x_value = 100,
                 graduations_x_range = 10,
                 graduations_y_range = 10,
                 x_axis_name = "x",
                 y_axis_name = "y",
                 x_is_invariant_culture = False,
                 y_is_invariant_culture = False,
                 is_grid = True,
                 ):
        """ Initialization with parameters
        
        @param size : size of the graph
        @param color : graph background color 
        @param dot_color : color of the dot 
        @param dot_connection_color : color of the line that connects dots
        @param minimum_y_value : minimum value on the Y axis
        @param minimum_x_value : minimum value on the X axis
        @param maximum_y_value : maximum value on the Y axis
        @param maximum_x_value : maximum value on the X axis
        @paran graduations_x_range : numerical interval between each graduation on the x axis
        @paran graduations_y_range : numerical interval between each graduation on the y axis
        @param x_axis_name : name of the X axis
        @param y_axis_name : name of the Y axis
        @param grid : specify the presence of a grid behind dots 
        
        """
        
        UIObject.__init__(self,is_active, position_in_current_CS, rotation_in_current_CS)
        Objects.ColoratedObject.__init__(self,is_visible=True, color=color)
        self.type = "graphUI"
        self.color = color
        self.dotColor = dot_color
        self.dotConnectionColor = dot_connection_color
        self.frameColor = frame_color
        self.textColor = text_color
        self.fontSizeInPoints = fontsize_in_points
        self.gridColor = grid_color
        self.size = size
        self.minimumYValue = minimum_y_value
        self.maximumYValue = maximum_y_value
        self.minimumXValue = minimum_x_value
        self.maximumXValue = maximum_x_value
        self.graduationsXRange = graduations_x_range
        self.graduationsYRange = graduations_y_range
        self.xAxisName = x_axis_name
        self.yAxisName = y_axis_name
        self.xIsInvariantCulture = x_is_invariant_culture
        self.yIsInvariantCulture = y_is_invariant_culture
        self.grid =is_grid
        
    
      
    def UseAppendStringAsValues(self, x_values_name = "", y_values_name = "", operation_x = "", operation_y = "") :
        self.xValuesName = x_values_name
        self.yValuesName = y_values_name
        self.operationX = operation_x
        self.operationY = operation_y
    
    def CreateSecondXAxis(self,x_up_axis_name="", operation_type="") :
            """
            Method used to create a graph with additionnal graduations and labels on the top
            @param x_up_labels : values for the graduations on the new X axis on the top
            @param x_up_axis_name : name of the new X axis on the top
    
            """
            self.secondXAxis = True
            self.xUpAxisName = x_up_axis_name

            
            x_up_values = []
            final_x_up_value = []
            values_count = round((self.maximumXValue - self.minimumXValue) / self.graduationsXRange)
            graduation_x = self.minimumXValue
            
            for i in range(0,values_count+1) :
                x_up_values.append(graduation_x)
                graduation_x += self.graduationsXRange
                
            
    
                
            if (operation_type == "conversionlogmardegrees") :
                
                for i in range(0,len(x_up_values)) :
                    valueConverted = tools.logmar_to_visual_angle_in_degrees(x_up_values[i])
                    final_x_up_value.append(round(valueConverted,2))
                                            
                self.XUpLabels = final_x_up_value
                
            elif (operation_type == "square") :
                
                for i in range(len(x_up_values)) :
                    final_x_up_value.append(x_up_values[i]**2)
                                            
                self.XUpLabels = final_x_up_value
                
            else :
                self.XUpLabels = x_up_values
        

    
    def CreateSecondYAxis(self,y_right_axis_name = "", operation_type="" ) :
        """
        Method used to create a graph with additionnal graduations and labels on the right
        @param y_right_labels : values for the graduations on the new Y axis on the right
        @param y_right_axis_name : name of the new Y axis on the right

        """
        self.secondYAxis = True
        self.yRightAxisName = y_right_axis_name

        
        y_right_values = []
        final_y_right_value = []
        values_count = round((self.maximumYValue - self.minimumYValue) / self.graduationsYRange)
        graduation_y = self.minimumYValue
        
        for i in range(0,values_count+1) :
            y_right_values.append(graduation_y)
            graduation_y += self.graduationsYRange
            
        if (operation_type == "square") :
            
            for i in range(len(y_right_values)) :
                final_y_right_value.append(y_right_values[i]**2)
                                        
            self.YRightLabels = final_y_right_value
        else :     
            self.YRightLabels = y_right_values

        
        

        
class TextUI (UIObject,ColoratedObject):
    def __init__(self, is_active=True,text="",
                 font_name='Arial',
                 is_bold=False,
                 is_italic=False,
                 horizontal_alignment = "Center",
                 vertical_alignment = "Middle",
                 fontsize_in_points = 14,
                 color=color.RGBColor(r=0, g=0, b=0, a=1),
                 texture = diffuse_texture,
                 position_in_current_CS=np.array([0.0,0.0,0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 textbox_width = 200,
                 textbox_height = 50
                 ):
        """ Initialization with parameters
        
        @param text : text to be displayed
        @param font_name : name of the font you want to use for the text. The font to use have to be placed in PTVR_Researchers > PTVR_Operators > resources > Fonts to be used. It has to be in a .ttf format
        @param fontsize_in_points : size of the text. The unit here is the Unity point, which is the standard unit of the font size in Unity.  
        @param horizontal_alignment : horizontal alignment between : Left, Right, Center, Justified
        @param vertical_alignment : vertical alignment between :Middle, Capline, Baseline, Bottom, Top
        @param visual_angle_of_centered_x_height_deg x-height : in degrees 
        @param position_in_current_CS value for be show while be between -1 and 1 x while be width  and y  while be height
        """
        UIObject.__init__(self,is_active, position_in_current_CS, rotation_in_current_CS)
        ColoratedObject.__init__(self,is_visible=True, color=color)
        self.text = text
        self.isBold = is_bold
        self.isItalic = is_italic

        ## Left, Right, Center, Justified
        self.alignHorizontal = horizontal_alignment 
        
        ## Middle, Capline, Baseline, Bottom, Top
        self.alignVertical = vertical_alignment 
        
        self.fontName = font_name
        self.fontSize = fontsize_in_points 
        
        self.textboxWidth = textbox_width
        self.textboxHeight = textbox_height
        

        self.isWrapping = False

        ## identifier for serialization
        self.type = "textUI"
       
class ImageUI (UIObject,ColoratedObject):
    def __init__(self, is_active=True,
                 color=color.RGBColor(r=0, g=0, b=0, a=1),
                 texture = diffuse_texture,
                 position_in_current_CS=np.array([0.0,0.0,0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 width =160.0 , 
                 height= 160.0,
                 image_file="default",
                 image_type = "default"
                 ):
        """ Initialization with parameters
        

        """
        UIObject.__init__(self,is_active, position_in_current_CS, rotation_in_current_CS)
        ColoratedObject.__init__(self,is_visible=True, color=color)
        self.width = width
        self.height = height
        self.imageFile = image_file
        ## identifier for serialization
        self.imageType = image_type
        self.type = "imageUI"
