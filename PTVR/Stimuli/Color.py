"""
Interfaces for various formats for color.
"""

class Color():
    """
    Base class for color
    """
    def to_json(self):
        
        assert False, "color: should derive to_json in subclasses"

class RGBColor(Color):
    """
    RGBColor specifies color as Red, Green and Blue values as floats in the range [0.0,1.0] each.
    The Alpha value is a float in the range [0.0,1.0]. Alpha denotes transparency (0 is transparent and 1 is opaque).
    """
    def __init__(self, r=0.0, g=0.0, b=0.0, a=1.0):
        self.r = float(r)
        self.g = float(g)
        self.b = float(b)
        ## additionnal alpha layer
        self.a = float(a)
        self.type ="rgbcolor"

    def get_color(self):
        return (self.r, self.g, self.b, self.a)
    def set_color(self, r, g, b, a=1.0):
        self.r = float(r)
        self.g = float(g)
        self.b = float(b)
        self.a = float(a)

    color = property(get_color, set_color)

    def to_json(self):
        return dict(self.__dict__)
    def __str__(self):
        return "RGB(%f, %f, %f, a=%f)" % (self.r, self.g, self.b, self.a)

class DynamicColor (Color):
    """
    Using RGB color looping with time
    """

    def __init__(self, colorCollection=[]):
        ## The colors colleciton should be a dictionnary of {time(ms), RGBAColor}
        self.colorCollection = colorCollection
        self.type ="dynamicColor"
    def add_time_color (self, time, color):
        """
        add a time and color in the collection. color should be a RGBColor
        """
        self.colorCollection[time]=color
        
    def set_dynamic_color(self, colorCollection):
        self.colorCollection = colorCollection

    def to_json(self):
        return dict(self.__dict__)

class RGB255Color(Color):
    """
    RGB255Color specifies color as Red, Green and Blue values as integers in the range [0,255] each.
    The Alpha value is a float in the range [0.0,1.0].
    Alpha denotes transparency (0 is transparent and 1 is opaque).
    """
    def __init__(self, r=0, g=0, b=0, a=1.0):
        self.set_color(r, g, b, a)
        
    def get_color(self):
        return (self.r, self.g, self.b)
    def set_color(self, r,g,b,a=1.0):
        assert 0 <= r and r <= 255, "RGB255Color: R=%d out of range (allowed 0-255)" % r
        assert 0 <= g and g <= 255, "RGB255Color: G=%d out of range (allowed 0-255)" % g
        assert 0 <= b and b <= 255, "RGB255Color: B=%d out of range (allowed 0-255)" % b
        assert 0.0 <= a <= 1.0, "RGB255Color: a=%f out of range (allowed range: [0.0,1.0])" % a
        self.r = r
        self.g = g
        self.b = b
        self.a = a
        self.type ="rgb255color"
    color = property(get_color, set_color)

    def to_json(self):
        return dict(self.__dict__)

    def __str__(self):
        return "RGB255(%d,%d,%d, a=%f)" % (self.r, self.g, self.b, self.a)

class HexColor(Color):
    """
    HexColor specifies color as Red, Green and Blue values encoded as "RRGGBB" in hex.
    The Alpha value is a float in the range [0.0,1.0].
    Alpha denotes transparency (0 is transparent and 1 is opaque).
    """
    def __init__(self, hex="FFFFFF", a = 1.0):
        assert 0.0 <= a <= 1.0, "HexColor: a=%f out of range (allowed range: [0.0,1.0])" % a
        self.colorHex = hex
        self.a = a
        self.type ="hexcolor"
        
    def get_color(self):
        return self.colorHex
    def set_color(self, hex):
        self.colorHex = hex

    color = property(get_color, set_color)

    def to_json(self):
        # return json.dumps({ 'type' : 'hexcolor', 'hex' : self.color })
        return dict(self.__dict__)

    def __str__(self):
        return "Hex(%s, a=%f)" % (hex(self.get_color()), self.a)


def hextorgb255(hexcolor):
    hexcode = hexcolor.color
    r = hexcode >> 16
    g = (hexcode >> 8) & "0000FF"
    b = (hexcode & "0000FF")
    return RGB255Color(r, g, b, hexcolor.a)

def rgb255tohex(c):
    (r,g,b) = c.color
    hex = (r << 16) | (g << 8) | b
    return HexColor(hex, c.a)

"""HTML color values"""
colorsRGB = {
    "aliceblue": RGBColor(0.882352941176471, 0.945098039215686, 1),
    "antiquewhite": RGBColor(0.96078431372549, 0.843137254901961, 0.686274509803922),
    "aqua": RGBColor(-1, 1, 1),
    "aquamarine": RGBColor(-0.00392156862745097, 1, 0.662745098039216),
    "azure": RGBColor(0.882352941176471, 1, 1),
    "beige": RGBColor(0.92156862745098, 0.92156862745098, 0.725490196078431),
    "bisque": RGBColor(1, 0.788235294117647, 0.537254901960784),
    "black": RGBColor(-1, -1, -1),
    "blanchedalmond": RGBColor(1, 0.843137254901961, 0.607843137254902),
    "blue": RGBColor(-1, -1, 1),
    "blueviolet": RGBColor(0.0823529411764705, -0.662745098039216, 0.772549019607843),
    "brown": RGBColor(0.294117647058824, -0.670588235294118, -0.670588235294118),
    "burlywood": RGBColor(0.741176470588235, 0.443137254901961, 0.0588235294117647),
    "cadetblue": RGBColor(-0.254901960784314, 0.23921568627451, 0.254901960784314),
    "chartreuse": RGBColor(-0.00392156862745097, 1, -1),
    "chocolate": RGBColor(0.647058823529412, -0.176470588235294, -0.764705882352941),
    "coral": RGBColor(1, -0.00392156862745097, -0.372549019607843),
    "cornflowerblue": RGBColor(-0.215686274509804, 0.168627450980392, 0.858823529411765),
    "cornsilk": RGBColor(1, 0.945098039215686, 0.725490196078431),
    "crimson": RGBColor(0.725490196078431, -0.843137254901961, -0.529411764705882),
    "cyan": RGBColor(-1, 1, 1),
    "darkblue": RGBColor(-1, -1, 0.0901960784313725),
    "darkcyan": RGBColor(-1, 0.0901960784313725, 0.0901960784313725),
    "darkgoldenrod": RGBColor(0.443137254901961, 0.0509803921568628, -0.913725490196078),
    "darkgray": RGBColor(0.325490196078431, 0.325490196078431, 0.325490196078431),
    "darkgreen": RGBColor(-1, -0.215686274509804, -1),
    "darkgrey": RGBColor(0.325490196078431, 0.325490196078431, 0.325490196078431),
    "darkkhaki": RGBColor(0.482352941176471, 0.435294117647059, -0.16078431372549),
    "darkmagenta": RGBColor(0.0901960784313725, -1, 0.0901960784313725),
    "darkolivegreen": RGBColor(-0.333333333333333, -0.16078431372549, -0.631372549019608),
    "darkorange": RGBColor(1, 0.0980392156862746, -1),
    "darkorchid": RGBColor(0.2, -0.607843137254902, 0.6),
    "darkred": RGBColor(0.0901960784313725, -1, -1),
    "darksalmon": RGBColor(0.827450980392157, 0.176470588235294, -0.0431372549019607),
    "darkseagreen": RGBColor(0.12156862745098, 0.474509803921569, 0.12156862745098),
    "darkslateblue": RGBColor(-0.435294117647059, -0.52156862745098, 0.0901960784313725),
    "darkslategray": RGBColor(-0.631372549019608, -0.380392156862745, -0.380392156862745),
    "darkslategrey": RGBColor(-0.631372549019608, -0.380392156862745, -0.380392156862745),
    "darkturquoise": RGBColor(-1, 0.615686274509804, 0.63921568627451),
    "darkviolet": RGBColor(0.16078431372549, -1, 0.654901960784314),
    "deeppink": RGBColor(1, -0.843137254901961, 0.152941176470588),
    "deepskyblue": RGBColor(-1, 0.498039215686275, 1),
    "dimgray": RGBColor(-0.176470588235294, -0.176470588235294, -0.176470588235294),
    "dimgrey": RGBColor(-0.176470588235294, -0.176470588235294, -0.176470588235294),
    "dodgerblue": RGBColor(-0.764705882352941, 0.129411764705882, 1),
    "firebrick": RGBColor(0.396078431372549, -0.733333333333333, -0.733333333333333),
    "floralwhite": RGBColor(1, 0.96078431372549, 0.882352941176471),
    "forestgreen": RGBColor(-0.733333333333333, 0.0901960784313725, -0.733333333333333),
    "fuchsia": RGBColor(1, -1, 1),
    "gainsboro": RGBColor(0.725490196078431, 0.725490196078431, 0.725490196078431),
    "ghostwhite": RGBColor(0.945098039215686, 0.945098039215686, 1),
    "gold": RGBColor(1, 0.686274509803922, -1),
    "goldenrod": RGBColor(0.709803921568627, 0.294117647058824, -0.749019607843137),
    "gray": RGBColor(0.00392156862745097, 0.00392156862745097, 0.00392156862745097),
    "grey": RGBColor(0.00392156862745097, 0.00392156862745097, 0.00392156862745097),
    "green": RGBColor(-1, 0.00392156862745097, -1),
    "greenyellow": RGBColor(0.356862745098039, 1, -0.631372549019608),
    "honeydew": RGBColor(0.882352941176471, 1, 0.882352941176471),
    "hotpink": RGBColor(1, -0.176470588235294, 0.411764705882353),
    "indianred": RGBColor(0.607843137254902, -0.27843137254902, -0.27843137254902),
    "indigo": RGBColor(-0.411764705882353, -1, 0.0196078431372548),
    "ivory": RGBColor(1, 1, 0.882352941176471),
    "khaki": RGBColor(0.882352941176471, 0.803921568627451, 0.0980392156862746),
    "lavender": RGBColor(0.803921568627451, 0.803921568627451, 0.96078431372549),
    "lavenderblush": RGBColor(1, 0.882352941176471, 0.92156862745098),
    "lawngreen": RGBColor(-0.0274509803921569, 0.976470588235294, -1),
    "lemonchiffon": RGBColor(1, 0.96078431372549, 0.607843137254902),
    "lightblue": RGBColor(0.356862745098039, 0.694117647058824, 0.803921568627451),
    "lightcoral": RGBColor(0.882352941176471, 0.00392156862745097, 0.00392156862745097),
    "lightcyan": RGBColor(0.756862745098039, 1, 1),
    "lightgoldenrodyellow": RGBColor(0.96078431372549, 0.96078431372549, 0.647058823529412),
    "lightgray": RGBColor(0.654901960784314, 0.654901960784314, 0.654901960784314),
    "lightgreen": RGBColor(0.129411764705882, 0.866666666666667, 0.129411764705882),
    "lightgrey": RGBColor(0.654901960784314, 0.654901960784314, 0.654901960784314),
    "lightpink": RGBColor(1, 0.427450980392157, 0.513725490196078),
    "lightsalmon": RGBColor(1, 0.254901960784314, -0.0431372549019607),
    "lightseagreen": RGBColor(-0.749019607843137, 0.396078431372549, 0.333333333333333),
    "lightskyblue": RGBColor(0.0588235294117647, 0.615686274509804, 0.96078431372549),
    "lightslategray": RGBColor(-0.0666666666666667, 0.0666666666666667, 0.2),
    "lightslategrey": RGBColor(-0.0666666666666667, 0.0666666666666667, 0.2),
    "lightsteelblue": RGBColor(0.380392156862745, 0.537254901960784, 0.741176470588235),
    "lightyellow": RGBColor(1, 1, 0.756862745098039),
    "lime": RGBColor(-1, 1, -1),
    "limegreen": RGBColor(-0.607843137254902, 0.607843137254902, -0.607843137254902),
    "linen": RGBColor(0.96078431372549, 0.882352941176471, 0.803921568627451),
    "magenta": RGBColor(1, -1, 1),
    "maroon": RGBColor(0.00392156862745097, -1, -1),
    "mediumaquamarine": RGBColor(-0.2, 0.607843137254902, 0.333333333333333),
    "mediumblue": RGBColor(-1, -1, 0.607843137254902),
    "mediumorchid": RGBColor(0.458823529411765, -0.333333333333333, 0.654901960784314),
    "mediumpurple": RGBColor(0.152941176470588, -0.12156862745098, 0.717647058823529),
    "mediumseagreen": RGBColor(-0.529411764705882, 0.403921568627451, -0.113725490196078),
    "mediumslateblue": RGBColor(-0.0352941176470588, -0.184313725490196, 0.866666666666667),
    "mediumspringgreen": RGBColor(-1, 0.96078431372549, 0.207843137254902),
    "mediumturquoise": RGBColor(-0.435294117647059, 0.63921568627451, 0.6),
    "mediumvioletred": RGBColor(0.56078431372549, -0.835294117647059, 0.0431372549019609),
    "midnightblue": RGBColor(-0.803921568627451, -0.803921568627451, -0.12156862745098),
    "mintcream": RGBColor(0.92156862745098, 1, 0.96078431372549),
    "mistyrose": RGBColor(1, 0.788235294117647, 0.764705882352941),
    "moccasin": RGBColor(1, 0.788235294117647, 0.419607843137255),
    "navajowhite": RGBColor(1, 0.741176470588235, 0.356862745098039),
    "navy": RGBColor(-1, -1, 0.00392156862745097),
    "oldlace": RGBColor(0.984313725490196, 0.92156862745098, 0.803921568627451),
    "olive": RGBColor(0.00392156862745097, 0.00392156862745097, -1),
    "olivedrab": RGBColor(-0.16078431372549, 0.113725490196078, -0.725490196078431),
    "orange": RGBColor(1, 0.294117647058824, -1),
    "orangered": RGBColor(1, -0.458823529411765, -1),
    "orchid": RGBColor(0.709803921568627, -0.12156862745098, 0.67843137254902),
    "palegoldenrod": RGBColor(0.866666666666667, 0.819607843137255, 0.333333333333333),
    "palegreen": RGBColor(0.192156862745098, 0.968627450980392, 0.192156862745098),
    "paleturquoise": RGBColor(0.372549019607843, 0.866666666666667, 0.866666666666667),
    "palevioletred": RGBColor(0.717647058823529, -0.12156862745098, 0.152941176470588),
    "papayawhip": RGBColor(1, 0.874509803921569, 0.670588235294118),
    "peachpuff": RGBColor(1, 0.709803921568627, 0.450980392156863),
    "peru": RGBColor(0.607843137254902, 0.0431372549019609, -0.505882352941176),
    "pink": RGBColor(1, 0.505882352941176, 0.592156862745098),
    "plum": RGBColor(0.733333333333333, 0.254901960784314, 0.733333333333333),
    "powderblue": RGBColor(0.380392156862745, 0.756862745098039, 0.803921568627451),
    "purple": RGBColor(0.00392156862745097, -1, 0.00392156862745097),
    "red": RGBColor(1, -1, -1),
    "rosybrown": RGBColor(0.474509803921569, 0.12156862745098, 0.12156862745098),
    "royalblue": RGBColor(-0.490196078431373, -0.176470588235294, 0.764705882352941),
    "saddlebrown": RGBColor(0.0901960784313725, -0.458823529411765, -0.850980392156863),
    "salmon": RGBColor(0.96078431372549, 0.00392156862745097, -0.105882352941176),
    "sandybrown": RGBColor(0.913725490196079, 0.286274509803922, -0.247058823529412),
    "seagreen": RGBColor(-0.63921568627451, 0.0901960784313725, -0.317647058823529),
    "seashell": RGBColor(1, 0.92156862745098, 0.866666666666667),
    "sienna": RGBColor(0.254901960784314, -0.356862745098039, -0.647058823529412),
    "silver": RGBColor(0.505882352941176, 0.505882352941176, 0.505882352941176),
    "skyblue": RGBColor(0.0588235294117647, 0.615686274509804, 0.843137254901961),
    "slateblue": RGBColor(-0.168627450980392, -0.294117647058823, 0.607843137254902),
    "slategray": RGBColor(-0.12156862745098, 0.00392156862745097, 0.129411764705882),
    "slategrey": RGBColor(-0.12156862745098, 0.00392156862745097, 0.129411764705882),
    "snow": RGBColor(1, 0.96078431372549, 0.96078431372549),
    "springgreen": RGBColor(-1, 1, -0.00392156862745097),
    "steelblue": RGBColor(-0.450980392156863, 0.0196078431372548, 0.411764705882353),
    "tan": RGBColor(0.647058823529412, 0.411764705882353, 0.0980392156862746),
    "teal": RGBColor(-1, 0.00392156862745097, 0.00392156862745097),
    "thistle": RGBColor(0.694117647058824, 0.498039215686275, 0.694117647058824),
    "tomato": RGBColor(1, -0.223529411764706, -0.443137254901961),
    "turquoise": RGBColor(-0.498039215686275, 0.756862745098039, 0.631372549019608),
    "violet": RGBColor(0.866666666666667, 0.0196078431372548, 0.866666666666667),
    "wheat": RGBColor(0.92156862745098, 0.741176470588235, 0.403921568627451),
    "white": RGBColor(1, 1, 1),
    "whitesmoke": RGBColor(0.92156862745098, 0.92156862745098, 0.92156862745098),
    "yellow": RGBColor(1, 1, -1),
    "yellowgreen": RGBColor(0.207843137254902, 0.607843137254902, -0.607843137254902),
}
colorsHex = {
    'aliceblue': HexColor("F0F8FF"),
    'antiquewhite': HexColor("FAEBD7"),
    'aqua': HexColor("00FFFF"),
    'aquamarine': HexColor("7FFFD4"),
    'azure': HexColor("F0FFFF"),
    'beige': HexColor("F5F5DC"),
    'bisque': HexColor("FFE4C4"),
    'black': HexColor("000000"),
    'blanchedalmond': HexColor("FFEBCD"),
    'blue': HexColor("0000FF"),
    'blueviolet': HexColor("8A2BE2"),
    'brown': HexColor("A52A2A"),
    'burlywood': HexColor("DEB887"),
    'cadetblue': HexColor("5F9EA0"),
    'chartreuse': HexColor("7FFF00"),
    'chocolate': HexColor("D2691E"),
    'coral': HexColor("FF7F50"),
    'cornflowerblue': HexColor("6495ED"),
    'cornsilk': HexColor("FFF8DC"),
    'crimson': HexColor("DC143C"),
    'cyan': HexColor("00FFFF"),
    'darkblue': HexColor("00008B"),
    'darkcyan': HexColor("008B8B"),
    'darkgoldenrod': HexColor("B8860B"),
    'darkgray': HexColor("A9A9A9"),
    'darkgreen': HexColor("006400"),
    'darkkhaki': HexColor("BDB76B"),
    'darkmagenta': HexColor("8B008B"),
    'darkolivegreen': HexColor("556B2F"),
    'darkorange': HexColor("FF8C00"),
    'darkorchid': HexColor("9932CC"),
    'darkred': HexColor("8B0000"),
    'darksalmon': HexColor("E9967A"),
    'darkseagreen': HexColor("8FBC8B"),
    'darkslateblue': HexColor("483D8B"),
    'darkslategray': HexColor("2F4F4F"),
    'darkturquoise': HexColor("00CED1"),
    'darkviolet': HexColor("9400D3"),
    'deeppink': HexColor("FF1493"),
    'deepskyblue': HexColor("00BFFF"),
    'dimgray': HexColor("696969"),
    'dodgerblue': HexColor("1E90FF"),
    'firebrick': HexColor("B22222"),
    'floralwhite': HexColor("FFFAF0"),
    'forestgreen': HexColor("228B22"),
    'fuchsia': HexColor("FF00FF"),
    'gainsboro': HexColor("DCDCDC"),
    'ghostwhite': HexColor("F8F8FF"),
    'gold': HexColor("FFD700"),
    'goldenrod': HexColor("DAA520"),
    'gray': HexColor("808080"),
    'green': HexColor("008000"),
    'greenyellow': HexColor("ADFF2F"),
    'honeydew': HexColor("F0FFF0"),
    'hotpink': HexColor("FF69B4"),
    'indianred': HexColor("CD5C5C"),
    'indigo': HexColor("4B0082"),
    'ivory': HexColor("FFFFF0"),
    'khaki': HexColor("F0E68C"),
    'lavender': HexColor("E6E6FA"),
    'lavenderblush': HexColor("FFF0F5"),
    'lawngreen': HexColor("7CFC00"),
    'lemonchiffon': HexColor("FFFACD"),
    'lightblue': HexColor("ADD8E6"),
    'lightcoral': HexColor("F08080"),
    'lightcyan': HexColor("E0FFFF"),
    'lightgoldenrodyellow': HexColor("FAFAD2"),
    'lightgray': HexColor("D3D3D3"),
    'lightgreen': HexColor("90EE90"),
    'lightpink': HexColor("FFB6C1"),
    'lightsalmon': HexColor("FFA07A"),
    'lightseagreen': HexColor("20B2AA"),
    'lightskyblue': HexColor("87CEFA"),
    'lightslategray': HexColor("778899"),
    'lightsteelblue': HexColor("B0C4DE"),
    'lightyellow': HexColor("FFFFE0"),
    'lime': HexColor("00FF00"),
    'limegreen': HexColor("32CD32"),
    'linen': HexColor("FAF0E6"),
    'magenta': HexColor("FF00FF"),
    'maroon': HexColor("800000"),
    'mediumaquamarine': HexColor("66CDAA"),
    'mediumblue': HexColor("0000CD"),
    'mediumorchid': HexColor("BA55D3"),
    'mediumpurple': HexColor("9370DB"),
    'mediumseagreen': HexColor("3CB371"),
    'mediumslateblue': HexColor("7B68EE"),
    'mediumspringgreen': HexColor("00FA9A"),
    'mediumturquoise': HexColor("48D1CC"),
    'mediumvioletred': HexColor("C71585"),
    'midnightblue': HexColor("191970"),
    'mintcream': HexColor("F5FFFA"),
    'mistyrose': HexColor("FFE4E1"),
    'moccasin': HexColor("FFE4B5"),
    'navajowhite': HexColor("FFDEAD"),
    'navy': HexColor("000080"),
    'oldlace': HexColor("FDF5E6"),
    'olive': HexColor("808000"),
    'olivedrab': HexColor("6B8E23"),
    'orange': HexColor("FFA500"),
    'orangered': HexColor("FF4500"),
    'orchid': HexColor("DA70D6"),
    'palegoldenrod': HexColor("EEE8AA"),
    'palegreen': HexColor("98FB98"),
    'paleturquoise': HexColor("AFEEEE"),
    'palevioletred': HexColor("DB7093"),
    'papayawhip': HexColor("FFEFD5"),
    'peachpuff': HexColor("FFDAB9"),
    'peru': HexColor("CD853F"),
    'pink': HexColor("FFC0CB"),
    'plum': HexColor("DDA0DD"),
    'powderblue': HexColor("B0E0E6"),
    'purple': HexColor("800080"),
    'red': HexColor("FF0000"),
    'rosybrown': HexColor("BC8F8F"),
    'royalblue': HexColor("4169E1"),
    'saddlebrown': HexColor("8B4513"),
    'salmon': HexColor("FA8072"),
    'sandybrown': HexColor("F4A460"),
    'seagreen': HexColor("2E8B57"),
    'seashell': HexColor("FFF5EE"),
    'sienna': HexColor("A0522D"),
    'silver': HexColor("C0C0C0"),
    'skyblue': HexColor("87CEEB"),
    'slateblue': HexColor("6A5ACD"),
    'slategray': HexColor("708090"),
    'snow': HexColor("FFFAFA"),
    'springgreen': HexColor("00FF7F"),
    'steelblue': HexColor("4682B4"),
    'tan': HexColor("D2B48C"),
    'teal': HexColor("008080"),
    'thistle': HexColor("D8BFD8"),
    'tomato': HexColor("FF6347"),
    'turquoise': HexColor("40E0D0"),
    'violet': HexColor("EE82EE"),
    'wheat': HexColor("F5DEB3"),
    'white': HexColor("FFFFFF"),
    'whitesmoke': HexColor("F5F5F5"),
    'yellow': HexColor("FFFF00"),
    'yellowgreen': HexColor("9ACD32")
}
colorsRGB255 = {
    "aliceblue": RGB255Color(240, 248, 255),
    "antiquewhite": RGB255Color(250, 235, 215),
    "aqua": RGB255Color(0, 255, 255),
    "aquamarine": RGB255Color(127, 255, 212),
    "azure": RGB255Color(240, 255, 255),
    "beige": RGB255Color(245, 245, 220),
    "bisque": RGB255Color(255, 228, 196),
    "black": RGB255Color(0, 0, 0),
    "blanchedalmond": RGB255Color(255, 235, 205),
    "blue": RGB255Color(0, 0, 255),
    "blueviolet": RGB255Color(138, 43, 226),
    "brown": RGB255Color(165, 42, 42),
    "burlywood": RGB255Color(222, 184, 135),
    "cadetblue": RGB255Color(95, 158, 160),
    "chartreuse": RGB255Color(127, 255, 0),
    "chocolate": RGB255Color(210, 105, 30),
    "coral": RGB255Color(255, 127, 80),
    "cornflowerblue": RGB255Color(100, 149, 237),
    "cornsilk": RGB255Color(255, 248, 220),
    "crimson": RGB255Color(220, 20, 60),
    "cyan": RGB255Color(0, 255, 255),
    "darkblue": RGB255Color(0, 0, 139),
    "darkcyan": RGB255Color(0, 139, 139),
    "darkgoldenrod": RGB255Color(184, 134, 11),
    "darkgray": RGB255Color(169, 169, 169),
    "darkgreen": RGB255Color(0, 100, 0),
    "darkgrey": RGB255Color(169, 169, 169),
    "darkkhaki": RGB255Color(189, 183, 107),
    "darkmagenta": RGB255Color(139, 0, 139),
    "darkolivegreen": RGB255Color(85, 107, 47),
    "darkorange": RGB255Color(255, 140, 0),
    "darkorchid": RGB255Color(153, 50, 204),
    "darkred": RGB255Color(139, 0, 0),
    "darksalmon": RGB255Color(233, 150, 122),
    "darkseagreen": RGB255Color(143, 188, 143),
    "darkslateblue": RGB255Color(72, 61, 139),
    "darkslategray": RGB255Color(47, 79, 79),
    "darkslategrey": RGB255Color(47, 79, 79),
    "darkturquoise": RGB255Color(0, 206, 209),
    "darkviolet": RGB255Color(148, 0, 211),
    "deeppink": RGB255Color(255, 20, 147),
    "deepskyblue": RGB255Color(0, 191, 255),
    "dimgray": RGB255Color(105, 105, 105),
    "dimgrey": RGB255Color(105, 105, 105),
    "dodgerblue": RGB255Color(30, 144, 255),
    "firebrick": RGB255Color(178, 34, 34),
    "floralwhite": RGB255Color(255, 250, 240),
    "forestgreen": RGB255Color(34, 139, 34),
    "fuchsia": RGB255Color(255, 0, 255),
    "gainsboro": RGB255Color(220, 220, 220),
    "ghostwhite": RGB255Color(248, 248, 255),
    "gold": RGB255Color(255, 215, 0),
    "goldenrod": RGB255Color(218, 165, 32),
    "gray": RGB255Color(128, 128, 128),
    "grey": RGB255Color(128, 128, 128),
    "green": RGB255Color(0, 128, 0),
    "greenyellow": RGB255Color(173, 255, 47),
    "honeydew": RGB255Color(240, 255, 240),
    "hotpink": RGB255Color(255, 105, 180),
    "indianred": RGB255Color(205, 92, 92),
    "indigo": RGB255Color(75, 0, 130),
    "ivory": RGB255Color(255, 255, 240),
    "khaki": RGB255Color(240, 230, 140),
    "lavender": RGB255Color(230, 230, 250),
    "lavenderblush": RGB255Color(255, 240, 245),
    "lawngreen": RGB255Color(124, 252, 0),
    "lemonchiffon": RGB255Color(255, 250, 205),
    "lightblue": RGB255Color(173, 216, 230),
    "lightcoral": RGB255Color(240, 128, 128),
    "lightcyan": RGB255Color(224, 255, 255),
    "lightgoldenrodyellow": RGB255Color(250, 250, 210),
    "lightgray": RGB255Color(211, 211, 211),
    "lightgreen": RGB255Color(144, 238, 144),
    "lightgrey": RGB255Color(211, 211, 211),
    "lightpink": RGB255Color(255, 182, 193),
    "lightsalmon": RGB255Color(255, 160, 122),
    "lightseagreen": RGB255Color(32, 178, 170),
    "lightskyblue": RGB255Color(135, 206, 250),
    "lightslategray": RGB255Color(119, 136, 153),
    "lightslategrey": RGB255Color(119, 136, 153),
    "lightsteelblue": RGB255Color(176, 196, 222),
    "lightyellow": RGB255Color(255, 255, 224),
    "lime": RGB255Color(0, 255, 0),
    "limegreen": RGB255Color(50, 205, 50),
    "linen": RGB255Color(250, 240, 230),
    "magenta": RGB255Color(255, 0, 255),
    "maroon": RGB255Color(128, 0, 0),
    "mediumaquamarine": RGB255Color(102, 205, 170),
    "mediumblue": RGB255Color(0, 0, 205),
    "mediumorchid": RGB255Color(186, 85, 211),
    "mediumpurple": RGB255Color(147, 112, 219),
    "mediumseagreen": RGB255Color(60, 179, 113),
    "mediumslateblue": RGB255Color(123, 104, 238),
    "mediumspringgreen": RGB255Color(0, 250, 154),
    "mediumturquoise": RGB255Color(72, 209, 204),
    "mediumvioletred": RGB255Color(199, 21, 133),
    "midnightblue": RGB255Color(25, 25, 112),
    "mintcream": RGB255Color(245, 255, 250),
    "mistyrose": RGB255Color(255, 228, 225),
    "moccasin": RGB255Color(255, 228, 181),
    "navajowhite": RGB255Color(255, 222, 173),
    "navy": RGB255Color(0, 0, 128),
    "oldlace": RGB255Color(253, 245, 230),
    "olive": RGB255Color(128, 128, 0),
    "olivedrab": RGB255Color(107, 142, 35),
    "orange": RGB255Color(255, 165, 0),
    "orangered": RGB255Color(255, 69, 0),
    "orchid": RGB255Color(218, 112, 214),
    "palegoldenrod": RGB255Color(238, 232, 170),
    "palegreen": RGB255Color(152, 251, 152),
    "paleturquoise": RGB255Color(175, 238, 238),
    "palevioletred": RGB255Color(219, 112, 147),
    "papayawhip": RGB255Color(255, 239, 213),
    "peachpuff": RGB255Color(255, 218, 185),
    "peru": RGB255Color(205, 133, 63),
    "pink": RGB255Color(255, 192, 203),
    "plum": RGB255Color(221, 160, 221),
    "powderblue": RGB255Color(176, 224, 230),
    "purple": RGB255Color(128, 0, 128),
    "red": RGB255Color(255, 0, 0),
    "rosybrown": RGB255Color(188, 143, 143),
    "royalblue": RGB255Color(65, 105, 225),
    "saddlebrown": RGB255Color(139, 69, 19),
    "salmon": RGB255Color(250, 128, 114),
    "sandybrown": RGB255Color(244, 164, 96),
    "seagreen": RGB255Color(46, 139, 87),
    "seashell": RGB255Color(255, 245, 238),
    "sienna": RGB255Color(160, 82, 45),
    "silver": RGB255Color(192, 192, 192),
    "skyblue": RGB255Color(135, 206, 235),
    "slateblue": RGB255Color(106, 90, 205),
    "slategray": RGB255Color(112, 128, 144),
    "slategrey": RGB255Color(112, 128, 144),
    "snow": RGB255Color(255, 250, 250),
    "springgreen": RGB255Color(0, 255, 127),
    "steelblue": RGB255Color(70, 130, 180),
    "tan": RGB255Color(210, 180, 140),
    "teal": RGB255Color(0, 128, 128),
    "thistle": RGB255Color(216, 191, 216),
    "tomato": RGB255Color(255, 99, 71),
    "turquoise": RGB255Color(64, 224, 208),
    "violet": RGB255Color(238, 130, 238),
    "wheat": RGB255Color(245, 222, 179),
    "white": RGB255Color(255, 255, 255),
    "whitesmoke": RGB255Color(245, 245, 245),
    "yellow": RGB255Color(255, 255, 0),
    "yellowgreen": RGB255Color(154, 205, 50),
}
