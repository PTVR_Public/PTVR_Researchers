"""
Interfaces for creating scenes for the 3D World
"""

from .Utils import Resizable
from .Utils import VirtualPoint
from .Utils import VirtualPoint2D
from .Utils import MetaData
from .Utils import ComplexEncoder
from .Utils import VisualObject
from .Utils import EmptyVisualObject
# from .Utils import InfiniteCircularCone
from .Utils import Button
from .Utils import Screen
from PTVR.LogManager import *
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import PTVR.Pointing.PointingCursor
import PTVR.Tools as tools
import PTVR.Visual
import PTVR.Stimuli.Objects
import random
import json
import numpy as np
import math
import PTVR.Stimuli.Color as color
from PTVR.Stimuli.Texture import Texture
import PTVR.Data.Interaction as Interaction

from PTVR.core.ptvr_event import PTVREvent

import copy


class Scene(MetaData):
    """

    The base class for a scene. An experiment consists of one or several consecutive scenes.

    """

    def __init__(self, trial=-1, background_color=color.RGBColor(0.6, 0.6, 0.6, 1),
                 are_both_hand_controllers_visible=False, is_left_hand_controller_visible=False,
                 is_right_hand_controller_visible=False, is_left_hand_visible=False, is_right_hand_visible=False,
                 skybox="", global_lights_intensity=1, side_view_on_pc=False):
        # the difuse background color
        if (are_both_hand_controllers_visible):
            self.isLeftControllerVisibility = are_both_hand_controllers_visible
            self.isRightControllerVisibility = are_both_hand_controllers_visible
        else:
            self.isLeftControllerVisibility = is_left_hand_controller_visible
            self.isRightControllerVisibility = is_right_hand_controller_visible
        self.isLeftHandVisibility = is_left_hand_visible
        self.isRightHandVisibility = is_right_hand_visible

        self.leftBackgroundColor = background_color
        self.rightBackgroundColor = background_color

        # the contingent pointers collection
        self.interaction = []
        self.pointingCursors = []
        self.id = int(random.random()*1e18)
        self.trial = trial
        self.origin = VirtualPoint()
        # For have event start scene
        begin_scene = event.Timer(event_name="start of scene")
        print_result = callback.PrintResultFileLine(
            callback_name="record start of scene")
        self.AddInteraction(events=[begin_scene], callbacks=[print_result])
        self.originIsSet = False
        self.skybox = skybox
        self.global_lights_intensity = global_lights_intensity
        self.side_view_on_pc = side_view_on_pc
        self.augmented_vision = False

    def set_augmented_vision(self):
        self.augmented_vision = True

    def to_json(self):
        assert False, "to_json should be derived in every subclass"

    def write(self, fp):
        # fp.write(str(self.to_json()))

        fp.write(json.dumps(self.to_json(), sort_keys=True,
                 indent=4, cls=ComplexEncoder))

    def AddInteraction(self, events=[], callbacks=[]):
        self.interaction.append(Interaction.Interaction(
            events=events, callbacks=callbacks))

    def AddPointingCursor(self, pointingCursor):
        print("AddPointingCursor is a Legacy function, please use place_contingent_cursor instead")
        pointingCursor.clean()
        self.pointingCursors.append(copy.deepcopy(pointingCursor))

    def place_contingent_cursor(self, pointingCursor):
        pointingCursor.clean()
        self.pointingCursors.append(copy.deepcopy(pointingCursor))

    def place_pointing_laser(self, pointingCursor):
        pointingCursor.clean()
        self.pointingCursors.append(copy.deepcopy(pointingCursor))


class VisualScene(Scene):
    """

    Parameters
    ----------
    trial :  The default is -1.
    background_color :The default is color.RGBColor(0.6, 0.6, 0.6, 1).

    skybox : "Night", "BrightMorning", "Cloudy", "Afternoon", "Sunset"
        DESCRIPTION. 
        The default is "".

    global_lights_intensity : number from zero to undefined upper limit.
        Six sources of light are placed above the XZ plane to induce
        some shading of the objects in a scene.
        It is not active if 'skybox' is set to one of its possible arguments.
        The default is 1.

    side_view_on_pc : 
        DESCRIPTION. When True, the PTVR window (on your PC) will show you 
        a "side view". 
        This is as if the PTVR window showed the view from a camera looking
        laterally at your subject (at its headset actually). 
        You can rotate this view with your mouse.
        This is convenient for didactic purposes.
        The default is False.

    Returns
    -------
    None.

    """

    def __init__(self, trial=-1, background_color=color.RGBColor(0.6, 0.6, 0.6, 1), are_both_hand_controllers_visible=False,
                 is_left_hand_controller_visible=False, is_right_hand_controller_visible=False,
                 is_left_hand_visible=False,
                 is_right_hand_visible=False, skybox="", global_lights_intensity=1,
                 side_view_on_pc=False, augmented_vision=False):

        Scene.__init__(self, trial, background_color,
                       are_both_hand_controllers_visible=are_both_hand_controllers_visible,
                       is_left_hand_controller_visible=is_left_hand_controller_visible,
                       is_right_hand_controller_visible=is_right_hand_controller_visible,
                       is_left_hand_visible=is_left_hand_visible, is_right_hand_visible=is_right_hand_visible,
                       skybox=skybox, global_lights_intensity=global_lights_intensity, side_view_on_pc=side_view_on_pc)

        # the collection of any type of visual objects in the scene
        self.objects = []
        self.UIobjects = []
        # identifier for serialization
        self.type = "VISUAL_SCENE"
        # callbacks to execute when the visual scene is loaded
        self.on_loaded_event = PTVREvent()
        self.augmented_vision = augmented_vision

    def place(self, visual_object, the_3D_world):
        """
        Place object in the scene 
        you need to indicate which object in which 3D World.
        """
        visual_object.current_coordinate_system = the_3D_world.coordinate_system
        # if(len(PTVR.Experiment.Experiment.coordinate_system)>0):
        #     o.current_coordinate_system_position = (PTVR.Experiment.Experiment.coordinate_system[-1]).local_position

        #     if(len(PTVR.Experiment.Experiment.coordinate_system[-1].local_rotation_in_current_CS_sequence)):
        #         o.current_coordinate_system_rotation_in_current_CS = (PTVR.Experiment.Experiment.coordinate_system[-1]).local_rotation_in_current_CS_sequence[-1]

        if type(visual_object) == list:
            for i in visual_object:
                self.place(i)
            return

        # if self.current_coordinate_system_id != (PTVR.Experiment.Experiment.coordinate_system[-1]).id:
        #    self.objects.append(copy.deepcopy(PTVR.Experiment.Experiment.coordinate_system[-1]))
        #    self.current_coordinate_system_id = (PTVR.Experiment.Experiment.coordinate_system[-1]).id

        # if o.parentId == -1:
        #    o.set_parent(PTVR.Experiment.Experiment.coordinate_system[-1])

        # bon, tout est en place, faut juste tester et vérifier avec le bordel des screens
        self.objects.append(copy.deepcopy(visual_object))

    def placeUI(self, o):
        self.UIobjects.append(copy.deepcopy(o))

    def _pickle_object(self, o, pos, rot):
        # return json.dumps({ "object" : o.to_json(),
        #                    "position_in_current_CS" : pos.tolist(),
        #                   "orientation" : rot.tolist() })
        return dict(obj=o, position_in_current_CS=pos, orientation=rot)

    def to_json(self):
        new_dict = dict(self.__dict__)
        del new_dict["on_loaded_event"]
        new_dict["onLoadedEventUUID"] = str(self.on_loaded_event.uuid)
        return new_dict
        # return dict(self.__dict__) #keep old one to remember. Does this works right?


class CheckScene(VisualScene):
    """
    This creates a scene whose main goal is to check that everything is OK before
    starting an experiment. This scene is usually the first scene of an experiment.
    You can notably visually check whether the XYZ gizmo is correctly oriented and positioned (namely at your feet and with Z pointing straight ahead).
    From this scene, it is only possible to go to the next scene if head and hand pointing are OK.
    """

    def __init__(self, my_world, trial=-1, activation_area_radius_deg=10, number_of_edge_polygon=8, number_polygone=12, fixation_point_color=color.RGBColor(r=1.0), background_color=color.RGBColor(0.6, 0.6, 0.6, 1), are_both_hand_controllers_visible=True, is_left_hand_controller_visible=False, is_right_hand_controller_visible=False, is_left_hand_visible=False, is_right_hand_visible=False, get_axis=False, distance_xp=500, text_information="This scene checks if everything (notably room-calibration, head and hand pointing) are ok", height_of_text_above_horizon_deg=10, tangent_screen_is_visible=True, feedback_visual_is_visible=False):
        VisualScene.__init__(self, trial, background_color, are_both_hand_controllers_visible=are_both_hand_controllers_visible, is_left_hand_controller_visible=is_left_hand_controller_visible,
                             is_right_hand_controller_visible=is_right_hand_controller_visible, is_left_hand_visible=is_left_hand_visible, is_right_hand_visible=is_right_hand_visible, side_view_on_pc=False)
        # foot TODO: profondeur = Default
        foot_left = PTVR.Stimuli.Objects.Plane(position_in_current_CS=np.array([-0.15, 0.0, 0.0]), rotation_in_current_CS=np.array([
                                               0.0, 0.0, 0.0]), size_in_meters=np.array([0.02, 0.02, 0.06]), color=color.RGBColor())
        foot_right = PTVR.Stimuli.Objects.Plane(position_in_current_CS=np.array([0.15, 0.0, 0.0]), rotation_in_current_CS=np.array([
                                                0.0, 0.0, 0.0]), size_in_meters=np.array([0.02, 0.02, 0.06]), color=color.RGBColor())
        self.place(foot_left, my_world)
        self.place(foot_right, my_world)
        # position RELATIVE par rapport
        fixationPointPos = np.array([0,  0, distance_xp])
        pointSizeDegree = 0.5
        pointSize = self.angToHeight(pointSizeDegree, distance_xp)

        fixationPoint = PTVR.Stimuli.Objects.Sphere(
            position_in_current_CS=fixationPointPos, size_in_meters=pointSize*2, color=fixation_point_color)
        self.place(fixationPoint, my_world)
        # Text
        angle = self.angToHeight(height_of_text_above_horizon_deg, distance_xp)
        textPos = np.array([0, angle, distance_xp])
        textCalibration = PTVR.Stimuli.Objects.Text(
            text=text_information, position_in_current_CS=textPos, visual_angle_of_centered_x_height_deg=1)
        self.place(textCalibration, my_world)

        click = event.HandController(valid_responses=['right_trigger', 'left_trigger'],
                                     event_name="check_scene_hand_controller")

        pointed_in = event.PointedAt(target_id=fixationPoint.id,
                                     activation_cone_radius_deg=activation_area_radius_deg,
                                     event_name="check_scene_pointing_on",
                                     mode="press")
        pointed_out = event.PointedAt(target_id=fixationPoint.id,
                                      activation_cone_radius_deg=activation_area_radius_deg,
                                      event_name="check_scene_pointing_off",
                                      mode="release")

        launch_experiment = callback.EndCurrentScene(
            callback_name="end calibration_scene")
        verification_zone_in = callback.AddInteractionPassedToCallback(
            events=[click], callbacks=[launch_experiment], effect="activate")
        verification_zone_out = callback.AddInteractionPassedToCallback(
            events=[click], callbacks=[launch_experiment], effect="deactivate")

        # callback ChangeBackgroundColor
        if (feedback_visual_is_visible):
            change_color = callback.ChangeBackgroundColor(
                effect="activate", evt_color=color.RGBColor(0.8, 0.6, 0.6, 1))
            default_color = callback.ChangeBackgroundColor(effect="deactivate")

            self.AddInteraction(events=[pointed_in], callbacks=[
                                default_color, verification_zone_in])
            self.AddInteraction(events=[pointed_out], callbacks=[
                                change_color, verification_zone_out])
        else:
            self.AddInteraction(events=[pointed_in], callbacks=[
                                verification_zone_in])
            self.AddInteraction(events=[pointed_out], callbacks=[
                                verification_zone_out])
        # Put axis
        gizmo = PTVR.Stimuli.Objects.Gizmo(
            position_in_current_CS=np.array([0.0, 0.0, 0.0]), length=1.0)
        self.place(gizmo, my_world)
        exterior_angle = 360/number_of_edge_polygon
        for i in range(1, number_polygone+1):
            polygone_size = pow(
                (1 + (i-1)*(math.sqrt(distance_xp)-1)/(number_polygone-1)), 2)
            p_a = np.array([0, 0, polygone_size])
            self.create_polygon(my_world, p_a, number_of_edge_polygon, polygone_size, color.RGBColor(
                r=0.5-i/40, g=0.5-i/40, b=0.5-i/40), exterior_angle)
        p_a = np.array([0, 0, 1])
        for j in range(0, number_of_edge_polygon+1):
            p_b = p_a * (number_polygone*number_polygone)
            self.create_perspective_line(
                my_world, p_a, p_b, color.RGBColor(r=0.5, g=0.5, b=0.5))
            p_a = tools.rotate_about_vector(u_vec=p_a, v_vec=np.array(
                [0.0, 1.0, 0.0]), theta_deg=exterior_angle)

        # identifier for serialization
        self.type = "VISUAL_SCENE"

    def create_polygon(self, my_world, p_a, number_of_edges, circumradius, choseen_color, exterior_angle):
        for i in range(0, number_of_edges):
            p_b = tools.rotate_about_vector(u_vec=p_a, v_vec=np.array(
                [0.0, 1.0, 0.0]), theta_deg=exterior_angle)
            line = PTVR.Stimuli.Objects.LineTool(
                start_point=p_a, end_point=p_b, width=0.05, color=choseen_color)
            self.place(line, my_world)
            p_a = p_b

    def create_perspective_line(self, my_world, p_a, p_b, choseen_color):
        line = PTVR.Stimuli.Objects.LineTool(
            start_point=p_a, end_point=p_b, width=0.05, color=choseen_color)
        self.place(line, my_world)

    def angToHeight(self, angle, distanceXP):  # EC: 11 nov enlevé le " distanceXP = 500"
        return distanceXP*np.tan(angle/180*np.pi)

    # def place(self, o):
    #    self.objects.append(copy.deepcopy(o))

    def placeUI(self, o):
        self.UIobjects.append(copy.deepcopy(o))

    def _pickle_object(self, o, pos, rot):
        # return json.dumps({ "object" : o.to_json(),
        #                    "position_in_current_CS" : pos.tolist(),
        #                   "orientation" : rot.tolist() })
        return dict(obj=o, position_in_current_CS=pos, orientation=rot)

    def to_json(self):
        # keep old one to remember. Does this works right?
        return dict(self.__dict__)


class SimplifiedTimerScene(VisualScene):
    """
    Interface for generating a Visual scene.
    A visual scene can be populated with visual objects. A trial can be made either of one single scene or of a succession of scenes.
    """

    def __init__(self, trial=1, display=event.Timer(),
                 scene_duration_in_ms=1000, background_color=color.RGBColor(0.6, 0.6, 0.6, 1), are_both_hand_controllers_visible=False,
                 is_left_hand_controller_visible=False, is_right_hand_controller_visible=False, is_left_hand_visible=False,
                 is_right_hand_visible=False, skybox="", global_lights_intensity=1):

        VisualScene.__init__(self, trial, background_color,
                             are_both_hand_controllers_visible=are_both_hand_controllers_visible,
                             is_left_hand_controller_visible=is_left_hand_controller_visible,
                             is_right_hand_controller_visible=is_right_hand_controller_visible,
                             is_left_hand_visible=is_left_hand_visible, is_right_hand_visible=is_right_hand_visible,
                             skybox=skybox, global_lights_intensity=global_lights_intensity)

        # the collection of any type of visual objects in the scene
        self.sceneDurationInMs = scene_duration_in_ms
        self.idNextScene = self.id+1

        # identifier for serialization
        self.eventTimer = event.Timer(delay_in_ms=self.sceneDurationInMs)
        self.callbackEndCurrentScene = callback.EndCurrentScene()
        self.type = "VISUAL_SCENE"

    def SetDuration(self, new_duration_scene_in_ms):
        self.durationSceneInMs = new_duration_scene_in_ms
        self.eventTimer.delayInMs = self.sceneDurationInMs

    def SetNextScene(self, id_next_scene):
        self.idNextScene = id_next_scene
        self.callbackEndCurrentScene.SetNextScene(self.idNextScene)

    def to_json(self):
        self.AddInteraction(events=[self.eventTimer], callbacks=[
                            self.callbackEndCurrentScene])
        # keep old one to remember. Does this works right?
        return dict(self.__dict__)
