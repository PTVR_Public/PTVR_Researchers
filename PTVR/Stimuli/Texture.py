"""
Interfaces for textures. A texture is basically a 'skin' to a visual object and consists of a shader
and a corresponding color/image
"""

import PTVR.Stimuli.Color as color
import PTVR.Stimuli.Utils as utils

class Texture(utils.MetaData):
    """
    A Texture class has three members- one, a shader that defines
    """
    def __init__(self, shader='diffuse', color=color.RGBColor(a=1.0), img=''):
        shaders = frozenset(['diffuse', 'unlit'])
        if shader in shaders:
            self.shader = shader # shader can be diffuse or unlit
        else:
            assert False, "Unknown shader %s (allowed shaders: %s)" % (shader, shaders)
        self.color = color
        self.img = img

    def to_json(self):
        return dict(shader=self.shader, color=self.color, img=self.img)
