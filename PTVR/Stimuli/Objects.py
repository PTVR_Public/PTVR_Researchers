"""
Interfaces for creating objects for the scenes of the 3D World
"""
from PIL import Image
import copy
import os
import PTVR.Data.Interaction as Interaction
from PTVR.Stimuli.Texture import Texture
import PTVR.Stimuli.Color as color
import numpy as np
import json
import random
import sys
import PTVR.SystemUtils
import PTVR.Visual
import PTVR.Tools as tools
import PTVR.Pointing.PointingCursor
from PTVR.LogManager import *
from .Utils import Screen
from .Utils import EmptyVisualObject
from .Utils import VisualObject
from .Utils import ComplexEncoder
from .Utils import MetaData
from .Utils import VirtualPoint2D
from .Utils import VirtualPoint
from .Utils import Resizable
# import warnings
# warnings.filterwarnings("ignore")
# from .Utils import InfiniteCircularCone
# from .Utils import Button


class Texture(MetaData):
    """
    A Texture class has three members- one, a shader that defines
    """

    def __init__(self, shader='diffuse', color=color.RGBColor(a=1.0), img=''):
        shaders = frozenset(['diffuse', 'unlit'])
        if shader in shaders:
            self.shader = shader  # shader can be diffuse or unlit
        else:
            assert False, "Unknown shader %s (allowed shaders: %s)" % (
                shader, shaders)
        self.color = color
        self.img = img

    def to_json(self):
        return dict(shader=self.shader, color=self.color, img=self.img)


diffuse_texture = Texture()


class ColoratedObject(MetaData):
    """
    for objects containing colors attributes
    """

    def __init__(self, is_visible=True, color=color.RGBColor(r=0.0, g=0.0, b=0.0, a=1.0), texture=diffuse_texture):

        # the kind of texture, can be diffuse only for now.
        self.isVisible = is_visible
        self.color = color
        self.texture = texture

    def set_color(self, color=color.RGBColor(r=0.0, g=0.0, b=0.0, a=1.0)):
        self.color = color


class CameraPTVR(VisualObject):
    """
    """

    def __init__(self, position_in_current_CS=np.array([0.0, 0.0, 0.0]), rotation_in_current_CS=np.array([0, 0, 0]), background_color=color.RGBColor(0.6, 0.6, 0.6, 1), field_of_view=20, is_active=True, is_visible=True):
        VisualObject.__init__(self, is_active, position_in_current_CS,
                              rotation_in_current_CS, 1, is_visible=is_visible)
        self.backgroundColor = background_color
        self.pastRendererToMaterial = []
        self.pastShotToMaterial = []
        self.type = "cameraPTVR"
        self.fov = field_of_view

    def set_renderer_to_material(self, object_id=-1):
        self.pastRendererToMaterial.append(copy.deepcopy((object_id)))

    def set_photo_to_material(self, object_id=-1):
        self.pastShotToMaterial.append(copy.deepcopy((object_id)))


class FlatScreen(VisualObject, ColoratedObject):
    def __init__(self, is_active=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0]),
                 texture=diffuse_texture,
                 color=color.RGBColor(r=0.0, g=0.0, b=0.0, a=1.0),
                 output_gaze=False,
                 output_headset=False,
                 is_visible=True
                 ):

        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)

        ColoratedObject.__init__(
            self, is_visible, color=color, texture=texture)

        self.isGetDataEyes = output_gaze
        self.isGetDataHead = output_headset
        self.type = "flat_screen"

        self.filenamePrefix = self.type+"_"+str(self.id)

    def ChangeName(self, filenamePrefix="screen"):
        self.filenamePrefix = filenamePrefix

    def GetEllipseChordEndpointsFromConeAngle(self, angleAtApex,
                                              OrientationOfEllipseChord,
                                              CAPOC,
                                              apexPosition=VirtualPoint()):
        infiniteCone = InfiniteCircularCone(
            angleAtApex=angleAtApex, apexPosition=apexPosition, CAPOC=CAPOC)
        return infiniteCone.GetEllipseChordEndpointsFromConeAngle(OrientationOfEllipseChord=OrientationOfEllipseChord,
                                                                  onThisScreen=self)

    def _place_object_on_screen_in_cartesian_coordinates(self, objToPlace, x_local=0.0, y_local=0.0, z_local=0.0):
        """
        Permits to place an object on the screen with cartesian coordinates.

        Parameters
        ----------
        objToPlace : VisualObject children (cube, sphere etc.)
            should be a visualobject such as a cube, sphere etc.
        x : float, optional
            the local x position on screen. The default is 0.
        y : float, optional
            the local y position on screen. The default is 0.
        z : float, optional
            the local z position on screen. The default is 0.
        Returns
        -------
        None.

        """
        objToPlace.set_parent(self, x_local, y_local, z_local)
        # objToPlace.position_in_current_CS[0]=-objToPlace.position_in_current_CS[0]


class TangentScreen(FlatScreen):
    """
    See documentation
    """

    def __init__(self, is_active=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0]),
                 texture=diffuse_texture,
                 color=color.RGBColor(r=0.0, g=0.0, b=0.0, a=1.0),
                 output_gaze=False,
                 output_headset=False,
                 is_visible=True
                 ):
        """ Initialization with parameters
        @param getData if True create file of result
        """
        FlatScreen.__init__(self, is_active, position_in_current_CS,
                            rotation_in_current_CS,
                            size_in_meters,
                            texture,
                            color,
                            output_gaze,
                            output_headset,
                            is_visible)
        self.__SetTangentRotation()
        self.has_object_place_on_it = False

        if (output_headset):
            PTVR.LogManager.SetError(
                "The output_headset parameter is no longer used in TangentScreen. Please use this parameter in The3DWorld")
            sys.exit()

    def __SetTangentRotation(self):
        self.type = "tangent_screen"
        self.filenamePrefix = self.type+str(self.id)
        PTVR.LogManager.SetWarning(
            "You have created a tangent screen: note that il will not be possible to rotate this Tangent Screen any longer.")
        # - 4 is current_CS
        self.rotate_to_look_at_in_opposite_direction(
            id_object_to_lookAt_in_opposite_direction=-4)
        self.get_position_tangent_screen_origin_in_global_cartesian_coordinates()

    def get_position_tangent_screen_origin_in_global_cartesian_coordinates(self):
        """
        Returns Screen_origin in global (np.array([x,y,z])) returns the position in GLOBAL coordinates of the TS
        -------
        TYPE


        """
        screen_x, screen_y, screen_z = self.get_position()

        self.screen_origin = np.array([screen_x, screen_y, screen_z])
        return self.screen_origin

    def get_axes(self):
        # Position O' centre tangent_screen
        self.screen_origin = self.get_position_tangent_screen_origin_in_global_cartesian_coordinates()
        # P
        current_cs_pos_x, current_cs_pos_y, current_cs_pos_z, current_cs_axis_x, current_cs_axis_y, current_cs_axis_z = self.current_coordinate_system.GetCoordinates()

        # Position Oc origin du current_cs
        self.current_cs_origin = np.array(
            [current_cs_pos_x, current_cs_pos_y, current_cs_pos_z])

        # Definition of local axis of tangent screen
        self.current_cs_to_screen_origin = tools.points_to_vec(
            point_a=self.current_cs_origin, point_b=self.screen_origin)
        self.Z_screen_vector = tools.normalize(
            self.current_cs_to_screen_origin)

        # WARNING Probleme axe X ? rend [-1.0.0]
        self.X_screen_vector = tools.normalize(tools.vector_product(u=self.Z_screen_vector, v=np.array(
            [0.0, 1.0, 0.0])))  # np.array([0,1,0]) correspond to global_axis_y
        self.Y_screen_vector = tools.vector_product(
            u=self.X_screen_vector, v=self.Z_screen_vector)
        return self.X_screen_vector,  self.Y_screen_vector, self.Z_screen_vector

    def get_position_on_screen_in_cartesian_coordinates_from_perimetric_coordinates(self, eccentricity_deg=0.0, half_meridian_deg=0.0, distance_from_screen=0.0):
        # Conversion of angles
        eccentricity_rad = tools.deg_to_rad(eccentricity_deg)
        half_meridian_rad = tools.deg_to_rad(half_meridian_deg)
        self.get_axes()
        # Prise en compte d'un affichage devant l'écran ( O' tilde)
        origin_of_display = self.screen_origin - \
            distance_from_screen * self.Z_screen_vector

        # Calcul de rho tilde
        radial_coordinates = tools.distance_between_two_points(
            point_a=self.current_cs_origin, point_b=origin_of_display) * np.tan(eccentricity_rad)

        # Calcul de la position de l'objet P
        obj_pos = origin_of_display + radial_coordinates * \
            np.cos(half_meridian_rad) * self.X_screen_vector + \
            radial_coordinates * \
            np.sin(half_meridian_rad) * self.Y_screen_vector

        # TODO: Stock obj_pos dans les coordonnées global de l'objet
        # Placement Unity Obj
        obj_pos_local = np.array([radial_coordinates*np.cos(half_meridian_rad),
                                  radial_coordinates*np.sin(half_meridian_rad),
                                  -distance_from_screen])
        return obj_pos_local

    def _place_object_on_screen_in_perimetric_coordinates(self, objToPlace, eccentricity_deg=0.0, half_meridian_deg=0.0, distance_from_screen=0.0, coordinate_system="perimetric"):
        if coordinate_system != "perimetric":
            Log.SetWarning(
                "set_spherical_coordinates_on_screen: only perimetric coordinates are implemented yet.")
        elif (eccentricity_deg < 0 or eccentricity_deg >= 90):
            PTVR.LogManager.SetWarning(
                "Your object will not be placed on tangent screen.\n To place an object on a tangent_screen in perimetric coordinates, eccentricity must be less than 90 degrees.")
        else:
            x, y, z = self.get_position()
            if (x != self.screen_origin[0]):
                self.get_axes()
            elif (y != self.screen_origin[1]):
                self.get_axes()
            elif (z != self.screen_origin[2]):
                self.get_axes()
            if (self.has_object_place_on_it == False):
                self.has_object_place_on_it = True
            obj_pos_local = self.get_position_on_screen_in_cartesian_coordinates_from_perimetric_coordinates(
                eccentricity_deg, half_meridian_deg, distance_from_screen)

            objToPlace.set_parent(
                parent=self, x_local=obj_pos_local[0], y_local=obj_pos_local[1], z_local=obj_pos_local[2])

    def create_and_place_visual_representation_of_a_segment_on_a_tangent_screen(self, my_world, my_scene,
                                                                                start_point_color, start_point_local,
                                                                                end_point_color, end_point_local,
                                                                                segment_main_color, segment_main_width):
        segment_main = PTVR.Stimuli.Objects.LineTool(start_point=start_point_local,
                                                     end_point=end_point_local,
                                                     color=segment_main_color,
                                                     width=segment_main_width)
        segment_main.set_parent(parent=self,
                                x_local=start_point_local[0],
                                y_local=start_point_local[1],
                                z_local=start_point_local[2])
        my_scene.place(segment_main, my_world)

        sphere_1 = PTVR.Stimuli.Objects.Sphere(color=start_point_color, size_in_meters=np.array(
            [segment_main_width, segment_main_width, segment_main_width]))
        sphere_1.set_cartesian_coordinates_on_screen(my_2D_screen=self,
                                                     x_local=start_point_local[0],
                                                     y_local=start_point_local[1],
                                                     z_local=start_point_local[2])
        my_scene.place(sphere_1, my_world)

        sphere_2 = PTVR.Stimuli.Objects.Sphere(color=end_point_color, size_in_meters=np.array(
            [segment_main_width, segment_main_width, segment_main_width]))
        sphere_2.set_cartesian_coordinates_on_screen(my_2D_screen=self,
                                                     x_local=end_point_local[0],
                                                     y_local=end_point_local[1],
                                                     z_local=end_point_local[2])
        my_scene.place(sphere_2, my_world)
        return my_scene

    def create_segment_on_tangent_screen_at_capoc_in_local_cartesian(self, capoc_local=np.array([0.0, 0.0, 0.0]), segment_size_deg=10.0, segment_orientation_value_deg=0.0):
        """

        Parameters
        ----------
        capoc_local : TYPE, optional
            DESCRIPTION. The default is np.array([0.0,0.0,0.0]).
        segment_size_deg : TYPE, optional
            DESCRIPTION. The default is 10.0.
        segment_orientation_value_deg : Dans cette fonction, l'orientation donnée doit toujours etre l'orientation reelle que l'on veut avoir sur l'écrsn
            DESCRIPTION. The default is 0.0.

        Returns
        -------
        point_1_local : TYPE
            DESCRIPTION.
        point_2_local : TYPE
            DESCRIPTION.

        """

        segment_orientation_deg = segment_orientation_value_deg

        self.get_axes()
        segment_size_rad = tools.deg_to_rad(segment_size_deg)

        # capoc est ici en coordonnees globales
        capoc = self.screen_origin + capoc_local[0] * self.X_screen_vector + \
            capoc_local[1] * self.Y_screen_vector + \
            capoc_local[2] * self.Z_screen_vector
        ok_vec = tools.points_to_vec(
            point_a=self.current_cs_origin, point_b=capoc)

        # calcul g
        g_vec = tools.normalize(tools.points_to_vec(
            point_a=self.current_cs_origin, point_b=capoc))

        # calcul t
        t_vec = tools.rotate_about_vector(
            self.X_screen_vector, self.Z_screen_vector,  segment_orientation_deg)

        a = np.square(tools.scalar_product(u=t_vec, v=g_vec)) - \
            np.square(np.cos(segment_size_rad/2.0))

        b = 2.0 * tools.scalar_product(u=ok_vec, v=g_vec) * tools.scalar_product(u=t_vec, v=g_vec) - \
            2.0 * tools.scalar_product(u=ok_vec, v=t_vec) * \
            np.square(np.cos(segment_size_rad/2))

        c = np.square(tools.scalar_product(u=ok_vec, v=g_vec)) - \
            np.square(tools.norm(ok_vec)) * \
            np.square(np.cos(segment_size_rad/2.0))

        delta = np.square(b) - 4.0 * a * c

        beta_1 = (-b - np.sqrt(delta)) / (2.0 * a)
        beta_2 = (-b + np.sqrt(delta)) / (2.0 * a)

        # WARNING Point Local prend en compte parent pas besoin utilise capoc_local instead of capoc
        point_1_local = np.array([capoc_local[0] + beta_1 * tools.scalar_product(u=t_vec, v=self.X_screen_vector),
                                 capoc_local[1] + beta_1 * tools.scalar_product(u=t_vec, v=self.Y_screen_vector), 0])
        point_2_local = np.array([capoc_local[0] + beta_2 * tools.scalar_product(u=t_vec, v=self.X_screen_vector),
                                 capoc_local[1] + beta_2 * tools.scalar_product(u=t_vec, v=self.Y_screen_vector), 0])
        print("     point alpha_local : x = " + str(point_1_local[0]) + ", y = " + str(
            point_1_local[1]) + " , z = "+str(point_1_local[2]))
        print("     point beta_local  : x = " + str(point_2_local[0]) + ", y = " + str(
            point_2_local[1]) + " , z = "+str(point_2_local[2]))
        print(
            "     [Exit create_segment_on_tangent_screen_at_capoc_in_local_cartesian]")

        return point_1_local, point_2_local

    def create_segment_on_tangent_screen_at_capoc_in_perimetric(self, capoc_eccentricity_deg=0.0, capoc_half_meridian_deg=0.0, segment_size_deg=10.0, segment_orientation_value_deg=0.0):
        self.get_axes()
        # capoc_local is in cartesian coordinates
        capoc_local = self.get_capoc_position_local(
            capoc_eccentricity_deg=capoc_eccentricity_deg, capoc_half_meridian_deg=capoc_half_meridian_deg)
        return self.create_segment_on_tangent_screen_at_capoc_in_local_cartesian(capoc_local=capoc_local, segment_size_deg=segment_size_deg, segment_orientation_value_deg=segment_orientation_value_deg)

    def get_capoc_position_global(self, capoc_eccentricity_deg=0.0, capoc_half_meridian_deg=0.0):
        capoc_local = self.get_capoc_position_local(
            capoc_eccentricity_deg=capoc_eccentricity_deg, capoc_half_meridian_deg=capoc_half_meridian_deg)
        capoc_global = tools.relative_to_global_cartesian_coordinates(point_in_relative_cs=capoc_local, relative_cs_origin=self.screen_origin,
                                                                      relative_cs_axis_X=self.X_screen_vector, relative_cs_axis_Y=self.Y_screen_vector, relative_cs_axis_Z=self.Z_screen_vector)
        return capoc_global

    def get_capoc_position_local(self, capoc_eccentricity_deg=0.0, capoc_half_meridian_deg=0.0):
        capoc_local = self.get_position_on_screen_in_cartesian_coordinates_from_perimetric_coordinates(
            capoc_eccentricity_deg, capoc_half_meridian_deg, 0.0)
        return capoc_local


class Calculator(VisualObject):
    """
    An object that can be created to calculate various values such as angles
    distances etc. The result can be used by other GO
    """

    def __init__(self):
        super().__init__()
        self.calculation = ""
        self.idsForCalculation = []
        self.type = "calculator"

    def StringIntegration(self):
        return "%"+str(self.id)

    def AngleAtOriginBetweenTwoVisualObjects(self, visualObject1, visualObject2):
        self.calculation = "AngleAtOriginBetweenTwoVisualObjects"
        self.idsForCalculation.append(visualObject1.id)
        self.idsForCalculation.append(visualObject2.id)

    def RadialDistanceFromOrigin(self, visualObject):
        self.calculation = "RadialDistanceFromOrigin"
        self.idsForCalculation.append(visualObject.id)

    def AngleAtHeadPOVBetweenTwoVisualObjects(self, visualObject1, visualObject2):
        self.calculation = "AngleAtHeadPOVBetweenTwoVisualObjects"
        self.idsForCalculation.append(visualObject1.id)
        self.idsForCalculation.append(visualObject2.id)

    def RadialDistanceFromHeadPOV(self, visualObject):
        self.calculation = "RadialDistanceFromHeadPOV"
        self.idsForCalculation.append(visualObject.id)

    def DistanceBetweenTwoVisualObjects(self, visualObject1, visualObject2):
        self.calculation = "DistanceBetweenTwoVisualObjects"
        self.idsForCalculation.append(visualObject1.id)
        self.idsForCalculation.append(visualObject2.id)

    def AngleAtApexBetweenTwoVisualObjects(self, apex, visualObject1, visualObject2):
        self.calculation = "AngleAtApexBetweenTwoVisualObjects"
        self.idsForCalculation.append(apex.id)
        self.idsForCalculation.append(visualObject1.id)
        self.idsForCalculation.append(visualObject2.id)


class AudioSource(VisualObject):
    """
    place an audio somewhere in the world
    """

    def __init__(self,
                 position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 volume=1.,
                 audioFile="",
                 is_active=True,
                 is_playing_directly=True,
                 is_a_loop=False,
                 keep_audio_source_across_scenes=False
                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS)
        # the sounds volume between 0 and 1
        self.volume = volume
        # the full audiofile path
        self.audioFile = audioFile
        # identifier for serialization
        self.isLoop = is_a_loop
        self.isPlayDirectly = is_playing_directly
        self.keep_audio_source_across_scenes = keep_audio_source_across_scenes
        self.type = "audio_source"


class Text(VisualObject, ColoratedObject):
    """
    Parameters
    ----------
    text : text to be displayed
    font_name : name of the font you want to use for the text.
        If you want to add new fonts, they must be placed in:
            ...> PTVR_Researchers > PTVR_Operators > resources > Fonts.
        They have to be in a .ttf format.
    fontsize_in_postscript_points : size of the text.

    horizontal_alignment :
        Left, Right, Center, Justified, Flush.

    vertical_alignment :
        Middle : middle of ALL the letters of the font.
        baseline : base line is the line on which the bottom of letters
                    like "a", "o", "e", "u" lie.
        midline : ("geometry" in Unity) middle of the letters present in the string.

    visual_angle_of_centered_x_height_deg x-height : in degrees
    """

    def __init__(self, is_active=True, is_visible=True, text="Edit the text parameter",
                 font_name='Arial',
                 is_bold=False,
                 is_italic=False,
                 horizontal_alignment="Center",
                 vertical_alignment="baseline",  # "Middle",
                 fontsize_in_postscript_points=20,
                 visual_angle_of_centered_x_height_deg=0,
                 color=color.RGBColor(r=0, g=0, b=0, a=1),
                 texture=diffuse_texture,
                 position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 use_collider=False

                 ):

        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, 1)
        ColoratedObject.__init__(
            self, is_visible, color=color, texture=texture)

        vertical_alignments = ["baseline", "midline",
                               "Middle", "middle", "Baseline", "Midline"]

        self.text = text
        self.isBold = is_bold
        self.isItalic = is_italic
        self.isTextStatic = False
        # Left, Right, Center, Justified, Flush
        self.alignHorizontal = horizontal_alignment

        # Middle, Capline, Baseline
        if vertical_alignment not in vertical_alignments:
            print("Warning ! The parameter vertical_alignment = '%s' is not valid" %
                  vertical_alignment)
            sys.exit()
        self.alignVertical = vertical_alignment

        self.fontName = font_name
        # The given font size is scaled to fits the PostScript Point standard
        self.fontSize = fontsize_in_postscript_points * 0.005
        self.textboxScale = np.array([1.0, 1.0])

        # visual angle in degrees
        self.visualAngleOfCenteredXHeight = visual_angle_of_centered_x_height_deg
        self.isWrapping = False

        # identifier for serialization
        self.type = "text"
        self.use_collider = use_collider

    def SetMnreadFormat(self, x_height_in_meters):
        """

        Method used to display the text using Mnread's standards. To use this method, the font size has to be expressed in degrees (see @method SetXheightAngularSize())
        Note that the the box width of the text is equal to 17.32 x-heights (New specification, Mansfield et al., 2019).

        """
        if (self.visualAngleOfCenteredXHeight):
            self.textboxScale = np.array([(17.32 * x_height_in_meters), 1.0])
            self.alignHorizontal = "Flush"
            self.isWrapping = True

        else:
            SetWarning("The MnreadFormat can only be set if the text's size is expressed in degrees. Use SetXheightAngularSize(visual_angle_of_centered_x_height_deg) to do so. \n")

    def SetTextBoxParameters(self, textbox_width_in_meters, textbox_height_in_meters, is_wrapping=False):
        """

        Method used to set the size of the box in wich the text is displayed. isWrapping allows the text to go back to line once the border of the box is reached.

        @param textboxWidth_m : width of the textbox in meters
        @param textboxHeight_m : height of the textbox in meters
        @param isWrapping : boolean allowing or not the phrase to go back to line

        """
        self.textboxScale = np.array(
            [textbox_width_in_meters, textbox_height_in_meters])
        self.isWrapping = is_wrapping

    def SetText(self, text):
        """

        Method used to indicate the text to be displayed

        @param text : text to be displayed

        """
        self.text = text

    def SetXheightAngularSize(self, visual_angle_of_centered_x_height_deg):
        """

        Method used to express the font size in degrees.
        @param visual_angle_of_centered_x_height_deg : visual angle in degrees

        """
        self.visualAngleOfCenteredXHeight = visual_angle_of_centered_x_height_deg


class Plane(VisualObject, ColoratedObject):
    """
    A plane that can be placed in a visual scene. Scale is the side in meters.
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([-90, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 color=color.RGBColor(),
                 texture=diffuse_texture
                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS,  size_in_meters)
        ColoratedObject.__init__(self, is_visible, color, texture)
        # identifier for serialization
        self.type = "plane"

    def __str__(self):
        return "Plane(side=%s,color=%s)" % (self.scale, self.color)


class Cube(VisualObject, ColoratedObject):
    """
    Size_in_meters is the side's size.
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 color=color.RGBColor(),
                 texture=diffuse_texture,
                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)
        ColoratedObject.__init__(self, is_visible, color, texture)
        # identifier for serialization
        self.type = "cube"

    def __str__(self):
        return "Cube(side=%s,color=%s)" % (self.scale, self.color)


class Ring(VisualObject, ColoratedObject):
    """
    Size_in_meters is the side's size.
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 color=color.RGBColor(),
                 texture=diffuse_texture,
                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)
        ColoratedObject.__init__(self, is_visible, color, texture)
        # identifier for serialization
        self.type = "ring"

    def __str__(self):
        return "Cube(side=%s,color=%s)" % (self.scale, self.color)


class Light(VisualObject):
    """
    A light that can be placed in a visual scene.
    """

    def __init__(self, is_active=True,
                 position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 intensity=1.0,
                 lightRange=10.0,
                 spotAngle=45.0
                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)

        self.intensity = intensity
        self.lightRange = lightRange
        self.spotAngle = spotAngle
        # identifier for serialization
        self.type = "light"

    def __str__(self):
        return "Cube(side=%s,color=%s)" % (self.scale, self.color)


class Button(VisualObject):
    """
    A Button that can be placed in a visual scene.
    """

    def __init__(self, is_active=True,
                 text="Button",
                 position_in_canvas=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 canvas_id=-1,
                 button_text_fontsize_in_postscript_points=100,
                 sprite_name=""
                 ):
        VisualObject.__init__(self, is_active, np.array(
            [0.0, 0.0, 0.0]), rotation_in_current_CS, np.array([1.0, 1.0, 1.0]))
        # VisualObject.__init__(self,is_active,position_in_canvas, rotation_in_current_CS, size_in_meters)
        # identifier for serialization
        self.type = "button"
        self.text = text
        self.position = position_in_canvas
        self.rotation = rotation_in_current_CS
        self.scale_x_y = size_in_meters
        self.canvas_id = canvas_id
        self.fontsize = button_text_fontsize_in_postscript_points
        self.sprite_name = sprite_name

    def __str__(self):
        return "Button(side=%s,color=%s)" % (self.scale, self.color)


class Canvas(VisualObject):
    """
    A Button that can be placed in a visual scene.
    """

    def __init__(self, is_active=True,
                 text="Canvas",
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 position_in_current_CS=np.array([0.0, 0.0]),
                 canvas_text="",
                 fontsize_in_postscript_points=120,
                 color=color.RGBColor()
                 ):
        VisualObject.__init__(self, is_active, np.array(
            [0.0, 0.0, 0.0]), rotation_in_current_CS, np.array([1.0, 1.0, 1.0]))
        # VisualObject.__init__(self,is_active,position_in_current_CS, rotation_in_current_CS, size_in_meters)
        # identifier for serialization
        self.type = "canvas"
        self.text = text
        self.rotation = rotation_in_current_CS
        self.interface_text = canvas_text
        self.canvas_position = position_in_current_CS
        self.canvas_size = size_in_meters
        self.fontSize = fontsize_in_postscript_points
        self.color = color

    def __str__(self):
        return "Button(side=%s,color=%s)" % (self.scale, self.color)


class CustomObject(VisualObject, ColoratedObject):
    """

    Create a Custom Object (i.e. not a PTVR primitive shape) by importing an
    Asset bundle.

    Parameters
    ----------
    path_to_asset_bundles on your PC: string
        Path to the Asset Bundle.
    asset_to_load : string
        Relative path defined in the '.manifest' file of the Asset Bundle.

    object_size : 3d array
        Size that your CustomObject will have in your experiment.
        Defined as a RATIO wrt initial size of the loaded asset.
        The default is np.array([1.0, 1.0, 1.0]).
    collider_size : 3d array
        Size of the collider which is created when the CustomObject is created.
        Defined in meters. The default is np.array ([1.0, 1.0, 1.0]).
    collider_center : 3d array
        Defined IN METERS wrt collider box origin,
        (the collider origin being at the collider's bottom). The default is np.array([0.0, 0.5, 0.0]).
    display_collider : Boolean
        Show the collider as a green cage. The default is False.
    display_ObjectBox : Boolean
        Show ObjectBoxPTVR as a white cage.
        ObjectBoxPTVR is a box which, when visible,
        allows users to see the initial gross dimensions of the loaded asset.
        It is good practice for modelers who create the asset bundle to create
        a surrounding ObjectBoxPTVR  for each asset.
        The default is False.
    calculate_dimensions : Boolean
        Displays the dimensions of the ObjectBox.
        These dimensions might become available to callbacks in future PTVR versions.

    make_camera_child : Boolean
        If True, the camera (hence what you see in the headset) will be controlled by
        the CustomObject (i.e. if the CustomObject moves,
        you will see in the headset what the CustomObject sees). The default is False.
    is_active : Boolean, optional
        The default is True.
    is_visible : Boolean, optional
        The default is True.
    position_in_current_CS : 3d array
        The default is np.array([0.0, 0.0, 0.0]).
    rotation_in_current_CS : 3d array
        The default is np.array([0.0, 0.0, 0.0]).
    color : TYPE, optional
        The default is color.RGBColor().

    display_animation :  Boolean. The default is False.
    If True, the names of the animations available with this
    asset (CustomObject) are displayed above the asset.
    For ex., "dance" animation for the rabbit asset (CustomObject)

    texture : TYPE, optional
        The default is diffuse_texture.
    pass_letter : TYPE, optional
        The default is None.
    add_physics : Boolean
        The default is False.
    static_object : Boolean
        The default is False.


    images_list : TYPE, optional
        The default is [].

    Returns
    -------
    None.

    """

    def __init__(self,

                 asset_to_load: str,
                 path_to_asset_bundles="",
                 asset_bundle_name="",
                 display_animations=False,
                 make_camera_child=False,
                 is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 object_size=np.array([1.0, 1.0, 1.0]),
                 color=color.RGBColor(),

                 texture=diffuse_texture,
                 pass_letter=None,
                 add_physics=False,
                 static_object=False,
                 collider_size=np.array([1.0, 1.0, 1.0]),
                 collider_center=np.array([0.0, 0.5, 0.0]),
                 display_collider=False,
                 display_ObjectBox=False,
                 calculate_dimensions=False,
                 images_list=[],
                 inverse_polarity=False

                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, object_size)
        ColoratedObject.__init__(self, is_visible, color, texture)
        # identifier for serialization
        self.type = "customObject"

        self.assetPath = asset_to_load
        self.make_camera_child = make_camera_child
        self.pass_letter = pass_letter
        self.add_physics = add_physics
        self.static_object = static_object
        self.collider_size = collider_size
        self.collider_center = collider_center
        self.display_collider = display_collider
        self.display_dimensions = display_ObjectBox
        self.calculate_dimensions = calculate_dimensions
        self.images_list = images_list
        self.position_in_current_CS = position_in_current_CS
        self.display_animations = display_animations
        self.inverse_polarity = inverse_polarity

        if (path_to_asset_bundles == ""):
            path_to_asset_bundles = PTVR.SystemUtils.UnityPTVRRoot + \
                ("Asset_bundles\\") + asset_bundle_name
            # print(path_to_asset_bundles)

        self.fileName = path_to_asset_bundles
        valid_paths = PTVR.SystemUtils.check_path_to_asset_bundles(
            path_to_asset_bundles)
        if (valid_paths == False):
            print("\n")

            print("Failed to find : " + path_to_asset_bundles)
            print("!!!!! -> See explanations in the changelog of version 1.1.3.")
            print("\n")
            sys.exit()

        valid_prefab_paths = PTVR.SystemUtils.check_path_to_prefabs_in_asset_bundle(
            path_to_asset_bundles, asset_to_load)
        if (valid_prefab_paths == False):
            print("\n")
            print("Failed to find : " + asset_to_load +
                  " in " + path_to_asset_bundles)
            print("!!!!! -> See explanations in the changelog of version 1.1.3.")
            print("\n")
            sys.exit()

    def __str__(self):
        return "CustomObject(side=%s,color=%s)" % (self.scale, self.color)


class Sphere(VisualObject, ColoratedObject):
    """
    Size_in_meters is the DIAMETER's size.
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 color=color.RGBColor(),
                 texture=diffuse_texture,
                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)
        ColoratedObject.__init__(self, is_visible, color, texture)
        # identifier for serialization
        self.type = "sphere"
        self.position_in_current_CS = position_in_current_CS


class Eye(VisualObject, ColoratedObject):
    """
    Size_in_meters is the DIAMETER's size.
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 color=color.RGBColor(),
                 texture=diffuse_texture,
                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)
        ColoratedObject.__init__(self, is_visible, color, texture)
        # identifier for serialization
        self.type = "eye"


class Elevator(VisualObject, ColoratedObject):
    """
    An elevator that can be placed in a visual scene. Scale is the radius in meters.
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 color=color.RGBColor(),
                 texture=diffuse_texture,
                 speed=1.0, height=10, comeBack=False,
                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)
        ColoratedObject.__init__(self, is_visible, color, texture)
        # identifier for serialization
        self.type = "elevator"
        self.speed = speed
        self.height = height
        self.comeBack = comeBack


class Forest(VisualObject, ColoratedObject):
    """
    An forest that can be placed in a visual scene. Scale is the radius in meters.
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 color=color.RGBColor(),
                 texture=diffuse_texture,
                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)
        ColoratedObject.__init__(self, is_visible, color, texture)
        # identifier for serialization
        self.type = "forest"


class Cylinder(VisualObject, ColoratedObject):
    """
    A cylinder that can be placed in a visual scene. 
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 color=color.RGBColor(),
                 texture=diffuse_texture):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)
        ColoratedObject.__init__(self, is_visible, color, texture)
        # identifier for serialization
        self.type = "cylinder"


class Capsule(VisualObject, ColoratedObject):
    """
    A cylinder that can be placed in a visual scene. 
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 color=color.RGBColor(),
                 texture=diffuse_texture,
                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)
        ColoratedObject.__init__(self, is_visible, color, texture)
        # identifier for serialization
        self.type = "capsule"


class Quad(VisualObject, ColoratedObject):
    """
    A quad that can be placed in a visual scene.
    A quad has normals in only one direction and appears invisible from the opposite face
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=1,
                 color=color.RGBColor(),
                 texture=diffuse_texture
                 ):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)
        ColoratedObject.__init__(self, is_visible, color, texture)
        # identifier for serialization
        self.type = "quad"


class Line(VisualObject, ColoratedObject):
    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]), line_color=color.RGBColor(r=0.0, g=0.0, b=0.0, a=1.0),
                 start_point=np.array([0.0, 0.0, 0.0]), end_point=np.array([0.0, 0.0, 0.0])):
        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS, size_in_meters)
        ColoratedObject.__init__(
            self, is_visible, color=line_color, texture=diffuse_texture)
        self.startColor = line_color
        self.endColor = line_color

        self.SetStartEndPoints(start_point, end_point)

        # identifier for serialization
        self.type = "line"

    def SetStartEndPoints(self, start_point, end_point):
        if type(start_point) is np.ndarray or type(start_point) is np.array:
            self.startPoint = start_point
            if hasattr(self, "startPointObj"):
                delattr(self, "startPointObj")
        else:
            self.startPointObj = start_point
        if type(end_point) is np.ndarray or type(end_point) is np.array:
            self.endPoint = end_point
            if hasattr(self, "endPointObj"):
                delattr(self, "endPointObj")
        else:
            self.endPointObj = end_point

    def __str__(self):
        return "Line(side=%s,color=%s)" % (self.scale, self.color)


class LineTool(VisualObject, ColoratedObject):
    """
    A Line (actually a cylinder) that can be placed in a visual scene.

    @param is_active indicates if the object is active (if active, works with pointed At)

    @param is_visible indicates if the renderer of the line is shown

    @param start_point indicates with np.array the starting point of the line
    in the CURRENT coordinates system 

    @param end_point indicates with np.array where the line ends in the CURRENT coordinates system 

    @param width indicates the width of the line

    @param color indicates the color of the line

    @param texture indicates the texture of the line, diffuse_texture is currently the only choice
    """

    def __init__(self, is_active=True, is_visible=True, start_point=np.array([0.0, 0.0, 0.0]),
                 end_point=np.array([0.0, 0.0, 0.0]),
                 width=1.0,
                 color=color.RGBColor(),
                 texture=diffuse_texture):
        VisualObject.__init__(self, is_active, start_point)
        ColoratedObject.__init__(self, is_visible, color, texture)
        self.startPoint = start_point
        self.endPoint = end_point
        self.scale = np.array([width, width, width])
        self.sizeX_po = width
        self.sizeY_po = 1.0
        self.sizeZ_po = width
        # identifier for serialization
        self.type = "lineTool"


class Sprite(VisualObject, ColoratedObject):
    """
    A 2D bitmap object that is created from an image file (in png or JPG format)
    A sprite has normals in two directions and appears visible from the 
    opposite face.

    About the 'path_to_image_file' parameter of Sprite():
        if 'path_to_image_file' is set to "" (default), then the 'image_file' parameter
        corresponds to a file stored on you PC in
        ->PTVR_Researchers->PTVR_Operators->resources->Images
        or in one of its sub-directories.

        if 'path_to_image_file' is set to say "C:", then the 'image_file' parameter
        corresponds to a file stored on you PC in "C:"
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 size_in_meters=np.array([1.0, 1.0, 1.0]),
                 path_to_image_file="",
                 image_file="default",
                 texture=diffuse_texture):

        VisualObject.__init__(
            self, is_active, position_in_current_CS, rotation_in_current_CS,  size_in_meters)
        ColoratedObject.__init__(self, is_visible, color.RGBColor(), texture)
        # identifier for serialization
        self.type = "sprite"
        # full sprite path
        self.imageFile = image_file
        # define if the pivot is in the center (True), or upper left (False)
        self.isCenterPivot = True

        self.resourcesPath = PTVR.SystemUtils.UnityPTVRRoot + \
            "\\PTVR_Researchers\\PTVR_Operators\\resources\\Images\\"

        self.path_to_image_file = path_to_image_file
        # check for directory and subdirectory
        image_path = ""
        if (path_to_image_file == ""):
            list_files = []
            for dirpath, dirnames, filenames in os.walk(self.resourcesPath):
                for filename in [f for f in filenames]:

                    if filename == image_file:
                        list_files.append(os.path.join(dirpath, filename))
                        image_path = os.path.join(dirpath, filename)
                        # print (os.path.join(dirpath, filename))

            if (len(list_files) > 0):
                pass
            else:
                print("image is not in the correct path")
                sys.exit()

        else:
            list_files = []
            # print (path_to_image_file)
            for dirpath, dirnames, filenames in os.walk(path_to_image_file):
                for filename in [f for f in filenames]:
                    # print (filename)
                    if filename == image_file:
                        list_files.append(os.path.join(dirpath, filename))
                        image_path = os.path.join(dirpath, filename)

            if (len(list_files) > 0):
                pass
            else:
                print("image is not in the correct path")
                sys.exit()

        # image = Image.open(image_path)
        # self.image_width = image.width
        # self.image_height = image.height


class Gizmo(VisualObject, ColoratedObject):
    """
    """

    def __init__(self, is_active=True, is_visible=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 length=1.0, width=0.05
                 ):

        VisualObject.__init__(self, is_active, position_in_current_CS,
                              rotation_in_current_CS,  np.array([1.0, 1.0, 1.0]))
        ColoratedObject.__init__(self, is_visible, color.RGB255Color(
            r=200.0, a=0.5), diffuse_texture)
        # identifier for serialization
        self.width = width
        self.length = length
        self.type = "gizmo"
