# Coordinate systems

import sys
import json
import numpy as np
import random
from scipy.spatial.transform import Rotation as R
import PTVR.LogManager as Log
from copy import deepcopy
import PTVR.Stimuli.Color as color
import copy
import PTVR.Tools as tools
from scipy.spatial.transform import Rotation as R
# A random value used as default value in some methods for under the "hood features"
# By this way probability to collide with a coherent value is about 1e-15
# By changing it each run it allows to be totally sure that we cannot have a recurent
# default value.
OPTIONAL_VALUE = random.uniform(1e10, 1e10)
# Some predefined textures
PSCounter = 0


def deg_to_radian(degree):
    radian = degree


class MetaData:
    """
    A mother class that simplify the metadata system. It's used for every
    objects, but only the scene saves theses metadatas to the output file.
    """

    def __init__(self):
        self.fillInResultsFileColumnString = {}

    def fill_in_results_file_column(self, column_name, value):
        """
        adds in the results file a variable 'column_name' with is current 'value'.

        Parameters
        ----------
        name : string
        value : float

        Returns
        -------
        None.
        """
        if not hasattr(self, "fillInResultsFileColumnString"):
            # the meta in string format collection
            self.fillInResultsFileColumnString = {}
        self.fillInResultsFileColumnString[column_name] = value

    def to_json(self):
        # keep old one to remember. Does this works right?
        return dict(self.__dict__)


class Frame(MetaData):
    """
    Its the main frame to Manage the default to_json. The serializable objects should inherits from this.
    """

    def __init__(self):
        super().__init__()
        self.parentId = 0
        self.isHeadPOVcontingent = False
        self.isOrigincontingent = False
        self.generate_new_id()

    def generate_new_id(self):
        # uniq id for kept objs, ~max csharp long
        self.id = int(random.random()*1e18)


class VirtualPoint2D(Frame):
    """
    The 2D version for visual field applications.
    """

    def __init__(self):
        Frame.__init__(self)

    # def set_perimetric_coordinates(self, eccentricity=0.0, halfMeridian=0.0):
    #     """
    #     Set the two coordinates eccentricity and halfMeridian in the CURRENT coordinate
    #     system.

    #     Parameters
    #     ----------
    #     eccentricity : float : the current coordinate system eccentricity position.  0.0 is the default.
    #     halfMeridian: float : the current coordinate system halfMeridian position.   0.0 is the default.

    #     Returns
    #     -------
    #     None.

    #     """
    #     if hasattr(self, "perimetricCoordinates"):

    #         self.perimetricCoordinates[0] = eccentricity/180.*np.pi
    #         self.perimetricCoordinates[1] = halfMeridian/180.*np.pi
    #     else:
    #         self.perimetricCoordinates = np.array([eccentricity/180.*np.pi, halfMeridian/180.*np.pi])

    #     x = np.cos(self.perimetricCoordinates[1])*np.sin(self.perimetricCoordinates[0])
    #     y = np.sin(self.perimetricCoordinates[1])*np.sin(self.perimetricCoordinates[0])

    #     self.set_cartesian_coordinates(x, y)

    #     self.eccentricity = self.perimetricCoordinates[0]
    #     self.half_meridian = self.perimetricCoordinates[1]

    #     x = np.cos(self.perimetricCoordinates[1])*np.sin(self.perimetricCoordinates[0])

    # def set_spherical_coordinates(self,coordinate_system="perimetric",eccentricity=OPTIONAL_VALUE, halfMeridian=OPTIONAL_VALUE, azimuth=OPTIONAL_VALUE, elevation=OPTIONAL_VALUE):
    #     """
    #     Set the coordinates of the 2D object in spherical_coordinates.

    #     Parameters
    #     ----------
    #     coordinate_system: type of spherical system to use  "perimetric" is the default and the only one support for the moment.
    #     eccentricity : float the current coordinate system eccentricity position is use with "perimetric". OPTIONAL_VALUE is the default.
    #     halfMeridian: float the current coordinate system halfMeridian position is use with "perimetric". OPTIONAL_VALUE is the default.

    #     azimuth : float the current coordinate system azimuth position. OPTIONAL_VALUE is the default.
    #     elevation : float the current coordinate system elevation position. OPTIONAL_VALUE is the default.

    #     *OPTIONAL_VALUE : is a random value to check if the system get value who be useless example "perimetric" don't need azimuth and elevation.
    #     Returns
    #     -------
    #     None.

    #     """
    #     if coordinate_system == "perimetric":
    #         if elevation!=OPTIONAL_VALUE or azimuth != OPTIONAL_VALUE:
    #             Log.SetWarning ("set_spherical_coordinates: the system is perimetric, uses eccentricity and halfmeridian only")
    #         #Changing to the expected default value
    #         if eccentricity == OPTIONAL_VALUE:
    #             eccentricity = 0.0
    #         if halfMeridian == OPTIONAL_VALUE:
    #             halfMeridian = 0.0
    #         self.set_perimetric_coordinates(eccentricity,halfMeridian)
    #     else:
    #         Log.SetError ("set_spherical_coordinates: " + coordinate_system + " is not available yet.")
    #   # add fick

    # def set_cartesian_coordinates(self, x, y):
    #     """
    #     Set the coordinates of the 2d point in x and y on current coordinate
    #     system

    #     Parameters
    #     ----------
    #     x : float
    #         the current coordinate system x posisition.
    #     y : float
    #         the current coordinate system y posisition.

    #     Returns
    #     -------
    #     None.

    #     """
    #     if hasattr(self, "position_in_current_CS"):
    #         self.position_in_current_CS[0] = x
    #         self.position_in_current_CS[1] = y
    #     else:
    #         self.position_in_current_CS = np.array([x,y])

    # def get_eccentricity (self):
    #     return self.perimetricCoordinates[0]*180/np.pi

    # def get_halfmeridian (self):
    #     return self.perimetricCoordinates[1]*180/np.pi

    # def get_x(self):
    #     return self.position_in_current_CS[0]

    # def get_y(self):
    #     return self.position_in_current_CS[1]


class CoordinateSystemTransformation(MetaData):
    def __init__(self):
        self.type = "reset"
        self.current_coordinate_system_transformation = np.array([
                                                                 0.0, 0.0, 0.0])

    def rotation_current_x(self, angle=0.0):
        self.type = "rotation_current"
        self.current_coordinate_system_transformation = np.array(
            [angle, 0.0, 0.0])

    def rotation_current_y(self, angle=0.0):
        self.type = "rotation_current"
        self.current_coordinate_system_transformation = np.array(
            [0.0, angle, 0.0])

    def rotation_current_z(self, angle=0.0):
        self.type = "rotation_current"
        self.current_coordinate_system_transformation = np.array(
            [0.0, 0.0, angle])

    def translation_current(self, vector=np.array([0.0, 0.0, 0.0])):
        self.type = "translation_current"
        self.current_coordinate_system_transformation = vector

    def rotation_global_x(self, angle=0.0):
        self.type = "rotation_global"
        self.current_coordinate_system_transformation = np.array(
            [angle, 0.0, 0.0])

    def rotation_global_y(self, angle=0.0):
        self.type = "rotation_global"
        self.current_coordinate_system_transformation = np.array(
            [0.0, angle, 0.0])

    def rotation_global_z(self, angle=0.0):
        self.type = "rotation_global"
        self.current_coordinate_system_transformation = np.array(
            [0.0, 0.0, angle])

    def translation_global(self, vector=np.array([0.0, 0.0, 0.0])):
        self.type = "translation_global"
        self.current_coordinate_system_transformation = vector

    def reset(self):
        self.type = "reset"
        self.current_coordinate_system_transformation = np.array([
                                                                 0.0, 0.0, 0.0])


class CoordinateSystem(MetaData):
    def __init__(self):
        self.listTransformation = []
        self.listTransformation.append(CoordinateSystemTransformation())

    def rotation_current_x(self, angle=0.0):
        transformation = CoordinateSystemTransformation()
        transformation.rotation_current_x(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_current_y(self, angle=0.0):
        transformation = CoordinateSystemTransformation()
        transformation.rotation_current_y(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_current_z(self, angle=0.0):
        transformation = CoordinateSystemTransformation()
        transformation.rotation_current_z(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def translation_current(self, vector=np.array([0.0, 0.0, 0.0])):
        transformation = CoordinateSystemTransformation()
        transformation.translation_current(vector)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_global_x(self, angle=0.0):
        transformation = CoordinateSystemTransformation()
        transformation.rotation_global_x(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_global_y(self, angle=0.0):
        transformation = CoordinateSystemTransformation()
        transformation.rotation_global_y(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_global_z(self, angle=0.0):
        transformation = CoordinateSystemTransformation()
        transformation.rotation_global_z(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def translation_global(self, vector=np.array([0.0, 0.0, 0.0])):
        transformation = CoordinateSystemTransformation()
        transformation.translation_global(vector)
        self.listTransformation.append(copy.deepcopy(transformation))

    def reset(self):
        transformation = CoordinateSystemTransformation()
        transformation.reset()
        self.listTransformation.append(copy.deepcopy(transformation))

    def GetCoordinates(self):
        global_CS_axis_X = np.array([1.0, 0.0, 0.0])
        global_CS_axis_Y = np.array([0.0, 1.0, 0.0])
        global_CS_axis_Z = np.array([0.0, 0.0, 1.0])
        global_CS_pos = np.array([0.0, 0.0, 0.0])

        current_CS_axis_X = np.array([1.0, 0.0, 0.0])
        current_CS_axis_Y = np.array([0.0, 1.0, 0.0])
        current_CS_axis_Z = np.array([0.0, 0.0, 1.0])
        current_CS_pos = np.array([0.0, 0.0, 0.0])

        for i in range(0, len(self.listTransformation)):
            if (self.listTransformation[i].type == "reset"):
                current_CS_axis_X = global_CS_axis_X
                current_CS_axis_Y = global_CS_axis_Y
                current_CS_axis_Z = global_CS_axis_Z
                current_CS_pos = global_CS_pos

            elif (self.listTransformation[i].type == "rotation_current"):

                if (self.listTransformation[i].current_coordinate_system_transformation[2] != 0):
                    current_CS_axis_Z = tools.rotate_about_vector(
                        current_CS_axis_Z, current_CS_axis_Z,  self.listTransformation[i].current_coordinate_system_transformation[2])
                    current_CS_axis_X = tools.rotate_about_vector(
                        current_CS_axis_X, current_CS_axis_Z,  self.listTransformation[i].current_coordinate_system_transformation[2])
                    current_CS_axis_Y = tools.rotate_about_vector(
                        current_CS_axis_Y, current_CS_axis_Z,  self.listTransformation[i].current_coordinate_system_transformation[2])
                if (self.listTransformation[i].current_coordinate_system_transformation[0] != 0):
                    current_CS_axis_Z = tools.rotate_about_vector(
                        current_CS_axis_Z, current_CS_axis_X,  self.listTransformation[i].current_coordinate_system_transformation[0])
                    current_CS_axis_X = tools.rotate_about_vector(
                        current_CS_axis_X, current_CS_axis_X,  self.listTransformation[i].current_coordinate_system_transformation[0])
                    current_CS_axis_Y = tools.rotate_about_vector(
                        current_CS_axis_Y, current_CS_axis_X,  self.listTransformation[i].current_coordinate_system_transformation[0])
                if (self.listTransformation[i].current_coordinate_system_transformation[1] != 0):
                    current_CS_axis_Z = tools.rotate_about_vector(
                        current_CS_axis_Z, current_CS_axis_Y,  self.listTransformation[i].current_coordinate_system_transformation[1])
                    current_CS_axis_X = tools.rotate_about_vector(
                        current_CS_axis_X, current_CS_axis_Y,  self.listTransformation[i].current_coordinate_system_transformation[1])
                    current_CS_axis_Y = tools.rotate_about_vector(
                        current_CS_axis_Y, current_CS_axis_Y,  self.listTransformation[i].current_coordinate_system_transformation[1])

            elif (self.listTransformation[i].type == "rotation_global"):
                if (self.listTransformation[i].current_coordinate_system_transformation[2] != 0):
                    current_CS_axis_Z = tools.rotate_about_vector(
                        current_CS_axis_Z, global_CS_axis_Z,  self.listTransformation[i].current_coordinate_system_transformation[2])
                    current_CS_axis_X = tools.rotate_about_vector(
                        current_CS_axis_X, global_CS_axis_Z,  self.listTransformation[i].current_coordinate_system_transformation[2])
                    current_CS_axis_Y = tools.rotate_about_vector(
                        current_CS_axis_Y, global_CS_axis_Z,  self.listTransformation[i].current_coordinate_system_transformation[2])
                if (self.listTransformation[i].current_coordinate_system_transformation[0] != 0):
                    current_CS_axis_Z = tools.rotate_about_vector(
                        current_CS_axis_Z, global_CS_axis_X,  self.listTransformation[i].current_coordinate_system_transformation[0])
                    current_CS_axis_X = tools.rotate_about_vector(
                        current_CS_axis_X, global_CS_axis_X,  self.listTransformation[i].current_coordinate_system_transformation[0])
                    current_CS_axis_Y = tools.rotate_about_vector(
                        current_CS_axis_Y, global_CS_axis_X,  self.listTransformation[i].current_coordinate_system_transformation[0])
                if (self.listTransformation[i].current_coordinate_system_transformation[1] != 0):
                    current_CS_axis_Z = tools.rotate_about_vector(
                        current_CS_axis_Z, global_CS_axis_Y,  self.listTransformation[i].current_coordinate_system_transformation[1])
                    current_CS_axis_X = tools.rotate_about_vector(
                        current_CS_axis_X, global_CS_axis_Y,  self.listTransformation[i].current_coordinate_system_transformation[1])
                    current_CS_axis_Y = tools.rotate_about_vector(
                        current_CS_axis_Y, global_CS_axis_Y,  self.listTransformation[i].current_coordinate_system_transformation[1])

            elif (self.listTransformation[i].type == "translation_current"):
                current_CS_pos[0], current_CS_pos[1], current_CS_pos[2] = tools.translate_along_axis(
                    current_CS_pos, axis_cs_x=current_CS_axis_X, axis_cs_y=current_CS_axis_Y, axis_cs_z=current_CS_axis_Z, epsilon=self.listTransformation[i].current_coordinate_system_transformation)

            elif (self.listTransformation[i].type == "translation_global"):
                current_CS_pos[0], current_CS_pos[1], current_CS_pos[2] = tools.translate_along_axis(
                    current_CS_pos, axis_cs_x=global_CS_axis_X, axis_cs_y=global_CS_axis_Y, axis_cs_z=global_CS_axis_Z, epsilon=self.listTransformation[i].current_coordinate_system_transformation)

        return current_CS_pos[0], current_CS_pos[1], current_CS_pos[2], current_CS_axis_X, current_CS_axis_Y, current_CS_axis_Z


class TransformationCoordinate(MetaData):
    def __init__(self):
        self.type = "reset_position"
        self.transformationCoordinate = np.array([0.0, 0.0, 0.0])

    def rotation_local_x(self, angle=0.0):
        self.type = "rotation_local"
        self.transformationCoordinate = np.array([angle, 0.0, 0.0])

    def rotation_local_y(self, angle=0.0):
        self.type = "rotation_local"
        self.transformationCoordinate = np.array([0.0, angle, 0.0])

    def rotation_local_z(self, angle=0.0):
        self.type = "rotation_local"
        self.transformationCoordinate = np.array([0.0, 0.0, angle])

    def translation_local(self, vector=np.array([0.0, 0.0, 0.0])):
        self.type = "translation_local"
        self.transformationCoordinate = vector

    def rotation_current_x(self, angle=0.0):
        self.type = "rotation_current"
        self.transformationCoordinate = np.array([angle, 0.0, 0.0])

    def rotation_current_y(self, angle=0.0):
        self.type = "rotation_current"
        self.transformationCoordinate = np.array([0.0, angle, 0.0])

    def rotation_current_z(self, angle=0.0):
        self.type = "rotation_current"
        self.transformationCoordinate = np.array([0.0, 0.0, angle])

    def translation_current(self, vector=np.array([0.0, 0.0, 0.0])):
        self.type = "translation_current"
        self.transformationCoordinate = vector

    def rotation_global_x(self, angle=0.0):
        self.type = "rotation_global"
        self.transformationCoordinate = np.array([angle, 0.0, 0.0])

    def rotation_global_y(self, angle=0.0):
        self.type = "rotation_global"
        self.transformationCoordinate = np.array([0.0, angle, 0.0])

    def rotation_global_z(self, angle=0.0):
        self.type = "rotation_global"
        self.transformationCoordinate = np.array([0.0, 0.0, angle])

    def translation_global(self, vector=np.array([0.0, 0.0, 0.0])):
        self.type = "translation_global"
        self.transformationCoordinate = vector

    def reset_position(self):
        self.type = "reset_position"
        self.transformationCoordinate = np.array([0.0, 0.0, 0.0])

    def reset_orientation(self):
        self.type = "reset_orientation"
        self.transformationCoordinate = np.array([0.0, 0.0, 0.0])


class TransformationCoordinateSequence(MetaData):
    def __init__(self):
        self.listTransformation = []
        self.listTransformation.append(TransformationCoordinate())

    def rotation_about_local_x(self, angle=0.0):
        transformation = TransformationCoordinate()
        transformation.rotation_local_x(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_about_local_y(self, angle=0.0):
        transformation = TransformationCoordinate()
        transformation.rotation_local_y(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_about_local_z(self, angle=0.0):
        transformation = TransformationCoordinate()
        transformation.rotation_local_z(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def translation_along_local(self, vector=np.array([0.0, 0.0, 0.0])):
        transformation = TransformationCoordinate()
        transformation.translation_local(vector)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_about_current_x(self, angle=0.0):
        transformation = TransformationCoordinate()
        transformation.rotation_current_x(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_about_current_y(self, angle=0.0):
        transformation = TransformationCoordinate()
        transformation.rotation_current_y(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_about_current_z(self, angle=0.0):
        transformation = TransformationCoordinate()
        transformation.rotation_current_z(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def translation_along_current(self, vector=np.array([0.0, 0.0, 0.0])):
        transformation = TransformationCoordinate()
        transformation.translation_current(vector)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_about_global_x(self, angle=0.0):
        transformation = TransformationCoordinate()
        transformation.rotation_global_x(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_about_global_y(self, angle=0.0):
        transformation = TransformationCoordinate()
        transformation.rotation_global_y(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def rotation_about_global_z(self, angle=0.0):
        transformation = TransformationCoordinate()
        transformation.rotation_global_z(angle)
        self.listTransformation.append(copy.deepcopy(transformation))

    def translation_along_global(self, vector=np.array([0.0, 0.0, 0.0])):
        transformation = TransformationCoordinate()
        transformation.translation_global(vector)
        self.listTransformation.append(copy.deepcopy(transformation))

    def reset_orientation(self):
        transformation = TransformationCoordinate()
        transformation.reset_orientation()
        self.listTransformation.append(copy.deepcopy(transformation))

    def reset_position(self):
        transformation = TransformationCoordinate()
        transformation.reset_position()
        self.listTransformation.append(copy.deepcopy(transformation))

    def reset(self):
        self.reset_orientation()
        self.reset_position()

    def GetCoordinates(self, coordinate_system, parent_cs, parent_axis_x, parent_axis_y, parent_axis_z):
        # 1 get Current CS
        current_pos_x, current_pos_y, current_pos_z, current_CS_axis_X, current_CS_axis_Y, current_CS_axis_Z = coordinate_system.GetCoordinates()

        # 2 get parent if got it:

        # 3 get pos obj
        obj_axis_X = current_CS_axis_X
        obj_axis_Y = current_CS_axis_Y
        obj_axis_Z = current_CS_axis_Z
        obj_pos = np.array([current_pos_x, current_pos_y, current_pos_z])
        equal_arrays = (parent_cs == np.array([0, 0, 0])).all()
        if (equal_arrays == False):
            obj_axis_X = parent_axis_x
            obj_axis_Y = parent_axis_y
            obj_axis_Z = parent_axis_z

            obj_pos = np.array([parent_cs[0], parent_cs[1], parent_cs[2]])

            # obj_pos = parent_cs
            # TODO Manage Rotation Parent

        for i in range(0, len(self.listTransformation)):
            if (self.listTransformation[i].type == "reset_orientation"):
                obj_axis_X = current_CS_axis_X
                obj_axis_Y = current_CS_axis_Y
                obj_axis_Z = current_CS_axis_Z
                equal_arrays = (parent_cs == np.array([0, 0, 0])).all()
                if (equal_arrays == False):
                    obj_axis_X = parent_axis_x
                    obj_axis_Y = parent_axis_y
                    obj_axis_Z = parent_axis_z
            elif (self.listTransformation[i].type == "reset_position"):
                obj_pos = np.array(
                    [current_pos_x, current_pos_y, current_pos_z])
                equal_arrays = (parent_cs == np.array([0, 0, 0])).all()
                if (equal_arrays == False):
                    obj_pos = np.array(
                        [parent_cs[0], parent_cs[1], parent_cs[2]])
            elif (self.listTransformation[i].type == "rotation_local"):
                if (self.listTransformation[i].transformationCoordinate[2] != 0):
                    obj_axis_Z = tools.rotate_about_vector(
                        obj_axis_Z, obj_axis_Z,  self.listTransformation[i].transformationCoordinate[2])
                    obj_axis_X = tools.rotate_about_vector(
                        obj_axis_X, obj_axis_Z,  self.listTransformation[i].transformationCoordinate[2])
                    obj_axis_Y = tools.rotate_about_vector(
                        obj_axis_Y, obj_axis_Z,  self.listTransformation[i].transformationCoordinate[2])
                if (self.listTransformation[i].transformationCoordinate[0] != 0):
                    obj_axis_Z = tools.rotate_about_vector(
                        obj_axis_Z, obj_axis_X, self.listTransformation[i].transformationCoordinate[0])
                    obj_axis_X = tools.rotate_about_vector(
                        obj_axis_X, obj_axis_X, self.listTransformation[i].transformationCoordinate[0])
                    obj_axis_Y = tools.rotate_about_vector(
                        obj_axis_Y, obj_axis_X, self.listTransformation[i].transformationCoordinate[0])
                if (self.listTransformation[i].transformationCoordinate[1] != 0):
                    obj_axis_Z = tools.rotate_about_vector(
                        obj_axis_Z, obj_axis_Y,  self.listTransformation[i].transformationCoordinate[1])
                    obj_axis_X = tools.rotate_about_vector(
                        obj_axis_X, obj_axis_Y,  self.listTransformation[i].transformationCoordinate[1])
                    obj_axis_Y = tools.rotate_about_vector(
                        obj_axis_Y, obj_axis_Y,  self.listTransformation[i].transformationCoordinate[1])

            elif (self.listTransformation[i].type == "rotation_current"):
                if (self.listTransformation[i].transformationCoordinate[2] != 0):
                    obj_axis_Z = tools.rotate_about_vector(
                        obj_axis_Z, current_CS_axis_Z,  self.listTransformation[i].transformationCoordinate[2])
                    obj_axis_X = tools.rotate_about_vector(
                        obj_axis_X, current_CS_axis_Z,  self.listTransformation[i].transformationCoordinate[2])
                    obj_axis_Y = tools.rotate_about_vector(
                        obj_axis_Y, current_CS_axis_Z,  self.listTransformation[i].transformationCoordinate[2])
                if (self.listTransformation[i].transformationCoordinate[0] != 0):
                    obj_axis_Z = tools.rotate_about_vector(
                        obj_axis_Z, current_CS_axis_X,  self.listTransformation[i].transformationCoordinate[0])
                    obj_axis_X = tools.rotate_about_vector(
                        obj_axis_X, current_CS_axis_X,  self.listTransformation[i].transformationCoordinate[0])
                    obj_axis_Y = tools.rotate_about_vector(
                        obj_axis_Y, current_CS_axis_X,  self.listTransformation[i].transformationCoordinate[0])
                if (self.listTransformation[i].transformationCoordinate[1] != 0):
                    obj_axis_Z = tools.rotate_about_vector(
                        obj_axis_Z, current_CS_axis_Y,  self.listTransformation[i].transformationCoordinate[1])
                    obj_axis_X = tools.rotate_about_vector(
                        obj_axis_X, current_CS_axis_Y,  self.listTransformation[i].transformationCoordinate[1])
                    obj_axis_Y = tools.rotate_about_vector(
                        obj_axis_Y, current_CS_axis_Y,  self.listTransformation[i].transformationCoordinate[1])

            elif (self.listTransformation[i].type == "rotation_global"):
                if (self.listTransformation[i].transformationCoordinate[2] != 0):
                    obj_axis_Z = tools.rotate_about_vector(obj_axis_Z, np.array(
                        [0, 0, 1]),  self.listTransformation[i].transformationCoordinate[2])
                    obj_axis_X = tools.rotate_about_vector(obj_axis_X, np.array(
                        [0, 0, 1]),  self.listTransformation[i].transformationCoordinate[2])
                    obj_axis_Y = tools.rotate_about_vector(obj_axis_Y, np.array(
                        [0, 0, 1]),  self.listTransformation[i].transformationCoordinate[2])
                if (self.listTransformation[i].transformationCoordinate[0] != 0):
                    obj_axis_Z = tools.rotate_about_vector(obj_axis_Z, np.array(
                        [1, 0, 0]),  self.listTransformation[i].transformationCoordinate[0])
                    obj_axis_X = tools.rotate_about_vector(obj_axis_X, np.array(
                        [1, 0, 0]),  self.listTransformation[i].transformationCoordinate[0])
                    obj_axis_Y = tools.rotate_about_vector(obj_axis_Y, np.array(
                        [1, 0, 0]),  self.listTransformation[i].transformationCoordinate[0])
                if (self.listTransformation[i].transformationCoordinate[1] != 0):
                    obj_axis_Z = tools.rotate_about_vector(obj_axis_Z, np.array(
                        [0, 1, 0]),  self.listTransformation[i].transformationCoordinate[1])
                    obj_axis_X = tools.rotate_about_vector(obj_axis_X, np.array(
                        [0, 1, 0]),  self.listTransformation[i].transformationCoordinate[1])
                    obj_axis_Y = tools.rotate_about_vector(obj_axis_Y, np.array(
                        [0, 1, 0]),  self.listTransformation[i].transformationCoordinate[1])

            elif (self.listTransformation[i].type == "translation_local"):
                obj_pos[0], obj_pos[1], obj_pos[2] = tools.translate_along_axis(
                    obj_pos, axis_cs_x=obj_axis_X, axis_cs_y=obj_axis_Y, axis_cs_z=obj_axis_Z, epsilon=self.listTransformation[i].transformationCoordinate)

            elif (self.listTransformation[i].type == "translation_current"):
                obj_pos[0], obj_pos[1], obj_pos[2] = tools.translate_along_axis(
                    obj_pos, axis_cs_x=current_CS_axis_X, axis_cs_y=current_CS_axis_Y, axis_cs_z=current_CS_axis_Z, epsilon=self.listTransformation[i].transformationCoordinate)

            elif (self.listTransformation[i].type == "translation_global"):
                obj_pos[0], obj_pos[1], obj_pos[2] = tools.translate_along_axis(obj_pos, axis_cs_x=np.array([1, 0, 0]), axis_cs_y=np.array(
                    [0, 1, 0]), axis_cs_z=np.array([0, 0, 1]), epsilon=self.listTransformation[i].transformationCoordinate)

        return obj_pos[0], obj_pos[1], obj_pos[2], obj_axis_X, obj_axis_Y, obj_axis_Z


class VirtualPoint(VirtualPoint2D):
    """
    Object in a 3D Cartesian coordinate system, defined by x, y and z coordinates centered at the centre of the tracked room.
    Initialization do first rotation_in_current_CS First in Order Z, X Y and after apply position_in_current_CS
    Positive x is rightward.
    Positive y is upward.
    Positive z is forward.

    """

    def __init__(self, position_in_current_CS=np.array([0.0, 0.0, 0.0]), rotation_in_current_CS=np.array([0, 0, 0])):
        VirtualPoint2D.__init__(self)
        self.parent_pos = np.array([0.0, 0.0, 0.0])
        self.transformationCoordinate = TransformationCoordinateSequence()
        # should be a 3 elements array. The rotation is in Z, X, Y order.
        if (rotation_in_current_CS[2] != 0):
            self.rotate_about_current_z(rotation_in_current_CS[2])
        if (rotation_in_current_CS[0] != 0):
            self.rotate_about_current_x(rotation_in_current_CS[0])
        if (rotation_in_current_CS[1] != 0):
            self.rotate_about_current_y(rotation_in_current_CS[1])

        # should be a 3d array. Cartesian X, Y, Z
        if (position_in_current_CS[0] != 0 or position_in_current_CS[1] != 0 or position_in_current_CS[2] != 0):
            self.set_cartesian_coordinates(
                position_in_current_CS[0], position_in_current_CS[1], position_in_current_CS[2])

        # uniq id for any coordinated object
        # if parentId <= 0 the referential is the world. The parent link by the id parentId otherwise.
        self.axis_X = np.array([1, 0, 0])
        self.axis_Y = np.array([0, 1, 0])
        self.axis_Z = np.array([0, 0, 1])

        self.parent_axis_X = np.array([1, 0, 0])
        self.parent_axis_Y = np.array([1, 0, 0])
        self.parent_axis_Z = np.array([1, 0, 0])

        self.current_coordinate_system = CoordinateSystem()
        self.isFacing = False
        self.onOppositeDirection = False

        self.azimuth = 0
        self.elevation = 0
        self.radialDistance = 0

    def rotate_to_look_at(self, id_object_to_lookAt):
        """
        Rotate the object so that its orientation is such that it "looks at" the
        object whose id is 'id_object_to_lookAt'.

        Parameters
        ----------
        id_object_to_lookAt : int
            my_object.id

            or

            my_world.HandControllerRight.id

            my_world.HandControllerLeft.id

            my_world.headset.id

            my_world.originCurrentCS.id

            my_world.originGlobalCS.id -> Origin of Global Coordinate System.

        Returns
        -------
        None.

        """
        self.isFacing = True
        self.idObjectToFacing = id_object_to_lookAt

    def rotate_to_look_at_in_opposite_direction(self, id_object_to_lookAt_in_opposite_direction):
        """
        Parameters
        ----------
        id_object_to_lookAt_in_opposite_direction : int
        On attends ici l'id de l'objet to lookAt in opposite direction.
        Il est possible de mettre l'id du headset,des controlles, du current_cs , de l'origin en donnant sois directement le bonne id soit en le récupérant de The3DWorld en utilisant soit :
        my_world.idHandControllerLeft -> correspond à la manette gauche (son id est -1 pour le mettre directement)
        my_world.idHandControllerRight -> correspond à la manette droite (son id est -2 pour le mettre directement)
        my_world.idHeadset -> correspond au casque (son id est -3 pour le mettre directement)
        my_world.idOriginCurrentCS -> correspond à l'origin du current_cs  (son id est -4 pour le mettre directement)
        my_world.idGlobalCS -> correspond à l'origin de room calibration (son id est -5 pour le mettre directement)
        Returns
        -------
        None.
        """
        self.idObjectToFacing = id_object_to_lookAt_in_opposite_direction
        self.isFacing = True
        self.onOppositeDirection = True

    def get_position(self):
        """
        Returns 
        -------
        np.array([x,y,z]) returns the position in GLOBAL coordinates of the object.
        """
        x, y, z, self.axis_X, self.axis_Y, self.axis_Z = self.transformationCoordinate.GetCoordinates(
            self.current_coordinate_system, self.parent_pos, self.parent_axis_X, self.parent_axis_Y, self.parent_axis_Z)
        # print("[in function get_position_in_global_CS ] x = "+ str(x) + " , y = "+ str(y) + ", z = "+ str(z))
        return np.array([x, y, z])

    def get_axes(self):
        """
        Get the 3 local axes of the object, i.e. get the 3 axes of the Object's coordinate system.
        These axes are specified in GLOBAL coordinates.

        Parameters
        ----------
        none

        Returns
        -------
        a tuple containing three 3D vectors: 
        axis_X, axis_Y, axis_Z 

        Returned values are not correct if the object has been previously rotated either with rotate_to_look_at () or rotate_to_look_at_in_opposite_direction ()

        """
        x, y, z, self.axis_X, self.axis_Y, self.axis_Z = self.transformationCoordinate.GetCoordinates(
            self.current_coordinate_system, self.parent_pos, self.parent_axis_X, self.parent_axis_Y, self.parent_axis_Z)
        # print("[in function get_position_in_global_CS ] x_axis = "+ str(self.axis_X) + " , y_axis = "+ str(self.axis_Y) + ", z_axis = "+ str(self.axis_Z))
        return self.axis_X, self.axis_Y, self.axis_Z

    def get_forward_axis(self):
        """
        Get the Z Axis in global cs

        Returns
        -------
        None.

        """
        self.axis_X, self.axis_Y, self.axis_Z = self.get_axes()
        return self.axis_Z

    def get_upward_axis(self):
        """
        Get the Y Axis in global cs

        Returns
        -------
        None.

        """
        self.axis_X, self.axis_Y, self.axis_Z = self.get_axes()
        return self.axis_Y

    def get_rightward_axis(self):
        """
        Get the X Axis in global cs

        Returns
        -------
        None.

        """
        self.axis_X, self.axis_Y, self.axis_Z = self.get_axes()
        return self.axis_X

    def rotate_about_current_x(self, angle_deg):
        """
        Rotation about the x axis of the current coordinate system.
        It corresponds to the "global" rotation.

        Parameters
        ----------
        angle : float
            rotation angle in degrees.

        Returns
        -------
        None.

        """
        self.transformationCoordinate.rotation_about_current_x(angle_deg)

    def rotate_about_current_y(self, angle_deg):
        self.transformationCoordinate.rotation_about_current_y(angle_deg)

    def rotate_about_current_z(self, angle_deg):
        self.transformationCoordinate.rotation_about_current_z(angle_deg)

    def rotate_about_global_x(self, angle_deg):
        """
        Rotation about the x axis of the current coordinate system.
        It corresponds to the "global" rotation.

        Parameters
        ----------
        angle : float
            rotation angle in degrees.

        Returns
        -------
        None.

        """
        self.transformationCoordinate.rotation_about_global_x(angle_deg)

    def rotate_about_global_y(self, angle_deg):
        self.transformationCoordinate.rotation_about_global_y(angle_deg)

    def rotate_about_global_z(self, angle_deg):
        self.transformationCoordinate.rotation_about_global_z(angle_deg)

    def rotate_about_object_x(self, angle_deg):
        self.transformationCoordinate.rotation_about_local_x(angle_deg)

    def rotate_about_object_y(self, angle_deg):
        self.transformationCoordinate.rotation_about_local_y(angle_deg)

    def rotate_about_object_z(self, angle_deg):
        self.transformationCoordinate.rotation_about_local_z(angle_deg)

    def translate_along_global_xyz(self, x, y, z):
        """
        Translate the object along the x, y and z axis of the global 
        system (aka the x, y and z axis of the current object)

        Parameters
        ----------
        x : float
            translation on the object x axis in meters.
        y : float
            translation on the object y axis in meters.
        z : float
            translation on the object z axis in meters.

        Returns
        -------
        None.

        """
        self.transformationCoordinate.translation_along_global(
            np.array([float(x), float(y), float(z)]))

    def translate_along_global_vector(self, translate_vector):
        """
        Translate the object along the x, y and z axis of the global
        system

        Parameters
        ----------
        translate_vector : 3d np.array
            translation of the vector values along x,y,z axis (in this order) in meters.


        Returns
        -------
        None.

        """
        self.transformationCoordinate.translation_along_global(
            translate_vector)

    def translate_along_current_vector(self, translate_vector):
        """
        Translate the object along the x, y and z axis of the current coordinate 
        system

        Parameters
        ----------
        translate_vector : 3d np.array
        translation of the vector values along x,y,z axis (in this order) in meters.


        Returns
        -------
        None.

        """
        self.transformationCoordinate.translation_along_current(
            translate_vector)

    def translate_along_current_xyz(self, x, y, z):
        """
        Translate the object along the x, y and z axis of the current coordinate 
        system

        Parameters
        ----------
        x : float
            translation on the x axis in meters.
        y : float
            translation on the y axis in meters.
        z : float
            translation on the z axis in meters.

        Returns
        -------
        None.

        """
        self.transformationCoordinate.translation_along_current(
            np.array([float(x), float(y), float(z)]))

    def translate_along_local_xyz(self, x, y, z):
        """
        Translate the object along the x, y and z axis of the local coordinates 
        system (aka the x, y and z axis of the current object)

        Parameters
        ----------
        x : float
            translation on the object x axis in meters.
        y : float
            translation on the object y axis in meters.
        z : float
            translation on the object z axis in meters.

        Returns
        -------
        None.

        """
        self.transformationCoordinate.translation_along_local(
            np.array([float(x), float(y), float(z)]))

    def translate_along_local_vector(self, translate_vector):
        """
        Translate the object along the x, y and z axis of the local coordinates 
        system (aka the x, y and z axis of the current object)

        Parameters
        ----------
        translate_vector : 3d np.array
            translation of the vector values along x,y,z axis (in this order) in meters.

        Returns
        -------
        None.

        """
        self.transformationCoordinate.translation_along_local(translate_vector)

    # system can be direct or unity like (y and z inversion)
    def set_cartesian_coordinates(self, x=0.0, y=0.0, z=0.0, system="leftHand_Zforward"):
        """

        Set the coordinates of the 3d point in x, y and z on current coordinate
        system

        Parameters
        ----------
        x : float
            the current coordinate system x position.
        y : float
            the current coordinate system y position.
        z : float
            the current coordinate system z position.

        Returns
        -------
        None.

        """
        if system == "rightHand_Zupward":
            self.transformationCoordinate.reset_position()
            self.transformationCoordinate.translation_along_current(
                np.array([x, y, -z]))
        else:
            self.transformationCoordinate.reset_position()
            self.transformationCoordinate.translation_along_current(
                np.array([x, y, z]))
        return

    # system can be direct or unity like (y and z inversion)
    def set_local_cartesian_coordinates(self, x=0.0, y=0.0, z=0.0, system="leftHand_Zforward"):
        if system == "rightHand_Zupward":
            self.transformationCoordinate.reset_position()
            self.transformationCoordinate.translation_along_local(
                np.array([float(x), float(y), float(-z)]))
        else:
            self.transformationCoordinate.reset_position()
            self.transformationCoordinate.translation_along_local(
                np.array([float(x), float(y), float(z)]))

    def set_position_from_calculator(self, calculatedPosition):
        self.calculatedPosition = calculatedPosition

    def set_spherical_coordinates(self, coordinate_system="perimetric", radialDistance=0.0,
                                  eccentricity=OPTIONAL_VALUE, halfMeridian=OPTIONAL_VALUE,
                                  elevation=OPTIONAL_VALUE, azimuth=OPTIONAL_VALUE):
        """
        Parameters
        ----------
        coordinate_system: type of spherical system to use  "perimetric" is the default and the only one support for the moment.

        radialDistance :float the current coordinate system radialDistance position. 0.0 is the default.
        eccentricity : float the current coordinate system eccentricity position is use with "perimetric". OPTIONAL_VALUE is the default if you dont change it would be 0.0'.
        halfMeridian: float the current coordinate system halfMeridian position is use with "perimetric". OPTIONAL_VALUE is the default if you dont change it would be 0.0'.

        azimuth : float the current coordinate system azimuth position. OPTIONAL_VALUE is the default if you dont change it would be 0.0'.
        elevation : float the current coordinate system elevation position. OPTIONAL_VALUE is the default if you dont change it would be 0.0'.

        *OPTIONAL_VALUE : is a random value to check if the system get value who be useless example "perimetric" don't need azimuth and elevation.



        Returns
        -------
        np.array
            the x y z array.

        """
        Log.SetError(
            "set_spherical_coordinates is deprecated, please use set_perimetric_coordinates instead")
        sys.exit()
        # if coordinate_system == "perimetric":
        #     if elevation != OPTIONAL_VALUE or azimuth != OPTIONAL_VALUE:
        #         Log.SetWarning(
        #             "set_spherical_coordinates: the system is perimetric, uses eccentricity and halfmeridian only")

        #     # Changing to the expected default value
        #     if eccentricity == OPTIONAL_VALUE:
        #         eccentricity = 0
        #     if halfMeridian == OPTIONAL_VALUE:
        #         halfMeridian = 0

        #     self.set_perimetric_coordinates(eccentricity=float(eccentricity), halfMeridian=float(
        #         halfMeridian), radialDistance=float(radialDistance))

        # if coordinate_system == "azimuth_elevation":
        #     if azimuth != OPTIONAL_VALUE or elevation != OPTIONAL_VALUE:
        #         Log.SetWarning(
        #             "set_spherical_coordinates: the system is Fick, uses elevation and azimuth only")
        #         # Log.SetError(
        #         #     "Beware the system is still experimental. Use at your own risks.")

        #     # Changing to the expected default value
        #     if elevation == OPTIONAL_VALUE:
        #         elevation = 0
        #     if azimuth == OPTIONAL_VALUE:
        #         azimuth = 0

        #     self.set_azimuth_elevation_coordinate(
        #         np.array([float(radialDistance), float(azimuth), float(elevation)]))

    def set_local_spherical_coordinates(self, coordinate_system="perimetric", radialDistance=0.0, eccentricity=OPTIONAL_VALUE, halfMeridian=OPTIONAL_VALUE, elevation=OPTIONAL_VALUE, azimuth=OPTIONAL_VALUE):
        """
        Parameters
        ----------
        coordinate_system: type of spherical system to use  "perimetric" is the default and the only one support for the moment.

        radialDistance :float the current coordinate system radialDistance position. 0.0 is the default.
        eccentricity : float the current coordinate system eccentricity position is use with "perimetric". OPTIONAL_VALUE is the default if you dont change it would be 0.0'.
        halfMeridian: float the current coordinate system halfMeridian position is use with "perimetric". OPTIONAL_VALUE is the default if you dont change it would be 0.0'.

        azimuth : float the current coordinate system azimuth position. OPTIONAL_VALUE is the default if you dont change it would be 0.0'.
        elevation : float the current coordinate system elevation position. OPTIONAL_VALUE is the default if you dont change it would be 0.0'.

        *OPTIONAL_VALUE : is a random value to check if the system get value who be useless example "perimetric" don't need azimuth and elevation.



        Returns
        -------
        np.array
            the x y z array.

        """
        if coordinate_system == "perimetric":
            if elevation != OPTIONAL_VALUE or azimuth != OPTIONAL_VALUE:
                Log.SetWarning(
                    "set_spherical_coordinates: the system is perimetric, uses eccentricity and halfmeridian only")

            # Changing to the expected default value
            if eccentricity == OPTIONAL_VALUE:
                eccentricity = 0.0
            if halfMeridian == OPTIONAL_VALUE:
                halfMeridian = 0.0

            self.set_local_perimetric_coordinates(eccentricity=float(
                eccentricity), halfMeridian=float(halfMeridian), radialDistance=float(radialDistance))

        if coordinate_system == "fick":
            if eccentricity != OPTIONAL_VALUE or halfMeridian != OPTIONAL_VALUE:
                Log.SetWarning(
                    "set_spherical_coordinates: the system is Fick, uses elevation and azimuth only")
                Log.SetError(
                    "Beware the system is still experimental. Use at your own risks.")

            # Changing to the expected default value
            if elevation == OPTIONAL_VALUE:
                elevation = 0.0
            if azimuth == OPTIONAL_VALUE:
                azimuth = 0.0

            self.set_local_fick_coordinate(
                np.array([float(radialDistance), float(elevation), float(azimuth)]))

    def set_infinite_circular_cone(self, infiniteCone):
        """
        Configure the visual cone center for placement on tangent screens.
        The objects will be placed with a shift with this Coordinated.
        @infiniteCone has to be an InfiniteCircularCone
        """
        self.infiniteCone = infiniteCone

    def set_perimetric_coordinates(self, radialDistance=0.0,
                                   eccentricity=0.0, halfMeridian=0.0):
        x, y, z = tools.set_spherical_to_cartesian(
            float(eccentricity), float(halfMeridian), float(radialDistance))

        # Creates an error if eccentricity == 0.
        self.set_cartesian_coordinates(x, y, z, False)
        # TODO: convertion managing refactoring to do.

    def set_local_perimetric_coordinates(self, radialDistance=0.0, eccentricity=0.0, halfMeridian=0.0):
        x, y, z = tools.set_spherical_to_cartesian(
            float(eccentricity), float(halfMeridian), float(radialDistance))
        self.set_local_cartesian_coordinates(x, y, z, False)

        print("set_local_perimetric_coordinates is a Legacy function, please use set_perimetric_coordinates instead ")

    # def set_fick_coordinate(self, coordinates):
    #     # radial, elevation, azimuth
    #     # coordinates[1] = coordinates[1]/180*np.pi
    #     # coordinates[2] = coordinates[2]/180*np.pi
    #     # self.fickCoordinates = coordinates

    #     # x=r∗sin(ϕ)∗cos(θ)
    #     # y=r∗sin(ϕ)∗sin(θ)
    #     # z=r∗cos(ϕ)

    #     z = float(coordinates[0])*np.cos(float(coordinates[1])
    #                                      )*np.sin(np.pi/2-float(coordinates[2]))
    #     x = float(coordinates[0])*np.sin(np.pi/2 -
    #                                      float(coordinates[1]))*np.sin(float(coordinates[2]))
    #     y = float(coordinates[0])*np.cos(np.pi/2-float(coordinates[1]))

    #     self.set_cartesian_coordinates(x, y, z)

    def set_azimuth_elevation_coordinates(self, radialDistance=0,
                                          azimuth=0,
                                          elevation=0,
                                          azimuth_reference_cardinal_direction="north",
                                          azimuth_rotation_direction="clockwise"):

        # the following only concerns x and y as they are in
        # the reference plane (ie xy plane)
        # See https://en.wikipedia.org/wiki/Atan2
        # sections East-counterclockwise, north-clockwise, etc.

        self.azimuth = azimuth
        self.elevation = elevation
        self.radialDistance = radialDistance

        if azimuth_reference_cardinal_direction == "east":    # default
            azimuth = azimuth - 90
            elevation = elevation
            if azimuth_rotation_direction == "counterclockwise":
                azimuth = azimuth
                elevation = elevation
            if azimuth_rotation_direction == "clockwise":
                azimuth = -azimuth
                elevation = elevation

        elif azimuth_reference_cardinal_direction == "west":
            azimuth = azimuth + 90
            elevation = elevation
            if azimuth_rotation_direction == "counterclockwise":
                azimuth = azimuth
                elevation = elevation
            if azimuth_rotation_direction == "clockwise":
                azimuth = -azimuth
                elevation = elevation

        elif azimuth_reference_cardinal_direction == "north":
            azimuth = azimuth
            elevation = elevation
            if azimuth_rotation_direction == "counterclockwise":
                azimuth = azimuth
                elevation = elevation
            if azimuth_rotation_direction == "clockwise":
                azimuth = -azimuth
                elevation = elevation

        elif azimuth_reference_cardinal_direction == "south":
            azimuth = azimuth + 180
            elevation = elevation
            if azimuth_rotation_direction == "counterclockwise":
                azimuth = azimuth
                elevation = elevation
            if azimuth_rotation_direction == "clockwise":
                azimuth = -azimuth
                elevation = elevation

        # radial, elevation, azimuth

        print(azimuth, elevation)
        # print(azimuth, 90-(elevation+1))

        # print(azimuth, 90-elevation)
        # print(azimuth, 90-(elevation+1))

        azimuth = -azimuth

        elevation = 90-elevation

        r = float(radialDistance)
        azimuth = float(azimuth/180)*np.pi
        elevation = float(elevation/180)*np.pi
        # self.fickCoordinates = coordinates

        # x=r∗sin(ϕ)∗cos(θ)
        # y=r∗sin(ϕ)∗sin(θ)
        # z=r∗cos(ϕ)

        # azimuth =

        z = r*np.cos(azimuth)*np.sin(elevation)
        x = r*np.sin(azimuth)*np.sin(elevation)
        y = r*np.cos(elevation)

        self.set_cartesian_coordinates(x, y, z)

    def set_local_fick_coordinate(self, coordinates):
        # coordinates[1]=coordinates[1]/180*np.pi
        # coordinates[2]=coordinates[2]/180*np.pi
        # self.fickCoordinates = coordinates

        z = float(coordinates[0])*np.cos(float(coordinates[1])
                                         )*np.sin(np.pi/2-float(coordinates[2]))
        x = float(coordinates[0])*np.sin(np.pi/2 -
                                         float(coordinates[1]))*np.sin(float(coordinates[2]))
        y = float(coordinates[0])*np.cos(np.pi/2-float(coordinates[1]))

        self.set_local_cartesian_coordinates(x, y, z)

    def set_parent(self, parent, x_local=0.0, y_local=0.0, z_local=0.0, need_parent_information=True):
        # the parent referential for rotation and position

        self.parentId = parent.id
        if (need_parent_information):
            parent_x = 0.0
            parent_y = 0.0
            parent_z = 0.0
            # Hotfix SPecific object headset controller
            if (self.parentId > 0):
                parent_x, parent_y, parent_z = parent.get_position()
                self.parent_axis_X, self.parent_axis_Y, self.parent_axis_Z = parent.get_axes()
                self.parent_pos = np.array([parent_x, parent_y, parent_z])

        self.set_local_cartesian_coordinates(
            float(x_local), float(y_local), float(z_local))
        # Log.SetWarning("Your Rotate about Current and Global while not be apply !!! Only Rotate_about_local while work")

    def set_parent_id(self, parent_id):
        """
        Give a parent to an object

        Parameters
        ----------
        parent_id : 
            my_world.handControllerRight.id

            my_world.headset.id

            id of any PTVR object

        Returns
        -------
        None.

        examples
        --------
            >>> my_world = visual.The3DWorld
            >>> my_object = ...
            >>> my_object.set_parent_id ( my_world.handControllerRight.id ) # hand-contingent object  
            or   
            >>> my_object.set_parent_id ( my_world.headset.id ) # head-contingent object   
            or 
            >>> my_object.set_parent_id ( my_world.any_object.id ) 

        """

        self.parentId = parent_id

    def set_cartesian_coordinates_on_screen(self, my_2D_screen, x_local=0.0,
                                            y_local=0.0,
                                            z_local=-0.001):
        """
        Place the current object on the tangent screen passed as argument.
        The object's position is specified in the (local) tangent screen's
        coordinate system with cartesian coordinates.

        Parameters
        ----------
        my_2D_screen : a FlatScreen or TangentScreen object
            The screen on which to place the object.
        x_local : float
            Local x cartesian coordinate (i.e. wrt Screen Origin).
        y_local : float
            Local y cartesian coordinate (i.e. wrt Screen Origin).
        z_local : float    
            Local z cartesian coordinate (i.e. wrt Screen Origin).
        Do not set the z_local parameter to zero as it places the object
        right ON the screen and produces flicker (this is a Unity constraint).
        This is why the default value is -0.001.
        A NEGATIVE value for this parameter has two different meanings depending
        on the type of screen passed to the function:
        if the screen is Flat: a negative value simply means that the local Z
        is negative (i.e. this depends on the screen's orientation in the global CS).
        If the screen is TANGENT, a negative value means that text is IN FRONT
        of the TS, i.e. between the TS and the origin of the current coordinate system.


        Returns
        -------
        None.

        """
        my_2D_screen._place_object_on_screen_in_cartesian_coordinates(
            self, float(x_local), float(y_local), float(z_local))

    def set_spherical_coordinates_on_screen(self, tangentscreen,
                                            eccentricity_local_deg=0.0,
                                            half_meridian_local_deg=0.0,
                                            coordinate_system="perimetric"):
        """

        """
        Log.SetError(
            "set_spherical_coordinates_on_screen is deprecated, please use set_perimetric_coordinates_on_screen instead")
        sys.exit()

    def set_perimetric_coordinates_on_screen(self, tangentscreen,
                                             eccentricity_local_deg=0.0,
                                             half_meridian_local_deg=0.0
                                             ):
        """
        Place the current object on the tangent screen passed as argument.
        The object's position is specified in the (local) tangent screen's
        coordinate system with perimetric coordinates.

        The eccentricity and half_meridian angular values are specified with respect
        to the origin of the screen.

        Parameters
        ----------
        my_tangentscreen : a TangentScreen object
            The screen on which to place the object.
        eccentricity_local_deg : float
            Local perimetric eccentricity coordinate (i.e. wrt Screen Origin).
        half_meridian_local_deg : float
            Local perimetric half_meridian coordinate(i.e. wrt Screen Origin).
        distance_from_screen : float
            Do not set this parameter to zero as it places the object
            right ON the screen and produces flicker (this is a Unity constraint).
            This is why the default value is 0.001.
            A positive value for this parameter means that the object is 
            in front of the TS, i.e. towards the origin of the current coordinate system.


        Returns
        -------
        None.

        """
        distance_from_screen = 0.001
        if (tangentscreen.type != "tangent_screen"):
            Log.SetWarning("You are attempting to place an object on perimetric coordinates on an object which is not a tangentscreen. \n this is not possible currently. your object will not be placed on it.")
        else:
            tangentscreen._place_object_on_screen_in_perimetric_coordinates(
                self, float(eccentricity_local_deg),
                float(half_meridian_local_deg), float(distance_from_screen))

    def reset(self):
        self.transformationCoordinate.reset_orientation()
        self.transformationCoordinate.reset_position()


class Resizable1D (VirtualPoint):
    def __init__(self, sizeX=0.0, position_in_current_CS=np.array([0.0, 0.0, 0.0]), rotation_in_current_CS=np.array([0, 0, 0])):
        VirtualPoint().__init__(position_in_current_CS, rotation_in_current_CS)
        self.sizeX_po = float(sizeX)
        self.sizeX = 1.

    def resize_without_its_children(self, sizeX=0.0):
        self.sizeX_po = sizeX

    def resize_with_its_children(self, sizeX=0.0):
        self.sizeX = sizeX

    def set_oriented_angular_size(self, orientationDeg, angularSize):
        self.orientationDeg = orientationDeg
        self.angularSize = angularSize


class Resizable2D (Resizable1D):
    def __init__(self, sizeX=0.0, sizeY=0.0, position_in_current_CS=np.array([0.0, 0.0, 0.0]), rotation_in_current_CS=np.array([0, 0, 0])):
        super().__init__(sizeX, position_in_current_CS, rotation_in_current_CS)
        self.sizeY_po = float(sizeY)
        self.sizeY = 1.

    def resize_without_its_children(self, sizeX=0.0, sizeY=0.0):
        self.sizeX_po = sizeX
        self.sizeY_po = sizeY

    def resize_with_its_children(self, sizeX=0.0, sizeY=0.0):
        self.sizeX = sizeX
        self.sizeY = sizeY

    def set_xy_angular_size(self, Xdeg, Ydeg, approximative=False):
        self.Xdeg = float(Xdeg)
        self.Ydeg = float(Ydeg)
        self.approximative = approximative
        # if radial_distance =0
        # if self.parentId is None and and self.radialDistance is None:$
        if self.parentId == -1 and self.get_magnitude() == 0:
            Log.SetWarning(
                "Object without parent and nul distance from staticPOV. I might not be visible.")


class Resizable(Resizable2D):
    def __init__(self, sizeX=0.0, sizeY=0.0, sizeZ=0.0, position_in_current_CS=np.array([0.0, 0.0, 0.0]), rotation_in_current_CS=np.array([0, 0, 0])):
        super().__init__(sizeX, sizeY, position_in_current_CS, rotation_in_current_CS)
        self.sizeZ = 1.
        self.sizeZ_po = float(sizeZ)

    def resize_without_its_children(self, sizeX=0.0, sizeY=0.0, sizeZ=0.0):
        self.sizeX_po = sizeX
        self.sizeY_po = sizeY
        self.sizeZ_po = sizeZ

    def resize_with_its_children(self, sizeX=0.0, sizeY=0.0, sizeZ=0.0):
        self.sizeX = sizeX
        self.sizeY = sizeY
        self.sizeZ = sizeZ


class Button:
    def __init__(self, name_button=""):
        self.nameButton = name_button


class ComplexEncoder(json.JSONEncoder):
    """
    An internal class that helps in encoding of objects that are and aren't directly serializable. Not much used
    """

    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()

        if hasattr(obj, 'to_json'):
            return obj.to_json()
        else:
            return json.JSONEncoder.default(self, obj)


class VisualObject(Resizable):
    """
    The base class for a 3D visual object
    """

    def __init__(self, is_active=True, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 # scale=np.array([1.0, 1.0, 1.0])): #TODO: 3d scale
                 scale=1, is_visible=True):
        VirtualPoint.__init__(self, position_in_current_CS,
                              rotation_in_current_CS)
        self.isActive = is_active
        if type(scale) is not np.ndarray:
            # define the size of the visual object. Can be a simple float or 3d table
            self.scale = np.array([scale, scale, scale])
        else:
            if scale.size == 2:
                self.scale = np.array([scale[0], scale[1], 1])
            else:
                self.scale = np.array([scale[0], scale[1], scale[2]])
        Resizable.__init__(self, self.scale[0], self.scale[1],
                           self.scale[2], position_in_current_CS, rotation_in_current_CS)

    def clone(self):
        a_VO = deepcopy(self)
        a_VO.generate_new_id()
        return a_VO


class EmptyVisualObject(VisualObject):
    """
    An empty visual object to allow parenting and 'unity side' works
    with simple coordinates.
    """

    def __init__(self, position_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 rotation_in_current_CS=np.array([0.0, 0.0, 0.0]),
                 relative_to_head=False):
        VisualObject.__init__(self, position_in_current_CS=position_in_current_CS,
                              rotation_in_current_CS=rotation_in_current_CS)
        # identifier for serialization
        self.position_in_current_CS = position_in_current_CS
        self.rotation_in_current_CS = rotation_in_current_CS
        self.type = "coordinated"
        self.isRelativeToHead = relative_to_head

# class InfiniteCircularCone(MetaData):
#     """
#     Tool to make easier visual angles on screens, particularly on tangent screens.
#     The circular base is the calculation with classicl "errors" corresponding
#     to the most used case for physical screens.
#     Ellipsoid is for exact angle calcultions.
#     @circularOrEllipseBase takes "ellipse" or "circle" arguments
#     @tangentCorrection is used for circular, it makes a correction following
#     eccentricity, in this case, only tangent diameters are exacts.
#     @coneCenter has to be a Coordinated.
#     """
#     def __init__(self, angleAtApex = 0, apexPosition = VirtualPoint(), CAPOC = VirtualPoint(), circularOrEllipseBase = "ellipse", tangentCorrection = False):
#         self.circularOrEllipseBase = circularOrEllipseBase
#         self.tangentCorrection = tangentCorrection
#         self.CAPOC = CAPOC
#         self.angleAtApex = angleAtApex
#         self.apexPosition = apexPosition

#     def SetConeAngleFromLengthInMetersOnScreen (self, length, onThisScreen):
#         self.LengthToBeConvertedInAngle = length

#     def GetEllipseChordEndpointsFromConeAngle (self, OrientationOfEllipseChord, onThisScreen):
#         endPoint1 = EmptyVisualObject ()
#         endPoint2 = EmptyVisualObject ()
#         #onThisScreen.SetInfiniteCircularCone(self)
#         onThisScreen.place_object_on_tangent_screen(endPoint1, eccentricity=self.angleAtApex/2, halfMeridian= OrientationOfEllipseChord)
#         onThisScreen.place_object_on_tangent_screen(endPoint2, eccentricity=self.angleAtApex/2, halfMeridian= 180+OrientationOfEllipseChord)

#         endPoint1.infiniteCone = self
#         endPoint2.infiniteCone = self
#         return endPoint1, endPoint2

#     def SetConeCenter(self, CAPOC):
#         self.CAPOC = CAPOC


class Screen:
    def __init__(self):
        print("screen")
