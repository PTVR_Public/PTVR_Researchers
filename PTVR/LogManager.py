import os
import subprocess
import sys      
from inspect import stack

def SetWarning(information) :
          return print ("\n\033[1;33mWarning in function " + str(stack()[1].function) + " in file : \n " + str(stack()[1].filename) + " \n " + information + "\033[0m\n") #ansi escape character
def SetError(information) :
          return print ("\n\033[1;31mError in function " + str(stack()[1].function) + " in file : \n " + str(stack()[1].filename) + " \n " + information +"\033[0m\n") #ansi escape character


    