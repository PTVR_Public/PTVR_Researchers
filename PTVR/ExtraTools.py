def print_as_table(data, headers = None):
    """
    print data in table format
    data is expected to be row by row
    """
    nr_cols = len(data[0])

    widths = [max(len(str(row[i])) for row in data) for i in range(nr_cols)]
    if headers is not None:
        widths = [max(len(headers[i]), widths[i]) for i in range(nr_cols)]

    # print headers and separator if needed
    if headers is not None:
        print(" | ".join(str(headers[i]).ljust(widths[i]) for i in range(nr_cols)))
        print("-" * (sum(widths) + nr_cols * 3 - 1)) # ' | ' = 3 chars; and -1 because last colum has no separator

    for row in data:
        print(" | ".join(str(row[i]).ljust(widths[i]) for i in range(nr_cols)))

# def recursive(list_of_independent_variables, depth, curr, result):
#     if depth == len(list_of_independent_variables):
#         result.append(curr)
#         return
# 
#     # for level in list_of_independent_variables[depth]:
#     #     trial =  recursive(depth+1, list_of_independent_variables, result + [level])
#     #     matrix.append(trial)
#     # return matrix
#     for i in range(len(list_of_independent_variables[depth])):
#         recursive(list_of_independent_variables, depth+1, curr + [list_of_independent_variables[depth][i]], result)

def get_permutation_matrix(list_of_independent_variables):
    # result = []
    # recursive(list_of_independent_variables, 0, [], result)

    result = [[]]
    for lst in list_of_independent_variables:
        temp = []
        for res in result:
            for item in lst:
                temp.append(res + [item])
        result = temp

    return result
