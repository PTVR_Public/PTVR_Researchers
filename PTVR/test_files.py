# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 20:03:20 2023

@author: chopi
"""

import os

for path, subdirs, files in os.walk("Data"):
    for name in files:
        if (name[-3:] == ".py"):
            print (name)
            print(os.path.join(path, name))