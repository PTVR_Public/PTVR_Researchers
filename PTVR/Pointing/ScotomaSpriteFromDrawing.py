# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 09:28:26 2024

@author: chopi
"""

"""
Created on Mon Mar 14 09:18:49 2022

ReticuleGenerator.py is a tool to generate PNGs, specialized for reticles.
Usage example: 
    rt = CursorGenerator ()
    rt.draw.rectangle(xy = (50, 50, 450, 450), fill=(0, 192, 192,a), outline=(255, 255, 255), width = 20)
    rt.draw.ellipse(xy = ((50, 50), (450, 450)), fill=(r, g, b, a), outline=(0, 0, 0))
    rt.draw.line((0, 0, 500, 500), fill=(255, 255, 0), width=10)
    rt.draw.line((0, 500, 500, 0), fill=(255, 255, 0), width=10)
    rt.Show ()
    rt.GeneratePNG ("myReticle.png")
    ptr.sprite = "myReticle.png" #ptr is a PointingCursor.
    
    
@author: synth
"""
import base64
from io import BytesIO  
import hashlib
#Import the library
from PIL import Image, ImageDraw, ImageFilter 

import PTVR.SystemUtils
import numpy as np
import sys

import PTVR.Stimuli.Color as color

class ScotomaSpriteFromDrawing():
    """
    More details on Website's Documentation -> User Manual -> Pointing at an object
    """
    resourcesPath = PTVR.SystemUtils.UnityPTVRRoot+"\\PTVR_Researchers\\PTVR_Operators\\resources\\"
    def __init__(self, image_resolution_pix=500, scotoma_color = color.RGBColor(r= 0,g=0,b=0,a=1)):
        """
        

        Parameters
        ----------

        Returns
        -------
        None.

        """
        
        print ('\n\n\n\n\x1b[0;33;40m' + "Error : ScotomaSpriteFromDrawing is deprecated, please use ScotomaImageFromDrawing instead" + '\x1b[0m\n\n\n\n')
        sys.exit()
        

    def Show(self):
        """
        Draw in a window the generated image.

        Returns
        -------
        None.

        """
        self.image.show()
        
    def GetPositionOfInAngle(self):
        """
        Allows to calculate the pixel ratio betwwen the out angle and in angle.
        Used mostly internally.

        Returns
        -------
        posInX : float
            The ratio for the X in angle and out angle.
        posInY : float
            The ratio for the X in angle and out angle.

        """
        SinX = np.tan(self.reticle_inner_x_radius_in_degrees/180*np.pi)
        SinY = np.tan(self.reticle_inner_y_radius_in_degrees/180*np.pi)
        SoutX = np.tan(self.reticle_outer_x_radius_in_degrees/180*np.pi)
        SoutY = np.tan(self.reticle_outer_y_radius_in_degrees/180*np.pi)
        
        
        SratioX = SoutX / SinX
        SratioY = SoutY / SinY
        
        posInX = self.image_resolution_x_in_pixels/SratioX
        posInY = self.image_resolution_y_in_pixels/SratioY
        
        #print(posInX,posInY)
        
        return posInX, posInY
        
    def GenerateDefaultCursor(self):
        """
        This produce the default cursor taking in account the ratio between
        the inner and outer angles. The generated cursor can be drawn or 
        used to produce a PNG.
        Mostly used internaly
        
          _____
         /  |  \
        /   |   \
        |__    __|
        |        |
         \   |  /
          \__|_/
        
        The external circle is defined by AngularSizeOut
        The internal extremity of the lines by AngularSizeIn
        
        Parameters
        ----------
        reticle_color : TYPE
            The color of the reticle lines.
        reticle_line_width_px : TYPE
            the reticle lines width.

        Returns
        Returns
        -------
        None.

        """
        self.reticle_color_r = 1.0
        self.reticle_color_g = 1.0
        self.reticle_color_b = 1.0
        
        added_pixels = 0
        
        self.draw.ellipse(xy = ((0,0), (self.image_resolution_x_in_pixels - added_pixels, self.image_resolution_y_in_pixels - added_pixels)),
                          fill=(int(self.scotoma_color.r*255), int(self.scotoma_color.g*255),
                               int(self.scotoma_color.b*255), int(self.scotoma_color.a*255)),
                          outline=(int(self.scotoma_color.r*255) , int(self.scotoma_color.g*255),
                                   int(self.scotoma_color.b*255), int(self.scotoma_color.a*255)))
        
        #self.image = self.image.filter(ImageFilter.GaussianBlur(radius = 10)) 
        # #and a couple of lines
        # self.draw.line((self.image_resolution_x_in_pixels/2, self.reticle_line_width_px/2, self.image_resolution_x_in_pixels/2, self.image_resolution_y_in_pixels/2-posInY/2), fill=(int(self.reticle_color.r*255) , int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        # self.draw.line((self.reticle_line_width_px/2,self.image_resolution_y_in_pixels/2, self.image_resolution_x_in_pixels/2-posInX/2, self.image_resolution_y_in_pixels/2), fill=(int(self.reticle_color.r*255) , int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        
        # self.draw.line((self.image_resolution_x_in_pixels/2, self.image_resolution_y_in_pixels - self.reticle_line_width_px/2, self.image_resolution_x_in_pixels/2, posInY/2+self.image_resolution_y_in_pixels/2), fill=(int(self.reticle_color.r*255) , int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        # self.draw.line((self.image_resolution_x_in_pixels - self.reticle_line_width_px/2,self.image_resolution_y_in_pixels/2, self.image_resolution_x_in_pixels/2+posInX/2, self.image_resolution_y_in_pixels/2), fill=(int(self.reticle_color.r*255) , int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        
        
        
        
    def Initialize (self, new_image_resolution_pix=500, new_reticle_inner_radius=1, 
                 newreticle_outer_radius_deg=3,
                 isDefaultReticle=True, reticle_color=color.RGBColor(1,1,1), reticle_line_width_deg=10):
        """
        Reset for an empty image with resolutions self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels
        new_image_resolution_pix : TYPE, optional
            Resolution of the reticle in pixels. The default is 500.
        new_reticle_inner_radius_deg : TYPE, optional
            The inner radius of the reticle. The default is 1°.
        new_reticle_outer_radius_deg : TYPE, optional
            The outer radius of the reticle. The default is 3°.
        isDefaultReticle : TYPE, optional
            If you want to create the default reticle. See GenerateDefaultCursor. The default is True.
        reticle_color : TYPE, optional
            the color of the reticle lines. The default is color.RGBColor(0,0,0).
        reticle_line_width : TYPE, optional
            the width of the reticle lines in pixels. The default is 10.
        Returns
        -------
        None.

        """
        self.__init__(new_image_resolution_pix, new_reticle_inner_radius, 
                      newreticle_outer_radius_deg,
                      isDefaultReticle, reticle_line_width_deg)
        
 
    def GeneratePNG (self, name):
        """
        

        Parameters
        ----------
        name : string
            Generate the PNG based on the given drawing.

        Returns
        -------
        None.

        """

        self.image.save(self.resourcesPath + "Images\\" + "Scotomas\\" +  "scotoma_from_drawing.png")
        self.image_path = self.resourcesPath + "Images\\" + "Scotomas\\" +  "scotoma_from_drawing.png"
        
    def HashReticleImage (self): 
        buffered = BytesIO()
        self.image.save(buffered, format="PNG")
        img_str = hashlib.md5(base64.b64encode(buffered.getvalue())).hexdigest() 
        return img_str
        
        
        
        
        
        
        
        
        
        
        
        