"""
The pointing classes. 
"""

import json
from enum import Enum, auto
import numpy as np
import PTVR.Stimuli.Color as color
from PTVR.Pointing.ReticleImageFromDrawing import ReticleImageFromDrawing as PNGReticle
from PTVR.Data.Event import PointedAt
from PTVR.Stimuli.Utils import VirtualPoint2D
import PTVR.SystemUtils
import copy
import os.path
import os
import sys
sys.path.append("../Stimuli")


class ContingentType(Enum):
    HEADSET = auto()
    GAZE = auto()
    LEFT_EYE = auto()
    RIGHT_EYE = auto()
    LEFT_HAND = auto()
    RIGHT_HAND = auto()


class ReticleContingency(Enum):
    HEADSET = auto()
    GAZE = auto()
    LEFT_EYE = auto()
    RIGHT_EYE = auto()


class ScotomaContingency(Enum):
    HEADSET = auto()
    GAZE = auto()
    LEFT_EYE = auto()
    RIGHT_EYE = auto()


def SpriteContingency(Enum):
    HEADSET = auto()
    GAZE = auto()
    LEFT_EYE = auto()
    RIGHT_EYE = auto()
    LEFT_HAND = auto()
    RIGHT_HAND = auto()
    print('\n\n\n\n\x1b[0;33;40m' +
          "Error : SpriteContingency is deprecated, please use ImageContingency instead" + '\x1b[0m\n\n\n\n')
    # sys.exit()


class ImageContingency(Enum):
    HEADSET = auto()
    GAZE = auto()
    LEFT_EYE = auto()
    RIGHT_EYE = auto()
    LEFT_HAND = auto()
    RIGHT_HAND = auto()


class LaserContingency(Enum):
    LEFT_HAND = auto()
    RIGHT_HAND = auto()


class MaskedEye(Enum):
    LEFT_EYE = auto()
    RIGHT_EYE = auto()
    NONE = auto()


class EyeWithReticle(Enum):
    LEFT_EYE = auto()
    RIGHT_EYE = auto()
    NONE = auto()


class PointingCursor(VirtualPoint2D):
    """

    """

    def __init__(self, cursor_color=color.RGBColor(1.0, 1.0, 1.0),
                 contingency_type=ContingentType.GAZE,
                 masked_eye=MaskedEye.NONE,
                 pointing_cursor_ecc_hm=np.array([0.0, 0.0]),
                 is_rendered_over_objects=True,
                 pointing_method="cone",
                 display_pointing_method=False,
                 is_distance_constant=False,
                 maxDistance=500,
                 name="default",
                 x_size_in_degrees=5,
                 y_size_in_degrees=5,
                 laserColor=color.RGBColor(1.0, 0.0, 0.0)):
        VirtualPoint2D.__init__(self)
        # If the pointer is contingent
        self.name = name
        # uniq id for the pointer
        # if it's active in the scene, or invisile
        self.active = True
        # only straight ones can be implemented. Later bezier and other will be added.
        self.type = "straight"
        # if the pointe sprite size is constant
        # self.distanceRescale= distance_rescale
        # the sprite path. Default will load or a default one, or the last loaded.
        self.sprite = "default"

        self.eccentricity = float(pointing_cursor_ecc_hm[0])/180.*np.pi
        self.half_meridian = float(pointing_cursor_ecc_hm[1])/180.*np.pi
        # TODO: add max distance and no hit to the reticle
        # the maximum distance of projection
        self.maxDistance = maxDistance
        self.distance_if_empty_cursor_cone = 0.5
        self.isShowDistanceInM = False
        # The number of position for smooth
        # self.smoothingStep = smoothing_step
        self.color = cursor_color
        # self.alphaSmooth = alpha_smooth
        # is the cursor project on gameobjects
        self.isProjected = False
        self.contingency_type = contingency_type.name
        self.maskedEye = masked_eye.name
        self.isRenderedOverObjects = is_rendered_over_objects
        self.spritePath = PTVR.SystemUtils.UnityPTVRRoot + \
            "PTVR_Researchers\\PTVR_Operators\\resources\\Images\\Reticles\\"
        self.pointing_method = pointing_method
        self.display_pointing_method = display_pointing_method
        self.is_distant_constant = is_distance_constant
        if (is_distance_constant):
            self.isRenderedOverObjects = True
        self.spriteAngularRadiusY = y_size_in_degrees
        self.spriteAngularRadiusX = x_size_in_degrees
        self.laserColor = laserColor
        # the default is 0.5 m: it is a distance where convergence of the eyes is easy. A smaller distance might make it difficulty for the eyes to converge.
        self.minimum_distance_if_non_empty_cursor_cone = 0.5
        self.objects_id = np.array([-1])
        self.head_cursor_id = np.array([-1])
        self.lookAtHeadset = False
        self.display_laser = False
        self.is_scotoma = False

    def look_at_headset(self):
        self.lookAtHeadset = True

    def set_augmented_vision(self, objects_id, head_cursor_id):
        self.augmented_vision = True
        self.objects_id = objects_id
        self.head_cursor_id = head_cursor_id

    def SetMaxDistanceProjectionOptions(self, max_distance, is_projected):
        self.maxDistance = max_distance
        self.isProjected = is_projected

    def SetPerimetricCoordinates(self, eccentricity, half_meridian):
        self.eccentricity = float(eccentricity)/180.*np.pi
        self.half_meridian = float(half_meridian)/180.*np.pi

    def SetCustomReticleGenerator(self, reticleGen, path_to_image=None):
        """
        Method that use the PTVR.PointingCursor.ReticleGenerator for the reticle image

        Parameters
        ----------
        reticleGen : PTVR.PointingCursor.ReticleGenerator
            The reticle generator that provide the reticle angles and produced PNG.
            The reticle have to be setup before this method.

        Returns
        -------
        None.

        """

        h = reticleGen.HashReticleImage()

        sprite_name = ("parametrizedReticle" +
                       # uniq number for the case we generate more than one reticle.
                       h +
                       ".png")

        reticleGen.GeneratePNG(sprite_name)

        self.sprite = sprite_name
        self.spritePath = reticleGen.image_path

    def set_display_distance_in_m(self):
        self.isShowDistanceInM = True

    def clean(self):
        print()


class BinocularReticle(PointingCursor):
    """
    Same as MonocularReticle but  the reticle is binocular.
    """

    def __init__(self, contingency_type=ReticleContingency.HEADSET, pointing_cursor_ecc_hm=np.array([0.0, 0.0]),
                 distance=0.70, cursor_color=color.RGBColor(1.0, 1.0, 1.0), is_rendered_over_objects=False,
                 distance_if_empty_cursor_cone=500):
        if (contingency_type == ReticleContingency.HEADSET):
            contingent = ContingentType.HEADSET
        if (contingency_type == ReticleContingency.LEFT_EYE):
            contingent = ContingentType.LEFT_EYE
        if (contingency_type == ReticleContingency.RIGHT_EYE):
            contingent = ContingentType.RIGHT_EYE
        if (contingency_type == ReticleContingency.GAZE):
            contingent = ContingentType.GAZE
        PointingCursor.__init__(self, cursor_color=cursor_color, pointing_cursor_ecc_hm=pointing_cursor_ecc_hm,
                                contingency_type=contingent, is_rendered_over_objects=is_rendered_over_objects)
        print(
            "BinocularReticle is a Legacy class, please use ImageToContingentCursor instead")
        self.maxDistance = distance
        self.distance_if_empty_cursor_cone = distance_if_empty_cursor_cone
        self.isProjected = False
        self.pointing_method = False

    def SetDistance(self, distance):
        self.maxDistance = distance

    def clean(self):
        print()


class ImageToContingentCursor(PointingCursor):
    """
    Details on Website's Documentation -> User Manual -> Pointing at an object.

    Parameters
    ----------
    image : image created by ReticleImageFromDrawing (), ScotomaImageFromDrawing (),
        ImageFromDrawing () or ImageFromLoading ().
        This image, which is actually a object containing an image, is
        transformed by ImageToContingentCursor() into a flat cursor, i.e. into
        a 2D image contingent on a pointing device - hand, head, ... 
        (like a mouse cursor being contingent on the user's hand).

    contingency_type : 
        ImageContingency.HEADSET, (default)
        ImageContingency.LEFT_EYE, 
        ImageContingency.RIGHT_EYE, 
        ImageContingency.GAZE,
        ImageContingency.LEFT_HAND
        ImageContingency.RIGHT_HAND
    
    eye_with_contingent_cursor :
        "both",  
        "left",  
        "right" 

    Notes : 
    --------    
        sprite_PTVR_object is deprecated, instead use : 'image'

        x_size_in_degrees and y_size_in_degrees are deprecated : 
        instead use:  size_in_degrees = [ argument for x , argument for y ].          
        ex: if 'image' is a circle, then size_in_degrees represents
        the circle's diameter.
    """

    def __init__(self, contingency_type=ImageContingency.HEADSET, image=None,
                 contingent_cursor_ecc_hm=np.array([0.0, 0.0]),
                 eye_with_contingent_cursor="both",
                 display_cursor_cone=False,
                 distance_if_empty_cursor_cone=500,
                 is_distance_constant=False,
                 constant_distance=500, size_in_degrees=[5, 5],
                 x_size_in_degrees=None, y_size_in_degrees=None,
                 is_rendered_over_all_flat_cursors =False):

        if (contingency_type == ImageContingency.HEADSET):
            contingent = ContingentType.HEADSET
        if (contingency_type == ImageContingency.LEFT_EYE):
            contingent = ContingentType.LEFT_EYE
        if (contingency_type == ImageContingency.RIGHT_EYE):
            contingent = ContingentType.RIGHT_EYE
        if (contingency_type == ImageContingency.GAZE):
            contingent = ContingentType.GAZE
        if (contingency_type == ImageContingency.LEFT_HAND):
            contingent = ContingentType.LEFT_HAND
        if (contingency_type == ImageContingency.RIGHT_HAND):
            contingent = ContingentType.RIGHT_HAND

        is_rendered_over_objects = True

        if (x_size_in_degrees is not None) or (y_size_in_degrees is not None):
            print('\n\n\n\n\x1b[0;33;40m' +
                  "Warning : x_size_in_degrees and y_size_in_degrees are deprecated, please use size_in_degrees" + '\x1b[0m\n\n\n\n')

        x_size_in_degrees = size_in_degrees[0]
        y_size_in_degrees = size_in_degrees[1]

        if (eye_with_contingent_cursor == "both"):
            PointingCursor.__init__(self, cursor_color=color.RGBColor(1.0, 1.0, 1.0), pointing_method="cone", display_pointing_method=display_cursor_cone,
                                    pointing_cursor_ecc_hm=contingent_cursor_ecc_hm, contingency_type=contingent, is_rendered_over_objects=is_rendered_over_objects,
                                    is_distance_constant=is_distance_constant, maxDistance=constant_distance, x_size_in_degrees=x_size_in_degrees, y_size_in_degrees=y_size_in_degrees)
        elif (eye_with_contingent_cursor == "right"):
            PointingCursor.__init__(self, cursor_color=color.RGBColor(1.0, 1.0, 1.0), pointing_method="cone", display_pointing_method=display_cursor_cone,
                                    pointing_cursor_ecc_hm=contingent_cursor_ecc_hm, contingency_type=contingent, masked_eye=MaskedEye.RIGHT_EYE,
                                    is_rendered_over_objects=is_rendered_over_objects, is_distance_constant=is_distance_constant, maxDistance=constant_distance, x_size_in_degrees=x_size_in_degrees, y_size_in_degrees=y_size_in_degrees)
        elif (eye_with_contingent_cursor == "left"):
            PointingCursor.__init__(self, cursor_color=color.RGBColor(1.0, 1.0, 1.0), pointing_method="cone", display_pointing_method=display_cursor_cone,
                                    pointing_cursor_ecc_hm=contingent_cursor_ecc_hm, contingency_type=contingent, masked_eye=MaskedEye.LEFT_EYE,
                                    is_rendered_over_objects=is_rendered_over_objects, is_distance_constant=is_distance_constant, maxDistance=constant_distance, x_size_in_degrees=x_size_in_degrees, y_size_in_degrees=y_size_in_degrees)

        self.distance_if_empty_cursor_cone = distance_if_empty_cursor_cone
        self.isProjected = True

        self.SetCustomReticleGenerator(image)
        self.is_scotoma = is_rendered_over_all_flat_cursors

        # if (path_to_contingent_sprite_image is None):

        #     self.spritePath +=  "Images\\"

        # else:

        #     self.spritePath +=  path_to_contingent_sprite_image

    def SetDistance(self, distance):
        self.maxDistance = distance

    def clean(self):
        print()


class AugmentedVisionROICursor(PointingCursor):
    """
    Details on Website's Documentation -> User Manual -> Pointing at an object.

    Parameters
    ----------
    image : image created by ReticleImageFromDrawing (), ScotomaImageFromDrawing (),
        ImageFromDrawing () or ImageFromLoading ().
        This image, which is actually a object containing an image, is
        transformed by ImageToContingentCursor() into a flat cursor, i.e. into
        a 2D image contingent on a pointing device - hand, head, ... 
        (like a mouse cursor being contingent on the user's hand).

    contingency_type : 
        ImageContingency.HEADSET, (default)
        ImageContingency.LEFT_EYE, 
        ImageContingency.RIGHT_EYE, 
        ImageContingency.GAZE,
        ImageContingency.LEFT_HAND
        ImageContingency.RIGHT_HAND

    Notes : 
    --------    
        sprite_PTVR_object is deprecated, instead use : 'image'

        x_size_in_degrees and y_size_in_degrees are deprecated : 
        instead use:  size_in_degrees = [ argument for x , argument for y ].          
        ex: if 'image' is a circle, then size_in_degrees represents
        the circle's diameter.
    """

    def __init__(self, contingency_type=ImageContingency.HEADSET, image=None,
                 contingent_cursor_ecc_hm=np.array([0.0, 0.0]),
                 eye_with_contingent_cursor="both",
                 display_cursor_cone=False,
                 distance_if_empty_cursor_cone=500,
                 is_distance_constant=False,
                 constant_distance=500, size_in_degrees=[5, 5],
                 list_of_ROI_id=np.array([-1]),
                 list_of_ROI_circle_id=np.array([-1]),
                 pointing_method="cone",
                 x_size_in_degrees=None, y_size_in_degrees=None):

        if (contingency_type == ImageContingency.HEADSET):
            contingent = ContingentType.HEADSET
        if (contingency_type == ImageContingency.LEFT_EYE):
            contingent = ContingentType.LEFT_EYE
        if (contingency_type == ImageContingency.RIGHT_EYE):
            contingent = ContingentType.RIGHT_EYE
        if (contingency_type == ImageContingency.GAZE):
            contingent = ContingentType.GAZE
        if (contingency_type == ImageContingency.LEFT_HAND):
            contingent = ContingentType.LEFT_HAND
        if (contingency_type == ImageContingency.RIGHT_HAND):
            contingent = ContingentType.RIGHT_HAND

        is_rendered_over_objects = True

        if (x_size_in_degrees is not None) or (y_size_in_degrees is not None):
            print('\n\n\n\n\x1b[0;33;40m' +
                  "Warning : x_size_in_degrees and y_size_in_degrees are deprecated, please use size_in_degrees" + '\x1b[0m\n\n\n\n')

        x_size_in_degrees = size_in_degrees[0]
        y_size_in_degrees = size_in_degrees[1]

        # pointing_method = "ray"  # "cone"

        if (eye_with_contingent_cursor == "both"):
            PointingCursor.__init__(self, cursor_color=color.RGBColor(1.0, 1.0, 1.0), pointing_method=pointing_method, display_pointing_method=display_cursor_cone,
                                    pointing_cursor_ecc_hm=contingent_cursor_ecc_hm, contingency_type=contingent, is_rendered_over_objects=is_rendered_over_objects,
                                    is_distance_constant=is_distance_constant, maxDistance=constant_distance, x_size_in_degrees=x_size_in_degrees, y_size_in_degrees=y_size_in_degrees)
        elif (eye_with_contingent_cursor == "right"):
            PointingCursor.__init__(self, cursor_color=color.RGBColor(1.0, 1.0, 1.0), pointing_method=pointing_method, display_pointing_method=display_cursor_cone,
                                    pointing_cursor_ecc_hm=contingent_cursor_ecc_hm, contingency_type=contingent, masked_eye=MaskedEye.RIGHT_EYE,
                                    is_rendered_over_objects=is_rendered_over_objects, is_distance_constant=is_distance_constant, maxDistance=constant_distance, x_size_in_degrees=x_size_in_degrees, y_size_in_degrees=y_size_in_degrees)
        elif (eye_with_contingent_cursor == "left"):
            PointingCursor.__init__(self, cursor_color=color.RGBColor(1.0, 1.0, 1.0), pointing_method=pointing_method, display_pointing_method=display_cursor_cone,
                                    pointing_cursor_ecc_hm=contingent_cursor_ecc_hm, contingency_type=contingent, masked_eye=MaskedEye.LEFT_EYE,
                                    is_rendered_over_objects=is_rendered_over_objects, is_distance_constant=is_distance_constant, maxDistance=constant_distance, x_size_in_degrees=x_size_in_degrees, y_size_in_degrees=y_size_in_degrees)

        self.distance_if_empty_cursor_cone = distance_if_empty_cursor_cone
        self.isProjected = True

        self.SetCustomReticleGenerator(image)
        self.set_augmented_vision(list_of_ROI_id, list_of_ROI_circle_id)

    def SetDistance(self, distance):
        self.maxDistance = distance

    def clean(self):
        print()


class SpriteToContingentCursor(PointingCursor):
    """
    Details on Website's Documentation -> User Manual -> Pointing at an object.

    Parameters
    ----------
    contingency_type : 
        ImageContingency.HEADSET, 
        ImageContingency.LEFT_EYE, 
        ImageContingency.RIGHT_EYE, 
        ImageContingency.GAZE,
        ImageContingency.LEFT_HAND
        ImageContingency.RIGHT_HAND

        The default is ImageContingency.HEADSET.

        x_size_in_degrees and y_size_in_degrees are deprecated : 
        instead use:  size_in_degrees = [ argument for x , argument for y ].

        ex: if 'image' is a circle, then size_in_degrees represents
        the circle's diameter.
    """

    def __init__(self, contingency_type=ImageContingency.HEADSET, image=None,
                 contingent_cursor_ecc_hm=np.array([0.0, 0.0]),
                 eye_with_contingent_cursor="both",
                 display_cursor_cone=False,
                 distance_if_empty_cursor_cone=500,
                 is_distance_constant=False,
                 constant_distance=500, size_in_degrees=[5, 5],
                 x_size_in_degrees=None, y_size_in_degrees=None):

        if (x_size_in_degrees is not None) or (y_size_in_degrees is not None):
            print('\n\n\n\n\x1b[0;33;40m' +
                  "Error : SpriteToContingentCursor is deprecated, please use ImageToContingentCursor instead" + '\x1b[0m\n\n\n\n')
            sys.exit()


class MonocularReticle(PointingCursor):
    '''
    Create a contingent monocular reticle allowing to point at any object
    with different kinds of contingency, i.e. by moving your Head,
    left eye, right eye or gaze.

    Parameters
    ----------
    contingency_type : 
        ReticleContingency.HEADSET, 
        ReticleContingency.LEFT_EYE, 
        ReticleContingency.RIGHT_EYE, 
        ReticleContingency.GAZE. 
        The default is ReticleContingency.HEADSET.

    masked_eye : 
        DESCRIPTION. The default is MaskedEye.RIGHT_EYE.
    pointing_cursor_ecc_hm : 2D vector
        Eccentricity and Half-meridian of pointing cone.
        The default is np.array([0.0, 0.0]).
    distance : TYPE, optional
        in meters: distance of the monocular reticle from headset. The default is 70.0.
    cursor_color : 
        DESCRIPTION. The default is color.RGBColor(1.0,1.0,1.0).
    is_rendered_over_objects : 
        If True, the reticle will be displayed in front of any object (even if
        the object is actually in front of the reticle).
        The default is False.

    Returns
    -------
    None.

    '''

    def __init__(self, contingency_type=ReticleContingency.HEADSET, eye_with_reticle=MaskedEye.RIGHT_EYE,
                 pointing_cursor_ecc_hm=np.array([0.0, 0.0]), distance=70.0, cursor_color=color.RGBColor(1.0, 1.0, 1.0),
                 is_rendered_over_objects=True, distance_if_empty_cursor_cone=500):

        print(
            "MonocularReticle is a Legacy class, please use ImageToContingentCursor instead")
        if (contingency_type == ReticleContingency.HEADSET):
            contingent = ContingentType.HEADSET
        if (contingency_type == ReticleContingency.LEFT_EYE):
            contingent = ContingentType.LEFT_EYE
        if (contingency_type == ReticleContingency.RIGHT_EYE):
            contingent = ContingentType.RIGHT_EYE
        if (contingency_type == ReticleContingency.GAZE):
            contingent = ContingentType.GAZE
        PointingCursor.__init__(self, cursor_color=cursor_color, pointing_cursor_ecc_hm=pointing_cursor_ecc_hm,
                                contingency_type=contingent, masked_eye=eye_with_reticle, is_rendered_over_objects=is_rendered_over_objects)
        self.maxDistance = distance
        self.distance_if_empty_cursor_cone = distance_if_empty_cursor_cone
        self.isProjected = False
        self.pointing_method = False

    def SetDistance(self, distance):
        self.maxDistance = distance

    def clean(self):
        print()


class PointingLaser(PointingCursor):
    """

    """

    def __init__(self, hand_laser=LaserContingency.RIGHT_HAND,  pointing_beam_ecc_hm=np.array([0.0, 0.0]),
                 laser_color=color.RGBColor(1.0, 1.0, 1.0),
                 laser_width=0.01):
        if (hand_laser == LaserContingency.LEFT_HAND):
            contingency_type = ContingentType.LEFT_HAND
        else:
            contingency_type = ContingentType.RIGHT_HAND
        # PointingCursor.__init__(self,cursor_color = cursor_color, pointing_cursor_ecc_hm = pointing_cursor_ecc_hm,contingency_type = contingency_type,masked_eye =MaskedEye.NONE,is_rendered_over_objects = is_rendered_over_objects)

        PointingCursor.__init__(self, cursor_color=color.RGBColor(1.0, 1.0, 1.0), pointing_method="ray", display_pointing_method=False,
                                contingency_type=contingency_type, is_rendered_over_objects=False,
                                is_distance_constant=False, maxDistance=500, pointing_cursor_ecc_hm=pointing_beam_ecc_hm)

        self.distance_if_empty_cursor_cone = 500
        self.isProjected = True
        self.laserColor = laser_color
        self.widthCollision = laser_width
        self.laserWidth = laser_width
        self.isProjected = True
        self.display_laser = True


class headset_contingent (PointingCursor):
    """
    The pointer direction depends on the head direction.
    """

    def __init__(self, smoothing_step=0, distance_rescale=True, is_show_distance_in_m=False, alpha_smooth=0.0, cursor_color=color.RGBColor(1.0, 1.0, 1.0), pointing_cursor_ecc_hm=np.array([0.0, 0.0])):
        PointingCursor.__init__(self, distance_rescale, is_show_distance_in_m,
                                alpha_smooth, cursor_color, "COMBINE", pointing_cursor_ecc_hm, True)
        self.origin = "pov"

    def clean(self):
        print()


class gaze_contingent (PointingCursor):
    """
    The pointer direction depends on the eyes watching direction.
    gaze_type : can be COMBINED, LEFT, RIGHT by default is COMBINED. 
    COMBINED is corresponding to an origin of the center of headset with the ray direction follow:
          (left_gaze_direction_normalized * left_eye_openess + right_gaze_direction_normalized * right_eye_openess) / (left_eye_openess  + right_eye_openess)
    Note : left_eye_openess and right_eye_openess corresponding to the open ness of the eye 0 is full close and 1 is full open.
    For the COMBINED if right and left eyes is close the direction while be Headset direction
    """

    def __init__(self, gaze_type="COMBINED", smoothing_step=0, distance_rescale=True, is_show_distance_in_m=False, alpha_smooth=0.0, cursor_color=color.RGBColor(1.0, 1.0, 1.0), pointing_cursor_ecc_hm=np.array([0.0, 0.0])):
        PointingCursor.__init__(self, smoothing_step, distance_rescale, is_show_distance_in_m,
                                alpha_smooth, cursor_color, gaze_type, pointing_cursor_ecc_hm, True)
        self.origin = "gaze"

    def clean(self):
        print()


class hands_contingent (PointingCursor):
    """
    The pointer direction depends on the hand-controller direction.
    hand_type : can be LEFT , RIGHT. Default is RIGHT. 
    """

    def __init__(self, hand_type="RIGHT", smoothing_step=0, distance_rescale=True, is_show_distance_in_m=False, alpha_smooth=0.0, cursor_color=color.RGBColor(1.0, 1.0, 1.0), pointing_cursor_ecc_hm=np.array([0.0, 0.0])):
        PointingCursor.__init__(self, smoothing_step, distance_rescale, is_show_distance_in_m,
                                alpha_smooth, cursor_color, hand_type, pointing_cursor_ecc_hm, True)
        self.origin = "hand"

    def clean(self):
        print()


class object_contingent (PointingCursor):
    def __init__(self):
        PointingCursor.__init__(self)
        self.origin = "pov"

    def clean(self):
        print()


class ReticleGenerator(PointingCursor):
    """
    """

    def __init__(self, target_id=-1, reticle_eccentricity=0.0, reticle_half_meridian_angle=0.0, pointing_area_radius_deg=2.0, reticle_outer_radius_deg=6.0, reticle_inner_radius_deg=2.0, reticle_line_width_deg=1.0, contingent="pov"):
        VirtualPoint2D.__init__(self)
        # If the pointer is contingent
        self.origin = contingent
        self.name = "default"
        # uniq id for the pointer
        # if it's active in the scene, or invisile
        self.active = True
        # only straight ones can be implemented. Later bezier and other will be added.
        self.type = "straight"
        # if the pointe sprite size is constant
        self.distanceRescale = True
        # the sprite path. Default will load or a default one, or the last loaded.
        self.sprite = "default"
        # in degrees. If no rescale, at 1 m distance
        self.spriteAngularRadiusY = 2.
        image_resolution_pix = 2048
        # in degrees
        self.spriteAngularRadiusX = 2.
        # TODO: add max distance and no hit to the reticle
        # the maximum distance of projection
        self.maxDistance = 500
        self.isShowDistanceInM = False
        # The number of position for smooth
        self.smoothingStep = 0

        self.alphaSmooth = 0.0
        # is the cursor project on gameobjects
        self.isProjected = True

        # Simple reticle generator implementation
        self.set_perimetric_angles(
            reticle_eccentricity, reticle_half_meridian_angle)
        reticle_gen = PNGReticle.ReticleGenerator(image_resolution_pix=image_resolution_pix,
                                                  reticle_outer_radius_deg=reticle_outer_radius_deg,
                                                  reticle_inner_radius_deg=reticle_inner_radius_deg,
                                                  reticle_color=color.RGBColor(
                                                      1.0, 1.0, 1.0, 1.0),
                                                  reticle_line_width_deg=reticle_line_width_deg)

        self.PointedIn = PointedAt(radius_deg=pointing_area_radius_deg, depth=self.maxDistance, pointer_ecc_hm=VirtualPoint2D(
            perimetric_coordinates=np.array([reticle_eccentricity, reticle_half_meridian_angle])), mode="press", target_id=target_id)
        self.PointedOut = PointedAt(radius_deg=pointing_area_radius_deg, depth=self.maxDistance, pointer_ecc_hm=VirtualPoint2D(
            perimetric_coordinates=np.array([reticle_eccentricity, reticle_half_meridian_angle])), mode="release", target_id=target_id)
        self.PointedStay = PointedAt(radius_deg=pointing_area_radius_deg, depth=self.maxDistance, pointer_ecc_hm=VirtualPoint2D(
            perimetric_coordinates=np.array([reticle_eccentricity, reticle_half_meridian_angle])), mode="hold", target_id=target_id)
        self.SetCustomReticleGenerator(reticle_gen)

    def clean(self):
        del self.PointedIn
        del self.PointedOut
        del self.PointedStay
