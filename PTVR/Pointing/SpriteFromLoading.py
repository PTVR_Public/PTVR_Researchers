# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 12:04:07 2024

@author: chopi
"""

"""
Created on Mon Mar 18 11:39:15 2024

@author: chopi
"""

import base64
from io import BytesIO  
import hashlib
#Import the library
from PIL import Image, ImageDraw, ImageFilter 
import os
import os.path
import PTVR.SystemUtils
import numpy as np
import sys
import PTVR.Stimuli.Color as color

class SpriteFromLoading():
    """
    This class generates an object containing a sprite-like 2D image after 
    loading a png image).
    This object must be passed to ImageToContingentCursor() where
    it will be converted to a flat cursor. In other words, you cannot display the 
    ImageFromLoading() object itself.
    
    
    More details on Website's Documentation -> User Manual -> Pointing at an object
    """
    resourcesPath = PTVR.SystemUtils.UnityPTVRRoot+"\\PTVR_Researchers\\PTVR_Operators\\resources\\"
    def __init__(self, image_resolution_pix=500, scotoma_color = color.RGBColor(1,1,1),
                 path_to_sprite_image_folder=None,
                 image_file="full_scotoma.png"):
        """
        

        Parameters
        ----------

        Returns
        -------
        None.

        """
        
        print ('\n\n\n\n\x1b[0;33;40m' + "Error : SpriteFromLoading is deprecated, please use ImageFromLoading instead" + '\x1b[0m\n\n\n\n')
        sys.exit()
        
    def Show(self):
        """
        Draw in a window the generated image.

        Returns
        -------
        None.

        """
        self.image.show()
        

        
    def GenerateDefaultCursor(self):
        """
        This produce the default cursor taking in account the ratio between
        the inner and outer angles. The generated cursor can be drawn or 
        used to produce a PNG.
        Mostly used internaly
        
          _____
         /  |  \
        /   |   \
        |__    __|
        |        |
         \   |  /
          \__|_/
        
        The external circle is defined by AngularSizeOut
        The internal extremity of the lines by AngularSizeIn
        
        Parameters
        ----------
        reticle_color : TYPE
            The color of the reticle lines.
        reticle_line_width_px : TYPE
            the reticle lines width.

        Returns
        Returns
        -------
        None.

        """
        self.reticle_color_r = 1.0
        self.reticle_color_g = 1.0
        self.reticle_color_b = 1.0

        self.draw.ellipse(xy = ((0,0), (self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels)),
                          fill=(int(self.scotoma_color.r*255), int(self.scotoma_color.g*255),
                               int(self.scotoma_color.b*255), int(self.scotoma_color.a*255)),
                          outline=(int(self.scotoma_color.r*255) , int(self.scotoma_color.g*255), int(self.scotoma_color.b*255)))
        
        #self.image = self.image.filter(ImageFilter.GaussianBlur(radius = 10)) 
        # #and a couple of lines
        # self.draw.line((self.image_resolution_x_in_pixels/2, self.reticle_line_width_px/2, self.image_resolution_x_in_pixels/2, self.image_resolution_y_in_pixels/2-posInY/2), fill=(int(self.reticle_color.r*255) , int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        # self.draw.line((self.reticle_line_width_px/2,self.image_resolution_y_in_pixels/2, self.image_resolution_x_in_pixels/2-posInX/2, self.image_resolution_y_in_pixels/2), fill=(int(self.reticle_color.r*255) , int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        
        # self.draw.line((self.image_resolution_x_in_pixels/2, self.image_resolution_y_in_pixels - self.reticle_line_width_px/2, self.image_resolution_x_in_pixels/2, posInY/2+self.image_resolution_y_in_pixels/2), fill=(int(self.reticle_color.r*255) , int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        # self.draw.line((self.image_resolution_x_in_pixels - self.reticle_line_width_px/2,self.image_resolution_y_in_pixels/2, self.image_resolution_x_in_pixels/2+posInX/2, self.image_resolution_y_in_pixels/2), fill=(int(self.reticle_color.r*255) , int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        
        
        
        
    def Initialize (self, new_image_resolution_pix=500, new_reticle_inner_radius=1, 
                 newreticle_outer_radius_deg=3,
                 isDefaultReticle=True, reticle_color=color.RGBColor(1,1,1), reticle_line_width_deg=10):
        """
        Reset for an empty image with resolutions self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels
        new_image_resolution_pix : TYPE, optional
            Resolution of the reticle in pixels. The default is 500.
        new_reticle_inner_radius_deg : TYPE, optional
            The inner radius of the reticle. The default is 1°.
        new_reticle_outer_radius_deg : TYPE, optional
            The outer radius of the reticle. The default is 3°.
        isDefaultReticle : TYPE, optional
            If you want to create the default reticle. See GenerateDefaultCursor. The default is True.
        reticle_color : TYPE, optional
            the color of the reticle lines. The default is color.RGBColor(0,0,0).
        reticle_line_width : TYPE, optional
            the width of the reticle lines in pixels. The default is 10.
        Returns
        -------
        None.

        """
        self.__init__(new_image_resolution_pix, new_reticle_inner_radius, 
                      newreticle_outer_radius_deg,
                      isDefaultReticle, reticle_line_width_deg)
        
 
    def GeneratePNG (self, name):
        """
        

        Parameters
        ----------
        name : string
            Generate the PNG based on the given drawing.

        Returns
        -------
        None.

        """


        self.image.save(self.resourcesPath+name)
        
    def HashReticleImage (self): 
        buffered = BytesIO()
        self.image.save(buffered, format="PNG")
        img_str = hashlib.md5(base64.b64encode(buffered.getvalue())).hexdigest() 
        return img_str