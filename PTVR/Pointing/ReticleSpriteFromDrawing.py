# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 09:18:49 2022

"""
import base64
from io import BytesIO
import hashlib
# Import the library
from PIL import Image, ImageDraw

import PTVR.SystemUtils
import numpy as np
import sys
import PTVR.Stimuli.Color as color


class ReticleSpriteFromDrawing():
    """
    Draws a square image i.e. with sides of equal size. Internally, a png image
    of image_resolution_pix pixels by image_resolution_pix pixels is created.
    Inner diameter and line width are expressed as a fraction of the square's
    side.
    More details in Website's Documentation -> User Manual -> Pointing at an object
    """
    resourcesPath = PTVR.SystemUtils.UnityPTVRRoot + \
        "\\PTVR_Researchers\\PTVR_Operators\\resources\\"

    def __init__(self, image_resolution_pix=500,
                 reticle_inner_diameter_as_ratio=0.5,
                 reticle_line_width_as_ratio=0.1,
                 color=color.RGBColor(r=1, g=1, b=1, a=1)):

        print('\n\n\n\n\x1b[0;33;40m' +
              "Error : ReticleSpriteFromDrawing is deprecated, please use ReticleImageFromDrawing instead" + '\x1b[0m\n\n\n\n')
        sys.exit()

    def Show(self):
        """
        Draw the generated image in a PC window .

        Returns
        -------
        None.

        """
        self.image.show()

    def GetPositionOfInAngle(self):
        """
        Allows to calculate the pixel ratio betwwen the out angle and in angle.
        Used mostly internally.

        Returns
        -------
        posInX : float
            The ratio for the X in angle and out angle.
        posInY : float
            The ratio for the X in angle and out angle.

        """
        # SinX = np.tan(self.reticle_inner_x_radius_in_degrees/180*np.pi)
        # SinY = np.tan(self.reticle_inner_y_radius_in_degrees/180*np.pi)
        # SoutX = np.tan(self.reticle_outer_x_radius_in_degrees/180*np.pi)
        # SoutY = np.tan(self.reticle_outer_y_radius_in_degrees/180*np.pi)

        # SinX = np.tan(self.reticle_inner_x_radius_in_degrees)
        # SinY = np.tan(self.reticle_inner_y_radius_in_degrees)
        # SoutX = np.tan(self.reticle_outer_x_radius_in_degrees)
        # SoutY = np.tan(self.reticle_outer_y_radius_in_degrees)

        # SratioX = SoutX / SinX
        # SratioY = SoutY / SinY

        # posInX = self.image_resolution_x_in_pixels/SratioX
        # posInY = self.image_resolution_y_in_pixels/SratioY

        posInX = self.reticle_inner_x_radius_in_degrees
        posInY = self.reticle_inner_y_radius_in_degrees

        print(posInX, posInY)

        # print(posInX,posInY)

        return posInX, posInY

    def GenerateDefaultCursor(self):
        """
        This produce the default cursor taking in account the ratio between
        the inner and outer angles. The generated cursor can be drawn or 
        used to produce a PNG.
        Mostly used internaly

          _____
         /  |  \
        /   |   \
        |__    __|
        |        |
         \   |  /
          \__|_/

        The external circle is defined by AngularSizeOut
        The internal extremity of the lines by AngularSizeIn

        Parameters
        ----------
        reticle_color : TYPE
            The color of the reticle lines.
        reticle_line_width_px : TYPE
            the reticle lines width.

        Returns
        Returns
        -------
        None.

        """
        self.reticle_color_r = 1.0
        self.reticle_color_g = 1.0
        self.reticle_color_b = 1.0
        # int(np.tan(self.reticle_line_width_deg/180*np.pi)/np.tan(self.reticle_outer_x_radius_in_degrees/180*np.pi)*self.image_resolution_x_in_pixels)
        self.reticle_line_width_px = self.reticle_line_width_deg

        posInX, posInY = self.GetPositionOfInAngle()

        self.draw.ellipse(xy=((0, 0), (self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels)), fill=(0, 0, 0, 0), outline=(
            int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        # and a couple of lines
        self.draw.line((self.image_resolution_x_in_pixels/2, self.reticle_line_width_px/2, self.image_resolution_x_in_pixels/2, self.image_resolution_y_in_pixels /
                       2-posInY/2), fill=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        self.draw.line((self.reticle_line_width_px/2, self.image_resolution_y_in_pixels/2, self.image_resolution_x_in_pixels/2-posInX/2, self.image_resolution_y_in_pixels/2),
                       fill=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)

        self.draw.line((self.image_resolution_x_in_pixels/2, self.image_resolution_y_in_pixels - self.reticle_line_width_px/2, self.image_resolution_x_in_pixels/2, posInY/2 +
                       self.image_resolution_y_in_pixels/2), fill=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        self.draw.line((self.image_resolution_x_in_pixels - self.reticle_line_width_px/2, self.image_resolution_y_in_pixels/2, self.image_resolution_x_in_pixels/2+posInX/2,
                       self.image_resolution_y_in_pixels/2), fill=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)

    def Initialize(self, new_image_resolution_pix=500, new_reticle_inner_radius=1,
                   newreticle_outer_radius_deg=3,
                   isDefaultReticle=True, reticle_color=color.RGBColor(1, 1, 1), reticle_line_width_deg=10):
        """
        Reset for an empty image with resolutions self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels
        new_image_resolution_pix : TYPE, optional
            Resolution of the reticle in pixels. The default is 500.
        new_reticle_inner_radius_deg : TYPE, optional
            The inner radius of the reticle. The default is 1°.
        new_reticle_outer_radius_deg : TYPE, optional
            The outer radius of the reticle. The default is 3°.
        isDefaultReticle : TYPE, optional
            If you want to create the default reticle. See GenerateDefaultCursor. The default is True.
        reticle_color : TYPE, optional
            the color of the reticle lines. The default is color.RGBColor(0,0,0).
        reticle_line_width : TYPE, optional
            the width of the reticle lines in pixels. The default is 10.
        Returns
        -------
        None.

        """
        self.__init__(new_image_resolution_pix, new_reticle_inner_radius,
                      newreticle_outer_radius_deg,
                      isDefaultReticle, reticle_line_width_deg)

    def GeneratePNG(self, name):
        """


        Parameters
        ----------
        name : string
            Generate the PNG based on the given drawing.

        Returns
        -------
        None.

        """

        self.image.save(self.resourcesPath + "Images\\" +
                        "Reticles\\" + "reticle_from_drawing.png")
        self.image_path = self.resourcesPath + "Images\\" + \
            "Reticles\\" + "reticle_from_drawing.png"

    def GeneratePNGfile(self, name):
        """


        Parameters
        ----------
        name : string
            Generate the PNG based on the given drawing.

        Returns
        -------
        None.

        """
        print(self.resourcesPath)
        self.image.save(self.resourcesPath + "Images\\" + name)
        self.image_path = self.resourcesPath + "Images\\"

    def HashReticleImage(self):
        buffered = BytesIO()
        self.image.save(buffered, format="PNG")
        img_str = hashlib.md5(base64.b64encode(
            buffered.getvalue())).hexdigest()
        return img_str
