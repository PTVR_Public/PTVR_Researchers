# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 10:34:10 2024

@author: chopi
"""

"""
Created on Mon Mar 14 09:18:49 2022

ReticuleGenerator.py is a tool to generate PNGs, specialized for reticles.
Usage example: 
    rt = CursorGenerator ()
    rt.draw.rectangle(xy = (50, 50, 450, 450), fill=(0, 192, 192,a), outline=(255, 255, 255), width = 20)
    rt.draw.ellipse(xy = ((50, 50), (450, 450)), fill=(r, g, b, a), outline=(0, 0, 0))
    rt.draw.line((0, 0, 500, 500), fill=(255, 255, 0), width=10)
    rt.draw.line((0, 500, 500, 0), fill=(255, 255, 0), width=10)
    rt.Show ()
    rt.GeneratePNG ("myReticle.png")
    ptr.sprite = "myReticle.png" #ptr is a PointingCursor.
    
    
@author: synth
"""
# Import the library




import PTVR.Stimuli.Color as color
import numpy as np
import PTVR.SystemUtils
from PIL import Image, ImageDraw
import hashlib
import base64
from io import BytesIO
class ImageFromDrawing():
    """
    This class generates an object containing a 2D image (the image
    is created with the PIL package).
    This object must be passed to ImageToContingentCursor() where
    it will be converted to a flat cursor. 
    If you want to open a window on your PC showing the image 
    that has just been created, use "my_image.Show()" 

    'shape' can be: "ellipse" (default), "rectangle", "triangle"

    More details on Website's Documentation -> User Manual -> Pointing at an object
    """
    resourcesPath = PTVR.SystemUtils.UnityPTVRRoot + \
        "\\PTVR_Researchers\\PTVR_Operators\\resources\\"

    def __init__(self,
                 shape="ellipse",
                 image_resolution_pix=500,
                 line_width_as_ratio=0.1,
                 color=color.RGBColor(r=1, g=1, b=1, a=1),
                 fill=False,
                 image_file="",
                 path_to_image_file=""
                 ):
        """


        """
        self.image_file = image_file
        self.path_to_image_file = path_to_image_file
        self.image_id = "sprite"
        self.image_path = ""
        if type(image_resolution_pix) is np.array or type(image_resolution_pix) is np.ndarray:
            self.image_resolution_x_in_pixels = image_resolution_pix[0]
            self.image_resolution_y_in_pixels = image_resolution_pix[1]
        else:
            self.image_resolution_x_in_pixels = image_resolution_pix
            self.image_resolution_y_in_pixels = image_resolution_pix
        self.image = Image.new("RGBA", (self.image_resolution_x_in_pixels,
                                        self.image_resolution_y_in_pixels))
        self.draw = ImageDraw.Draw(self.image)
        # self.isDefaultReticle = isDefaultReticle
        self.reticle_line_width_deg = line_width_as_ratio * image_resolution_pix
        self.reticle_color = color
        self.fill = fill
        self.set_outer_inner_radiuses(
            image_resolution_pix, image_resolution_pix)

        if (shape == "ellipse"):

            self.draw_ellipse()

        if (shape == "rectangle"):

            self.draw_rectangle()

        if (shape == "triangle"):

            self.draw_triangle()

        if (image_file != ""):
            self.GeneratePNGfile(image_file, path_to_image_file)
        # self.set_outer_inner_radiuses (reticle_inner_radius_deg, reticle_outer_radius_deg)

    def set_outer_inner_radiuses(self, x_size_in_degrees=1, y_size_in_degrees=3):

        self.reticle_outer_x_radius_in_degrees = x_size_in_degrees
        self.reticle_outer_y_radius_in_degrees = y_size_in_degrees
        # if self.isDefaultReticle:
        #     self.GenerateDefaultCursor()

    def Show(self):
        """
        Draw in a window the generated image.

        Returns
        -------
        None.

        """
        self.image.show()

    def GetPositionOfInAngle(self):
        """
        Allows to calculate the pixel ratio betwwen the out angle and in angle.
        Used mostly internally.

        Returns
        -------
        posInX : float
            The ratio for the X in angle and out angle.
        posInY : float
            The ratio for the X in angle and out angle.

        """
        SinX = np.tan(self.reticle_inner_x_radius_in_degrees/180*np.pi)
        SinY = np.tan(self.reticle_inner_y_radius_in_degrees/180*np.pi)
        SoutX = np.tan(self.reticle_outer_x_radius_in_degrees/180*np.pi)
        SoutY = np.tan(self.reticle_outer_y_radius_in_degrees/180*np.pi)

        SratioX = SoutX / SinX
        SratioY = SoutY / SinY

        posInX = self.image_resolution_x_in_pixels/SratioX
        posInY = self.image_resolution_y_in_pixels/SratioY

        # print(posInX,posInY)

        return posInX, posInY

    def GenerateDefaultCursor(self):
        """
        This produce the default cursor taking in account the ratio between
        the inner and outer angles. The generated cursor can be drawn or 
        used to produce a PNG.
        Mostly used internaly

          _____
         /  |  \
        /   |   \
        |__    __|
        |        |
         \   |  /
          \__|_/

        The external circle is defined by AngularSizeOut
        The internal extremity of the lines by AngularSizeIn

        Parameters
        ----------
        reticle_color : TYPE
            The color of the reticle lines.
        reticle_line_width_px : TYPE
            the reticle lines width.

        Returns
        Returns
        -------
        None.

        """
        self.reticle_color_r = 1.0
        self.reticle_color_g = 1.0
        self.reticle_color_b = 1.0
        # int(np.tan(self.reticle_line_width_deg/180*np.pi)/np.tan(self.reticle_outer_x_radius_in_degrees/180*np.pi)*self.image_resolution_x_in_pixels)
        self.reticle_line_width_px = self.reticle_line_width_deg

        posInX, posInY = self.GetPositionOfInAngle()

        self.draw.ellipse(xy=((0, 0), (self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels)), fill=(0, 0, 0, 0),
                          outline=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(self.reticle_color.b*255)), width=self.reticle_line_width_px)
        # and a couple of lines

    def draw_ellipse(self):
        # int(np.tan(self.reticle_line_width_deg/180*np.pi)/np.tan(self.reticle_outer_x_radius_in_degrees/180*np.pi)*self.image_resolution_x_in_pixels)
        self.line_width_px = int(self.reticle_line_width_deg)

        if (self.fill):
            self.draw.ellipse(xy=((0, 0), (self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels)),
                              fill=(int(self.reticle_color.r*255), int(self.reticle_color.g*255),
                                    int(self.reticle_color.b*255), int(self.reticle_color.a*255)),
                              outline=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(
                                  self.reticle_color.b*255), int(self.reticle_color.a*255)),
                              width=self.line_width_px)
        else:
            self.draw.ellipse(xy=((0, 0), (self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels)), fill=(0, 0, 0, 0),
                              outline=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(
                                  self.reticle_color.b*255), int(self.reticle_color.a*255)),
                              width=self.line_width_px)

    def draw_rectangle(self):
        # int(np.tan(self.reticle_line_width_deg/180*np.pi)/np.tan(self.reticle_outer_x_radius_in_degrees/180*np.pi)*self.image_resolution_x_in_pixels)
        self.line_width_px = int(self.reticle_line_width_deg)

        if (self.fill):
            self.draw.rectangle(xy=((0, 0), (self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels)),
                                outline=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(
                                    self.reticle_color.b*255), int(self.reticle_color.a*255)),
                                width=self.line_width_px,
                                fill=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(self.reticle_color.b*255), int(self.reticle_color.a*255)))
        else:
            self.draw.rectangle(xy=((0, 0), (self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels)), fill=(0, 0, 0, 0),
                                outline=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(
                                    self.reticle_color.b*255), int(self.reticle_color.a*255)),
                                width=self.line_width_px)

    def draw_triangle(self):
        # int(np.tan(self.reticle_line_width_deg/180*np.pi)/np.tan(self.reticle_outer_x_radius_in_degrees/180*np.pi)*self.image_resolution_x_in_pixels)
        self.line_width_px = int(self.reticle_line_width_deg)

        if (self.fill):
            self.draw.polygon(xy=((0, self.image_resolution_y_in_pixels), (self.image_resolution_x_in_pixels/2.0, 0), (self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels)),
                              outline=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(
                                  self.reticle_color.b*255), int(self.reticle_color.a*255)),
                              fill=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(self.reticle_color.b*255), int(self.reticle_color.a*255)))
        else:
            print("triangle")
            lim = 20
            points = ((lim, self.image_resolution_y_in_pixels-lim), (self.image_resolution_x_in_pixels/2.0, lim),
                      (self.image_resolution_x_in_pixels-lim,
                       self.image_resolution_y_in_pixels-lim),
                      (lim, self.image_resolution_y_in_pixels-lim))

            self.draw.line(points, fill=(int(self.reticle_color.r*255), int(self.reticle_color.g*255), int(self.reticle_color.b*255),
                                         int(self.reticle_color.a*255)), joint="curve", width=self.line_width_px)

    def Initialize(self, new_image_resolution_pix=500, new_reticle_inner_radius=1,
                   newreticle_outer_radius_deg=3,
                   isDefaultReticle=True, reticle_color=color.RGBColor(1, 1, 1), reticle_line_width_deg=10):
        """
        Reset for an empty image with resolutions self.image_resolution_x_in_pixels, self.image_resolution_y_in_pixels
        new_image_resolution_pix : TYPE, optional
            Resolution of the reticle in pixels. The default is 500.
        new_reticle_inner_radius_deg : TYPE, optional
            The inner radius of the reticle. The default is 1°.
        new_reticle_outer_radius_deg : TYPE, optional
            The outer radius of the reticle. The default is 3°.
        isDefaultReticle : TYPE, optional
            If you want to create the default reticle. See GenerateDefaultCursor. The default is True.
        reticle_color : TYPE, optional
            the color of the reticle lines. The default is color.RGBColor(0,0,0).
        reticle_line_width : TYPE, optional
            the width of the reticle lines in pixels. The default is 10.
        Returns
        -------
        None.

        """
        self.__init__(new_image_resolution_pix, new_reticle_inner_radius,
                      newreticle_outer_radius_deg,
                      isDefaultReticle, reticle_line_width_deg)

    def GeneratePNG(self, name):
        """


        Parameters
        ----------
        name : string
            Generate the PNG based on the given drawing.

        Returns
        -------
        None.

        """

        self.image.save(self.resourcesPath + "Images\\" +
                        "sprite_from_drawing.png")  # + name
        self.image_path = self.resourcesPath + "Images\\" + "sprite_from_drawing.png"

    def GeneratePNGfile(self, name, path_to_image_file):
        """


        Parameters
        ----------
        name : string
            Generate the PNG based on the given drawing.

        Returns
        -------
        None.

        """
        if (path_to_image_file == ""):
            self.image.save(self.resourcesPath + "Images\\" + name)
            self.image_path = self.resourcesPath + "Images\\"
        else:
            self.image.save(path_to_image_file + "Images\\" + name)
            self.image_path = path_to_image_file + "Images\\"
            self.path_to_image_file = path_to_image_file

    def HashReticleImage(self):
        buffered = BytesIO()
        self.image.save(buffered, format="PNG")
        img_str = hashlib.md5(base64.b64encode(
            buffered.getvalue())).hexdigest()
        return img_str
