from abc import ABC
from enum import Enum, auto
from typing import List
import json
import uuid

import PTVR.SystemUtils
from PTVR.SystemUtils import CustomEncoder
from .ptvr_input import Keyboard

class PTVREvent():

	def __init__(self):
		self.uuid = uuid.uuid4()
		self._callbacks = []
		EventManager.add_event(self)

	def add_callback(self, callback):
		self._callbacks.append(callback)

	def add_callbacks(self, callbacks):
		self._callbacks.extend(callbacks)

	def to_json(self):
		result = {}
		# result["callbacks"] = self.callbacks
		result["uuid"] = str(self.uuid)
		result["callbackUUIDs"] = [str(callback.uuid) for callback in self._callbacks]
		result["eventType"] = type(self).__name__
		return result

########################################################################
# Events to be instantiated by users
########################################################################

class KeyEvent(PTVREvent):
	class Mode(Enum):
		ANY = auto()
		PRESS = auto()
		RELEASE = auto()

	def __init__(self, key = Keyboard.Key.NONE, mode = Mode.ANY):
		super().__init__()
		self.key = key
		self.mode = mode

	def to_json(self):
		result = super().to_json()
		result["key"] = self.key._name_
		result["mode"] = self.mode._name_
		return result

class KeyPressEvent(KeyEvent):

	def __init__(self, key = Keyboard.Key.NONE):
		super().__init__(key, KeyEvent.Mode.PRESS)

class KeyReleaseEvent(KeyEvent):

	def __init__(self, key = Keyboard.Key.NONE):
		super().__init__(key, KeyEvent.Mode.RELEASE)

########################################################################

class EventManager(ABC):
	_events = dict()

	@classmethod
	def add_event(cls, event : PTVREvent):
		cls._events[str(event.uuid)] = event

	@classmethod
	def add_events(cls, events : List[PTVREvent]):
		for evt in events:
			cls._events[str(evt.uuid)] = evt

	@classmethod
	def write(cls):
		path_to_json_directory = PTVR.SystemUtils.get_path_to_app_json_directory()
		# create the directory if it doesn't exist
		path_to_json_directory.mkdir(exist_ok=True)
		path_to_json_file = path_to_json_directory / (PTVR.SystemUtils.get_ptvr_app_name() + "_Events.json")
            
		with open (path_to_json_file, "w") as file:
			json.dump(cls._events, file, indent=4, cls=CustomEncoder)

