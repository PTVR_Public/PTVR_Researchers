from abc import ABC, abstractclassmethod, abstractmethod
from pathlib import Path
import json
import uuid

import PTVR.SystemUtils
from PTVR.SystemUtils import CustomEncoder
from .ptvr_data_types import ObjectProperty, RelationalOperator
from .ptvr_event import PTVREvent

class __Callback(ABC):
    
    def __init__(self):
        self.on_executed_event = PTVREvent()
        self.uuid = uuid.uuid4()

    def to_json(self):
        result = {}
        result["callbackType"] = type(self).__name__
        result["uuid"] = str(self.uuid)
        result["onExecutedEvent"] = self.on_executed_event.to_json()
        return result

# Interface
class ValueProvider(ABC):
    @abstractmethod
    def get_value(self):
        pass

class _GetPropertyValueCallback(__Callback):

    def __init__(self, property_name, data_provider):
        super().__init__()
        self.property_name = property_name
        self.data_provider = data_provider

    def to_json(self):
        result = super().to_json()
        result["propertyName"] = self.property_name
        result["dataProviderUUID"] = str(self.data_provider.uuid)
        return result

class _FinishCurrentTrialCallback(__Callback):
    pass

class _StartExperimentalProcedureCallback(__Callback):
    pass


class _SubmitTrialAnswerCallback(__Callback):

    def __init__(self,
                 value):
        super().__init__()
        self._value = value

    def to_json(self):
        result = super().to_json()
        if (isinstance(self._value, _GetPropertyValueCallback) \
            or isinstance(self._value,_GetFormattedStringCallback)):
            result["value"] = dict()
            result["value"]["dataType"] = type(self.value).__name__
            result["value"]["UUID"] = str(self._value.uuid)
        else:
            result["value"] = self._value
        return result


class _GetFormattedStringCallback(__Callback):

    def __init__(self, format_specifier:str, parameters):
        super().__init__()
        self.format_specifier = format_specifier
        self.parameters = parameters

    def to_json(self):
        result = super().to_json()
        result["formatSpecifier"] = self.format_specifier
        result["parameters"] = self.parameters
        return result

class _CompareValues(__Callback):

    def __init__(self, a, relational_operator : RelationalOperator, b):
        super().__init__()
        self._a = a
        self._b = b
        self._relational_operator = relational_operator

    def to_json(self):
        result = super().to_json()
        result["relationalOperator"] = self._relational_operator.name

        if (isinstance(self._a, _GetPropertyValueCallback) \
            or isinstance(self._a,_GetFormattedStringCallback)):
            result["a"] = dict()
            result["a"]["dataType"] = type(self._a).__name__
            result["a"]["UUID"] = str(self._a.uuid)
        else:
            result["a"] = self._a

        if (isinstance(self._b, _GetPropertyValueCallback) \
            or isinstance(self._b,_GetFormattedStringCallback)):
            result["b"] = dict()
            result["b"]["dataType"] = type(self._b).__name__
            result["b"]["UUID"] = str(self._b.uuid)
        else:
            result["b"] = self._b

        return result

class _SetObjectPropertyCallback(__Callback):

    def __init__(self,
                 ptvr_object,
                 object_property : ObjectProperty,
                 value):
        super().__init__()
        self.ptvr_object = ptvr_object
        self.object_property = object_property
        self.value = value

    def to_json(self):
        result = super().to_json()
        result["ptvrObjectId"] = self.ptvr_object.id
        result["objectPropertyType"] = self.object_property.name
        if (isinstance(self.value, _GetPropertyValueCallback) \
            or isinstance(self.value,_GetFormattedStringCallback)):
            result["value"] = dict()
            result["value"]["dataType"] = type(self.value).__name__
            result["value"]["UUID"] = str(self.value.uuid)
        else:
            result["value"] = self.value
        return result

class _IfElseCallback(__Callback):

    def __init__(self, condition):
        super().__init__()
        self._condition = condition
        self.on_if_case_event = PTVREvent()
        self.on_else_case_event = PTVREvent()

    def to_json(self):
        result = super().to_json()
        result["conditionUUID"] = str(self._condition.uuid)
        result["onIfCaseEventUUID"] = str(self.on_if_case_event.uuid)
        result["onElseCaseEventUUID"] = str(self.on_else_case_event.uuid)
        return result

class CallbackCreator(ABC):
    _callbacks = dict()

    @classmethod
    def perform_if_else_statement(cls, condition):
        cb = _IfElseCallback(condition)
        cls._callbacks[str(cb.uuid)] = cb
        return cb

    @classmethod
    def compare_values(cls, a, relational_operator:RelationalOperator, b):
        cb = _CompareValues(a, relational_operator, b)
        cls._callbacks[str(cb.uuid)] = cb
        return cb

    @classmethod
    def submit_trial_answer(cls, value):
        cb = _SubmitTrialAnswerCallback(value)
        cls._callbacks[str(cb.uuid)] = cb
        return cb

    @classmethod
    def get_formatted_string(cls, format_specifier:str, parameters):
        cb = _GetFormattedStringCallback(format_specifier, parameters)
        cls._callbacks[str(cb.uuid)] = cb
        return cb

    @classmethod
    def get_property_value(cls, property_name, data_provider):
        cb = _GetPropertyValueCallback(property_name, data_provider)
        cls._callbacks[str(cb.uuid)] = cb
        return cb

    @classmethod
    def start_experimental_procedure(cls):
        cb = _StartExperimentalProcedureCallback()
        cls._callbacks[str(cb.uuid)] = cb
        return cb

    @classmethod
    def finish_current_trial(cls):
        cb = _FinishCurrentTrialCallback()
        cls._callbacks[str(cb.uuid)] = cb
        return cb

    @classmethod
    def set_object_property(
            cls,
            ptvr_object,
            object_property : ObjectProperty,
            value):
        cb = _SetObjectPropertyCallback(ptvr_object, object_property, value)
        cls._callbacks[str(cb.uuid)] = cb
        return cb

    @classmethod
    def write(cls):
        path_to_json_directory = PTVR.SystemUtils.get_path_to_app_json_directory()
        path_to_json_directory.mkdir(exist_ok=True)
        path_to_json_file = path_to_json_directory / (PTVR.SystemUtils.get_ptvr_app_name() + "_Callbacks.json")
        with open (path_to_json_file, "w") as file:
            json.dump(cls._callbacks, file, indent=4, cls=CustomEncoder)
