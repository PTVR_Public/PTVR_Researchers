from enum import Enum, auto
from abc import ABC, abstractclassmethod, abstractmethod
import re # for checking if hexadecimal color is correct

import random
from math import sin, cos, tan, radians, pi

# for backward compatibility
from PTVR.Stimuli.Color import RGBColor

class RelationalOperator(Enum):
    EQUAL_TO = auto()
    NOT_EQUAL_TO = auto()
    LESS_THAN = auto()
    LESS_OR_EQUAL_THAN = auto()
    GREATER_THAN = auto()
    GREATER_OR_EQUAL_THAN = auto()

class ObjectProperty(Enum):
    """
    Work-In-Progress (WIP):
    Usage might change in the future.

    List of object properties that we support,
    i.e. users can specify them in PTVR's Python code.

    If a property is not listed here, its usage might be
    too advanced and that's not the purpose of PTVR,
    i.e. they are better off using Unity engine directly

    PTVR is NOT a 1 to 1 mapping of Unity functionality
    but a friendly simplification of it.
    """
    POSITION = auto()
    LOCAL_POSITION = auto()
    SIZE = auto()
    COLOR = auto()
    VISIBILITY = auto()
    TEXT = auto()

class ObjectPropertyValue(ABC):
    """
    Base class of PTVR Data Types that are used
    for setting ObjectProperties
    """
    def __init__(self):
        self.data_type = None

    def to_json(self):
        result = {}
        result["dataType"] = type(self).__name__
        return result



class Color(ObjectPropertyValue):
    # color constants
    RED = None
    LIME = None
    BLUE = None
    YELLOW = None
    CYAN = None
    MAGENTA = None
    BLACK = None
    WHITE = None
    GRAY = None
    SILVER = None
    MAROON = None
    OLIVE = None
    GREEN = None
    PURPLE = None
    TEAL = None
    NAVY = None
    AQUA = None
    FUCHSIA = None
    LIMEGREEN = None
    DARKORANGE = None
    DEEPPINK = None

    def __init__(self, r=0.0, g=0.0, b=0.0, a=1.0):
        self.r = r
        self.g = g
        self.b = b
        self.a = a

    @property
    def ptvr_color(self):
        return RGBColor(self.r, self.g, self.b, self.a)

    @classmethod
    def from_RGBA255(cls, r:int=0, g:int=0, b:int=0, a:int=255):
        if not 0 <= r <= 255:
            raise ValueError("Wrong r value {}. Valid range [0-255]".format(r))
        if not 0 <= g <= 255:
            raise ValueError("Wrong g value {}. Valid range [0-255]".format(g))
        if not 0 <= b <= 255:
            raise ValueError("Wrong b value {}. Valid range [0-255]".format(b))
        if not 0 <= a <= 255:
            raise ValueError("Wrong a value {}. Valid range [0-255]".format(a))
        return Color(r/255.0, g/255.0, b/255.0, a/255.0)

    @classmethod
    def from_hex(cls, hex:str):
        # valid pattern:
        # - string starts with #
        # - 6 or 8 characters between [0-f]
        pattern = r'^#[a-fA-F0-9]{6}$|^#[a-fA-F0-9]{8}$'
        if re.match(pattern, hex) is None:
            raise ValueError("Wrong hex {} value. It should start with '#' and followed by 6 or 8 characters from [0-F]".format(hex))
        r = int(hex[1:3], 16)
        g = int(hex[3:5], 16)
        b = int(hex[5:7], 16)
        a = 255
        if (len(hex) == 9):
            a = int(hex[7:9], 16)
        return cls.from_RGBA255(r,g,b,a)

    @classmethod
    def get_random(cls):
        random_red = random.uniform(0, 1)
        random_green = random.uniform(0, 1)
        random_blue = random.uniform(0, 1)
        return Color(random_red, random_green, random_blue, 1)

    def to_json(self):
        result = super().to_json()
        result["r"] = self.r
        result["g"] = self.g
        result["b"] = self.b
        result["a"] = self.a
        return result

    def __str__(self):
        return f"Color(r={self.r:.2f}, g={self.g:.2f}, b={self.b:.2f}, a={self.a:.2f})"

# initialization of color constants

Color.RED =  Color(1, 0, 0, 1)
Color.LIME =  Color(0, 1, 0, 1)
Color.BLUE =  Color(0, 0, 1, 1)
Color.YELLOW =  Color(1, 1, 0, 1)
Color.CYAN =  Color(0, 1, 1, 1)
Color.MAGENTA =  Color(1, 0, 1, 1)
Color.BLACK =  Color(0, 0, 0, 1)
Color.WHITE =  Color(1, 1, 1, 1)
Color.GRAY =  Color(0.5, 0.5, 0.5, 1)
Color.SILVER =  Color(0.75, 0.75, 0.75, 1)
Color.MAROON =  Color(0.5, 0, 0, 1)
Color.OLIVE =  Color(0.5, 0.5, 0, 1)
Color.GREEN =  Color(0, 0.5, 0, 1)
Color.PURPLE =  Color(0.5, 0, 0.5, 1)
Color.TEAL =  Color(0, 0.5, 0.5, 1)
Color.NAVY =  Color(0, 0, 0.5, 1)
Color.AQUA =  Color(0, 1, 1, 1)
Color.FUCHSIA =  Color(1, 0, 1, 1)
Color.LIMEGREEN =  Color(0.196, 0.804, 0.196, 1)
Color.DARKORANGE =  Color(1, 0.549, 0, 1)
Color.DEEPPINK =  Color(1, 0.078, 0.576, 1)

class Vector3(ObjectPropertyValue):

    RIGHT = None
    UP = None
    FORWARD = None
    ZERO = None
    ONE = None

    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

    @classmethod
    def from_perimetric(cls, eccentricity_deg, radial_distance_deg, half_meridian_deg):
        if not 0 <= eccentricity_deg <= 180:
            raise ValueError("Wrong eccentricity value {}. Valid range [0-180]".format(eccentricity_deg))
        if not 0 <= half_meridian_deg <= 360:
            raise ValueError("Wrong half meridian value {}. Valid range [0-360]".format(half_meridian_deg))

        x = radial_distance_deg * sin(radians(eccentricity_deg)) * cos(radians(half_meridian_deg))
        y = radial_distance_deg * sin(radians(eccentricity_deg)) * sin(radians(half_meridian_deg))
        z = radial_distance_deg * cos(radians(eccentricity_deg))
        return Vector3(x, y, z)

    @classmethod
    def get_random_inside_xy_circle(cls, radius:float = 1.0):
        random_radius = random.uniform(0, radius)
        random_angle = random.random() * 2 * pi # [0, 2PI)
        return Vector3(random_radius * cos(random_angle), random_radius * sin(random_angle), 0)

    @classmethod
    def get_random_inside_yz_circle(cls, radius:float = 1.0):
        random_radius = random.uniform(0, radius)
        random_angle = random.random() * 2 * pi # [0, 2PI)
        return Vector3(0, random_radius * cos(random_angle), random_radius * sin(random_angle))

    @classmethod
    def get_random_inside_xz_circle(cls, radius:float = 1.0):
        random_radius = random.uniform(0, radius)
        random_angle = random.random() * 2 * pi # [0, 2PI)
        return Vector3(random_radius * cos(random_angle), 0, random_radius * sin(random_angle))

    @classmethod
    def get_random_inside_sphere(cls, radius:float = 1.0):
        random_radius = random.uniform(0, radius)
        random_eccentricity = random.uniform(0, pi) # 0-180 degrees
        random_half_meridian = random.uniform(0, 2*pi) # 0-360 degrees
        return Vector3(
                random_radius * sin(random_eccentricity) * cos(random_half_meridian),
                random_radius * sin(random_eccentricity) * sin(random_half_meridian),
                random_radius * cos(random_eccentricity))

    @classmethod
    def get_random_on_sphere(cls, radius:float = 1.0):
        random_eccentricity = random.uniform(0, pi) # 0-180 degrees
        random_half_meridian = random.uniform(0, 2*pi) # 0-360 degrees
        return Vector3(
                radius * sin(random_eccentricity) * cos(random_half_meridian),
                radius * sin(random_eccentricity) * sin(random_half_meridian),
                radius * cos(random_eccentricity))

    def to_json(self):
        result = super().to_json()
        result["x"] = self.x
        result["y"] = self.y
        result["z"] = self.z
        return result

    def __str__(self):
        return f"Vector3({self.x:.2f}; {self.y:.2f}; {self.z:.2f})"

    def __add__(self, other):
        return Vector3(self.x+other.x, self.y+other.y, self.z+other.z)

Vector3.RIGHT =  Vector3(1, 0, 0)
Vector3.UP =  Vector3(0, 1, 0)
Vector3.FORWARD =  Vector3(0, 0, 1)
Vector3.ZERO =  Vector3(0, 0, 0)
Vector3.ONE =  Vector3(1, 1, 1)
