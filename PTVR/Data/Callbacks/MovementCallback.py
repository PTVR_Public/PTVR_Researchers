"""
Interfaces to callback and creating various kinds of effect, must of the time each callback while have an activate and a deactivate for the callback.
"""

import sys
from PTVR.Stimuli.Utils import MetaData
import random
import numpy as np
import PTVR.LogManager as log


class Callback(MetaData):
    def __init__(self, type_callback, callback_name, effect, is_callback_pausable):
        self.id = int(random.random()*1e18)
        self.type = type_callback
        self.callbackName = callback_name
        self.effect = effect
        self.isCallbackPausable = is_callback_pausable


class SetAzimuthAndElevationInGlobalCS(Callback):

    def __init__(self, object_id=-1, label_of_updated_GV_to_use="",
                 azimuth=0, elevation=0, radialDistance=0,
                 azimuth_reference_cardinal_direction="north",
                 azimuth_rotation_direction="clockwise",
                 translation_applied_for_new_CS=np.array([0, 0, 0]),
                 effect="activate", callback_name="SetAzimuthAndElevationInGlobalCS",
                 is_callback_pausable=True):
        Callback.__init__(self, "SetAzimuthAndElevationInGlobalCS",
                          callback_name, effect, is_callback_pausable)

        self.objectId = object_id
        self.label_of_updated_GV_to_use = label_of_updated_GV_to_use
        self.azimuth = azimuth
        self.elevation = elevation
        self.radialDistance = radialDistance
        self.azimuth_reference_cardinal_direction = azimuth_reference_cardinal_direction
        self.azimuth_rotation_direction = azimuth_rotation_direction
        self.translation_vector = translation_applied_for_new_CS


class MoveInAzimuthAndElevationCS(Callback):

    def __init__(self, object_id=-1, movement_id=-1, radialDistance=0,
                 movement_direction_in_azimuth_and_elevation=np.array([0, 0]),
                 speed=1,
                 translation_applied_for_new_CS=np.array([0, 0, 0]),
                 azimuth_reference_cardinal_direction="north",
                 azimuth_rotation_direction="clockwise",
                 effect="activate", callback_name="MoveInAzimuthAndElevationCS",
                 is_callback_pausable=True):
        Callback.__init__(self, "MoveInAzimuthAndElevationCS",
                          callback_name, effect, is_callback_pausable)

        self.objectId = object_id
        self.movement_id = movement_id
        self.movement_direction_in_azimuth_and_elevation = movement_direction_in_azimuth_and_elevation
        self.radialDistance = radialDistance
        self.speed = speed
        self.azimuth_reference_cardinal_direction = azimuth_reference_cardinal_direction
        self.azimuth_rotation_direction = azimuth_rotation_direction
        self.translation_vector = translation_applied_for_new_CS


class SetCartesianCoordinatesInGlobalCS(Callback):
    """
    Callback to set the cartesian coordinates of an object specified in 
    in the global Coordinate System (CS)

    Parameters
    ----------
    object_id :    
        Id of object whose coordinates are to be set. 
        DESCRIPTION. The default is -1.

    label_of_updated_GV_to_use :
        If this string is filled with the label of a Global Variable (GV) containing
        an array of 3 values,
        then these 3 values will be used to set the cartesian coordinates.
        If this string is "" (i.e. empty), then 'position_in_global_CS' will
        be used instead.        
        DESCRIPTION. The default is "".

    position_in_global_CS : 
        If 'label_of_updated_GV_to_use' is "" (i.e. empty),
        then 'position_in_global_CS' must contain an array of 3 values. these 
        3 values will be used to set the cartesian coordinates.

        DESCRIPTION. The default is np.array([0,0,0]).  


    Returns
    -------
    None.

    """

    def __init__(self, object_id=-1, label_of_updated_GV_to_use="", position_in_global_CS=np.array([0, 0, 0]),
                 effect="activate", callback_name="SetCartesianCoordinatesInGlobalCS", is_callback_pausable=True):
        Callback.__init__(self, "SetCartesianCoordinatesInGlobalCS",
                          callback_name, effect, is_callback_pausable)

        self.objectId = object_id
        self.label_of_updated_GV_to_use = label_of_updated_GV_to_use
        self.position_in_global_CS = position_in_global_CS


class Move_with_periodic_function(Callback):
    """
    Callback to move an object with a periodic function.

    Parameters
    ----------
    object_id : 
        Id of object to be moved. The default is -1.
    temporal_period_in_sec : TYPE, optional
        Temporal period of motion in seconds. The default is 2.0.
    look_in_movement_direction : TYPE, optional
        DESCRIPTION. The default is False.
    end_position : TYPE, optional
        Relative end position wrt to the CURRENT position of the object. This therefore defines the spatial amplitude of the periodic movement. The default is np.array([0.0, 0.0, 0.0]).
    function_shape : TYPE, optional
        Shape of periodic function: "sinusoidal", "triangle", "rectangle". The default is "sinusoidal".
    effect : TYPE, optional
        "activate" or "deactivate". The default is "activate".
    stop_mode : TYPE, optional
        "pause" (re-start from current position), or "reset" (re-start from original position). The default is "reset".
    callback_name : TYPE, optional
        DESCRIPTION. The default is "Move_with_periodic_function".
    is_callback_pausable : TYPE, optional
        DESCRIPTION. The default is True.

    Returns
    -------
    None.

    """

    def __init__(self, object_id=-1, temporal_period_in_sec=2.0, look_in_movement_direction=False,
                 end_position=np.array([0.0, 0.0, 0.0]), function_shape="sinusoidal",
                 effect="activate", start_position=np.array([0.0, 0.0, 0.0]),
                 callback_name="Move_with_periodic_function", is_callback_pausable=True):
        Callback.__init__(self, "Move_with_periodic_function",
                          callback_name, effect, is_callback_pausable)

        log.SetError(
            "Move_with_periodic_function is deprecated. Please use MoveWithPeriodicFunction instead")
        sys.exit()
        self.objectId = object_id
        self.temporal_period_in_sec = temporal_period_in_sec
        self.end_position = end_position
        self.function_shape = function_shape
        self.look_in_movement_direction = look_in_movement_direction
        self.start_position = start_position


class MoveWithPeriodicFunction(Callback):
    """
    Callback to move an object with a periodic function.

    Parameters
    ----------
    object_id : 
        Id of object to be moved. The default is -1.
    temporal_period_in_sec : TYPE, optional
        Temporal period of motion in seconds. The default is 2.0.
    look_in_movement_direction : TYPE, optional
        DESCRIPTION. The default is False.
    end_position : TYPE, optional
        Relative end position wrt to the CURRENT position of the object. This therefore defines the spatial amplitude of the periodic movement. The default is np.array([0.0, 0.0, 0.0]).
    function_shape : TYPE, optional
        Shape of periodic function: "sinusoidal", "triangle", "rectangle". The default is "sinusoidal".
    effect : TYPE, optional
        "activate" or "deactivate". The default is "activate".
    stop_mode : TYPE, optional
        "pause" (re-start from current position), or "reset" (re-start from original position). The default is "reset".
    callback_name : TYPE, optional
        DESCRIPTION. The default is "Move_with_periodic_function".
    is_callback_pausable : TYPE, optional
        DESCRIPTION. The default is True.

    Returns
    -------
    None.

    """

    def __init__(self, object_id=-1, temporal_period_in_sec=2.0, look_in_movement_direction=False,
                 end_position=np.array([0.0, 0.0, 0.0]), function_shape="sinusoidal",
                 effect="activate", start_position=np.array([0.0, 0.0, 0.0]),
                 callback_name="MoveWithPeriodicFunction", is_callback_pausable=True):
        Callback.__init__(self, "MoveWithPeriodicFunction",
                          callback_name, effect, is_callback_pausable)

        self.objectId = object_id
        self.temporal_period_in_sec = temporal_period_in_sec
        self.end_position = end_position
        self.function_shape = function_shape
        self.look_in_movement_direction = look_in_movement_direction
        self.start_position = start_position


class RevolveAround(Callback):
    """
    Parameters
    ----------
    object_id : id of the revolving object.
        The default is -1.
    revolution_center_id : id of the object which specifies the revolution center.
        The default is -1.
    revolution_axis : axis of the revolution
        The default is np.array([0.0, 1.0, 0.0]), i.e. revolution axis is parallel to
        the global Y axis.

    revolution_speed : 
        In degrees per second.
        The default is 5.

    effect : 
        "activate" (default) or "deactivate".
    face_movement_direction : 
        Is the orientation of revolving object such that it is "looking" straight-ahead.
        The default is False.
    callback_name : 
        The default is "RevolveAround".
    is_callback_pausable : 
        The default is True.

    Returns
    -------
    None.

    """

    def __init__(self, object_id=-1,
                 revolution_center_id=-1,  revolution_axis=np.array([0.0, 1.0, 0.0]),
                 revolution_speed=5,
                 effect="activate", face_movement_direction=False,
                 callback_name="RevolveAround", is_callback_pausable=True):

        Callback.__init__(self, "RevolveAround", callback_name,
                          effect, is_callback_pausable)
        self.objectId = object_id
        self.rotate_around_id = revolution_center_id
        self.revolution_speed = revolution_speed
        self.axis = revolution_axis
        self.face_movement_direction = face_movement_direction


class Translate(Callback):
    def __init__(self, object_id=-1, speed=2.0, movement_direction=np.array([0.0, 0.0, 0.0]),
                 effect="activate", face_movement_direction=False,
                 global_coordinate_system=False,
                 callback_name="Translate", is_callback_pausable=True):
        Callback.__init__(self, "Translate", callback_name,
                          effect, is_callback_pausable)

        if (not global_coordinate_system) and (face_movement_direction):
            print('\x1b[0;31;40m' + "Error, program aborted ! face_movement_direction set to True is not comptatible with global_coordinate_system set to False : " +
                  "Check 'vehicle' implementation")
            sys.exit()

        self.objectId = object_id
        self.speed = speed
        self.movement_direction = movement_direction
        self.face_movement_direction = face_movement_direction
        self.global_coordinates = global_coordinate_system
        self.movement_id = -1


class FaceMovementDirection(Callback):
    def __init__(self, object_id=-1,
                 effect="activate", movement_id=-1,
                 callback_name="FaceMovementDirection", is_callback_pausable=True):
        Callback.__init__(self, "FaceMovementDirection",
                          callback_name, effect, is_callback_pausable)
        self.objectId = object_id

        self.movement_id = movement_id


class MoveTowards(Callback):
    def __init__(self, object_id=-1, target_object_id=-1, speed=0,
                 effect="activate", offset=np.array([0, 0, 0]),
                 face_movement_direction=False,
                 callback_name="MoveTowards", is_callback_pausable=True):
        Callback.__init__(self, "MoveTowards", callback_name,
                          effect, is_callback_pausable)
        self.objectId = object_id
        self.target_object_id = target_object_id
        self.offset = offset
        self.speed = speed
        self.face_movement_direction = face_movement_direction


class Rotate(Callback):
    def __init__(self, object_id=-1,
                 effect="activate", global_coordinate_system=False, rotation_vector=np.array([0, 0, 0]),
                 callback_name="Rotate", is_callback_pausable=True):
        Callback.__init__(self, "Rotate", callback_name,
                          effect, is_callback_pausable)
        self.objectId = object_id
        self.global_coordinates = global_coordinate_system
        self.rotation_vector = rotation_vector


class SetOrientationInGlobalCS(Callback):
    def __init__(self, object_id=-1,
                 effect="activate",
                 rotation_in_global_CS=np.array([0]),
                 callback_name="SetOrientationInGlobalCS", is_callback_pausable=True):
        Callback.__init__(self, "SetOrientationInGlobalCS",
                          callback_name, effect, is_callback_pausable)
        self.objectId = object_id
        self.final_rotation_vector = rotation_in_global_CS


class SetCircleInAzimuthAndElevation(Callback):
    def __init__(self, object_id=-1, object_to_track_id=-1,
                 movement_direction_in_azimuth_and_elevation=np.array([0, 0]),
                 translation_applied_for_new_CS=np.array([0, 0, 0]),
                 initial_diameter=1.0,
                 effect="activate",
                 callback_name="SetCircleInAzimuthAndElevation", is_callback_pausable=True):
        Callback.__init__(self, "SetCircleInAzimuthAndElevation",
                          callback_name, effect, is_callback_pausable)
        self.objectId = object_id
        self.objectToTrackId = object_to_track_id
        self.movement_direction_in_azimuth_and_elevation = movement_direction_in_azimuth_and_elevation
        self.translation_vector = translation_applied_for_new_CS
        self.initial_diameter = initial_diameter


class MoveThroughPoints(Callback):
    def __init__(self, object_id=-1,
                 list_of_points=np.array([np.array([0.0, 0.0, 0.0])]),
                 speed=5,
                 effect="activate", face_movement_direction=False,
                 callback_name="MoveThroughPoints", is_callback_pausable=True):
        Callback.__init__(self, "MoveThroughPoints",
                          callback_name, effect, is_callback_pausable)
        self.objectId = object_id
        self.list_of_points = list_of_points
        self.speed = speed
        self.face_movement_direction = face_movement_direction
