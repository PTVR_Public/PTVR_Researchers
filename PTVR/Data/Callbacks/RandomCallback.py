# -*- coding: utf-8 -*-
"""
Created on Wed Jul 10 10:46:02 2024

@author: chopi
"""

"""
Callbacks involved in randomization processes.
"""




from PTVR.Stimuli.Utils import MetaData
import random
import PTVR.Stimuli.Utils
import numpy as np
class Callback(MetaData):
    """ 
    """

    def __init__(self, type_callback, callback_name, effect, is_callback_pausable):
        self.id = int(random.random()*1e18)
        self.type = type_callback
        self.callbackName = callback_name
        self.effect = effect
        self.isCallbackPausable = is_callback_pausable


class RandomVector3(Callback):
    """
        Goal : provide a triplet of random values.
        This callback creates a global variable, 
        labels this global variable (by default 'GV').
        draws a triplet of random values,
        and stores the triplet into the global variable.

        The triplet in the global variable is then available (thanks to its label)
        for other callbacks !        

        Parameters
        ----------
        label_of_updated_GV : This label is crucial, it is used by other 
        callbacks to access the content of the Global variable created by
        the current callback.
            DESCRIPTION. The default is "GV".

        x_min_max : TYPE, optional
            DESCRIPTION. The default is np.array([0,1]).

        y_min_max : TYPE, optional
            DESCRIPTION. The default is np.array([0,1]).
        z_min_max : TYPE, optional
            DESCRIPTION. The default is np.array([0,1]).
        effect : TYPE, optional
            DESCRIPTION. The default is "activate".
        callback_name : TYPE, optional
            DESCRIPTION. The default is "RandomVector3".
        is_callback_pausable : TYPE, optional
            DESCRIPTION. The default is True.

        Returns
        -------
        None.

    """

    def __init__(self, x_min_max=np.array([0, 1]), label_of_updated_GV="GV",
                 y_min_max=np.array([0, 1]), z_min_max=np.array([0, 1]),
                 effect="activate", callback_name="RandomVector3", is_callback_pausable=True):
        Callback.__init__(self, "RandomVector3", callback_name,
                          effect, is_callback_pausable)
        self.x_min_max = x_min_max
        self.y_min_max = y_min_max
        self.z_min_max = z_min_max
        self.label_of_updated_GV = label_of_updated_GV
        self.object_id = -1


class RandomItemFromList(Callback):
    """
     Goal : provide an item randomly drawn from a list.
     This callback creates a global variable, 
     labels this global variable (by default 'GV').
     randomly draws an item from a list (the list is passed as an argument 
     to 'list_of_items),
     and stores the drawn item into the global variable.

     The item in the global variable is then available (thanks to its label)
     for other callbacks !             

    Parameters
    ----------

    list_of_items : list of items created by the user so that one of
    these items will be randomly chosen.
        The default is np.array([0,1]).  

    label_of_updated_GV : This label is crucial, it is used by other 
    callbacks to access the content of the Global variable created by
    the current callback.
    The default is "GV".

    effect : TYPE, optional
        DESCRIPTION. The default is "activate".
    callback_name : TYPE, optional
        DESCRIPTION. The default is "RandomItemFromList".
    is_callback_pausable : TYPE, optional
        DESCRIPTION. The default is True.

    Returns
    -------
    None.

    """

    def __init__(self, list_of_items=np.array([0, 1]), label_of_updated_GV="GV",
                 effect="activate", callback_name="RandomItemFromList", is_callback_pausable=True):

        Callback.__init__(self, "RandomItemFromList",
                          callback_name, effect, is_callback_pausable)
        self.list_of_items = list_of_items
        self.label_of_updated_GV = label_of_updated_GV
        self.object_id = -1


class RandomArrayFromList(Callback):
    """
     Goal : provide an arrat randomly drawn from a list of arrays.
     This callback creates a global variable, 
     labels this global variable (by default 'GV').
     randomly draws an item from a list (the list is passed as an argument 
     to 'list_of_items),
     and stores the drawn item into the global variable.

     The array in the global variable is then available (thanks to its label)
     for other callbacks !             

    Parameters
    ----------

    list_of_items : list of arrays created by the user so that one of
    these arrays will be randomly chosen.
        The default is np.array(np.array([0])).  

    label_of_updated_GV : This label is crucial, it is used by other 
    callbacks to access the content of the Global variable created by
    the current callback.
    The default is "GV".

    effect : TYPE, optional
        DESCRIPTION. The default is "activate".
    callback_name : TYPE, optional
        DESCRIPTION. The default is "RandomArrayFromList".
    is_callback_pausable : TYPE, optional
        DESCRIPTION. The default is True.

    Returns
    -------
    None.

    """

    def __init__(self, list_of_items=np.array(np.array([0])), label_of_updated_GV="GV",
                 effect="activate", callback_name="RandomArrayFromList", is_callback_pausable=True):

        Callback.__init__(self, "RandomArrayFromList",
                          callback_name, effect, is_callback_pausable)
        self.list_of_items = list_of_items
        self.label_of_updated_GV = label_of_updated_GV
        self.object_id = -1
