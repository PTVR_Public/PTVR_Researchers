"""
Interfaces to callback and creating various kinds of effect, must of the time each callback while have an activate and a deactivate for the callback.
"""

from PTVR.Stimuli.Utils import MetaData

import random
import PTVR.Stimuli.Utils
import numpy as np


class Callback(MetaData):
    def __init__(self, type_callback, callback_name, effect, is_callback_pausable):
        self.id = int(random.random()*1e18)
        self.type = type_callback
        self.callbackName = callback_name
        self.effect = effect
        self.isCallbackPausable = is_callback_pausable


class CounterGVupdate(Callback):
    """
The following callback initially creates a COUNTER that contains a continuous
value in a GV (Global Variable) whose name is given by the argument passed
to 'label_of_updated_GV'.


        Parameters
        ----------
        operation : (string)
            operation used to update the value contained in the counter.
            "+" or "-" or "*" or "/"
            The default is "+".
        factor : (float)
            . The default is 1.0.
        label_of_updated_GV : 
            defines the name of the counter. This counter can be accessed
            by other callbacks thanks to this name. The default is "GV".
        global_variable_initial_value : 
            The initial value contained in the counter. The default is 0.

        limit :
            ex: limit is 0, operation is "-", initial_value is a positive number:
                The zero limit makes sure that the counter will not go below
                zero.


        Returns
        -------
        None.

    """

    def __init__(self, operation="+", factor=1.0, label_of_updated_GV="GV", global_variable_initial_value=0,
                 effect="activate", callback_name="CounterGVupdate",
                 limit=10000,
                 is_callback_pausable=True):

        Callback.__init__(self, "CounterGVupdate",
                          callback_name, effect, is_callback_pausable)
        self.operation = operation
        self.operation_value = factor
        self.global_variable_name = label_of_updated_GV
        self.global_variable_initial_value = global_variable_initial_value
        self.global_variable_change_once_per_object = False
        self.object_id = -1
        self.global_variable_name_check = ""
        self.limit = limit


class OperationBetweenGVs(Callback):
    """
    """

    def __init__(self, global_variable_name_to_update="GV", operation="+",
                 global_variables_to_use=np.array([""]),
                 effect="activate", callback_name="OperationBetweenGVs",
                 is_callback_pausable=True):
        Callback.__init__(self, "OperationBetweenGVs",
                          callback_name, effect, is_callback_pausable)

        self.global_variable_name_to_update = global_variable_name_to_update
        self.global_variables_to_use = global_variables_to_use
        self.operation = operation


class FloatGVupdate(Callback):
    """
    """

    def __init__(self, label_of_updated_GV="GV", global_variable_initial_value=0,
                 global_variable_name_used="",
                 effect="activate", callback_name="FloatGVupdate",
                 is_callback_pausable=True):
        Callback.__init__(self, "FloatGVupdate",
                          callback_name, effect, is_callback_pausable)

        self.global_variable_name = label_of_updated_GV
        self.global_variable_change_once_per_object = False
        self.global_variable_name_check = ""
        self.global_variable_initial_value = global_variable_initial_value
        self.global_variable_name_used = global_variable_name_used


class StringGVupdate(Callback):
    """
    """

    def __init__(self, label_of_updated_GV="GV",
                 global_variable_initial_value="",
                 object_with_string_id=-1,
                 effect="activate", callback_name="StringGVupdate", is_callback_pausable=True):
        Callback.__init__(self, "StringGVupdate",
                          callback_name, effect, is_callback_pausable)

        self.global_variable_name = label_of_updated_GV
        self.global_variable_name_check = ""
        self.global_variable_initial_value = global_variable_initial_value
        self.object_with_string_id = object_with_string_id


class StringGVcompare(Callback):
    """
    """

    def __init__(self, label_of_string_GV="GV",
                 label_of_updated_GV="GV",
                 string_to_compare="",
                 object_with_string_id=-1,
                 label_of_updated_GV_counter="",
                 effect="activate", callback_name="StringGVcompare", is_callback_pausable=True):
        Callback.__init__(self, "StringGVcompare",
                          callback_name, effect, is_callback_pausable)

        self.label_of_string_GV = label_of_string_GV
        self.string_to_compare = ""
        self.label_of_updated_GV = label_of_updated_GV
        self.object_with_string_id = object_with_string_id
        self.string_to_compare = string_to_compare
        self.label_of_updated_GV_counter = label_of_updated_GV_counter


class FloatGVFromArray(Callback):
    """
    """

    def __init__(self, label_of_updated_GV="GV", index=0, label_of_used_GV="GV_used",
                 label_of_used_GV_for_index="",
                 effect="activate", callback_name="FloatGVFromArray", is_callback_pausable=True):
        Callback.__init__(self, "FloatGVFromArray",
                          callback_name, effect, is_callback_pausable)

        self.global_variable_name = label_of_updated_GV
        self.global_variable_name_used = label_of_used_GV
        self.global_variable_change_once_per_object = False
        self.global_variable_name_check = ""
        self.label_of_used_GV_for_index = label_of_used_GV_for_index
        self.index = index


class IndexGVFromFloatArray(Callback):
    """
    """

    def __init__(self, global_variable_name_to_store_index="GV", label_of_used_float_GV="GV_used",
                 label_of_used_array_GV="",
                 effect="activate", callback_name="IndexGVFromFloatArray", is_callback_pausable=True):
        Callback.__init__(self, "IndexGVFromFloatArray",
                          callback_name, effect, is_callback_pausable)

        self.global_variable_name_to_store_index = global_variable_name_to_store_index
        self.label_of_used_float_GV = label_of_used_float_GV
        self.label_of_used_array_GV = label_of_used_array_GV


class FloatArrayGVupdate(Callback):
    """
    """

    def __init__(self, label_of_updated_GV="GV", global_variable_initial_value=np.array([]),
                 effect="activate", callback_name="FloatArrayGVupdate", is_callback_pausable=True):
        Callback.__init__(self, "FloatArrayGVupdate",
                          callback_name, effect, is_callback_pausable)

        self.global_variable_name = label_of_updated_GV
        self.global_variable_initial_value = global_variable_initial_value
        self.global_variable_name_check = ""


class BooleanGVupdate(Callback):
    """
    """

    def __init__(self, boolean_value_of_updated_GV="True", label_of_updated_GV="GV",
                 effect="activate", callback_name="BooleanGVupdate", is_callback_pausable=True):
        Callback.__init__(self, "BooleanGVupdate",
                          callback_name, effect, is_callback_pausable)
        self.operation = boolean_value_of_updated_GV
        self.operation_value = 1
        self.global_variable_name = label_of_updated_GV
        self.global_variable_initial_value = 0
        self.global_variable_change_once_per_object = False
        self.object_id = -1
        self.global_variable_name_check = ""


class BooleanGVupdateFromObject(Callback):
    """
    """

    def __init__(self, boolean_value_of_updated_GV="True", label_of_updated_GV="GV",
                 update_the_boolean_only_once=False, object_id=-1, update_if_this_GV_is_true="",
                 effect="activate", callback_name="BooleanGVupdateFromObject", is_callback_pausable=True):
        Callback.__init__(self, "BooleanGVupdateFromObject",
                          callback_name, effect, is_callback_pausable)
        self.operation = boolean_value_of_updated_GV
        self.operation_value = 1
        self.global_variable_name = label_of_updated_GV
        self.global_variable_initial_value = 0
        self.global_variable_change_once_per_object = update_the_boolean_only_once
        self.object_id = object_id
        self.global_variable_name_check = update_if_this_GV_is_true


class CounterGVupdateFromObject(Callback):
    """
    """

    def __init__(self, operation="+", factor=1.0, label_of_updated_GV="GV", global_variable_initial_value=0, object_id=-1, update_if_this_GV_is_true="",
                 effect="activate", callback_name="CounterGVupdateFromObject", is_callback_pausable=True,
                 update_the_global_variable_only_once=False):
        Callback.__init__(self, "CounterGVupdateFromObject",
                          callback_name, effect, is_callback_pausable)
        self.operation = operation
        self.operation_value = factor
        self.global_variable_name = label_of_updated_GV
        self.global_variable_initial_value = global_variable_initial_value
        self.global_variable_change_once_per_object = update_the_global_variable_only_once
        self.object_id = object_id
        self.global_variable_name_check = update_if_this_GV_is_true


class ResetGlobalVariables(Callback):
    """
    """

    def __init__(self, label_of_updated_GV_list=np.array(["GV"]),
                 global_variable_initial_value_list=np.array([0]), object_id=-1,
                 effect="activate", callback_name="ResetGlobalVariables", is_callback_pausable=True,
                 update_the_global_variable_only_once=False):
        Callback.__init__(self, "ResetGlobalVariables",
                          callback_name, effect, is_callback_pausable)

        self.global_variable_name_list = label_of_updated_GV_list
        self.global_variable_initial_value_list = global_variable_initial_value_list
        self.object_id = object_id


class DisplayCounter(Callback):
    """
    """

    def __init__(self, text_id=-1, display_trial=False,  label_of_updated_GV_to_use="GV",
                 display_scene_number=False,
                 display_GV=True,
                 effect="activate", callback_name="DisplayCounter", is_callback_pausable=True):
        Callback.__init__(self, "DisplayCounter",
                          callback_name, effect, is_callback_pausable)
        self.object_id = text_id
        self.display_trial = display_trial
        self.display_scene_number = display_scene_number
        self.global_variable_name = label_of_updated_GV_to_use
        self.display_GV = display_GV
