"""
Interfaces to callback and creating various kinds of effect, must of the time each callback while have an activate and a deactivate for the callback.
"""
import PTVR.Stimuli.Color as color
from PTVR.Stimuli.Utils import MetaData
import PTVR.Counter as Counter
import PTVR.Data.Interaction as Interaction
import random
import PTVR.Stimuli.Utils
import numpy as np

import pickle
from pathlib import Path
import PTVR.SystemUtils
import json
import os
import warnings


class Callback(MetaData):
    def __init__(self, type_callback, callback_name, effect, is_callback_pausable):
        self.id = int(random.random()*1e18)
        self.type = type_callback
        self.callbackName = callback_name
        self.effect = effect
        self.isCallbackPausable = is_callback_pausable


class AddGVtoResults(Callback):
    """ 
    """

    def __init__(self, effect="activate", callback_name="AddGVtoResults",
                 global_variable_label="",
                 is_callback_pausable=True):

        Callback.__init__(self, "AddGVtoResults",
                          callback_name, effect, is_callback_pausable)

        self.global_variable_label = global_variable_label


class ChangeBackgroundColor(Callback):
    """

    @param `callback_color` : Color to apply to Background
    @param `effect`  : change background color to the callback_color , 'deactivate' removes the change to the background color.
    @param `which_eye` : on which eye do you want to modify the background : "Left", "Right" , "Both". 
    @param `callback_name`: you can change the callback name that will appear in the results file
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, new_color=color.RGBColor(), effect="activate", callback_name="ChangeBackgroundColor", which_eye="Both", is_callback_pausable=True):
        Callback.__init__(self, "ChangeBackgroundColor",
                          callback_name, effect, is_callback_pausable)

        self.newColor = new_color
        self.whichEye = which_eye


class Joker(Callback):
    """ 
    """

    def __init__(self, effect="activate", callback_name="Joker",
                 list_letter_cubes=np.array([-1]), list_attractors=np.array([-1]),
                 global_variable_name_for_string="GV",
                 global_variable_name_for_joker_counter="",
                 remove_letters=False,

                 is_callback_pausable=True):

        Callback.__init__(self, "Joker",
                          callback_name, effect, is_callback_pausable)

        self.list_letter_cubes = list_letter_cubes
        self.list_attractors = list_attractors
        self.global_variable_name_for_string = global_variable_name_for_string
        self.remove_letters = remove_letters
        self.global_variable_name_for_joker_counter = global_variable_name_for_joker_counter


class AddHandControllerEventsToResultsFile(Callback):
    """
    """

    def __init__(self, eventId=-1, effect="activate", callback_name="AddHandControllerEventsToResultsFile",
                 is_callback_pausable=True):
        Callback.__init__(self, "AddHandControllerEventsToResultsFile",
                          callback_name, effect, is_callback_pausable)

        self.eventId = eventId


class InstantiateObject(Callback):
    """
    """

    def __init__(self, visual_object=PTVR.Stimuli.Utils.VisualObject(), effect="activate", callback_name="InstantiateObject", is_callback_pausable=True):
        Callback.__init__(self, "InstantiateObject",
                          callback_name, effect, is_callback_pausable)
        # The object
        self.visualObject = visual_object


class SetPosition(Callback):
    """
    """

    def __init__(self, object_id=-1, label_of_updated_GV_to_use="", position=np.array([0, 0, 0]),
                 effect="activate", callback_name="SetPosition", is_callback_pausable=True
                 ):
        Callback.__init__(self, "SetPosition", callback_name,
                          effect, is_callback_pausable)

        self.objectId = object_id
        self.label_of_updated_GV_to_use = label_of_updated_GV_to_use
        self.position = position


class DisplayVector3GV(Callback):
    """
       The effect of this callback is to display the spatial coordinates of the 
    collision point (if any) between an input device (handcontroller, headset, ...) 
    and a tangent screen everytime the handcontroller trigger is pressed.    
       These spatial coordinates are contained in a Global Variable 
    (GV) containing a vector of 3 values.

    This callback has an effect only if you have enabled recording of : 
        - handcontroller : this is done with 
            The3DWorld ( output_hand_controller = True).
        - headset : TODOOOOOOOOOOOO
    ...
    and if you have created a text object in your script (see 'text_id' below).      

    TO FINISH when recording of headset will be finished.


    Parameters
    ----------
    text_id : 
        id of a text object already created in your script. This text object
        will be used to display the Global Variable. For instance,
        this text object will define the size of the text. The default is -1.
    label_of_GV_to_use : STRING
        String that will be displayed along with the 3 values contained
        in the GV. The default is "".
    vector_components_names : np.array()
        Names that you want to display as the labels of the 3 values
        contained in the GV. The default is np.array(["x", "y", "z"]).You will use
        these default name say when you want to display cartesian coordinates.

    Returns
    -------
    None.

    """

    def __init__(self, text_id=-1, label_of_GV_to_use="",
                 text_to_display="",
                 vector_components_names=np.array(["x", "y", "z"]),
                 effect="activate", callback_name="DisplayVector3GV",
                 is_callback_pausable=True
                 ):

        Callback.__init__(self, "DisplayVector3GV", callback_name,
                          effect, is_callback_pausable)

        self.object_id = text_id
        self.global_variable_name = label_of_GV_to_use
        self.vector_components_names = vector_components_names
        self.text_to_display = text_to_display


class AugmentVision(Callback):
    """
    """

    def __init__(self, zooming_factor=1, transition_duration_in_sec=0.5,
                 is_ROAV_world_centered=False,
                 change_zooming_factor=False,
                 is_ROAV_a_frozen_image=False,
                 effect="activate", callback_name="AugmentVision", is_callback_pausable=True):
        Callback.__init__(self, "AugmentVision", callback_name,
                          effect, is_callback_pausable)

        self.factor = zooming_factor
        self.dt = transition_duration_in_sec
        self.lock_augmented_window = is_ROAV_world_centered
        self.change_zooming_factor = change_zooming_factor
        self.screenshot = is_ROAV_a_frozen_image


class ChangeObjectColor(Callback):
    """
    If effect is "deactivate", this corresponds to a color reset.
    """

    def __init__(self, object_id=-1, new_color=color.RGBColor(), effect="activate", callback_name="ChangeObjectColor", is_callback_pausable=True):
        Callback.__init__(self, "ChangeObjectColor",
                          callback_name, effect, is_callback_pausable)
        # The object to changed the color
        self.objectId = object_id
        # The color of the object when the callback is trigged
        self.newColor = new_color


class ChangeCustomObjectColor(Callback):
    """
    """

    def __init__(self, object_id=-1, new_color=color.RGBColor(), effect="activate", callback_name="ChangeCustomObjectColor", is_callback_pausable=True):
        Callback.__init__(self, "ChangeCustomObjectColor",
                          callback_name, effect, is_callback_pausable)
        # The object to changed the color
        self.objectId = object_id
        # The color of the object when the callback is trigged
        self.newColor = new_color


class ChangeObjectScale(Callback):
    """
    If effect is "deactivate", this corresponds to a position reset.
    """

    def __init__(self, object_id=-1, scaling_factor=2.0, effect="activate", callback_name="ChangeObjectScale", is_callback_pausable=True):
        Callback.__init__(self, "ChangeObjectScale",
                          callback_name, effect, is_callback_pausable)
        self.objectId = object_id
        self.scalingFactor = scaling_factor


class GlobalVariableChange(Callback):
    """
    """

    def __init__(self, operation="+", factor=1.0, global_variable_name="GV", global_variable_initial_value=0,
                 global_variable_change_once_per_object=False, object_id=-1, global_variable_name_check="",
                 effect="activate", callback_name="GlobalVariableChange", is_callback_pausable=True):
        Callback.__init__(self, "GlobalVariableChange",
                          callback_name, effect, is_callback_pausable)
        self.operation = operation
        self.operation_value = factor
        self.global_variable_name = global_variable_name
        self.global_variable_initial_value = global_variable_initial_value
        self.global_variable_change_once_per_object = global_variable_change_once_per_object
        self.object_id = object_id
        self.global_variable_name_check = global_variable_name_check


class GlobalVariableDisplay(Callback):
    """
    """

    def __init__(self, object_id=-1, display_trial=False,  global_variable_name="GV", effect="activate", callback_name="GlobalVariableDisplay", is_callback_pausable=True):
        Callback.__init__(self, "GlobalVariableDisplay",
                          callback_name, effect, is_callback_pausable)
        self.object_id = object_id
        self.display_trial = display_trial
        self.global_variable_name = global_variable_name


class AnimateObject(Callback):
    """
    Parameters
    ----------    
    object_id : 
        Id of object to be animated. The default is -1.
    effect : 
        "activate" or "deactivate". The default is "activate"
    """

    def __init__(self, object_id=-1,
                 effect="activate",
                 animation_index=0,
                 callback_name="AnimateObject", is_callback_pausable=True, animation_string="", play_until_the_end=True):
        Callback.__init__(self, "AnimateObject", callback_name,
                          effect, is_callback_pausable)
        self.objectId = object_id
        self.animation_string = animation_string
        self.animation_index = animation_index
        self.play_until_the_end = play_until_the_end


class ChangeObjectOutline(Callback):
    """
    @param `effect`: indicates if callback is activated or not. 
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, object_id=-1, outline_color=color.RGBColor(r=0.0, g=1.0, b=0.0, a=1), outline_width=0.1, effect="activate", callback_name="ChangeObjectOutline", is_callback_pausable=True):
        Callback.__init__(self, "ChangeObjectOutline",
                          callback_name, effect, is_callback_pausable)
        self.outlineColor = outline_color
        self.outlineWidth = outline_width
        self.objectId = object_id


class Grab(Callback):
    def __init__(self, object_id=-1, parent_id=-4, effect="activate",
                 keep_object_orientation=True,
                 place_object_on_placeholder=False,
                 callback_name="Grab", is_callback_pausable=True):
        Callback.__init__(self, "Grab", callback_name,
                          effect, is_callback_pausable)
        self.objectId = object_id
        self.parentId = parent_id
        self.keep_object_orientation = keep_object_orientation
        self.place_object_on_placeholder = place_object_on_placeholder


class MakeGrabable(Callback):
    def __init__(self, object_id=-1, parent_id=-4, effect="activate", callback_name="MakeGrabable", is_callback_pausable=True):
        Callback.__init__(self, "MakeGrabable", callback_name,
                          effect, is_callback_pausable)
        self.objectId = object_id


class ResetPosition(Callback):
    def __init__(self, object_id=-1, effect="activate", callback_name="ResetPosition", is_callback_pausable=True):
        Callback.__init__(self, "ResetPosition", callback_name,
                          effect, is_callback_pausable)
        self.objectId = object_id


class ResetButtons(Callback):
    def __init__(self, object_id=-1, effect="activate", callback_name="ResetButtons", is_callback_pausable=True):
        Callback.__init__(self, "ResetButtons", callback_name,
                          effect, is_callback_pausable)
        self.objectId = object_id


class Attractive(Callback):
    def __init__(self, object_id=-1, readingAttractorID=-1, attractors_list=np.array(-1),
                 attraction_distance=np.array([0.5, 0.5, 0.5]), readSupportId=-1, isRead=False,
                 expected_string="", letter_in_cube="", attractor_id_for_letter=-1,
                 effect="activate", callback_name="Attractive", is_callback_pausable=True):
        Callback.__init__(self, "Attractive", callback_name,
                          effect, is_callback_pausable)
        self.objectId = object_id
        self.attractors_list = attractors_list
        self.attraction_distance = attraction_distance
        self.isRead = isRead
        self.readSupportId = readSupportId
        self.readingAttractorID = readingAttractorID
        self.expected_string = expected_string
        self.letter_in_cube = letter_in_cube
        self.attractor_id_for_letter = attractor_id_for_letter


class Attractor(Callback):
    def __init__(self, object_id=-1, readingAttractorID=-1, readSupportId=-1,
                 expectedString="", effect="activate", callback_name="Attractor",
                 expectedLetter="",
                 is_callback_pausable=True):
        Callback.__init__(self, "Attractor", callback_name, effect,
                          is_callback_pausable)
        self.objectId = object_id
        self.readingAttractorID = readingAttractorID
        self.readSupportId = readSupportId
        self.expectedString = expectedString
        self.expectedLetter = expectedLetter


class ChangeObjectEnable(Callback):
    """
    """

    def __init__(self, object_id=-1, effect="activate", callback_name="ChangeObjectEnable", is_callback_pausable=True):
        Callback.__init__(self, "ChangeObjectEnable", callback_name, effect,
                          is_callback_pausable)
        self.objectId = object_id
        self.effect = effect  # 2 options "activate" , "deactivate"


class ChangeLaserVisibility(Callback):
    """
    """

    def __init__(self, effect="activate", callback_name="ChangeLaserVisibility", is_callback_pausable=True,
                 isVisible=True):
        Callback.__init__(self, "ChangeLaserVisibility", callback_name, effect,
                          is_callback_pausable)
        self.isVisible = isVisible


class ChangeObjectVisibility(Callback):
    """
    """

    def __init__(self, object_id=-1, effect="activate", callback_name="ChangeObjectVisibility", is_callback_pausable=True):
        Callback.__init__(self, "ChangeObjectVisibility", callback_name, effect,
                          is_callback_pausable)
        self.objectId = object_id


class ChangeObjectRotateTo(Callback):
    """
    @param `rotate_type`: "rotate_to_look_at_in_opposite_direction" , "rotate_to_look_at"
    @param `object_to_look_id`: indicates object to look AT
    @param `callback_name`:  callback name in results file
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. Default value is True.
  """

    def __init__(self, object_id=-1, rotate_type="rotate_to_look_at_in_opposite_direction", object_to_look_id=-1, effect="activate", callback_name="ChangeObjectRotateTo", is_callback_pausable=True):
        Callback.__init__(self, "ChangeObjectRotateTo", callback_name, effect,
                          is_callback_pausable)
        self.objectId = object_id
        self.rotateType = rotate_type
        self.objectToLookId = object_to_look_id


class ChangeObjectParent(Callback):
    """
    @param `callback_name`:  you can change the callback name that will appear in the results file
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, object_id=-1, parent_id=-4, effect="activate",
                 callback_name="ChangeObjectParent", is_callback_pausable=True):
        Callback.__init__(self, "ChangeObjectParent", callback_name, effect,
                          is_callback_pausable)
        self.objectId = object_id
        self.parentId = parent_id


class CreateNewResultFile(Callback):
    """
    @param `callback_name`:  you can change the callback name that will appear in the results file
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`.
  """

    def __init__(self, data_columns, data, object_id=-1, file_name="new",
                 callback_name="CreateNewResultFile", is_callback_pausable=True):
        Callback.__init__(self, "CreateNewResultFile", callback_name, "activate",
                          is_callback_pausable)
        self.objectId = object_id
        self.dataColumns = data_columns
        self.fileName = file_name
        for i in range(len(data)):
            data[i] = str(data[i])
        self.data = data  # string


class SaveText(Callback):
    """
    @param `callback_name`:  you can change the callback name that will appear in the results file
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, string_to_append="", text="", word_num=[], effect="activate",
                 callback_name="SaveText", is_callback_pausable=True):
        Callback.__init__(self, "SaveText", callback_name,
                          effect, is_callback_pausable)
        self.text = text
        self.nameAppendString = string_to_append
        self.wordNum = word_num


class FillInResultsFileColumn(Callback):
    """
    This Callback will add in the column 'column_name' of the results file the 'a_string' when this callback is activated
    and remove the a_string when deactivated \n

    @param  a_string : value or string to add when effect is activated and to remove when effect is deactivated.\n
    @param effect : activate or deactivate \n
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 

    """

    def __init__(self, column_name="", a_string="",  effect="activate",
                 callback_name="FillInResultsFileColumn", is_calculator_id=False,
                 is_callback_pausable=True):
        Callback.__init__(self, "FillInResultsFileColumn", callback_name, effect,
                          is_callback_pausable)
        self.columnName = column_name
        self.columnValue = a_string
        self.isCalculatorId = is_calculator_id


class UpdateLastPoint(Callback):
    """
    Update the last point placed on a graph
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. Default value is True.
    """

    def __init__(self, graph_id=-1, callback_name="UpdateLastPoint",
                 is_callback_pausable=True):
        Callback.__init__(self, "UpdateLastPoint", callback_name, "activate",
                          is_callback_pausable)
        self.graphId = graph_id


class AddNewPoint(Callback):
    """
    Add a new point to the graph wich is still not linked to the other 
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. Default value is True.
    """

    def __init__(self,  graph_id=-1, callback_name="AddNewPoint", is_callback_pausable=True):
        Callback.__init__(self, "AddNewPoint", callback_name,
                          "activate", is_callback_pausable)
        self.graphId = graph_id


class ShowAllPoints(Callback):
    """
    Shows on graph all point previously created with the AddNewPoint callback
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. Default value is True.
    """

    def __init__(self,  graph_id=-1, callback_name="ShowAllPoints", is_callback_pausable=True):
        Callback.__init__(self, "ShowAllPoints", callback_name,
                          "activate", is_callback_pausable)
        self.graphId = graph_id


class HighlightButton(Callback):
    def __init__(self, object_id=-1,
                 effect="activate",
                 callback_name="HighlightButton", is_callback_pausable=True):
        Callback.__init__(self, "HighlightButton",
                          callback_name, effect, is_callback_pausable)
        self.objectId = object_id


class ProcessButton(Callback):
    def __init__(self, object_id=-1,
                 effect="activate",
                 callback_name="ProcessButton", is_callback_pausable=True,
                 expected_answer=-1,
                 feedback_bad_answer="",
                 feedback_good_answer=""):
        Callback.__init__(self, "ProcessButton", callback_name,
                          effect, is_callback_pausable)
        self.objectId = object_id
        self.expected_answer = expected_answer
        self.feedback_bad_answer = feedback_bad_answer
        self.feedback_good_answer = feedback_good_answer


class DeleteLastPoint(Callback):
    """
    Delete the last point created on the graph 
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. Default value is True.
    """

    def __init__(self,  graph_id=-1, callback_name="DeleteLastPoint", is_callback_pausable=True):
        Callback.__init__(self, "DeleteLastPoint", callback_name,
                          "activate", is_callback_pausable)
        self.graphId = graph_id


class SaveLastPoint(Callback):
    """
    Save the last point created on the graph 
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. Default value is True.
    """

    def __init__(self,  graph_id=-1, callback_name="SaveLastPoint", is_callback_pausable=True):
        Callback.__init__(self, "SaveLastPoint", callback_name,
                          "activate", is_callback_pausable)
        self.graphId = graph_id


class ChangeObjectText(Callback):
    """
    This Callback will save the given string into a list
    @param text_object_id : the id of text to change
    @param text : the string that has to be save in the list
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. Default value is True.
    """

    def __init__(self, text_object_id=-1, text="", font_size_in_points=0, is_bold=False, is_italic=False, effect="activate", callback_name="ChangeObjectText", is_callback_pausable=True):
        Callback.__init__(self, "ChangeObjectText",
                          callback_name, effect, is_callback_pausable)
        self.textObjectId = text_object_id
        self.fontSize = font_size_in_points

        if (is_bold):
            text = "<b>"+text+"</b>"
        if (is_italic):
            text = "<i>"+text+"</i>"
        self.text = text


class CalibrationEyesTracking(Callback):
    """
    This Callback will launch calibration Sranipall
    """

    def __init__(self, is_callback_pausable=True, callback_name="CalibrationEyesTracking"):
        Callback.__init__(self, "CalibrationEyesTracking",
                          callback_name, "activate", is_callback_pausable)


class PauseCurrentScene(Callback):
    """
    This Callback will Pause the Current Scene if the effect is activate, if the effect is deactivate resumes the Current Scene
    @param effect : activate pause Current Scene, deactivate resume Current Scene
    @param `callback_name`:  
    """

    def __init__(self, effect="activate", callback_name="PauseCurrentScene"):
        Callback.__init__(self, "PauseCurrentScene",
                          callback_name, effect, False)


class RestartCurrentScene(Callback):
    """
    This Callback will Reload the Current Scene if the effect is activate
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`.
    """

    def __init__(self, callback_name="RestartCurrentScene", is_callback_pausable=True):
        Callback.__init__(self, "RestartCurrentScene",
                          callback_name, "activate", is_callback_pausable)


class RestartCurrentTrial(Callback):
    """
    This Callback will Reload the Current Trial first scene if the effect is activate
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, callback_name="RestartCurrentTrial", is_callback_pausable=True):
        Callback.__init__(self, "RestartCurrentTrial",
                          callback_name, "activate", is_callback_pausable)


class EndCurrentScene(Callback):
    """
    This Callback will end CurrentScene and will load the NextScene
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, callback_name="EndCurrentScene", is_callback_pausable=True, id_next_scene=-1):
        Callback.__init__(self, "EndCurrentScene", callback_name,
                          "activate", is_callback_pausable)
        self.idNextScene = id_next_scene

    def SetNextScene(self, id_next_scene=0):
        """
        @param id_next_scene : id of the NextScene to load
        """
        self.idNextScene = id_next_scene


class NextScene(Callback):
    """
    This Callback will end CurrentScene and will load the NextScene
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, callback_name="NextScene", is_callback_pausable=True, index_next_scene=-1,
                 global_variable_name_used=""):
        Callback.__init__(self, "NextScene", callback_name,
                          "activate", is_callback_pausable)
        self.indexNextScene = index_next_scene
        self.global_variable_name_used = global_variable_name_used


class DrawWireArc(Callback):
    """
    This Callback will end CurrentScene and will load the NextScene
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, callback_name="DrawWireArc", is_callback_pausable=True,
                 center=np.array([]), normal=np.array([]), from_point=np.array([]), rotation_in_global_CS=np.array([]),
                 angle=0, radius=0):
        Callback.__init__(self, "DrawWireArc", callback_name,
                          "activate", is_callback_pausable)
        self.center = center
        self.normal = normal
        self.from_point = from_point
        self.angle = angle
        self.radius = radius
        self.rotation_in_global_CS = rotation_in_global_CS


class EndCurrentTrial(Callback):
    """
    This Callback will endCurrentTrial and will load the NextTrial
    QUE VEUT DIRE : it will LOAD  .... !!!
    AT EC : explain number_of_trial
    AT JTM : rename number_of_trial -> number_of_trials_per_block
    @param scene_to_end : the scene_to_end EC: ???????
    @param `callback_name`: you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`.
    """

    def __init__(self, scene_to_end, number_of_trial=1, reset_number_trial=-1,
                 callback_name="EndCurrentTrial", is_callback_pausable=True):
        Callback.__init__(self, "EndCurrentTrial", callback_name,
                          "activate", is_callback_pausable)
        # if( number_of_trial < scene_to_end.trial+1 ): # changé par EC le 20 oct 2022
        if (scene_to_end.trial+1 > number_of_trial):

            self.idNextTrial = reset_number_trial
        else:
            self.idNextTrial = scene_to_end.trial+1
        print("idNextTrial : "+str(self.idNextTrial))

    def SetNextTrial(self, id_next_trial=0):
        """
        @param idNextTrial : id of the NextTrial to load
        """
        self.idNextTrial = id_next_trial


class PrintResultFileLine(Callback):
    """

    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, callback_name="PrintResultFileLine", is_callback_pausable=True):
        Callback.__init__(self, "PrintResultFileLine",
                          callback_name, "activate", is_callback_pausable)
        self.resultFile = ["_Main_"]


class SaveResult(Callback):
    """
    This Callback will print SaveResult inside the .csv by Default 
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, callback_name="SaveResult", is_callback_pausable=True):
        Callback.__init__(self, "SaveResult", callback_name,
                          "activate", is_callback_pausable)
        self.resultFile = ["_Main_"]


class TakePicture(Callback):
    """
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, filename="", resolution_width=1920, resolution_height=1080, callback_name="TakePicture", is_callback_pausable=True):
        Callback.__init__(self, "TakePicture", callback_name,
                          "activate", is_callback_pausable)
        self.resWidth = resolution_width
        self.resHeight = resolution_height
        self.filename = filename


class CheckerCallback(Callback):
    """
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, callbacks=[], effect="activate", callback_name="Checkercallback", is_callback_pausable=True):
        Callback.__init__(self, "CheckerCallback",
                          callback_name, effect, is_callback_pausable)
        self.checks = []
        self.callbacks = callbacks


class AddInteractionPassedToCallback(Callback):
    """
    Ce Callback peut ajouter une interaction lorsque cet callback est appelé 
    pour rappel une interaction consiste à une liste d'input qui lorsqu'enclenché appel la liste d'callbacks 
    @param `callback_name`:  is use to indicate the callback name in resultfile is give possibility to customize the name.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. Default value is True.
    """

    def __init__(self, events=[], callbacks=[], effect="activate", callback_name="AddInteractionPassedToCallback", is_callback_pausable=True):
        Callback.__init__(self, "AddInteractionPassedToCallback",
                          callback_name, effect, is_callback_pausable)
        self.interaction = []
        self.interaction.append(Interaction.Interaction(
            events=events, callbacks=callbacks))


class ChangeAudioSettings(Callback):
    """
    @param `callback_name`: you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, audio_id=-1, effect="activate", is_a_loop=False, callback_name="ChangeAudioSettings",
                 label_of_updated_GV_to_use="", is_callback_pausable=True, clip_name=""):
        Callback.__init__(self, "ChangeAudioSettings",
                          callback_name, effect, is_callback_pausable)
        self.audioId = audio_id
        self.isLoop = is_a_loop
        self.global_variable_to_use = label_of_updated_GV_to_use
        self.clip_name = clip_name


class ChangeHapticSettings(Callback):
    """
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, effect="activate", controller="RIGHT", frequency=10, amplitude=1, callback_name="ChangeHapticSettings", is_callback_pausable=True):
        Callback.__init__(self, "ChangeHapticSettings",
                          callback_name, effect, is_callback_pausable)
        self.controller = controller
        self.frequency = frequency
        self.amplitude = amplitude


class Quit(Callback):
    """
    This Callback will close PTVR.exe
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, callback_name="Quit", is_callback_pausable=True):
        Callback.__init__(self, "Quit", callback_name,
                          "activate", is_callback_pausable)


class ExecutePythonScript(Callback):
    """
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`.
    """

    def __init__(self, python_executable_complete_path, callback_name="ExecutePythonScript",
                 is_callback_pausable=True,
                 script_path="",  # Root is PTVR_Researchers/
                 script_name=""):
        Callback.__init__(self, "ExecutePythonScript",
                          callback_name, "activate", is_callback_pausable)
        self.scriptPath = script_path
        self.scriptName = script_name
        self.pythonExecutablePath = python_executable_complete_path


class AdaptiveResponseProcessingFor3dObject(Callback):
    """
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`.
    """

    def __init__(self, python_executable_complete_path, callback_name="AdaptiveResponseProcessingFor3dObject",
                 is_callback_pausable=True,
                 script_path="",  # Root is PTVR_Researchers/
                 script_name="",
                 observer_response="left",
                 objects_list=np.array(-1),
                 object_attribute="size_in_meters",
                 procedure="staircase",
                 staircases=None,
                 initial_staircase_id=1,
                 attribute_dimensions=np.array([0, 1, 2]),
                 path_to_staircases=None,
                 name_of_subject=""):
        """
        TODOOOOOO: EC
        # dans le HELP : function: process the Response of the observer with
        # an adaptive procedure. This procedure will affect attributes
        # of a 3D object.

        Parameters
        ----------
        python_executable_complete_path : TYPE
            DESCRIPTION.
        callback_name : TYPE, optional
            DESCRIPTION. The default is "ControlFromPythonScript".
        is_callback_pausable : TYPE, optional
            DESCRIPTION. The default is True.
        script_path : TYPE, optional
            DESCRIPTION. The default is "".
        ## Root is PTVR_Researchers/                 script_name : TYPE, optional
            DESCRIPTION. The default is "".
        script_option : TYPE, optional
            DESCRIPTION. The default is "left".
        objects_list : TYPE, optional
            DESCRIPTION. The default is np.array(-1).
        object_attribute : TYPE, optional
            DESCRIPTION. The default is "size_in_meters".
        procedure : TYPE, optional
            DESCRIPTION. The default is "staircase".
        staircases : TYPE, optional
            DESCRIPTION. The default is None.
        initial_staircase_id : TYPE, optional
            DESCRIPTION. The default is 1.
        attribute_dimensions : TYPE, optional
            DESCRIPTION. The default is np.array([0,1,2]).
        path_to_staircases : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        None.

        """
        Callback.__init__(self, "AdaptiveResponseProcessingFor3dObject",
                          callback_name, "activate", is_callback_pausable)

        self.scriptPath = script_path
        self.scriptName = script_name
        self.observer_response = observer_response
        self.pythonExecutablePath = python_executable_complete_path
        self.objects_list = [item.id for item in objects_list]
        self.object_attribute = object_attribute
        self.attribute_dimensions = attribute_dimensions
        self.initial_staircase_id = initial_staircase_id
        self.staircase_file_name = ""

        # create Pickle
        if (staircases is not None):

            dico_objects_staircases = {}
            staircase_id = 1
            for staircase in staircases:
                dico_objects = {}
                staircase.staircase_id = staircase_id
                object_attribute_for_staircase = staircase.start_val
                for item in objects_list:
                    dico_objects[item.id] = {object_attribute: np.array([object_attribute_for_staircase,
                                                                         object_attribute_for_staircase,
                                                                         # getattr(item, object_attribute),
                                                                         object_attribute_for_staircase]),
                                             "attribute_dimensions": attribute_dimensions}

                dico_objects_staircases[staircase.staircase_id] = dico_objects
                staircase_id += 1

            if path_to_staircases is None:

                default_path = Path(PTVR.SystemUtils.UnityPTVRRoot) / "PTVR_Researchers" / \
                    "PTVR_Operators" / "Results" / PTVR.SystemUtils.get_ptvr_app_name()
            else:
                default_path = Path(path_to_staircases +
                                    PTVR.SystemUtils.get_ptvr_app_name())

            # Folder creation if this does not exist
            if not os.path.exists(default_path):
                os.makedirs(default_path)

            with open(default_path.__str__() + "/experiment_data.pickle", 'wb') as handle:
                pickle.dump(dico_objects_staircases, handle,
                            protocol=pickle.HIGHEST_PROTOCOL)

            list_staircases = []

            if (staircases is not None):
                for sc in staircases:
                    list_staircases.append(sc.__dict__)
                    # the json file where the output must be stored

                self.staircase_file_name = "staircases_" + \
                    name_of_subject + "_" + sc.date + ".json"

                out_file = open(default_path.__str__() + "\\" +
                                self.staircase_file_name, "w")
                json.dump(list_staircases, out_file, indent=6)
                out_file.close()
                # with open(default_path.__str__() + "/staircases.pickle", 'wb') as handle:
                #     pickle.dump(staircase_handler, handle, protocol=pickle.HIGHEST_PROTOCOL)


class AdaptiveResponseProcessingForGlobalVariable(Callback):
    """
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`.
    """

    def __init__(self, python_executable_complete_path, callback_name="AdaptiveResponseProcessingForGlobalVariable",
                 is_callback_pausable=True,
                 script_path="",  # Root is PTVR_Researchers/
                 script_name="",
                 observer_response="left",
                 objects_list=np.array(-1),
                 object_attribute="size_in_meters",
                 procedure="staircase",
                 staircases=None,
                 initial_staircase_id=1,
                 path_to_staircases=None,
                 name_of_subject=""):
        """
        TODOOOOOO: EC
        # dans le HELP : function: process the Response of the observer with
        # an adaptive procedure. This procedure will affect attributes
        # of a 3D object.

        Parameters
        ----------
        python_executable_complete_path : TYPE
            DESCRIPTION.
        callback_name : TYPE, optional
            DESCRIPTION. The default is "ControlFromPythonScript".
        is_callback_pausable : TYPE, optional
            DESCRIPTION. The default is True.
        script_path : TYPE, optional
            DESCRIPTION. The default is "".
        ## Root is PTVR_Researchers/                 script_name : TYPE, optional
            DESCRIPTION. The default is "".
        script_option : TYPE, optional
            DESCRIPTION. The default is "left".
        objects_list : TYPE, optional
            DESCRIPTION. The default is np.array(-1).
        object_attribute : TYPE, optional
            DESCRIPTION. The default is "size_in_meters".
        procedure : TYPE, optional
            DESCRIPTION. The default is "staircase".
        staircases : TYPE, optional
            DESCRIPTION. The default is None.
        initial_staircase_id : TYPE, optional
            DESCRIPTION. The default is 1.
        attribute_dimensions : TYPE, optional
            DESCRIPTION. The default is np.array([0,1,2]).
        path_to_staircases : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        None.

        """
        Callback.__init__(self, "AdaptiveResponseProcessingForGlobalVariable",
                          callback_name, "activate", is_callback_pausable)

        self.scriptPath = script_path
        self.scriptName = script_name
        self.observer_response = observer_response
        self.pythonExecutablePath = python_executable_complete_path
        self.objects_list = [item.id for item in objects_list]
        self.object_attribute = object_attribute
        self.initial_staircase_id = initial_staircase_id
        self.staircase_file_name = ""

        attribute_dimensions = np.array([0, 1, 2])

        # create Pickle
        if (staircases is not None):

            dico_objects_staircases = {}
            staircase_id = 1
            for staircase in staircases:
                dico_objects = {}
                staircase.staircase_id = staircase_id
                object_attribute_for_staircase = staircase.start_val
                for item in objects_list:
                    dico_objects[item.id] = {object_attribute: np.array([object_attribute_for_staircase,
                                                                         object_attribute_for_staircase,
                                                                         # getattr(item, object_attribute),
                                                                         object_attribute_for_staircase]),
                                             "attribute_dimensions": attribute_dimensions}

                dico_objects_staircases[staircase.staircase_id] = dico_objects
                staircase_id += 1

            if path_to_staircases is None:

                default_path = Path(PTVR.SystemUtils.UnityPTVRRoot) / "PTVR_Researchers" / \
                    "PTVR_Operators" / "Results" / PTVR.SystemUtils.get_ptvr_app_name()
            else:
                default_path = Path(path_to_staircases +
                                    PTVR.SystemUtils.get_ptvr_app_name())

            # Folder creation if this does not exist
            if not os.path.exists(default_path):
                os.makedirs(default_path)

            with open(default_path.__str__() + "/experiment_data.pickle", 'wb') as handle:
                pickle.dump(dico_objects_staircases, handle,
                            protocol=pickle.HIGHEST_PROTOCOL)

            list_staircases = []

            if (staircases is not None):
                for sc in staircases:
                    list_staircases.append(sc.__dict__)
                    # the json file where the output must be stored

                self.staircase_file_name = "staircases_" + \
                    name_of_subject + "_" + sc.date + ".json"

                out_file = open(default_path.__str__() + "\\" +
                                self.staircase_file_name, "w")
                json.dump(list_staircases, out_file, indent=6)
                out_file.close()
                # with open(default_path.__str__() + "/staircases.pickle", 'wb') as handle:
                #     pickle.dump(staircase_handler, handle, protocol=pickle.HIGHEST_PROTOCOL)


class AugmentationCamera(Callback):
    """
    This Callback will change the zoom on a PTVR camera (CameraPTVR).
    @param camera_id : Id of Object to change the zoom
    @param zoom : value of zoom
    @param `callback_name`:  you can change the callback name that will appear in the results file.
    @param `is_callback_pausable`: Indicates if the callback is affected by the callback `PauseCurrentScene`. 
    """

    def __init__(self, camera_id=-1, zoom=1.0, effect="activate", callback_name="AugmentationCamera", is_callback_pausable=True):
        Callback.__init__(self, "AugmentationCamera",
                          callback_name, effect, is_callback_pausable)
        self.cameraId = camera_id
        self.zoom = zoom
