# -*- coding: utf-8 -*-
"""
TODO : Add Interaction description
"""
import PTVR.Stimuli.Color as color
from PTVR.Stimuli.Texture import Texture
from PTVR.Stimuli.Utils import MetaData
from copy import deepcopy

class Interaction (MetaData):
    def __init__(self, events  , callbacks ):
       self.events = deepcopy(events)
       self.callbacks = deepcopy(callbacks)
       self.type = "interaction"
    pass

