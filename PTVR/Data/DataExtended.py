# -*- coding: utf-8 -*-
import PTVR.Visual # Used to create the experiment
import PTVR.SystemUtils # Used to launch the experiment
import numpy as np
import math
from PTVR.Stimuli.Utils import MetaData
import csv
import PTVR.Tools as tools
import pandas

class Extended:
    def __init__(self,file_name):
        """
        Parameters
        ----------
        file_name : indicate the filename and where is from the code who call this function.
        """
        header = []
        rows = []
        # Read the existing CSV file and get the header and data
        with open(file_name, 'r') as file:
            reader = csv.reader(file, delimiter=';')
            header = next(reader)
            print("header " + str(header))
            for row in reader:
                rows.append(row)
        self.header = header
        self.rows = rows
        self.new_rows = []
          
    def generate_csv(self,new_file_name = "extended.csv"):
        """
        Parameters
        ----------
        new_file_name : string indicate the name the csv while have.
        """
        with open(new_file_name , 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(self.header)
            for i in range(0,len(self.rows)-1):
                self.rows[i].append(self.new_rows[i])
                writer.writerow(self.rows[i])
                
    
    def add_data(self,column_name,data):
        """
        Parameters
        ----------
        column_name : string indicate in which header save the data given.
        data : the data to store
        """
        # Check if the specified column name exists in the header
        print("header inside : " + str(self.header))
        if column_name not in self.header:
            self.header.append(column_name)
   

        self.new_rows.append(data)
        
    def calculate_angle_between_three_points_in_deg(self,A_point,B_point,C_point,column):
        """
        Parameters
        ----------
        A_point : np.array([x,y,z])
        B_point : np.array([x,y,z])
        C_point : np.array([x,y,z])
        column : string to indicate in which header save the data returns

        Returns
        -------
        angle_deg : angle between B and C in degree
        """
        AB = tools.points_to_vec(point_a = A_point , point_b = B_point)
        AC = tools.points_to_vec(point_a = A_point , point_b = C_point)
        angle_deg = tools.angle_between_two_vectors_deg(a_vec = AB,  b_vec = AC)
        self.add_data(column_name = column, data = angle_deg)
        return angle_deg
      

        
    def calculate_theta():
        print()
        # Column theta x
        # Column theta y
    def calculate_in_perimetric():   
        print()
        # Column Eccentricity
        # Column HalfMeridian
        
        # Column Distance Combined to Target