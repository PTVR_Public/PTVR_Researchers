"""
Interfaces to event and creating various kinds of displays, for example, a timeout display or a user option display.
"""
from PTVR.Stimuli.Utils import MetaData
import random
import numpy as np


class InputEvent(MetaData):
    def __init__(self):
        self.id = int(random.random()*1e18)
    pass

# class ExampleInput(InputSystem):
# def __init__(self, validResponses=['y', 'n'],mode="press"):#=set({'y', 'n'}
# self.mode = mode     ## 3 options available "press", "hold" and "release"
# self.type = "exampleInput"


class TriggerWhenValueIsReached (InputEvent):
    """
    This event is triggered depending on a comparison between 
    'TriggerWhenValueIsReached' and the value 
    contained in the Global Variable (GV) called 'label_of_updated_GV_to_use'.    
    This comparison is defined by 'comparison_sign' 
    (for example, comparison_sign = "<=").

    By default, if 'comparison_sign' is say ">=", then the "Score" counter GV 
    is reset if the value contained in this counter is larger than or equal to 
    'value_to_be_reached'. 
    This resetting behaviour will not occur 
    
    If the counter GV is called "another_score", then this counter GB will be
    reset as described above ONLY if you change the default argument to 
    'global_variable_to_rest'

    Note : This "Score" counter GV can be updated notably by the
    CounterGVupdate () callback. 

    Parameters
    ----------
    value_to_be_reached : 
        The comparison between the value passed to 'value_to_be_reached'
        and the value contained in the 
        Global Variable (GV) called 'label_of_updated_GV_to_use' determines
        if the present Event is triggered.        
        The default is 10.
    label_of_updated_GV_to_use : 
        Name of the GV whose content must be compared to 'value_to_be_reached'. 
        The default is "Score".
    comparison_sign : 
        "=", "<", ">", "<=", or ">=". 
        The default is "=".        
        
    global_variables_to_reset : (LIST).
        The labels in this list define one or several GVs that are 
        reset to 0 everytime this event is triggered.  
        The default is np.array([ "Score"]). 
        Beware ! if  label_of_updated_GV_to_use is not called "Score" 
        (say it is called "score_2"), note that this "score_2" counter will be
        reset after current event is triggered ONLY if you have passed
        the correct argument to 'global_variables_to_reset' as in following line:
        global_variables_to_reset = np.array ( ["score_2"] )    
                                                                       
 
    event_name : 
        This name will be displayed in the results file everytime the present
        event is triggered.


    Returns
    -------
    None.

    """

    def __init__(self,
                 value_to_be_reached=10,
                 label_of_updated_GV_to_use="Score",
                 comparison_sign="=",  # "=" "<" ">" "<=" ">="
                 global_variables_to_reset=np.array(["Score"]),
                 event_name="TriggerWhenValueIsReached"):

        ##
        InputEvent.__init__(self)
        self.global_variable_max_value = value_to_be_reached
        self.global_variable_name = label_of_updated_GV_to_use
        self.global_variables_to_reset = global_variables_to_reset
        self.operation = comparison_sign
        ##
        # self.durationInMs=durationInMs
        # mode : 3 options available "press", "hold" and "release"
        self.mode = "press"
        self.type = "triggerWhenValueIsReached"
        self.eventName = event_name


class TriggerWhenBooleanIsReached (InputEvent):
    def __init__(self,
                 boolean_to_be_reached=True,
                 label_of_updated_GV_to_use="Score",
                 global_variables_to_reset=np.array([]),
                 event_name="TriggerWhenValueIsReached"):
        """


        Parameters
        ----------
        boolean_to_be_reached : TYPE, optional
            DESCRIPTION. The default is True.
        label_of_updated_GV_to_use : TYPE, optional
            DESCRIPTION. The default is "Score".
        global_variables_to_reset : TYPE, optional
            DESCRIPTION. The default is np.array([]).
        event_name : TYPE, optional
            DESCRIPTION. The default is "TriggerWhenValueIsReached".

        Returns
        -------
        None.

        """
        ##
        InputEvent.__init__(self)
        if (boolean_to_be_reached):
            self.global_variable_max_value = 1.0
        else:
            self.global_variable_max_value = 0.0
        # self.global_variable_max_value = boolean_to_be_reached
        self.global_variable_name = label_of_updated_GV_to_use
        self.global_variables_to_reset = global_variables_to_reset
        self.boolean_to_be_reached = boolean_to_be_reached
        ##
        # self.durationInMs=durationInMs
        # mode : 3 options available "press", "hold" and "release"
        self.mode = "press"
        self.type = "triggerWhenBooleanIsReached"
        self.eventName = event_name

        """


        """
