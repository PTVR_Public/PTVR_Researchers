"""
Interfaces to event and creating various kinds of displays, for example, a timeout display or a user option display.
"""
from PTVR.Stimuli.Utils import MetaData
from PTVR.Stimuli.Utils import VirtualPoint
from PTVR.Stimuli.Utils import VirtualPoint2D
from PTVR.Stimuli.Utils import Button
import copy
import random
import hashlib
import sys
import numpy as np
import PTVR.Stimuli.Color as color
import PTVR.LogManager as Log
import ctypes

class InputEvent(MetaData):
    def __init__(self):
       self.id = int(random.random()*1e18)
    pass

##class ExampleInput(InputSystem):
##    def __init__(self, validResponses=['y', 'n'],mode="press"):#=set({'y', 'n'}
##        self.mode = mode     ## 3 options available "press", "hold" and "release"
##        self.type = "exampleInput"

class Keyboard(InputEvent):
    """
    @param  mode : 3 options available "press", "hold" and "release" \n

    @param input_name: is used to indicate the event name in the results file. It allows you to customize the name. \n
    @param validResponses : indicates which Keycaps can be used. \n
    "a":
    "b":
    "c":
    "d":
    "e":
    "f":
    "g":
    "h":
    "i":
    "j":
    "k":
    "l":
    "m":
    "n":
    "o":
    "p":
    "q":
    "r":
    "s":
    "t":
    "u":
    "v":
    "x":
    "y":
    "z":
    "enter":
    "space":
    "scroll lock":
    "numlock":
    "caps lock":
    "right shift":
    "left shift":
    "right ctrl":
    "left ctrl":
    "right alt":
    "left alt":
    "backspace": 
    "delete":
    "tab":
    "pause":
    "escape":
    "insert":
    "home":
    "end":
    "page up":
    "page down":
    "f1":
    "f2":
    "f3":
    "f4":
    "f5":
    "f6":
    "f7":
    "f8":
    "f9":
    "f10":
    "f11":
    "f12":
    "numPad0":
    "numPad1":
    "numPad2":
    "numPad3":
    "numPad4":
    "numPad5":
    "numPad6":
    "numPad7":
    "numPad8":
    "numPad9":
    "numPad*":
    "numPad+":
    "numPad/":
    "numPad=":
    "numPad-":
    "left":
    "right":
    "down":
    "up":
    """
    def __init__(self, valid_responses=['y', 'n'],mode="press",event_name="keyboard"):#=set({'y', 'n'}
        ##
        InputEvent.__init__(self)
        self.validResponses = valid_responses
        ##mode : 3 options available "press", "hold" and "release"
        self.mode = mode 
        self.type = "keyboard"
        self.eventName = event_name
        self.language = self.__get_keyboard_language()
        
    def __get_keyboard_language(self):
        """
        Gets the keyboard language in use by the current
        active window process. : 
        """
        #languages keyboard supported other than Qwerty
        languages = {'0x40c' : "French - France"
                     }
        user32 = ctypes.WinDLL('user32', use_last_error=True)

        # Get the current active window handle
        handle = user32.GetForegroundWindow()

        # Get the thread id from that window handle
        threadid = user32.GetWindowThreadProcessId(handle, 0)

        # Get the keyboard layout id from the threadid
        layout_id = user32.GetKeyboardLayout(threadid)

        # Extract the keyboard language id from the keyboard layout id
        language_id = layout_id & (2 ** 16 - 1)

        # Convert the keyboard language id from decimal to hexadecimal
        language_id_hex = hex(language_id)

        # Check if the hex value is in the dictionary.
        if language_id_hex in languages.keys():
            return languages[language_id_hex]
        else:
            # Return language id hexadecimal value if not found.
            return str(language_id_hex)
    #Set var 'layout' to the keyboard layout of thread #1344 (windows explorer) or 0 (current thread)   

class HandController(InputEvent):
    """
    Parameters
    ----------
    mode : 3 options available "press", "hold" and "release"  \n
    "press" is when your finger press the button
    "hold" is when your finger have already press the button and your finger still on it  \n
    "release" is when your finger stop pressing the button \n
    
    validResponses : indicates which buttons will be call event \n
    
    event_name: is use to indicate the event name in resultfile is give possibility to customize the name. \n
    The following buttons can be used \n
    'right_trigger' \n
    'right_grip' \n
    'right_touch' \n
    'left_trigger' \n
    'left_grip' \n
    'left_touch' \n
    
    """
    def __init__(self, valid_responses=['right_trigger', 'left_trigger'],mode="press",event_name ="hand_controller"):
        ##
        InputEvent.__init__(self)
        self.validResponses = valid_responses
        ## mode : 3 options available "press", "hold" and "release"
        self.mode = mode 
        self.type = "handController"
        self.eventName = event_name

class Timer (InputEvent):
    """
    Parameters
    ----------
    param delay_in_ms : delay in milliseconds (with respect to 'param timer_start') at which the event is triggered.  \n

    param timer_start: time at which the Timer starts. It can be "Start Scene", "Start Experiment", "Start Trial", "Current Time" or "Callback Timestamp".\n 
    "Current Time" is used when the event is used during an AddInteractionPassedToEvent.\n
    param event_name: this allows you to customize the event's name in the results file.\n
    """
    ##TODO split Timer and event
    def __init__(self, delay_in_ms = 0, timer_start = "Start Scene", event_name = "Timer", callback_id=-1, period_in_ms=0):
        ##
        InputEvent.__init__(self)
        self.delayInMs = delay_in_ms
        self.timerStart = timer_start
        ##
        #self.durationInMs=durationInMs
        ##mode : 3 options available "press", "hold" and "release"
        self.mode = "press"
        self.type = "timer"
        self.eventName = event_name
        self.callback_id = callback_id
        self.period_in_ms=period_in_ms

import random


class RandomTimer (Timer):
    """

    """
    def __init__(self, delay_in_ms = 0, random_delay_in_ms_min_max = np.array([0,0]), timer_start = "Start Scene", event_name = "Timer",
                 callback_id=-1, period_in_ms=0, random_interval_in_ms_min_max = np.array([0, 0])):
        ##
        
        if not ((random_delay_in_ms_min_max==np.array([0,0])).all()):
            delay_in_ms = random.randint(random_delay_in_ms_min_max[0], random_delay_in_ms_min_max[1])

        
        Timer.__init__(self, delay_in_ms = delay_in_ms, timer_start = timer_start,
                       event_name = event_name, callback_id=callback_id, period_in_ms=period_in_ms)
        
        if not ((random_interval_in_ms_min_max==np.array([0,0])).all()):
            self.t_min_max = random_interval_in_ms_min_max


class GlobalVariableEvent (InputEvent):
    """

    """
    ##TODO split Timer and event
    def __init__(self,
                 global_variable_value_reached = 10,
                 global_variable_name = "Score",
                 is_end_current_scene=False,
                 event_name = "GlobalVariableEvent"):
        ##
        InputEvent.__init__(self)
        self.global_variable_max_value = global_variable_value_reached
        self.global_variable_name = global_variable_name
        self.is_end_current_scene=is_end_current_scene
        ##
        #self.durationInMs=durationInMs
        ##mode : 3 options available "press", "hold" and "release"
        self.mode = "press"
        self.type = "globalVariableEvent"
        self.eventName = event_name

class PointedAt(InputEvent): 
    """
    Parameters
    ----------
    target_id : Object that must be pointed at. The default is -1.
    
    activation_cone_origin_id : 
        Description: id of the pointing device. 
    ex. activation_cone_origin_id = my_world.headset.id.
    
    -1 -> my_world.handControllerLeft.id
    
    -2 -> my_world.handControllerRight.id
    
    -3 -> my_world.headset.id (default)
    
    -6 -> my_world.eyeLeft.id
    
    -7 -> my_world.eyeRight.id
    
    -8 -> my_world.eyeCombined.id    
        
    activation_cone_radius_deg : Radius in degrees of the pointing activation area. The default is 0.
    
    activation_cone_depth : The depth in meters below which pointing is operational. The default is 1000.
    
    activation_cone_ecc_hm : eccentricity and half-meridian of the pointing cone axis. The default is np.array([0.0, 0.0]).
   
    mode : "press" (default) or "release".
    
    event_name : It can be any name you choose. The default is "pointed_at".

    Returns
    -------
    None.

    """
    
    def __init__(self,target_id = -1, activation_cone_origin_id = -3, activation_cone_radius_deg = 0, activation_cone_depth = 1000, activation_cone_ecc_hm = np.array([0.0,0.0]),mode="press", event_name ="pointed_at"):   
        InputEvent.__init__(self)
        self.pointingConeRadiusDeg = activation_cone_radius_deg
        self.pointingConeDepth = activation_cone_depth
        ##pointerEccHM 
        self.eccentricity =float( activation_cone_ecc_hm[0])
        self.halfMeridian = float(activation_cone_ecc_hm[1])
        ##mode : 3 options available "press", "hold" and "release"
        self.mode = mode 
        self.coneOriginId = activation_cone_origin_id
        self.type = "pointedAt"
        self.targetId = target_id
        self.eventName = event_name
        self.coneHash = self.__hash_collision_cone(activation_cone_depth , activation_cone_radius_deg, activation_cone_ecc_hm)

    def __hash_collision_cone (self,length, radius, EccHM):
        maliste = []
        maliste.append(length)
        maliste.append(radius)
        maliste.append(self.eccentricity)
        maliste.append(self.halfMeridian)
        return (int.from_bytes(hashlib.md5(str(maliste).encode()).digest(), byteorder=sys.byteorder)%1000000000000000000)

class Button(InputEvent):
   """ 
   @param  mode : 3 options available "press", "hold" and "release \n
   @param id_object_button : indicate id of the object button to link to the event can be for the moment only a ButtonUI \n
   @param event_name: is use to indicate the event name in resultfile is give possibility to customize the name.\n

   """
   def __init__(self,mode = "press",id_object_button = -1,event_name = "button"):
        InputEvent.__init__(self)
        self.mode = mode
        self.idObjectButton = id_object_button
        self.eventName = event_name
        self.type = "button"

        #if id_object_button is -1:
           #Log.SetError("Setup an idObjectButton to use this event")
            #Create World ButtonUI

class DistanceFromPoint(InputEvent) :
    """ 
    @param  point_coordinates : coordinates of the point in the center of the area.  \n
    @param distance_from_point_in_meters : distance defining the area around the point to check. \n
    @param event_name: is use to indicate the event name in resultfile is give possibility to customize the name. \n

    """
    def __init__(self,activation_area_center_point =[0.0,0.0,0.0],
                 activation_area_radius=10,
                 event_name ="distanceFromPoint",
                 mode="press"):
        ##
        InputEvent.__init__(self)
        self.pointCoordinates = activation_area_center_point
        self.distanceFromPointInMeters = activation_area_radius
        self.objectTracked = "headset"
        ##mode : 3 options available "press", "hold" and "release"
        self.mode = mode
        self.type = "distanceFromPoint"
        self.eventName = event_name
         
           
class Display(object):
    """Associate an event with each scene which determines when the scene ends.
       A scene can end after a pre-determined time (e.g., show a stimulus for
       500 ms), or after a user event (e.g., a valid key press or a selection).
       All such options are derived from this class. There can be more complex options, such as
       user event within a certain time, or a reminder each k ms, etc."""

    def show(self):
       # the stimulus is shown
       pass

    def get(self):
       # get the outcome
       pass


class UserOptionDisplay(Display):
    # the scene is shown until an user-level event is triggered
    def __init__(self, events=HandController()):
        self.events = events

    def to_json(self):
        return dict(type="user_option", events=self.events)


class TimerDisplay(Display):
    def __init__(self, ms=1000):
        self.ms = ms

    def to_json(self):
        return dict(type="Timer", ms=self.ms)


class UserOptionWithTimeoutDisplay(UserOptionDisplay):
    def __init__(self, ms=1000):
        super.__init__()
        self.ms = ms


class UserOptionWithSoundFeedbackDisplay(UserOptionDisplay):
    pass