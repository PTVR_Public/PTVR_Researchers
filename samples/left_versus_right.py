""""
An experiment to study acuity on the left and right visual field if the stimulus is
showed for a very short period of time.
"""

import numpy as np
from copy import deepcopy

import ptvr.experiment
import ptvr.stimuli.world
import ptvr.data.input as input
import ptvr.stimuli.color as color


def main():
    print("Creating an experiment to study left versus right visual acuity...")

    # Set-up the experiment file
    exp = ptvr.experiment.Experiment(name="LeftVRightExperiment", output_file="expLeftVRight.txt")

    # Add a fixation screen with info on the experiment
    fs = ptvr.stimuli.world.FixationScene(text="In this experiment, you shall see stimuli on either left/right side "
                                               "of the fixation cross.\n You have to identify which."
                                               "\nPress side buttons to continue.")
    exp.add_scene(fs)

    num = 0  # A variable to store id numbers
    # Visual scene that holds all the visual objects
    t1 = ptvr.stimuli.world.VisualScene(id=num, display=input.TimedDisplay(ms=6000))

    height = np.array([0.0, 1.5, 0.0]) # Height at which stimulus is placed

    dot = ptvr.stimuli.world.Quad(side=0.1, color=color.RGBColor(r=0, g=0, b=0, a=1))
    dot_pos = np.array([3.0, 0.0, 0.0])  # This position is 3m in front, at height of 1.5m
    t1.place(dot, dot_pos + height, np.array([0.0, 0.0, 0.0]))  # Place the dot at dot_pos, without rotation
    exp.add_scene(t1)  # t1 is added to the experiment
    num += 1

    # Next, we want a scene that shows some stimuli around the dot
    t2 = deepcopy(t1)
    t2.id = num  # Increment its identifier
    spherical_array = np.array([[0, -30], [15, -15], [30, 0], [15, 15], [0, 30],
                                [-15, 15], [-30, 0], [-15, -15]])

    square = ptvr.stimuli.world.Quad(side=0.2, color=color.RGBColor(r=0, g=1, b=0, a=1))

    for elem in range(len(spherical_array)):
        # Get spherical coordinates
        spherical_coord = ptvr.stimuli.world.Spherical(r=3.0, polar=spherical_array[elem, 0],
                                                       elevation=spherical_array[elem, 1])
        cartesian = ptvr.stimuli.world.spherical2cartesian(spherical_coord)
        # Place square in the chosen location
        t2.place(square, cartesian + height, np.array([0.0, 0.0, 0.0]))
    # Add this scene to the experiment
    exp.add_scene(t2)
    num += 1

    t3 = deepcopy(t1)
    # Add another scene with just a plain dot to the experiment
    t3.id = num
    exp.add_scene(t3)
    num += 1

    # Now, a scene with stimulus on only one side
    t4 = deepcopy(t1)
    t4.id = num

    for elem in range(int(len(spherical_array)/2 + 1)):
        # Get spherical coordinates
        if spherical_array[elem, 0] >= 0:
            # Only left side
            spherical_coord = ptvr.stimuli.world.Spherical(r=3.0, polar=spherical_array[elem, 0],
                                                           elevation=spherical_array[elem, 1])
            # Convert to cartesian coordinates
            cartesian = ptvr.stimuli.world.spherical2cartesian(spherical_coord)
            # Place square in the chosen location
            t4.place(square, cartesian + height, np.array([0.0, 0.0, 0.0]))

    exp.add_scene(t4)
    num += 1

    # Add a response scene to ask the participant what he/she saw
    exp.add_scene(ptvr.stimuli.world.ResponseScene(id=num, text="Was the stimulus on the left "
                                                                "or right?\n[1]Left\n[2]Right"))
    num += 1

    # Write the experiment to file
    exp.write()
    print("The experiment has been written.")


if __name__ == "__main__":
        main()
