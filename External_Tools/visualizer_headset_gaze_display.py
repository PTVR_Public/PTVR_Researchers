# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 09:34:07 2022

@author: jdelacha
"""

import pandas as pd
import matplotlib.pyplot as plt
import math


# =============================================================================
# PARAMETERS
# =============================================================================


headset = False
left_eye= True
right_eye = True
# Fill with the full path to the result file. Don't forget the .csv at the end. \ must be changed to /.
# Target
target_data_path_file = 'gaze_cursor_pointing_with_recording__Main__Doe_2022-15-12--15-36-52.csv' 

target_columns = ["distance_exp"]
# Headset
headset_data_path_file = 'my_Path/MyFile.csv'

headset_columns = ["x_headset_forward_direction_projection_on_screen","y_headset_forward_direction_projection_on_screen"]
# Gaze
gaze_data_path_file = 'gaze_cursor_pointing_with_recording_on_text_tangent_screen111630448440454560_gaze_Doe_2023-11-1--14-24-23.csv'

left_eye_columns = ["x_left_eye_forward_direction_projection_on_screen","y_left_eye_forward_direction_projection_on_screen"]

right_eye_columns = ["x_right_eye_forward_direction_projection_on_screen","y_right_eye_forward_direction_projection_on_screen"]

# =============================================================================
# =============================================================================


def deg_to_rad(deg) :
    return deg * math.pi/180


def graph(show_headset, show_left_eye, show_right_eye) :
    
    fig, ax = plt.subplots()
    ## Axis setting
    # ax.set(xlim=(-1.2*rayon, 1.2*rayon), ylim = (-1.2*rayon, 1.2*rayon))
    ## Circle Creation 
    # a_circle = plt.Circle((0, 0), rayon,fill=False)
    # ax.add_artist(a_circle)
    
    ## Plot head data
    if(show_headset) :
        plt.plot(x_headset,y_headset,"o", color='orange')
    ## Plot Gaze data
   
    if(show_left_eye):
        plt.plot(left_x,left_y,"o", color='green') # left eye
    if(show_right_eye):
        plt.plot(right_x,right_y,"o", color='blue') # right eye
    ## Plot the target data   
    plt.plot(target_x,target_y,"o", color='red') 
    
    ## Show the entire figure
    plt.show()
    
    
 
   

target = pd.read_csv(target_data_path_file,
                     sep=',', 
                     decimal='.',
                     header=0,
                     usecols=target_columns) 


if (headset) : 
    headset_data = pd.read_csv(headset_data_path_file, 
                          sep=',', 
                          decimal='.',
                          header=0,
                          usecols=headset_columns)
if (left_eye) : 
    gaze_left_eye_data =   pd.read_csv(gaze_data_path_file, 
                          sep=';', 
                          decimal='.',
                          header=0,
                          usecols=left_eye_columns
                          )
    print(gaze_left_eye_data)
if (right_eye) : 
    gaze_right_eye_data = pd.read_csv(gaze_data_path_file, 
                          sep=';', 
                          decimal='.',
                          header=0,
                          usecols=right_eye_columns)
    


# création des listes
target_x = []
target_y = []
target_distance = target[target_columns[0]]
# target_eccentricity = target[target_columns[1]]
# half_meridian = target[target_columns[2]]
# max_eccentricity = max(target_eccentricity) 
# max_distance = max(target_distance)
# rayon = max_distance * math.tan( deg_to_rad(max_eccentricity) )

#conversion des positions de la cible en cartésien local
# for i in range (len(target_eccentricity)) :
#     target_x.append( round( target_distance * math.tan( deg_to_rad(target_eccentricity[i]) ) * math.cos( deg_to_rad(half_meridian[i]) ) ) )
#     target_y.append( round( target_distance * math.tan( deg_to_rad(target_eccentricity[i]) ) * math.sin( deg_to_rad(half_meridian[i]) ) ) ) 

if(headset) : 
    # création des listes de x/y pour la tête
    x_headset = headset_data[headset_columns[0]]
    y_headset = headset_data[headset_columns[1]]


# création des listes de x/y pour les yeux
if(left_eye):
    left_x = gaze_left_eye_data[left_eye_columns[0]]
    left_y = gaze_left_eye_data[left_eye_columns[1]]
if(right_eye):
    right_x = gaze_right_eye_data[right_eye_columns[0]]
    right_y = gaze_right_eye_data[right_eye_columns[1]]


graph(show_headset=headset, show_left_eye=left_eye, show_right_eye=right_eye)



