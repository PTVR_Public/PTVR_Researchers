"""
MathToolBox.py

usage: change the global parameters bellow then execute it with  
$ python ellipseCalculator.py

You can also open it with spyder and exectute it directly, without PYTHONPATH configuration.

This tool permits a calculation of the end points of the ellipse chord
passing througth its angular center.
It uses the cone equation instead of the vector method used in PTVR.
Its goal is to cross check the endpoints positions.
The parameter name follow the document (PTVR_Cone_and_Visual_Angles-3.pdf) 

"""


import numpy as np

#The system XYZ, useful for calculation (don't change it)
x=np.array([1,0,0])
y=np.array([0,1,0])
z=np.array([0,0,1])

#3d identity matrix  (don't change it)
#1 0 0
#0 1 0
#0 0 1
I = np.array( [ [ 1, 0, 0 ], [ 0, 1, 0 ], [ 0, 0, 1 ] ] )

#the vector pointing to the SO. Its norm is the distance
#for instance, if the vector is (0,0,5), the screen is on the z axis at 5 m distance
p = np.array([10,0,10])
print ("the vector pointing to the SO. Its norm is the distance")
print (p)
print ()

#the cone axis vector. It's direction is the direction of the cone axis.
#For easier use, it can be not normalized, never mind.
g = np.array([0,1,1])
print("the cone axis vector. It can be not normalized, never mind.")
print (g)
print ()

#the position of the cone apex in the space
O = np.array([0,0,0])
print ("the position of the cone apex")
print (O)
print ()

#the cone apperture angle (angle at apex in degrees)
gamma = 15
print ("the cone apperture angle (in deg)")
print (gamma)
print ("the calculus starts:\n==============================================>\n")

#Étape 1 of the document, calculation of screen x axis, called xPrime
def calculateXprime(p):
    xPrime = np.cross(y,p)
    xPrime = xPrime/np.linalg.norm(xPrime)
    return xPrime

#Étape 2 (3) of the document, calculation of the rotation R of xPrime
#This rotation gives the same orientation as the desired chord
def XprimeRotation(theta, p):
    p = p / np.linalg.norm(p)
    theta = np.pi/180*theta
    
    Q = np.array([[0,-p[2],p[1]],
                  [p[2],0,-p[0]],
                  [-p[1],p[0],0]])
    
    QQ = np.dot(Q,Q)

    R = I+np.sin(theta)*Q+(1-np.cos(theta))*QQ

    return R

#Étape 2 of the document, calculation of the rotated xPrime t
#t is parralel to the desired chord, its origin is SO.
def tCalculator(R, xPrime):
    return np.dot(R,xPrime) #Verifier doc numpy car avant écrit np.dot(xPrime,R) 

#Étape 3 of the document, calculation of the angular center of the ellipse
#K
def kCalculator(g,p):
    d = np.linalg.norm(p) # ||p||  =? ||OO'|| = d
    g = g / np.linalg.norm(g)
    p = p / np.linalg.norm(p)

    alpha = d / np.dot(g,p)

    return O + alpha*g

#Étape 4 of the document, calculation or the desired endpoints
#E1 and E2
def getE1E2(t,g,gamma,K):
    gamma = gamma/180*np.pi
    g = g / np.linalg.norm(g)
    t = t / np.linalg.norm(t)# verifier si utile

    a = np.dot(t,g)**2-np.cos(gamma/2)**2
    b = 2*np.dot(K-O, g)*np.dot(t,g)-2*np.dot(K-O, t)*np.cos(gamma/2)**2
    c = np.dot(K-O, g)**2-np.linalg.norm(K-O)**2*np.cos(gamma/2)**2

    beta1 = (-b-np.sqrt(b**2-4*a*c))
    E1 = K + beta1*t

    beta2 = (-b+np.sqrt(b**2-4*a*c))
    E2 = K + beta2*t

    return E1,E2


#the main go througth each steps, giving the analytic result of each "étape".
def main():
    xPrime=calculateXprime(p)
    print ("xPrime, the screen local x axis:")
    print (xPrime)
    print ()
    R = XprimeRotation(90,p) 
    print ("rotation matrix of xprime to get t:")
    print (R)
    print ()
    t=tCalculator(R,xPrime)
    print ("the parallel to the cord vector at SO t:")
    print (t)
    print ()
    K = kCalculator(g,p)
    print ("the pseudo center of the ellipse K:")
    print (K)
    print ()
    E1,E2 = getE1E2(t,g,gamma,K)
    print ("the two endpoints of the cord E1,E2:")
    print (E1,E2)

    
main ()


           
