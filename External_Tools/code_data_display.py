# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 13:33:40 2022

@author: jdelacha
"""


import pandas as pd
import matplotlib.pyplot as plt


# =============================================================================
# PARAMETERS
# =============================================================================


## Fill with the full path to the result file. Don't forget the .csv at the end. \ must be changed to /.

data_path_file = '' 

## Add 2 columns name 
## the first column is used to define the X coordinates of the displayed point
## the second column is used to define the Y coordinates of the displayed point
columns_to_use = ["",""]

# =============================================================================
# =============================================================================



data = pd.read_csv(data_path_file,
                     sep=',', 
                     decimal='.',
                     header=0,
                     usecols=columns_to_use) 


x_data_point = data[columns_to_use[0]]
y_data_point = data[columns_to_use[1]]

fig, ax = plt.subplots()

## Maximum range of axis
#ax.set(xlim=20, ylim = 20)


# "o" : only points
# "-o" : points linked with a line 
# "--o" : points linked with a dashed line
plt.plot(x_data_point,y_data_point,"o", color='red') 

## Show the entire figure
plt.show()


