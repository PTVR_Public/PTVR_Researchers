Getting Started with PTVR {#gettingStartedwithPTVR}
============

List of sections
-------------------
<img src="Getting-started-icon.png" width="225" height="205" style="float:right; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->



- @subpage setupHMD  
- @subpage ptvrInstallation   
- @subpage calibrationsOfHMD  
- @subpage displayHelloWorld 

<!-- on a décidé de pas les garder pour l'instant
- @subpage userProfile  
 
- @subpage interfaceOfPTVR  
- @subpage ResultFile  
-->



Requirements at a glance
---------------------------
- **HTC Vive** VR device with hand controllers [(help here)](@ref setupHMD).  
Note that we initially chose this headset as it had the **highest display refresh rate** (90 Hz).  
	
- A **Windows** **computer** with HTC Vive already set-up [(help here)](@ref setupHMD).  
Note that the necessity to have a Windows PC is a constraint due to the [HTV Vive Headset's requirements](https://www.vive.com/us/support/vive-pro-eye/category_howto/what-are-the-system-requirements.html) and not to Unity [(see section Unity Player system requirements -> Desktop)](https://docs.unity3d.com/Manual/system-requirements.html).  
	
- **PTVR** installed [(help here)](@ref ptvrInstallation).
- **Python 3.6**.  we recomment the Spyder IDE. [(help here : see 'Install Python' section)](@ref ptvrInstallation).




<BR>
**Attributions of images**  
Image from <a href="https://commons.wikimedia.org/wiki/File:Getting-started-icon.png">junopayments</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons.