Calibrations {#calibrationsOfHMD}
============

**Room calibration**

- [See section in User manual.](@ref calibrationOfHeadset)
- [and see Vive support.](https://www.vive.com/us/support/vive-pro-eye/category_howto/planning-your-play-area.html)





**Eyetracker calibration**

- [See Vive support.](https://www.vive.com/us/support/vive-pro-eye/category_howto/calibrating-eye-tracking.html)   

<BR>
