Display "Hello World" {#displayHelloWorld}
============

If you have followed the previous "Getting Started" subsections,  
and this includes having carefully read the **IMPORTANT warning** in the [Install Python subsection](@ref ptvrInstallation) about setting the **Python Environment Variable PYTHONPATH**,  
then you should be able to display "Hello World" in your VR headset with the following script **10_hello_world.py** :



    """
    ...\Demos\Text\hello_world.py
    """
    
    import PTVR.SystemUtils
    import numpy as np
    import PTVR.Visual
    import PTVR.Stimuli.Scenes
    import PTVR.Stimuli.Objects
    
    def main():
    	my_world = PTVR.Visual.The3DWorld()
    
    	my_scene  = PTVR.Stimuli.Scenes.VisualScene ()
    	text = PTVR.Stimuli.Objects.Text (	text = 'Hello World !', 
    										position_in_current_CS = np.array ( [ 0, 1, 1 ] ),
    										fontsize_in_postscript_points = 60
    									)
    my_scene.place (text, my_world)
    my_world.add_scene (my_scene)
     
    my_world .write () # writes to .json file
    print (".json file has been created.")
    
    if __name__ == "__main__":
    	main()
    	PTVR.SystemUtils.LaunchThe3DWorld("Jane Doe")



<div id="note">\emoji :wink: **Remark:** For more detailed explanations on the script 10_hello_world.py, have a look at [this section](@ref introOfAnExperiment). </div>



**Demos**
Path of demos:
...\PTVR_Researchers\Python_Scripts\Demos\Text\
<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 10_hello_world.py 
<td> this demo contains the code displayed above.


<BR>
</table>
