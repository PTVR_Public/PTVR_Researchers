User profile {#userProfile}
============
<b>Choose the part of PTVR you Need </b> <BR>
First step is fork the project at gitlab.inria.
Look at the picture bellow to know which part of project you Need:

<p align="center">
  <img src="What_Folder_I_Need.jpg">
</p>

Depending of your choice you can use only a part of the project.
Indeed the master of the project Perception Toolbox with Virtual Reality contains PTVR_Researchers, who contains PTVR_Examinator.

<p align="center">
  <img src="ListFolder.jpg">
</p>

<b>Whats is goal of each folder?</b> <BR>
- The folder PTVR_Examinator contain the build Unity (you don't need to have a version of Unity install if you have use the .exe).
- The folder PTVR_Researchers contain the python code and the folder PTVR_Examinator.
- The master of the gitlab contain all folder and code Unity (To learn read /ref dev_manual ). 

To create our experiments we used the <a href="https://www.vive.com/fr/product/vive-pro-eye/overview/">HTC Vive pro eyes</a>. <BR>
We also need to install [SteamVR](https://store.steampowered.com/app/250820/SteamVR/) and the [Sranipal](https://developer.vive.com/resources/vive-sense/sdk/vive-eye-and-facial-tracking-sdk/).
(if you want eyestracking data).
