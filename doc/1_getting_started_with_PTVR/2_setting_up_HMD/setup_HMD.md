VR device Setup  {#setupHMD}
============

<img src="640px-Gamedesignerin_Philomena_Schwab.jpg" width="320" height="213" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

<span style="font-size:170%;">
<strong> Before putting your headset on, here are a few steps to follow... </strong>
</span>


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->
(image from <a href="https://commons.wikimedia.org/wiki/File:Gamedesignerin_Philomena_Schwab.jpg">Pascal Wiederkehr</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons).



<BR>
***************************
Minimal requirements
----------------------

- HMD: PTVR has been tested with an HTC VIVE Pro HMD (no eyetracking) and an HTC VIVE Pro **Eye** (with eyetracking).  
    [See VIVE Pro Series](https://www.vive.com/us/product/#pro%20series).
- PC :  [see System requirements](https://www.vive.com/us/support/vive-pro-eye/category_howto/what-are-the-system-requirements.html)  
   Note: a PC with the label **VR READY** is recommended for the longevity of the PC updates.

<BR>
*************************
Setup of headset, hand controllers, base stations,  etc...
----------------------
Literally **everything** you need is very clearly explained
<span style="font-size:150%;">
[here in the VIVE Pro Eye support](https://www.vive.com/us/support/vive-pro-eye/) or [here to get their user guide in pdf format](https://dl4.htc.com/Web_materials/Manual/Vive_Pro_Eye/VIVE_Pro_Eye_user_guide.pdf).    
</span>  

You will thus learn quickly how to setup the headset (figure 1), the link box (figure 2), the base stations (figure 3) and the (hand) controllers (figure 4) with figures such as those below (images from <a href="https://www.vive.com/us/support/vive-pro-eye/category_howto/headset-and-link-box.html"> Vive site</a>).

<!-- Figures 1, 2, 3 et 4  -->
<table>
<tr>
<td style="width:25%;" align="center">
<img src="headset_htc_vive.png" width="250" height="135"/>
</td>
<td style="width:25%;" align="center">
<img src="link_box.png" width="250pix"/>  
</td>
<td style="width:25%;" align="center">
<img src="base_stations.png" width="250pix"/>  
</td>
<td style="width:25%;" align="center">
<img src="hand_controller_vive.png" width="229" height="179"/>  
</td>
</tr>

<tr>
<td>Figure 1: Headset </td>
<td>Figure 2: Link box </td>
<td>Figure 3: Base stations </td>
<td>Figure 4: Controllers </td>
</tr>
</table>
<BR>




<BR>
<div id="note">\emoji :sunglasses: **Tip**: with a laptop PC, it is likely that you will need a mini display port (miniDP) to mini display port (miniDP) cable between the link box and your PC (as most laptops only have a miniDP). 
 </div>  
<BR>

<hr>
<span style="font-size:170%;">
Software Installation
</span>


1/ Steam and SteamVR Installation
----------------------
If you do not need to measure ocular behaviour (gaze position, pupil size, ...), you only have to install  [**Steam** and **SteamVR**](https://www.steamvr.com/en/) to control the Head-Mounted Display - HMD. Once these softwares are installed, you can test your Head-Mounted Display (even if PTVR is not yet installed)..  

<img src="logo_steam.svg" width="300" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->
If  Steam is not  installed yet on your PC, [download it here](https://store.steampowered.com/about/), and install it.  
If you do not have a Steam account yet, create one to be able to install Steam.

Open Steam software **within which** you can directly look for [SteamVR software](https://www.steamvr.com/en/) and install it.

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

**Run SteamVR** : this should open the following window (it might be hidden in the Windows task-bar):

<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="mini_window_steamVR.png" width="250pix"/>
</td>
<td style="width:50%;" align="center">
<img src=""  width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 1:.

 </td>
<td>Figure 2: . </td>
</tr>
</table>
<BR>
<!-- FIN DE Figures 1 et 2  -->


<div id="note">\emoji :sunglasses: **Tip**: This MINI Window is very important!!! It will allow you to control many things. </div>

Note the icons in the bottom bar (from left to right: headset, controller #1, controller #2, base station #1,  base station #2) : when colored, this means that they are detected and active.

Most importantly, click on **the 3 bars in the top-left corner**, and you'll see all the important tasks that you can accomplish, such as the Room Calibration process
[see section Calibrations](@ref calibrationsOfHMD).  
<BR>
<div id="note">\emoji :sunglasses: **Tip**: The SteamVR program will be launched automatically when you run a PTVR script. </div>

<BR>
2/ Eyetracker Installation (if ocular measurements are needed)
----------------------

This section has been **updated on 20. October 2023 !**

There are two recommended options to install the eye tracking software:
 

- [1/ Install the Vive Console for SteamVR from the Steam website](https://store.steampowered.com/app/1635730/VIVE_Console_for_SteamVR/)   
This will automatically install the eyetracking software.   
You can check that the console is correctly installed by checking the "Vive Software" line in the Windows Start menu: you should then see what is shown in the figure below.  
Until further notice, PTVR uses this option to enable eyetracking.

<img src="Vive_console_in_start_menu.png" width="400" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->
Note the line with the robot's head which indicates that the eyetracking software (Vive Sranipal) is installed.  
Clicking on this line will launch the eyetracking software.  

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->
<BR>



- [2/ the second option - not currently used in PTVR - is to use OpenXR](https://developer.vive.com/resources/openxr/)   
  OpenXR will enables XR portability across hardware VR devices



The two options presented above are summarized with important additional information in
[this Vive web page](https://developer.vive.com/eu/support/sdk/category_howto/where-to-download-eye-tracking-runtime-and-sdk.html).  
 This important page is reproduced below (retrieved on 20. october 2023).

<img src="OpenXR_is_now_recommended.png" width="800" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->
<BR>


The important point to note in the figure above is that the OpenXR option is now strongly recommended so that future PTVR developments will aim at replacing the current Sranipal eyetracking software by the OpenXR approach. 
  
<BR>

<div id="note">\emoji :scream: **Warning:** After installing these softwares, do not forget to calibrate the Room and the eye-tracker within the SteamVR environment [(see details here)](@ref calibrationsOfHMD).  It is also possible to launch the eyetracking calibration from a PTVR python script. </div>  
<BR>


<div id="note">\emoji :sunglasses: **Tip**: if the eyetracking software is running, you should see its icon in the **system tray** as a **robot's head**. </div>
*The **System Tray** is another name given to the **Notification Area**, which you can find in the **right-side of the Windows Taskbar**. The System Tray features different types of notifications and alerts from your computer like your Internet connection, or the volume level.

<BR>
&nbsp; &nbsp; &nbsp; &nbsp;     Eyetracking Troubleshooting (for installation and use)
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; [See Vive developer's page](https://developer.vive.com/us/support/sdk/category_howto/vive-eye-tracking.html)


<BR>
General error codes and messages from Vive Support
-------------------------------------------------

[See here what you can do depending on error codes](https://www.vive.com/us/support/cosmos/category_howto/what-do-these-error-codes-and-messages-mean.html).
<BR>
<BR>