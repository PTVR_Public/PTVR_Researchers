PTVR and Python installation {#ptvrInstallation}
============

<img src="icon_ptvr_plus_logo.png" width="150" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

Install PTVR manually
========================

1st solution: Clone from the command line with HTTPS
-----------------------------------------

- Step 0. [Install GIT for Windows](https://git-scm.com/downloads)  
  (or install a GIT Graphical User Interface (GUI) such as [SourceTree](https://www.sourcetreeapp.com/), but note that we do not provide examples for GIT GUIs in the Documentation).

- Step 1. Go to the following web address : [https://gitlab.inria.fr/PTVR_Public/PTVR_Researchers/](https://gitlab.inria.fr/PTVR_Public/PTVR_Researchers/)  
For those familiar with the git terminology ([see gitlab's documentation for instance](https://docs.gitlab.com/)), this is the PTVR project's repository.  
The top part of the main block in this web page should look like the screenshot below (displayed in dark mode).  
Click on the button **Clone** (bottom right corner of the screenshot) and Copy the URL (web address) for "Clone with HTTPS".    
This copied URL "https://gitlab.inria.fr/PTVR_Public/PTVR_Researchers.git" will be used in Step 3. 

<img src="top_part_of_Public_PTVR_Researchers_Gitlab_page.png" width="810" height="193" />  


<BR>

- Step 2. Open a terminal and go to the directory where you want to clone/install the PTVR program on your PC. Let's call the path to this directory ***my_path_for_PTVR_program***.  
In the example to be presented below, ***my_path_for_PTVR_program*** is :
**C:\Users\castet\Documents\PTVR\** 


- Step 3. Run the following command where the part after "clone" is what you copied in Step 1.  
```
git clone https://gitlab.inria.fr/PTVR_Public/PTVR_Researchers.git  
```    
A folder with the name **PTVR_Researchers** is automatically created on your PC in ***my_path_for_PTVR_program*** and all the directories and files of the PTVR program are downloaded in this **PTVR_Researchers** directory.  

- Step 4. To view where the files were cloned (as a quick check that everything went smoothly), go to the **PTVR_Researchers** directory that will be in ***my_path_for_PTVR_program***.  
**If you do NOT see the PTVR_Researchers** directory, something went wrong in the preceding steps.

That's all !

Example
--------------

As an illustration, the image below shows a Windows Explorer window showing the files and directories present in the **PTVR_Researchers** directory after cloning. In this example, **my_path_for_PTVR_program** (highlighted in green) where you chose to clone PTVR is: **C:\Users\castet\Documents\PTVR\**   
Note again that this path is totally chosen by the user whereas the directory PTVR_Researchers was automatically created when you cloned the PTVR project.


<img src="the_path_to_your_directory_in_windows_explorer.png" width="710" />  

<div id="note">\emoji :scream: **Warning:** If your Python IDE is already installed, do not forget to set its **Python Environment Variable PYTHONPATH** ! (see section 'Install Python' below) </div>  

**Note**: in the example above, the **PYTHONPATH** that the user has to define in the Pythonpath Manager is :
`C:\Users\castet\Documents\PTVR\PTVR_Researchers\ `     
(see snapshot  from Spyder below)  

<img src="pythonpath_manager.png" width="350" />  

<BR>
(see more details on Git cloning in [https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-with-https](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-with-https) )
<BR>
<BR>


2nd solution : Download the source code from a zip file in the gitlab RELEASE.
-----------------------------------------------
If you insist on not using Git cloning and pulling (which is the most convenient way to maintain PTVR when new versions are added), you can also try the following solution.  
Go to the first PTVR_Researchers git page :  
[https://gitlab.inria.fr/PTVR_Public/PTVR_Researchers/](https://gitlab.inria.fr/PTVR_Public/PTVR_Researchers/) 
You'll see a **Release** button next to the takeoff of a rocket.   

<img src="release_on_first_PTVR_researchers_page.png" width="810" height="100" />  

Click on this button, and you'll be able to download the full PTVR_Researchers code in a compressed format. Once downloaded, and unzipped, you have to place it in a directory called **PTVR_Researchers** (do NOT use another name).  
**The name 'PTVR_Researchers' is mandatory!**.   
<BR>
Once the unzipped code is in the **PTVR_Researchers** directory, you can place the latter in any path on your PC. For instance, as already examplified in the '1st solution' section above, this ***my_path_for_PTVR_program*** path might be for instance:   
**C:\Users\castet\Documents\PTVR\**. 

Let's insist, errors will occur when you run a PTVR script if the unzipped code is not contained in a directory called **PTVR_Researchers** (see the example figure in the '1st solution' section above).  


Everytime a new PTVR release will be available, you will have to replace the previous installation with the most recent one using this second solution. Alternatively, you can keep the previous installation and add the new version in a different path on your PC (in this case, do not forget to add the new **PYTHONPATH**  in the Pythonpath Manager (see above in the 'first solution' section).

<BR>

Install PTVR with a standalone package
===========================================
Be patient ! In the future, it will be possible to install PTVR with a standalone package. 

----------

<BR>
Finally : Install Python
===========================================

Once PTVR is installed, you will need a **Python** IDE (Integrated Development Environment) to run and create PTVR scripts.  
<img src="spyder.jpg" width="100" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

<BR>
We strongly recommend the open-source IDE [**Spyder**](https://www.spyder-ide.org/).  VS code or PyCharm are also nice alternatives.

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->


<BR>
<div id="note">\emoji :scream: **Warning:** Once your Python IDE is installed, do not forget to set its **Python Environment Variable PYTHONPATH** !! </div>  

Otherwise, you will get an error message from your IDE.

Here is the procedure to follow with [**Spyder**](https://www.spyder-ide.org/) to set the Python Environment Variable PYTHONPATH: 

Go to Tools -> 'PYTHONPATH Manager', and add the following path:  
`my_path_for_PTVR_program\PTVR_Researchers `   
(For the definition of the ***my_path_for_PTVR_program***, see the subsection "Install PTVR Manually" above )
 (see snapshot  from Spyder below)  
<BR>
<img src="pythonpath_manager.png" width="350" />  


Otherwise, you will get the following error message in the console of your IDE   : `ModuleNotFoundError : No Module named 'PTVR`

You may have to restart Spyder after this procedure.

