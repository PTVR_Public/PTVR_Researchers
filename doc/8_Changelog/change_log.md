Changelog {#changelog}
======================

# Semantic Versioning
PTVR strives to use [Semantic Versioning](https://semver.org/) for its releases: (Major).(Minor).(Patch).

Given a version number, MAJOR.MINOR.PATCH increments the:  

1. MAJOR version when you make incompatible API changes  
1. MINOR version when you add functionality in a backwards compatible manner  
1. PATCH version when you make backwards compatible bug fixes

# PTVR 1.2.2 (26 February 2025)
- Improved the Sprite object to display several hundred of them without affecting the program performance

# PTVR 1.2.1 (21 February 2025)

- New Coordinate System (CS) : Azimuth and Elevation   
    See Demo in ...\PTVR_Researchers\Python_Scripts\Demos\Positions\    
    3.0_create_object_in_azimuth_elevation_coord.py       
	or Demo in ...\PTVR_Researchers\Python_Scripts\Pedagogical_tools\Maths\         
   azimuth_elevation_CS.py    
	

- set_spherical_coordinates () is deprecated: use set_perimetric_coordinates() instead.   
	see [cheatsheet on Objects' position](@ref specifyPositionCheatSheet)
- set_spherical_coordinates_on_screen () is deprecated: use set_perimetric_coordinates_on_screen() instead.   
	see [cheatsheet on Objects' position](@ref specifyPositionCheatSheet)


# PTVR 1.2.0 ( 07 February 2025)

- a new directory has been created: ...\PTVR_Researchers\Python_Scripts\Pedagogical_tools\     
	See for ex.    
   rotations_in_global_and_local_cartesian_CS.py   

- It is now possible to record in a file the position and orientation of the **headset** with the same logic as for the handcontroller. Notably, the recorded files allow the user to get coordinates of the collision between the headset (or handcontroller) direction and a Tangent Screen (TS).   
	See Demos in ...\PTVR_Researchers\Python_Scripts\Demos\Tracking_of_hand_head_or_gaze\   
	See Documentation section [Recording handcontroller, headset, etc.](@ref recordingHandcontrollerEtc)    

- New script illustrating a recently published "augmented vision" experiment  
    See Demos in ...\PTVR_Researchers\Python_Scripts\Experiments\Augmented_Vision\  
    2025_IEEEVR_paper.py   
	    https://hal.science/hal-04918641

- small bug corrected in visual_search.py

# PTVR 1.1.4 (13 December 2024)

- The structure and the names of the  free asset bundles stored in https://files.inria.fr/ptvr have been tidied up. 

# PTVR 1.1.3 (10 December 2024)

- It is now possible to record in a file the position and orientation of the handcontroller.  
	See Documentation section [Recording handcontroller, headset, etc.](@ref recordingHandcontrollerEtc)

- About creating objects imported from asset bundles using CustomObject().

	In PTVR releases prior to 1.1.3, the asset bundles that were necessary to create CustomObjects could be stored on any directory of your PC (with the paths of these directories defined in your scripts).	
	From this version on, to simplify the handling of paths, you will have to store all your asset bundles in a directory called:
	"/Asset_bundles". 
	In addition, this directory will have to be at the same level as the "\PTVR_Researchers" directory.
	
	For instance, if you installed PTVR such that it was in the following directory:
	C:\Nancy\PTVR_Researchers
	then the directory containing ALL your asset bundles will now have to be in:
	C:\Nancy\Asset_bundles.
	
	Another change is that the free asset bundles stored in https://files.inria.fr/ptvr are now organized in directories to help you retrieve easily the asset bundles you need (e.g. for a certain demo). As this directory structure is not the same as in previous PTVR versions, the current release contains a link to a zip file (Asset_bundles_for_PTVR_releases_prior_to_1_1_3.zip) containing all the asset bundles that you need if you run scripts developed with PTVR releases prior to 1.1.3. Note that you will always have access to this zip file in https://files.inria.fr/ptvr.	

	Another related change concerns the new 'asset_bundle_name' parameter when you import an asset bundle with CustomObject(). See the example code below:   

		my_character = CustomObject(
		asset_bundle_name = "humans_cartoons",
		asset_to_load = "Assets/Prefabs/Boy.prefab"
		)


# PTVR 1.1.2 (28 November 2024)

Unity Version : 2020.3.34f1

- New important parameter in CustomObject()   
You can now type 'display_animations = True' to see in the headset above the CustomObject (Asset bundle) created what are the available animations associated with this object.  
Illustrated in these 2 demos:
...\PTVR_Researchers\Python_Scripts\Demos\Assets_to_import_into_PTVR\      
   20_animate_rabbit_asset.py and  
30_boy_asset_w_walk_and_run_animation.py  

- RENAMING  
(you'll have to replace these names if you used them previously in some of your scripts)
'Sprite' has been replaced with 'Image' in the 5 following objects:    
ReticleSpriteFromDrawing -> ReticleImageFromDrawing   
ScotomaSpriteFromDrawing -> ScotomaImageFromDrawing   
SpriteFromDrawing -> ImageFromDrawing   
SpriteFromLoading -> ImageFromLoading  
SpriteToContingentCursor ->  ImageToContingentCursor   
The reason is that the previous names containing 'Sprite' gave the wrong impression that these objects were creating or using PTVR [SPRITE objects](@ref spriteObjects). 


# PTVR 1.1.1 (05 November 2024)

Unity Version : 2020.3.34f1

## Improvements

- Minor hotfixes and documentation changes.
- RENAMING
(you'll have to replace these names if you used them previously in some of your scripts)
Some objects related to movements (e.g. Translate, Rotate, ...) that were defined in :
	"...\PTVR_Researchers\PTVR\Data\Callback.py" 
are now defined in:
	"...\PTVR_Researchers\PTVR\Data\Callbacks\MovementCallback.py"
Thus the following line of code will induce an error if you use the Translate() callback:
	import PTVR.Data.Callback
so that you should now write:
	import PTVR.Data.Callbacks.MovementCallback



# PTVR 1.1.0 (25 October 2024)

Unity Version : 2020.3.34f1

## Improvements

- New demos in ...\PTVR_Researchers\Python_Scripts\Demos\Assets_to_import_into_PTVR.
They illustrate some new features when importing and animating Assets (characters, animals, objects, landscape, cities, vehicles, ...).

- New demos in ...\PTVR_Researchers\Python_Scripts\Demos\Global_Variables
They illustrate some of the many uses of the"global variable", a very important new feature which potentiates the possibilities offered by callbacks.

# PTVR 1.0.2 (04 July 2024)

Unity Version : 2020.3.34f1

## Improvements
- Improved Documentation (see User Manual -> Pointing at an object) and Docstrings for flat Cursors (reticles, scotomas, ...) and Pointing events.
- Improved documentation for Sprites (see User Manual -> Visual Objects -> Sprites or Flat 2D objects.


# PTVR 1.0.1 (23 April 2024)

Unity Version : 2020.3.34f1

## Improvements
- Big Improvements of the Flat Cursor Objects aka contingent Sprites. These objects allow us notably to create contingent artificial scotomas and reticles (crosshairs). Highly detailed documentation is also now provided. This release corresponds to the publication (end of April) of the PTVR paper "PTVR (Perception Toolbox for Virtual Reality) a software in Python to make virtual reality experiments easier to build and more reproducible." in a special Journal Of Vision issue on Virtual Reality.

# PTVR 1.0.0 (23 February 2024)

Unity Version : 2020.3.34f1

## Improvements
- Improved demos with CustomObject.


# PTVR 0.5.0 (21 February 2024)
Unity Version : 2020.3.34f1


## Improvements
- Improved CustomObject class to load assets from an AssetBundle.


# PTVR 0.11.6 (07 February 2024)

Unity Version : 2020.3.34f1

## Improvements
- Added CustomObject class to load assets from an AssetBundle.
- Added several demos to use CustomObject.


# PTVR 0.11.5 (23 October 2023)

Unity Version : 2020.3.34f1

## Improvements
- Eyetracking bug fixed: gaze_cursor_pointing_with_recording.py works again
- Updated documentation on eyetracking
- MNRead test bug fixed : mnread_vr_experiment.py works again


# PTVR 0.11.4 (20 September 2023)

Unity Version : 2020.3.34f1

## Improvements
- Improved demos
- Added Results folder to PTVR_Operators

# PTVR 0.11.3 (14 September 2023)

Unity Version : 2020.3.34f1

## Improvements
- Renamed rotate_about_local_... into rotate_about_object...

## Features
- Added callback_timestamp_ms to results file
- Added application_version to results file


# PTVR 0.11.2 (25 July 2023)

Unity Version : 2020.3.34f1

## Improvements
- Deleted outdated demos
- Improved documentation


# PTVR 0.11.1 (24 July 2023)

Unity Version : 2020.3.34f1
## Features
- Add scene_start_timestamp to results file
- Add scene_end_timestamp to results file


# PTVR 0.11.0 (04 July 2023)

Unity Version : 2020.3.34f1
## Features
- Add button for open json

## Improvements
- Change Input to Events and Events to Callbacks


# PTVR 0.10.2 and PTVR 0.10.3 (16 December 2022)

Unity Version : 2020.3.34f1

## Features
- New Callbacks CalibrationEyesTracking

## Improvements
- Documentation upgrade Doxygen Awesome
- Correct Reset
- Correct Eyetracking Demos

## Known Issues
- Documentation table broken

# PTVR 0.10.4 (22 Mars 2023)

Unity Version : 2020.3.34f1
## Features
- Possibility to set 3DWorld in Monocular
- New Callback ChangeObjectScale
- New Callback ChangeHapticSettings
- HandController can manage the parenting

## Improvements
- PointedAt is now able to manage HandController and EyesTracking
- PointingCursor is now able to manage hands_contingent
- Rename Relative to Local

# PTVR 0.10.2 and PTVR 0.10.3 (16 December 2022)

Unity Version : 2020.3.34f1

## Features
- New Callbacks CalibrationEyesTracking

## Improvements
- Documentation upgrade Doxygen Awesome
- Correct Reset
- Correct Eyetracking Demos

## Known Issues
- Documentation table broken

# PTVR 0.10.1 (12 December 2022)

Unity Version : 2020.3.34f1

## Hotfix 
Documentation

# PTVR 0.10.0 (09 December 2022)

Unity Version : 2020.3.34f1

## Features
### Creation of new ids for some objects (experimental)
- ```my_world.idHandControllerLeft``` -> -1
- ```my_world.idHandControllerRight``` ->-2
- ```my_world.idHeadset``` -> -3
- ```my_world.idOriginCurrentCS``` -> -4
- ```my_world.idGlobalCS``` -> -5

### Get objects characteristics (positions , orientation)

- ```my_object.get_current_CS_cartesian_coordinates()``` not used anymore 
- ```my_object.get_position_in_global_CS()``` (not working if rotate_to_look_at or rotate_to_look_at_in_opposite_direction is used)
- ```my_object.get_axis_in_global_CS()``` 
- ```my_object.get_axis_tangent_screen()```
- ```my_object.get_position_on_screen_in_cartesian_coordinates_from_perimetric_coordinates(eccentricity_deg, half_meridian_deg, distance_ahead_from_screen)```
- ```my_object.reset_orientation()```
- ```my_object.reset_position()```
- ```my_object.reset()```

### New rotation function
- ```my_object.rotate_to_look_at(id_object_to_lookAt)```
- ```my_object.rotate_to_look_at_in_opposite_direction(id_object_to_lookAt_in_opposite_direction)```


## Improvements

### New functions in PTVR.Tools.py
- ```tools.norm(u)```
- ```tools.scalar_product(u,v)```
- ```tools.normalize(u)```
- ```tools.vector_product(u,v)```
- ```tools.intersection_between_line_and_tangent_screen(origin, line_vector_eccentricity_deg, line_vector_half_meridian_deg,tangent_screen_origin)```
- ```tools.capoc_ray(origin, line_vector_eccentricity_deg, line_vector_half_meridian_deg)```
- ```tools.rotate_about_vector(u_vec,v_vec,theta_deg)```
- ```tools.translate_along_axis(u,axis_cs_x,axis_cs_y,axis_cs_z,epsilon)```
- ```tools.deg_to_rad(deg)```
- ```tools.rad_to_deg(rad)```
- ```tools.points_to_vec(point_a,point_b)```
- ```tools.distance_between_two_points(point_a,point_b)```
- ```tools.angle_between_two_vectors_deg(a_vec,b_vec)```
- ```tools.angle_between_two_vectors_rad(a_vec,b_vec)```
- ```tools.global_to_relative_cartesian_coordinates(point_in_global_cs,relative_cs_origin,relative_cs_axis_X,relative_cs_axis_Y,relative_cs_axis_Z)```
- ```tools.relative_to_global_cartesian_coordinates(point_in_relative_cs,relative_cs_origin,relative_cs_axis_X,relative_cs_axis_Y,relative_cs_axis_Z)```
- ```tools.segment_orientation_to_segment_projected_orientation_deg(orientation_deg, tangent_screen_X_axis,tangent_screen_Z_axis,  capoc_eccentricity_deg, capoc_half_meridian_deg)```
- ```tools.segment_projected_orientation_to_segment_orientation_deg(projected_orientation_deg, tangent_screen_X_axis,tangent_screen_Z_axis,  capoc_eccentricity_deg, capoc_half_meridian_deg)```
- ```tools.segment_orientation_from_local_cartesian_coordinates_deg(point_a,point_b)```
- ```tools.set_spherical_to_cartesian(eccentricity_deg, half_meridian_deg , radial_distance)```
- ```tools.set_spherical_to_cartesian_vec(eccentricity_deg, half_meridian_deg , radial_distance)```

### Update in PTVR.LogManager.py
- ```log.SetWarning(information)```
- ```log.SetError(information)```

## Changes

### Current_CS

**CS translations**
- ```my_world.translate_coordinate_system_along_current(translation)```
- ```my_world.translate_coordinate_system_along_global(translation)```

**CS rotation**
- ```my_world.rotate_coordinate_system_about_current_x(rotation_deg)```
- ```my_world.rotate_coordinate_system_about_current_y(rotation_deg)```
- ```my_world.rotate_coordinate_system_about_current_z(rotation_deg)```
- ```my_world.rotate_coordinate_system_about_global_x(rotation_deg)```
- ```my_world.rotate_coordinate_system_about_global_y(rotation_deg)```
- ```my_world.rotate_coordinate_system_about_global_z(rotation_deg)```

**Reset**
- ```my_world.reset_coordinate_system()```

### Objects

**Objects translation (new functions)**
- ```my_object.set_cartesian_coordinates(x=0.0,y=0.0,z=0.0, system="leftHand_Zforward")```
- ```my_object.set_local_cartesian_coordinates(x=0.0,y=0.0,z=0.0, system="leftHand_Zforward")```
- ```my_object.set_spherical_coordinates( coordinate_system, radialDistance, eccentricity, halfMeridian, elevation, azimuth)``` 
- ```my_object.set_local_spherical_coordinates( coordinate_system, radialDistance, eccentricity, halfMeridian, elevation , azimuth)```
- ```my_object.set_perimetric_coordinates( radialDistance, eccentricity, halfMeridian)```
- ```my_object.set_local_perimetric_coordinates(coordinate_system, radialDistance , eccentricity, halfMeridian , elevation = OPTIONAL_VALUE, azimuth = OPTIONAL_VALUE)```
- ```my_object.translate_along_current_vector(translation = np.array([0.0,0.0,0.0]))```
- ```my_object.translate_along_current_xyz(x,y,z)```
- ```my_object.translate_along_global_vector(translation = np.array([0.0,0.0,0.0]))```
- ```my_object.translate_along_global_xyz(x,y,z)```
- ```my_object.translate_along_local_vector(translation = np.array([0.0,0.0,0.0]))```
- ```my_object.translate_along_local_xyz(x,y,z)```

**Objects rotation**
- ```my_object.rotate_about_global_x(angle_deg)```
- ```my_object.rotate_about_global_y(angle_deg)```
- ```my_object.rotate_about_global_z(angle_deg)```
- ```my_object.rotate_about_current_x(angle_deg)```
- ```my_object.rotate_about_current_y(angle_deg)```
- ```my_object.rotate_about_current_z(angle_deg)```
- ```my_object.rotate_about_local_x(angle_deg)```
- ```my_object.rotate_about_local_y(angle_deg)```
- ```my_object.rotate_about_local_z(angle_deg)```
- ```my_object.rotate_to_look_at(id_object_to_lookAt)```
- ```my_object.rotate_to_look_at_in_opposite_direction(id_object_to_lookAt_in_opposite_direction)```

**Reset**
- ```my_object.reset_position()```
- ```my_object.reset_orientation()```
- ```my_object.reset()```

### Parenting
**New Functions:**
- ```my_object..set_parent(parent,x_local, y_local, z_local) ```
- ```my_object.set_spherical_coordinates_on_screen(my_tangentscreen, eccentricity_local_deg, half_meridian_local_deg, distance_ahead_from_screen ,coordinate_system="perimetric")```
- ```my_object.set_perimetric_coordinates_on_screen(my_tangentscreen, eccentricity_local_deg, half_meridian_local_deg, distance_ahead_from_screen)```
- ```my_object.set_cartesian_coordinates_on_screen(my_2D_screen, x_local, y_local, z_local)```

### Other
- Use ```my_scene.place(my_object,my_world)``` instead of my_scene.place(my_object)
- Correction of ```Linetool``` (now takes into account current_cs and parenting)
- ```position_in_current_CS``` after ```init``` not taken into account anymore



# PTVR 0.9.2 (4 July 2022)

Unity Version : 2020.3.34f1 


