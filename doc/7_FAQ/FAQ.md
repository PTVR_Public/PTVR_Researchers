FAQ {#FAQ}
==================




- What should I do if I get a *ModuleNotFoundError* message in the Python console ?   
e.g.   
`ModuleNotFoundError : No Module named 'PTVR'` 

Answer : Read the IMPORTANT warning in the [Install Python subsection](@ref ptvrInstallation) about setting the **Python Environment Variable PYTHONPATH**.  
<BR>

- What should I do if I get a *TypeError* message in the Python console :   
For instance, in the Spyder IDE   :   
`TypeError: __init__() got an unexpected keyword argument 'image_fil'  ` 

<img src="spyder_error_msg.png" width="800"   />

From this message, you can guess that there is no 'image_fil' parameter in the Sprite() object. To help you solve this issue, you can look at all the parameters of an object by looking at the **Docstring** of this object.   
In spyder, for instance, you can hover with your mouse on the word Sprite in the script. As shown in the image below, a grey window will appear (here just above the word Sprite) with all parameters' names and explanations about them: this is the **Docstring** of the Sprite object.
<img src="spyder_error_msg_2.png" width="800"   />
In this case, you can see that there is a parameter called 'image_file' in the Sprite object and that the error arose from misspelling this name.   

**Note**: You could also type "Ctrl + i" once your mouse cursor is inserted on the Sprite word in the script. Then, the docstring would appear in the top right pane (see image below).      
<img src="spyder_error_msg_3.png" width="800"   />
<BR>


- How can I quit (exit) a PTVR program ?

Answer : When nothing is programmed within the PTVR script to quit (e.g. the demo quit_PTVR.py), simply close the window of the PTVR running script (usually by clicking with your mouse in the window's top right corner).
This is a safe way of quitting.  
<BR>