\mainpage Documentation for Users

Sections of the Documentation
=============================

<img src="Personal files-rafiki.png" width="350" height="350" style="float:right; padding-right:100px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->


 -   @subpage overviewOfPTVR
<BR>
 -   @subpage gettingStartedwithPTVR
<BR>
 -   @subpage BehaviouralSciences
<BR>
 -   @subpage UserManual
<BR>
 -   @subpage UserManualAdvanced_1
<BR>
 -   @subpage FamousExperiments
<BR>
 -   @subpage immerseYourselfInFigures
<BR>
 -   @subpage Index
<BR>
 -   @subpage Glossary
<BR>
 -   @subpage FAQ 
<BR>
 -   @subpage changelog  
<!--
<BR>
 -   @subpage Contribute
-->

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

Notes on the Documentation
============================

<img src="aide_provisoire_pour_click_sur_subsections.png" width="180" height="100" style="float:right; padding-right:220px; padding-left:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->
 
Do not forget the arrows in the navigation tree on the left
---------------------------------------------------------------------
Click on the arrow to the left of a section and a **dropdown menu** will appear to let you select among its sub-sections (see figure on the right).    


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->



**Attributions of images**

<a href="https://storyset.com/office">Office illustrations by Storyset</a>

