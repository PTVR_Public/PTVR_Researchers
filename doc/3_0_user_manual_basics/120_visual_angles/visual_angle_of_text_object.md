Visual angle of Text {#visualAngleOfText}
=====================

#### SIZE OF TEXT

When you create a Text visual object, you can make **one of the two** following parameters active to define the **size** of the characters of this Text object [(See the definitions of these two parameters here)](@ref TextAndTypography):  
- `fontsize_in_postscript_points`  
- `visual_angle_of_centered_x_height_deg`

The argument (20 and 0) passed by default to these parameters are shown below:   


    my_text = PTVR.Stimuli.Objects.Text ( ..., fontsize_in_postscript_points = 20,   visual_angle_of_centered_x_height_deg = 0, ... )  
    
If you pass an argument (different from zero) to the parameter `visual_angle_of_centered_x_height_deg`, the parameter `fontsize_in_postscript_points` will be **ignored**.

#### ANGULAR SIZE OF TEXT : conditions of validity

Defining the parameter `visual_angle_of_centered_x_height_deg` is very convenient to define an angular x-height for your text.
However, this nominal angular size will only correspond to the actual value if you respect some conditions that are [explained in details here](@ref simpleDefinitionsInVS).

To say it quicky: your text should be positioned on a Tangent Screen and at the center of this tangent screen (i.e. at the point called Screen Origin - SO [in the figures here](@ref _2DcartesianCoordinateSystemInVS)).

Then, and only then, the angle (in degrees) **subtended** by the height of an 'x' (for a given font)  **at the origin** of the current Coordinate System will correspond to the value of  `visual_angle_of_centered_x_height_deg` .


#### DEFINING ANGULAR SIZE OF TEXT EVEN WHEN ACTUAL VALUE IS A GROSS APPROXIMATION

It is **often convenient** to use the  `visual_angle_of_centered_x_height_deg` parameter, even when you do not want to control precisely the actual angular value of the text.

The simplest example is when you want to display labels for objects placed at short and long viewing distances: you want these labels to be visible independently of their viewing distance and using the  `visual_angle_of_centered_x_height_deg` parameter is THE solution.

A good example is shown in the 1_global_cartesian_coordinate_system.py  demo  where the axes tick labels are all visible although they are placed at very different viewing distances (path of this demo: ...\PTVR_Researchers\Python_Scripts\Demos\Coordinate_Systems\).


<BR>
<div id="note">\emoji :sunglasses: **Tip**: You can get more information on all the Text Object parameters when you are in your Python interface development environment (**IDE**). For instance, in **Spyder**, if you see a code similar to that shown below, you can either hover with your mouse on the word **Text** (after "world.") or click on it and press Ctrl-i: </div>

    my_text = PTVR.Stimuli.Objects.Text ( text = 'Hello World !', 
                                        position_in_current_CS = np.array ( [ 0, 1, 1 ] ),
                                        fontsize_in_postscript_points = 60
                                    )





#### Demos
The path of demos is:
...\PTVR_Researchers\Python_Scripts\Demos\Text

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>10_hello_world.py 
<td> Simplest way of displaying text (character size is defined with  fontsize_in_postscript_points, i.e. this is NOT an angular value)

<tr>
<td>text_height_at_different_radial_distances.py 
<td> Visually check that texts of same angular x-height at different viewing distances are visually overlapping.

<tr>
<td>Stroop_effect_with_tangent_screens.py 
<td> A simple demo of the Stroop effect with simultaneously displayed tangent screens. (character size is defined with  fontsize_in_postscript_points, i.e. this is NOT an angular value - why is it here then? just for the pleasure of seeing colours)

<BR>
</table>
