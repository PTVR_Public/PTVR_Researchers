Visual angle of objects {#visualAngleOfObject}
=====================

*Note: This section is under construction but the following links or demos are operational.*
<BR>


#### Links 

- [Visual angles in 'Essentials' section](@ref visualAnglesInVS)


#### Demos


<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 
Main demos in : ...\PTVR_Researchers\Demos\Objects\   
angular_size_of_object.py 
<td> How to specify the angular size of an object.

<tr>
<td> angular_size_check.py 
<td> How to check if the angular size of an object is correct.




<BR>
</table>