Visual Angles {#visualAngles} 
============


This section is divided into the following sub-sections:  
           
- @subpage visualAngleOfObject  
- @subpage visualAngleOfText  



#### Links 

- [Visual angles in 'Essentials' section](@ref visualAnglesInVS)