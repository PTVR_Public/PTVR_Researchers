Graph UI {#graphUI}
==========

PTVR vous permet de créer des graphiques dans votre UI grâce à l'objet PTVR.Stimuli.World.GraphUI . Vous avez la possibilité de créer un graphique simple, ou un graphique à double entrées. 


## Créer un graphique simple

Le graphique est composé des principaux paramètres suivants : 
- Sa taille en hauteur et en largeur, exprimée en pixels
- Les points à afficher sur le graphique, par paire : Pour un point, il faut donner sa position en x et en y
- La valeur maximum sur l'axe des Y, qui sert notamment a laisser de l'espace en Y dans le graphique 
- Les graduations en x
- Les graduations en y 
- Le nom de l'axe x
- Le nom de l'axe y 

Un exemple concret d'utilisation est donné ci-dessous :  

```
points = [ [0.2,6] , [0.4,13] , [0.6,20] , [0.8,22] , [1,22] , [1.2,21] , [1.4,23] , [1.6,22] ]
graph = PTVR.Stimuli.UIObjects.GraphUI(data_points=points, 
                                       maximum_y_value=25, 
                                       spacing_x_values=300,
                                       x_labels = [ 0.2 , 0.4 , 0.6 , 0.8 , 1 , 1.2 , 1.4 , 1.6 ],
                                       y_labels = [ 6 , 13 , 20 , 22 , 22 , 21 , 23 , 22 ],
                                       x_axis_name= "X values",
                                       y_axis_name = "Y values")
```

<img src="simpleGraph.PNG" /> 

## Créer un graphique à double entrées 

En plus des axes x et y traditionnels, vous pouvew décider de rajouter jusqu'à deux autres axes : un en haut du graphique, et un à droite du graphique. Pour chacun de ces deux nouveaux axes, vous avec la possibilité de mettre de nouvelles valeur pour les graduations et un nouveau nom. 
Pour cela, vous devez utiliser la méthode ```CreateDoubleEntryGraph()```

Voici un exemple concret de son utilisation : 

```
points = [ [0.2,6] , [0.4,13] , [0.6,20] , [0.8,22] , [1,22] , [1.2,21] , [1.4,23] , [1.6,22] ]
graph = PTVR.Stimuli.UIObjects.GraphUI(data_points=points, 
                                       maximum_y_value=25, 
                                       spacing_x_values=300,
                                       x_labels = [ 0.2 , 0.4 , 0.6 , 0.8 , 1 , 1.2 , 1.4 , 1.6 ],
                                       y_labels = [ 6 , 13 , 20 , 22 , 22 , 21 , 23 , 22 ],
                                       x_axis_name= "X values",
                                       y_axis_name = "Y values")

graph.CreateDoubleEntryGraph(x_up_labels = [ 95.09 , 150.71 , 238.86 , 378.57 , 600.0 , 950.94 , 1507.13 , 2388.64],
                                 x_up_axis_name= "New X values",
                                 y_right_labels = [ 80 , 55 , 30 , 25 , 25 , 24 , 26 , 25 ],
                                 y_right_axis_name= "New Y Values"
                                 )
```

<img src="complexGraph.PNG" /> 


#### Links

- PTVR.Stimuli.UIObjects.GraphUI 
- PTVR.Stimuli.UIObjects.GraphUI.CreateDoubleEntryGraph	

#### Demos

<table id="demo_table">

<tr><th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>UI_graph.py
<td>Show how to create a simple UI Graph and a double entre UI Graph


</table>

