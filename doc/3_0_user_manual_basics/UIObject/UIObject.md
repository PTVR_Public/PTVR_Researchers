UIObject{#uiObject}
============

Dans PTVR il est possible de placer des uiObject visible uniquement sur l'ecran de l'operateur.

La position pour les différents UIObject repose sur x et y 
x pour l'axe width 
y pour l'axe height.
0 égal au centre de l'écran et 1 au maximum de l'axe de l'ecran et -1 au minimum de l'ecran.

<p align="center">
  <img src="UICanvas.png">
</p>

La liste d'UIObject disponible

- - @subpage graphUI