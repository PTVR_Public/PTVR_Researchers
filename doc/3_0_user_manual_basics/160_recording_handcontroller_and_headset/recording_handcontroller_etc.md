Tracking handcontroller, headset, etc. {#recordingHandcontrollerEtc}
================
<BR>
<img src="recording_handcontroller_etc.jpg" width="600" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

(Free Image from <a href="https://www.freepik.com/">Freepik</a>)

<BR>
#### Recording handcontroller data in a file

The code below shows how to record handcontroller data in a file.

    my_world = The3DWorld (
      output_hand_controller = True,
      hand_controller_save_sampling_period = 10, # in ms: 10 ms is the maximal value and produces approximately a 90 Hz sampling rate.	
      hand_controller_events_are_recorded = True # if True -> records events such as the trigger of the handcontroller.
    )

The following lines represent the variables (columns) recorded in the file.

"hc" stands for handcontroller.   
"CS" stands for Coordinate System.  
"TS" stands for Tangent Screen

name_of_subject	  
trial  
scene_id   
elapsed_frames   
date_of_exp_run   
timestamp   
time_stamp_ms   

hc_global_pos_x :  x cartesian coordinate of hc (defined in the global CS).   
hc_global_pos_y :  y cartesian coordinate of hc (defined in the global CS).   
hc_global_pos_z :  z cartesian coordinate of hc (defined in the global CS).     

hc_global_unit_vector_x :  x cartesian coordinate of the unit vector pointing in the direction of hc (defined in the global CS).   
hc_global_unit_vector_y :  y cartesian coordinate of the unit vector pointing in the direction of hc (defined in the global CS).   
hc_global_unit_vector_z :  z cartesian coordinate of the unit vector pointing in the direction of hc  (defined in the global CS).     
> The term normalized vector is sometimes used as a synonym for unit vector. (Wikipedia contributors. (2024, September 6). Unit vector. In Wikipedia, The Free Encyclopedia. Retrieved 18:32, December 5, 2024, from <a href="https://en.wikipedia.org/w/index.php?title=Unit_vector&oldid=1244283561">this link.</a>)

hc_global_rot_x :  rotation of hc about the x axis (defined in the global CS).   
hc_global_rot_y :  rotation of hc about the y axis (defined in the global CS).   
hc_global_rot_z :  rotation of hc about the z axis (defined in the global CS).
> These are Euler angles. Thus, the order in which the rotations are applied does matter.   
> [In Unity Manual:](https://docs.unity3d.com/2022.2/Documentation/Manual/QuaternionAndEulerRotationsInUnity.html) « Unity performs the Euler rotations sequentially around the z-axis, the x-axis and then the y-axis. » This is also the case in PTVR.    

hc_collision_global_pos_x : x cartesian coordinate of the collision point (defined in the global CS).   
hc_collision_global_pos_y : y cartesian coordinate of the collision point (defined in the global CS).   
hc_collision_global_pos_z : z cartesian coordinate of the collision point (defined in the global CS).   

hc_collision_in_TS_cartesian_x : x cartesian coordinate of the collision point (defined in the CS of the TS) when the collision point is on a TS.   
hc_collision_in_TS_cartesian_y : y cartesian coordinate of the collision point (defined in the CS of the TS) when the collision point is on a TS.    
hc_collision_in_TS_cartesian_z : z cartesian coordinate of the collision point (defined in the CS of the TS) when the collision point is on a TS.    
 
hc_collision_in_TS_perimetric_ecc : eccentricity of the collision point (defined in the 2D CS of the TS).   
hc_collision_in_TS_perimetric_hm :  half-meridian of the collision point (defined in the 2D CS of the TS).   

hc_collided_object_global_pos_x : x cartesian coordinate of the collided object (defined in the global CS).  
hc_collided_object_global_pos_y : y cartesian coordinate of the collided object (defined in the global CS). 
hc_collided_object_global_pos_z : z cartesian coordinate of the collided object (defined in the global CS). 

hc_collision_distance :  	orthogonal distance between the hc and the collision point. 
hc_collided_object : 	 	name of the collided object (for instance "sphere, "TS", etc.).  

event_name :   		  	 	name of an event associated with the hc (for instance "trigger"). 



#### Recording headset data in a file

The code below shows how to record **headset** data in a file.

    my_world = The3DWorld (
      output_headset = True,  
	  headSaveSamplingPeriod = 10,
      hand_controller_events_are_recorded = True # if True -> records events such as the trigger of the handcontroller (useful to record for instance the instants at which the user points at an object with his/her head AND subsequently presses the trigger of the handcontroller.
    )

The following lines represent the variables (columns) recorded in the file.

Reminder:  
"CS" stands for Coordinate System.  
"TS" stands for Tangent Screen

name_of_subject	   
trial    
scene_id   
elapsed_frames    
date_of_exp_run    
timestamp    
time_stamp_ms    

headset_global_pos_x :  x cartesian coordinate of headset (defined in the global CS).   
headset_global_pos_y :  y cartesian coordinate of headset (defined in the global CS).   
headset_global_pos_z :  z cartesian coordinate of headset (defined in the global CS).     

headset_global_unit_vector_x :  x cartesian coordinate of the unit vector pointing in the direction of headset (defined in the global CS).   
headset_global_unit_vector_y :  y cartesian coordinate of the unit vector pointing in the direction of headset (defined in the global CS).   
headset_global_unit_vector_z :  z cartesian coordinate of the unit vector pointing in the direction of headset  (defined in the global CS).     
> The term normalized vector is sometimes used as a synonym for unit vector. (Wikipedia contributors. (2024, September 6). Unit vector. In Wikipedia, The Free Encyclopedia. Retrieved 18:32, December 5, 2024, from <a href="https://en.wikipedia.org/w/index.php?title=Unit_vector&oldid=1244283561">this link.</a>)

headset_global_rot_x :  rotation of headset about the x axis (defined in the global CS).   
headset_global_rot_y :  rotation of headset about the y axis (defined in the global CS).   
headset_global_rot_z :  rotation of headset about the z axis (defined in the global CS).
> These are Euler angles. Thus, the order in which the rotations are applied does matter.   
> [In Unity Manual:](https://docs.unity3d.com/2022.2/Documentation/Manual/QuaternionAndEulerRotationsInUnity.html) « Unity performs the Euler rotations sequentially around the z-axis, the x-axis and then the y-axis. » This is also the case in PTVR.  

headset_collision_global_perimetric_ecc : eccentricity of the collision point (defined in the global perimetric CS).   
headset_collision_global__perimetric_hm : half-meridian of the collision point (defined in the global perimetric CS).   
headset_collision_global__perimetric_rd : radial distance of the collision point (defined in the global perimetric CS).

headset_collision_global_pos_x : x cartesian coordinate of the collision point (defined in the global CS).   
headset_collision_global_pos_y : y cartesian coordinate of the collision point (defined in the global CS).   
headset_collision_global_pos_z : z cartesian coordinate of the collision point (defined in the global CS).

headset_collision_in_TS_cartesian_x : x cartesian coordinate of the collision point (defined in the CS of the TS) when the collision point is on a TS.   
headset_collision_in_TS_cartesian_y : y cartesian coordinate of the collision point (defined in the CS of the TS) when the collision point is on a TS.    
headset_collision_in_TS_cartesian_z : z cartesian coordinate of the collision point (defined in the CS of the TS) when the collision point is on a TS.    
 
headset_collision_in_TS_perimetric_ecc : eccentricity of the collision point (defined in the 2D CS of the TS).   
headset_collision_in_TS_perimetric_hm :  half-meridian of the collision point (defined in the 2D CS of the TS).

headset_collided_object_global_pos_x : x cartesian coordinate of the collided object (defined in the global CS).  
headset_collided_object_global_pos_y : y cartesian coordinate of the collided object (defined in the global CS). 
headset_collided_object_global_pos_z : z cartesian coordinate of the collided object (defined in the global CS). 

headset_collision_distance :  	orthogonal distance between the headset and the collision point.   
headset_collided_object : 	 	name of the collided object (for instance "sphere, "TS", etc.).  

event_name :   		  	 		name of an event associated with the HANDCONTROLLER (hc) (for instance "trigger"). 





<!-- TODO later

This section is divided into the following sections:

- @subpage 

-->



<!-- TODO later
#### Demos

Main demos are in:
...\PTVR_Researchers\Python_Scripts\DEMOS\Tracking_of_hand_head_or_gaze\

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> TODO.py 
<td> 


<BR>
</table>


-->

