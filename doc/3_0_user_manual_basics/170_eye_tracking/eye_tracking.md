Eye Tracking {#eyeTrackingInUserManual}
================

<img src="1024px-Western_Lowland_Gorilla2010501A_(cropped).jpg" width="738" height="343

" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

Gaze to his/her right.
<BR><BR><BR>

(Image from <a href="https://commons.wikimedia.org/wiki/File:Western_Lowland_Gorilla2010501A_(cropped).jpg">Oka21000</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons)

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->


#### Under construction

<!-- TODO later

This section is divided into the following sections:

- @subpage 

-->


#### Recommendations
**In general, we do not recommend using online eyetracking for instance to simulate scotomas or retinopathies.**  
see:  
Stein, N., Niehorster, D. C., Watson, T., Steinicke, F., Rifai, K., Wahl, S., & Lappe, M. (2021). A Comparison of Eye Tracking Latencies Among Several Commercial Head-Mounted Displays. I-Perception, 12(1), 204166952098333. https://doi.org/10.1177/2041669520983338  

Aguilar, C., & Castet, E. (2011). Gaze-contingent simulation of retinopathy: Some potential pitfalls and remedies. Vision Res, 51, 997–1012. https://doi.org/10.1016/j.visres.2011.02.010




#### Links
[For eye tracker installation click here](@ref setupHMD)



#### Demos

Main demos are in:
...\PTVR_Researchers\Python_Scripts\DEMOS\eye_tracking\

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> gaze_cursor_pointing_with_recording.py 
<td> a target (defined with eccentricity and half-meridian jumps when you press the hand-controller trigger, and you can track this target with your gaze. Eyes recording is performed automatically and an eye-contingent reticle is displayed (or not)...


<BR>
</table>




