Interactions between Events and Callbacks {#Interaction}
============

# Subsections

-   @subpage Event

-   @subpage Callback

-   @subpage simplifiedInteractions

## Introduction
When you create a visual Scene, you can also add an **Interaction** to this scene.    
For example, you can decide that pressing the keyboard's "q" key **while this scene is displayed** will allow you to instantaneously quit the PTVR.exe program ( [see demo here](@ref Quit) ).

An Interaction requires a list of **[Events](@ref Event)** and a list of **[Callbacks](@ref Callback)**.

In the example above, the Event would be triggered by "q" press and this Event would then call the Quit Callback ( [see demo here](@ref Quit) ). This interaction is represented by highlighted colors in the figure below.

To add an Interaction to a visualScene, use the function .AddInteraction() as in the following line of code:   
```
my_scene.AddInteraction (Events = [my_events], Callbacks = [my_callbacks])
```

[my_events] is a list of ONE or SEVERAL [Events](@ref Event)  

[my_callbacks] is a list of ONE or SEVERAL [Callbacks](@ref Callback)  

Note that this syntax clearly shows that an interaction is specific to a given scene !


The figure below shows some of the Events and Callbacks that you can use to create one or multiple interactions for a given scene (the [Events](@ref Event) and the [Callbacks](@ref Callback) are respectively in yellow and blue).

<p align="center">
  <img src="interaction.png">
</p>





<!-- début de TODO : demander à JTM

In Pink, indicate what's happen when the Callback is "deactivate"

In Red, features not implemented for the moment

FIN de TODO : demander à JTM -->

