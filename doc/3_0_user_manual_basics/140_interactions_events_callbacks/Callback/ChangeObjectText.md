ChangeObjectText {#ChangeObjectText}
============

Cet Callback permet :
Lorsqu'il est appeler en effect <b>"activate"</b>  permet de changer le contenu d'un objet Text.
lorsqu'il est appeler en effect <b>"deactivate"</b> permet de remettre le contenu d'un objet Text par défaut.

```
myCallback = PTVR.Data.Callback.ChangeObjectText(text="My New Text", textObjectId=myText.id,effect = "activate")
```

<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>CallbackChangeObjectText.py
<td>How to use Callback ChangeObjectText

</table>

<br>