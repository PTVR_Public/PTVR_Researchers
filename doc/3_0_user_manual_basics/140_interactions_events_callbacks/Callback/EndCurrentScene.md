End current scene {#EndCurrentScene}
============


#### Default behavior
The EndCurrentScene Callback ends a scene and goes by default to the next scene of the experiment.

```
	my_callback = PTVR.Data.Callback.EndCurrentScene () 
	# default behaviour is to go to the next scene
```

This default behaviour of going to the next scene can be changed by further calling SetNextScene () (a method / function of the 'EndCurrentScene' Callback) with an argument defining the scene that must be presented next (called  ID_of_the_scene_to_be_presented_next in the code below).

    my_callback = PTVR.Data.Callback.EndCurrentScene ()   
    my_callback.SetNextScene ( idNextScene = ID_of_the_scene_to_be_presented_next)   
    

#### Examble of use
This code is often used to create a **cyclic** presentation **across time** of all the scenes created at the beginning of the script. For this purpose, the variable ID_of_the_scene_to_be_presented_next is the rank of the first scene of the set of scenes.



<p align="center">
  <img src="EndCurrentSceneCyclique.png" width = "300">
</p>


#### Demos

<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 


<tr>
<td>
  ...\PTVR_Researchers\Python_Scripts\Demos\Scenes\
2.0_one_scene_w_NON_simplified_duration.py
<td> Creates ONE scene  whose duration is controlled by using a standard interaction between an Event and a Callback.

<tr>
<td>
  ...\PTVR_Researchers\Python_Scripts\Demos\Scenes\
4_cycle_of_scenes_w_NON_simplified_duration.py
<td> Creates a CYCLE of scenes whose duration is controlled by using a standard interaction between an Event and a Callback.


</table>

<br>