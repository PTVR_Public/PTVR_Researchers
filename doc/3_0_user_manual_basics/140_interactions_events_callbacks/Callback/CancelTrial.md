CancelTrial {#CancelTrial}
============
Ce Callback permet d'indiquer dans la colonne Callback que la ligne de resultat que l'essai n'est pas à prendre en compte en indiquant "CancelTrial".


```
myCallback = PTVR.Data.Callback.CancelTrial()
```

<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>CallbackCancelTrial.py
<td>How to use Callback CancelTrial

</table>

<br>