EndCurrentTrial {#EndCurrentTrial}
============

Cet Callback gére la fin d'un essai et lance la première scene de l'essai suivant.

```
myCallback = PTVR.Data.Callback.EndCurrentTrial (sceneToEnd =  myScene)
```
Il est possible de choisir le prochain Essai grâce à la fonction SetNextTrial

```
myCallback.SetNextTrial(idNextScene = 2)
```
<br>
<table id="demo_table">
<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>CallbackEndCurrentTrial.py
<td>How to use Callback EndCurrentTrial
</table>
</br>