ChangeObjectColor {#ChangeObjectColor}
============

Cet Callback permet :
Lorsqu'il est appeler en effect <b>"activate"</b>  permet de change la couleur de l'objet indiqué.
lorsqu'il est appeler en effect <b>"deactivate"</b> de retourner à la couleur par défaut de l'objet indiqué.

```
myCallback = PTVR.Data.Callback.ChangeObjectColor(new_color=PTVR.Stimuli.Color.RGBColor(0.0, 1.0, 0.0, 1),objectId=myObject.id,effect = "activate")
```

  </code>

 <table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>CallbackChangeObjectColor.py
<td>How to use Callback ChangeObjectColor

</table>

<br>