ChangeBackgroundColor {#ChangeBackgroundColor}
============
Cet Callback permet :
Lorsqu'il est appeler en effect <b>"activate"</b>  de changer la couleur du fond
lorsqu'il est appeler en effect <b>"deactivate"</b> de retourner à la couleur par défaut de la scene 

```
myCallback = PTVR.Data.Callback.ChangeBackgroundColor(new_color=PTVR.Stimuli.Color.RGBColor(1.0, 0.0, 0.0, 1),effect = "activate")
```


 <table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>CallbackChangeBackgroundColor.py
<td>How to use Callback ChangeBackgroundColor

</table>

<br>