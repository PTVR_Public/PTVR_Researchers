RestartCurrentScene {#RestartCurrentScene}
============
Cet Callback permet de relancer la CurrentScene.

```
myCallback = PTVR.Data.Callback.RestartCurrentScene()
```
<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>CallbackRestartCurrentScene.py
<td>How to use Callback RestartCurrentScene

</table>

<br>