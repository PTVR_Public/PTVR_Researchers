Callbacks {#Callback}
============
A Callback is like a function that is called when an [Event](@ref Event) is triggered. 

For instance, the following line of code creates a Callback that will change the background color to red.   

    my_callback = PTVR.Data.Callback.ChangeBackgroundColor ( new_color = color.RGBColor(r = 0.7), effect = "activate")

<BR>

#### Links to subsections about some common Callbacks are listed below:    

- @subpage EndCurrentScene   


- @subpage Quit

<!--

- @subpage EndCurrentTrial

- @subpage CancelTrial

- @subpage RestartCurrentScene

- @subpage SuspendCurrentScene

- @subpage ChangeBackgroundColor

- @subpage ChangeObjectColor

- @subpage ChangeObjectEnable

- @subpage ChangeObjectText
-->


<BR>
The diagram below presents some other examples of Callbacks that can **interact** with [Events](@ref Event).



<p align="center">
  <img src="Callback.PNG">
</p>

#### Demos
Path of Demos:  
...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Interactions_Events_Callbacks\



<!--
https://www.gloomaps.com/t7bjtvMJHG

Chaque Callback peuvent être UnPausable et s'enclencher même si SuspendCurrentScene a été enclenché si isCallbackPausable = False.
Par défaut les Callbacks ont isCallbackPausable = True.
-->