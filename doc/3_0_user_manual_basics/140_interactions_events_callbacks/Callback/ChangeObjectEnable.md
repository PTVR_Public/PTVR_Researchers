ChangeObjectEnable {#ChangeObjectEnable}
============

Cet Callback permet :
Lorsqu'il est appeler en effect <b>"activate"</b>  permet de rendre visible l'objet indiqué.
lorsqu'il est appeler en effect <b>"deactivate"</b> permet de rendre invisible l'objet indiqué.

```
myCallback = PTVR.Data.Callback.ChangeObjectEnable (objectId=myObject.id, effect = "activate")
```

<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>CallbackChangeObjectEnable.py
<td>How to use Callback EndCurrentScene

</table>

<br>