SuspendCurrentScene {#SuspendCurrentScene}
============
Cet Callback permet :
De mettre en pause PTVR lorsqu'il est appeler en <b>"activate"</b>
De reprendre la scene en <b>"deactivate"</b>.

En pause l'écoulement du temps ne s'écoule plus, ainsi que les Callbacks.

```
myCallback = PTVR.Data.Callback.SuspendCurrentScene(effect = "activate")
```

<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>CallbackSuspendCurrentScene.py
<td>How to use Callback SuspendCurrentScene

</table>

<br>