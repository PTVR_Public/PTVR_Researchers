Quit PTVR {#Quit}
============
This Callback allows you to instantaneously quit PTVR.exe.

```
my_callback = PTVR.Data.Callback.Quit()
```

Beware ! Do NOT add Callbacks AFTER the Quit Callback as they would be ignored.



#### Demos
Path of Demos:  
...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Interactions_Events_Callbacks\Callbacks

<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>quit_PTVR.py
<td> How to quit a running script.

</table>

<br>





