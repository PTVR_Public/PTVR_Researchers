PointedAt (under construction) {#PointedAt} 
=======================

A "pointedAt" Event is a relatively complex kind of Event.

A "pointedAt" Event allows us to detect online if an object is pointed at by a pointing "device" such as the headset, the gaze (eye) or hand-controller ([see "Pointing" Section in User Manual](@ref pointing)).

<BR>
##### Links
["Pointing" Section in User Manual](@ref pointing)

<BR>
#### Demos
<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td> head_pointing_to_change_an_object.py
<td> shows How to use the PointedAt Event.

</table>

<br>




