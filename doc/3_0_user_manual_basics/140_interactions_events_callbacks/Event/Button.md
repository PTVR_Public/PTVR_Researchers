Button {#Button}
============
Il est possible de créer des Events ButtonUI qui s'enclenche:
Lorsque l'utilisateur appuye sur le bouton en mode "press", 
Lorsque l'utilisateur appuye sans relacher sur le bouton  en mode "hold" 
Lorsque l'utilisateur relache le bouton en mode "release"

La position des ButtonUI repose sur le même systeme de position d'[UIObject](@ref uiObject).

```
myButtonUI = PTVR.Stimuli.UIObjects.ButtonUI(nameButton=""ButtonUI")
```

```
myEvent = PTVR.Data.Event.Button(idObjectButton=myButtonUI.id)
```


Il est possible d'ajouté des UIObjects en enfant du ButtonUI. 

```
In Progress not activate
```

<table id="demo_table">
<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>EventButtonUI.py
<td>How to use Button with a ButtonUI  

</table>

<br>