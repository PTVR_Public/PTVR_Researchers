Events {#Event}
============

An Event is triggered when "something happens" during your experiment - for instance a key press.  

The following line of code creates an Event that will be triggered when the 'ENTER' (aka 'RETURN') key is pressed on the keyboard.

    my_event = PTVR.Data.Event.Keyboard ( validResponses = ['ENTER'], mode = "press" )


<BR>
#### Links to subsections about some common Events are listed below:

- @subpage Keyboard

- @subpage handController

- @subpage Timer

- @subpage PointedAt


<BR>


Each Event has a parameter 'mode" that can be set to "press" or "release". By default the mode is set to "press".  
The exact functions of these two modes depend on each specific type of Event.


Once an Event is created, it can be linked to one or several [Callbacks](@ref Callback) thanks to [interactions between Events and Callbacks](@ref Interaction).



#### Demos
Path of Demos:  
...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Interactions_Events_Callbacks\





