Keyboard {#Keyboard}
============

Here is the way to create an Event that will be triggered when the subject presses the space bar on the keyboard :     
 
    my_event = PTVR.Data.Event.Keyboard ( validResponses = ['space'], mode = "press" )   



While the code above requires a space bar key press from the subject, it is also possible to have a list of several key presses passed as arguments to the parameter 'validResponses'. In the line of code below for instance, the Event will be activated if the 'space' key **OR** the 'escape' key is pressed:  

    my_event = PTVR.Data.Event.Keyboard ( validResponses = ['space', 'escape'], mode = "press" )


All the valid key presses and their respective codes are shown at the end of the present page.
<BR>

#### Demos
Path of Demos:  
...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Interactions_Events_Callbacks\

<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td> 1_keyboard_Event.py
<td> How to use the Keyboard to associate Events (key presses) with Callbacks (such as a color change of the background).

</table>

<br>



#### Parameter 'validResponses' of the PTVR.Data.Event.Keyboard object.
Here are the valid arguments to pass to the 'validResponses' parameter:

    'backspace'   
    'delete'  
    'tab'   
    'pause'  
    'escape'  
    'space'  
    'up'  
    'down'  
    'right'  
    'left'  
    'insert'  
    'home'  
    'end'   
    'page up'  
    'page down'  
    'f1'  
    'f2'      
    'f3'  
    'f4'  
    'f5'  
    'f6'  
    'f7'  
    'f8'  
    'f9'  
    'f10'  
    'f11'  
    'f12'  
    'numPad0'  
    'numPad1'  
    'numPad2'  
    'numPad3'  
    'numPad4'  
    'numPad5'  
    'numPad6'  
    'numPad7'  
    'numPad8'  
    'numPad9'  
    'numPad*'  
    'numPad+'  
    'numPad-'  
    'numPad/'  
    'numPad='  
    'a'  
    'b'  
    'c'   
    'd'   
    'e'   
    'f'   
    'g'    
    'h'  
    'i'  
    'j'  
    'k'  
    'l'  
    'm'  
    'n'  
    'o'  
    'p'  
    'q'  
    'r'  
    's'  
    't'  
    'u'  
    'v'  
    'w'  
    'x'  
    'y'  
    'z'  
    'numlock'   
    'caps lock'  
    'scroll lock'  
    'right shift'  
    'left shift'  
    'right ctrl'  
    'left ctrl'  
    'right alt'  
    'left alt'  


