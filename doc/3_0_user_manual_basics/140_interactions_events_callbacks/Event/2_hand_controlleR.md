Hand controllers {#handController}
============

On each of the two HTC Vive hand controllers, there are 3 possible choices to create Events:  
- the **trigger**   
- the **grip**  
- the **touch**  

<p align="center">
  <img src="ControllerVR.png" width="600pix"/>>
</p>


#### Examples

An Event activated when the user **presses** the trigger of the **right** controller is created with the line of code below:
```
  my_event = PTVR.Data.Event.HandController (validResponses = ['right_trigger'], mode="press")
```

An Event activated when the user **presses** the trigger of the **left** controller is created with the line of code below:
```
  my_event = PTVR.Data.Event.HandController (validResponses = ['left_trigger'], mode="press")   
```     

An Event activated when the user **releases** the trigger of the **left** controller is created with the line of code below:
```
  my_event = PTVR.Data.Event.HandController (validResponses = ['left_trigger'], mode="release")   
``` 


<BR>
<div id="note">\emoji :scream: **Warning:** Do not confuse the PTVR Hand Controller with a Hand Tracker (the latter does not exist (yet) in PTVR).</div>




#### Valid arguments for the 'validResponses' parameter'
- 'right_trigger'
- 'right_grip'
- 'right_touch'
- 'left_trigger'
- 'left_grip'
- 'left_touch'


#### Demos
<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td> hand_controller_Event.py
<td> How to create Events for the hand controllers

</table>

<br>
