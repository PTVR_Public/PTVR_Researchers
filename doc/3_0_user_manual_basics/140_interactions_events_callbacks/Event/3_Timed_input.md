Timer Event {#Timer}
============

The Timer Event occurs after a certain duration has elapsed since the beginning of the scene.


<p align="left">
  <img src="timed_Event.png" width = "300">
</p>

The code below creates a Timer Event that will occur 1000 ms after the beginning of the scene.
```
  my_event = PTVR.Data.Event.Timer (delay_in_ms = 1000)
```
<BR>


#### Example of use

A Timer Event can be used to define the duration of a scene.   
The principle is to create an **[interaction](@ref Interaction)** for a given scene between a **Timer Event** and an **[EndCurrenScene Callback](@ref EndCurrenScene)** (see code below):


    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    
	my_event = PTVR.Data.Event.Timer (delay_in_ms = my_scene_duration_in_ms)
    my_callback = PTVR.Data.Callback.EndCurrentScene() 

    my_scene.AddInteraction ( Events = [my_event], Callbacks = [my_callback] )

Thus the 'my_scene' VisualScene object will have a duration of my_scene_duration_in_ms milliseconds.



<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>
...\PTVR_Researchers\Python_Scripts\Demos\Scenes\
2.0_one_scene_w_NON_simplified_duration.py

<td> Simple script displaying one scene whose duration is controlled by using a standard interaction between an Event and an Callback.

</table>

<br>

