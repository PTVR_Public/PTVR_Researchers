Simplified Interactions {#simplifiedInteractions}
============

In some special cases, PTVR provides **simplified interactions**, i.e. ways to avoid creating standard interactions relying on explicit interactions between [Events](@ref Event) and [Callbacks](@ref Callback).

These **simplified interactions** are rare in PTVR: they are only meant to reduce code length in very special cases as presented below.

#### Control of Scene duration

When you want to control the duration of a scene, you can do it by using the special class **SimplifiedTimerScene** (instead of the class **Scene** plus an **interaction**) with one line of code as shown below:

```
   my_scene = PTVR.Stimuli.Scenes.SimplifiedTimerScene ( scene_duration_in_ms = 1000)  
```


This simplified code above does the same as the standard code below :

```
   my_scene = PTVR.Stimuli.Scenes.VisualScene ()  

   my_event = PTVR.Data.Event.Timer (delay_in_ms = 1000)
   my_callback = PTVR.Data.Callback.EndCurrentScene ()  

   my_scene.AddInteraction ( Events = [my_event], Callbacks = [my_callback] ) 
```



#### Demos
<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>
  ...\PTVR_Researchers\Python_Scripts\Demos\Scenes\
1_one_scene_w_simplified_duration.py
<td> Creates a scene whose duration is controlled by using the SimplifiedTimerScene class.

<tr>
<td>
  ...\PTVR_Researchers\Python_Scripts\Demos\Scenes\
2.0_one_scene_w_NON_simplified_duration.py
<td> Creates a scene  whose duration is controlled by using a standard interaction between an Event and an Callback.



<tr>
<td>  ...\PTVR_Researchers\Python_Scripts\Demos\Orientations\
20_rotation_animated_across_scenes.py
<td> Animated rotation of a cube  with a succession of scenes each being controlled in duration by the SimplifiedTimerScene class.

</table>

<br>

