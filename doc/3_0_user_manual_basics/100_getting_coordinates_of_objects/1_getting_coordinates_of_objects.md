Get position coordinates of objects {#gettingCoordinatesOfObjects} 
==============================
<!--- voir technote inria de JTM here : https://notes.inria.fr/iHJfp8q9SLmjh-hdgrdEtA# -->

             
You can recover the position coordinates (specified in the Global Coordinate System) of an object by using the following code:  
`my_object.get_position ()`  


#### Limitations

a/  
`my_object.get_position ()` does not return the correct coordinates of "my_object" if 
my_object has been rotated before with either `my_object.rotate_to_look_at ()` or `rotate_to_look_at_in_opposite_direction ()`.   
b/   
This limitation also occurs if the **parent** of "my_object" (if there is a parent) has been rotated either with `my_object.rotate_to_look_at ()` or `my_object.rotate_to_look_at_in_opposite_direction ()`.

  
More information on orientations of objects: [HERE](@ref specifyOrientationCheatSheet)


#### Demos
Path of Demos: ...\PTVR_Researchers\Python_Scripts\Demos\Coordinate_Systems\


<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 50_get_global_coordinates_of_objects.py 
<td> Does what it says.

<tr>
<td> 51_get_global_coordinates_of_objects_placed_on_tangent_screen.py
<td> Does what it says.




<BR>
</table>



