Infinite cone concept {#InfiniteConeConcept}
============

## Why the infinite cone concept?

Historically researchers use physical flat screen for their experiments.

An important consideration is the angular size of the objects placed on the experimental screen. Due to geometrical considerations an object's angular size placed on this screen depends on its position. There are several ways to mitigate this problem.

The first one is to calculate the angular size at the center of the screen and to neglect any size correction following it's on screen position. This corresponds to the circular cone with no corrections described below.

The second one is the same, but with a relatively simple correction making the angular size correct for an arbitrary orientation. This corresponds to the circular cone with corrections described below.

The last one is the precise angle in any case. This corresponds to the ellipsoid cone described below.

## Quick description

The cone concept is intrinsic to visualObjects and is used for placements on tangent screens.

The properties defining this concept are the perimetric coordinates given by the method `TangentScreen.SetObjectOnScreen`and the `coneCenter` attribute, both contained by the class TangentScreen.

The `coneCenter` attribute has to be a `coordinated` object or any child class.

Thus this center can even be an object placed on the screen. There are two kinds of cones. The circular base cone, already implemented, and the ellipsoid base cone under development.

## The Circular Base Cone

TODO: gif for each cases

Currently the cone use only the circular cone definition as described in https://paramanands.blogspot.com/2012/06/conics-and-the-cone-part-1.html#.YXhrnBw682w and shown in the figure below :

<p align="center">
  <img src="cones.jpg">
</p>

In this figure the plane defined by BPC is the `TangentScreen`, O is defined by the `coneCenter`attributes coordinates on the screen, and the angle BÂC is defined by the placed game objects coordinates on the screen.

Thus, by definition, the circular cone is defined by its apex, A, which corresponds to the scene origin, BPC is a circle centered in O. The demo for the "How to use" is `Cone_test.py` in Demos, giving the result illustrated below:

<p align="center">
  <img src="conev1-os.gif">
</p>
This is the simplest case, with no correction for the angular size. As you can see, the angle remains at 16° when the cone is centered, and is smaller if not, with a variation.

The first way to mitigate it is to add the ```isTangentAngularCorrection``` option to true. We obtain the result bellow:

As you can see, in the case of a tangent segment, the angle is correctly set at 16°. But, knowing that the cone base is circular, the segment absolute size is constant. Thus the visual angle changes following the orientation, the worst case being the perpendicular one.

## The Ellipsoid Base Cone

As seen above the circular base cone correspond to the simplest case in which the angle is approximated. It corresponds to the most used cases.

The ellipsoid cone is under development to allow an easy use of non-approximative angle size saw from the POV.

The ellipsoid cone definition corresponds to the intersection between a plan (the screen) and the cone (from the POV). In the image below the point "a" corresponds to the POV point. 

<p align="center">
  <img src="ellipsoidCone.png">
</p>

The plan is defined by the screen on which we want to place the gameObjects. We are interested in the ellipsoid equation that determines the objects positions. 

This equation can be defined thanks to the plan equation and the cone equation.

### The cone equation and plane equation

We first start with the general cone equation along the z axis with angle theta :
\f$
x^2/a^2+z^2/b^2=y^2\tan^2\theta
\f$
Which can be simplified, for circular unitary perpendicular cone, by
```math
x^2+z^2=y^2\tan^2\theta
```
Then we will need the plan equation : 
```math
Ax+By+Cz=D
```
This equation can be determined by a normal vector, its coordinates define A,B and C. Then D can be determined using any point on the plane.

Note that, knowing that the cone is defined as unitary and in the z direction, the plane equation SHOULD be given in the cone base. So if the cone, in Unity, is not in the unity z direction (general case), the plane equation should be transferred in the coordinate base in which the cone is defined on the z axis.

from now and then, we will note the Unity coordinate z',y',z' and the cone coordinate x,y,z.

By substitution, the ellipsoid equation can be defined:
```math
\kappa(A\tau\cos t+B\tau\sin t+C)=D,\,\kappa=\frac{D}{A\tau\cos t+B\tau\sin t+C}
```

with
```math
\tau = \tan \theta
```
and t is the angular position on the ellipsoid.



Finally, this equation defines the ellipsoid equation in the x,y,z base. To get the Unity base ellipsoid, we need a final transfer.

## Notes on Unity implementation pathway

Even if the problem is mathematically well defined, its implementation is far to be trivial.

This part defines how this complex problem is being solved.

There are two ways to implement the base transfer. The hard one, using the base transfer matrices. The easier one, using the Transform methods such as ``LookAt`` and local/global position.

The cone can be defined by a vector pointing on the screen (coneCenter) and its angular size (the eccentricity on which the object will be placed).

We first need to create a ``Transfom`` , ``coneTransform`` that, using the ``LookAt`` method, is oriented to the ``coneCenter`` point.

Then we place a new ``Transform`` at the same global position as the screen, but having as parent ``coneTransform``, called ``screenTransfom``. In this way, the ``screenTransfom`` local position corresponds to the position of the screen in the cone base.

Thanks to this transform, we can deduce the plan equation in the correct coordinates base.

Once the position on the ellipsoid is correctly defined, we can come back on the global position and place correctly the object.

