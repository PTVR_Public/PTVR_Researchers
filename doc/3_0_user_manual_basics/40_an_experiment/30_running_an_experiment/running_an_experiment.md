Running an experiment (under construction) {#runningAnExperiment} 
==================================================


<div id="note">\emoji :sunglasses: **Tip**: Before running an experiment, you should not forget to perform the [Room Calibration process](@ref calibrationOfHeadset).
</div>  



#### Demos
The path of demos is :  
 ...\PTVR_Researchers\Python_Scripts\Demos\Experiment_building\

[To be completed]

<!--
<table id="demo_table">
<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 
<tr>
<td> xxxxxxxxxxx.py 
<td> xxxx
<tr>
<td> xxxxx.py
<td> xxxxx
<BR>
</table>
--> 

<!-- FIN de DEMOS -->
