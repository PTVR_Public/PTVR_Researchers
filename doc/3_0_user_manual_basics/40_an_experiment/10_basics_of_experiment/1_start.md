Basics of experiment building {#introOfAnExperiment}
==================================================

We first introduce the use of the **The3DWorld** Object thanks to the script **10_hello_world.py** which is one of the smallest PTVR scripts. It shows the minimal code necessary to build a  3D World and place objects into it.  
The **10_hello_world.py** script is explained in details with the following steps:

1. The 3D World
---------------
Any PTVR script needs a **The3DWorld**  object (here called 'my_world') usually created as below:  

    my_world = PTVR.Visual.The3DWorld ()   

  
The minimal code to put a PTVR experiment into action is the following:


    import PTVR.SystemUtils
	import PTVR.Visual
    
    def main():
    	my_world = PTVR.Visual.The3DWorld ()              # Line A   
    	my_world.write()   									 # Line B  
    
    if __name__ == "__main__":
    	main()
    	PTVR.SystemUtils.LaunchThe3DWorld()

The script above does only two things from the user's perspective :      
- a/ it creates an empty 3D World.  
- b/ it creates a **results file** whose path is indicated in the console of your Interactive Development Environment (this results file will be explained in details later on).   

Out of curiosity, you may want to know what the 'my_world.write' line of code stand for: it creates a .json file that contains all the experiment's parameters that are used by the Unity Engine working behind the scene.


2. The Scene as a container of visual stimuli
------------------------------------------------
The didactic example above is of course not very useful.
If you want to present stimuli, you need a [scene](@ref Scene), implemented as a VisualScene Object, that will contain these stimuli (we usually refer to a PTVR VisualScene Object simply as a scene). You first create the scene (here called 'my_scene'), and then you add this [scene](@ref Scene) into the Experiment. This is achieved in the code below by the two lines of code **interleaved** between # Line A and # Line B of the previous code:

    	my_world = PTVR.Visual.The3DWorld ()   				# Line A

    	my_scene = PTVR.Stimuli.Scenes.VisualScene ()    	# create scene
    	my_world.add_scene (my_scene)                        	# add scene to experiment

    	my_world.write () 								   	# Line B
<BR>
<div id="note">\emoji :sunglasses: **Tip**: For those of you familiar with Unity, note that the PTVR VisualScene object does not have the same meaning as the Unity Scene object.
</div>  


3. Placing a stimulus in the scene
---------------------------------
We're stil far from having a useful experiment : we now want to add a stimulu (here a text). This is again achieved in two steps: you first create the object (here called 'my_text'), and then you place this object into the [scene](@ref Scene). These two lines of code must be **interleaved** between the lines of 'Scene creation' and 'Scene addition to the experiment'.
 
	...
    	my_scene  = PTVR.Stimuli.Scenes.VisualScene ()    	# create scene  

    	my_text = PTVR.Stimuli.Objects.Text (text = 'Hello World !', position_in_current_CS = np.array ( [ 0, 1, 1 ] ), fontsize_in_postscript_points = 60)
    	my_scene.place (my_text, my_world)  				
				
    	my_world.add_scene (my_scene)							# add scene to experiment  
	...


4.Summary
---------------
The **10_hello_world.py** script is now complete as shown below. It is not a useful experiment but it summarizes, as explained in the sections above, the three minimal steps for building a PTVR experiment. The last fundamental step is the interaction between Events and Callbacks and is explained in [another section](@ref Interaction).


    import PTVR.SystemUtils
    import PTVR.Visual
    
    def main():
    	my_world = PTVR.Visual.The3DWorld ()     		# Line A 

    	my_scene  = PTVR.Stimuli.Scenes.VisualScene ()    	# create scene

    	my_text = PTVR.Stimuli.Objects.Text (text = 'Hello World !', position_in_current_CS = np.array ( [ 0, 1, 1 ] ), fontsize_in_postscript_points = 60)
    	my_scene.place (my_text, my_world)  				
				
    	my_world.add_scene (my_scene)							# add scene to experiment

    	my_world.write() 									# Line B
    
    if __name__ == "__main__":
    	main()
    	PTVR.SystemUtils.LaunchThe3DWorld()






<BR>

**Demos**  
The path of demos is :  
 ...\PTVR_Researchers\Python_Scripts\Demos\Text\
<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 10_hello_world.py 
<td> Does what it says: useful to understand the The3DWorld object.



<BR>
</table>
<!-- FIN de DEMOS -->
