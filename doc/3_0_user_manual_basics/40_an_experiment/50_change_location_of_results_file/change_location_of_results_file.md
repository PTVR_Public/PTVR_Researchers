Change location of results files (under construction){#changeLocation0fResultsFile} 
==================================================

*Note: Although this subsection is under construction, you can use the Demos below to learn how to change the location of the [results file](@ref resultsOfAnExperiment).*
 
#### Demos
The path of demos is :  
 ...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Experiment_building\Saving_results\
<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 1_where_is_my_results_file_by_default.py 
<td> . 


<tr>
<td> 2_where_else_can_I_save_my_results_file.py 
<td> .

<BR>
</table>
<!-- FIN de DEMOS -->
