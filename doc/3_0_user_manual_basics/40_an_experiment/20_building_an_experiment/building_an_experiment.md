Building an experiment {#buildingAnExperiment}
==================================================

The first steps necessary to build a PTVR experiment have been explained in the previous section [basics of experiment building](@ref introOfAnExperiment). We are now introducing the key topic of Experimental Design which whill be extensively dealt with in [this specific section](@ref experimentalDesign).

The present page is divided into the following sub-sections:   
**a.** The trials.  
**b.** Building a sequence of trials WITHOUT PTVR ExperimentalDesign Objects.  
**c.** Building a sequence of trials WITH PTVR ExperimentalDesign Objects.  

#### Trials
<!-- Figure 1  -->

<tr>
  <img src="succession_of_3_trials_visual_search.png"  style="width:100%;">
</tr>

<tr>
  <td> Figure 1: Visual search task. Succession of three trials with one scene per trial. </td>
</tr>
<br>


An experiment is usually a sequence of trials as exemplified in Figure 1.

Our goal in the next sections is to explain the different ways of building a **sequence of trials** corresponding to our experimental questions.


#### Building a sequence of trials WITHOUT PTVR ExperimentalDesign Objects.

Under construction.    
 
In the meantime look at the first three demos at the end of the present page.


#### Building a sequence of trials WITH PTVR ExperimentalDesign Objects.
Under construction.  


#### Demos
The path of demos is :  
 ...\PTVR_Researchers\Python_Scripts\Demos\Experiment_building\
<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 10_loop_of_trials_w_one_scene_per_trial.py 
<td> The smallest experiment : each trial is made of ONE PTVR scene.

<tr>
<td> 20_loop_of_trials_w_several_scenes_per_trial.py
<td> A slightly more complex experiment : each trial is made of SEVERAL PTVR scenes.

<tr>
<td> 30_simplest_yes_no_experiment.py
<td> Same as '1_loop_of_trials_w_one_scene_per_trial.py' except that the subject has to press either the return key for "yes" or the space bar for "no" to go to the next trial.

<BR>
</table>
<!-- FIN de DEMOS -->

