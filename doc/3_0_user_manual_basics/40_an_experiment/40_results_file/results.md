The results file {#resultsOfAnExperiment}
==================================================

Everytime you run a script containing a The3DWorld object, a **results file** is automatically created. This file always contains a set of crucial variables (e.g subject's name), and you can add some other variables of your choice with a simple line of code for each added variable.
To illustrate these points, let's run the **3_simplest_yes_no_experiment.py** script below (in  ...\PTVR_Researchers\Python_Scripts\Demos\Experiment_building\)  ...

	nb_of_trials = 5
   
	def main():
    my_world = PTVR.Visual.The3DWorld(name_of_subject = "Sean" )

    for i in range (0, nb_of_trials):
        trial_scene = PTVR.Stimuli.Scenes.VisualScene (trial = i+1)
        trial_text = PTVR.Stimuli.Objects.Text ( text = "trial : " + 
                                                   str (trial_scene.trial) + " / " + str (nb_of_trials) +
                                                   "\nPress ENTER key for 'yes' or SPACE bar for 'no'",
                                               fontsize_in_postscript_points= 200,
                                               position_in_current_CS = np.array ( [0, 1, 1]) )
        # Create interaction for the scene         
        my_response_yes =  Event.Keyboard ( valid_responses = ['enter'], mode = "press", Event_name = "yes" )     
        my_response_no  =  Event.Keyboard ( valid_responses = ['space'], mode = "press", Event_name = "no"  )     
 
        STOP_current_scene = Callback.EndCurrentScene ()    
        
        trial_scene.AddInteraction ( 
                Events = [my_response_yes, my_response_no], 
                Callbacks = [STOP_current_scene])       
        trial_scene.place ( trial_text,my_world )

        # the next two lines add my_variable in the results file
        my_variable = random.randrange(1,10)
        trial_scene.fill_in_results_file_column("my_variable", my_variable) 

        my_world.add_scene ( trial_scene )
        
    my_world.write() # Write the Experiment to .json file
    
	if __name__ == "__main__":
            main()
            PTVR.SystemUtils.Launch3DWorld ()


... and during the experiment let's press the RETURN key ("yes") for the first 4 trials and the SPACE bar key ("no") for the 5th trial.

Now let's open the results file which by default will be in the following directory :  

     ...\PTVR\PTVR_Researchers\PTVR_Operators\Results\
with the following name:  

    3_simplest_yes_no_experiment__Main__Sean_2024-01-15-11-09-22.450.csv  

**How to interpret the information contained in the name of this file ?**
This name means that the script '3_simplest_yes_no_experiment.py' was run with a subject called "Sean" on the 15th of January 2024 at 11:09:22 and 450 ms. 
A further information is carried by 'Main' to distinguish this file from other optional recorded files (discussed later) that contain information on eye-/gaze- and head-tracking.

Here are the first colums of the results file displayed across two rows:

<img src = "3_simplest_yes_no_experiment__Main__Sean_2024-01-15-11-09-22.450_part1.jpg" width="900pix">   


<img src = "3_simplest_yes_no_experiment__Main__Sean_2024-01-15-11-09-22.450_part2.jpg" width="400pix">

The variables in each column (except "my_variable" in the last column) are the default variables that are automatically saved without the need to write any code into the script (some of them are not shown for clarity as indicated by '...').  

The next section explains the content of the results file.  


#### Each line of the file corresponds to an interaction
The most important point here is to understand that **each line corresponds to a PTVR [interaction](@ref Interaction)** having occurred during the experiment.  
In this simple experiment of 5 trials (with one scene per trial) there are only 5 lines as only one interaction was coded for each of the 5 scenes.
 
Here, in this particular experiment, the first line corresponds to the response made by the subject in first trial when he/she pressed the RETURN key [(Event)](@ref Event) (this event is called "yes" - see code in the script). This [Event](@ref Event) triggered the [Callback](@ref Callback) EndCurrentScene that occurred at the date shown in the column 'callback_timestamp'. Numbers in this column are converted to a more useful format in the column 'callback_timestamp_ms' (note that that the difference in ms (not shown here) between an event and its callback can be considered as zero). These timestamps can be used in many ways. For instance, it is often useful to measure a **reaction time (RT)**. Here, one could calculate the reaction time as the difference between the 'yes' response time (in the column 'callback_timestamp_ms') and the beginning of the scene (in the column 'scene_start_timestamp'). 


<div id="note">\emoji :sunglasses: **Tip**: It is recommended to open the .csv file with UTF 8 mode especially if you have letters with accents in your saved strings.   
</div>  

We have checked with LibreOffice that the .csv files are correctly opened and can be used to present the raw results (as in the table above), and they are also correctly processed by R / RStudio for statistical analysis.

#### How to add variables of your choice in the results file.   
For didactic purposes, a variable called "my_variable" was created on each trial of this simple experiment.   
Of course, this variable does not belong to the set of variables that are automatically saved in the results file. You therefore have to use the code below (that you might already have noticed in **3_simplest_yes_no_experiment.py**) to add this variable in the results file.

        # the next two lines add my_variable in the results file
        my_variable = random.randrange (1,10)
        trial_scene.fill_in_results_file_column ("my_variable", my_variable) 


#### Demos
------------------
Some Demos in:  
...\PTVR_Researchers\Python_Scripts\Demos\Experiment_building\
<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 30_simplest_yes_no_experiment.py 
<td> A short experiment with 5 trials, each containing one scene. In each trial, the subject has to press either the return key for "yes" or the space bar for "no" to go to the next trial. 


<BR>
</table>
<!-- FIN de DEMOS -->
