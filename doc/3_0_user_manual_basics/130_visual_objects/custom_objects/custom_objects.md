Custom Objects{#customobjects}
=============

In your PTVR experiment, you can integrate 3D complex or realistic objects (e.g., animals or characters) that VR designers have created. These 3D stimuli are often called “assets” in the VR community and can be bought or obtained for free. This is very convenient as it removes the burden of creating ex nihilo many 3D entities from simple ones like furniture to very complex ones such as entire cities. In Unity, several assets can be integrated within an “asset bundle” to allow efficient importing/exporting of a large number of assets (even with large file sizes). It is possible in a PTVR script to import an “asset bundle” with a user-friendly syntax so that the assets contained in the asset bundle become available as objects in the PTVR experiment. 

#### Links

<ul>
<li><a href="https://files.inria.fr/ptvr/index.html">Download free asset bundles</a> that are used in some PTVR demos</li>
</ul>


<img src="dancing_rabbit.jpg" width="600"   />



#### Demos

Main demos to create and use assets (CustomObjects) are in:
...\PTVR_Researchers\Python_Scripts\DEMOS\Assets_to_import_into_PTVR\   
The numbers 10, 20, 30 at the beginning of files are meant to order the scripts when you look at them say in the Windows Explorer. This order is to offer a didactic progression when you discover a PTVR feature (here the CustomObjects).

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>10_import_a_rabbit_asset_into_PTVR.py
<td>Shows how to import AssetBundles to create CustomObjects.  

<tr>
<td>20_animate_rabbit_asset.py
<td>Shows how to animate CustomObjects. 

<tr>
<td>dancing_rabbit_in_room_w_interactions.py
<td>Relatively complex demo to see some principles of interactivity between the user and the animations (here rabbit is dancing when it is pointed at by a hand-controller).

</table>

<br>
