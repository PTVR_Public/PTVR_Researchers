Sprites or Flat 2D Objects {#spriteObjects}
=============

Sprites are flat 2D primitive objects that you can use like any other primitive PTVR object (e.g. a sphere).   

> "In computer graphics, a sprite is a two-dimensional bitmap that is integrated into a larger scene, most often in a 2D video game" [(wikipedia link)](https://en.wikipedia.org/wiki/Sprite_(computer_graphics)).   

<img src="cat_sprites_on_cubes.png">

Figure 1: Several PTVR Sprites have been created from the same image file and have been placed on several cubes sides.  

Example of code to create a Sprite object:   

    my_sprite = PTVR.Stimuli.Objects.Sprite (
		path_to_image_file = "", 
    	image_file = "yellow_square_100pix.png", 
    	position_in_current_CS = np.array ( [ -1, -0.5, 2 ] ),
    	rotation_in_current_CS = np.array ([90, 0, 0]) )

**Where should the image file be stored on your PC ?**   
if '**path_to_image_file**' is set to "" (default), then the 'image_file' parameter    corresponds to a file stored on your PC in :   
...\PTVR_Researchers\PTVR_Operators\resources\Images   
or in one of its sub-directories.   
    
if '**path_to_image_file**' is set to say "C:", then the 'image_file' parameter corresponds to a file stored on your PC in "C:".  


<BR>

#### Demos

Main demos to create and use Sprites are in:
...\PTVR_Researchers\Python_Scripts\DEMOS\Objects\

The numbers 10, 20, 30 ...(just after 'Sprite_') are meant to order the scripts when you look at them say in the Windows Explorer. This order is to offer a didactic progression when you discover a PTVR feature (here the Sprites).

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> Sprite_10_2D_object_from_png_file.py
<td> Simple creation of a sprite

<tr>
<td> Sprite_20_letters_from_png_file.py
<td> Create 2D letters and rotate them.

<tr>
<td> Sprite_30_cats_on_cubes.py
<td> Create 2D cats images and place them on cubes' sides. 

</table>