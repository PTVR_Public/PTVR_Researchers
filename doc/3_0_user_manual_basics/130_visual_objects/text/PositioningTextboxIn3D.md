Position your Textbox directly in 3D {#textPositioningTextboxIn3D}
==========

#### Step 1: Set Textbox center position

PTVR offers you two ways to position the Textbox center position. The first possibility is to use **cartesian** coordinates. You can do it with the constructor: 

```
text_object = PTVR.Stimuli.Objects.Text(text="Hello", position=np.array([0.0,0.0,3.0]))
```

or with the method ``set_cartesian_coordinates()``:

```
text_object = PTVR.Stimuli.Objects.Text(text="Hello")  
text_object.set_cartesian_coordinates(x=0.0, y=0.0, z=3.0) 
```
The second possibility is to use **perimetrci coordinates** with the method ``set_perimetric_coordinates()``:

```
text_object = PTVR.Stimuli.Objects.Text(text="Hello")  
text_object.set_perimetric_coordinates(eccentricity=0.0, halfMeridian=0.0, radialDistance=3.0) 
```

#### Step 2: Set Textbox orientation 

By default, your Textbox will be oriented on the Z-axis in the 3D world. You can change its orientation using the method ``SetOrientation()``. 

For example:

- If you want your Textbox to face the origin, you can do it with **PTVR.Stimuli.Utils.VirtualPoint.isFacingOrigin**. Note that the origin is defined by default at position [0.0,0.0,0.0] in the 3D world, but that can be changed with **PTVR.Experiment.Experiment.set_origin_of_room_calibration_coordinate_system_virtual_point**.

- If you want you textbox to face the [HeadPOV](TODO-add-pointer) (user headset), you can do it with **PTVR.Stimuli.Utils.VirtualPoint.isFacingHeadPOV**

```
text_object = PTVR.Stimuli.Objects.Text(text="Hello")  
text_object.SetOrientation(isFacingHeadPOV=False, isFacingOrigin=False)
```

<img src="orientation1.PNG" height="200"/> 

```
text_object = PTVR.Stimuli.Objects.Text(text="Hello")  
text_object.SetOrientation(isFacingHeadPOV=False, isFacingOrigin=True)
```

<img src="orientation2.PNG" height="200"/> 

<div id="todo">To do : changer les images par une vue du dessus de la scène avec les axes représentés pour mieux voir l'orientation du texte</div>

#### Links

- PTVR.Stimuli.Objects.Text.__init__()
- PTVR.Stimuli.Utils.VirtualPoint.set_cartesian_coordinatesCoordinates()
- PTVR.Stimuli.Utils.VirtualPoint.SetSphericalCoordinates()
- PTVR.Stimuli.Utils.VirtualPoint.SetOrientation()  

#### Demos

<table id="demo_table">

<tr><th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>text_position.py
<td>Visualize the impact of the textbox on the text position.

<tr>
<td>text_orientation.py
<td>Show how the use of isFacingOrigin on the text in order to make it tangent works.

</table>

