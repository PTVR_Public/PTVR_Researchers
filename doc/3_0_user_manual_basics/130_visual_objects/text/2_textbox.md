Textbox{#textBox}
============

A Textbox is the **bounding box** for the text you want to display. It is the container in which the text is written. A defined by:
- its geometry (width in meters [textboxWidth_m](PTVR.Stimuli.Objects.Text.SetTextBoxParameters()), and height in meters **textboxHeight_m**)
- the line break permission  **isWrapping**

You can set these parameters with the [SetTextBoxParameters()](PTVR.Stimuli.Objects.Text.SetTextBoxParameters()) method like this:

```
text = PTVR.Stimuli.Objects.Text(text="Hello", position=np.array([0.0,0.0,3.0]))  
text.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=1, isWrapping=True)
```

<div id="note">\emoji :wink: **Remark:** When **isWrapping=True** the text lines are cut off to prevent them from exceeding the Textbox boundaries widthwise. When **isWrapping=False**, the limits of the Textbox can be exceeded, and the text will overflow from the Textbox. See the page @subpage textAboutIsWrapping for a more detailed explanation.</div>

Then, to position your Textbox in your 3D environment, you have to define the position of its **center** and its orientation. PTVR offers two possibilities to position your Textbox:

- @subpage textPositioningTextboxIn3D
- @subpage textPositioningTextboxOnScreen, using the local 2D coordinate system, of the screen, which allows the Textbox to benefit from features from Screen (e.g., eye-tracking). 

#### Links

- PTVR.Stimuli.Objects.Text.SetTextBoxParameters()

#### Demos

<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>textbox_properties.py
<td>Show the effect of the text box on the text.  

</table>

<br>


