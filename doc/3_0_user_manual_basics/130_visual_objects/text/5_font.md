Font{#textFont}
============

You can specify the **font** of your text with the parameter **font_name** (default font is ``Arial``)

```
text = PTVR.Stimuli.Objects.Text(text="Hello", font_name="TimesRoman")
```

Main fonts availables are ``Arial``, ``CourierNew``, ``georgia``, ``impact``, ``LiberationSans``, ``TimesNewRoman``, ``TimesRoman``, ``verdana``.  
You may also add a new font. To do so, save your font in the **TTF** format in the directory **PTVR\_Researchers > PTVR\_Operators > resources > Fonts**.

<div id="note">\emoji :wink: **Remark:** the name used for **font\_name** must be the same as the name of the TTF file.</div>

#### Links

- PTVR.Stimuli.Objects.Text.__init__	
- [Text and Typography](../../BehaviouralSciences/Stimuli3DWorld/Text/TextAndTypography.md)

#### Demos

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>typography_comparison.py
<td> Visualize the difference of fontsize between differents fonts when they have the same physical height. 

</table>