Alignment {#textAlignment}
============

The alignment of the text is done relative to its [Textbox](Textbox.md).  Therefore, you can to define the type of **horizontal alignment** (available options: **Center**, **Left**, **Right**, **Justified**, **Flush**) and **vertical alignment** (available options: **Middle**, **Top**, **Bottom**, **Capline**, **Baseline**). By default your text will be positioned with **Center** and **Middle** parameters.

For example, to position your text at the top left of the Textbox, you can do:

```
loremipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
text_object = PTVR.Stimuli.Objects.Text(text=loremipsum, position=np.array([0.0,0.0,3.0]), align_horizontal="Left", align_vertical="Top")
text_object.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping=True)

```



<img src="topleft.png" />

Note that for **Top** and **Bottom** vertical alignments, the alignment is relative to the [Em-Square](../../BehaviouralSciences/Stimuli3DWorld/Text/TextAndTypography.md). Therefore, when you place a text with a top alignment, the height limit of the Em-Square will be stuck to the top edge of the Textbox. Similarly, when you place a text with a Bottom alignment, the bottom boundary of the Em-Square will be stuck to the bottom edge of the Textbox.  

We show below two illustrations of this behavior, where the Textbox is represented by the yellow rectangle, starting with the **Top** alignement:

```
text_object1 = PTVR.Stimuli.Objects.Text (text="yxy", position= np.array([0.0,0.0,3.0]), align_horizontal="Left", align_vertical="Top")
text_object1.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
 
text_object2 = PTVR.Stimuli.Objects.Text (text="ÔxÔ", position= np.array([0.0,0.0,3.0]), align_horizontal="Center", align_vertical="Top")
text2.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)

text_object3 = PTVR.Stimuli.Objects.Text (text="txt", position= np.array([0.0,0.0,3.0]), align_horizontal="Right", align_vertical="Top")
text_object3.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
```

<img src="alignment_top.png" />

and then the **Bottom** alignement:

```
text_object1 = PTVR.Stimuli.Objects.Text (text="yxy", position= np.array([0.0,0.0,3.0]), align_horizontal="Left", align_vertical="Bottom")
text_object1.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
 
text_object2 = PTVR.Stimuli.Objects.Text (text="ÔxÔ", position= np.array([0.0,0.0,3.0]), align_horizontal="Center", align_vertical="Bottom")
text2.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)

text_object3 = PTVR.Stimuli.Objects.Text (text="txt", position= np.array([0.0,0.0,3.0]), align_horizontal="Right", align_vertical="Bottom")
text_object3.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
```
<img src="alignment_bottom.png" />  

The **Middle** vertical alignment is done at the line level in the center of the set of glyphs displayed (represented below by the blue line.). The presence of characters with ascendants or descendants will therefore impact the alignment of the text. We illustrate this behavior in the following example:

```
text_object1 = PTVR.Stimuli.Objects.Text (text="yxy", position= np.array([0.0,0.0,3.0]), align_horizontal="Left", align_vertical="Middle")
text_object1.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
 
text_object2 = PTVR.Stimuli.Objects.Text (text="xxx", position= np.array([0.0,0.0,3.0]), align_horizontal="Center", align_vertical="Middle")
text2.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)

text_object3 = PTVR.Stimuli.Objects.Text (text="txt", position= np.array([0.0,0.0,3.0]), align_horizontal="Right", align_vertical="Middle")
text_object3.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
```

<img src="middle_alignment.png" />

<div id="note">\emoji :scream: **Warning:** With **Justified** horizontal alignment, if  **isWrapping=False**, the text will compact into a single line rather than exit the Textbox. This is because justifying text means inserting spaces between words, so that both ends of each line are aligned on both margins (in this case both edges of the Textbox). The **isWrapping=False** prevents the sentence from returning to the line, but the **Justified** alignment still forces the text to be contained to the Textbox, giving the above result.</div>

An example of that case is:

```
loremipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
text_object = PTVR.Stimuli..Objects.Text(text=loremipsum, position=np.array([0.0,0.0,3.0]), align_horizontal="Justified", align_vertical="Middle")
text_object.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping=True)
```

giving the following result:

<img src="justified.png" />


#### Links

- [Textbox](Textbox.md)
- PTVR.Stimuli.Objects.Text.alignHorizontal
- PTVR.Stimuli.Objects.Text.alignVertical

#### Demos

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>text_alignment_ascender_descender.py 
<td> Show the impact of ascender and descender on the alignment.

<tr>
<td>text_alignment.py
<td>Show the behaviour of some text alignment methods.  

<tr>
<td>text_alignment_middle.py
<td> Showing that the alignment have the same behaviour regardless of the font used. 

</table>
