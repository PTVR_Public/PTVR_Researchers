About isWrapping parameter{#textAboutIsWrapping}
==============

The goal of this section to show you the effect of **isWrapping** parameter in different conditions. Several examples are illustrated where (i) Textbox objects are always represented by the yellow rectangle, and (ii) we always use default horizontal alignement (Center).

When **isWrapping=True** the text lines are cut off to prevent them from exceeding the Textbox boundaries widthwise. That's the main purpose of 
isWrapping parameter.

```
loremipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit." 
text_object = PTVR.Stimuli.Objects.Text (text=loremipsum, position=np.array([0.0,0.0,3.0]))
text_object.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping=True)
```

<img src="wrapping_true.png" />

When **isWrapping=False**, the limits of the Textbox can be exceeded, and the text will overflow from the Textbox.

```
loremipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit." 
text_object = PTVR.Stimuli.Objects.Text (text=loremipsum, position=np.array([0.0,0.0,3.0]))
text_object.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping=False)
```

<img src="wrapping_false.png" />

If the text is too long (i.e., it can't fit the Textbox dimension), when **isWrapping=True**, the text will overflow at the top and/or bottom depending on the vertical alignment of the text. For example, if we use the default vertical alignement (Middle), then the text will overflow at the bottom and top of the Textbox

```
loremipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ac metus consequat, rhoncus mauris nec, iaculis lectus" 
text_object = PTVR.Stimuli.Objects.Text (text=loremipsum, position=np.array([0.0,0.0,3.0]))
text_object.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping=True)
```

<img src="wrapping_true_long.png" />

However if **isWrapping=False**, the text will overflow only on the sides like above. Note that, in general, the effect of **isWrapping** may vary depending on text alignement. See also the page [Alignement](Alignment.md) for a more detailed explanation.
