Text {#text}
============

PTVR text features let you display text in a highly controlled manner regarding positioning in space, alignement, print size, font type and color. 

<img src="text_demo.PNG" width="100%">
Example of text placed on tangent screens (reference to the Stroop effect) - see the demo Stroop_effect_with_tangent_screens.py

You can manipulate all these notions easily using the Text object PTVR.Stimuli.World.Text. The Text object is composed of two elements: The text content and the bounding box of the text in which the text is displayed (called Textbox).  This object is based on the [TextMeshPro](http://digitalnativestudios.com/textmeshpro/docs/) object in Unity.

This section (under construction) contains information on the following text features:  

- @subpage linkToVisualAngleOfText

<!--

Ces fichiers ci-dessous sont plein d'erreurs et d'anciens codes PTVR !!!
- @subpage textBox
- @subpage textAlignment
- @subpage textSize
- @subpage textFont 
- @subpage textColor
-->

For more information on text in general and other related concepts, see :  

- [Text and Typography](@ref TextAndTypography)

- [TextmeshPro](http://digitalnativestudios.com/textmeshpro/docs/)

