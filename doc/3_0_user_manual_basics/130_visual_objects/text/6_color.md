Color{#textColor}
============

Color can be modifed with parameter **color**. Specify it with RGBA (Red, Green, Blue, Alpha) values each in [0.0,0.1]. See PTVR.Stimuli.Color.RGBColor for more information. 

For example, to display a text in red, you should write:

```
text_object = PTVR.Stimuli.Objects.Text(text="Hello", color=color.RGBColor(1.0,0.0,0.0,1.0))
```


#### Links

- PTVR.Stimuli.Objects.Text.__init__	
- PTVR.Stimuli.Color.RGBColor
