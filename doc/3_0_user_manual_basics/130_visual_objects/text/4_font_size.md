Font size{#textSize}
============


La taille du texte peut être définie en points, en se basant sur le PostScript point qui a une 
taille de 0.00035 mètres. Ainsi, si on affiche un texte de 14 points, il aura une taille physique de 0.0049 mètres.   

```
text = PTVR.Stimuli.Objects.Text(text="Hello",fontsize_in_points=14)
```
Par défaut, le texte possède une taille de 12 points.  

<div id="note">\emoji :wink: **Remark:** Dans Unity, la correspondance entre la 
taille du point et sa taille en mètres n'est pas la même que pour le PostScript. 
Une conversion est ainsi effectuée lorsque vous indiquez la taille en points que 
vous souhaitez avoir afin que le texte affiché corresponde bel et bien aux mesures 
standards du PostScript point. Plus d'informations sur la conversion utilisée 
[dans cette documentation]
(https://docs.microsoft.com/en-us/windows/mixed-reality/develop/unity/text-in-unity)</div>


Otherwise, PTVR offers the possibility to define the font size in degrees, which is much better from the user's perspective. Indeed, this method considers the **distance** between the origin and the text. Here, the font size refers to the [x-height](@ref TextAndTypography), i.e., the height of the lower case x.

```
text = PTVR.Stimuli.Objects.Text(text="Hello", position=np.array([0.0,0.0,3.0]), visual_angle_of_centered_x_height_deg=3) 
```

Note that, by default, a text has a visual angle of 0 degrees, and its size in points is prevailing. If an angular size is specified, then this angular size will be used instead. 

#### Links 

- PTVR.Stimuli.Objects.Text.SetXheightAngularSize()
- [Text font](Font.md)
- [Text and Typography](../../BehaviouralSciences/Stimuli3DWorld/Text/TextAndTypography.md)
- [Text in Unity, Microsoft documentation](https://docs.microsoft.com/en-us/windows/mixed-reality/develop/unity/text-in-unity)
-   [Gordon E.Legge, Charles A.Bigelow, Does print size matter for reading? A review of findingsfrom vision science and typography, Journal of Vision, 2011](http://j.pelet.free.fr/publications/design/Does_print_size_matter_for_reading_A_review_of_%EF%AC%81ndings_from_vision_science_and_typography.pdf)

#### Demos

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>typography_x_height_angular_size.py 
<td>Prove internal consistency in Unity with respect to angular size.

<tr>
<td>typography_comparison.py
<td>Visualize the difference of fontsize between differents fonts when they have the same physical height. 

<tr>
<td>text_fontsize_in_points.py
<td>Visualize a simple text with its size in points. 

</table>