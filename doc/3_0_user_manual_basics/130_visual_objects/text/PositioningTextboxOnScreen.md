Position your Textbox on a Screen {#textPositioningTextboxOnScreen}
==========

The Textbox can be positioned on a Screen, either on a FlatScreen with the **SetObjectOnScreen()** method, or on a TangentScreen with the **SetObjectOnTangentScreen()** method.

<div id="note">\emoji :wink: **Reminder:** The TangentScreen is a FlatScreen to which the SetTangentRotation()`` method is applied to be placed perpendicular to the origin.</div>

<div id="note">\emoji :sunglasses: **Tip**: By choosing this method to place this Textbox, you will be able to collect eye-tracking data relative to your text. See the page [Eye tracking](TODO-add-link) for a more detailed explanation. </div>

In practice, your Textbox will "belong" to the Screen. You will position its center with respect to the Screen's coordinate system. Below are two examples, on a flat screen:



```
flat_screen = PTVR.Stimuli.Objects.FlatScreen(size_in_meters=np.array([2,1]))
flat_screen.SetSphericalCoordinates(eccentricity=0, halfMeridian=0, radialDistance=3.0)
text_object = PTVR.Stimuli.Objects.Text(text="Hello")  

flat_screen.SetObjectOnScreen(text_object, x=0, y=0 )
```

and then on a tangent screen:

```
tangent_screen = PTVR.Stimuli.Objects.FlatScreen(size_in_meters=np.array([2,1]))
tangent_screen.SetTangentRotation()  
tangent_screen.SetSphericalCoordinates(eccentricity=0, halfMeridian=0, radialDistance=3.0)
text_object = PTVR.Stimuli.Objects.Text(text="Hello")  

tangentScreen.SetObjectOnTangentScreen(text_object, eccentricity=0, halfMeridian=0, radialDistance=0 )
```

#### Links

- PTVR.Stimuli.Objects.FlatScreen.SetObjectOnScreen()
- PTVR.Stimuli.Objects.FlatScreen.SetObjectOnTangentScreen()

#### Demos

<table id="demo_table">

<tr><th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>text_on_flat_screen.py
<td>Show how to place text on flat screen and rotate the screen + text afterwards. 

<tr>
<td>text_on_flat_screen_x_height_centering.py
<td>Show that a text with a 1 deg x_height is superimposed with a flat screen whose height is 1 deg. 

<tr>
<td>text_on_tangent_screen_x_height_centering.py
<td>Show that a text with a 1 deg x_height is superimposed with a flat screen whose height is 1 deg.

<tr>
<td>text_On_tangent_screen.py
<td>Show how to place text on flat screen and rotate the screen + text afterwards.  

</table>
