The scene {#Scene}
================
# Introduction

The **Scene** is a crucial concept in PTVR !

To give a first concrete idea of its use, let's assume that we want to create an **animation** during which one cone stays static while a second cone moves away from the first cone. This is created here parsimoniously with three scenes (see figure 1).

<!-- Figures 1  -->
<table>
<tr>
<td style="width:100%;" align="center">
<img src="succession_of_scenes_animation.png" width="650pix"/>
</td>
</tr>

<tr>
<td>Figure 1: Animation made of three successive scenes: this creates **apparent motion** of the second cone with respect to the first cone.  </td>
</table>
<BR>

A **PTVR scene** is a very simple PTVR object although potentially confusing as the 'scene' concept is associated with different meanings in different fields.

The exact name of this object is **VisualScene** and it is created as shown in the code below:

     my_scene = PTVR.Stimuli.Scenes.VisualScene ()



# Links with different terminologies used in Vision Science

A **PTVR scene** is **very close** to the concept of an **image** as explained in the 'Using OpenGL' section  of the OpenGL documentation [1].

> ... the general way to use OpenGL is to draw everything you need to draw, then show this **image** with a platform-dependent buffer swapping command. If you need to update the **image**, you draw everything again, .... If you want to animate objects moving on the screen, you need a loop that constantly clears and redraws the screen. 



The **PTVR scene** is also **close** to the concept of :  
- a **slide** in a slide show (or slide presentation)  
- a **picture** as in the definition of motion picture below ([Merriam-Webster](https://www.merriam-webster.com/dictionary/motion%20picture)):

> definition of motion picture (movie) : a series of **pictures** projected on a screen in rapid succession with objects shown in successive positions slightly changed so as to produce the optical effect of a continuous picture in which the objects move.


<BR>
Beware : For those familiar with Unity, the PTVR scene is **NOT** the same as the Unity scene.
Sorry about this !   

<BR>
<div id="note">\emoji :sunglasses: **Tip**: The best way to get familiar with the PTVR VisualScene object and its use is to look at the following Demos :
</div>  



# Demos

#### Demos on the DURATION of scenes

<div id="note">\emoji :sunglasses: **Tip**: For explanations on 'simplified interactions' see [section Simplified Interactions ](@ref simplifiedInteractions)..
</div> 

The path of demos is :  
 ...\PTVR_Researchers\Python_Scripts\Demos\Scenes\

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 1_one_scene_w_simplified_duration.py 
<td> displays one scene whose duration is defined by a simplified interaction.

<tr>
<td> 2.0_one_scene_w_NON_simplified_duration.py
<td> displays one scene whose duration is not defined by a simplified interaction. It requires therefore to define an Event, an Callback and their interaction.

<tr>
<td> 3_cycle_of_scenes_w_simplified_duration.py
<td> displays a cyclic presentation of a few scenes whose duration is defined by a simplified interaction.

<tr>
<td> 4_cycle_of_scenes_w_NON_simplified_duration.py
<td> displays a cyclic presentation of a few scenes whose duration is NOT defined by a simplified interaction.

<BR>
</table>
<!-- FIN de DEMOS on the DURATION of scenes -->

#### Demos on the number of scenes per TRIAL
The path of demos is :  
 ...\PTVR_Researchers\Python_Scripts\Demos\Experiment_building\

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 10_loop_of_trials_w_one_scene_per_trial.py 
<td> The smallest experiment : each trial is made of ONE PTVR scene.

<tr>
<td> 20_loop_of_trials_w_several_scenes_per_trial.py
<td> A slightly more complex experiment : each trial is made of SEVERAL PTVR scenes.

<BR>
</table>
<!-- FIN de DEMOS on the number of scenes per TRIAL -->