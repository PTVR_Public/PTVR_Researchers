Placing objects with 2 coordinates on Screens {#placingObjectsOnScreens}
============

<BR>

<div id="note">\emoji :sunglasses: **Reminder:** If you need an extensive summary of relevant information on placing objects on 2D screens, do not forget this helpful [cheatsheet](@ref specifyPositionCheatSheet). </div>


This section is divided into the following sections:


- @subpage definitionFlatScreen
- @subpage definitionsTS
- @subpage placingObjectsOnFlatScreen
- @subpage placingObjectsOnTangentScreen


<div id="note">\emoji :sunglasses: **Tip**: when you choose the 2D position of a **symmetrical** object, this position is applied to the **center** of this object. If the object is **asymmetrical**, like a chair, then this position is applied to the **pivot** of this object</div>
