Orientation of a tangent screen {#orientationTangentScreen}
============

<!-- template_TSwSphere_CS=perimetric.ggb utilisé dans geogebra pour cette page  -->


While you can choose the POSITION of a PTVR **Tangent Screen** object, you CANNOT change its ORIENTATION once its position is defined. The orientation of a Tangent Screen is adjusted internally so that its local **X axis** is parallel to the horizontal plane (XZ) of the CURRENT coordinate system (here the global system). This is illustrated in the figures below:

In figure 1, a tangent screen is changing its position by increasing its eccentricity acros time and by keeping its Screen Origin (SO) on a constant Half-Meridian of 0°. In this case, it is easy to visualize that the horizontal axis of the tangent screen is parallel to the horizontal XZ plane.  
This latter is also easy to visualize in Figure 2 where Screen Origin (SO) follows a path of isoeccentricity (90°) by changing its half-meridian from 0° to 360°.


<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="TSwSphere_CS=perimetric_1_fig1.gif" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="TSwSphere_CS=perimetric_1_fig2.gif" width="450pix"/>  
</td>

<tr>
<td>Figure 1: Animation to illustrate how the orientation of a tangent screen is automatically adjusted to maintain its local x axis parallel to the horizontal plane (XZ) of the CURRENT coordinate system (here the global system). Here, the Screen Origin (S0) moves on a constant half-meridian (0°) by increasing its eccentricity from 0° to 180° (the orange circle is the iso-eccentricity circle corresponding to the instantaneous eccentrity of SO.  </td>
<td>Figure 2: Same goal as in figure 1, except that, here, the Screen Origin (S0) moves on an isoeccentricy circle (90° ie coinciding with the great circle about z axis) by increasing its half-meridian from 0° to 360°.   </td>
</tr>
</table>




<!-- Figures 3 et 4  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="TSwSphere_ConstantHalfMeridian.gif" width="450pix"/>
</td>

</td>
<tr>
<td>Figure 3: Same goal as in figure 1, except that, here, the Screen Origin (S0) moves on a constant half-meridian (70°) by increasing its eccentricity from 0° to 180° (the orange circle is the iso-eccentricity circle corresponding to the instantaneous eccentrity of SO.  </td>

</tr>
</table>


In Figure 3, note the two moving vertical dashed lines that should help you visualize that the horizontal axis of the tangent screen is parallel to the horizontal XZ plane (the two numbers indicate the distances between the lower corners of the tangent screen and the XZ plane).


#### Demos
---------------------
Many Demos in : 
...\PTVR_Researchers\Python_Scripts\Demos\Screens
 
<table id="demo_table">
 
<tr><th id="double_th_1"> Python file
<th id="double_th_2">Description 
 
<tr>
<td> 
...\PTVR_Researchers\Python_Scripts\Demos\Screens\
2.0.0_create_one_tangent_screen.py
<td> Create a tangent screen.
 
<tr>
<td> 
...\PTVR_Researchers\Python_Scripts\Demos\Screens\
2.2_impossible_to_rotate_tangent_screen.py
<td> Shows that you cannot rotate (i.e. change the orientation) of a tangent screen.


 
</table>