Create a tangent screen {#definitionTangentScreen}
============
<!-- template_TSwSphere_CS=perimetric.ggb utilisé dans geogebra pour cette page  -->

The **tangent screen** is a special object of PTVR (that does not exist in Unity). You will a priori use it much more often than the **[flat screen object](@ref definitionFlatScreen)**.

PTVR definition of the Tangent Screen Object
-------------------------------------------------
The 3 defining characteristics of a PTVR tangent screen are the following:  
a/ The position of a tangent screen is defined by the position of its center (called the Screen Origin - **SO**).   
(This is identical to the flat screen).

b/ A tangent screen is tangent to the sphere centered on the origin of the current coordinate system and having a diameter equal to the distance between this origin and **SO**.  
(This is not a defining characteristic of the flat screen).

c/ The orientation of a tangent screen is constrained so that its **local horizontal axis** is parallel to the horizontal plane (i.e. XZ plane) of the CURRENT coordinate system.  
(This is not a defining characteristic of the flat screen).

Placing a tangent screen in cartesian coordinates
-------------------------------------------------
The following bit of code creates a tangent screen and places it in my_scene with a position defined by the triplet of cartesian coordinates (0,0,1) (as illustrated in figure 1).
```
...
   my_scene = PTVR.Stimuli.Scenes.VisualScene ()
   my_tangent_screen = PTVR.Stimuli.Objects.TangentScreen ( position_in_current_CS = np.array([0, 0, 1]) )			
			# position : x=0, y=0, z=1 in the the GLOBAL system
   my_scene.place (my_tangent_screen, my_world) 
.... 
```


<!-- Figures 1 et 2 TSwCartesianCS.gif  -->
<table>
<tr>
<td style="width:50%; height: auto; background-color:white;" align="center">
<img src="TSwCartesianCS.png" width="550">
</td>
<td style="max-width:50%; height: auto; background-color:white;" align="center">
<img src="TSwCartesianCS.gif" width="550">  
</td>

<tr>
<td>Figure 1: Grey Tangent screen at one meter from the Origin on the Z axis. The screen center is called SO (Screen Origin).  </td>

<td>Figure 2: Animation (horizontal rotation about Y) of figure 1 to allow you to build a better mental representation of the 3D structure of the figure (as if you were able to look at it from different angles)..   </td>
</tr>
</table>
<BR>

Once the Tangent screen has been created, changing its parameter 'position_in_current_CS' has no effect (this is the same as for any object). It is therefore necessary to apply methods of the tangent screen object. For instance, it is  possible to change the cartesian coordinates with the function
set_cartesian_coordinates(). Using this function, the following bit of code creates a tangent screen and places it at the same position as the one shown in Figure 1.
```...
   my_scene = PTVR.Stimuli.Scenes.VisualScene ()
   my_tangent_screen = PTVR.Stimuli.Objects.TangentScreen ()      
   my_tangent_screen.set_cartesian_coordinates (x = 0, y = 0, z = 1) 

   my_scene.place (my_tangent_screen, my_world) 
....
```


Placing a tangent screen with perimetric coordinates
-----------------------------------------------------------------------
The following bit of code creates a tangent screen and places it in `my_scene` with perimetric coordinates.


(as illustrated in figure 3).
```
...
   my_scene = PTVR.Stimuli.Scenes.VisualScene ()
   my_tangent_screen = PTVR.Stimuli.Objects.TangentScreen () # default position (0,0,0) is not shown.
	
   my_tangent_screen.set_perimetric_coordinates (
      eccentricity= 30, 
      halfMeridian = 90, 
      radialDistance = 1)      

   my_scene.place (my_tangent_screen, my_world) 
....
```

<!-- Figures 3 et 4  -->
<table>
<tr>
<td style="width:50%; height: auto; background-color:white;" align="center">
<img src="TSwCartesianCS_ECC30_HM_90.png" width="550">
</td>
<td style="max-width:50%; height: auto; background-color:white;" align="center">
<img src="TSwCartesianCS_ECC30_HM_90.gif" width="550">  
</td>

<tr>
<td>Figure 3: Grey Tangent screen whose position is defined with perimetric coordinates (eccentricity=30, half-meridian = 90, radial distance = 1). The screen center is called SO (Screen Origin).  </td>

<td>Figure 4: Animation (horizontal rotation, i.e. about Y) of figure 3 to allow you to build a better mental representation of the 3D structure of the figure (as if you were able to look at it from different angles)..   </td>
</tr>
</table>
<BR>





When to use it ?
-----------------------
The PTVR tangent screen is often used to **place stimuli** on it [(see Placing Objects On Tangent Screens)](@ref placingObjectsOnTangentScreen).  

<BR>
Remember
-----------------------
The **orientation** of a PTVR tangent screen is constrained by its position [(see Orientation of a Tangent Screen)](@ref orientationTangentScreen).  
<BR>

Demos
-----------------------
Path of Demos:  
...\PTVR_Researchers\Python_Scripts\Demos\Screens\

<table id="demo_table">
 
<tr><th id="double_th_1">Python file
<th id="double_th_2">Description 
 
<tr>

<td>2.0.0_create_one_tangent_screen.py. 
<td> Create a tangent screen :0) 

 
</table>
<BR>
<BR>