FLAT screens : key points {#definitionFlatScreen}
============

<!-- template_TSwSphere_CS=perimetric.ggb utilisé dans geogebra pour cette page
il faut rendre invisible les axes de x_local et local_y
  -->


The **flat screen** is a special visual object of PTVR. 

This object is *a priori* less useful than the **[tangent screen](@ref definitionTangentScreen)** which is another special visual object of PTVR. 


The **position** of a flat screen is defined by the position of its center (called the Screen Origin - **SO**). See Figure 1 and the demo at the end of the section.


The **orientation** of a flat screen follows the same rules as any other visual object  [(see section 'Orientation of objects')](@ref orientationOfObjects).    

Figure 1 shows an animation where this orientation changes across time.
Although the flat screen is constantly rotating in the same direction, you might experience some bistability and perceive sometimes a change in rotation direction.


<!-- Figures 1   -->
<table>
<tr>
<td style="width:100%; height: auto; background-color:white;" align="center">
<img src="TSwSphere_CS_FLAT_screen_in_rotation.gif" width="350">
</td>
<tr>
<td>Figure 1: Grey FLAT screen rotating about the axis through Origin and SO. The screen center is called SO (Screen Origin).  </td>

</tr>
</table>
<BR>


Visual Objects can be **placed** on a flat screen [(see section 'Placing objects on FLAT Screens')](@ref placingObjectsOnFlatScreen)



#### Demos

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 
...\PTVR_Researchers\Python_Scripts\Demos\Screens\
1.0_create_flat_screens_w_translation_of_CS.py
<td> Simplest demo for creating a flat screen.

<tr>
<td> 
...\PTVR_Researchers\Python_Scripts\Demos\Screens\
spherical_array_of_flat_screens_around_current_CS_origin.py
<td> A spherical array of flat screens is placed around the current CS's origin.

<tr>
<td> 
...\PTVR_Researchers\Python_Scripts\Demos\Orientations\
flat_screen_looking_at_hand_controller.py
<td> A flat screen follows you moving hand.



<BR>
</table>
<!-- FIN de DEMOS -->