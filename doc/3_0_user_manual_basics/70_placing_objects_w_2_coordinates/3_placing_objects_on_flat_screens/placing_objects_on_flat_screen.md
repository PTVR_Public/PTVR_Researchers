Placing objects on FLAT Screens {#placingObjectsOnFlatScreen}
===============


The definition of a **flat screen** was already presented [here.](@ref definitionFlatScreen)

The **main principle** is that you can use special functions to place objects with **coordinates that are LOCAL** (i.e. they are defined with respect to the screen coordinate system).


You can place objects on a flat screen as shown in the demo at the end of this section.  
HOWEVER, **it is only possible to use a cartesian coordinate system.**


In the PTVR code, you must first create and place a flat screen object,  
and then, and only then, ...  
you can place objects on it.




#### Links
[See cheatsheet on 'position of objects"](@ref specifyPositionCheatSheet)



#### Demos
Main demos are in:
...\PTVR_Researchers\Python_Scripts\DEMOS\Screens\

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 1.3_place_objects_on_flat_screen.py 
<td> Does what it says..


<BR>
</table>
