Placing objects on TANGENT screens {#placingObjectsOnTangentScreen}
============

The PTVR definition of a **tangent screen** was already presented [here.](@ref definitionOfTangentPlaneInVS)


The principle of placing objects on TANGENT screens was already presented with animations in the section on [the tangent screen as a local coordinate system](@ref exTangentScreenAsLocalCS).

A more quantitative illustration is presented here in figures 1 and 2 by superimposing a cartesian AND a perimetric coordinate system in the same figure.  
A point P lying on a sphere is projected on a tangent screen thus creating a point called **Projection of P**.


<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Spherical_Coordinates+projection_on_frontal_plane.png" width="550pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Spherical_Coordinates+projection_on_frontal_plane.gif" 
width="550pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: joint use of the cartesian and perimetric coordinates to specify the position of the point called 'projection of P'. </td>

<td>Figure 2: Animation (horizontal rotation) of figure 1 to allow you to build a better mental representation of the 3D structure of the figure (as if you were able to look at figure 1 from different angles). </td>
</tr>
</table>
<BR>

To easily create this point **projection of P** on this tangent screen, it is helpful to use the special PTVR functions that create  objects with **coordinates that are LOCAL** to the screen, i.e. that are defined with **two coordinates** in the screen coordinate system.  
You have currently two options to specify these **2D coordinates**:  
- in **cartesian** coordinates  (with parameters x and y in **meter** units )
or  
- in **perimetric** coordinates (with parameters eccentricity and half_meridian parameters in **degree** units)

This is illustrated in the code below. Note that, in the PTVR code, you must first create and place a tangent screen object,  
and then, and only then, ...  
you can place objects on it.
    
For didactic reasons, in the code below, the arguments passed to the parameters correspond to the values used in figures 1 and 2, although the scale is not respected in the figures.  


    # step 1  : create a tangent screen
	tangent_screen_#1 = PTVR.Stimuli.Scenes.TangentScreen ( np.array ( [0, 0, 7] )  ) 	# note that z is set to 7 as in the figure 
    my_scene.place (tangent_screen_#1, my_world) 

    # step 2  : place objects on tangent screen : ...

	# ... either in cartesian coordinates
    my_object = PTVR.Stimuli.Objects.Cube ( ... )    
    my_object.set_cartesian_coordinates_on_screen (my_2D_screen = tangent_screen_#1, x_local = 4, y_local = 3) 

	# ... or in perimetric coordinates    
	my_object_2 = PTVR.Stimuli.Objects.Sphere ( ... )    
    my_object_2.set_perimetric_coordinates_on_screen (tangentscreen = tangent_screen_#1, eccentricity_local_deg = 38, half_meridian_local_deg = 36)


<BR>

<div id="note">\emoji :scream: **EMPHASIS:** To reiterate : A tangent screen **MUST BE PLACED BEFORE** the objects lying on it are placed.</div>


#### Links
[See cheatsheet on 'position of objects"](@ref specifyPositionCheatSheet)


#### Demos

Main demos are in:
...\PTVR_Researchers\Python_Scripts\DEMOS\Screens\

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 2.3_place_objects_on_tangent_screen.py 
<td> Does what it says..


<BR>
</table>
