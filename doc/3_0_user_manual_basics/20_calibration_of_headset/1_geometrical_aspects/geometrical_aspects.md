Geometrical aspects of VR calibration {#geometricalAspectsOfRoomCalibration} 
============

#### Goal of the VR Calibration
<span style="font-size:130%;">
The goal of the calibration process is to create the [GLOBAL (aka WORLD) coordinate sytem](@ref globalAndLocalCoordinateSystems).</span>

The calibration of the VR system aims at **tracking** the relevant VR equipments, notably the **headset** and **controllers**, when they move **within your real room**. <BR> 


<div id="note">\emoji :wink: **Remark:** PTVR has been tested only with HTC Vive Pro devices (more devices will be tested in the future). You will find instructions to perform calibration of these devices on their web site [here : planning your play area for the Vive System](https://www.vive.com/us/support/vive/category_howto/planning-your-play-area.html)
</div>

It is important to understand the geometrical aspects of the VR calibration to use the coordinate systems of PTVR.  
The Calibration process has **two goals** :  
**a/** to create in the virtual environment a Y axis defined by a **scale** (in meters) matching the scale in the real world and by an **origin** lying at the height of the user's real floor. (this is usually achieved by measuring the height above the floor of the headset placed on a surface such as a table).  
**b/** to create a Z axis forming a YZ plane that contains the **local forward direction** of the headset **AT THE MOMENT OF the calibration**.

<div id="note">\emoji :sunglasses: **Emphasis**: The Coordinate System created by this calibration is the **GLOBAL Coordinate System**  </div>


Two different calibration modes are possible depending on whether or not the user will move within a surface of a few square meters during the experiment per se:  
- mode 1: the user does not intend to move and therefore will be seated or standing.  
- mode 2: the user will walk and move within a room.  

 
#### Mode 1: Calibrating a seating- or standing-only play area 
&emsp;This mode implies that the user will be either seated or standing at a constant location during the whole course of an experiment.
[(See "Setting up a standing-only play area" for the Vive System)](https://www.vive.com/us/support/vive/category_howto/setting-up-standing-only-play-area.html)

Note that the figures below correspond to the **seated** situation. In the standing situation, the only change is that the feet would be right ON the Origin position instead of being slightly forward (and of course the head would be higher than in the seated situation).




#### &emsp; 1.1 &emsp; Headset oriented towards an object in the real world
<!-- YOUR_COMMENTS_HERE -->
<!-- j'ai testé la différence entre % et pix et je préfère les pix car ça change tout quand on fait un zoom DANS le NAVIGATEUR: on peut arriver à voir plus de figures ensemble ! !   -->

<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead+silhouette_assise_1.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead.gif" 
width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: Illustration of the calibration process for a seated subject oriented towards a cylinder in the real world: calibration aligns the virtual Z axis (blue) with the vertical plane (beige) containing the local forward direction of the headset. In this example, the vertical plane contains the position of a cylinder in the real world. </td>

<td>Figure  2: Same image as figure 1 with an animation of the headset orientation WITHIN the YZ plane (i.e. about the X axis): any of these orientations in the real world at the moment of the calibration will result in this particular XYZ coordinate system (see figures below for another XYZ system with respect to the real world) </td>
</tr>
</table>
<BR>

<!-- Figure 3  -->
<img src="calibration_with_fixed_object_ahead_with_rotation.gif" width="450" style="float:left; padding-right:20px" />  

<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

<!--FIN de  Figure 3  -->
**Figure 3**: Animation (horizontal rotation) of figure 2 (without the cylinder) to allow you to build a better mental representation of the 3D structure of the figure (as if you were able to look at this figure from different angles). 

<BR>

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->


<div id="note">\emoji :wink: **Remark:** In the figures of the present page, the only real-world 'objects' (i.e. that are not represented in the virtual world) are the cylinder, the headset and the person's whole body ([more explanations here](@ref importantNoteOnRoomCalibration)).
</div>
<BR>

#### &emsp; 1.2 &emsp;  Headset oriented away from an object in the real world
The following example is the same as the one presented above except that the subject is seated in a different direction with respect to the cylinder in the real world. This creates a virtual XYZ coordinate system that is identical to the previous one except for a rotation with respect to the cylinder (here 36° to the left of the cylinder).

<!-- Figures 4 et 5  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_to_the_right+silhouette_assise.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src= "calibration_with_fixed_object_to_the_right.gif" width="450pix"/>
</td>
</tr>
<tr>
<td>Figure 4: Illustration of the calibration process for a seated subject oriented away from an object in the real world: the Z axis (blue) is aligned with the vertical plane (beige) containing the forward direction of the headset. In contrast to figure 1, this vertical plane is now 36° to the left of the real world cylinder.</td>

<td>Figure 5: Same image as figure 3 with an animation of the headset orientation WITHIN the YZ plane (i.e. about the X axis): any of these orientations in the real world at the moment of the calibration will result in this particular XYZ coordinate system</td>
</tr>
</table>



#### Mode 2: Calibrating a room-scale play area ##

In the "room-scale" mode, the user can move within an area of a few square-meters
([See "Setting up a room-scale play area" for the Vive System](https://www.vive.com/us/support/vive/category_howto/setting-up-room-scale-play-area.html)).

Although this mode can be used with PTVR, the current documentation illustrates only cases in which subjects are seated or standing at a fixed position in the real world (i.e. mode 1).
<BR><BR>