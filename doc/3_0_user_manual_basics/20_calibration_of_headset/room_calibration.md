Calibration of VR system in the real-world room {#calibrationOfHeadset} 
============
<span style="font-size:160%;">
The goal of the VR system calibration process is to create the [GLOBAL (aka WORLD) coordinate system)](@ref globalAndLocalCoordinateSystems).</span>


<BR>

The present section is divided into the following sub-sections:  

- @subpage geometricalAspectsOfRoomCalibration  
- @subpage DisplayingTheGlobalCoordinateSystem  
- @subpage linkBetweenRealAndVirtual  
