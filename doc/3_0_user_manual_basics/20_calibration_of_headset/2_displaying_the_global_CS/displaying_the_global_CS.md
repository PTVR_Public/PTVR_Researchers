Diplaying the global CS {#DisplayingTheGlobalCoordinateSystem} 
========================
<!-- On ne voit pas la figure 1 (cartesian_CS_RUF.png) car elle est dans un autre dossier que le dossier courant-->

<div id="note">\emoji :wink: **Reminder and Emphasis:** The goal of the VR calibration process is to construct the **GLOBAL coordinate system** [(see here)](@ref globalAndLocalCoordinateSystems).</div>
	
<div id="note">\emoji :wink: **Implication:**  As long as you do NOT create new coordinate systems in your script [(see here)](@ref coordinateTransformations), the coordinates you will be using to place objects in the virtual world will refer to the **GLOBAL coordinate system**.</div>

#### Visualization in the PTVR documentation 

In the whole PTVR documentation, except otherwise stated and whenever possible, the **GLOBAL COORDINATE SYSTEM** is displayed on your 2D monitor with the [RUF display](@ref cartesianCoordinateSystemInVS) format shown in Figure 1 : i.e. the X axis pointing to the right, the Y axis to the top and **the Z axis pointing at a point "behind" the plane of your monitor's screen (receding axis)**.  


<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="cartesian_CS_RUF.png" width="550pix"/>
</td>
<td style="width:50%;" align="center">
<img src="cartesianCoordinateSystemInUserManual_3D.png" 
width="550pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: Standard way of displaying the **GLOBAL COORDINATE SYSTEM** in the PTVR documentation. </td>

<td>Figure  2: Simple example of a Point P whose coordinates in the **GLOBAL COORDINATE SYSTEM** have been specified by PTVR code #1.</td>
</tr>
</table>

To emphasize again, right after a Room calibration, the **current** coordinate system is the **Global**coordinate system. If you then create an object, its coordinates will thus be [GLOBAL coordinates](@ref globalAndLocalCoordinateSystems) - see **Implication** above.  
In PTVR code #1 below, the parameter **position_in_current_CS** means that argument values  (here the array (x=0, y=1.5, z=1.2)) will correspond to **coordinates defined in the CURRENT coordinate system**. As the current coordinate system is still the [GLOBAL coordinate sytem](@ref globalAndLocalCoordinateSystems) - see **Implication** above, these values will thus correspond to coordinates defined in the GLOBAL coordinate system. The corresponding point's position is shown in figure 2 .


#### PTVR code #1

    my_point_P = PTVR.Stimuli.Objects.Sphere (position_in_current_CS = np.array ( [0.0, 1.5, 1.2] ) )

<BR>

#### Take-home messsages

<div id="note">\emoji :scream: **Warning:** 
In many figures of the PTVR documentation, the **GLOBAL COORDINATE SYSTEM** is oriented as in Figures 1 and 2. This ubiquitous representation is a mnemonic technique that will hopefully aid users in **memorizing the geometry** of the PTVR virtual world.
</div>

<div id="note">\emoji :scream: **Warning:** 
Except otherwise stated, a representation similar to that in Figures 1 or 2 is not meant to provide any information on the spatial mapping between the real world and the virtual world.
</div>


#### Demos

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>10_global_cartesian_coordinate_system.py 
<td>Displays the GLOBAL COORDINATE SYSTEM in your VR headset with the same format as that used in PTVR documentation.
</table>.

<BR><BR>