Link between Real and Virtual Worlds {#linkBetweenRealAndVirtual} 
============

#### Displaying the REAL and VIRTUAL worlds

If the PTVR documentation needs to help you distinguish between the virtual world AND real world (as was already the case in [section: room calibration](@ref geometricalAspectsOfRoomCalibration), this will always be explicitely mentioned in the text. Figure 1 shows such a case where  the virtual/real geometrical relationship is explicitely represented. In this example, Figure 1 summarizes the three following points: a/ the **room calibration** process has been performed with the seated subject's body being aligned with a real cylinder that is physically present in the room where he/she is physically seated; and b/ the resulting **GLOBAL COORDINATE SYSTEM** is thus oriented so that Z points to this cylinder; and c/ a point P has been created in the virtual world.

<div id="note">\emoji :scream: **Warning:** In figures 1 and 2, the subject sees the point P in his/her VR headset but he/she does not see the real world cylinder. </div>

Similarly, Figure 2 shows explicitely a similar situation to that represented in Figure 1 **except for one point**: here, the real orientation of the seated subject in the real world AT THE TIME OF ROOM CALIBRATION was to the left (by 36°) of the real cylinder.

In sum, whatever the position/orientation of the subject's body in the **real room**, for instance facing the real world cylinder (figure 1), or being oriented away from the real world cylinder (Figure 2), it is usually more convenient to create documentation figures that **ignore** the real world. 


This was the case in [Figure 2 of the previous section](@ref DisplayingTheGlobalCoordinateSystem) that only depicted the virtual world with the [Global coordinate system in RUF format](@ref cartesianCoordinateSystemInVS). In this case, the goal was only to describe what a subject will see in his/her virtual wolrd but not whether the subject will bump into the real word cylinder if he/she stands up and walks.

 

<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead+bonhomme+Point_P.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_to_the_right+bonhomme+Point_P.png" 
width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: 
This figure aims at representing both the virtual world's **GLOBAL COORDINATE SYSTEM** AND the real world (symbolized here by a cylinder). 
From this figure, you can predict for instance that if the subject stands up and walk in the direction of the virtual point P, he/she will bump into the real world cylinder that is not visible in his/her virtual world.</td>

<td>Figure  2: Same aim as in Figure 1 with a different orientation of the subject at the time of the room calibration. Here, the **room calibration** has been performed with the subject oriented away from the real world cylinder (the **GLOBAL Coordinate System** Z axis is thus not aligned any longer with the real world cylinder). In this case, if we assume that the figure has displayed all the objects of the real world within a circle radius of say 3 meters), then we can make the following prediction: if the subject stands up and walk in the direction of the virtual point P (not more than 3 meters), he/she will NOT bump into any real object. </td>
</tr>
</table>
<BR>


#### Demos

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>10_global_cartesian_coordinate_system.py 
<td>Displays the GLOBAL COORDINATE SYSTEM in your VR headset with the same format as that used in PTVR documentation.
</table>.

<BR><BR>