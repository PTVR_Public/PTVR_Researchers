Translation vector specified in CURRENT CS {#shiftingTheOriginOfRoomCalibrationCS} 
==================

<!-- Names of figure files:
Pour Fig 1:
DAns DOSSIER \transformations\
calibration_with_fixed_object_ahead_origin_sPOV_shift_origin_current.png

Pour fig 2:
calibration_with_fixed_object_ahead_origin_sPOV_shift_origin_current.gif
 -->

<span style="font-size:150%;"> This page shows how to transform coordinates with a translation vector whose coordinates are specified in the **CURRENT** coordinate system..</span>

To illustrate this kind of translation, a simple example in 3 steps is used:


**Step 1 :** At the beginning of the example, the current coordinate system (shown in figure 1) is NOT the one that we will use to draw our next stimuli. We actually want to translate/shift this system by the vector joining the current origin and a point (sphere) S already specified in this system with coordinates (0, 0, 0.5).  This joining vector is therefore trivially specified in the current system by the coordinates (0, 0, 0.5). Let's call this vector V.  
**Step 2:** The vector V is used by a specific PTVR function to create a new **translated** system shown in figure 3. 
In other words, the coordinate transformation amounts to a translation by the vector V, i.e. along the current Z axis.  
**Step 3:** a point (sphere) T is created with the coordinates (0, 0, 0.5) in this new translated coordinate system. We note that the **local** coordinates (0, 0, 0.5) of S and T are identical within their respective coordinate systems although their **global** coordinates are different. 

These steps are detailed below:  

Step 1
----------
The CURRENT coordinate system in step 1 is shown in figure 1 by the three unit vectors called Current X, Current Y and Current Z. 
The triplet of coordinates (0, 0, 0.5) in this system specifies a point S as achieved with the following bit of code.

```
def main():  	
   exp = PTVR.Experiment.Experiment ()
   ... (code for coordinate transformation not shown)

   my_pink_point_S = PTVR.Stimuli.Objects.Sphere ( position_in_current_CS = pink_point_coordinates, 
				color = color.RGBColor (r=1, g=0, b=1), size_in_meters = 0.1 ) 
   my_scene.place (my_pink_point_S, my_world) 
...
```

Notes about the code above: 

- The current system during step 1 is NOT the global coordinate system because several coordinate transformations (not shown) have been performed before.
- Remember that the **position** parameter needs a **3-dimension np.array (i.e. a triplet)** that is interpreted as x, y and z **cartesian coordinates** ([see Position in 3D cartesian coordinates](@ref in3DcartesianCS)) in the CURRENT system.

<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_shift_origin_current.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_shift_origin_current.gif"  width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: During step 1, the CURRENT coordinate system consists of the three unit vectors called Current X, Current Y and Current Z. The coordinates (0, 0, 0.5) in this system specify the point S.

 </td>
<td>Figure 2: Same as in figure 1 with a horizontal rotation about global Y to allow a better mental representation of the 3D structure of the figure. </td>
</tr>
</table>
<BR>
<!-- FIN DE Figures 1 et 2  -->

Step 2  
----------
The PTVR code below shows how to create a new coordinate system by **translating/shifting** the coordinate system of step 1 (the shift corresponds to the vector V joining the current origin and the point S shown in figure 1).
```
translation_defined_in_current_CS = np.array ( [0, 0, 1 ] ) 

def main():
   my_world = PTVR.Visual.The3DWorld ()
   ...
   my_world.translate_coordinate_system_along_current (translation_defined_in_current_CS)  # current Origin is now at the position of S
...
```

The new coordinate system resulting from the transformation in step 2 is shown in figure 3.

<!-- Figures 3 et 4  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_shift_origin_NEW.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_shift_origin_NEW.gif"  width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 3: The CURRENT coordinate system is now a translation of the coordinate system shown in figure 1.  The coordinates (0, 0, 0.5) in this system specify the location of point T.
 </td>
<td>Figure 4: Same as in figure 3 with a horizontal rotation about global Y to allow a better mental representation of the 3D structure of the figure. </td>
</tr>
</table>
<BR>
<!-- FIN DE Figures 3 et 4  -->


Step 3
---------
The **CURRENT** coordinate system is now the system created by the coordinate translation  in step 2. In this current system, the triplet of coordinates (0, 0, 0.5) specifies the location of point T (see figure 3).  
The point T is created by adding the following line of code after having translated the system in step 2. 
```
...
   my_pink_point_T = PTVR.Stimuli.Objects.Sphere ( position_in_current_CS = pink_point_coordinates, 
				color  = color.RGBColor ( r=1, g=0, b=1), size_in_meters = 0.1 )         
   my_scene.place (my_pink_point_T, my_world) 
```

Links
---------------
PTVR.Stimuli.World.Scene.translate_coordinate_system_along_current ()

Wikipedia contributors, 'Translation of axes', Wikipedia, The Free Encyclopedia, 11 October 2021, 06:20 UTC, <https://en.wikipedia.org/w/index.php?title=Translation_of_axes&oldid=1049331106> [accessed 17 March 2022] 




Demos
-------------------
Some demos in:  
...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Screens
<table id="demo_table">
<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>10_global_cartesian_coordinate_system.py 
<td>Basic demo for PTVR beginners. Displays the Global coordinate system with its colored axes (red X, green Y, blue Z).







