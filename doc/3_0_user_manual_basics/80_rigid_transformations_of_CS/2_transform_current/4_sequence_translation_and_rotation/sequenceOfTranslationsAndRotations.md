Translations + Rotations {#sequenceOfTranslationsAndRotations}
==================

<!-- Names of figure files:
Pour Fig 1:
calibration_with_fixed_object_ahead+reduced_Wall.ggb en enlevant la layer 9
calibration_with_fixed_object_ahead+reduced_Wall+point_P.png
Pour fig 2:
calibration_with_fixed_object_ahead_origin_sPOV_rotation.ggb
-->

TODOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO 


#### One translation and one rotation


<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead+reduced_Wall+Point_P.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="
calibration_with_fixed_object_ahead_origin_sPOV+Point_M.png"  width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: Situation right after the room calibration process. This Coordinate System is called the **Room Calibration Coordinate system** throughout the PTVR documentation.

 </td>
<td>Figure 2: Situation after the **Room Calibration Coordinate system** has been shifted upwards by 1.2 meters. The virtual YZ plane is not displayed for clarity. </td>
</tr>
</table>
<!-- FIN DE Figures 1 et 2  -->
<BR>



<!-- Figures 3 et 4  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_rotation+translation.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_rotation+translation.gif"  width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 3: Situation after the **Room Calibration Coordinate system** has been shifted upwards by 1.2 meters **and rotated by 45°**.

 </td>
<td>Figure 4: Animation showing all the possible rotations of the coordinate system for a given upwards translation (here 1.2 meters as in Fig. 2 and 3). </td>
</tr>
</table>
<!-- FIN DE Figures 3 et 4  -->
<BR>
