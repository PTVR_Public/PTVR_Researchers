Several rotations {#sequenceOfRotations}
==================

<!-- Names of figure files:
TODOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
 -->
**Goal:**  To illustrate how to code **two** successive rotations of the coordinate system.

<!-- Names of figure files:
calibration_with_fixed_object_ahead_origin_sPOV_rotation.ggb
Pour faire descendre l'origine, on le fait à la main dans les coordonnées
du point newOrigin.
-->


**Code to create a Sphere P in the Room Calibration coordinate system (Figure 1)**  
Note that PTVR.Stimuli.Objects.Sphere() fonction is NOT called **at the beginning of the main() fonction**. This is because some coordinate transformation (not shown in the code) has already taken place earlier in the code. Therefore the [**current** coordinate system](@ref coordinateTransformations) shown in Figure 1 is NOT the [Room Calibration coordinate system](@ref calibrationOfHeadset). A sphere P (0, 0, 1.2) is created within this current system (Figure 1).

```
def main():
 	# some code a few lines above has already transformed the room calibration coordinate system into the current coordinate system shown in Figure 1.

	# Time T1
   	PTVR.Stimuli.Objects.Sphere (position_in_current_CS = np.array ( [0, 0, 1.2] ) ) # creates a sphere

```

**Subsequent Code to create a Sphere M after a rotation of +30° about Y : first rotation (Figures 2 and 3)**  
A rotation (+30° about Y as represented in motion in Figure 2) is applied to the coordinate system shown in Figure 1 so that the current system is now the one represented in Figure 3. 
Thereafter, in the code, a Sphere M having the same coordinates as P in Figure 1 (i.e. the same triplet (0, 0, 1.2) is created within the current coordinate system as shown in Figure 3.

```
   my_world.rotate_coordinate_system_about_Y (+30)    # Note that a single value is passed to this fonction

   # Time T2 (see figure 3)
   PTVR.Stimuli.Objects.Sphere (position_in_current_CS = np.array ( [0, 0, 1.2] ) ) 

```


<!-- Figures 1 et 2 et 3 -->
<table>
<tr>
<td style="width:33%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_rotation_350_Deg.png" width="450pix"/>
</td>

<td style="width:33%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_rotation_1st_rotation.gif"  width="450pix"/>  
</td>

<td style="width:33%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_rotation_320_Deg.png"  width="450pix"/>  
</td>

</tr> 

<tr>
<td>Figure 1. **Current** system at time **T1**: a point P (0, 0, 1.2) is created in the **current** coordinate system (note that the current coordinate system is not the same as the [Room Calibration Coordinate system](@ref calibrationOfHeadset)).
</td>

<td>Figure 2.  Animation showing the rotation (**about current Y** by +30°) applied to the coordinate system in figure 1 to obtain the coordinate system in figure 3.   
This is the **first rotation**.
</td>

<td>Figure 3. **Current** system at time **T2**: this system has been obtained after a rotation by +30° about Y has been applied to the coordinate system in Figure 1. Note the position of sphere M.
</td>

</tr>
</table>
<!-- FIN DE Figures 1 et 2  et 3 -->


<!-- Figures 4, 5  -->
<table>
<tr>
<td style="width:33%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_rotation_2nd_rotation.gif" width="450pix"/>
</td>

<td style="width:33%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_rotation_about_current_X.png"  width="450pix"/>  
</td>
</tr> 

<tr>
<td>Figure 4: Animation showing the rotation (**about current X** by -45°) applied to the coordinate system in figure 3 to obtain the coordinate system in figure 5.  
 This is the **second rotation**.
</td>

<td>Figure 5:  **Current** system at time **T3**: this system is obtained after a rotation (by -45° **about current X**) has been applied to the coordinate system in Figure 3. Note the position of sphere S.
</td>

</tr>
</table>
<!-- FIN DE Figures  4, 5 -->
<BR>

**Subsequent code to create a Sphere S after a rotation of -45° about current X : second rotation (Figures 4 and 5)** 
A rotation (-45° about x as represented in motion in Figure 4) is applied to the coordinate system shown in Figure 3 so that the current system is now the one represented in Figure 5. Thereafter, in the code, a Sphere S having the same local coordinates as P in Figure 1 (i.e. the same triplet (0, 0, 1.2) is created within the current coordinate system as shown in Figure 5.

```...
   my_world.rotate_coordinate_system_about_X (-45)    # Note that a single value is passed to this fonction

   # Time T3 (see figure 5)
   PTVR.Stimuli.Objects.Sphere (position_in_current_CS = np.array ( [0, 0, 1.2] ) ) 
...
```
