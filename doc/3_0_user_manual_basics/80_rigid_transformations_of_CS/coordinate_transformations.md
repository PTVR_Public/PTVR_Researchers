Placing objects after coordinate transformations {#coordinateTransformations}
==================
Goal
------------
**It is often very convenient for scientists to apply some coordinate transformations before placing objects in 3D.**
<BR><BR>

<img src="Lorentz2.jpg" width="320" height="211" style="float:left; padding-right:20px" >
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

However, coordinate transformations can be sometimes **painful** for non-mathematicians. PTVR offers some features whose aim is to **reduce such pain** for several common experimental situations as detailed in the present page.


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->


**Figure 1**:  '**Simple** derivation of the Lorentz transformation by improvement of the **intuitive** Voigt transformation'. (\emoji :wink:Verbatim from <a href="https://commons.wikimedia.org/wiki/File:Lorentz2.jpg">Mattcomm</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons).
<BR><BR>

Translations and Rotations
--------------------------------
**PTVR provides functions to easily perform translations and rotations of coordinate systems** in the following way (translations and rotations are part of rigid transformations - see Definitions below):


At any moment in a PTVR script, there is a **CURRENT** coordinate system that is **used by all  subsequent relevant fonctions**.  This **CURRENT** coordinate system (say CS1) is used until a new rigid transformation is applied, thus leading to a **new** coordinate system (say CS2) that becomes the **current** coordinate system..

At the **beginning** of the script, the **CURRENT** coordinate system is the [GLOBAL coordinate system (aka World coordinate system)](@ref globalAndLocalCoordinateSystems). 
<BR>


<div id="note">\emoji :sunglasses: **Tip**: you can come back to the **GLOBAL** coordinate system at any moment with : **my_experiment.reset_coordinate_system()**
 </div>

<BR>
PTVR functions to apply these coordinate transformations
-------------------------------------------------------------
There are two kinds of PTVR functions to create a **NEW coordinate system** as detailed in the two following sub-sections:  

1/ functions that use the [GLOBAL coordinate system (aka World coordinate system)](@ref globalAndLocalCoordinateSystems) to define the coordinates of the transformation vector (translation or rotation) and to apply the transformation.     
- @subpage transformGlobalCS   

2/ functions that use the  **CURRENT** coordinate system to define the coordinates of the transformation vector (translation or rotation) and to apply the transformation.  
- @subpage transformCurrentCS  


<BR>
<div id="note">\emoji :sunglasses: **Reminder:** If you need an extensive summary of relevant information on coordinate transformations, do not forget the helpful ['coordinate transformations' cheatsheet](@ref coordinateTransformationsCheatSheet). </div>





<BR><BR>
Links
--------------------------
- [Great WebSite by Joey de Vries:](https://learnopengl.com/Getting-started/Coordinate-Systems) : the goal of this site is to teach OpenGL but it is so well done and didactic that you can use it as a more general resource to learn 3D geometry in computer graphics.

- Wikipedia contributors, '**Rigid transformation**', Wikipedia, The Free Encyclopedia, 4 December 2021, 08:06 UTC, <https://en.wikipedia.org/w/index.php?title=Rigid_transformation&oldid=1058556511> [accessed 10 May 2022] 

- Section '**Transformations**' in:  
Wikipedia contributors, '**Coordinate system**', Wikipedia, The Free Encyclopedia, 18 March 2022, 15:49 UTC, <https://en.wikipedia.org/w/index.php?title=Coordinate_system&oldid=1077860317> [accessed 10 May 2022] 

- Wikipedia contributors, '**Translation (geometry)**', Wikipedia, The Free Encyclopedia, 14 January 2022, 08:25 UTC, <https://en.wikipedia.org/w/index.php?title=Translation_(geometry)&oldid=1065586209> [accessed 10 May 2022] 

- Wikipedia contributors, '**Rotation of axes**', Wikipedia, The Free Encyclopedia, 22 November 2021, 22:47 UTC, <https://en.wikipedia.org/w/index.php?title=Rotation_of_axes&oldid=1056635843> [accessed 10 May 2022] 

- Wikipedia contributors, 'List of common coordinate transformations', Wikipedia, The Free Encyclopedia, 12 March 2022, 18:19 UTC, <https://en.wikipedia.org/w/index.php?title=List_of_common_coordinate_transformations&oldid=1076744186> [accessed 16 March 2022]
<BR>

<BR>
Definitions
---------------------------
> ... **rigid transformations** include **rotations**, **translations**, reflections, or any sequence of these. Reflections are sometimes excluded from the definition of a rigid transformation by requiring that the transformation also preserve the [handedness of objects](@ref cartesianCoordinateSystemInVS) in the Euclidean space. (A reflection would not preserve handedness; for instance, it would transform a left hand into a right hand.) ....  
> Any object will keep the **same shape and size** after a proper rigid transformation.  
> [(Wikipedia contributors, '**Rigid transformation**')](https://en.wikipedia.org/w/index.php?title=Rigid_transformation&oldid=1058556511). 
<BR>


<!-- Figures 2 et 3  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Traslazione_OK.svg.png" width="70pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Rotation_of_coordinates.svg.png"  width="150pix"/>  
</td>
</tr>
<tr>
<td>Figure 2: A translation moves every point of a figure or a space by the same amount in a given direction. Image from <a href="https://commons.wikimedia.org/wiki/File:Traslazione_OK.svg">Fred the Oyster</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons.

 </td>
<td>Figure 3: An xy-Cartesian coordinate system rotated through an angle theta to an x′y′-Cartesian coordinate system. Image from <a href="https://commons.wikimedia.org/wiki/File:Rotation_of_coordinates.svg">Guy vandegrift</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, via Wikimedia Commons. </td>
</tr>
</table>

<!-- FIN DE Figures 1 et 2  -->
<BR>

Demos
-----------------------
Several scripts in:
PTVR_Researchers\Python_Scripts\Demos\Coordinate_Systems\  

Several scripts in:
PTVR_Researchers\Python_Scripts\Demos\Screens\