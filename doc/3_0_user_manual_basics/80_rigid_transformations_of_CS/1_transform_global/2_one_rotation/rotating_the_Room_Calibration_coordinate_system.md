A single rotation {#rotatingTheRoomCalibrationCSGlobal}
==================

Single Rotation from the GLOBAL  coordinate system
-------------------------------------------------------

**Goals:**   
**step 1:** create a Sphere P in the Room Calibration coordinate system  
**step 2:** rotate the [current coordinate system](@ref coordinateTransformations) (here the Room Calibration coordinate system)   
**step 3:** draw a sphere M in the current coordinate system (which is not any longer the Room Calibration coordinate system).  

**Note on terminology** : P And M have the same **local** position with respect to their current coordinate system (i.e. at the time of creation). However, due to the rotation of the coordinate system, they have different coordinates in the **global** coordinate system.

<!-- Names of figure files:
Pour Fig 1:
calibration_with_fixed_object_ahead_origin_sPOV_rotation.ggb en enlevant la layer 9
calibration_with_fixed_object_ahead_origin_sPOV_rotation.png
Pour fig 2:
calibration_with_fixed_object_ahead_origin_sPOV_rotation.ggb
Pour faire descendre l'origine, on le fait à la main dans les coordonnées
du point newOrigin.

-->


**Code to create a Sphere P in the Room Calibration coordinate system (Figure 1)**  
Note that the PTVR.Stimuli.Objects.Sphere() fonction is called **at the beginning of the main() fonction**. At this stage of the script, the **[CURRENT COORDINATE SYSTEM](@ref coordinateTransformations)** is the [Room Calibration coordinate system](@ref importantNoteOnRoomCalibration) (aka Global or Main or World coordinate system). A sphere P (0, 0, 1.2) is thus created within this system as shown in Figure 1.

```
def main():
   # Time T1 (see figure 1)
   my_point_P = PTVR.Stimuli.Objects.Sphere (position_in_current_CS = np.array ( [0, 0, 1.2] ) ) # creates a sphere
```

<!-- Figures 1 et 2 et 3 -->
<table>
<tr>
<td style="width:33%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_rotationNO.png" width="450pix"/>
</td>

<td style="width:33%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_rotation_from_0_to_40.gif"  width="450pix"/>  
</td>

<td style="width:33%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_rotation40Deg.png"  width="450pix"/>  
</td>

</tr> 

<tr>
<td>Figure 1. **Current** system at time **T1**: Situation right after the room calibration process : the **current** coordinate system at this stage is the [Room Calibration Coordinate system](@ref calibrationOfHeadset). Note the position of the sphere P.
</td>

<td>Figure 2. Animation showing the rotation (about Y by -40°) applied to the coordinate system in figure 1 to obtain the coordinate system in figure 3. 
</td>

<td>Figure 3. **Current** system at time **T2**: this system is obtained after a rotation by -40° about Y has been applied to the coordinate system in Figure 1. Note the position of sphere M.
</td>

</tr>
</table>
<!-- FIN DE Figures 1 et 2  et 3 -->
<BR>


**Code to create a Sphere M after the Room Calibration coordinate system has been rotated (Figures 2 and 3)**  
A rotation (-40° about Y as represented in Figure 2) is applied to the Room Calibration coordinate system (as the latter is the CURRENT coordinate system) so that the current system is now the one represented in Figure 3.  
Thereafter, in the code, a Sphere M having the same coordinates as P in Figure 1 (i.e. the same triplet (0, 0, 1.2) is created within the current coordinate system as shown in Figure 3.

<div id="note">\emoji :wink: **Remark:** Note the trivial fact that P and M have different positions in the virtual World (i.e. in the global coordinate system).</div>

```
def main():
...
   my_world.rotate_coordinate_system_about_Y (-40)    # Note that a single value is passed to this fonction

   # Time T2 (see figure 3)
   my_point_M = PTVR.Stimuli.Objects.Sphere (position_in_current_CS = np.array ( [0, 0, 1.2] ) ) 
...
```



<div id="note">\emoji :wink: **Remember:** A negative rotation is Counterclockwise in a left-handed coordinate system (RUF in the PTVR documentation). (see [orientationVisualObjectsInVisionScience "see in section Orientation of Objects"](@ref orientationVisualObjectsInVisionScience))</div>


Links
---------------
- PTVR.Visual.The3DWorld.rotate_room_calibration_coordinate_system_about_X ()  
Il faudra changer le nom ci-dessus comme ci-dessous:
- PTVR.Visual.The3DWorld.rotate_coordinate_system_about_Y()
- PTVR.Visual.The3DWorld.rotate_coordinate_system_about_Z()

Wikipedia contributors, 'Rotation of axes', Wikipedia, The Free Encyclopedia, 22 November 2021, 22:47 UTC, <https://en.wikipedia.org/w/index.php?title=Rotation_of_axes&oldid=1056635843> [accessed 17 March 2022] 


Demos
--------------
<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> two_superimposed_rotated_coordinate_systems_MINIMAL.py
<td> Displays simultaneously the Room Calibration coordinate system and its rotated version (with minimal code).

<tr>
<td> two_superimposed_coordinate_systems.py
<td> Displays simultaneously the Room Calibration coordinate system and its translated + rotated version (with minimal code).


<tr>
<td>shift_and_rotate_coordinate_systems.py
<td> shows the result of successively translating and rotating the Room Calibration coordinate system.

<tr>
<td>1_global_cartesian_coordinate_system.py 
<td>Displays the Room Calibration coordinate system with its colored axes (red X, green Y, blue Z).


TODOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
todo **créer** une autre démo car celle ci ne le fait pas simultaneously
</table>

