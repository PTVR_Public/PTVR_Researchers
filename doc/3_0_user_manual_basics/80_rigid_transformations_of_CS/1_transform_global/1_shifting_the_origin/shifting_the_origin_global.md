Translations {#shiftingTheOriginOfRoomCalibrationCSGlobal}
==================
<!-- Names of figure files:
Pour Fig 1:
Dans DOSSIER \transformations\
calibration_with_fixed_object_ahead+reduced_Wall.ggb en enlevant la layer 9
calibration_with_fixed_object_ahead+reduced_Wall+Point_P.png

Pour fig 2:
calibration_with_fixed_object_ahead_**origin_sPOV**.ggb en enlevant la layer 9
calibration_with_fixed_object_ahead_origin_sPOV+cylindre_M.png
 -->

<span style="font-size:150%;"> This page shows how to transform coordinates with a translation vector whose coordinates are specified in the **GLOBAL** coordinate system.</span>

To illustrate this kind of translation, a simple example (in 3 steps) is presented in which two objects  (P and M) having the same location in the global coordinate system are specified with different coordinates (x, y, z) in their two respective coordinate systems.

**step 1:** creates a pink point (sphere) P in the [global coordinate system](@ref coordinateTransformations).  
**step 2:** creates a new coordinate system by **translating** the old one (in step 1) with a translation defined by global coordinates..  
**step 3:** creates a cylinder M in the new **translated** coordinate system so that M has the same global location as P.

These steps are detailed below:  

Step 1
---------------
As shown in Figure 1, the triplet  of coordinates (0, 1.1, 1.2) specifies the position of pink point P in the **global coordinate system**, i.e. in the [cartesian coordinate system](@ref cartesianCoordinateSystemInVS) created right after the [room calibration process](@ref calibrationOfHeadset). This is achieved with the following bit of code.  
```
def main():  	
   my_world  = PTVR.Visual.The3DWorld ()
   my_scene  = PTVR.Stimuli.Scenes.VisualScene () 
   my_pink_point_P = PTVR.Stimuli.Objects.Sphere ( position_in_current_CS = np.array ( [0, 1.1, 1.2] ),   
		   			color = color.RGBColor (r = 1, g=0.2, b=1), size_in_meters = 0.1 )
   my_scene.place (my_pink_point_P, my_world) 
```

In the code above, remember that the **position_in_current_CS** parameter needs a **3-dimension np.array (i.e. a triplet)** as an argument. This triplet of values is interpreted as x, y and z **cartesian coordinates** in the CURRENT coordinate system([see Position in 3D cartesian coordinates](@ref in3DcartesianCS)).

<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead+reduced_Wall+Point_P.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV+cylindre_M.png"  width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: 
The CURRENT coordinate system is the GLOBAL (aka **World**) coordinate system. The coordinates (0, 1.1, 1.2) in the global system specify the location of pink point P.
</td>

<td>Figure 2: The CURRENT coordinate system is now an upward translation of the coordinate system used in step 1. The coordinates (0, 0, 1.2) in the current system specify the location of cylinder M (black label). Note that P (pink label) and M have the same location in the global coordinate system, hence the superimposition of these two different objects.
</td>
</tr>
<!-- FIN DE Figures 1 et 2  -->

<!-- image couleurs neon  -->
<tr> 
	<td colspan="2">
<img src="VR_logo.jpg" width="120pix" style="float:left; padding-right:20px"/> <strong>DEMO !  </strong> Immerse yourself in Figure 2 with the demo  translation_vector_specified_in_GLOBAL_coordinate_system.py  
<p> Directory of demo: ...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Coordinate_Systems\
</p>
<p>
 (logo from <a href="https://www.freepik.com/free-vector/guy-wearing-vr-goggles-neon-sign_4550703.htm"> Neon icon vector created by katemangostar - www.freepik.com</a>)
</p>
	</td>
</tr>

<!-- images bonhomme avec VR : site storyset.com -->
<tr> 
	<td colspan="2">
<img src="bonhomme_avec_vr.png" width="120pix" style="float:left; padding-right:20px"/> 
<p>
(logo from <a href = "https://storyset.com/illustration/virtual-reality/amico"> Storyset</a>)
</p>
	</td>
</tr>


<!-- images bonhomme plus en silhouette : site /thenounproject.com -->
<tr> 
	<td colspan="2">
<img src="bonhomme_dans_cylindre.png" width="120pix" style="float:left; padding-right:20px"/> 
<p>
(logo by rdesign from <a href="https://thenounproject.com/icon/virtual-reality-4868851/">the Noun project</a>)
</p>
	</td>
</tr>


</table>






Step 2
---------------
The PTVR code below shows how to create a new coordinate system by **translating/shifting** the coordinate system of step 1 by a vector (0, 1.1, 0) defined in the global system (this upward shift corresponds to the vector V joining the current origin and the orange point shown in figure 1).  
This is illustrated in figure 2.
```
height_of_point_P = 1.1 # in meters: corresponds to headset height when subject is seated.  
global_coordinates_of_orange_point = np.array ( [0, height_of_point_P, 0 ] ) 
def main():  
   my_world = PTVR.Visual.The3DWorld ()
   my_world.translate_coordinate_system_along_global( global_coordinates_of_orange_point)   
...
```

Step 3
---------------
The **CURRENT** coordinate system is now the system created by the coordinate  translation in step 2. In this new current system, the triplet of coordinates (0, 0, 1.2) specifies the location of cylinder M whose location in global space is the same as the location of point P (see figure 2).  
The point M is created by adding the following line of code after having translated the coordinates in step 2.  

```
...
my_cylinder_M = PTVR.Stimuli.Objects.Cylinder ( position_in_current_CS = np.array ( [0, 0, 1.2 ] ) ,  
	color = color.RGBColor (r = 0, g=0, b=0), size_in_meters = 0.08 ) 
my_scene.place (my_cylinder_M, my_world)
```


Notes
---------------
- Terminology : Translating a coordinate system is also known as SHIFTING its origin.


- A very common usage of the upward translation exemplified in this page is detailed [in the section about PTVR viewpoints](@ref staticViewpointInUserManual). Briefly, the goal is to shift the origin of the global coordinate system to the [viewpoint](@ref staticViewpointInUserManual) corresponding to the **intended headset position** during the experiment.  Thus, the **new origin** will allow the researcher to place stimuli at positions defined **with respect to this viewpoint.** 


Links
---------------
PTVR.Visual.The3DWorld.translate_coordinate_system_along_global()

Wikipedia contributors, 'Translation of axes', Wikipedia, The Free Encyclopedia, 11 October 2021, 06:20 UTC, <https://en.wikipedia.org/w/index.php?title=Translation_of_axes&oldid=1049331106> [accessed 17 March 2022] 




Demos
---------------
Some demos in: 
...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Coordinate_Systems\
<table id="demo_table">
<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>10_global_cartesian_coordinate_system.py 
<td>Basic demo for PTVR beginners. Displays the Global coordinate system with its colored axes (red X, green Y, blue Z).

<tr>
<td> 20_translation_vector_specified_in_GLOBAL_coordinate_system.py
<td> Full immersion in Figure 2.

</table>
