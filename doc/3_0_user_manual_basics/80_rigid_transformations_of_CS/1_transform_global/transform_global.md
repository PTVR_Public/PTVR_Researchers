Transformations defined in GLOBAL CS {#transformGlobalCS}
==================

<span style="font-size:160%;"> This section shows how to create a new coordinate system  with transformations defined in the **GLOBAL** coordinate system. </span>


- @subpage shiftingTheOriginOfRoomCalibrationCSGlobal


<!-- 
- @subpage rotatingTheRoomCalibrationCSGlobal
- @subpage sequenceOfRotationsGlobal
- @subpage sequenceOfTranslationsAndRotationsGlobal
-->





**Links**  
Wikipedia contributors, '**Rigid transformation**', Wikipedia, The Free Encyclopedia, 4 December 2021, 08:06 UTC, <https://en.wikipedia.org/w/index.php?title=Rigid_transformation&oldid=1058556511> [accessed 10 May 2022] 

Section '**Transformations**' in:  
Wikipedia contributors, '**Coordinate system**', Wikipedia, The Free Encyclopedia, 18 March 2022, 15:49 UTC, <https://en.wikipedia.org/w/index.php?title=Coordinate_system&oldid=1077860317> [accessed 10 May 2022] 

Wikipedia contributors, '**Translation (geometry)**', Wikipedia, The Free Encyclopedia, 14 January 2022, 08:25 UTC, <https://en.wikipedia.org/w/index.php?title=Translation_(geometry)&oldid=1065586209> [accessed 10 May 2022] 

Wikipedia contributors, '**Rotation of axes**', Wikipedia, The Free Encyclopedia, 22 November 2021, 22:47 UTC, <https://en.wikipedia.org/w/index.php?title=Rotation_of_axes&oldid=1056635843> [accessed 10 May 2022] 