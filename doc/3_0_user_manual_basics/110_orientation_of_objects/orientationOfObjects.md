Orientation of objects {#orientationOfObjects} 
============
<!-- les figures pour 2 successive rotations sont faites avec cartesian_CS_rotations_about_global_X.ggb -->

> ... three composed **elemental** rotations (**rotations about the axes of a coordinate system**) are always sufficient to reach **any target ORIENTATION**, starting from a known reference orientation... [(see Wikipedia contributors, 'Euler angles' -> section Chained rotation equivalence).](https://en.wikipedia.org/w/index.php?title=Euler_angles&oldid=1084343998)

<BR>

<img src="initial_orientation_before_2_rotations_about_X_et_Y.png" width="500" height="400" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->


In this page, we start from the reference orientation shown in figure 1.  
And we show how **single elemental rotations** of different sorts (global or local rotations) can change this reference orientation.



<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

**Figure 1**: What is the effect of rotating the green arrow **about the Y axis** ?
<BR><BR><BR>





GLOBAL Rotations : rotation axes are aligned with those of the global coordinate system.
--------------------------------------------------------------------------------------
> In mathematics and physics, these rotations are often called **extrinsic** (i.e.  they are rotations about the x or y or z axes of the original coordinate system, which is assumed to remain motionless)
[(see Wikipedia contributors, 'Euler angles' -> section Chained rotation equivalence).](https://en.wikipedia.org/w/index.php?title=Euler_angles&oldid=1084343998)


ONE global rotation
--------------------------------------------------------------------------------------
<!-- Figures 2, 3, 4 -->
<table>
<tr>
<td style="width:33%;" align="center">
<img src="cartesian_CS_rotations_about_global_X.gif" width="450pix"/>
</td>

<td style="width:33%;" align="center">
<img src="cartesian_CS_rotations_about_global_Y.gif"  width="450pix"/>  
</td>

<td style="width:33%;" align="center">
<img src="cartesian_CS_rotations_about_global_Z.gif"  width="450pix"/>  
</td>

</tr> 

<tr>
<td>Figure 2. Rotation of object about GLOBAL X.
</td>

<td>Figure 3. Rotation of object about GLOBAL Y.
</td>

<td>Figure 4. Rotation of object about GLOBAL Z.
</td>

</tr>
</table>
<!-- FIN DE Figures 2, 3, 4  -->
<BR>


PTVR code for ONE global rotation
--------------------------------------------------------------------------------------
< si j'ai mis une ligne de --- ci-dessus alors il faut un espace à la ligne ci-dessous sinon le code déconne>

    ...
    rotation_angle = 40 # and choose one of the 3 following lines
    my_arrow.rotate_about_global_x (rotation_angle) # rotation about global X as in Figure 2 
    my_arrow.rotate_about_global_y (rotation_angle) # rotation about global Y as in Figure 3
    my_arrow.rotate_about_global_z (rotation_angle) # rotation about global Z as in Figure 4
    ...


Two successive global rotations
--------------------------------------------------------------------------------------  

Goal: change the orientation from that shown in figure 5 to that shown in figure 6. This can be achieved with two succesive rotations.
<!-- Figures 5,6  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="initial_orientation_before_2_rotations_about_X_et_Y.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="final_orientation_after_2_rotations_about_X_et_Y.png" width="450pix"/>  
</td>

</tr>

<tr>
<td>Figure 5: initial orientation of object.
</td>
 
<td>Figure 6: Final orientation of object after two successive rotations : the first about global X (see figure 7) and the second about Z (see figure 8).
</td>
</tr>
</table>
<BR>
<!-- FIN de Figures 5,6  -->


<!-- Figures 7,8  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="first_rotation_about_x.gif" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="second_rotation_about_y.gif" width="450pix"/>  
</td>

</tr>

<tr>
<td>Figure 7: Animation to help visualize the first rotation (about global X).
</td>
 
<td>Figure 8: Animation to help visualize the second rotation (about global Z). 
</td>
</tr>
</table>
<BR>
<!-- FIN de Figures 7,8  -->


PTVR code for TWO successive global rotations
--------------------------------------------------------------------------------------
    
    ...
    my_arrow.rotate_about_global_x (240) # rotation about global X as in Figure 7 followed by following line
    my_arrow.rotate_about_global_z (120) # rotation about global Z as in Figure 8
    ...
    



Intrisic Rotations : Rotation axes are aligned with those of the object
--------------------------------------------------------------------------------------
<BR>

> In mathematics and physics, these rotations are often called **intrinsic** (i.e. they are rotations about the axes of the rotating coordinate system XYZ, **solidary with the moving body**, which changes its orientation after each elemental rotation) [(see Wikipedia contributors, 'Euler angles' -section Chained rotation equivalence).](https://en.wikipedia.org/w/index.php?title=Euler_angles&oldid=1084343998)
 

<BR>
<img src="Euler2a.gif" width="500" height="400" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->



Note that the three blue axes correspond to the PTVR **GLOBAL** coordinate system. 
This is therefore one of the few exceptions where we do not represent the GLOBAL coordinate system with the RUF display (Sorry about this laziness on our part!).

The sphere can be thought as a PTVR object which changes orientations 3 times about three different **intrinsic**  (i.e. solidary with the moving object) axes.

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

**Figure 9**: Intrinsic rotations. Image from <a href="https://commons.wikimedia.org/wiki/File:Euler2a.gif">Euler2.gif: Juansemperederivative work: Xavax</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, via Wikimedia Commons.
<BR><BR><BR>


PTVR code for intrinsic rotations
--------------------------------------------------------------------------------------

    ...
    rotation_angle = 40 # and choose one of the 3 following lines
    my_arrow.rotate_about_object_x (rotation_angle) # rotation about intrinsic X
    my_arrow.rotate_about_object_y (rotation_angle) # rotation about intrinsic Y
    my_arrow.rotate_about_object_z (rotation_angle) # rotation about intrisic Z 
    ...
    



Demos
--------------------------------------------------------------------------------------
<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 
...\PTVR_Researchers\Python_Scripts\Demos\Orientations\
20_rotation_animated_across_scenes.py
<td> Animation displaying a cube rotating about one axis.


<BR>
</table>


Links:
--------------------------------------------------------------------------------------
Wikipedia contributors, 'Orientation (geometry)', Wikipedia, The Free Encyclopedia, 11 March 2022, 09:11 UTC, <https://en.wikipedia.org/w/index.php?title=Orientation_(geometry)&oldid=1076477808> [accessed 5 May 2022] 


Wikipedia contributors, 'Euler angles', Wikipedia, The Free Encyclopedia, 23 April 2022, 23:15 UTC, <https://en.wikipedia.org/w/index.php?title=Euler_angles&oldid=1084343998> [accessed 5 May 2022] 


