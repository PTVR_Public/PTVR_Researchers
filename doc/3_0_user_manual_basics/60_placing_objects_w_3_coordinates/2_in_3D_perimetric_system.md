Position in 3D perimetric coordinates {#in3DperimetricCS}
============
**General Goal** : you want to create and place in the [GLOBAL coordinate system (aka World coordinate system)](@ref globalAndLocalCoordinateSystemsl) an object whose position is defined by its three **perimetric** coordinates (**eccentricity**, **half-meridian**, **radial distance**).

**Specific Goal** : You want to create and place a small sphere at the position of the orange point P in Figure 1.

The perimetric coordinates of point P in the figures below are :  
- *Eccentricity* = 20°    
- *Half-meridian* = 135°  
- *Radial distance* = 1 meter (distance between the Origin and P, i.e the sphere radius)  
  
**PTVR fonction to use**:  
Apply the **set_perimetric_coordinates()** fonction to the already created object "my_sphere" as below:  

```
my_sphere = PTVR.Stimuli.Objects.Sphere ()   
...    
my_sphere.set_perimetric_coordinates (eccentricity = 20, halfMeridian = 135, radialDistance = 1)
```


<span style="background-color:----------">

</span>


<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Spherical_Coordinates_Perimetric_avec_bonne_rotation_Y.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Spherical_Coordinates_Perimetric_avec_bonne_rotation_Y.gif" width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: How to display the orange point P in the **3D perimetric** coordinate system of PTVR.  </td>
<td>Figure 2: Same as figure 1 with a horizontal rotation to allow a better representation of the 3D structure of the figure.   </td>
</tr>
</table>






Links 
-----------------------
- [3D perimetric coordinate systems in PTVR documentation](@ref perimetricCoordinateSystemInVS)

Demos
-----------------------
<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>2_create_object_in_perimetric_coord.py
<td> Create an object and place it in the 3D global space with perimetric coordinates.



</table>
