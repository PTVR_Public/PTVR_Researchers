Position in 3D cartesian coordinates {#in3DcartesianCS}
============
**General Goal** : you want to create and place an object whose position is defined by its 3 cartesian coordinates (x, y, z) in the [GLOBAL coordinate system (aka World coordinate system)](@ref globalAndLocalCoordinateSystemsl). 

**Specific Goal** : You want to create and place a small sphere at the position of pink point P (x=0.25, y=0.20, z=0.30) as represented in figure 1 (PTVR coordinates are defined in meters by default).

**PTVR code 1: function to create an object at a specific position in global coordinates**
```
PTVR.Stimuli.Objects.Sphere (position_in_current_CS = np.array ( [0.25, 0.20, 0.30] ) ) # create a point (here a sphere) with its position
``` 



<div id="note">\emoji :sunglasses: **Tip**: Beyond this code snippet, you can look at full fonctional code of relevant demos at the end of the page. </div>

<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="cartesianCoordinateSystemInVS_3D.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="cartesianCoordinateSystemInVS_3D.gif" width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: Creation of the pink point P with **Global coordinates**.  </td>
<td>Figure 2: Same as in figure 1 with a horizontal rotation about Y to allow a better representation of the 3D structure of the figure.   </td>
</tr>
</table>
<BR>

<div id="note">\emoji :sunglasses: **Tip**: In code snippet 1 above, it is assumed that the CURRENT coordinate system is the GLOBAL coordinate system (i.e. the one shown in the figure). </div>

If you don't like the use of an array containing the three x, y and z coordinates (PTVR code 1), you can first create the object (here a sphere) and then specify its position as shown below:  
**PTVR code 2:**  
```
mySphere = PTVR.Stimuli.Objects.Sphere ()
mySphere.set_cartesian_coordinates (x = 0.25, y = 0.20, z = 0.30) 
```




#### Links 

- [3D cartesian coordinate system in 'Essentials' section](@ref cartesianCoordinateSystemInVS)
- [GLOBAL coordinate system (aka World coordinate system)](@ref globalAndLocalCoordinateSystems)

#### Demos

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>
...\PTVR_Researchers\Python_Scripts\Demos\Positions\
1.0_create_object_in_cartesian_coord.py 
<td> create a cube in front of you : its coordinates are specified with cartesian coordinates in the **Global coordinate system** 

<tr>
<td>
...\PTVR_Researchers\Python_Scripts\Demos\Coordinate_Systems\
10_global_cartesian_coordinate_system.py 
<td>Displays the **Global coordinate system** with its colored axes (red X, green Y, blue Z). 

<BR>
</table>
<!-- FIN de DEMOS -->



<div id="note">\emoji :sunglasses: **Tip**: when you click on one of the PTVR file names in the table above, a new window appears and allows you to click on the link "Go to the source code of this file" (see figure 3 below). This will directly lead you to the full raw code. </div>  

<BR>
<img src="Go to the source code of this file.png" width="700" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->


<BR><BR><BR>
**Figure 3**


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

<BR><BR>


