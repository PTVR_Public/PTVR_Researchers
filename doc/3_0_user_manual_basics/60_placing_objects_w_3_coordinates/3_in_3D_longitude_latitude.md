Azimuth and elevation {#in3DlongitudeLatitudeCS} 
============

<BR>

The azimuth / elevation Coordinate System belongs to the class of spherical coordinate systems.

In this coordinate system, the 3D position of a point **P** is defined by 3 coordinates in the following way :  
– **Azimuth** (from -180° to 180°).     
– **Elevation** (from -180° to 180°).  
– **Radial Distance** (in meters) – corresponds to the radius of the sphere on which P is lying (i.e., constant distance between Origin and P).


**PTVR fonction to use**:  
Apply the **set_azimuth_elevation_coordinates()** fonction to the already created object "my_sphere" as below:  

```
my_sphere = PTVR.Stimuli.Objects.Sphere ()   
...    
my_sphere.set_azimuth_elevation_coordinates (azimuth = 20, elevation = 135, radialDistance = 1)
```



<div id="note">\emoji :scream: **Warning:** Note that the terms **Azimuth** and **elevation** have different meanings in different spherical systems. </div>



Links 
-----------------------
- [Spherical Coordinate System - Wikipedia](https://en.wikipedia.org/wiki/Spherical_coordinate_system)

Demos
-----------------------
<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 3.0_create_object_in_azimuth_elevation_coord.py
<td> Create an object and place it in the 3D global space with azimuth and elevation coordinates.



</table>



<!--

<BR><BR>
<div id="note">\emoji :scream: **Warning:** Do not get confused with the Fick coordinate system (see TO DO XXXXXXXXXXXXXXXXXXXXXXXXX CoordinateSystemsForEyeRotationsInVisionScience). </div>

The confusion with the Fick system can be easily made as the Fick system is also an azimuth-longitude / elevation-latitude coordinate system. "However, azimuth is assessed FIRST with reference to the sagittal plane of the head ..., and the eyes are assumed to rotate gimbal fashion. In this case, order matters - the measurements are not commutative." In Howard, I. P., & Rogers, B. J. (2008). Chapter 20. Classification of binocular disparity. In Seeing in Depth (Vol. 2). Oxford University Press. https://oxford.universitypressscholarship.com/view/10.1093/acprof:oso/9780195367607.001.0001/acprof-9780195367607-chapter-20
-->