Placing objects with 3 coordinates {#placingObjectsInVacuum}
==================

<div id="note">\emoji :sunglasses: **Note 1:** In this section, all figures and examples illustrate the use of [GLOBAL (aka WORLD) coordinates)](@ref globalAndLocalCoordinateSystems) to place objects. </div>

<div id="note">\emoji :sunglasses: **Note 2:** If you want to use **LOCAL coordinates**, see the next Sections: [Placing objects with 2 coordinates on Screens](@ref placingObjectsOnScreens) or [Placing objects after coordinate transformations](@ref coordinateTransformations). </div>


**This section is divided into the following sub-sections:**

<!-- Figure 1  -->
<table style="float:right; margin-left:20px; width:50%">

<tr>
  <td> <img src="RPi_learning_Getting_Started_With_Mathematica.png"  style="width:100%;">
  </td>
</tr>

<tr>
  <td style="text-align:center" >             </td>
</table>
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->





- @subpage in3DcartesianCS  
Using this coordinate system can be helpful for instance when you want to place objects, for instance chairs, tables, ... on the floor of your virtual room (i.e. on the XZ plane of the Global Coordinate System).
 

- @subpage in3DperimetricCS  
In vision science, the cartesian coordinate system is often not appropriate as stimuli must often be positioned to induce well-characterized retinal coordinates, hence the need for the perimetric coordinate system.



- @subpage in3DlongitudeLatitudeCS [To be completed]


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

<BR>

<div id="note">\emoji :sunglasses: **Reminder:** If you need an extensive summary of relevant information on the different ways of placing objects, do not forget this helpful [CHEATSHEET](@ref specifyPositionCheatSheet). </div>


<BR>
<div id="note">\emoji :sunglasses: **Tip**: when you set the 3D position of a **symmetrical** object, this position is applied to the **center** of this object. If the object is **asymmetrical**, like a chair, then this position is applied to the **pivot** of this object </div>



**Attributions**  
Image from <a href="https://commons.wikimedia.org/wiki/File:RPi_learning_Getting_Started_With_Mathematica.png">Raspberry Pi Foundation - https://www.raspberrypi.org</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons.