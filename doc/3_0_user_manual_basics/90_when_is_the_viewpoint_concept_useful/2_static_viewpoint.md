Static viewpoint {#staticViewpointInUserManual}
============================
<BR>

<!-- Geogebra figures 2, 3, 4 
/transformations/calibration_with_fixed_object_ahead_origin_sPOV.ggb
-->

#### 1. The static viewpoint where the subject's eye should be : real world.#
<BR>
<!-- Image 1 -->
<img src="Perimetric_CS_Hm150_Ecc20_frog_at_origin.png" width="550" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

<span style="font-size:130%;">   
"Ideally, your ultimate methodological goal is to place a subject's eye at the origin of a perimetric system." Psychophysical Mantra.
</span>

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

**Figure 1:**. A frog's eye is placed at the origin of a perimetric system.
(image of the frog from <a href="https://commons.wikimedia.org/wiki/File:Golden-eyed_tree_frog_(Agalychnis_annae)_1.jpg">Charles J. Sharp</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons).   


The standard practice in Vision Science is to specify the positions of visual objects by using a [perimetric coordinate systems](@ref _2DperimetricCoordinateSystemInVS) whose **ORIGIN** is chosen as the **[VIEWPOINT](@ref viewpointInVS)** for the user. We already saw how this is achieved in the real world with most ophthalmological devices [in this Vision Science Section](@ref viewpointsInOphthalmology).   

Thus, in the real world, the experimenter needs to make sure that the subject's eye is exactly at the Origin's position, or very close to it so that visual angles can be validly approximated ([see section on approximation of Visual Angles](@ref approximationsOfVisualAngles)).

#### 2. The static viewpoint where the subject's eye should be : virtual world.#
The points above lead us to the following key question :  
<span style="font-size:130%;"> 
 How does Virtual Reality allow us to achieve valid visual angle approximations ?      &emsp;      **[QUESTION 1]**
</span>

To answer this question, we consider two issues for clarity of presentation (although they are cleary interdependent) : A/ stabilizing the head and b/ stabilizing the eyes.

#### 2.1. Stabilizing the head

<!-- Image 2 -->
<img src="Spherical_Coordinates_perimetric_w_moving_headset.gif" width="550" style="float:left; padding-right:20px" />  

<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->
<BR>
To make this question very explicit, it is worth emphasizing that the present section deals only with experimental situations where the **VIEWPOINT** is **static** in the virtual world.   
Consequently, as the subject's head (and thus headset) is allowed to move freely in VR, **the challenge for the experimenter is to maintain the headset as close as possible to the viewpoint** (Figure 2). Otherwise, the validity of the visual angles validity decreases.


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" any longer  -->
**Figure 2**: Schematic animation showing the headset translating in the vicinity of the viewpoint.

The experimental PTVR solutions that aim at responding to Question 1 are presented below. Note that it is recommended to use them in synergy if possible: 

#### a/ Using a chinrest
The most obvious solution is to use a chinrest as in usal experimental setups in Vision Research laboratories. Of course, this solution removes a lot of the huge possibilities of Virtual Reality. 
However, the advantages of using VR over standard experimental setups remain large and many VR papers report using a chinrest in their experiments.   
&emsp e.g. Denkinger, S., Antoniou, M.-P., Tarello, D., Levi, D. M., Backus, B. T., Bavelier, D., & Chopin, A. (2023). The eRDS v6 Stereotest and the Vivid Vision Stereo Test: Two New Tests of Stereoscopic Vision. *Translational Vision Science & Technology*, 12(3), 1. [https://doi.org/10.1167/tvst.12.3.1](https://doi.org/10.1167/tvst.12.3.1)



#### b/ Seated Subject

The advantage of using a [seated subject](@ref geometricalAspectsOfRoomCalibration) is that it allows a reasonable control of the body's position (up to the neck) so that only small head movements are possible. This situation is actually quite close to the situation with a chinrest as the latter is far from always producing a perfect immobilization of the head.

The main steps are the following:  
- the experimenter measures **with a ruler** the **height of the subject's head** when he/she is seated (Figure 3). Note that this measurement must be made with the same position (in the real world) used in the [Room Calibration process](@ref calibrationOfHeadset) (and of course this position will be the same in the experiment to come).  
- This measurement is used to create a VIEWPOINT (see my_viewpoint in code below). Usually, the experimenter will decide that the coordinates of the VIEWPOINT are (x = 0, y = height_of_headset, z = 0). The experimenter could decide otherwise depending on the goals of his/her experiment ([see section Static Viewpoints at Different Positions](@ref staticViewpointsAtDifferentPositions)).  
- A change of origin ([see Coordinate Transformations](@ref coordinateTransformations)) is made in the code so that the new origin is at the position chosen by the experimenter as the VIEWPOINT (Figure 4).  
- With this new origin, position of points can now be specified with respect to the viewpoint (figure 5). Here a point M is created straight ahead of head and could be used for instance as a Point of Fixation.
<!-- Figures 3 et 4 et 5 -->
<table>
<tr>
<td style="width:40%;" align="center">
<img src="calibration_with_fixed_object_ahead+silhouette_assise+savant_fou.jpg" width="450pix"/>
</td>
<td style="width:30%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_without_P.png" width="350pix"/>  
</td>
<td style="width:30%;" align="center">
<img src="calibration_with_fixed_object_ahead_origin_sPOV_avec_Point_M.png" width="350pix"/>  
</td>
</tr>

<tr>
<td>Figure 3: An older professor would like to create a viewpoint at the approximate location where the subject's head position will be during the experiment. (Image of the Professor from <a href="https://commons.wikimedia.org/wiki/File:Professor_Failed_Experiment_Cartoon.svg">Videoplasty.com</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons)
</td>
 
<td>Figure 4: The professor first shifts the global origin to the desired viewpoint (Coordinate Transformation). The new origin is thus now at a height of 1.2 meters above the global origin in figure 3.
</td>
<td>Figure 5: The professor then creates a point M (x=0, y=0, z=1.2) whose coordinates are now specified with respect to the viewpoint.
</td>
</tr>
</table>
<BR>
<!-- FIN de Figures 3 et 4 et 5 -->

#### PTVR code used for the steps above
```
height_of_headset = 1.2 # in meters 
my_viewpoint = np.array([0, height_of_headset, 0] ) 


def main():
   my_world = PTVR.Visual.The3DWorld ()
   my_world.translate_coordinate_system_along_global (my_viewpoint)

   my_point_M = PTVR.Stimuli.Objects.Sphere ( np.array ( [0, 0, 1.2]  )   )

```




#### c/ Long viewing distance of objects

In preparation

<!-- Début de A FAIRE PLUS TARD

TODO EC: faire une demo.py avec un carré près et un loin et faire bouger sa tête de qqs cm

Il y a bcp d'astuces pour vérifier la coicidence :
par
calculator en ligne !!
etc....
ou bien adapter la variable height_of_head au début de chaque essai, à la hauteur physique du casque.

à 500 m ya pas de disparité binoculaire, ça vaut le coup de le dire

 Fin de  A FAIRE PLUS TARD -->



#### d/ Offline or real time control of Head and gaze
In preparation

<!-- Début de A FAIRE PLUS TARD

 Real time calculator

TODO EC: faire une demo.py qui affiche l'angle visuel en real time quand le sujet bouge un peu ssa t^te


Fin de  A FAIRE PLUS TARD -->



#### 2.2. Stabilizing the eyes
In preparation

<!-- Début de A FAIRE PLUS TARD


pê à la fin dire:

Stated differently, this means that the instantaneous position of points is **NOT** controlled by (or contingent on) the position of the headset (or gaze) (the latter kind of real time control (or contingency) is described in the [Mobile Viewpoint section](@ref mobileViewpointInUserManual)).
et

As usual, researchers have to decide what methodological constraints are adequate to achieve their specific experimental goals.

Here, we only describe some methodological choices made by PTVR to offer open-source tools to researchers so that future studies can assess their validity and the reproducibility of the results obtained with these tools.
Fin de  A FAIRE PLUS TARD -->

<BR><BR>


#### DEMOS 

in ... \PTVR_Researchers\Python_Scripts\Demos\Rotations\

<table id="demo_table">

<tr><th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> rectangles_looking_at_current_CS_origin.py
<td> Try and move your head to be exactly at the origin of the current Coordinate System where all rectangles will be all looking at you !


<BR>
</table>

