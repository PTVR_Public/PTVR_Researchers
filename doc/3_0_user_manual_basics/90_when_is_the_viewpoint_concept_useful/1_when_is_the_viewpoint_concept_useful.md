When is the viewpoint concept useful ? {#whenIsTheViewpointConceptUseful} 
============




<span style="font-size:130%;">The viewpoint concept is useful in the situations described in the following sub-sections:  </span>

- @subpage staticViewpointInUserManual
- @subpage mobileViewpointInUserManual
- @subpage staticViewpointsAtDifferentPositions