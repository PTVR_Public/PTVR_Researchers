Coordinate transformations {#coordinateTransformationsCheatSheet}
============
<!--- voir technote inria de JTM here : https://notes.inria.fr/iHJfp8q9SLmjh-hdgrdEtA# -->

<img src="coordinate_transformations.png" width="992pix"/>

<!--- NE PAS OUBLIER de ROGNER l'image ci-dessus -->

#### Notes    ####

- Transformations have cumulative effects :  
ex.  
`rotate_coordinate_system_about_global_x (20)` followed by    
`rotate_coordinate_system_about_global_x (20)`   
produces a 40° rotation.



#### Demos ####
Many Demos in: ...\PTVR_Researchers\Python_Scripts\Demos\Screens\

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> 1.0_create_flat_screens_w_translation_of_CS.py
<td> does what it says... (CS stands for Coordinate System)


<BR>
</table>




#### Links ####
[Transformations in Global coordinates](@ref transformGlobalCS)  
[Transformations in Current coordinates](@ref transformCurrentCS) 