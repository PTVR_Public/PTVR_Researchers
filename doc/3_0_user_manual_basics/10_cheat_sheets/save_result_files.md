Saving Result Files {#saveResultsCheatSheet}
============


<img src="save_result_files.png" width="500pix"/>

<!--- NE PAS OUBLIER de ROGNER l'image ci-dessus -->

*See Demo : 1_where_is_my_results_file_by_default.py  
**See Demo : 2_where_else_can_I_save_my_results_file.py


#### Demos

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr> 
<td> 1_where_is_my_results_file_by_default.py
<td> Illustrates where your result files are saved.


<tr>
<td> 2_where_else_can_I_save_my_results_file.py
<td> Illustrates how you can define the location where you save your result files.



<BR>
</table>

