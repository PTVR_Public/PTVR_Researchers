Position of Objects {#specifyPositionCheatSheet}
============
<!--- voir technote inria de JTM here : https://notes.inria.fr/iHJfp8q9SLmjh-hdgrdEtA# -->

**Reminder**.
At any moment in a script, there is a **CURRENT** coordinate system (**CS**) that is used by **all subsequent fonctions** related to objects position or orientation (see [transformations of coordinate systems](@ref coordinateTransformations)).  

For instance, when creating an object with the following code:  
`my_object = …Sphere (position_in_current_CS = np.array ([x, y, z] ) ` 
The parameter's name **position_in_current_CS** reminds you that the coordinates x, y and z are specified in the CURRENT coordinate system. 

In other words, the **CURRENT** coordinate system (say CS1) is in action until a new rigid transformation of the Coordinate System is applied thus leading to a new **CURRENT** coordinate system (say CS2).

---------------------------------------------------------------------------------

#### We assume that the 2 following lines of code have been written before giving a position to objects:

`my_world = PTVR.Visual.The3DWorld ()`  
`my_scene = PTVR.Stimuli.Scenes.VisualScene ()`   

(my_world and my_scene could be any names, however we use them throughout the documentation for consistency) 

<img src="specify_position_of_objects_in_3D.png" width="992pix"/>
<!--- NE PAS OUBLIER de ROGNER l'image ci-dessus -->

<img src="specify_position_of_objects_in_2D.png" width="992pix"/>
<!--- NE PAS OUBLIER de ROGNER l'image ci-dessus -->          

#### Notes
- Once an object has been created and placed in a given scene, do not forget the following line of code:  
`my_scene.place (my_object, my_world)`

- if you want to modify the position of an object that has already been created,  changing its 'position_in_current_CS' parameter will not have any effect (i.e. you must instead use functions/methods such as 
`my_object.set_cartesian_coordinates ()` 
or
`my_object.set_perimetric_coordinates ()` )    


- You can recover the position coordinates (specified in the Global coordinate system) with this code:  
`my_object.get_position()`  


- For detailed explanations on my_object.get_position() (including its limitations), see  the documentation in User Manual → [Get coordinates of objects](@ref gettingCoordinatesOfObjects)
	

#### Demos
Many Demos in these directories:  
...\PTVR_Researchers\Python_Scripts\Demos\Objects\  
...\PTVR_Researchers\Python_Scripts\Demos\Screens\

The key Demos are the following (click on one of them to go to the source code) :

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>	1.0_create_object_in_cartesian_coord.py
<td> Does what it says.

<tr>
<td>	2_create_object_in_perimetric_coord.py
<td> Does what it says.

<tr>
<td>	1.3_place_objects_on_flat_screen.py
<td> Does what it says.

<tr>
<td>	2.3_place_objects_on_tangent_screen.py
<td> Does what it says.


<BR>
</table>


#### Links
[Placing objects with 3 coordinates](@ref placingObjectsInVacuum)  
[Placing objects with 2 coordinates on 2D screens](@ref placingObjectsOnScreens)



<!-- début de todo plus tard

#### MODIFIER DANS PTVR :
*il faudrait enlever l’autre sytem de CH  qui ne marche pas pour faire plus tard  le « rightHanded » system

Set2DPerimetric () est utilisé avec plusieurs type d’objets :
- avec  VirtualPoint ex. dans ...\Demos\Visual Angles\base_infinite_cone_demo.py
- avec VirtualPoint2D ex. dans  ...\Demos\Input\InputPointedAt
- avec tangentScreen ?? ex. dans ...\Demos\Visual Angles\base_infinite_cone_demo.py

il me semble qu'on doit supprimer cette fonction maintenant qu'on a des coordinate transformations: set_coordinate_system_from_a_virtual_point ()
ainsi que celles du même genre s'il y en a encore.

d'ailleurs je me demande si j'ai bien compris: la seule occasion où on utilise explicitement un virtual-point c'est pour faire des conversions? 
Sachant que l'histoire des empty objects n'est pas claire pour EC (cf. question de jérémy).

#### COMPRENDRE et Réfléchir

 B/ je me demande si c’est vraiment justifié d’avoir des VirtualPoint si le prix à payer  est d’avoir des noms de fonction différents !!!
donc quel est vaiment l’intérêt de ces VirtualPoint à  part de prendre moins de « place ».


- Pourquoi PTVR.Stimuli.Utils.VirtualPoint2D ()
alors que PTVR.Stimuli.World.VirtualPoint ()

- pourquoi faut-il une classe VirtualPoint2D alors qu’il y a déjà VirtualPoint ?
Autre façon de dire : pourquoi dériver  VirtualPoint2D de Utils ????????

- A quoi sert MyText.SetPerimetricCS (np.array([ ,  , ])) ?? dans  InputPointedAt.py
et pourquoi ça prend un np.array, alors qu’il faudrait des paramètres : eccentricity =, etc...

#### A quoi servent ces fonctions et sont-elle fonctionnelles et nécessaires ?
Set2DPerimetric(eccentricity, halfMeridian)  
SetCalculatedPosition()  
SetFickCoordinates()  
SetPerimetricCoordinates()  
SetVirtualPointData()  
SetXYZ(x, y, z) et GetXYZ() JTM: supprimées car doublon avec get_x etc...

QST : est ce que ça veut dire que les autres restent?


#### idem pour ces fonctions? et où peut-on trouver des démos les utilisant?
GetReferencePosition()  




EC TODOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
dire ou mettre qq part: Les coordonnées globales sont disponibles coté unity, au travers du calculator. Pour le moment il faut faire du cas par cas. Ce sera bcp plus dynamique avec le server.


<span style="background-color:yellow">
et il faut aussi répondre aux questions dans Tech Note::  
[https://notes.inria.fr/tiGPLfGcQ9iHhDnj867iSw#](https://notes.inria.fr/tiGPLfGcQ9iHhDnj867iSw#)   
</span>



FIN de todo plus tard -->