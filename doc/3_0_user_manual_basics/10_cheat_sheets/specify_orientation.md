Orientation of Objects {#specifyOrientationCheatSheet}
============

<!--- voir technote inria de JTM here : https://notes.inria.fr/iHJfp8q9SLmjh-hdgrdEtA# -->


<img src="specify_orientation_of_objects.png" width="992pix"/>

<!--- NE PAS OUBLIER de ROGNER l'image ci-dessus -->

#### Objects that can be passed as arguments to rotate_to_look_at() or to rotate_to_look_at_in_opposite_direction()

my_world<span style="font-size:250%;">.</span>   
&emsp; handControllerLeft.id  
&emsp; handControllerRight.id  
&emsp; headset.id  
&emsp; originCurrentCS.id  	&emsp;&emsp;&emsp; (origin of current Coordinate System)  
&emsp; originGlobalCS.id 	&emsp;&emsp;&emsp; (origin of global Coordinate System)

Examples:   
    
    my_object.rotate_to_look_at ( my_world.headset.id )
In this case, my_object is (continuously) looking at the headset

    my_object.rotate_to_look_at_in_opposite_direction  ( my_world.originCurrentCS.id ) 
In this case, my_object is turning its back to the origin of the current Coordinate system.


#### Demos
Path of Demos: ...\PTVR_Researchers\Python_Scripts\Demos\Orientations\  
A few examples:   
<table id="demo_table">
<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>	20_rotation_animated_across_scenes.py
<td> Illustrates the use of my_object.rotate_about_current_z ()

<tr>
<td>	eyes_looking_at_you.py
<td> Illustrates the use of my_object.rotate_to_look_at (my_world.headset.id). This script also illustrates the use of PARENTING.

<tr>
<td>	eyes_turning_their_back_to_current_CS_origin.py
<td> Illustrates the use of my_object.rotate_to_look_at_in_opposite_direction (my_world.originCurrentCS.id)

<tr>
<td>	flat_screen_looking_at_hand_controller.py
<td> Illustrates the use of my_object.rotate_to_look_at (my_world.handControllerRight.id)


<BR>
</table>


#### Links
[Orientation of Objects](@ref orientationOfObjects)
<BR><BR>