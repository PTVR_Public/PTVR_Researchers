Activation of an object by pointing {#selectingPointedObject} 
=========================================

#### Introduction

As explained in the [Introduction of the current section](@ref pointingAtIntroduction), there are two independent PTVR components when it comes to pointing at objects:  


- a/ the flat cursor which is a visual feedback and which is not obligatory to point at objects (although the absence of a flat cursor might be detrimental for pointing accuracy). This component has been abundantly detailed in previous sub-sections.
- b/  the activation of the **pointed at** object which allows users to have different kinds of action on any object in the 3D world. This component is programmed in PTVR thanks to a **PTVR event**.
This component is detailed in the present sub-section.


#### The ACTIVATION Cone (programmed with a PTVR event)

An action, or many actions, on a variety of objects in the 3D world can be performed thanks to a conic volume called the **ACTIVATION CONE** (Figure 1). This cone has its origin (or apex) at the position of the pointing device (headset, hand-controller or gaze).   

<!-- Figures 1   -->
<table>
<tr>
<td style="width:100%; height: auto; background-color:white;" align="center">
<img src="reticle_en_3D_bonhomme_assis.png" width="350">
</td>
<tr>
<td>Figure 1: Perspective view of the **ACTIVATION cone (dashed lines)**. In this example, a cylinder object is within the volume of the activation cone so that pointing is validated (i.e. the cylinder is activated by pointing with this activation cone). The activation cone must not be confused with the cursor cone (in solid lines), the latter being a visual feedback (here a reticle) which is independent of the activation of pointing. </td>

</tr>
</table>
<BR>

Note again the independence between the activation and the cursor cone. An example of this independence is illustrated in Figure 1 : here the activation cone (dashed lines) and the cursor cone (solid lines) have different angular sizes (the radius of the activation cone is 2 degrees).   

<BR><BR>
<div id="note">\emoji :sunglasses: The activation cone must not be confused with the cursor cone.  
 </div>
<br>

**Crucially**, the activation cone is associated with an object that has to be pointed at to validate the pointing process achieved by this activation cone. 


An activation cone is programmed in PTVR with an [Event](@ref Interaction) called 'PointedAt' (see demos at the end of the current page).   

The main arguments passed to the 'PointedAt' event determine:
- The identity of the object that will be pointed at.
- The cone origin (i.e. the id of the pointing device - headset, hand-controller or gaze).
- The angular size of the activation cone (angle subtended at the cone origin).


<BR><BR>
<div id="note">\emoji :sunglasses: **Pointing is defined as valid when the 'PointedAT' Event occurs !**.  
 </div>
<br>




#### The action performed when pointing is valid
Once a 'PointedAt' event is triggered (i.e. when a given object has been activated by pointing), many actions can be performed with a [Callback](@ref Interaction) or several callbacks associated with the 'PointedAt' event (see demos at the end of the current page). 



#### Demos
Main demos are in:  
...\PTVR_Researchers\Python_Scripts\DEMOS\Pointing\events_and_callbacks_for_pointing\  
  
<table id="demo_table">

<tr>
<th id="double_th_1">Python file  
<th id="double_th_2">Description 

<tr>
<td> head_pointing_to_change_an_object.py 
<td> Simple demo to change the color of an object by pointing at it. There is no flat cursor in this demo. 

<tr>
<td> head_pointing_to_change_an_object_w_flat_cursor.py 
<td> Same as previous demo except that a head-contingent flat cursor is displayed. 

<tr>
<td> head_pointing_to_end_trials.py 
<td> In this demo, pointing at an object (with a flat cursor) is required to end each trial. Position of the object changes on each trial. 



</table>
<!-- FIN de DEMOS -->