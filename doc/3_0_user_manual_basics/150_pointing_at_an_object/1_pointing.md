Pointing at an object {#pointing}
================
<span style="font-size:130%;"> This section is divided into the following sections:  </span>

- @subpage pointingAtIntroduction
- @subpage flatCursors
- @subpage differentKindsOfFlatCursors
- @subpage keyFeaturesOfFlatCursors
- @subpage pointingLaserBeam
- @subpage selectingPointedObject





<img src="woman_pointing_at_a_vase_w_flat_cursor.png" width="1024pix"/>

