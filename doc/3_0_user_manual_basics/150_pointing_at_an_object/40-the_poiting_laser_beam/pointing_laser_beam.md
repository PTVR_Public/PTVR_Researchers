Another pointing tool : the laser beam {#pointingLaserBeam}
================

Another tool which makes it easy to point at objects is the **laser beam** originating from the hand-controller. 

In the figure below, a subject points at the rabbit with a green PTVR laser beam. 


<img src="pointing_with_laser_beam.jpg" width="800pix"/>



#### Demos

Main demos are in:  
...\PTVR_Researchers\Python_Scripts\DEMOS\Pointing  
...\PTVR_Researchers\Python_Scripts\DEMOS\Serious_games

<table id="demo_table">

<tr>
<th id="double_th_1">Python file  
<th id="double_th_2">Description 

<tr>
<td> laser_pointing.py 
<td>     Illustrates how to point at (and interact with) spheres with a laser beam. 

<tr>
<td> dancing_rabbit_in_room_w_interactions.py 
<td> Have the rabbit dance with your hand-contingent laser beam. 


</table>
<!-- FIN de DEMOS -->



