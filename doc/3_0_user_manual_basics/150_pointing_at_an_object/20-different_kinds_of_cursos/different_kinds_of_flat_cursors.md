The differerent kinds of PTVR "flat cursors "{#differentKindsOfFlatCursors}
==================

The different types of PTVR flat cursors are displayed in the summary figure below, along with the different ways of creating them.

<img src="flat_cursors.png" width="700pix"/>


Whatever the flat cursor type, you have to set its **angular sizes** (see 'X in deg' and 'Y in deg' in the top left of the figure) corresponding to the visual angles as seen from the pointing device, i.e. headset, hand-controller or gaze. This argument is set in ImageToContingentCursor().  


The ImageFromDrawing class generates an object containing a 2D image (the image is created with the PIL package).  
The ImageFromLoading class generates an object containing a 2D image after loading a png image file.  
<div id="note">\emoji :sunglasses: PTVR currently only uses png and jpg image files with ImageFromLoading.  
 </div>
These two object must be passed to ImageToContingentCursor() where it will be converted to a flat cursor, i.e. to a 2D image controlled by a pointing device. 


 
Example of code with notes :   
 
    my_image = SFL.ImageFromLoading (
    		path_to_image_folder = "", 
    		image_file = "mouse_cursor_blue_arrow.png" )
    my_image.Show () # if you want to display in a window of your PC the image created by ImageFromLoading.
    my_cursor = ImageToContingentCursor ( image =  my_image ) 
    my_scene.place_contingent_cursor (my_cursor)

<BR>
<div id="note">\emoji :sunglasses: the object returned by ImageFromLoading (here my_image) **IS NOT like the primitive PTVR Objects** such as spheres, cubes, etc. For instance, the returned object does not have a parameter to position the image. This is also true for ImageFromDrawing, ReticleImageFromDrawing, ScotomaImageFromDrawing.
 </div>


**Where should the image file be stored on your PC ?  **   
if '**path_to_image_folder**' is set to None (default), then the 'image_file' parameter    corresponds to a file stored on your PC in :   
...\PTVR_Researchers\PTVR_Operators\resources\Images   
or in one of its sub-directories.   
    
if '**path_to_image_folder**' is set to say "C:", then the 'image_file' parameter corresponds to a file stored on your PC in "C:". 



#### Demos

Main demos to create and use flat cursors are in:
...\PTVR_Researchers\Python_Scripts\DEMOS\Pointing

<table id="demo_table">

<tr>
<th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td> reticle.py
<td> Simplest way of creating and using a reticle



</table>


