Key features of Flat Cursors {#keyFeaturesOfFlatCursors} 
=========================================
As already said in a previous section, a clear understanding of the specific features of flat cursors requires to realize that they have been initially designed in PTVR to create  **RETICLES** and artificial **SCOTOMAS**.    


### First specific feature : the cursor cone

It is important to first understand what is called the **cursor cone** in order to understand the other specific features of flat cursors.
  
A cursor cone is represented by **solid lines** in Figure 1.  
This is a conic volume aligned with the flat cursor that, in this case, is a reticle (crosshair).  
(note that explanations on the conic volume represented by dashed lines - the activation cone - is provided in a subsequent section on the "selection of pointed objects")


<!-- Figures 1   -->
<table>
<tr>
<td style="width:100%; height: auto; background-color:white;" align="center">
<img src="reticle_en_3D_bonhomme_assis.png" width="350">
</td>
<tr>
<td>Figure 1: Perspective view of the **cursor cone (solid lines)** when the flat cursor is a reticle. In this case, the reticle is head-contingent. </td>

</tr>
</table>
<BR>



### Second specific feature : Adaptive position 

The flat cursor is particular in the sense that its 3D position can be either fixed or variable depending on some parameters.  

By default, the 3D position of the flat cursor is adaptive : at any instant, the flat cursor position depends on the interaction between its cursor cone and objects (the latter being contained or not in the cursor cone).  

Three different cases occur as represented in figure 2 (cursor cones are represented by yellow cones).
a/ For the leftward cursor cone.
In this case, there are no objects in the curso cone. 
Consequently, the flat cursor is placed at constant distance defined by an argument passed when calling ImageToContingentCursor [(see previous section)](@ref differentKindsOfFlatCursors).

b/ For the middle cursor cone.
In this case, there are two objects in the cursor cone.  
Consequently, the flat cursor is placed close to the center of the nearest object (here the red object) .

c/ For the rightward cursor cone.
In this case, there is one object in the cursor cone, namely a wall (grey bar).  
Consequently, the flat cursor is placed at the nearest point of collision with the wall. 


<!-- Figures 2   -->
<table>
<tr>
<td style="width:100%; height: auto; background-color:white;" align="center">
<img src="adaptive_position_of_cursor.png" width="350">
</td>
<tr>
<td>Figure 2: Bird's eye view of three different cursor cones, all having the same origin. The 3 cones subtend the same angle. </td>

</tr>
</table>
<BR>

#### Rationale for the "adaptive position" behaviour : avoiding double vision.

Why did PTVR create the "adaptive position" behaviour described above?  
Figure 3 illustrates the answer by assuming that the flat cursor is a scotoma (thick black bar). 

If the distance of the scotoma (from the eyes) is constant (as shown in Figure 3), then double vision occurs either if gaze is fixating the object or if gaze is fixating the scotoma.  

Having the object (within the cursor cone) near the flat cursor (as shown in figure 2) avoids this undesirable double vision. 


<!-- Figures 3   -->
<table>
<tr>
<td style="width:100%; height: auto; background-color:white;" align="center">
<img src="scotoma_bird's_eye_view.png" width="350">
</td>
<tr>
<td>Figure 3: Situations producing "double vision". (A) Gaze is fixating the object. (A) Gaze is fixating the scotoma. </td>

</tr>
</table>
<BR>



### Third specific feature :
The third key feature corresponds to a famous situation in video games when an object #1 is seen as ** rendered ON TOP** of an object #2 although object #1 is behind object #2.

Consider for instance the situation in the middle of figure 2. Let's assume that the flat cursor is a scotoma. If we take Figure 2 at face value, a part of the scotoma should be invisible as it is within the red sphere and also because this sphere is not transparent.
However, this does not happen because the **rendered ON TOP** algorithm is used by default along with the "adaptive position" behaviour. This is why the scotoma is completely hiding the red object in the situation of the middle of figure 2.
 

###  Alternatives to the default behaviour described above : constant distance

It is possible to deactivate the "adaptive position" behaviour by modifying a parameter in ImageToContingentCursor () [(see previous section](@ref differentKindsOfFlatCursors), thus allowing the flat cursor to be at a **constant distance** from the eyes. 
  
Although this situation will allow the occurrence of double vision, it might be desirable for some purposes. For instance, one might want to create a reticle that would be operational only beyond a certain distance. In other words, this flat cursor would disappear when it is behind an object (left situation in Figure 4). The reason of this disappearance is that the **rendered ON TOP** mode is automatically deactivated when the **constant distance** mode is activated.


<!-- Figures 4   -->
<table>
<tr>
<td style="width:100%; height: auto; background-color:white;" align="center">
<img src="constant_distance_of_cursor.png" width="350">
</td>
<tr>
<td>Figure 4: The flat cursor is at a constant distance from the eyes.
</tr>
</table>
<BR>


#### Demos

Main demos to create and use flat cursors are in:
...\PTVR_Researchers\Python_Scripts\DEMOS\Pointing

