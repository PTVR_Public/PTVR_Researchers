Flat Cursors to create reticles and scotomas {#flatCursors}
================

### Main goals of Flat Cursors
To understand the specific features of flat cursors, it is important to realize that they have been initially designed in PTVR in order to create  **RETICLES** and artificial **SCOTOMAS**.    
These specific features of flat cursors will be explained in more details in the [section on 'key features of flat cursors'](@ref keyFeaturesOfFlatCursors).    


#### a/ Reticles
A PTVR **reticle** (see Figure 1) looks like the reticles used in video games to point at objects.

Standard Reticle definition : A reticle, or reticule, also known as a graticule, is a pattern of fine lines or markings built into the eyepiece of an optical device such as a telescopic sight, spotting scope, theodolite, optical microscope or the screen of an oscilloscope (https://en.wikipedia.org/wiki/Reticle).

"Reticle" is sometimes synonymous with "crosshair".

<!-- Figures 1   -->

<img src="reticle_front_view.png" width="100" height="100">

Figure 1: Front view of the PTVR 2D reticle (aka crosshair).  

<BR>





#### b/ Scotomas
An artificial **scotoma**  (see Figure 2) is a gaze-contingent mask used to simulate scotomas (i.e. zones of blindness in the visual field) as those commonly encountered in Low Vision (cf. Aguilar & Castet, 2011).   
Although a real scotoma is gaze-contingent, a PTVR scotoma can be head- or hand-contingent.

<!-- Figures 2   -->
<img src="woman_pointing_at_a_vase_w_scotoma.png" width="800pix"/>

Figure 2: Here the flat cursor is a scotoma.
<BR>
<BR>
<BR>



### General definition of PTVR flat cursors.  
A "flat cursor" (aka "contingent 2D image") in PTVR is a 2D object that is **controlled by** head, gaze or hand movements.

<img src="mouse_cursor_glove.png" width="80"  style="float:right; padding-left:150px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

This a analogous to the standard **mouse cursor** on the 2D monitor of a PC. The mouse cursor's position on a PC's monitor is said to be **controlled** by the mouse. Equivalently, following the common terminology used in Neuroscience, we also say that PTVR objects can be head-, gaze- or head- **contingent**.
(See the glove cursor in the figure to the right - Image by <a href="https://pixabay.com/users/openclipart-vectors-30363/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1297941">OpenClipart-Vectors</a> from <a href="https://pixabay.com//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1297941">Pixabay</a>)

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->


A PTVR flat cursor is always perpendicular to a line joining the head, gaze or hand-controller and the flat cursor.  
By defaut, the eccentricity of the flat cursor is ZERO degrees with respect to the head, gaze or hand-controller. Thus, a flat cursor with zero-degree eccentricity is in front of the head, gaze or hand-controller. This is represented in figure 3 for a head-contingent (aka head-controlled) flat cursor.


<!-- Figures 3 et 4  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="reticle_on_TS_from_above_figs_a_b.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="reticle_on_TS_from_above_ecc_20_deg.png" width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 3: A head-contingent flat cursor (thick black bar) in bird's eye view for two positions of the head. In this figure, the flat cursor is in front of the head, i.e. flat cursor eccentricity is zero degrees (this is the default). Note also that the flat cursor has an angular size of 20°.  </td>
<td>Figure 4: Same as in figure 3 except that the flat cursor has now an excentricity of 20° with respect to head direction (half-meridian is 0°- i.e. horizontal rightward).   </td>
</tr>
</table>


Figure 4 shows a different case where the eccentricity of the flat cursor is now 20° to the right (with respect to head direction). This ability to choose the eccentricity and half-meridian of the flat cursor is interesting for instance to simulate **excentric scotomas**, i.e. scotomas that are not aligned with the fovea in the case of gaze-contingent scotomas.


#### Demos

Main demos to create and use flat cursors are in:
...\PTVR_Researchers\Python_Scripts\DEMOS\Pointing