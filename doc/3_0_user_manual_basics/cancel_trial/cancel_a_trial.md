Cancel a Trial {#CancelATrial}
============

Lors du déroulement d'une expérience, il peut arriver que l'essai en cours ne doit pas être pris en compte car le sujet à fait quelque chose qui fausse le résultat.

Le traitement d'un essai annulé dépend en fonction de l'experience (voir en dessous)

<p align="center">
  <img src="ArbreDécision_CancelTrial.PNG">
</p>

Afin d'indique dans le fichier de resultat il faut brancher a un [Event](@ref Event) l'Callback [CancelTrial](@ref CancelTrial)

##Solution 1 : Restart Identical Trial
Pour faire la solution 1 il faut brancher aussi sur l'[Event](@ref Event) , l'Callback [RestartCurrentTrial](@ref RestartCurrentTrial)

<div id="note">\emoji :wink: **Remark:** A noté que si pour l'expérience si un essai est égal à 1 scene alors l'Callback [RestartCurrentScene](@ref RestartCurrentScene) suffit.</div>

##Solution 2 : Restart avec la même VI mais avec un élément différent du précédent
<div id="todo">Cette solution n'est actuellement pas supporté, elle est prévu d'être fessable dans les prochaines mise à jour de PTVR.</div>

##Solution 3 : Continu avec le Trial suivant
Pour passé l'essai il faut brancher au même [Event](@ref Event) l'Callback [EndCurrentTrial](@ref EndCurrentTrial) pour passé l'essai en cours 

<div id="note">\emoji :wink: **Remark:** A noté que si pour l'expérience si un essai est égal à 1 scene alors l'Callback [EndCurrentScene](@ref EndCurrentScene) suffit.</div>

<table id="demo_table">
<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 
<tr>
<td>CancelTrial_Logique_RestartSameTrial_Solution1.py
<td>Demonstration how use solution 1
<tr>
<td>CancelTrial_Logique_NextTrial_Solution3.py
<td>Demonstration how use solution 3
<tr>
</table>
<br>