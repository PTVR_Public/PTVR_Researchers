Distance {#distance}
============
Grâce au calculator il est notamment possible de calculer des distances.

Trois type de calculs d'angles est actuellement disponible dans PTVR:
Actuellement il faut entrer des VisualObjects en argument à terme le calcul prendra directement la position.

- RadialDistanceFromOrigin 

Ce calcul permet de d'avoir la valeur en metre entre l'Origin et le visualObjectA.


```
  myCalculator.RadialDistanceFromOrigin(visualObjectA)   
```

<p align="center">
  <img src="OriginDistance.png">
</p>


- RadialDistanceFromHeadPOV

Ce calcul permet de d'avoir la valeur en metre entre HeadPOV et le visualObjectA .

```
 myCalculator.RadialDistanceFromHeadPOV(visualObjectA)  
```

<p align="center">
  <img src="HeadPOVDistance.png">
</p>

- DistanceBetweenGameObjects

Ce calcul permet de d'avoir la valeur en metre entre visualObjectA et visualObjectB.

```
 myCalculator.DistanceBetweenGameObjects(visualObjectA, visualObjectB)
```
<p align="center">
  <img src="DistanceBetweenObjects.png">
</p>