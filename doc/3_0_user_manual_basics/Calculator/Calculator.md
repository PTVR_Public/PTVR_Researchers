Calculator {#Calculator}
============
Dans PTVR il est impossible d'effectuer des calculs dynamiques grâce à l'objet Calculator.

Il est possible de le mettre en place grâce à la ligne : PTVR.Stimuli.Objects.Calculator()

```
 myCalculator = PTVR.Stimuli.Objects.Calculator()
```

Il ne faut pas oublier une fois l'un des calculs inscrit de placer dans la scene l'objet Calculator créer.

Actuellement les calcules suivant sont possible dans PTVR grâce au calculator :

-   @subpage angle

-   @subpage distance

A noter que les calculs proposé retourne une valeur qu'il est possible d'afficher directement dans un objet texte grâce à myCalculator.StringIntegration().

