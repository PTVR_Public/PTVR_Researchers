Angle {#angle}
============
Grâce au calculator il est notamment possible de calculer des angles.

Trois type de calculs d'angles est actuellement disponible dans PTVR:
Actuellement il faut entrer des VisualObjects en argument à terme le calcul prendra directement la position.

- AngleAtOriginBetweenTwoVisualObjects

Ce calcul permet d'avoir la valeur de l'angle avec l'Origin, visualObjectA et visualObjectB. 

```
 myCalculator.AngleAtOriginBetweenTwoVisualObjects(visualObjectA, visualObjectB)  
```

<p align="center">
  <img src="OriginAngle.png">
</p>


- AngleAtHeadPOVBetweenTwoVisualObjects

Ce calcul permet d'avoir la valeur de l'angle avec HeadPOV, le visualObjectA, le visualObjectB.

```
 myCalculator.AngleAtHeadPOVBetweenTwoVisualObjects(visualObjectA, visualObjectB)  
```

<p align="center">
  <img src="HeadPOVAngle.png">
</p>

- AngleAtApexBetweenTwoVisualObjects

Ce calcul permet de d'avoir la valeur de l'angle avec l'apex, le visualObjectA, le visualObjectB.

```
myCalculator.AngleAtApexBetweenTwoVisualObjects(apex, visualObjectA, visualObjectB) 
```
<p align="center">
  <img src="AngleBetweenObjects.png">
</p>
