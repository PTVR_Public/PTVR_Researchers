Immerse yourself in figures {#immerseYourselfInFigures}
============

Some Figures of the PTVR documentation are special in that they are completely reproduced by demos that allow you therefore to literally immerse yourself in these figures.


<img src="VR_logo.jpg" width="120" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

These figures are emphasized in the documentation by the the icon to the left.

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->



And these figures are indexed below to find them quickly in the Documentation.



- [Transform coordinates with a translation vector whose coordinates are specified in the GLOBAL coordinate system  (figure 2)](@ref shiftingTheOriginOfRoomCalibrationCSGlobal)
