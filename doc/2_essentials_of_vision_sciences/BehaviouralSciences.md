Some essentials of Vision Science {#BehaviouralSciences}
==================================

The goal of this section is to remind some scientific and methodological key points of vision and behavioural science that will be useful to design PTVR experiments in VR. 
Go directly to the [Manual for Users](@ref UserManual) if you want to write some PTVR code now.


List of sections
----------------

<img src="Descartes;_Coordination_of_muscle_and_visual_mechanisms._Wellcome_L0002392.jpg" width="210" height="292
" style="float:right; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to push text further away -->




- @subpage greatResoucesOnVision   
- @subpage axesAndPlanesInVS  
- @subpage CoordinateSystemsInVisionScience  
- @subpage TSCoordinateSystemsInVisionScience  
- @subpage orientationVisualObjectsInVisionScience  
- @subpage CoordinateSystemsForEyeRotationsInVisionScience  
- @subpage globalAndLocalCoordinateSystems  
- @subpage visualAnglesInVS  
- @subpage viewpointInVS  
- @subpage Stimuli3DWorld  


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

<BR>
**Attributions of Images**  
Image from <a href="https://commons.wikimedia.org/wiki/File:Descartes;_Coordination_of_muscle_and_visual_mechanisms._Wellcome_L0002392.jpg">See page for author</a>, <a href="https://creativecommons.org/licenses/by/4.0">CC BY 4.0</a>, via Wikimedia Commons.
