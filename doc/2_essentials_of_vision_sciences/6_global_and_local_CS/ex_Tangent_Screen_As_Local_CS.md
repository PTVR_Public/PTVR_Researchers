The Tangent Screen as a local coordinate system {#exTangentScreenAsLocalCS}
============

<!-- from geogebra TSwPerimetricCS.ggb -->

As in the previous section, the **global** coordinate system is represented by the [cartesian XYZ coordinate system](@ref cartesianCoordinateSystemInVS) shown in the figures below. 

A tangent screen placed anywhere in the **global** 3D world is an intuitive example of the utility of using **local** coordinates. It is for instance clear that a point located say 10 cm to the right of your monitor's center will keep this **local** cordinate (xP = + 0.10 m) whatever the position of the monitor in the **global** world.

This is illustrated in the figures below with a point P changing position across time from A to B.

In figure 1, P is moving
<span style="background-color:rgba(255, 0, 0, 0.5);">
in the X axis direction, i.e. rightward, in the global RUF coordinate system.
</span>  

In figure 2, a tangent screen is created: The perimetric coordinates of its center (SO - Screen Origin) in the global system are 0° eccentricity and 0° half-meridian (its radial distance is not relevant here), and its orientation is 0°.  
For our purpose, this implies a/ that the global X axis and the local x axis are parallel and therefore that P is moving
<span style="background-color:rgba(255, 0, 0, 0.5);">
rightward in the global world system AND in the local tangent screen system.
</span>.

In figure 3, **the point P is still locally moving in the rightward direction (local x vector)**. 
Or course, it is however not moving any longer in the global rightward direction (global X vector) as the Tangent Screen's position and orientation in global (world-centered) coordinates are different than in the previous figure (so that the local x axis is not parallel to the global X axis any more). 

<!-- Figures 1, 2 et 3 -->
<table>
<tr>
<td style="width:33%;" align="center">
<img src="TSwSphere_CS=perimetric._local_motion_1.gif" width="300pix"/>
</td>
<td style="width:33%;" align="center">
<img src="TSwSphere_CS=perimetric._local_motion_2.gif" width="300pix"/>  
</td>
<td style="width:33%;" align="center">
<img src="TSwSphere_CS=perimetric._local_motion_3.gif" width="300pix"/>  
</td>
</tr>

<tr>
<td>Figure 1: P is moving **rightward** in the **global** system.
</td>
 
<td>Figure 2: P is still moving **rightward** in the **global** system AND **rightward** in the **local** coordinate system of the Tangent Screen. "SO" stands for Screen Origin.
</td>

<td>Figure 3: P is not moving **rightward** in the **global** system any longer. However (as in previous figure), it is still
moving **rightward** in the **local** coordinate system of the Tangent Screen.
</td>

</tr>
</table>
<BR>
<!-- FIN de Figures 1, 2 et 3 -->

Figures 4, 5, and 6 illustrate the same reasoning as above for a point P moving in a different direction.

<!-- Figures 4, 5 et 6 -->
<table>
<tr>
<td style="width:33%;" align="center">
<img src="TSwSphere_CS=perimetric._local_motion_4.gif" width="300pix"/>
</td>
<td style="width:33%;" align="center">
<img src="TSwSphere_CS=perimetric._local_motion_5.gif" width="300pix"/>  
</td>
<td style="width:33%;" align="center">
<img src="TSwSphere_CS=perimetric._local_motion_6.gif" width="300pix"/>  
</td>
</tr>

<tr>
<td>Figure 4: P is moving **upward** in the **global** system 
</td>
 
<td>Figure 5: P is still moving **upward** in the **global** system AND **upward** in the **local** coordinate system of the Tangent Screen. "SO" stands for Screen Origin. 
</td>

<td>Figure 6: P is not moving **upward** in the **global** system any longer. However (as in previous figure), it is still
moving **upward** in the **local** coordinate system of the Tangent Screen. 
</td>

</tr>
</table>
<BR>
<!-- FIN de Figures 4, 5 et 6-->




