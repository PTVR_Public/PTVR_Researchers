Global and local Coordinate Systems{#globalAndLocalCoordinateSystems}
============

<img src="1192px-1661_Cellarius's_chart_illustrating_a_heliocentric_model_of_the_universe,_as_proposed_by_Nicolaus_Copernicus.jpg" width="572" height="490" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->
<span style="font-size:170%;">
Is the earth spinning aroung the sun or is it the opposite?  
</span>

(chart illustrating a heliocentric model of the Universe as proposed by Nicolaus Copernicus : image from <a href="https://commons.wikimedia.org/wiki/File:1661_Cellarius%27s_chart_illustrating_a_heliocentric_model_of_the_universe,_as_proposed_by_Nicolaus_Copernicus.jpg">Andreas Cellarius</a>, Public domain, via Wikimedia Commons).

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

In Vision science, it is quite common to make use of a hierarchy of coordinate systems. 

The **global system**, which is the "parent" of all other local systems, is usually the  real **WORLD** (i.e. the coordinates of this system are referred to as **world-centered coordinates**). 

And it is convenient to create other coordinate systems, called **local**, that are assigned to entities (e.g. head, gaze, hand, ...) that are moving with respect to the **global** system.


The idea is thus to measure the position (and/or orientation) of an object within a **local** coordinate system which itself has coordinates within a **global** coordinate system. It is actually possible to have a chain of **local** coordinate systems, each one being a "child" of a higher-level **local**  coordinate system up to the highest-level coordinate system, the **global** one, which has no "parent".

<div id="note">\emoji :sunglasses: **SYNONYMS**: the **GLOBAL** coordinate system is also known as the **WORLD** coordinate system.
</div>


Practical example
----------------------

For instance, a scientist might want to measure how accurately a bird flying in the sky is tracked by the eyes (smooth pursuit eye movement) of an observer whose head and body are static in the world. More precisely, the scientist might want to assess the position of the bird's retinal image across time.


In this situation, it is useful to model the situation like this:  

The coordinate system representing the real world is the **GLOBAL** coordinate system. 

Each eye is represented by a **LOCAL** coordinate system.  

With these two coordinate systems (global and local), the goal of the scientist is to assess the position of the bird measured with respect to the eye **local** systems which are themselves rotating within the **global** system.   
Thus, if the eyes are perfectly tracking the bird, the image of the bird on each retina should be perfectly static. 
In other words, the bird's retinal image should be motionless in the eye **local** coordinate systems.

Links and excerpts
----------------------------

- [Great WebSite by Joey de Vries:](https://learnopengl.com/Getting-started/Coordinate-Systems) : the goal of this site is to teach OpenGL but it is so well done and didactic that you can use it as a more general resource to learn 3D geometry in computer graphics.



- In Section **Orientation-based coordinates**  
(Wikipedia contributors. 2021, December 15). Coordinate system. In Wikipedia, The Free Encyclopedia. Retrieved 18:38, December 23, 2021, from https://en.wikipedia.org/w/index.php?title=Coordinate_system&oldid=1060461397):
In geometry and kinematics, coordinate systems are used to describe the (linear) position of points and the angular position of axes, planes, and rigid bodies.[15] In the latter case, the orientation of a second (typically referred to as **local**) coordinate system, ..., is defined based on the first (typically referred to as **global** or **world** coordinate system). For instance, the orientation of a rigid body can be represented by an orientation matrix, which includes, in its three columns, the Cartesian coordinates of three points. These points are used to define the orientation of the axes of the **local** system; they are the tips of three unit vectors aligned with those axes. 




- In Section **Mathematical representations -> Three dimensions**   
(Wikipedia contributors. 2021, August 25). Orientation (geometry). In Wikipedia, The Free Encyclopedia. Retrieved 18:43, December 23, 2021, from https://en.wikipedia.org/w/index.php?title=Orientation_(geometry)&oldid=1040517864. :
In general the position and orientation in space of a rigid body are defined as the position and orientation, relative to the main reference frame, of another reference frame, which is fixed relative to the body, and hence translates and rotates with it (the body's **local** reference frame, or **local** coordinate system). At least three independent values are needed to describe the orientation of this **local** frame. Three other values describe the position of a point on the object. ...
Further details about the mathematical methods to represent the orientation of rigid bodies and planes in three dimensions are given in the following sections...




- In **Active and passive transformation**  
Wikipedia contributors. (2022, February 17). Active and passive transformation. In Wikipedia, The Free Encyclopedia. Retrieved 10:49, March 16, 2022, from https://en.wikipedia.org/w/index.php?title=Active_and_passive_transformation&oldid=1072457534 :  
... Put differently, a passive transformation refers to description of the same object in two different coordinate systems. On the other hand, an active transformation is a transformation of one or more objects with respect to the same coordinate system. For instance, active transformations are useful to describe successive positions of a rigid body. On the other hand, passive transformations may be useful in human motion analysis to observe the motion of the tibia relative to the femur, that is, its motion relative to a **local** coordinate system which moves together with the femur, rather than a **global** coordinate system which is fixed to the floor.

<BR>

The current section is divided into the following sub-sections:  
- @subpage exBodyAsLocalCS  
- @subpage exTangentScreenAsLocalCS
<BR><BR>

