The Head as a local coordinate system {#exBodyAsLocalCS}
============
In the present section (as in many sections of PTVR documentation), the **global** coordinate system is represented by the [cartesian XYZ coordinate system](@ref cartesianCoordinateSystemInVS) shown in the figures below.

In each of these figures, the head is a **local** coordinate system (i.e. with its own axes). Here, only the local forward vector is shown.

Note that the head is oriented (with respect to the global system) differently in figures 1 and 2. This change in head orientation between the two figures can be defined by the different directions of each local forward axis (i.e. each local Z axis).

Stated differently: a point with a **local** Z coordinate of 1 meter would be always placed 1 meter in front of the head on its local forward axis.


<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="local_forward_vector_UP_wrt_head.png" width="550pix"/>
</td>
<td style="width:50%;" align="center">
<img src="local_forward_vector_wrt_head.png" width="550pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: The local coordinate system of the head allows us to define a local FORWARD direction (or vector). </td>
<td>Figure 2:  Same as in Figure 1, except that the head orientation is different thus producing a different local FORWARD direction (or vector). </td>
</tr>
</table>
<BR>

