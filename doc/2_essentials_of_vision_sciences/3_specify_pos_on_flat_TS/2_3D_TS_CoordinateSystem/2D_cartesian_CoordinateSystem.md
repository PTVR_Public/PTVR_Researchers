Tangent screen with 2D Cartesian coordinates {#_2DcartesianCoordinateSystemInVS}
============


Using the conventions of the [**RUF** cartesian system (Rightward-Upward-Forward)](@ref cartesianCoordinateSystemInVS) presented earlier, a **Tangent Screen** can be simply defined as all the points with a constant Z coordinate (0.30 m in Figure 1). Only one point "P" is shown in Figures 1 and 2.
It is usually assumed that the subject's eyes are lying on the X axis on each side of the Origin, so that the **viewing distance** in this experiment is 0.30 m (such a short viewing distance is commonly used with low vision patients).

<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="cartesianCoordinateSystemInVS_2D_1.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="cartesianCoordinateSystemInVS_2D_1.gif" width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: Static representation of a 2D flat **tangent screen** displayed in a 3D cartesian coordinate system. All points lying on this **tangent screen** have the same Z coordinate of 0.30 m. The **viewing distance** is thus said to be 0.30 m. (SO : Screen Origin) </td>
<td>Figure 2: Same as in figure 1 with the **viewing distance** varying from 0.20 to 0.40 m. (SO : Screen Origin)  </td>
</tr>
</table>
<BR>

In sum, Virtual Reality can be used to simulate previous experiments whose stimuli were displayed on a tangent screen. 

With a [3D cartesian system](@ref cartesianCoordinateSystemInVS), one can define a/ the **viewing distance of the tangent screen** and b/ **two "rectangular" coordinates (X and Y in meters)** for each point on this tangent screen. Of course, this is not the best choice for vision experiments where using angular values corresponding to the retinal projections of points is usually needed. This is discussed in more details in later sections.

<div id="note">\emoji :wink: **Remark:** One advantage of using the RUF (Rightward-Upward-Forward) coordinate system is that its X and Y axes are actually oriented in the same way as many important open-source programs designed to program experiments on 2D monitors - see notably the great [PsychoPy](https://www.psychopy.org/).</div>

The remark above suggests that users of programs such a [PsychoPy](https://www.psychopy.org/) should feel quickly at ease to specify the 3D positions of objects in PTVR.


