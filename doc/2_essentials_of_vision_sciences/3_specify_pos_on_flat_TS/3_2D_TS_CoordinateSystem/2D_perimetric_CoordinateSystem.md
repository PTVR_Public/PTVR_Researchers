Tangent Screen with 2D Perimetric coordinates {#_2DperimetricCoordinateSystemInVs}
============                                   

<!-- Geogebra Figures   
Spherical_Coordinates+projection_on_frontal_plane.ggb
-->


Different 2D coordinate systems can be used to define the position of a point P on a **flat tangent screen** that is itself placed in a 3D space. One of them, the cartesian system, was described in a [previous section](@ref _2DcartesianCoordinateSystemInVS). Another one is very important in vision science: this is the **2D perimetric system** that was also already presented in 3D in another [previous section](@ref perimetricCoordinateSystemInVS).
Here we illustrate how 3D **projective geometry** can be used to define a 2D perimetric system on a tangent screen.

In the figures below, a **point P lying on a sphere** is moving while staying at the surface of the sphere.
This point P is projected on a 2D screen that is tangent to the sphere at the point where the Z axis meets the 2D screen. Note that this point is called the **Screen Origin (SO)** of the tangent screen in the PTVR documentation.


<div id="note">\emoji :wink: **Remark:** For simplicity, as in the [previous section](@ref _2DcartesianCoordinateSystemInVS), the position of the **tangent screen** is chosen so that its origin (i.e. **SO**) lies on the Z axis.
</div>



In these figures, the projection of point P is traced on the 2D tangent screen as the point moves.

<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="projection_of_perimetric_sphere_on_plane-animation_Ecc_6-36_HM180.gif" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="projection_of_perimetric_sphere_on_plane-animation_Ecc_6-36_HM270.gif" width="450pix"/>  

</td>
<tr>
<td>Figure 1: Projection of point P onto a tangent screen. The motion of P is defined in a perimetric coordinate system with Eccentricity varying from 6° to 36° while keeping **Half-meridian constant at 180°**.  </td>
<td>Figure 2: Projection of point P onto a tangent screen. The motion of P is defined in a perimetric coordinate system with Eccentricity varying from 6° to 36° while keeping **Half-meridian constant at 270°**.  </td>
</tr>
</table>

<!-- Figures 3 et 4  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="projection_of_perimetric_sphere_on_plane-animation_Ecc_15_HM_0-360.gif" width="450pix"/> <!-- Figures 3  -->
</td>
<td style="width:50%;" align="center">
<img src="projection_of_perimetric_sphere_on_plane-animation_Ecc_30_HM_0-360.gif" width="450pix"/> 
 

<tr>
<td>Figure 3: Projection of point P onto a tangent screen. The motion of P is defined in a perimetric coordinate system with half-meridian varying from 0° to 360° while keeping **eccentricity constant at 15°**. </td>
<td>Figure 4: Projection of point P onto a tangent screen. The motion of P is defined in a perimetric coordinate system with half-meridian varying from 0° to 360° while keeping **eccentricity constant at 30°**.  </td>
</tr>
</table>




**Important references for understanding the use of projections in vision science:**


section 3.8 : **Types of geometry** in:
Howard, I., & Rogers, B. (2008-02-01). Psychophysics and analysis. In Seeing in Depth: Volume 1: Basic Mechanics/ Volume 2: Depth Perception 2-Volume Set. : Oxford University Press. Retrieved 14 Jan. 2022, from https://oxford.universitypressscholarship.com/view/10.1093/acprof:oso/9780195367607.001.0001/acprof-9780195367607-chapter-3. 


Section 24.1 : **Perspective** in:
Howard, I., & Rogers, B. (2008-02-01). Depth from monocular cues and vergence. In Seeing in Depth: Volume 1: Basic Mechanics/ Volume 2: Depth Perception 2-Volume Set. : Oxford University Press. Retrieved 14 Jan. 2022, from https://oxford.universitypressscholarship.com/view/10.1093/acprof:oso/9780195367607.001.0001/acprof-9780195367607-chapter-24. 

<BR><BR>