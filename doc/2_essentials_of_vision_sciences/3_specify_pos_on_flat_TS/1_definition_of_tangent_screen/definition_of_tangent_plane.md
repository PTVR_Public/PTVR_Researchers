Definition of a Tangent screen {#definitionOfTangentPlaneInVS}
==================================================

<!-- template_TSwSphere_CS=perimetric.ggb utilisé dans geogebra pour cette page  -->

## Mathematical definition

In the common language of Vision Science, the definition of a **tangent screen** (an important visual object implemented in PTVR) is based on the **mathematical** definition of the **tangent plane to a surface.**


> The tangent plane to a surface at a given point p is defined in an analogous way to the tangent line in the case of curves. It is the best approximation of the surface by a plane at p, and can be obtained as the limiting position of the planes passing through 3 distinct points on the surface close to p as these points converge to p.   
...  
> ... the tangent plane to a surface at a given point is the plane that "just touches" the surface at that point. The concept of a tangent is one of the most fundamental notions in differential geometry and has been extensively generalized.  
> The word "tangent" comes from the Latin tangere, "to touch". 
>
(retrieved from Wikipedia contributors, 'Tangent', Wikipedia, The Free Encyclopedia, 9 June 2022, 01:05 UTC, <https://en.wikipedia.org/w/index.php?title=Tangent&oldid=1092236138> [accessed 14 July 2022] )


## Vision Science terminology
In vision science, the tangent plane is usually called a **tangent screen** (this is also the terminology used by PTVR) by reference to the common use of monitor screens in Vision experiments, and it is actually a **tangent plane to a sphere** because it is related to the use of [**spherical** coordinate systems](@ref class_of_spherical_coordinate_systems).


As stated above, the strict definition of a tangent screen (or plane) tangent to a sphere implies that the screen must **touch** the sphere. This is the case for instance in Figures 1 and 2 that will be explained in more detais [here in the User Manual.](@ref definitionTangentScreen)

<!-- Figures 1 et 2   -->
<table>
<tr>
<td style="width:50%; height: auto; background-color:white;" align="center">
<img src="TSwCartesianCS.png" width="550">
</td>
<td style="max-width:50%; height: auto; background-color:white;" align="center">
<img src="TSwCartesianCS.gif" width="550">  
</td>

<tr>
<td>Figure 1: Grey Tangent screen at one meter from the Origin on the Z axis. The screen center is called SO (Screen Origin).  </td>

<td>Figure 2: Animation (horizontal rotation about Y) of figure 1 to allow you to build a better mental representation of the 3D structure of the figure (as if you were able to look at it from different angles)..   </td>
</tr>
</table>
<BR>



Note however that you will find figures in the PTVR documentation where a Tangent Screen will not touch the sphere displayed in the figure. An example, that is presented in details 
[here in the sub-section after the next](_2DperimetricCoordinateSystemInVS), is shown in Figure 3.


<!-- Figures 3  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="projection_of_perimetric_sphere_on_plane-animation_Ecc_6-36_HM180.gif" width="450pix"/>
</td>
</tr>

</td>
<tr>
<td>Figure 3: Projection of a point P, lying on the sphere, onto a tangent screen.  </td>

</tr>
</table>

The important point in Figure 3 is that the tangent screen is **tangent to a sphere (not displayed)** whose radius equals the distance between the Origin Point and the SO point. For visual clarity, this sphere is not shown and instead a sphere with a smaller radius is displayed as the projection of point P is the same whatever the sphere's radius (this is actually one important property of the [spherical coordinate systems](@ref classOfSphericalCoordinateSystems)).
<BR>