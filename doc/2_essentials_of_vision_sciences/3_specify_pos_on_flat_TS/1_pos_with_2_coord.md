Specifying 3D position with 2 coordinates on a Screen {#TSCoordinateSystemsInVisionScience}
==================================================

<img src="Fatty_watching_himself_on_TV.jpg" width="210" height="280" style="float:left; padding-right:20px" />  
<!-- the text below is NOW going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

Many experiments in Vision Science have been performed with human or non-human subjects watching visual stimuly displayed on a **flat 2D screen** (often a monitor screen).
In general, this screen is **perpendicular** to the subject's line of sight and is thus often called **tangent screen** (this is not exactly the case in this figure as the cat's head is not in front of the monitor).   


(Image from <a href="https://commons.wikimedia.org/wiki/File:Fatty_watching_himself_on_TV.jpg">cloudzilla</a>, <a href="https://creativecommons.org/licenses/by/2.0">CC BY 2.0</a>, via Wikimedia Commons).   
<!-- EC : "use this file on the web -> attribution -> click sur HTML et copier/coller -->

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

The simplest ways to simulate a **tangent screen** in a 3D environment are presented in the following sub-sections:

- @subpage definitionOfTangentPlaneInVS
- @subpage _2DcartesianCoordinateSystemInVS
- @subpage _2DperimetricCoordinateSystemInVs
