Basic Definition of visual angles {#simpleDefinitionsInVS}
============================

#### Visual angle subtended at the eye WITH MONOCULAR VIEWING
Most of the time, vision scientists are not interested in the **euclidean distances** (aka **linear distances**) measured in the 3D world.
They want to know how visual objects project onto the retina. In other words, knowing that an object is 10 meters high is not very informative. What vision scientists need is a description of the **visual input at the retinal level**: the basic question is therefore to assess the **angular** values of the projected images of objects onto the retina.
This has been traditionnally illustrated with schematic graphs similar to the one shown in Figure 1 [(Wikipedia link)](https://en.wikipedia.org/w/index.php?title=Visual_angle&oldid=1018114704). Here, an observer's eye looks at an arrow (segment BA) displayed in a frontal plane. The euclidean distance (often called **linear size**) between the arrow's endings B and A is S meters. The euclidean distance between the midpoint of the arrow and a special optical point O in the eye (called the fused/united/composite nodal point) is D meters (for accurate details see for instance [Atchison & Smith (2000)](https://doi.org/10.1016/B978-0-7506-3775-6.X5001-9). This euclidean distance is often called the monocular **Viewing Distance**.  

<div id="note">\emoji :wink: **Remark:** a **Visual Angle** is thus the angle subtended at the eye (actually at the fused nodal point) by an object in the 3D world.</div>


<!-- Figures 1 et 2 -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="640px-EyeOpticsV400y.jpg" width="550pix"/>
</td>
<td style="width:50%;" align="center">
<img src="visual_angle_with_static_POV_anim_radial_distance_with_one_eye.gif" width="550pix"/>  
</td>
</tr>

<tr>
<td>Figure 1: Typical figure and formula used to define a visual angle with a schematic eye. 
The point O (fused nodal point) is the point at which the angle subtended "at the eye" by the BA arrow is measured. (image from <a href="https://commons.wikimedia.org/wiki/File:EyeOpticsV400y.jpg">Ojosepa</a>, <a href="https://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>, via Wikimedia Commons) 
</td>
 
<td>Figure 2: Animation of Figure 1 to illustrate how the **visual angle V** depends on the **viewing distance D** for a constant **Euclidean distance S** (here 0.2 meters) between the endings A and B of the vertical arrow. Note that the point O represents the fused nodal point displayed in figure 1.

</td>
</tr>
</table>
<BR>
<!-- FIN de Figures 1 et 2 -->

#### Formula used for the size of a segment lying on a tangent screen
The traditional way of measuring a visual angle (indicated in figure 1) is the following:


    V = 2 * arctan ( S / (2 * D) ) **(formula 1)** 

where S and D stand respectively for the linear Size of the BA segment and for the viewing distance.

This formula has been extensively used for instance when a stimulus of constant linear size (here S = 0.2 meters) is displayed at different **viewing distances (D)** as illustrated in Figure 2.





#### Conditions of validity of formula 1

Formula (1) is a good approximation for most purposes in Vision Science. However, it must be born in mind that this approximation might not be good enough in certain experimental situations. We therefore emphasize below the conditions of validity of formula (1).

As emphasized in Figures 3 and 4, the visual angle provided by formula (1) is exact if and only if the **two requirements** below are met:

- **First** the BA segment must lie on [tangent screen](@ref _2DcartesianCoordinateSystemInVS) whose Origin (i.e. the orthogonal projection of point O) is represented by the pink point [S0 : Screen Origin](@ref _2DperimetricCoordinateSystemInVS) in Figure 3.

- **Second**, the position of the BA segment on the tangent screen must be such that the angle AMO is a **right angle (90° or Pi/2)** - with M being the midpoint of BA. This is the case when the BA midpoint (M) coincides with the Screen Origin (S0) or more generally when M lies on the [local X axis of the tangent screen](@ref globalAndLocalCoordinateSystems). This can be observed in the animation in figure 4 by checking that the visual angle predicted by formula (1) is the same as the exact value of V. This equivalence between predicted and actual value also occurs when a horizontal segment is moving vertically on the local Y axis of the tangent screen (not shown).

<!-- Figures 3 et 4  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="visual_angle_predicted_value.png" width="550"/>
</td>
<td style="width:50%;" align="center">
<img src="visual_angle_predicted_value_vertical_bar_on_local_X.gif" width="550"/>  
</td>
</tr>
<tr>
<td>Figure 3: Note in this figure that the **Screen Origin (SO)** of a tangent screen is always the orthogonal projection of point O on the tangent screen. 
</td>
<td>Figure 4: Visual angle subtended by vertical BA segment decreases as euclidean distance from SO to the midpoint M of BA gets larger. The key point here is to note that the visual angle predicted by formula (1) is always identical to the actual angle V. As in figures 1 and 2, the Euclidean distance S (here 0.2 meters) between the vertical arrow's endings A and B is constant.
</td>
</tr>
</table>
<BR>

<div id="note">\emoji :wink: **Remark:** Figure 4 also illustrates a trivial phenomenon : the visual angle of the BA segment gets smaller when the segment's eccentricity (with respect to SO) increases. This geometrical fact is well-known and occurs in many common situations: for instance, the height of the letters of a word displayed on a tangent screen and centered at SO subtend a vertical visual angle that decreases with their euclidean distance from SO. See [animation here](@ref retinalImagesInVS) and [Pelli et al., 2007](https://doi.org/10.1167/7.2.20).
</div>

<BR><BR>
#### Example of a situation where prediction of formula 1 is not exact
<!-- Figures 5  -->
<img src="visual_angle_predicted_value_2.png" width="550" height="400" style="float:left; padding-right:20px;">
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->
<BR>

Very often in Vision Research, although visual stimuli lie on a tangent screen (i.e. they meet the first requirement of formula (1)), they do not meet the second requirement described above (i.e. angle OMA, or equivalently OMB, should be a right angle). This is not an issue for most purposes because the difference between visual angle predicted by formula (1) and actual angular value is usually small.  
For instance, in figure 5, the position of point M is now such that angle AMO (108.32°) is not a right angle any longer. The consequence is that the actual value of the visual angle V is 17.09° (in orange) whereas its predicted value from formula (1) is 17.95°, a small enough difference for many purposes.


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

**Figure 5**: Example of a segment BA lying on the tangent screen and whose angle V subtended at the eye (i.e. at point O) is slightly different from the angular value predicted by formula (1).
<BR><BR>



<BR>
#### Links
Atchison, D. A., & Smith, G. (2000). Optics of the Human Eye. Elsevier. https://doi.org/10.1016/B978-0-7506-3775-6.X5001-9

Pelli, D. G., Tillman, K. A., Freeman, J., Su, M., Berger, T. D., & Majaj, N. J. (2007). Crowding and eccentricity determine reading rate. J Vis, 7, 20 1-36. https://doi.org/10.1167/7.2.20. (their Appendix B is especially relevant here)

Wikipedia contributors. (2021, April 16). Visual angle. In Wikipedia, The Free Encyclopedia. Retrieved 17:11, February 15, 2022, [from https://en.wikipedia.org/w/index.php?title=Visual_angle&oldid=1018114704](https://en.wikipedia.org/w/index.php?title=Visual_angle&oldid=1018114704)
<BR><BR>
