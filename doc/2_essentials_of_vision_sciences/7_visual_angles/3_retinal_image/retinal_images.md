Retinal Images {#retinalImagesInVS}
============================

This section illustrates with figures :  


- a/ how a point on a tangent screen projects onto the retina (figures 1 and 2)


- b/ how a segment lying on a tangent screen projects onto the retina (figure 3). 


<!-- Figures 1 et 2 -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="TSwPerimetricCS+retinalProjection.png" width="550pix"/>
</td>
<td style="width:50%;" align="center">
<img src="TSwPerimetricCS+retinalProjection_horizontal_rotation.gif" width="550pix"/>  
</td>
</tr>

<tr>
<td>Figure 1: Geometry of retinal image formation is illustrated with a static figure of a very simplistic eye (blue sphere). The AB segment is moving on a Tangent screen while the images of A and B are projected onto the retina. Note that the actual nodal point of an eye is more anterior than the sphere's center of rotation used here as a proxy of the fused nodal point..
</td>
 
<td>Figure 2: Horizontal rotation of Figure 1 to help you build a mental representaton of the 3D structure.
</td>
</tr>
</table>
<BR>
<!-- FIN de Figures 1 et 2 -->


<!-- Figures 3 et 4 -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="TSwPerimetricCS+retinalProjection.gif" width="550pix"/>
</td>

</tr>

<tr>
<td>Figure 3: Animation to show how the angular size of the retinal image of AB segment varies as a function of the horizontal euclidean distance between S0 (tangent Screen Origin) and AB segment.
</td>
 

</tr>
</table>
<BR>
<!-- FIN de Figures 3 et 4 -->

<BR><BR>







<BR>
#### Links
Atchison, D. A., & Smith, G. (2000). Optics of the Human Eye. Elsevier. [https://doi.org/10.1016/B978-0-7506-3775-6.X5001-9](https://doi.org/10.1016/B978-0-7506-3775-6.X5001-9) - see notably the section "Image formation : the focused paraxial image".

<BR><BR>
