Approximation of Visual Angles with binocular viewing {#approximationsOfVisualAngles}
============================
<!-- visual_angle_predicted_value.ggb utilisé dans geogebra pour cette page  -->


When viewing is binocular, which a very common experimental situation, the visual angle subtended at each of both eyes (the monocular angle) is **often approximated** by applying formula (1) to a **virtual eye** **O** located midway between the two eyes.
This is schematically represented in Figure 1.
A point **O** is displayed midway between the eyes separated by a typical interpupillary distance of 6.5 cm.  
In this example, the exact visual angle subtended at right eye (brown angle) and left eye (angle not shown) is 19.97°.  
This angular value is usually approximated by the exact angular value (here 20°) subtended at the virtual eye (point O). The difference between these two exact values is usually considered as a valid approximation for most purposes.

The difference between these two exact values becomes larger as the point M gets further from the point SO on the tangent screen. An example is shown in figure 2 where M moves along the local x axis of the tangent screen. 
It is the responsibility of the researcher to assess whether approximating the monocular visual angle by the angle subtended at O is still valid with large eccentricies of M.


<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="visual_angle_with_static_POV_with_two_eyes.png" width="550pix"/>
</td>
<td style="width:50%;" align="center">
<img src="visual_angle_with_static_POV_with_two_eyes.gif" width="550pix"/>  
</td>
</tr>

<tr>
<td>Figure 1: Static view showing the **exact** visual angles subtended at virtual point O (orange) and at the right eye (brown) by the BA segment.
Note the small difference between these two visual angles. 
</td>
 
<td>Figure 2: Animation with the midpoint M of vertical segment BA moving on the local X axis of the tangent screen. Note that the difference between both angles increases with the eccentricity of M.
</td>

</tr>
</table>
<BR>
<!-- FIN de Figures 1 et 2  -->

<div id="note">\emoji :wink: **Remark:** In the methods section of most papers using binocular viewing, the **viewing distance** is defined as the euclidean distance between O and SO (it is usually measured with a ruler) - and this value is assigned to the D paramter of formula 1 ([see section 'Basic definition of visual angles'](@ref simpleDefinitionsInVS)) to measure angular values of any segment lying on the tangent screen, even when the segment's center is not coinciding with SO.</div>












