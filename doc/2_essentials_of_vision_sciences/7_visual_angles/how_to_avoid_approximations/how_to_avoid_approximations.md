How to avoid approximations {#howToAvoidApproximations}
============

For some theoretical reasons, it is sometimes necessary to calculate the exact angle subtended by objects displayed in the world when these objects are not displayed with the standard location that was described [in section 'Basic definition of visual angles'](@ref simpleDefinitionsInVS).

Some resources can be helpful to avoid delving too much in trigonometry if you want to calculate these exact angles.

One nice attempt for this purpose when stimuli are displayed on a tangent screen can be found on the [SR Research Ltd company's website](https://www.sr-research.com/).  
They propose an interactive interface to deal with different scenarios depending on the location of a segment displayed on the tangent screen [(see here).](https://www.sr-research.com/visual-angle-calculator/)

Figure 1 shows how they first deal with the **standard experimental situation** where the segment's center is located on the point SO (Screen Origin in PTVR terminology) - [see section 'Basic definition of visual angles' for more information on point SO.](@ref simpleDefinitionsInVS). This situation (see right part of the figure) is called **centered** segment in the SR Research terminology. 



<!-- Figures 1  -->
<table>
<tr>
<td style="width:1000%;" align="center">
<img src="convert_pixels_and_degrees_centered.png" width="990pix"/>
</td>
</tr>

<tr>
<td>Figure 1: A segment is displayed with its center located at the point called **SO** in PTVR (i.e. at the orthogonal projection of the eye). [Retrieved on 21. June 2022 from SR Research Ltd website](https://www.sr-research.com/visual-angle-calculator/)
</td>
</tr>
</table>
<BR>
<!-- FIN de Figures 1  -->

A second situation, which requires different trigonometry calculations, is shown in Figure 2. This time the segment is referred to as being **one-sided** : one of its endings is at the Screen Origin (SO) location.


<!-- Figure 2  -->
<table>
<tr>
<td style="width:1000%;" align="center">
<img src="convert_pixels_and_degrees_one_sided.png" width="990pix"/>
</td>
</tr>

<tr>
<td>Figure 2: A segment is displayed with one of its endings at the point called **SO** in PTVR (i.e. at the orthogonal projection of the eye). [Retrieved on 21. June 2022 from SR Research Ltd website](https://www.sr-research.com/visual-angle-calculator/)
</td>
</tr>
</table>
<BR>
<!-- FIN de Figures 2  -->

The third situation which also requires different trigonometry calculations, is shown in Figure 3. The segment is referred to as being **off-center** : it does not contain the Screen Origin (SO) location and it is horizontal or vertical.

<!-- Figure 3  -->
<table>
<tr>
<td style="width:1000%;" align="center">
<img src="convert_pixels_and_degrees_off_center.png" width="990pix"/>
</td>
</tr>

<tr>
<td>Figure 3: A segment is displayed horizontally or vertically without containing the point called **SO** in PTVR (i.e. the orthogonal projection of the eye). [Retrieved on 21. June 2022 from SR Research Ltd website](https://www.sr-research.com/visual-angle-calculator/)
</td>
</tr>
</table>
<BR>
<!-- FIN de Figures 3  -->




<BR>
#### Links
Atchison, D. A., & Smith, G. (2000). Optics of the Human Eye. Elsevier. https://doi.org/10.1016/B978-0-7506-3775-6.X5001-9

<BR>