Visual Angles subtended by objects {#visualAnglesInVS}
============

<BR>
<span style="font-size:28px;">
General Question : What is the link between Euclidean distances in the world and angles on the retina ?
</span>
<BR><BR>


<img src="EB1911_Vision_-_Ideal_or_Schematique_Eye.jpg" width="640" height="400" style="float:left; padding-right:20px" >
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->


**Figure 1**: Image from unknown artist in 1911 (Public domain): Ideal or Schematic eye. Legend: A, summit of cornea; SC, sclerotic; S, Schlemm's canal; CH, choroid; I, iris; M, ciliary muscle; R, retina; N, optic nerve; HA, aqueous humour; L, crystalline lens, the anterior of the double lines on its face showing its form during accommodation; HV, vitreous humour; DN, internal rectus muscle; DE, external rectus; YY′, principal optical axis; ΦΦ, visual axis, making an angle of 5° with the optical axis; C, centre of the ocular globe. The cardinal points of Listing: H₁H₂, principal points; K₁K₂, nodal points; F₁F₂, principal focal points. The dioptric constants according to Giraud-Teulon: H, principal points united; Φ₁Φ₂, principal foci during the repose of accommodation; Φ′₁Φ′₂, principal foci during the maximum of accommodation; O, fused nodal points. (Image from <a href="https://commons.wikimedia.org/wiki/File:EB1911_Vision_-_Ideal_or_Schematique_Eye.jpg">Unknown artistUnknown artist</a>, Public domain, via Wikimedia Commons).  
<BR><BR>

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->




The current section is divided into the following sub-sections:

- @subpage simpleDefinitionsInVS
- @subpage approximationsOfVisualAngles
- @subpage howToAvoidApproximations
- @subpage retinalImagesInVS


##Links##
Atchison, D. A., & Smith, G. (2000). Optics of the Human Eye. Elsevier. https://doi.org/10.1016/B978-0-7506-3775-6.X5001-9

