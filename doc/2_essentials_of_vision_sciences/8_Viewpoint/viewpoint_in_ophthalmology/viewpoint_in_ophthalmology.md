Viewpoints in ophthalmology  {#viewpointsInOphthalmology}
============
<!-- Figures de Geogebra
C:\Users\castet\ownCloud\DEVISE\Devise non partagé\site web\Visual Angles\


figures 3 et 4
visual_angle_with_static_POV_and_perimeter.ggb
visual_angle_with_static_POV_and_perimeter.png
visual_angle_with_static_POV_and_perimeter_binocular.png

figures 5 et 6
visual_angle_with_static_POV_binocular_FIXATION.png
pour le gif:
visual_angle_with_static_POV_anim_eccentricity.ggb

-->



In ophthalmology (and vision research), the concept of a **viewpoint** (as defined in [Viewpoint definitions](@ref viewpointDefinitionInVS)) is most often **implicit**. The present section gives some examples to render this concept more explicit and to show how useful it will be when programming Virtual Reality experiments in PTVR.  


#### Perimetry with monocular vision
The goal of monocular perimetry is to establish a quantitative description of the visual field of a subject's eye. In brief, the subject's task is to detect whether he/she perceives **points of light** (usually called **targets**) that are successively displayed at different eccentricities and half-meridians to allow a quantitative description of the retinal images of these targets.


To produce accurate eccentricities and half-meridians, the most common approach is to **immobilize the subject's head** and ask the subject to keep his/her **gaze fixed** (we leave aside for now the issue of controlling if the subject is correctly fixating). Figure 1 illustrates one way of immobilizing the head with a chin rest in a vintage **perimeter**  (visual stimuli are displayed on the semi-circular "arm" - see the green point). The importance of the chin rest is further illustrated with a more sophisticated "perimeter" where stimuli are displayed within a "bowl" (Figure 2).
Then, in both cases, the subject's **eye** must be kept **fixed** on a point (usually called the **Point Of Fixation - POF**) that is displayed in the middle of the arm or bowl.

As can be seen in these figures, perimeters impose (thanks to the chin (and forehead) rest) a well-defined **distance between the eye and the POF**. This control allows a stringent control of visual angle values which notably guarantees that **the retinal images of the targets are correctly specified.**


<div id="note">\emoji :wink: **to remember** : 
It is crucial in perimetry to place the eye to be tested at a particular 3D position that is called **viewpoint** in the PTVR documentation. In this monocular case, PTVR calls it a **monocular viewpoint**. </div>


	
<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Visual_field_test.jpg" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Goldmann_perimeter.jpg" width="300pix"/>  
</td>
</tr>

<tr>
<td>Figure 1: Illustration of the importance of stabilizing (usually with a chin rest) the subject's or patient's head in vision and ophthalmology research. The subject, whose head is immobilized, has to keep his gaze fixed and report whether he perceives points of light displayed at different eccentries along the perimeter's "arm" (here along the horizontal meridian, i.e. on the 0° and 180° half-meridians). (image from <a href="https://commons.wikimedia.org/wiki/File:Visual_field_test.jpg">Evgeniy Aparshin</a>, <a href="https://creativecommons.org/licenses/by/4.0">CC BY 4.0</a>, via Wikimedia Commons) 
</td>
 
<td>Figure 2: Same as Figure 1, except that the perimeter is more sophisticated. Notably, points of light are displayed on a hemispheric "bowl". The subject is on the RIGHT of the image (with his left eye covered) and the operator/experimenter is on the left. 
(Goldmann perimeter Image from <a href="https://commons.wikimedia.org/wiki/File:Fotothek_df_n-10_0000776.jpg">Deutsche Fotothek‎</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0/de/deed.en">CC BY-SA 3.0 DE</a>, via Wikimedia Commons). 
</td>

</tr>
</table>
<BR>
<!-- FIN de Figures 1 et 2  -->

The points described qualitatively in the previous paragraphs are now explained in a more quantitative way. We have seen that the position of the tested eye with respect to the  perimeter's **Point Of Fixation (POF)** is very important to allow accurate and reproducible description of visual fields (in terms of retinal eccentricity and half-meridian).   
Figure 3 illustrates this key point with a schematic view of the perimeter setup shown in Figure 1. The default PTVR coordinate system is used ([see RUF coordinate system](@ref cartesianCoordinateSystemInVS) ) and we arbitrarily decide that the tested eye is the left eye. Figure 3 shows that the position of the left eye has to meet three requirements:
- the left eye must lie in the same horizontal plane as that of the **Point Of Fixation (POF)**, i.e. the left eye and POF must lie in the XoZ plane.
- the left eye and the POF must lie on a line perpendicular to the plane tangent to the "arm" at POF. In other words, the left eye and POF must lie on the Z axis.
- the left eye and any visual stimulus on the perimeter "arm" must be separated by a constant radial distance, i.e. the left eye must be at the center of a circle built form the semi-circular "arm" (remember that visual stimuli are points of light randomy displayed across trials somewhere on the "arm").

The position occupied by the left eye with respect to the perimeter thus allows accurate positioning of visual stimuli in terms of retinal projections.


<!-- Figures 3 et 4  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="visual_angle_with_static_POV_and_perimeter.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="visual_angle_with_static_POV_and_perimeter_binocular.png" width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 3:  Schematic display of the perimeter setup shown in Figure 1 illustrating the importance of having a coordinate system correctly oriented and with its origin placed at the perimeter's viewpoint. In this figure, the subject has placed his/her left eye right (the right eye is covered) at the viewpoint/origin so that the eccentricities of the target points (on the perimeter's arm) will accurately correspond to their retinal images in the subject's eye.
</td>
<td>Figure 4: Same as in Figure 3 except that the goal of the research is now to measure a **binocular** visual field. Both eyes are now uncovered and fixate the POF (brown arrows). The subject's chin must be aligned with the Z axis in this binocular exam. See the section Final Notes about the angular approximations implied by this binocular situation.
</td>
</tr>
</table>
<BR>

<div id="note">\emoji :wink: **To remember:**  
All this can be summarized by the two following key points:
1/ The **viewpoint** is a special 3D position defined by the geometrical design of the perimeter.
2/ Placing the **origin of the coordinate system** at this **viewpoint** with a correct orientation allows a correct specification of the targets positions in the perimeter.
</div>


#### Perimetry with binocular vision

If the ophthalmologist now wants to measure a **binocular visual field** with the same perimeter device, the situation is the same except that the head sagittal plane [see here](@ref HeadAxesAndPlanesInVS) must now be aligned with the (YoZ) plane so that both eyes lie on each side of this plane (Figure 4). This experimental situation, where both eyes are open and fixate the POF, is probably one of the most frequent in Vision Research. In this case, the **viewpoint** is exactly between the eyes and PTVR calls it a **binocular viewpoint**. The [section on Visual Angles approximations](@ref approximationsOfVisualAngles) explains why (and in what conditions) the angular values at each of the two eyes are valid although the origin of the coordinate system lies midway between them (see also one of the final notes at the end of the present page).


To sum up, the position of the eye(s) with respect to the **Point Of Fixation (POF)** AND to the **visual stimuli** is crucial for accuracy of visual stimulation (in terms of retinal angular values such as excentricity) and for reproducibility of results. Although all vision scientists are quite aware of this, there does not seem to be a consensual name to refer to the position that the eye(s) should occupy to meet all the requirements described above. This is why the PTVR documentation proposes to use the name **viewpoint**.


#### Take Home Message
The **viewpoint** is a concept that is ubiquitous, although often implicit, in Vision Research. We thought that it deserved a clarification and a very explicit name in PTVR because it will be often used in the PTVR documentation ([especially in the Manual for Users](@ref UserManual)). 


#### Some final notes

a/ The most appropriate coordinate system for perimetric exams (although not shown in Figures 3 and 4) is actually the **perimetric** coordinate system with its **Origin at the viewpoint**. In this system, each point of light displayed on the perimeter can be uniquely defined by its eccentricity, its half-meridian and its radial distance.

b/ The notion of the **viewpoint** used in PTVR is quite close to the notion of the **Cyclopean Point** essentially used in the context of binocular vision - See Howard & Rogers (2008)  (note that the cyclopean "point" is not the same concept as the cyclopean "eye"). 

c/ While Ophthalmology often employs semi-circular "arms" or "bowls" to display visual stimuli, vision scientists have often been using [flat 2D screens](@ref TSCoordinateSystemsInVisionScience) as schematized in Figure 5. However, the definition of the **viewpoint** is exactly the same. 

d/ As already emphasized, the experimental binocular situation in which the viewpoint is between the eyes is probably the most frequent in vision research. 
Figure 6 illustrates that, in this case, the excentricity of a point P (here moving along the 90° half-meridian) subtended at the viewpoint is a good approximation of the same measure subtended at one of the eyes (here the left). The main reason is that the distance between the eyes is much smaller than the viewing distance of the screen in most experimental settings.


<!-- Figures 5 et 6  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="visual_angle_with_static_POV_binocular_FIXATION.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="visual_angle_with_static_POV_anim_eccentricity.gif" width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 5: </td>
<td>Figure 6:   </td>
</tr>
</table>
<BR>

#### Links
About **Cyclopean point or location**:
As **Howard & Rogers (2008)** put it: Cyclopean coordinates are very important in Vision Science to give the directions of objects in binocular space and for specifying binocular eye movements. ... it is useful to specify binocular visual direction and binocular eye movements with respect to a **single** set of coordinate axes. We refer to these as **cyclopean coordinates**. The line joining the nodal points in the two eyes is the interocular axis, and the point on the interocular axis midway between the eyes is the **cyclopean point**. (section 15.2.2e - Cyclopean coordinates in : [Howard, I. P., & Rogers, B. J. (2008). Chapter 15. Binocular correspondence and the horopter. In Seeing in Depth (Vol. 2). Oxford University Press.]( https://oxford.universitypressscholarship.com/view/10.1093/acprof:oso/9780195367607.001.0001/acprof-9780195367607-chapter-15) ).

<BR><BR>