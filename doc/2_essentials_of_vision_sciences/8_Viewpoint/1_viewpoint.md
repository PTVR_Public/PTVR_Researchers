Viewpoint {#viewpointInVS}
============

<BR>
<span style="font-size:28px;">
What is a **viewpoint** in PTVR ?
</span>
<BR><BR>


<img src="A_vieu_of_Plymouth_(BM_J,1.156).jpg" width="640" height="865" style="float:left; padding-right:20px" >
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

<BR><BR>
This green show-box offers two **viewpoints** of Plymouth thanks to two round holes. Lord Amherst is using the viewpoint on the left.

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

**Figure 1**: A "vieu" of Plymouth: A raree-show man (right) is exhibiting his peep-show, in a box inscribed "A Vieu of Plymouth", to Lord Amherst, in profile to the right, who stoops down, his hands on his knees, to look through one of the two round holes. The showman says, "There you see Cannons without Carriages and Carriages without Cannons. There you see Generals without Orders there you see &c. &c." The show-box is supported on trestles. Amherst is in general's uniform, wearing the ribbon of the Bath. Behind him, partly cut off by the left margin of the print, stands a Grenadier at attention, holding a musket; he watches the general with a grin. Beneath the design is engraved:   
"Col Mushrooms Compts to Lord Am------t recommends this cheap but Satisfactory mode of viewing distant Garrisons hopes his Lordship has received the Golden Pippins a few of them are for his Secretary." 4 May 1780  
Hand-coloured etching  
(image from <a href="https://commons.wikimedia.org/wiki/File:A_vieu_of_Plymouth_(BM_J,1.156).jpg">British Museum</a>, Public domain, via Wikimedia Commons)
<BR><BR>



The current section is divided into the following sub-sections:

- @subpage viewpointDefinitionInVS
- @subpage viewpointsInOphthalmology
