Viewpoint definitions  {#viewpointDefinitionInVS}
============

Most of the time, vision scientists need to accurately define the positions of visual objects **with respect to the eyes**. This is mainly because vision scientists want to relate the positions of these distant objects to their **retinal positions** and eventually to their representations in cerebral retinotopic visual areas.

To emphasize the importance of this point, the PTVR documentation uses the **viewpoint** concept.

The concept of a **viewpoint** in PTVR is very close to one of its definitions found in many dictionaries, as for instance: <span style="background-color:yellow"> "A viewpoint is a place from which you can get a good view of something".</span> ([Online Collins dictionary](https://www.collinsdictionary.com/dictionary/english), [Wikipedia contributors - 2021](https://en.wikipedia.org/w/index.php?title=Scenic_viewpoint&oldid=1061800316)  )

A touristic **viewpoint** is often associated with an **orientation table** (Figure 1).

In some countries, an **orientation table** is called a **viewpoint indicator** (Figure 2) or a ... **toposcope**. Whatever the name, this is "a kind of graphic display erected at viewpoints on hills, mountains or other high places which indicates the direction, and usually the distance, to notable landscape features which can be seen from that point." [(Wikipedia contributors - 2018)](https://en.wikipedia.org/w/index.php?title=Toposcope&oldid=844807054)


#### What is a viewpoint in PTVR ?

<span style="background-color:yellow">
Inspired by the definitions above, a **viewpoint** in PTVR is a **virtual point** in 3D space **where the eyes of a subject should be positioned** to allow an experimenter to control accurately the locations of visual stimuli with respect to the eyes.
</span>

Controlling the position of stimuli with respect to the eyes is essential to assess the angular positions and sizes of these stimuli (see [section on visual angles](@ref visualAnglesInVS)).


	
<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Sanilhac-table_d'orientation-tour_de_Brisson.jpg" width="550pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Detail_of_the_northern_Tormain_Hill_viewpoint_indicator_-_geograph.org.uk_-_1307740.jpg" width="550pix"/>  
</td>
</tr>

<tr>
<td>Figure 1: **Viewpoint** with its **orientation table** (Ardèche, France). (Image from <a href="https://commons.wikimedia.org/wiki/File:Sanilhac-table_d%27orientation-tour_de_Brisson.jpg">Robin Chubret</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons)
</td>
 
<td>Figure 2: Detail of the northern Tormain Hill **viewpoint indicator**, near to Ratho, Edinburgh, Great Britain.
(Image from <a href="https://commons.wikimedia.org/wiki/File:Detail_of_the_northern_Tormain_Hill_viewpoint_indicator_-_geograph.org.uk_-_1307740.jpg">M J Richardson</a>, <a href="https://creativecommons.org/licenses/by-sa/2.0"> CC BY-SA 2.0</a>, via Wikimedia Commons)

</td>

</tr>
</table>
<BR>
<!-- FIN de Figures 1 et 2  -->


Some of these **orientation tables** emphasize the necessity for the observer to be located at a **restricted location** (namely at the center of the circular table) to allow a perfect **alignment of the gaze axis** with any direction on the table pointing at a "notable landscape feature" (Figure 3).

Note however that for any **distant** stimulus such a the Mont-Blanc in Figure 4, the alignment will be close to perfect even if the observer does not stand precisely in the center of the orientation table.


<!-- Figures 3 et 4  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="1024px-Haut_du_tot_-_Table_orientation_3.jpg" width="550pix"/>
</td>
<td style="width:50%;" align="center">
<img src="A_beatifull_Orientation_tablet_at_Roche_de_Mio_2739_m.jpg" width="550pix"/>  
</td>
</tr>
<tr>
<td>Figure 3: An enclosing orientation table. (<a href="https://commons.wikimedia.org/wiki/File:Haut_du_tot_-_Table_orientation_3.jpg">photography taken by Christophe Finot</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, via Wikimedia Commons).
</td>
<td>Figure 4: A beautiful orientation table at Roche de Mio (2739 m), with in North-East direction at 39 km the Mont Blanc clearly visible (France). You can walk on this table !  Ideally, the observer has to stand precisely in the center of the circular orientation table.  
(Image from <a href="https://commons.wikimedia.org/wiki/File:A_beatifull_Orientation_tablet_at_Roche_de_Mio_2739_m,_with_in_North-East_direction_at_39_km_the_Mont_Blanc_clearly_visible_-_panoramio.jpg">Henk Monster</a>, <a href="https://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>, via Wikimedia Commons)
</td>
</tr>
</table>
<BR>

#### Take-home message
In vision and ophthalmology research, it is often necessary to specify the **angular values (eccentricity and size) that visual objects subtend at the observer's eyes**. This is often achieved by placing the eyes at a fixed position which is called **viewpoing** in PTVR documentation.  
Using this **viewpoing** terminology will prove useful when programming Virtual Reality experiments with PTVR. 




The next section ([Viewpoint and ophthalmology](@ref viewpointIllustrations)) illustrates how the **viewpoing** terminology helps describe the logic of some essential ophthalmology techniques 



#### Links
Wikipedia contributors. (2021, December 24). Scenic viewpoint. In Wikipedia, The Free Encyclopedia. Retrieved 11:08, March 12, 2022, from [https://en.wikipedia.org/w/index.php?title=Scenic_viewpoint&oldid=1061800316](https://en.wikipedia.org/w/index.php?title=Scenic_viewpoint&oldid=1061800316)  

Wikipedia contributors. (2018, June 7). Toposcope. In Wikipedia, The Free Encyclopedia. Retrieved 19:15, March 11, 2022, from [https://en.wikipedia.org/w/index.php?title=Toposcope&oldid=844807054](https://en.wikipedia.org/w/index.php?title=Toposcope&oldid=844807054)
<BR>
<BR>
