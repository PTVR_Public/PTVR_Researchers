Text and typography {#TextAndTypography}
============


<h1>Typography key concepts</h1>

In your experiments, you will probably want to display texts, which are 2D objects, in your 3D environments on screens.

Displaying text requires important knowledge and information explained here.


***

<h2>Difference between a character and a glyph </h2>

The letter "A" is a character, while <img src="glyphes.PNG"/><br> are glyphs representing "A".

<h2>The Font</h2>
A font is a collection of glyphs, usually with at least one glyph associated with each character in the font's character set.
Historically, a font was a collection of small metal blocks each bearing the engraved image of a letter. There was traditionally a different font for each point size.


<h2>Metrics</h2>

<h3>The X-height</h3>

The X-height is the height of a lowercase letter (such as "x", "v" or "z") above the Baseline.  

<img src="xheight.PNG"/><br>

<h3>The Ascender / ascent</h3>

The ascender is the portion of a lowercase letter that extends above the x-height (as in the letters "l" or "k" for example). The ascent is the ascending height of the character.  
<img src="ascender.PNG"/><br>

<h3>The Descender / descent</h3>

The descender is the portion of a lowercase letter that extends below the Baseline (as in the letters "j" or "q" for example). The descent is the descending height of the character.  
<img src="descender.PNG"/><br>

<h3>The Cap-Height </h3>

The Cap-Height is the height of a capital letter above the Baseline  
<img src="capheight.PNG"/><br>

<h3>The Baseline</h3>

The Baseline is the horizontal line on which the letters (Latin, Greek and Cyrillic) sit.  
<img src="baseline.PNG"/><br>



## X-height as basic measure

In PTVR, when defining the text size, we define the size of the x-height.
According to the study by Drs. Legge and Bigelow, it is interesting to use the height of x for several reasons :   
«
1. Lowercase predominates in most English texts [...]. In addition, 96% of lowercase letters have
graphical features at the x-height, so x-height features are approximately 16 times more common in
text than capital height features.

2. It is easier to measure physical x-height than body
size, because graphical features (e.g., arches, serifs,
and terminals) cluster along the baseline and x-line,
whereas body height is more difficult to measure
because ascenders and descenders are more sparsely
distributed. [...]

3. The x-height region (the horizontal band between
baseline and x-line) contains most of the black area
of text type. [...]

4. Although not proven by rigorous laboratory studies,
typographic opinion (Williamson, 1966) and informal
observation indicate that x-height is a more salient
determinant of perceived type size than is body size.[...]

»

[Gordon E.Legge, Charles A.Bigelow, Does print size matter for reading? A review of findingsfrom vision science and typography, Journal of Vision, 2011](http://j.pelet.free.fr/publications/design/Does_print_size_matter_for_reading_A_review_of_%EF%AC%81ndings_from_vision_science_and_typography.pdf)

## Le EM
Lorsque l'on affiche une font, on utilise le mot "em" pour représenter la taille de la font. Si une font est affichée en 12 points, alors 1em sera égal à 12points. Si la font est affichée en 18 points, 1em sera alors égal à 18 points.  
Dans la plupart des polices PostScript, il y a 1000 unités pour 1em, les polices TrueType ont souvent 1024 ou 2048 unités pour 1em (TrueType recommande d'utiliser une puissance de deux).  
Un « em-square », ou cadratin en français, est un carré d'un em de chaque côté. Dans la typographie traditionnelle (lorsque chaque lettre était coulée dans du métal), le glyphe devait être dessiné à l'intérieur du carré em.  


<table>
<caption> Lettre minuscule x dans un em square de 2048*2048 unités (représenté en rouge)</caption>
<tr><td><img src="em.png" />  
</table>

Application concrète :  
On prend un tiret en PostScript font qui fait 500 unités de long, et dont la font fait 1000 unités pour 1 em. On affiche ce tiret en 12 points. Ce tiret sera donc dessiné avec une longueur de 500/100 * 12 = 6 points. 

## Police proportionnelle

Dans une police proportionnelle, la place qu’occupe une lettre dans un mot est proportionnelle à sa largeur.

<table>
<tr><td><img src="police_proportionnelle .png" /> 
<tr><td> La police Arial est proportionnelle : on voit que le i prend moins de place que le m dans le mot : « impossible »
</table>

## Police mono/fixe  

Dans une police mono, toutes les lettres occupent la même place dans un mot, quelle que soit leur largeur.

<table>
<tr><td><img src="police_mono.png" /> 
<tr><td> La police Courrier New est fixe : on voit que le i prend autant de place que le m dans le mot : « impossible »
</table>






<h2>Links</h2>

- [FontForge](https://fontforge.org/en-US/documentation/ ) : an open source font editor with a great and complete documentation about text in general
- [TextMeshPro documentation](http://digitalnativestudios.com/textmeshpro/docs/ ) : system used to create Texts in Unity on which is based the PTVR Text object.
-  [Gordon E.Legge, Charles A.Bigelow, Does print size matter for reading? A review of findingsfrom vision science and typography, Journal of Vision, 2011](http://j.pelet.free.fr/publications/design/Does_print_size_matter_for_reading_A_review_of_%EF%AC%81ndings_from_vision_science_and_typography.pdf)