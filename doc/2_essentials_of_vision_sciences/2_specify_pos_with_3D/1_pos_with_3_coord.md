Specifying 3D position with 3 coordinates {#CoordinateSystemsInVisionScience}
==================================================


<img src="Flammarion_Woodcut_1888_Color_2.jpg" width="500" height="400" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

<span style="font-size:130%;">
Several 3D **coordinate systems** (or **frames of reference**) have been commonly used in Vision Science to specify the **position** of objects in 3D space. We present some of the most important of these coordinate systems. 

A common abbreviation in PTVR is **CS** for **Coordinate System**.

</span>

(Image from <a href="https://commons.wikimedia.org/wiki/File:Flammarion_Woodcut_1888_Color_2.jpg">AnonymousUnknown author</a>, Public domain, via Wikimedia Commons)

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->


The present section is divided into the following sub-sections:


- @subpage cartesianCoordinateSystemInVS

The 3 following sub-sections concern the class of the **SPHERICAL** coordinate systems.   
- @subpage classOfSphericalCoordinateSystems
- @subpage perimetricCoordinateSystemInVS  
- @subpage harmsCoordinateSystemInVS