3D Cartesian coordinate system {#cartesianCoordinateSystemInVS}
==============================


In a 3D cartesian coordinate system (aka XYZ system), the position of a point is defined by 3 coordinates as illustrated in Figure 1. In this example, the coordinates of the Point P are:    
– **X** : 0.25 meters  
– **Y** : 0.20 meters  
– **Z** : 0.30 meters  

Several different conventions for defining the axes directions of a 3D cartesian coordinate system are used in Mathematics and in Vision Science. 
The convention for the coordinate system displayed in Figure 1 is the **left-handed** rule (see subsection below). This is the **convention adopted by PTVR**.

<div id="note">\emoji :sunglasses: **Tip**: One advantage of this convention is the following : the first two dimensions (X and Y) should remind you of the convention used in most two-dimensional X-Y figures in the real world (e.g. a plot of weight vs. height). The only point you have to visualize and remember is that the Z axis goes beyond the X-Y plane (receding axis). </div>

This xyz structure is used in some graphics programs (e.g. [Unity](https://unity.com/), [RenderMan](https://renderman.pixar.com/)). For users familiar with Unity, note that the correspondence between colors and axes is the **same in PTVR and in Unity**.


<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="cartesianCoordinateSystemInVS_3D.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="cartesianCoordinateSystemInVS_3D.gif" width="450pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: Representation of the left-handed cartesian coordinate system used in PTVR (following Unity).
</td>

<td>Figure 2: Animation (horizontal rotation) of figure 1 to allow you to build a better mental representation of the 3D structure of the figure (as if you were able to look at the static coordinate system from different angles). </td>
</tr>
</table>
<BR>

<div id="note">\emoji :wink: **Note 2:** If you're impatient to use cartesian coordinates in PTVR, go directly to the [PTVR User Manual](@ref in3DcartesianCS). </div>


# Left-handed coordinate system #

<img src="3D_Cartesian_Coodinate_Handedness.jpg" width="400" height="200" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

You can use this image as a mnemonic to remember the spatial configuration of a **left-handed** coordinate system
 (Image from <a href="https://commons.wikimedia.org/wiki/File:3D_Cartesian_Coodinate_Handedness.jpg">Primalshell</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, via Wikimedia Commons). 


Knowing if a system is left- or right-handed is useful notably to specify the orientation of 3D objects [(Wikipedia contributors. (2022, February 3))](https://en.wikipedia.org/w/index.php?title=Right-hand_rule&oldid=1069678571).
<!-- EC : "use this file on the web -> attribution -> click sur HTML et copier/coller -->

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->
<BR>



# Rightward-Upward-Forward (RUF configuration)

It does not really matter if you do not remember the formal definitions of right- and left-handed coordinate systems.  
Try simply to remember the figure below.


<BR>
<img src="cartesian_CS_RUF.png" width="450pix" style="float:left; padding-right:20px"/>
<BR>
 
<span style="background-color:rgba(255, 0, 0, 0.5);">The <strong>X</strong> axis points in the <strong>rightward</strong> direction,</span>  
<span style="background-color:LightGreen">The <strong>Y</strong> axis points in the <strong>upward</strong> direction,</span>  
<span style="background-color:LightSkyblue">And the <strong>Z</strong> axis points in the <strong>forward</strong> direction, i.e. "behind" the screen.</span>   
<BR>

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->
<BR>

This spatial configuration will be used so often in the PTVR documentation that you should remember it quickly.

<BR>
<div id="note">\emoji :sunglasses: **Tip**: as a mnemonic trick, you can also call this configuration <strong>"RUF"</strong> (Rightward (x) - Upward (y) - Forward (z) ) (as in 'ROUGH'). </div>


<BR>
# Some comparisons with previous work #
It can be helpful for those of you who are familiar with other coordinate systems to see how they relate with previous work. Only a few examples are presented among a multitude of studies.

In the field of stereo-vision, Read & Cumming (2006) use a head-centered space coordinates to describe an object's position relative to the head : "Z is the depth axis (Z increases with distance from the observer), Y is the vertical axis (Y increases as the object moves upward), and X is the horizontal axis (X increases as the object moves leftward). "  
The latter system is therefore right-handed (in contrast to the PTVR RUF system).
 
Read, J. C. A., & Cumming, B. G. (2006). Does depth perception require vertical-disparity detectors? Journal of Vision, 6(12), 1. https://doi.org/10.1167/6.12.1
<BR>

#### Links
Wikipedia contributors. (2022, February 3). Right-hand rule. In Wikipedia, The Free Encyclopedia. Retrieved 14:46, February 21, 2022, from https://en.wikipedia.org/w/index.php?title=Right-hand_rule&oldid=1069678571


<BR><BR>