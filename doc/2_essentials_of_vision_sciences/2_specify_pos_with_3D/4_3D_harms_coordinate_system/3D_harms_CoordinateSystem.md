3D Harms coordinate system (under construction) {#harmsCoordinateSystemInVS} 
============

*Note: If you really want to look at something in the present page: we recommend the introduction and Figures 1 and 2. The other figures are OK except that they display a purple full plane instead of a blue half-plane as correctly shown in Figures 1 and 2, and explaining text is still missing.*

<!-- DEBUT DEBUUUUUUUUUUUUUUUUUUUUUUUUT de ce qui est en chantier (under construction)  

**@PK **: 
En fait, tout ce que je t'ai dit au tél le mardi 5 à propos de la figure 8 etc, c'est parce que j'ai considéré que quand on parlait des PLANS c'était vraiment des PLANS.
Or si on prend des DEMI-PLANS alors tout s'arrange !
donc il faut juste qu'on décide pour des raisons mathématiques ou des raisons de choix propre à Vision science si on prend des DEMI-PLANS

@EC: si on choisit les demi-plans, il faut que je refasse toutes les figures.

J'ai déjà refait les figures 1 et 2 qui sont en plus beaucoup plus claires.

**@PK **: 
a/ grosse question 
d'abord la remarque essentielle EN VISION SCIENCE : c'est evident qu'il faut avoir des valeurs postiives et négatives pour elevation par rapport au plan
MAIS DU COUP ça n'est intuitif **que SI** on n'utilise pas les azimuths supérieurs à abs(90°) car sinon une elevation positive sera derrière moi MAIS EN BAS.

donc ma question : est ce qu'on laisse la possibilité mathématique d'utiliser le CS Harms pour positionner des objets derrière soi avec le risque de confusion entre haut et bas. ET on pourra juste dire dans la doc : Nous ne recommandons pas ça: si vous voulez faire des expériences derrière vous, nous recommandons de faire d'abord une rotation du CS about Y de 180°
FIN FIIIIIIIIIIIIIIIIN de ce qui est en chantier (under construction)   -->


#### Introduction
The 3D **Harms** coordinate system belongs to the class of **spherical** coordinate systems.
The 3D **Harms** coordinate system is used specifically either by vision scientists, e.g. in the field of binocular disparity, or by ophthalmologists, e.g. in the field of strabology ([Read & Cumming, 2008](https://doi.org/10.1167/6.12.1), [Dysli et al., 2021](https://doi.org/10.1080/09273972.2020.1871382)).

This coordinate system is often called an **azimuth-longitude / elevation-longitude** system ([Howard & Rogers, 2008 - chapter 20](https://oxford.universitypressscholarship.com/view/10.1093/acprof:oso/9780195367607.001.0001/acprof-9780195367607-chapter-20), [Schor's lab - chapter 3](http://schorlab.berkeley.edu/passpro/oculomotor/html/chapter_3.html#coord)). 




A previous section explains the different spherical coordinate systems used to specify the azimuth and the elevation of a point ([Class  of spherical coordinate systems](@ref classOfSphericalCoordinateSystems)).

In the Harms system, the 3D position of a point **P** is defined by 3 coordinates:  
1. the azimuth-longitude.  
2. the elevation-longitude.  
3. the radial distance.    

These 3 coordinates are described in details in the 3 following sections.


**1. The azimuth-longitude**  
This is the dihedral angle between a vertical half-plane passing through P (purple plane in the figures below) and the zero azimuth half-plane (i.e. the YZ plane in PTVR).  
  
The azimuth-longitude of Point P is -60° in figures 1 and 2 (-180° < azimuth-longitude < 180°).  

<!-- Début Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Spherical_Harms_Coordinates+projection_on_frontal_plane_horiz_rotation_azi_-60.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Spherical_Harms_Coordinates+projection_on_frontal_plane_horiz_rotation_azi_-60.gif"
width="450pix"/>  
</td>
<tr>
<td>Figure 1: The point P has an azimuth angle of -60° (brown angle) with respect to the YZ half-plane (i.e. it lies on the purple half-plane). The elevation of point P is 19.96° (blue angle).</td>
<td>Figure 2: Animation (horizontal rotation about the Y axis) of figure 1 to allow you to build a better  mental representation of its 3D structure (as if you were able to look at Figure 1 from different angles).</td>
</tr>
</table>
<!-- FIN de Figures 1, 2  -->
<BR>

<div id="note">\emoji :wink: **Remark:** Note that the RADIAL DISTANCE of P is **constant** in Figures 1 to 8.</div>
<BR>

**2. The elevation-longitude**   
This is the dihedral angle between an elevated plane passing through the point and the zero elevation plane.
[To be completed]
<!--TODOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO: faire commme pour 1
 &nbsp;&nbsp;&nbsp; the xxxxxxxxxxxxxxxxxxx of Point P in the figures below is xxxxxxxxxxxxx°.-->
(from -90° to 90°)
The elevation-longitude_poles_on_X_axis



<!-- Début Figures 3, 4  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Spherical_Harms_Coordinates+projection_on_frontal_plane_horiz_rotation_elev_60.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Spherical_Harms_Coordinates+projection_on_frontal_plane_horiz_rotation_elev_60.gif"
width="450pix"/>  
<tr>
<td>Figure 3:  .</td>
<td>Figure 4: [To be completed]
Animation (horizontal rotation about the Y axis) of figure 3 to allow you to build a better  mental representation of the 3D structure of the figure (as if you were able to look at the static coordinate system from different angles).. </td>
</tr>
</table>
<!-- FIN de Figures 3,4  -->


**3. The radial Distance** (in meters) – corresponds to the radius of the sphere on which P is lying (i.e. constant distance between Origin and P).


<BR>
[To be completed] <!--text here TODOOOOOOOOOOOOOOOOOO for figures below-->


<!-- Début Figures 5,6 -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Spherical_Harms_Coordinates+projection_on_frontal_plane_moving_elevation_plane_azi_0.gif" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Spherical_Harms_Coordinates+projection_on_frontal_plane_moving_elevation_plane_azi_60.gif"
width="450pix"/>  
<tr>
<td>Figure 5:  elevation from 0° to 84° with constant azimuth (here 0°). 
Note the iso-azimuth trajectory formed by the path of P on the 0° longitude going through the Y axis poles.</td>
<td>Figure 6:  elevation from 0° to 84° with constant azimuth (here 60°). Note the iso-azimuth trajectory formed by the path of P on the 60° longitude going through the Y axis poles. </td>
</tr>
</table>
<!-- FIN de Figures 5,6  -->


<!-- Début Figures 7, 8 -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Spherical_Harms_Coordinates+projection_on_frontal_plane_moving_azimuth_plane_elev_0.gif" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Spherical_Harms_Coordinates+projection_on_frontal_plane_moving_azimuth_plane_elev_30.gif" width="450pix"/>  
</td>
<tr>
<td>Figure 7:  azimuth from 0° to 135° back and forth with constant elevation (here 0°). 
Note the iso-elevation trajectory formed by the path of P on the 0° longitude going through the Z axis poles.</td>
<td>Figure 8:  azimuth from 0° to 179° back and forth with constant elevation (here 30°). Note the iso-elevation trajectory formed by the path of P on the 30° longitude going through the Z axis poles. </td>
</tr>
</table>
<!-- FIN de Figures 7, 8  -->

<BR><BR>
<div id="note">\emoji :wink: **Remark:** Note that the RADIAL DISTANCE of P is **NOT constant** in the following Figures (in contrast with the above figures).</div>



<!-- @EC : mettre logo CNRS sur Figure 9  -->


<!-- Début Figures 9,10 -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Spherical_Harms_Coordinates+projection_on_frontal_plane_iso_elevation_displacement.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="PK_Screenshot_2022-04-01_at_09.54.04.png" width="450pix"/>  
</td>
<tr>
<td>Figure 9:  
Special case when P is on a tangent Screen: ie radial distance varies.
[To be completed]
</td>
<td>Figure 10: [To be completed]
</td>
</tr>
</table>
<!-- FIN de Figures 9,10  -->




<BR>
#### Important reference for understanding spherical coordinate systems in vision science:  
Schor, C. M. Chapter 3—KINEMATICS : COORDINATE SYSTEMS FOR DESCRIBING EYE POSITION. [Retrieved July 30, 2021, from http://schorlab.berkeley.edu/passpro/oculomotor/html/chapter_3.html#coord](http://schorlab.berkeley.edu/passpro/oculomotor/html/chapter_3.html#coord)

Howard, I. P., & Rogers, B. J. (2008). chap. 20. Classification of binocular disparity. In Seeing in Depth (Vol. 2). Oxford University Press. ([Web link](https://oxford.universitypressscholarship.com/view/10.1093/acprof:oso/9780195367607.001.0001/acprof-9780195367607-chapter-20))

Wikipedia contributors. (2022, February 16). Spherical coordinate system. In Wikipedia, The Free Encyclopedia. Retrieved 15:18, February 21, 2022, from [https://en.wikipedia.org/w/index.php?title=Spherical_coordinate_system&oldid=1072122604](https://en.wikipedia.org/w/index.php?title=Spherical_coordinate_system&oldid=1072122604) 

<BR><BR>