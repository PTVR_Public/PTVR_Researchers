3D Perimetric coordinate system {#perimetricCoordinateSystemInVS}
============

<!-- template_spherical_coordinates=perimetric.ggb  -->



The 3D **perimetric** coordinate system belongs to the class of **spherical** coordinate systems.
The 3D **perimetric** coordinate system is used extensively by vision scientists.

In this coordinate system, the 3D position of a point **P** is defined by the 3 following coordinates:  
– **Half-meridian** (from 0° to 360°). &nbsp;&nbsp;&nbsp;The Half-meridian of Point P in the figures below is 135°.  
– **Eccentricity** (from 0° to 180°). &nbsp;&nbsp;&nbsp; the Eccentricity of Point P in the figures below is 20°.  
– **Radial Distance** (in meters) – corresponds to the radius of the sphere on which P is lying (i.e. constant distance between Origin and P).


<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Spherical_Coordinates_Perimetric_avec_bonne_rotation_Y.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Spherical_Coordinates_Perimetric_avec_bonne_rotation_Y.gif"
width="450pix"/>  
<tr>
<td>Figure 1: Static representation of the 3D perimetric coordinate system. </td>
<td>Figure 2: Animation (horizontal rotation about the Y axis) of figure 1 to allow you to build a better  mental representation of the 3D structure of the figure (as if you were able to look at the static coordinate system from different angles).  </td>
</tr>
</table>
<!-- FIN de Figures 1 et 2  -->


<!-- Figures 3 et 4  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Perimetric_CS_Hm150_Anim_Ecc_8_40deg.gif" width="450pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Perimetric_CS_Hm150_Anim_HM_0_360deg.gif"
width="450pix"/>  
<tr>
<td>Figure 3: Eccentricity of point P varies from 8° to 40° while keeping Half-meridian constant at 135°. </td>
<td>Figure 4:   Half-meridian of point P varies from 0° to 360° while keeping eccentricity constant at 20°.</td>
</tr>
</table>
<!-- FIN de Figures 3 et 4  -->

#### Links

- [3D Perimetric coordinate system In the User Manual](@ref in3DperimetricCS)


#### Important reference for understanding spherical coordinate systems in vision science:  
Howard, I. P., & Rogers, B. J. (2008). chap. 20. Classification of binocular disparity. In Seeing in Depth (Vol. 2). Oxford University Press. https://oxford.universitypressscholarship.com/view/10.1093/acprof:oso/9780195367607.001.0001/acprof-9780195367607-chapter-20

Wikipedia contributors. (2022, February 16). Spherical coordinate system. In Wikipedia, The Free Encyclopedia. Retrieved 15:18, February 21, 2022, from https://en.wikipedia.org/w/index.php?title=Spherical_coordinate_system&oldid=1072122604

<BR><BR>