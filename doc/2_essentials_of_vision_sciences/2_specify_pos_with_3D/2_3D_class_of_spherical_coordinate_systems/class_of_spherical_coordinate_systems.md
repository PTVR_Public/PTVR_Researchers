Class of spherical coordinate system (under construction) {#classOfSphericalCoordinateSystems} 
============


<!-- DEBUT DEBUUUUUUUUUUUUUUUUUUUUUUUUT de ce qui est en chantier (under construction)  

Introduire les "next sub-sections" ie perimetric et Harms.

EXPLIQUER Howard et Roger dans leru fig 20.1 p. 214
QUE AZIMUTH et LONGITUDE sont tout le temps présent

donne ma déf de LONGITUDE : ie qui passe par les poles sauf pour great circle/diameter etc...

et améliorer leur figure
en 
FIN FIIIIIIIIIIIIIIIIN de ce qui est en chantier (under construction)   -->

<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Schor_spherical_coordinate_systems.png" width="450pix"/>
</td>
<td style="width:50%;" align="center">
...
<!--<img src=".gif"
width="450pix"/> -->
<tr>
<td>Figure 1: Spherical Coordinate systems in Vision Science. Figure from [Schor's lab - chapter 3](http://schorlab.berkeley.edu/passpro/oculomotor/html/chapter_3.html#coord) </td>
<td>Figure 2: ...  </td>
</tr>
</table>




#### Important reference for understanding spherical coordinate systems in vision science:  


Schor, C. M. Chapter 3—KINEMATICS : COORDINATE SYSTEMS FOR DESCRIBING EYE POSITION. [Retrieved July 30, 2021, from http://schorlab.berkeley.edu/passpro/oculomotor/html/chapter_3.html#coord](http://schorlab.berkeley.edu/passpro/oculomotor/html/chapter_3.html#coord)


Howard, I. P., & Rogers, B. J. (2008). chap. 20. Classification of binocular disparity. In Seeing in Depth (Vol. 2). Oxford University Press. https://oxford.universitypressscholarship.com/view/10.1093/acprof:oso/9780195367607.001.0001/acprof-9780195367607-chapter-20

Wikipedia contributors. (2022, February 16). Spherical coordinate system. In Wikipedia, The Free Encyclopedia. Retrieved 15:18, February 21, 2022, from https://en.wikipedia.org/w/index.php?title=Spherical_coordinate_system&oldid=1072122604

<BR><BR>