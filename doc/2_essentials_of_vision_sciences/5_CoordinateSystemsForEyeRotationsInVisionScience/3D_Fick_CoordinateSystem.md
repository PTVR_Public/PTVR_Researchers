3D Fick coordinate system - UNDER CONSTRUCTION {#fickCoordinateSystemInVS}
============


The **Fick** coordinate system is a 3D coordinate system that is used specifically by vision scientists for specifying **orientations of the eyes in the head**.

In short, in this system, the vertical axis is fixed to the skull, and the horizontal axis rotates **gimbal** fashion. The order of the rotations thus matters.

#### Links
Section "coordinate systems for eye movements" in:
Howard, I. P., & Rogers, B. J. (2008). Chapter 9. Vergence eye movements. In Seeing in Depth (Vol. 1). Oxford University Press. https://doi.org/10.1093/acprof:oso/9780195367607.003.0009

Haslwanter, T. (1995). Mathematics of three-dimensional eye rotations. Vision Res, 35, 1727–1739. 
https://doi.org/10.1016/0042-6989(94)00257-M

Quaia, C., & Optican, L. M. (2003). Three-dimensional rotation of the eyes. In Adler’s Physiology of the Eye, 10th Edition. Kaufman & Alm, Eds.


<BR><BR><BR>

