Specifying 3D Orientation of gaze {#CoordinateSystemsForEyeRotationsInVisionScience}
==================

<img src="Ruete's_Ophthalmotrope_2nd_model,_1857.jpg" width="473" height="412"  />  

Coordinate systems to specify eye orientations are mentionned in this documentation as PTVR, in the future, will be used to design didactic demos to teach these systems to students.

These coordinate systems, although not currently implemented internally in PTVR, can be easily implemented in PTVR scripts.

(Image from <a href="https://commons.wikimedia.org/wiki/File:Ruete%27s_Ophthalmotrope_2nd_model,_1857.jpg">Christian Georg Theodor Ruete</a>, Public domain, via Wikimedia Commons)

<!-- EC : "use this file on the web -> attribution -> click sur HTML et copier/coller -->
<BR>





The Present section is divided into the following sub-sections:

- @subpage fickCoordinateSystemInVS




