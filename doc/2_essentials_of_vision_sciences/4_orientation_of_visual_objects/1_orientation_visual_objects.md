Specifying 3D Orientation of objects {#orientationVisualObjectsInVisionScience}
==================================================

<!-- 
figures 2 et 3
cartesian_CS_rotations_about_Y_object_anywhere.ggb
Dans une version antérieure, j'avais mis 
cartesian_CS_rotations_about_Y.ggb
mais c'était ambigu car ça donnait l'impression que ça ne marchait QUE SI l'objet
était sur l'axe.
-->

<BR>
<!--     figure 1       -->
<img src="cartesian_CS_rotations_about_Y_object_anywhere.png" width="500" height="400" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

A visual object is not only defined by its position but also by its **orientation**.

Say we want to change the orientation of the green arrow shown in Figure 1.
For instance, we want to **rotate** the green arrow  **about the Y axis**.

Can you visualize  what this rotation will look like?

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

**Figure 1**: What is the effect of rotating the green arrow say **about the Y axis** ?
<BR><BR><BR>

To apply a rotation to this object, we need three parameters:  
- the **pivot** of the object  
- the **vector** corresponding to the axis of rotation (here the Y axis)  
- the **sign** of the rotation

<div id="note">\emoji :wink: **Remark:** if not specified otherwise, the axes correspond to the [GLOBAL coordinate system](@ref globalAndLocalCoordinateSystems) </div>

The first two parameters are simple to visualize.

 
<BR>
<!--     figure 2   ie avec l'oeil qui regarde le bout de l'axe    -->
<img src="eye_looking_at_Y_axis_from_the_tip.png" width="300" height="300" style="float:left; padding-right:12px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->
<BR>


The **sign** of the rotation is however a trickier parameter.  

You have to look at the Y axis from the tip of its axis (as in Figure 2) and you need to remember that the PTVR coordinate system is a **left-handed system** [(see here)](@ref cartesianCoordinateSystemInVS).


<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->

**Figure 2**: Looking at an axis from its tip.
<BR><BR>




In a **left-handed system** (hence in PTVR) ...  
- if you apply an **increasing** change of orientation about the Y axis to the green arrow object, the arrow will rotate **clockwise** (see Figure 3).  
- if you apply a **decreasing** change of orientation about the Y axis to the green arrow object, the arrow will rotate **CounterClockwise** (see Figure 4).

<!-- Figures 3 et 4  -->
<table>
<tr>
<td style="width:33%;" align="center">
<img src="cartesian_CS_rotations_about_Y_object_anywhere_CW.gif" width="450pix"/>
</td>
<td style="width:33%;" align="center">
<img src="cartesian_CS_rotations_about_Y_object_anywhere_CCW.gif" width="450pix"/>  
</td>

</tr>

<tr>
<td>Figure 3: increasing orientation -> Clockwise (when seen from the tip of the axis).
</td>
 
<td>Figure 4: decreasing orientation -> CounterClockwise (when seen from the tip of the axis).
</td>
</tr>
</table>
<BR>
<!-- FIN de Figures 3 et 4  -->

By comparison, in a right-handed system (not shown) ...  
- if you apply an **increasing** change of orientation about the Y axis to the green arrow object, the arrow will rotate **Counterclockwise**.  
- if you apply a **decreasing** change of orientation about the Y axis to the green arrow object, the arrow will rotate **Clockwise**.




Sequence of rotations
----------------------------
In the examples above a single rotation was applied (here about the Y axis).
In many cases, you might want to apply successively (the order matters) 3 rotations about the 3 axes to reach the final orientation you wish.
These cases will be explained in the Manual for users with concrete examples.

Links
---------------
Wikipedia contributors, 'Orientation (geometry)', Wikipedia, The Free Encyclopedia, 11 March 2022, 09:11 UTC, <https://en.wikipedia.org/w/index.php?title=Orientation_(geometry)&oldid=1076477808> [accessed 5 May 2022] 

Excerpts
-------------------
Section Mathematical representations -> 3D
>In general the **position** and **orientation** in space of a rigid body are defined as the **position** and **orientation** (relative to the main reference frame) of another reference frame, which is fixed relative to the body, and hence translates and rotates with it (the body's local reference frame, or local coordinate system).  
>At least three independent values are needed to describe the **orientation** of this local frame.  
>Three other values describe the **position** of a point on the object. ...  
<BR><BR>