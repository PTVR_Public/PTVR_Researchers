Axes and Planes {#axesAndPlanesInVS}
==================================================

<img src="Lone_starfish_-_geograph.org.uk_-_2850690.jpg" width="640" height="427" style="float:left; padding-right:20px" />  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

<span style="font-size:170%;">
Should I move forward or backward ? says the starfish.</span>

<div style="clear:both"></div>
<!-- NOW, the text below will not "float" anhy longer  -->


- @subpage wholeBodyAxesAndPlanesInVS
- @subpage HeadAxesAndPlanesInVS

