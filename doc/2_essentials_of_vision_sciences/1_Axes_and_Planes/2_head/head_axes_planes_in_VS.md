Human Head Axes and Planes {#HeadAxesAndPlanesInVS}
==============================

Planes and Axes for the **Head** are directly derived from the **standard anatomical position**, or **standard anatomical model** described in the [previous section on the anatomical planes of the human body.](@ref wholeBodyAxesAndPlanesInVS). As the colours of the human body used in this previous section are not the same as those used here, the correspondence between the 3 planes and their colour is summarized  below:  
The three main anatomical planes are:.
> 
>  - A **transverse** plane (also known as **Axial** or **horizontal** plane) is parallel to the ground; it separates the superior from the inferior (here in blue).
>  - A **coronal** plane (also known as **frontal** plane) is perpendicular to the ground; it separates the anterior from the posterior, the front from the back (here in green).
>  - A **sagittal** plane (also known as **anteroposterior** plane) is perpendicular to the ground, separating left from right.  

&emsp;&emsp;&emsp;&emsp; The **median** (or **midsagittal**) plane is the sagittal plane that is exactly in the middle of the head (here in red).  

&emsp;&emsp;&emsp;&emsp; All other sagittal planes (also known as **parasagittal** planes) are parallel to it (one of many paragittal planes is shown in yellow).

<BR>


<div id="note">\emoji :scream: **Warning:** The colors used in these figures have **NO relationship** with the colours of the axes used throughout the PTVR Documentation. </div>

<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="Human_head_with_labeled_anatomic_planes_vers_l_avant_gauche.jpg" width="550pix"/>
</td>
<td style="width:50%;" align="center">
<img src="Rotating_human_head_with_labeled_anatomic_planes.gif" width="550pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: Planes of the head. (image from <a href="https://commons.wikimedia.org/wiki/File:Human_head_with_labeled_anatomic_planes.jpg">David Richfield (User:Slashme)When using this image in external works, it may be cited as follows:Richfield, David (2014). &quot;Medical gallery of David Richfield&quot;. WikiJournal of Medicine 1 (2). DOI:10.15347/wjm/2014.009 SMASH. ISSN 2002-4436.</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons)
</td>



<td>Figure 2: Animation (horizontal rotation) of Figure 1 to help you build a 3D mental image of the head model.

</td>
</tr>
</table>
<BR>


<div id="note">\emoji :sunglasses: **Tip**: These planes are also helpful to define the **local** coordinate systems used for the head [in section Global and Local coordinate systems.](@ref globalAndLocalCoordinateSystems) </div>




#### Links
Wikipedia contributors. (2022, January 2). Standard anatomical position. In Wikipedia, The Free Encyclopedia. Retrieved 08:53, February 21, 2022, from https://en.wikipedia.org/w/index.php?title=Standard_anatomical_position&oldid=1063409930

Wikipedia contributors. (2022, January 30). Anatomical plane. In Wikipedia, The Free Encyclopedia. Retrieved 08:25, February 21, 2022, from https://en.wikipedia.org/w/index.php?title=Anatomical_plane&oldid=1068939098

Howard, I. P., & Rogers, B. J. (2008). Chapter 15. Binocular correspondence and the horopter. In Seeing in Depth (Vol. 2). Oxford University Press. https://oxford.universitypressscholarship.com/view/10.1093/acprof:oso/9780195367607.001.0001/acprof-9780195367607-chapter-15


<BR><BR>