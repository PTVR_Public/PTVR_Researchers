Human Body Axes and Planes {#wholeBodyAxesAndPlanesInVS}
==============================

> The **standard anatomical position**, or **standard anatomical model**, is the scientifically agreed upon reference position for anatomical location terms. Standard anatomical positions are used to standardise the position of appendages of animals with respect to the main **body** of the organism [Wikipedia contributors. (2022, January 2)](https://en.wikipedia.org/w/index.php?title=Standard_anatomical_position&oldid=1063409930).


Excerpts from [Wikipedia contributors (2022, January 30) ](https://en.wikipedia.org/w/index.php?title=Anatomical_plane&oldid=1068939098):
> The standard anatomical model helps avoid confusion in terminology when referring to the same organism in **different postures**. ...
> 
> In human anatomy, the three main anatomical planes are defined in reference to the anatomical model being in the upright, or standing, orientation.
> 
>  - A **transverse** plane (also known as **axial** or **horizontal** plane) is parallel to the ground; it separates the superior from the inferior, or the head from the feet.
>  - A **coronal** plane (also known as **frontal** plane) is perpendicular to the ground; it separates the anterior from the posterior, the front from the back, the ventral from the dorsal.
>  - A **sagittal** plane (also known as **anteroposterior** plane) is perpendicular to the ground, separating left from right. 
 
&emsp;&emsp;&emsp;&emsp; The **median** (or **midsagittal**) plane is the sagittal plane that is exactly in the middle of the body; it passes through midline structures such as the navel and the spine.  

&emsp;&emsp;&emsp;&emsp; All other sagittal planes (also known as **parasagittal** planes) are parallel to it.
<BR>


<!-- Figures 1 et 2  -->
<table>
<tr>
<td style="width:50%;" align="center">
<img src="768px-Human_anatomy_planes,_labeled.png" width="550pix"/>
</td>
<td style="width:50%;" align="center">
<img src="496px-Anatomical_axes.svg.png" width="350pix"/>  
</td>
</tr>
<tr>
<td>Figure 1: Anatomical planes. Original xyz labels of axes have been intentionnally hidden to avoid confusion with the XYZ coordinate systems used in PTVR documentation. (image from <a href="https://commons.wikimedia.org/wiki/File:Human_anatomy_planes,_labeled.svg">David Richfield and Mikael Häggström, M.D. and cmglee</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons)
</td>


<td>Figure 2: Anatomical axes. (image from <a href="https://commons.wikimedia.org/wiki/File:Anatomical_axes.svg">Edoarado and Mikael Häggström</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, via Wikimedia Commons)

</td>
</tr>
</table>
<BR>



<div id="note">\emoji :scream: **Warning:** The colors used in these figures have **NO relationship** with the colours of the axes used throughout the PTVR Documentation. </div>




#### Links
Wikipedia contributors. (2022, January 2). Standard anatomical position. In Wikipedia, The Free Encyclopedia. Retrieved 08:53, February 21, 2022, from https://en.wikipedia.org/w/index.php?title=Standard_anatomical_position&oldid=1063409930

Wikipedia contributors. (2022, January 30). Anatomical plane. In Wikipedia, The Free Encyclopedia. Retrieved 08:25, February 21, 2022, from https://en.wikipedia.org/w/index.php?title=Anatomical_plane&oldid=1068939098

Howard, I. P., & Rogers, B. J. (2008). Chapter 1. Introduction. In Seeing in Depth. Oxford University Press. https://doi.org/10.1093/acprof:oso/9780195367607.003.0001

<BR><BR>