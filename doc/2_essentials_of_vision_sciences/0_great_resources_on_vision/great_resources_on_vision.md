Great resources on Vision Science {#greatResoucesOnVision} 
============

This is a list of huge and incredible reviews written by leading scientists.

This list is of course not exhaustive (by lack of time) but they contain many other resources that you could find useful. And we will try to complement it gradually.


**Free access**

Wandell, B. A. (1995). Foundations of vision. Sinauer Associates.   
A wonderful paper book ...  
 [Also available as an electronic book on the Stanford University site.](http://foundationsofvision.stanford.edu/)


Schor, C.M. Oculomotor Functions & Neurology   
 [Available as an electronic book on the Berkeley University site.](http://schorlab.berkeley.edu/passpro/oculomotor/html/chapters__toc_.html)


**Not Free**

Howard, I. P., & Rogers, B. J. (2008). Seeing in Depth (Vol. 1–2). Oxford University Press. [https://doi.org/10.1093/acprof:oso/9780195367607.001.0001](https://doi.org/10.1093/acprof:oso/9780195367607.001.0001)  
An irreplaceable bible (actually two ...)


Atchison, D. A., & Smith, G. (2000). Optics of the Human Eye. Elsevier.   
[https://doi.org/10.1016/B978-0-7506-3775-6.X5001-9](https://doi.org/10.1016/B978-0-7506-3775-6.X5001-9)  
Very good to understand the details of visual angles !