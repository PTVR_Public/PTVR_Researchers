Index {#Index}
============
Tip: use the Search tool of your web navigator if this index is too long.

Alignment   
	-  [Text alignment](@ref textAlignment) 
	
Animation  
	-  [Animation with succession of scenes](@ref Scene)   

Assets  (and Asset bundles)
	-  [Assets : common terminology in video games](@ref customobjects)	 

Azimuth
	-  [Azimuth and Elevation coordinates](@ref in3DlongitudeLatitudeCS)	

Center (of objects)  
	- [Center of symmetrical object](@ref placingObjectsInVacuum)  
	- [Center of asymmetrical object](@ref placingObjectsInVacuum)  
	- [Placing objects on 2D screens](@ref placingObjectsOnScreens)

Coordinate System    
	- [Rigid Coordinate Transformations](@ref coordinateTransformations)   
	- [Global and local CS](@ref globalAndLocalCoordinateSystems)   

Cone (for pointing at an object)   
	- [Cursor cone](@ref pointing)      
	- [Activation cone](@ref selectingPointedObject)

Contingent  
	- [reticles, scotomas, contingent flat cursors](@ref pointing)  

Contingent Flat Cursors  
	- [reticles, scotomas, etc.](@ref pointing)  
	
Current  
	- [Current Coordinate System](@ref coordinateTransformations)  

Crosshair
	- see Cursor

CS (Coordinate System)  
	- [A good start is here](@ref CoordinateSystemsInVisionScience)   

Cursor (flat) - for pointing    
	- [reticles, scotomas, contingent flat cursors](@ref pointing)

Cyclic  
	- [Cyclic presentation of scenes](@ref EndCurrentScene) 
	
Elevation
	-  [Azimuth and Elevation coordinates](@ref in3DlongitudeLatitudeCS)

Errors  
	- [ModuleNotFoundError : No Module named 'PTVR'](@ref ptvrInstallation)  
	- [Error messages and codes for the VR device](@ref setupHMD)

Euclidean Distance  
	- [Visual Angles](@ref simpleDefinitionsInVS)  
		
Experiment  
	- [An experiment from start to finish](@ref anExperiment)  

Eyetracking  
	- [eyetracking in VR device setup](@ref setupHMD)  

Files  
	- [basic experiment's output file](@ref results_of_an_experiment)

Frame of Reference  
	- [... is Synonymous with Coordinate System](@ref CoordinateSystemsInVisionScience) 

Global vs. Local coordinate systems  
	- [In 'Essentials of Vision Science'](@ref globalAndLocalCoordinateSystems)  
	- [In 'User Manual'](@ref coordinateTransformations)    
	
Global Variable  
	- [In 'User Manual'](@ref globalVariablesAndInteractions)    	

Hand controller (handcontroller)   
	- [Event Object: Hand Controller](@ref handController) 	  
	- [Recording Hand Controller data](@ref recordingHandcontrollerEtc) 	 
	
Harms  
	- [Harms coordinate systems](@ref harmsCoordinateSystemInVS)  

Image  
	- [Sprites or Flat 2D objects](@ref spriteObjects) 
	- [reticles, scotomas, contingent flat cursors](@ref pointing)  
	
Laser pointing  
	- [Pointing at an object](@ref pointing) 	

Left-handed coordinate system  
	- [3D Cartesian Coordinate System](@ref cartesianCoordinateSystemInVS)

Local coordinate system  
	- [Global and local coordinate systems](@ref globalAndLocalCoordinateSystems) 	

Main coordinate system  
	- [Main Coordinate System](@ref globalAndLocalCoordinateSystems) 

Origin  
	- [Visual Angles](@ref simpleDefinitionsInVS)  

Perimetric  
	- [Perimetric coordinate systems](@ref perimetricCoordinateSystemInVS)  

Pivot (of objects)  
	- [3D orientation of objects](@ref orientationVisualObjectsInVisionScience)  
	- see Center (of objects)

Point Of Fixation (POF)       
	- [viewpoints in Ophthalmology](@ref viewpointsInOphthalmology)  

Pointer  
	- [reticles, scotomas, contingent flat cursors](@ref pointing)  

Random values at runtime  
	- [In 'User Manual'](@ref globalVariablesAndInteractions)    

Reaction Time (RT)  
	- [The results file](@ref resultsOfAnExperiment)  

Recording (head, hand, or gaze data)  
	- [Recording headset, handcontroller, etc...](@ref recordingHandcontrollerEtc)  

Reticle  
	- [reticles, scotomas, contingent flat cursors](@ref pointing)  

Retinal  
	- [Retinal Image](@ref retinalImagesInVS)  

RUF ("Rightward - Upward - Forward" Coordinate System)   
	- [3D Cartesian Coordinate System](@ref cartesianCoordinateSystemInVS)

Scene   
	- [Scene definition ](@ref Scene) 
	- [Control of scene duration ](@ref simplifiedInteractions)

Scotoma  
	- [reticles, scotomas, contingent flat cursors](@ref pointing)  

Spherical  
	- [Class of Spherical coordinate systems](@ref CoordinateSystemsInVisionScience)

SO (Screen Origin)  
	- [2D Cartesian Coordinate System](@ref _2DcartesianCoordinateSystemInVS)  
	- [2D perimetric Coordinate System](@ref _2DperimetricCoordinateSystemInVs)

Sprites (2D bitmaps)  
	- [Sprites or flat 2D objects](@ref spriteObjects) 
	
Tangent Screens (In Vision Science section)  
	- [Definition of a Tangent Screen](@ref definitionOfTangentPlaneInVS)  
	- [Tangent screen with 2D Cartesian coordinates](@ref _2DcartesianCoordinateSystemInVS)  
	- [Tangent Screen with 2D Perimetric coordinates](@ref _2DperimetricCoordinateSystemInVs)

Tangent Screens (In User Manual)  
	- [Tangent Screens: key points](@ref definitionsTS)  
	- [Placing objects with 2 coordinates on Screens](@ref placingObjectsOnScreens)

Text  
	- [Key points about text and typography](@ref TextAndTypography)  
	- [Text in User Manual](@ref text)  
	- [Visual angle of Text](@ref visualAngleOfText)

Textbox  
	- [Textbox](@ref textBox)

Tracking (head, hand, or gaze data)  
	- [Recording - or tracking - headset, handcontroller, etc...](@ref recordingHandcontrollerEtc)  

Viewing Distance   
	- [2D Cartesian Coordinate System](@ref _2DcartesianCoordinateSystemInVS)  
	- [Visual Angle with monocular viewing](@ref simpleDefinitionsInVS)  
	- [Visual Angles with binocular viewing](@ref approximationsOfVisualAngles) 

Viewpoint (static)   
	- [in section Vision Science](@ref viewpointInVS)  
	- [in section User Manual](@ref whenIsTheViewpointConceptUseful)  

Viewpoint (mobile)   
	- [Mobile Viewpoint](@ref mobileViewpointInUserManual)


Visual Angle  
	- [Visual Angle definition](@ref simpleDefinitionsInVS)  
	- [Retinal Image](@ref retinalImagesInVS)   
	- [Visual angle of Text](@ref visualAngleOfText)  
	
World Space   
	- [World Space Coordinate System](@ref globalAndLocalCoordinateSystems)  
<BR>