General guidelines {#docGeneralGuidelines}
============

> **Objectives:** This section presents some guidelines to write software documents efficiently. Some points are highlighted, some useful Links are given, but don't hesitate to add yours. Since several people contribute to the documentation, we all must start with the same rules and spirit. After going through this section, you should write your software documentation like a pro!

#### Why writing documentation?

- For you 
  - You will be using your code in 6 months
  - You want people to use your code and give you credit
  - You want to be a better writer
  - Others would be encouraged to contribute to your code
- For others: 
  - Others can easily use your code and build upon it
- For science:
  - Allow reproducibility and transparency
  - Encourage open science, which is what PVTR is about


#### Top 10 guidelines

1. **Take your time to write it!** Software documentation, just like any other form of technical writing, cannot be rushed. As such, this has to be planed in our sprint definitions.
2. **Always keep in mind the purpose and audience of the document**
3. **Anticipate the questions and needs the readers** might have about the software
4. **Think about your writing style:** 
  - Don't write more than you need to; 
  - Wherever possible, avoid using jargon; 
  - Use plain English to convey your thoughts (keep it simple! use short sentences); 
  - Avoid being unambiguous;
5. **Never copy-paste information from a tier website** (unless it appears really necessary); instead use Links
6. **Leverage Visuals (flowcharts, illustrations, screenshots, etc.)** to augment your document. These visuals can be used to emphasize a point, further explain a technical concept, help out the reader, and just make your document look so much better.
7. **Perform final editing:** Polish it, don't hesitate to ask a colleague to go through it. 
8. **Think about cross-references as much as you can**. The documentation will become very powerful if users can jump from one point to a different point of the documentation. For example, reading something about a particular function will be very useful to provide a link to a section in the "User manual" where a demo is available. 
9. **You introduce a new term, a new definition? add it in the Index section**
10. \emoji :joy: *Ah Ah!... we are missing one, but for sure you will have suggestions!!* ;-)

#### Links

- [How to write great software documentation in 7 simple steps](https://technicalwriterhq.com/how-to-write-software-documentation/)
- [Technical Writing Course: Documentation on Software Projects](https://www.googleadservices.com/pagead/aclk?sa=L&ai=DChcSEwijutP-_uL0AhWY91EKHfIYDgcYABABGgJ3cw&ae=2&ohost=www.google.com&cid=CAESQeD26kaPTP4nmTkdEdLwYmZ4csIVcg1B69m3cL1f5N_6edbNh6wR99fssq8x3TCbuVEH8LhZd2kf8U09j2zLUFy3&sig=AOD64_2XY7lcZmnCfDq0_IP13yGrkByN7w&q&adurl&ved=2ahUKEwiLwsr-_uL0AhVK14UKHcAeBZ0Q0Qx6BAgDEAE&dct=1)
- [WikiHow: How to Write Software Documentation](https://www.wikihow.com/Write-Software-Documentation)
- *Other useful ressources? feel free to complete that list*