Editing Markdown files {#docWritingMarkdownFiles}
============

#### General spirit

- When you start a new section, put some illustration (see, [example from Unity documentation](https://docs.unity3d.com/Manual/Graphics.html))
- Try to have each page as short as possible (see, e.g., Unity pages). Be short, add links and use subsections as much as possible to segment de pages. To do so, use [`\subpage`](https://www.doxygen.nl/manual/commands.html#cmdsubpage) command. 
- Using `\subpage` defines the structure of the documentation: *Each page can be the sub page of only one other page and no cyclic relations are allowed, i.e. the page hierarchy must have a tree structure.* 
- Do not create heading of levels 1, 2 or 3 (using a number signs # in front of a word or phrase). The reason is that it is going to interfere in the table of contents with the structure defined by the `\subpages`. If you wish to structure a page, only use headings of level 4 (i.e., with ####), because this will not appear in the table of contents.


#### Citing lines of code in the documentation

When you need to insert some lines of code, you can do it this way:

```
<pre>
<code class="prettyprint">text = PTVR.Stimuli.Objects.Text(text="Hello", position=np.array([0.0,0.0,3.0]))  
text.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=1, isWrapping=True)
</code>
</pre>
```

This is based on ``` ``` and gives something like this:

```
text = PTVR.Stimuli.Objects.Text(text="Hello", position=np.array([0.0,0.0,3.0]))  
text.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=1, isWrapping=True)
```

<span style="background-color:yellow">PLEASE DO NOT USE:</span>


```
text = PTVR.Stimuli.Objects.Text(text="Hello", position=np.array([0.0,0.0,3.0]))  
text.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=1, isWrapping=True)
```


which will look like this:

```
text = PTVR.Stimuli.Objects.Text(text="Hello", position=np.array([0.0,0.0,3.0]))  
text.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=1, isWrapping=True)
```

#### creating hyperlinks
Very easy:
Vous voulez créer un hyperlien vers le fichier "caParleDeToto.md". 

La première ligne de ce fichier est : Ceci est le titre de toto {#nomTotoEnHashtag}

Alors le hyperlink doit être comme ceci:
[votre blabla] (nomTotoEnHashtag.html)  
attention pas d'espace entre []et()


#### Essais autour du code...


Et dans du texte comme `PTVR.Stimuli.World.Text`, ou ``PTVR.Stimuli.World.Text``, ou ```PTVR.Stimuli.World.Text```: une difference?

#### Mentioning functions, parameters, options, etc in a text

<div id="todo">Mieux definir le style de cela</div>

Veut-on par exemple:

- ... and to do so you can use **PTVR.Stimuli.World.Text** and also...
- ... and to do so you can use ```PTVR.Stimuli.World.Text``` and also...


#### Add a link to a demo "immerse yourself in figure X"
Add this dans une table :
<table>
<tr> 
	<td colspan="2">
<img src="VR_logo.jpg" width="120pix" style="float:left; padding-right:20px"/> <strong>DEMO !  </strong> Immerse yourself in Figure 2 with the demo  translation_vector_specified_in_GLOBAL_coordinate_system.py  
<p> Directory of demo: ...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Coordinate_Systems\
</p>
<p>
 (logo from <a href="https://www.freepik.com/vectors/neon-icon">Neon icon vector created by katemangostar - www.freepik.com</a>)
</p>
	</td>
</tr>
</table>



#### Add a remark

You can highlight a remark like this:

```
<div id="note">\emoji :wink: **Remark:** Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt ipsum vel tempus egestas. Suspendisse.</div>
```

which gives:

<div id="note">\emoji :wink: **Remark:** tLorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt ipsum vel tempus egestas. Suspendisse.</div>

For information

#### Add a warning

You can highlight a warning message like this:

```
<div id="note">\emoji :scream: **Warning:** Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt ipsum vel tempus egestas. Suspendisse.</div>
```

which gives:

<div id="note">\emoji :scream: **Warning:** Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt ipsum vel tempus egestas. Suspendisse.</div>

#### Add hints, tips, or nice ideas

You can indicate some hint, tip, or nice idea like this:

```
<div id="note">\emoji :sunglasses: **Tip**: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt ipsum vel tempus egestas. Suspendisse. </div>
```

which gives:

<div id="note">\emoji :sunglasses: **Tip**: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt ipsum vel tempus egestas. Suspendisse. </div>

#### Modify style of text
**Increase text size**  
You can do it like this:  

	<span style="font-size:130%;"> Bigger text </span>
which gives:  
<span style="font-size:130%;"> Bigger text </span>   

<BR>
**Add some colored background to a text**

You can do it like this

```
<span style="background-color:yellow">text with a yellow background</span>
```

which gives:

<span style="background-color:yellow">text with a yellow background</span>

<BR>
**Add non-breaking space**  
A commonly used entity in HTML is the non-breaking space:    
`&nbsp; `   
A non-breaking space is a space that will not break into a new line.  
[https://www.w3schools.com/html/html_entities.asp](https://www.w3schools.com/html/html_entities.asp)

**UTF-8 General Punctuation**  
[https://www.w3schools.com/charsets/ref_utf_punctuation.asp](https://www.w3schools.com/charsets/ref_utf_punctuation.asp "https://www.w3schools.com/charsets/ref_utf_punctuation.asp")  
Add empty spaces:

    &nbsp;
    &ensp;
    &emsp;

Add underscore  

	&#95;
	<span>height&#95;of&#95;head</span> 
which gives:
<span>height&#95;of&#95;head</span> 



#### Add a table to list demos

A the end of a page, you can add a table to list demos related to the topic discussed. This can be done like this:

```
<table id="demo_table">

<tr><th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>text_on_flat_screen.py
<td>Show how to place text on flat screen and rotate the screen + text afterwards. 

<tr>
<td>text_on_flat_screen_x_height_centering.py
<td>Show that a text with a 1 deg x_height is superimposed with a flat screen whose height is 1 deg. 

</table>
```

which gives:

<table id="demo_table">

<tr><th id="double_th_1">Python file
<th id="double_th_2">Description 

<tr>
<td>text_on_flat_screen.py
<td>Show how to place text on flat screen and rotate the screen + text afterwards. 

<tr>
<td>text_on_flat_screen_x_height_centering.py
<td>Show that a text with a 1 deg x_height is superimposed with a flat screen whose height is 1 deg. 

</table>

Two comments:
- For the Python file: add a link, make sure your code works of course and that it is properly commented. 
- For the description, be short and start with a verb. 

#### Links


Last but not least, you may finish your section by adding links. These Links can be internal (referring to other PTVR documentation pages) or external.

For example:

```
#### Links
- [How to use Emojis in Doxygen](https://www.doxygen.nl/manual/emojisup.html)
- [List of Emojis](https://gist.github.com/rxaviers/7360908)
```

#### Think about the glossary

As you write your documentation, you are going to introduce and define new terms. Please think about adding these terms in the glossary with proper cross-references. Organize them by topics. See [Unity glossary example](https://docs.unity3d.com/Manual/Glossary.html).


