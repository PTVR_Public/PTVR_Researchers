Contribute to documentation  {#Contribute}
============

<img src="https://i.imgur.com/uc4WSHU.jpeg" width="50%">

You want to contribute to the documentation? That's great, thank you! Your contribution to the documentation is essential for the project. Here you will find three sections. Please read them carefully before starting. 

- @subpage docStructure You don't know where to put the information? This section describes the objectives for each section of the documentation to help you find the best location. If you have any doubt, no worries, let us discuss together to make sure to target the good section.  
- @subpage docGeneralGuidelines You are not sure how to write it, what you should write or not, if there should be illustrations, how deep you should go, etc; please read this section. It is advised that everybody contributing to the software documentation goes through these guidelines to share common rules. Also, very importantly, feel free to make them evolve and be more precise! 
- @subpage docPTVRDocGuidelines TODO our recommendations
- **Report a mistake or make suggestions:** You see something wrong or you have suggestions? Just go there to [send us a message](mailto:ptvr.dev@inria.fr?Subject=[PTVR] Feedback about the documentation for users)! 


> Pour quote utilisé 
> Example de citation
> ICI

Pour code 
```
example ici
```