Structure of the Documentation for Users {#docStructure}
============

#### Getting started with PTVR

> **Objectives of this section:** Starting from scratch, the goal of this section is to provide all information for new users to run their first code with PTVR (Hello_PTVR.py). It should indicate the requirements for the user (prior knowledge needed), hardware, and software. Then this section includes all aspects of the installation, from the hardware to the software part. After going through this section, you will be all set to start working with PTVR! 
> 
> *What's next? To discover the key concepts about PTVR, jump to the next section, "Behavioural sciences"*

#### Behavioural sciences: from 2D to 3D, Key concepts

> **Objectives of this section:** You know about behavioral sciences, you have already done your own protocols and experiments, or you want to start doing it, then check out what you will find in PTVR to make it. This section is voluntarily not technical. After going through this section, you should be able to imagine how to build your experiments in 3D, using PTVR features. 
> 
> *What's next? To enter technical details associated with these concepts, jump to the next section, "User Manual"*
 
#### User manual (methods, functions) 

> **Objectives of this section:** This section mirrors the section "Behavioral sciences" in its structure, but here we will present the technical details and an overview of the main functionalities to build your experiment. You will find some simple minimalist demo to test them in most cases. After going through this section, you will have practical knowledge about PTVR, the capacity to experiment by yourself main functions, and a better idea of what you need to build your own experiment! To go into more details, then you will need to 
> 
> **Remark:** Concerning demos, this common organization between the section "User Manual" and "Behavioral sciences" should be used to define the directories to store the demos. Note that every piece of code is now called a demo (no distinction between demo and test anymore). 
> 
> *What's next? To see real experiments running with PTVR, jump to the next section, "Famous experiments"*

#### Famous experiments

> **Objectives:** This section describes some famous behavioral experiments implemented with PTVR, just revisiting or extending them in 3D. After going through this section, you will see by yourself how a complete experiment works, how to set parameters, how to define the trials, how to get output, etc.
> 
> *What's next? To learn more about the technical specifications of PTVR, jump to the next sections, "Namespaces" and "Data Structures"*


#### Namespaces and Data Structures

> **Objectives:** This section is the technical documentation. It is automatically extracted from the code. What you here has been written IN THE CODE. Comments should be concise but precise and self-contained. Avoid repetitions or copy-paste editing: use Links as much as possible. 
> 
> *What's next? To learn more about software documentation guidelines, jump to the next section, "Guidelines to contribute"

#### Index

> **Objectives:** This section lists the main definitions and concepts used in PTVR. It will give you a way to access the pages where they are discussed quickly.
