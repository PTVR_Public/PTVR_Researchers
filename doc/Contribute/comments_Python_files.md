Writing comments in Python files {#docWritingCommentsInPythonFiles}
============

Jeremy? Johanna? Christophe?

### Create a File/Folder PTVR_Researchers
Create File and Folder inside PTVR_Researchers must be write with underscore and never with space.
Example :
write my_folder_about_this_subject and not my folder about this subject

The name of variable use only on python side mush be write in underscore.

The only exception is the variable write inside the json (this kind of variable will be all time write inside python core of PTVR) wo will be write in camelCase example myVarriable