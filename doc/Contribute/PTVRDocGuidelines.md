Specific guidelines {#docPTVRDocGuidelines}
============

- @subpage docWritingMarkdownFiles: here you fill find precise guidelines concerning the style to write your markdown files. You will also find some pieces of text that you can simply copy-paste.

- @subpage docWritingCommentsInPythonFiles: here you will find good practices to add the comments in the Python files that will be used to create the pages under Namespaces and Data Structure

- @subpage docDemoCreation: here you will find recommendations to create demos.
