Glossary {#Glossary}
============

Fixated Point (FP)    
- The location that is actually and currently fixated.  
- Not to be confused with : Point Of Fixation (POF) which in PTVR refers to a stimulus that the subject has to fixate.


Point Of Fixation (POF) :  
- A stimulus that the subject has to fixate. 
- Note 1: this does not mean that the subject is fixating it.
- Note 2: a POF is not necessarily in the middle of a tangent screen.


Visual axis  
- Axis from fovea to Fixated Point (FP)  
