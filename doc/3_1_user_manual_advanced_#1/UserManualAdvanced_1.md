User Manual (Advanced #1) {#UserManualAdvanced_1}
================

## List of sections ##

<img src="863px768-Handyman's_Book_-_Flickr_-_brewbooks.jpg" width="400" style="float:right; padding-right:20px"/>  
<!-- the text below is now going to "float" around the above image  -->
<!-- And, most importantly, the image will have a border to get text further away -->

- @subpage MotionWithinScene
- @subpage GlobalVariableIntroduction

<!--

- @subpage animatingObjects
- @subpage CancelATrial
- @subpage Calculator
- @subpage InfiniteConeConcept
-->


<div style="clear:both" ></div>
<!-- NOW, the text below will not "float" anhy longer  -->


<BR>
**Attributions of Images**  
Image from <a href="https://commons.wikimedia.org/wiki/File:Handyman%27s_Book_-_Flickr_-_brewbooks.jpg">brewbooks from near Seattle, USA</a>, <a href="https://creativecommons.org/licenses/by-sa/2.0">CC BY-SA 2.0</a>, via Wikimedia Commons. 