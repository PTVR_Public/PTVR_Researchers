Global variables for randomization {#globalVariablesForRandomization}
============


Here is a simple, and very useful, example to illustrate how Global variables can be used to draw random values at runtime. 

<BR>


#### A Global Variable for runtime randomization
We now present a demo script that illustrates how to draw (at runtime) a triplet of random values everytime the keyboard 'ENTER' key is pressed. Once the triplet is drawn, it is used to change the position of an object.   

The corresponding conceptual flow of Events and Callbacks is simplified in the following way:  

EVENT ('ENTER' is pressed) -> Callback #1 (draw a random triplet) -> callback #2 (use this triplet to change object's position).   
     
**Warning** : Note that **the two callbacks are called one after the other** even though the delay between them is tiny (you can check for yourself in the results file).  


The key point here is that **the two callbacks need to share a Global variable**.      
This process is schematically represented in the figure below.  

<p align="center">
  <img src="conceptual_flowchart.png">
</p>

<BR>

The process schematized in this figure is implemented with the following code:  
```
	# Event
    my_event = PTVR.Data.Event.Keyboard (valid_responses=['enter'])

	# Callback 1`
	create_random_vector3 = PTVR.Data.Callbacks.RandomCallback.RandomVector3(
                label_of_updated_GV = "my_vector3_GV", # GV for Global Variable
                x_min_max = np.array([-limits, limits]),
                y_min_max = np.array([-limits, limits]),
                z_min_max = np.array([1, 3]) )
	# Callback 2`
    change_position = PTVR.Data.Callbacks.MovementCallback.SetCartesianCoordinatesInGlobalCS(
                                object_id = my_sphere.id, 
                                label_of_updated_GV_to_use = "my_vector3_GV" ) 
	# Interactions`
    my_scene.AddInteraction (events = [my_event],
                            callbacks = [ create_random_vector3, 
                                          change_position ]       )  
```

A simple demo script illustrating these interactions is presented below in the "Demos" section.


#### Demos
<table id="demo_table">

<tr >
<th id="double_th_1">Python file
<th id="double_th_1">Description 

<tr>
<td>
...\PTVR_Researchers\Demos\Global_Variables\Global_Variables_for_randomizing\  
random_position.py
<td> Create a random triplet at runtime and use it to change an object's position.



</table>

<br>
