Global Variables (GV) {#GlobalVariableIntroduction}
============


#### Introduction

In standard computer programming, a **Global Variable** is a variable with global scope, meaning that it is visible (hence accessible) throughout the program. With this analogy in mind, PTVR has created the notion of a Global Variable to allow Events and Callbacks to communicate at runtime with each other via one or several Global Variables.      
The exact meaning of "at runtime" will be explained later.

<BR>
<div id="note">\emoji :sunglasses: DO NOT CONFUSE the PTVR Global Variable with the [Global (aka WORLD) Coordinate System](@ref globalAndLocalCoordinateSystems). </div>
<BR>

**This section is divided into the following sub-sections:**

-   @subpage globalVariablesForRandomization


