Visual search {#visualsearch}
============

<img width="560" src="visual_search.png"></img>

This example illustrates PTVR pointing techniques. Visual search is a very well-known topic in 2D, but little is known about how humans perform search tasks in immersive 3D environments with more numerous objects. We revisited the 3D visual search task introduced by (Mathur et al. 2018). In this experiment, the user must find and select a target (here, a red cube) among distractors (red and green spheres, green cubes) placed around the user in perimetric coordinates. The user is free to look around and use the hand controller equipped with a laser pointer to point at the target and press the trigger when the target is pointed at. Note that no formal study has been conducted so far with that experiment. 

[Video](https://www.youtube.com/watch?v=UmJG9__rKSI) | Code: visual_search.py 

