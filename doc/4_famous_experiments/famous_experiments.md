Famous experiments {#FamousExperiments}
============

To make PTVR capability even more concrete, we present in this section three experiments, two of which begin directly related to our research in the field of low-vision. These examples show how PTVR features can be used to build complex experiments.

<ul>
<li> @subpage visualsearch
<li> @subpage saroi
<li> @subpage mnread
</ul>