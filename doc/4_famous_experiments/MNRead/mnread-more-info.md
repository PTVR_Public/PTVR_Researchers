The MNREAD test in virtual reality: More information {#mnreadmoreinfo}
============

## The operator's GUI

<img src="operator-gui.png"/>
<i>Figure 2: Screenshot of the operator's GUI</i>

While the patient reads sentences, the operator has access to an interface providing information on the experiment in progress.

The interface is divided into several elements (from left to right and top to bottom):

- The Information block (elements at the top of the interface), where information about the current experiment is displayed: sentence counter, size in logMAR, size in degrees, stopwatch (starts as soon as a new scene with a new sentence is started, and stops when the Stop button is pressed) and distance between headset and text. On the right are the name of the subject, the name of the chart used and the initial distance between the head and the text,
- The Misread Words block, where each word in the sentence displayed to the subject is represented as a button. If the subject reads the word correctly, the operator leaves the button green. If the subject misreads the word, the operator clicks on the misread word, which turns red. There are also two buttons that switch all words to "well-read" or "poorly-read".
- The Reading Speed Evolution block, where you'll find the graph showing the patient's reading speed in words/minute for each sentence.
- The Actions block, with buttons for each action available to run the experiment:
  - The Start button launches the experiment
  - The Quit button exits the experiment. Click on "Yes" to validate the experiment, and the script ```result_file_post_treatment.py``` will be executed to clean up the results file.
  - The Ignore Trial button is used to ignore the trial in progress.
  - The Stop button stops the timer when the patient has finished reading the sentence.
  - The Continue button moves on to the next scene.
  - The Finish Test button stops the experiment before its end.


## Output files

At the end of the experiment, two result files are generated:

- File ```mnread_vr_experiment_Main_SubjectName_date.csv```, where SubjectName is the name of the subject and date is the date and time of the experiment. This file is the raw output of the experiment. Each time an interaction is performed (i.e., each time a button is clicked, for example) a result line is printed. This file contains the following information:
   - ```x_logmar```: size of x in logMAR of the current sentence
   - ```misread_words```: words indicated as misread at time T. If the box is empty, there are no misread words 
   - ```sentence_displayed```: the whole sentence displayed
   - ```x_visual_angle_degrees```: the size of x in degrees of the current sentence

<img src="mnread_vr_experiment_Main_SubjectName_date.png"/>

- File ```mnread_vr_experiment_Main_SubjectName_date_cleaned.csv```, which is a cleaned version of the previous one, obtained thanks to the python script  ```result_file_post_treatment.py```. 

<img src="mnread_vr_experiment_Main_SubjectName_date_cleaned.png"/>





<table id="MNRead-Related demos" >
<th id="double_th_1">Python file
<th id="double_th_2">Description 
<tr>
<td>mnread_single_phrase.py     
<td>Visualize one single MNRead sentence. 
<tr>
<td>mnread_cardboard.py     
<td>Visualize a MNRead cardboard with a set of 3 sentences with printsize varying by 0.1LogMar.
<tr>
<td> <b>mnread_vr_experiment.py 
<td><b>Replication of the MNREAD  in virtual reality.
<BR>

</table>