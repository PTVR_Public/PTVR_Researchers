#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
# Frequences of each caracter in the given language
import Algorithms.mnread_phrases_frequences as phrases_frequences
# Width of each caracter in the TimesRoman font
import Algorithms.mnread_letter_width as width
import pandas


# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

text_description = """
goal : This script allows to split a given sentence respecting the constraints of the MNREAD test

how to use :  Replace the parameter "sentence_set" bellow with the name of the sentence set that you want to use. It has to be in .txt format and 1 sentence per line 

predictions : The script should create a new csv  "csv_result_file.csv" with : 
    - the full sentence 
    - Each 3 piece of the sentence with the current % of the space width when the sentence was cut and if the cut was made in the given intervals (1 for yes, 0 for no) 
    - A summary column indicating if the sentence fully respects mnread test's standards (1 for yes, 0 for no)
    - 2 last rows with the total number of sentences in the given set and the total number of sentences respecting fully the mnread test's standards




@author: johanna delachambre
Created on Thu Jul 22 10:21:51 2021
"""


sentence_set = "set_francais_5.txt"

csv_result_file = pandas.DataFrame(columns=['full_sentence', 'piece_1', 'space_1', 'is_in_interval_1',
                                   'piece_2', 'space_2', 'is_in_interval_2',
                                              'piece_3', 'space_3', 'is_in_interval_3',
                                              'is_sentence_in_interval'])


high_interval = 1.25
low_interval = 0.8
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================


counter_sentence_fitting = 0


def string_width(string_to_analyse):
    """ Calculate the width of a string based on the values in 'letter_width' 
    @param string_to_analyse is the string we want the width of
    """
    string_width = 0
    # For each letter in the word, we calculate its width and make the sum of them
    for i in range(len(string_to_analyse)):
        string_width += width.times_roman_letter_width[string_to_analyse[i]]
    return string_width


def mnread_line_width_standard(_space_width):
    """" Calculate the standard width of the box containing the MNRead sentence
    @param _space_width is the width of a space

    """
    average_character_width = 0
    for letter in phrases_frequences.english_letter_frequence:  # spaces are counted
        # Corresponding to w̅ = ∑ wc fc
        average_character_width += width.times_roman_letter_width[letter] * \
            phrases_frequences.english_letter_frequence[letter]
    # Corresponding to 20w̅ – wspace
    return ((20 * average_character_width) - _space_width)


def mnread_phrase_cutting(_phrase_to_display):
    """ Cut a given phrase to fit mnread's phrase's size standards 
    @param _phrase_to_display is the phrase we want to cut
    """
    mnread_phrase = _phrase_to_display.replace(
        "'", '’')  # The apostrophes are replaced to match those used in the sentences
    # We split each word of the list and place them in a liste
    words = mnread_phrase.split()
    # Used to indicate in the result file if the sentence fits entirely the mnread format
    counter_dans_intervalles = 0

    line = []  # Correspond to the current line

    cutted_phrase = ""  # each final piece of the sentence
    words_width = 0  # total width of the words in the phrase
    number_of_spaces = 0  # number of spaces in the line
    fit = 0  # allows us to know if the space between words are between 0.8 and 1.25
    # use the function string_width() which give the width of a string to get the width of a space
    space_width = string_width(' ')

    list_cutted_phrases = []  # final list with the 3 pieces of the phrase
    fit_list = []  # list of spaces widths in order to access the previous one later in the code
    sentence_check = False

    # We add one by one the words of the phrase in a line. Once the space's width of the line are all between 80% and 125% of the standard width of a space,
    # the current line is validate, and we do this process again in order to have 3 lines
    for i in range(len(words)):

        line.append(words[i])  # We add the word to the current line
        # We get the word's width and we add this width to the total width of the words in the line
        words_width += string_width(words[i])
        # We get the number of spaces in the line
        number_of_spaces = len(line) - 1

        if number_of_spaces != 0:  # if =0, error division by zero
            # We calculate if the spaces's widths are between 80 and 125% of the standard width of a space
            fit = round((mnread_line_width_standard(space_width) -
                        words_width) / (number_of_spaces * space_width), 2)

            if (fit >= low_interval and fit <= high_interval):
                for word in line:
                    if word != line[len(line)-1]:  # if this is not the last word
                        cutted_phrase += word + ' '  # we remake the phrase with spaces
                    else:
                        cutted_phrase += word  # we don't put extra space at the end of the line
                # We add this piece of phrase at the list of the pieces
                list_cutted_phrases.append(cutted_phrase)

                # We add the data to the result file
                csv_result_file.loc[csv_result_file.full_sentence == _phrase_to_display,
                                    'piece_'+str(len(list_cutted_phrases))] = cutted_phrase
                csv_result_file.loc[csv_result_file.full_sentence ==
                                    _phrase_to_display, 'space_'+str(len(list_cutted_phrases))] = fit
                csv_result_file.loc[csv_result_file.full_sentence == _phrase_to_display,
                                    'is_in_interval_'+str(len(list_cutted_phrases))] = 1
                # fit is in the interval, so 1 is printed.
                counter_dans_intervalles += 1
                # We reset the parameters to start again with the next line
                cutted_phrase = ""
                fit = 0
                words_width = 0
                number_of_spaces = 0
                line = []
                sentence_check = True

            if (fit < low_interval and sentence_check == False):

                # If the interval has been passed without the sentence being cut, the difference between the value of the current fit
                # and the low limit of the interval is compared with the difference between the value of the previous fit and the high
                # limit of the interval. The smallest value is retained, and the sentence is cut at the fit corresponding to the smallest difference

                # difference between actual fit and low level of the interval
                lower_limit_diff = low_interval - fit
                # difference between previous fit and high level of the interval
                upper_limit_diff = fit_list[-1] - high_interval

                if upper_limit_diff < lower_limit_diff:

                    line.pop()  # we cut juste before the last added word
                    for word in line:
                        # if this is not the last word
                        if word != line[len(line)-1]:
                            cutted_phrase += word + ' '  # we remake the phrase with spaces
                        else:
                            cutted_phrase += word  # we don't put extra space at the end of the line
                    # We add this piece of phrase at the list of pieces of phrase
                    list_cutted_phrases.append(cutted_phrase)

                    csv_result_file.loc[csv_result_file.full_sentence == _phrase_to_display,
                                        'piece_'+str(len(list_cutted_phrases))] = cutted_phrase
                    csv_result_file.loc[csv_result_file.full_sentence ==
                                        _phrase_to_display, 'space_'+str(len(list_cutted_phrases))] = fit
                    csv_result_file.loc[csv_result_file.full_sentence == _phrase_to_display,
                                        'is_in_interval_'+str(len(list_cutted_phrases))] = 0
                    counter_dans_intervalles += 0
                    # We reset the parameters to start again with the next line
                    cutted_phrase = ""
                    fit = 0
                    words_width = 0
                    number_of_spaces = 0
                    line = []
                    # last added word is transfered to the next line
                    line.append(words[i])
                else:

                    for word in line:
                        # if this is not the last word
                        if word != line[len(line)-1]:
                            cutted_phrase += word + ' '  # we remake the phrase with spaces
                        else:
                            cutted_phrase += word  # we don't put extra space at the end of the line
                    # We add this piece of phrase at the list of pieces of phrase
                    list_cutted_phrases.append(cutted_phrase)

                    csv_result_file.loc[csv_result_file.full_sentence == _phrase_to_display,
                                        'piece_'+str(len(list_cutted_phrases))] = cutted_phrase
                    csv_result_file.loc[csv_result_file.full_sentence ==
                                        _phrase_to_display, 'space_'+str(len(list_cutted_phrases))] = fit
                    csv_result_file.loc[csv_result_file.full_sentence == _phrase_to_display,
                                        'is_in_interval_'+str(len(list_cutted_phrases))] = 0
                    counter_dans_intervalles += 0
                    # We reset the parameters to start again with the next line
                    cutted_phrase = ""
                    fit = 0
                    words_width = 0
                    number_of_spaces = 0
                    line = []
            if (i == len(words)-1 and len(list_cutted_phrases) == 2):
                # Once we have 2 lines, the other words go automatically in the 3rd line
                for word in line:
                    if word != line[len(line)-1]:  # if this is not the last word
                        cutted_phrase += word + ' '  # we remake the phrase with spaces
                    else:
                        cutted_phrase += word  # we don't put extra space at the end of the line
                list_cutted_phrases.append(cutted_phrase)

                csv_result_file.loc[csv_result_file.full_sentence == _phrase_to_display,
                                    'piece_'+str(len(list_cutted_phrases))] = cutted_phrase
                csv_result_file.loc[csv_result_file.full_sentence ==
                                    _phrase_to_display, 'space_'+str(len(list_cutted_phrases))] = fit

                if (fit >= low_interval and fit <= high_interval):
                    csv_result_file.loc[csv_result_file.full_sentence == _phrase_to_display,
                                        'is_in_interval_'+str(len(list_cutted_phrases))] = 1
                    counter_dans_intervalles += 1
                else:
                    csv_result_file.loc[csv_result_file.full_sentence == _phrase_to_display,
                                        'is_in_interval_'+str(len(list_cutted_phrases))] = 0
                    counter_dans_intervalles += 0

            fit_list.append(fit)
            sentence_check = False

    if (counter_dans_intervalles == 3):
        # If all the sentences have been cut according to the defined intervals (so that all the pieces
        # have a value of 1 in the dedicated column of the result file), the sentence fits perfectly and the value is therefore
        # summed up to 1. If at least one of the pieces of a sentence has not been cut according to the defined intervals,
        # then the sentence is set to 0 and does not fit perfectly according to the given standards
        csv_result_file.loc[csv_result_file.full_sentence ==
                            _phrase_to_display, 'is_sentence_in_interval'] = 1
        global counter_sentence_fitting
        counter_sentence_fitting += 1
    else:
        csv_result_file.loc[csv_result_file.full_sentence ==
                            _phrase_to_display, 'is_sentence_in_interval'] = 0
    return list_cutted_phrases


def main():

    abs_file_path = os.path.join(os.path.dirname(
        __file__), sentence_set)  # <-- absolute dir of the .txt
    with open(abs_file_path, encoding='utf8') as sentences:
        phrases_to_display = sentences.readlines()

    csv_result_file['full_sentence'] = phrases_to_display

    for i in range(len(phrases_to_display)):
        mnread_phrase_cutting(phrases_to_display[i])

    csv_result_file.loc[len(csv_result_file)] = [
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
    csv_result_file.loc[len(csv_result_file)] = ['Total number of sentences',
                                                 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', len(phrases_to_display)]
    csv_result_file.loc[len(csv_result_file)] = ['Total number of fitting sentences',
                                                 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', counter_sentence_fitting]

    csv_result_file.to_csv(
        sentence_set[:-4]+"_csv_result_file.csv", sep=";", index=False, header=True)
    print("csv written")


if __name__ == "__main__":
    main()
