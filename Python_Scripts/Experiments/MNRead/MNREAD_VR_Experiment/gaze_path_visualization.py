# -*- coding: utf-8 -*-
import pandas
import glob
import os
from pathlib import Path
from datetime import datetime
import math
from matplotlib import image
import numpy as np
import PTVR.VisualAngleCalculation as VAcalcul
import matplotlib.pyplot as plt
import cv2

import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

import Algorithms.mnread_phrase_cutting as mnread
import PTVR.VisualAngleCalculation as VAcalcul
import mnread_vr_parameters as parameters
import mnread_vr_interface as interface
import mnread_vr_interactions as interactions
import PTVR.Stimuli.Color as color # Used for color in PTVR's objects

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

text_description ="""
goal : Recreate gaze path on the mnread sentence 

how to use :  

predictions :   

Created on :  Thu Oct 20 14:51:14 2022
@author:jdelacha
"""

root_folder = str(Path(__file__).parents[4])
root_folder = root_folder.replace('\\','/' )
my_path = root_folder + "/PTVR_Operators/PTVR_Operators/Results/mnread_vr_experiment/"

list_of_cleaned_files = glob.glob(my_path + "*csv_cleaned.csv") # * means all if need specific format then *.csv
result_cleaned_file_name = max(list_of_cleaned_files, key=os.path.getctime)



current_coordinate_system = np.array([0,1.2,0]) # 1.2 is the height of the static POV when the user is sitting

use_hm_ecc = False
use_cartesian = True

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

def create_scene(my_world,tangent_screen_id, sentence,screen_width,screen_height,logmar,distance):
    
    scene  = PTVR.Stimuli.Scenes.VisualScene () 
    my_world.add_scene(scene)
    
    visual_angle_of_centered_object = VAcalcul.logmar_to_visual_angle_in_degrees(logmar)
    
    tangent_screen_id = str(tangent_screen_id)
    tangent_screen_id = tangent_screen_id.replace('.', '')
    tangent_screen_id = tangent_screen_id[0:5] # pour contrecarer le formatage de l'id du screen. 
    print(tangent_screen_id)
    
    list_of_files = glob.glob( my_path+"*"+str(tangent_screen_id)+"*.csv") # * means all if need specific format then *.csv
    print(list_of_files)
    result_file_name = max(list_of_files, key=os.path.getctime)
    
    data = pandas.read_csv(result_file_name, sep=";", encoding="utf_8")
    eccentricity_right = data['ecc_right_eye_forward_direction_projection_on_screen']
    half_meridian_right = data['hm_right_eye_forward_direction_projection_on_screen']
    eccentricity_left = data['ecc_left_eye_forward_direction_projection_on_screen']
    half_meridian_left = data['hm_left_eye_forward_direction_projection_on_screen']
    
    data_x_right = data['x_right_eye_forward_direction_projection_on_screen']
    data_y_right = data['y_right_eye_forward_direction_projection_on_screen']
    data_x_left = data['x_left_eye_forward_direction_projection_on_screen']
    data_y_left = data['y_left_eye_forward_direction_projection_on_screen']
    
    
    screen_width =  18 * VAcalcul.visual_angle_to_size_on_tangent_screen(visual_angle_of_centered_object = visual_angle_of_centered_object, 
                                                                         radial_distance = distance)
    
    screen_height = 8* VAcalcul.visual_angle_to_size_on_tangent_screen(visual_angle_of_centered_object = visual_angle_of_centered_object, 
                                                                       radial_distance = distance)
    
    tangent_screen = PTVR.Stimuli.World.TangentScreen(position_in_current_CS=np.array([0,0,(parameters.viewing_distance_in_m)]),
                                                       color=color.RGBColor(r=1.0,g=1.0,b=1.0,a=1.0),
                                                       size_in_meters=np.array([screen_width,screen_height]))
    text = PTVR.Stimuli.World.Text(text=sentence,
                                   font_name="TimesRoman",
                                   visual_angle_of_centered_x_height_deg = visual_angle_of_centered_object 
                                   )
    text.SetMnreadFormat(x_height_in_meters = VAcalcul.visual_angle_to_size_on_tangent_screen(visual_angle_of_centered_object = visual_angle_of_centered_object, 
                                                                         radial_distance = distance))
    
    tangent_screen._place_object_on_tangent_screen(objToPlace = text,
                                                   eccentricity=0,
                                                   halfMeridian=0)
    
    scene.place(tangent_screen)
    scene.place(text)
    
    sphere_size = VAcalcul.visual_angle_to_size_on_tangent_screen(visual_angle_of_centered_object = visual_angle_of_centered_object/2, 
                                                                             radial_distance = distance)
    for i in range(len(data_x_right)-1) :
        
        
        sphere = PTVR.Stimuli.World.Sphere(size_in_meters=np.array([sphere_size, sphere_size, sphere_size]),
                                           color = color.RGBColor(r=1.0,g=0.0,b=0.0,a=1.0))
        if (use_cartesian):
            tangent_screen._place_object_on_tangent_screen(objToPlace = sphere,
                                                       x=data_x_right[i],
                                                       y=data_y_right[i])
        if(use_hm_ecc):
            tangent_screen._place_object_on_tangent_screen(objToPlace = sphere,
                                                   halfMeridian=half_meridian_right[i],
                                                   eccentricity=eccentricity_right[i])
    
        scene.place(sphere)
    for i in range(len(data_x_left)-1) :
        
        sphere = PTVR.Stimuli.World.Sphere(size_in_meters=np.array([sphere_size, sphere_size, sphere_size]),
                                           color = color.RGBColor(r=0.0,g=0.0,b=1.0,a=1.0))
        if(use_cartesian):
            tangent_screen._place_object_on_tangent_screen(objToPlace = sphere,
                                                       x=data_x_left[i],
                                                       y=data_y_left[i])
        if(use_hm_ecc) :
            tangent_screen._place_object_on_tangent_screen(objToPlace = sphere,
                                                   halfMeridian=half_meridian_left[i],
                                                   eccentricity=eccentricity_left[i])
    
        scene.place(sphere)
        
    
    
    start_controller_input = PTVR.Data.Input.HandController(valid_responses=['right_touch', 'left_touch'])
    start_keyboard_input = PTVR.Data.Input.Keyboard(valid_responses=['space'])
    next_scene_event = PTVR.Data.Event.EndCurrentScene()
    ## Take picture of the sentence 
    capture = PTVR.Data.Event.TakePicture(filename = "image_captured_"+tangent_screen_id+'.png')
    
    scene.AddInteraction(inputs=[start_controller_input,start_keyboard_input],events=[next_scene_event,capture])
    
    

def called_function(_result_cleaned_file_name):
    print("in called function")
    my_world = visual.The3DWorld(name_of_subject=parameters.subject_name)
    my_world.translate_coordinate_system_along_global(current_coordinate_system)
    
    data_cleaned = pandas.read_csv(_result_cleaned_file_name, sep=";", encoding="utf_8")
    
    for i in range(len(data_cleaned['tangent_screen_id'])):
        create_scene(my_world,  
                     data_cleaned['tangent_screen_id'][i],
                     data_cleaned['sentence_displayed'][i],
                     screen_width = data_cleaned['tangent_screen_width_in_m'][i],
                     screen_height =data_cleaned['tangent_screen_height_in_m'][i],
                     logmar = data_cleaned['x_logmar'][i],
                     distance = data_cleaned['viewing_distance_in_m'][i])
    
    my_world.write()
    print("The experiment has been written.")  
    PTVR.SystemUtils.LaunchThe3DWorld("subject") 

def main():
    my_world = visual.The3DWorld(name_of_subject=parameters.subject_name)
    my_world.translate_coordinate_system_along_global(current_coordinate_system)
    
    data_cleaned = pandas.read_csv(result_cleaned_file_name, sep=";", encoding="utf_8")
    
    for i in range(len(data_cleaned['tangent_screen_id'])):
        create_scene(my_world,  
                     data_cleaned['tangent_screen_id'][i],
                     data_cleaned['sentence_displayed'][i],
                     screen_width = data_cleaned['tangent_screen_width_in_m'][i],
                     screen_height =data_cleaned['tangent_screen_height_in_m'][i],
                     logmar = data_cleaned['x_logmar'][i],
                     distance = data_cleaned['viewing_distance_in_m'][i])
    
    my_world.write()
    print("The experiment has been written.")
    PTVR.SystemUtils.LaunchThe3DWorld("subject") 
    
    
if __name__ == "__main__":
            main()

