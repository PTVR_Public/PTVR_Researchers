#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
IMPORTANT notes:
Note 1:     Make sure to add in the python Path manager the following path:
        ...\Python_Scripts\Experiments\MNREAD\MNREAD_VR_Experiment\Algorithms
        containing the different libraries needed for this main code
Note 2: When you open the .csv results file (say with the opensource LibreOffice),
        make sure to solely use the separator ";" (i.e. the semi-colon character)


goal : This experiment recreates the MNRead test in VR

how to use :  Execute this script to launch the entire experiment. 
The main parameters of the experiment have to be set in the file mnread_vr_parameters.py

predictions : An interface allowing you to start the experiment should be displayed on 
the PC. 
For each scene, a new sentence is displayed in the headset. An interface should be 
visible on the PC screen to interact with the experiment 

tech note : https://notes.inria.fr/qJt3btPVTnS5zXckWjzn2Q

Created on :  Tue May 17 11:22:36 2022
@author:jdelacha
"""


import PTVR.SystemUtils

import numpy as np
from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import TangentScreen, Text
import PTVR.Stimuli.UIObjects
import Algorithms.mnread_phrase_cutting as mnread
import PTVR.Tools as tools
import mnread_vr_parameters as parameters
import mnread_vr_interface as interface
import mnread_vr_interactions as interactions
import PTVR.Pointing.ReticleImageFromDrawing as RG
from PTVR.Pointing.PointingCursor import BinocularReticle, ReticleContingency
import PTVR.Stimuli.Color as color

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================


headPOV = parameters.height_of_subject_in_m
# 1.2 is the height of the static POV when the user is sitting
current_coordinate_system = np.array([0, headPOV, 0])
informations_text_size = parameters.visual_angle_of_centered_object

# Current tangent screen used in the operator interface to get the distance between the headPOV and the screen
current_tangent_screen = []
empty_screen = []
new_origin_coordinates = []
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================


def create_scotoma(scene):
    if (parameters.scotoma_visible):
        scotoma = BinocularReticle(
            contingent_type=ReticleContingency.GAZE)  # reticle
        scotoma.sprite = parameters.scotoma_image
        scotoma.spriteAngularRadiusX = parameters.scotoma_x_angular_radius
        scotoma.spriteAngularRadiusY = parameters.scotoma_y_angular_radius
        scene.AddPointingCursor(scotoma)

    if (parameters.reticle_visible):
        reticle_gaze_color = color.RGBColor(r=0, g=0, b=0.9)
        my_reticle_outer_radius_deg = 6.
        my_reticle_inner_radius_deg = 2.

        # The reticle resolution in degrees
        my_image_resolution_pix = 512

        # The reticle color and width
        my_reticle_color = reticle_gaze_color
        my_reticle_line_width_deg = 0.5

        reticle_gen = RG.ReticleImageFromDrawing(image_resolution_pix=my_image_resolution_pix,
                                                 reticle_outer_radius_deg=my_reticle_outer_radius_deg,
                                                 reticle_inner_radius_deg=my_reticle_inner_radius_deg,
                                                 reticle_color=my_reticle_color,
                                                 reticle_line_width_deg=my_reticle_line_width_deg)

        reticle_eyes = BinocularReticle(
            contingent_type=ReticleContingency.GAZE)
        reticle_eyes.SetMaxDistanceProjectionOptions(
            max_distance=parameters.viewing_distance_in_m, is_projected=False)
        reticle_gen.Initialize(new_image_resolution_pix=my_image_resolution_pix, new_reticle_inner_radius=my_reticle_inner_radius_deg,
                               newreticle_outer_radius_deg=my_reticle_outer_radius_deg, isDefaultReticle=True, reticle_color=reticle_gaze_color, reticle_line_width_deg=my_reticle_line_width_deg)
        reticle_eyes.SetCustomReticleGenerator(reticle_gen)
        scene.AddPointingCursor(reticle_eyes)


def create_tangent_screen(visual_angle_of_centered_object, scene, is_active, first_screen=False):

    screen_width = 18 * tools.visual_angle_to_size_on_perpendicular_plane(visual_angle_of_centered_object_in_deg=visual_angle_of_centered_object,
                                                                          viewing_distance_in_m=parameters.viewing_distance_in_m)

    screen_height = 8 * tools.visual_angle_to_size_on_perpendicular_plane(visual_angle_of_centered_object_in_deg=visual_angle_of_centered_object,
                                                                          viewing_distance_in_m=parameters.viewing_distance_in_m)

    if (is_active and parameters.record_eye_gaze and first_screen == False):
        tangent_screen = TangentScreen(position_in_current_CS=np.array([0, 0, (parameters.viewing_distance_in_m)]),
                                       color=parameters.tangent_screen_color,
                                       size_in_meters=np.array(
                                           [screen_width, screen_height]),
                                       is_active=is_active,
                                       output_gaze=True)
    else:
        tangent_screen = TangentScreen(position_in_current_CS=np.array([0, 0, (parameters.viewing_distance_in_m)]),
                                       color=parameters.tangent_screen_color,
                                       size_in_meters=np.array(
                                           [screen_width, screen_height]),
                                       is_active=is_active
                                       )

    scene.fill_in_results_file_column(
        column_name=parameters.resultfile_tangent_screen_width_in_m, value=screen_width)
    scene.fill_in_results_file_column(
        column_name=parameters.resultfile_tangent_screen_height_in_m, value=screen_height)
    return tangent_screen


def create_scene(phrase_to_display,
                 visual_angle_of_centered_object,
                 scene, my_world, phrase_to_write="", tag="",
                 logmar_value=0):
    """ 
    This function is used to create each scene displayed 
    """

    # The tangent screen is the screen behind the text
    # The size is set so that the text fits the screen borders

    tangent_screen = create_tangent_screen(
        visual_angle_of_centered_object=visual_angle_of_centered_object, scene=scene, is_active=True)

    x_height_in_meters = tools.visual_angle_to_size_on_perpendicular_plane(visual_angle_of_centered_object_in_deg=visual_angle_of_centered_object,
                                                                           viewing_distance_in_m=parameters.viewing_distance_in_m)

    # Text displayed on the screen
    text = Text(text=phrase_to_display,
                font_name=parameters.font_to_use,
                color=parameters.text_color,
                horizontal_alignment="Flush",
                vertical_alignment="Middle",  # "Middle",
                visual_angle_of_centered_x_height_deg=visual_angle_of_centered_object,
                )

    text.isWrapping = True
    text.textboxScale = np.array([(17.32 * x_height_in_meters), 1.0])

    # text.SetMnreadFormat(x_height_in_meters=tools.visual_angle_to_size_on_perpendicular_plane(visual_angle_of_centered_object_in_deg=visual_angle_of_centered_object,
    #                                                                                           viewing_distance_in_m=parameters.viewing_distance_in_m))

    # Fill columns in the result file

    scene.fill_in_results_file_column(
        column_name=parameters.resultfile_sentence_display, value=phrase_to_write)
    scene.fill_in_results_file_column(
        column_name=parameters.resultfile_sentence_tag, value=tag)
    scene.fill_in_results_file_column(
        column_name=parameters.resultfile_visual_angle, value=str(visual_angle_of_centered_object))
    scene.fill_in_results_file_column(
        column_name=parameters.resultfile_misread_words, value="")
    scene.fill_in_results_file_column(
        column_name=parameters.resultfile_logmar, value=str(logmar_value))
    scene.fill_in_results_file_column(
        column_name=parameters.resultfile_viewing_distance, value=parameters.viewing_distance_in_m)
    scene.fill_in_results_file_column(
        column_name=parameters.resultfile_sentence_number, value=interactions.counter_sentences)
    scene.fill_in_results_file_column(
        column_name=parameters.resultfile_tangent_screen_id, value=tangent_screen.id)

    # Empty screen displayed between each sentence, which has a size adapted to the next sentence

    next_visual_angle = tools.logmar_to_visual_angle_in_degrees(
        logmar_value - parameters.decreasing_size)
    if (parameters.sentences_testing_mode):
        empty_tangent_screen = create_tangent_screen(
            visual_angle_of_centered_object=visual_angle_of_centered_object, scene=scene, is_active=False)
    else:
        empty_tangent_screen = create_tangent_screen(
            visual_angle_of_centered_object=next_visual_angle, scene=scene, is_active=False)

    # Placement of the elements in the scene
    # tangent_screen._place_object_on_tangent_screen(objToPlace = text,
    #                                                eccentricity=0,
    #                                                halfMeridian=0)
    text.set_perimetric_coordinates_on_screen(tangent_screen,
                                              eccentricity_local_deg=0.0,
                                              half_meridian_local_deg=0.0
                                              )
    scene.place(tangent_screen, my_world)
    scene.place(empty_tangent_screen, my_world)
    scene.place(text, my_world)

    current_tangent_screen.append(tangent_screen)
    empty_screen.append(empty_tangent_screen)
    # Add the scene to the experiment
    my_world.add_scene(scene)


def sentences_sequence_mnread(my_world):
    """     
    At each scene a new sentence is displayed. Each new phrase has a decreasing size.    
    """

    # Variable used to store the smallest logmar displayed in order to calculate the reading acuity
    smallest_logmar = 0

    # Variable with the current visual angle. Used to decrease the size of each sentence
    current_visual_angle = parameters.visual_angle_of_centered_object

    if (parameters.start_logmar):
        current_visual_angle = tools.logmar_to_visual_angle_in_degrees(
            parameters.start_logmar)

    for i in range(parameters.number_of_sentences_to_use):
        interactions.counter_sentences += 1
        phrase_to_display = parameters.phrases_to_display[i].rstrip("\n")
        sentence_tag = ""
        if (parameters.use_tagged_sentences):
            sentence_tag = parameters.sentence_tag[i]
        # conversion degrees to logmar
        logmar_value = tools.visual_angle_in_degrees_to_logmar(
            current_visual_angle)
        # Transformation of the phrase in order to have 3 lines following the MNRead's standards
        list_cutted_phrases_from_mnread_phrase_cutting = mnread.mnread_phrase_cutting(
            phrase_to_display)

        phrase_with_back_to_line = str(list_cutted_phrases_from_mnread_phrase_cutting[0]) + "\n" + str(
            list_cutted_phrases_from_mnread_phrase_cutting[1]) + "\n" + str(list_cutted_phrases_from_mnread_phrase_cutting[2])

        # Creation of the scene with the displayed phrase
        t1 = VisualScene(background_color=parameters.scene_background_color)
        create_scotoma(t1)
        create_scene(phrase_to_display=phrase_with_back_to_line,
                     visual_angle_of_centered_object=current_visual_angle,
                     scene=t1,
                     my_world=my_world,
                     phrase_to_write=phrase_to_display,
                     tag=sentence_tag,
                     logmar_value=logmar_value)

        # Creation of the main UI template
        interface.create_background_image_UI(t1)
        # Creation of one button per word
        interactions.create_words_UI(
            t1, my_world, list_cutted_phrases_from_mnread_phrase_cutting)
        # Creation of main button UI to interact with the experiment
        interactions.create_main_UI(t1, my_world, list_cutted_phrases_from_mnread_phrase_cutting,
                                    current_tangent_screen[0], empty_screen[0], logmar_value, current_visual_angle)
        current_tangent_screen.clear()
        empty_screen.clear()

        if (i == len(parameters.phrases_to_display)):
            smallest_logmar = logmar_value

        # Management of the current visual angle in order to decrease the size of the next phrase
        if (parameters.sentences_testing_mode == False):
            current_visual_angle = tools.logmar_to_visual_angle_in_degrees(
                logmar_value - parameters.decreasing_size)

    # Creation of the end scene
    end_scene = VisualScene()
    create_scotoma(end_scene)
    interface.create_background_image_UI(end_scene)
    interactions.end_experiment(my_world, end_scene, smallest_logmar)
    my_world.add_scene(end_scene)


def main():

    # Set-up the experiment file
    my_world = The3DWorld(
        operatorMode=True, name_of_subject=parameters.subject_name)
    my_world.translate_coordinate_system_along_global(
        current_coordinate_system)
    start_scene = VisualScene(
        background_color=parameters.scene_background_color)
    my_world.add_scene(start_scene)

    current_visual_angle = parameters.visual_angle_of_centered_object
    if (parameters.start_logmar):
        current_visual_angle = tools.logmar_to_visual_angle_in_degrees(
            parameters.start_logmar)

    first_empty_screen = create_tangent_screen(
        current_visual_angle, start_scene, is_active=True, first_screen=True)
    start_scene.place(first_empty_screen, my_world)

    create_scotoma(start_scene)

    interactions.start_experiment(start_scene, my_world, first_empty_screen)

    # Write The3DWorld to file
    for i in range(1, parameters.number_of_sentences_to_use):
        my_world.scenes[my_world.id_scene[i]].interaction[-1].callbacks[0].SetNextScene(
            id_next_scene=my_world.id_scene[-1])
    my_world.write()
    print("The experiment has been written.")


if __name__ == "__main__":
    main()

    PTVR.SystemUtils.LaunchThe3DWorld(parameters.subject_name)
