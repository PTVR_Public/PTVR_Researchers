Notre gros matou a dormi dehors avant l’arrivée de ta nièce
Les parents de mon professeur sont des sportifs très connus
Je repars en voyage cette année avec un ami et ses cousines
Ton petit chiot noir a mangé treize vers mais pas de fourmi
Ta grand-mère a eu peur des orages sur ma maison hier soir
Ce petit chien dans sa niche est bien au chaud dans un coin
La fille des voisins est venue à la plage avec maman et moi
L’enfant a reçu des cadeaux et des jeux pour cette occasion 
La pluie tombe fort sur le village de ma femme près du bois
Tu es allé voir leur mère avec ma sœur et ta meilleure amie
La petite fille a une poupée qui aime le gâteau de sa mamie
Le pêcheur est déjà parti ce matin avec mon frère et un ami
Tu as reçu plein de cadeaux hier quand ton amie est rentrée
Je pars en vacances dans le sud avec un cousin et mon frère
Notre petit chat est parti cette fois sans maman et mon ami
Le mariage de mon cousin se passait au soleil sur une plage
Il est parti chez son médecin pour avoir son remède miracle
J’ai déjà apporté un nouveau jeu à mon frère qui aime jouer
L’été je vais aller à la mer et mon chien ira chez ma maman 