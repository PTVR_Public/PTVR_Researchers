# -*- coding: utf-8 -*-
import PTVR.SystemUtils
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
 
import mnread_vr_experiment as experiment
import mnread_vr_parameters as parameters
import mnread_vr_interface as interface
# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
 
text_description ="""
goal : This script contains all UI interactions for the MNREAD vr experiment 

how to use :  Execute the script mnread_vr_experiment to launch the entire experiment. 

predictions : An interface allowing you to start the experiment should be first displayed. For each scene, an interface should be visible on the 
PC screen to interact with the experiment. The interface should be the same as showed in the tech note 

tech note : https://notes.inria.fr/qJt3btPVTnS5zXckWjzn2Q

@author:jdelacha
"""


resultfile_sentence_display = parameters.resultfile_sentence_display
resultfile_visual_angle = parameters.resultfile_visual_angle
resultfile_misread_words = parameters.resultfile_misread_words
resultfile_logmar = parameters.resultfile_logmar

counter_sentences = 0
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================




def start_experiment(start_scene,my_world,empty_screen) :
    
    
# =============================================================================
#     OBJECT UI CREATION
# =============================================================================
    interface.create_background_image_UI(start_scene)
    
    quit_button_inactive = interface.quit_button_inactive()
    quit_description = interface.quit_button_description()
    
    ignore_button_inactive = interface.ignore_button_inactive()
    ignore_description = interface.ignore_button_description()
    
    start_button = interface.start_button()
    start_description = interface.start_button_description()
    
    informations_content = interface.informations_text_left()
    mnread_sentences = interface.number_sentences()
    mnread_sentences_description = interface.number_sentences_description()
    logmar = interface.logmar()
    logmar_description = interface.logmar_description()
    
    # ## Timer
    # timer = interface.timer()
    # timer_description = interface.timer_description()
    
    ## Distance head-text   
    # distance_head_text = my_world.calculatorManager.AddRadialDistanceFromHeadPOV(empty_screen)
    # headset_screen_distance_content = interface.headset_screen_distance_content(distance_head_text.StringIntegration())
    # headset_screen_distance_description = interface.headset_screen_distance_description()
    
    
    
# =============================================================================
#     INPUT CREATION
# =============================================================================
    start_input= PTVR.Data.Event.Button(id_object_button=start_button.id)
    start_controller_input = PTVR.Data.Event.HandController(valid_responses=['right_touch', 'left_touch'])
    

# =============================================================================
#     callback CREATION
# =============================================================================
    next_scene_callback = PTVR.Data.Callback.EndCurrentScene()
    ## Take picture of the sentence 
    capture = PTVR.Data.Callback.TakePicture(filename = "image_"+str(counter_sentences))
    
# =============================================================================
#     OBJECT PLACEMENT
# =============================================================================
    
    start_scene.placeUI(quit_button_inactive)
    start_scene.placeUI(quit_description)
    start_scene.placeUI(ignore_button_inactive)
    start_scene.placeUI(ignore_description)
    start_scene.placeUI(start_button)
    start_scene.placeUI(start_description)
    
    start_scene.placeUI(informations_content)
    #start_scene.placeUI(headset_screen_distance_content)
    #start_scene.placeUI(timer)
    start_scene.placeUI(mnread_sentences)
    start_scene.placeUI(mnread_sentences_description)
    start_scene.placeUI(logmar)
    start_scene.placeUI(logmar_description)
    #start_scene.placeUI(timer_description)
    # start_scene.placeUI(headset_screen_distance_description)


# =============================================================================
#   INTERACTIONS
# =============================================================================
    if(parameters.take_picture):
        start_scene.AddInteraction(events=[start_input,start_controller_input],callbacks=[next_scene_callback,capture])
    else : 
        start_scene.AddInteraction(events=[start_input,start_controller_input],callbacks=[next_scene_callback])
    
    experiment.sentences_sequence_mnread(my_world)
    
    

def create_main_UI(scene,my_world,phrase,current_tangent_screen,empty_screen,logmar,degrees):
    
# =============================================================================
#     OBJECT UI CREATION
# =============================================================================
    ## Graph UI
    graph_UI= interface.create_graph_UI()
    
    ## Pop up Container, for Quit and Ignore validation
    popup_container = interface.pop_up_container()
    
    ## Stop
    stop_button = interface.stop_button()
    stop_description = interface.stop_button_description()
    
    ## Continue
    continue_button = interface.continue_button()
    continue_description = interface.continue_button_description()
    
    ## Quit
    quit_button = interface.quit_button()
    quit_button_inactive = interface.quit_button_inactive()
    yes_quit_button = interface.yes_button()
    no_quit_button = interface.no_button()
    quit_description = interface.quit_button_description()
    
    ## Ignore
    ignore_button = interface.ignore_button()
    ignore_button_inactive = interface.ignore_button_inactive()
    yes_ignore_button = interface.yes_button() 
    no_ignore_button = interface.no_button()
    ignore_description = interface.ignore_button_description()
    
    ## Finish test
    finish_test_button = interface.finish_test_button()
    finish_test_button_inactive = interface.finish_test_button_inactive()
    yes_finish_button = interface.yes_button() 
    no_finish_button = interface.no_button()
    finish_validate_text = interface.validate_finish_text()
    finish_desciption = interface.finish_button_description()
    
    ## Text informations
    informations_text_left = interface.informations_text_left()
    validate_ignore_text = interface.validate_ignore_text()
    validate_quit_text = interface.validate_quit_text()
    
    # ## Timer
    
    # timer_calculator = my_world.calculatorManager.AddElaspedTimeCurrentScene()
    
    
    # timer_calculator_current = timer_calculator.StringIntegration()
    # timer_calculator_freezed = timer_calculator.StringIntegration(is_freezed = True)
    
    
    warning = interface.distance_warning(is_active = False)
    
    
    mnread_sentences = interface.number_sentences(counter_sentences)
    mnread_sentences_description = interface.number_sentences_description()
    logmar = interface.logmar(round(logmar,2))
    logmar_description = interface.logmar_description(round(degrees,2))
    
    # timer = interface.timer(timer_calculator_current)
    # timer_description = interface.timer_description()
    
    # ## Distance head-text   
    # distance_head_text = my_world.calculatorManager.AddRadialDistanceFromHeadPOV(empty_screen)
    # headset_screen_distance_content = interface.headset_screen_distance_content(distance_head_text.StringIntegration())
    # headset_screen_distance_description = interface.headset_screen_distance_description()
    
# =============================================================================
#     INPUT CREATION
# =============================================================================
    
    ## Stop
    stop_input= PTVR.Data.Event.Button(id_object_button=stop_button.id)
    if(parameters.solo_testing_mode):
        stop_controller_input = PTVR.Data.Event.HandController(valid_responses=['right_trigger', 'left_trigger'])
    
    ## Continue
    continue_input= PTVR.Data.Event.Button(id_object_button=continue_button.id)
    if(parameters.solo_testing_mode):
        continue_controller_input = PTVR.Data.Event.HandController(valid_responses=['right_touch', 'left_touch'])
    
    ## Quit- related events
    quit_input= PTVR.Data.Event.Button(id_object_button=quit_button.id)
    yes_quit_input= PTVR.Data.Event.Button(id_object_button=yes_quit_button.id)
    no_quit_input= PTVR.Data.Event.Button(id_object_button=no_quit_button.id)
    
    ## Ignore-related events
    ignore_input= PTVR.Data.Event.Button(id_object_button=ignore_button.id)
    yes_ignore_input= PTVR.Data.Event.Button(id_object_button=yes_ignore_button.id)
    no_ignore_input= PTVR.Data.Event.Button(id_object_button=no_ignore_button.id)
    
    
    ## Finish test input 
    finish_test_input = PTVR.Data.Event.Button(id_object_button=finish_test_button.id)
    yes_finish_input = PTVR.Data.Event.Button(id_object_button=yes_finish_button.id)
    no_finish_input = PTVR.Data.Event.Button(id_object_button=no_finish_button.id)
    
    ## Distance from point 
    
    # Outside given limit
    outside_given_limit_input =  PTVR.Data.Event.DistanceFromPoint(activation_area_center_point=experiment.current_coordinate_system ,
                                                                   activation_area_radius=parameters.limit_warning_area_radius,
                                                                   mode = "release") 
    # Inside given limit 
    inside_given_limit_input =  PTVR.Data.Event.DistanceFromPoint(activation_area_center_point=experiment.current_coordinate_system,
                                                                  activation_area_radius=parameters.limit_warning_area_radius,
                                                                  mode="press")
    
    
# =============================================================================
#     callback CREATION
# =============================================================================

    ## Timer 
    # freeze_timer_callback = PTVR.Data.Callback.ChangeObjectText(text_object_id=timer.id, text= timer_calculator_freezed + " s.")
        
    ## Feedback wrong distance 
    
    show_warning_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=warning.id, effect="activate",callback_name="OutsideAreaLimit")
    hide_warning_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=warning.id, effect="deactivate",callback_name="InsideAreaLimit")
    
    ## Take picture of the sentence 
    capture = PTVR.Data.Callback.TakePicture(filename = "image_"+str(counter_sentences))
    
    ## Graph UI
    new_point_graph_callback = PTVR.Data.Callback.AddNewPoint(graph_id = graph_UI.id)
    all_points_graph_callback = PTVR.Data.Callback.ShowAllPoints(graph_id = graph_UI.id)
    delete_point_graph_callback = PTVR.Data.Callback.DeleteLastPoint(graph_id = graph_UI.id)
    save_point_graph_callback = PTVR.Data.Callback.SaveLastPoint(graph_id = graph_UI.id)
        
    ## Stop 
    hide_stop_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=stop_button.id, effect="deactivate")
    hide_stop_description_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=stop_description.id, effect="deactivate")
    
    ## Continue
    continue_callback = PTVR.Data.Callback.EndCurrentScene()
    show_continue_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=continue_button.id, effect="activate")
    show_continue_description_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=continue_description.id, effect="activate")
    
    ## Quit
    quit_callback = PTVR.Data.Callback.Quit()
    
    show_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=quit_button.id, effect="activate")
    
    hide_quit_inactive_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=quit_button_inactive.id, effect="deactivate")
    
    show_yes_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=yes_quit_button.id, effect="activate")
    hide_yes_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=yes_quit_button.id, effect="deactivate")
    
    show_no_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=no_quit_button.id, effect="activate")
    hide_no_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=no_quit_button.id, effect="deactivate")
    
    show_text_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=validate_quit_text.id, effect="activate")
    hide_text_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=validate_quit_text.id, effect="deactivate")
    
    show_container_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=popup_container.id, effect="activate")
    hide_container_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=popup_container.id, effect="deactivate")
    
    ## Ignore
    print_cancel_callback = PTVR.Data.Callback.PrintResultFileLine(callback_name="IgnoreTrial")
    
    show_ignore_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=ignore_button.id, effect="activate")
    hide_ignore_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=ignore_button.id, effect="deactivate")
    
    show_ignore_inactive_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=ignore_button_inactive.id, effect="activate")
    hide_ignore_inactive_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=ignore_button_inactive.id, effect="deactivate")
    
    show_text_ignore_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=validate_ignore_text.id, effect="activate")
    hide_text_ignore_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=validate_ignore_text.id, effect="deactivate")
    
    show_yes_ignore_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=yes_ignore_button.id, effect="activate")
    hide_yes_ignore_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=yes_ignore_button.id, effect="deactivate")
    
    show_no_ignore_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=no_ignore_button.id, effect="activate")
    hide_no_ignore_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=no_ignore_button.id, effect="deactivate")
    
    
    ## Finish test 
    finish_test_callback = PTVR.Data.Callback.EndCurrentScene()
    
    show_finish_test_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=finish_test_button.id, effect="activate")
    hide_finish_test_inactive_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=finish_test_button_inactive.id, effect="deactivate")
    
    show_yes_finish_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=yes_finish_button.id, effect="activate")
    hide_yes_finish_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=yes_finish_button.id, effect="deactivate")
    
    show_no_finish_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=no_finish_button.id, effect="activate")
    hide_no_finish_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=no_finish_button.id, effect="deactivate")
    
    show_text_finish_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=finish_validate_text.id, effect="activate")
    hide_text_finish_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=finish_validate_text.id, effect="deactivate")
    
    
    ## tangent screen 
    hide_text_tangent_screen_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=current_tangent_screen.id, effect="deactivate")
    show_empty_screen_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=empty_screen.id, effect="activate")
    
    
# =============================================================================
#     OBJECT PLACEMENT
# =============================================================================
   
    # DO NOT change the order of placement, otherwise there might be positioning issues with the pop up container
    
    scene.placeUI(graph_UI)
    
    scene.placeUI(stop_button)
    scene.placeUI(stop_description)
    
    scene.placeUI(continue_button)
    scene.placeUI(continue_description)
    
    scene.placeUI(quit_button)
    scene.placeUI(quit_button_inactive)
    scene.placeUI(quit_description)
    
    scene.placeUI(ignore_button)
    scene.placeUI(ignore_button_inactive)
    scene.placeUI(ignore_description)
    
    scene.placeUI(finish_test_button)
    scene.placeUI(finish_test_button_inactive)
    scene.placeUI(finish_desciption)
    
    
    scene.placeUI(informations_text_left)
    
    scene.placeUI(mnread_sentences)
    scene.placeUI(mnread_sentences_description)
    scene.placeUI(logmar)
    scene.placeUI(logmar_description)
    # scene.placeUI(timer_description)
    # scene.placeUI(headset_screen_distance_description)
    
    # scene.placeUI(timer)
    # scene.placeUI(headset_screen_distance_content)
    scene.placeUI(warning)
    
    scene.placeUI(popup_container)
    
    scene.placeUI(validate_quit_text)
    scene.placeUI(yes_quit_button)
    scene.placeUI(no_quit_button)

    scene.placeUI(validate_ignore_text)
    scene.placeUI(yes_ignore_button)
    scene.placeUI(no_ignore_button)
    
    scene.placeUI(finish_validate_text)
    scene.placeUI(yes_finish_button)
    scene.placeUI(no_finish_button)
    
# =============================================================================
#   INTERACTIONS
# =============================================================================
    
    if(parameters.solo_testing_mode):
        events_stop = [stop_input,stop_controller_input]
        events_continue=[continue_input,continue_controller_input]
    else: 
        events_stop =[stop_input]
        events_continue=[continue_input]
        
    if(parameters.take_picture):
        callbacks_continue = [continue_callback,save_point_graph_callback,all_points_graph_callback,capture]
    else : 
        callbacks_continue = [continue_callback,save_point_graph_callback,all_points_graph_callback]
    scene.AddInteraction(events=events_stop,callbacks=[new_point_graph_callback,
                                               # freeze_timer_callback,
                                               hide_stop_callback,
                                               hide_stop_description_callback,
                                               show_continue_callback,
                                               show_continue_description_callback,
                                               hide_quit_inactive_callback,
                                               show_quit_callback,
                                               hide_ignore_inactive_callback,
                                               show_ignore_callback,
                                               hide_finish_test_inactive_callback,
                                               show_finish_test_callback,
                                               hide_text_tangent_screen_callback,
                                               show_empty_screen_callback
                                                ])
    
    scene.AddInteraction(events=events_continue,callbacks=callbacks_continue)
    
    scene.AddInteraction(events=[quit_input],callbacks=[
                                                  show_text_quit_callback, 
                                                  show_yes_quit_callback, 
                                                  show_no_quit_callback,
                                                  show_container_quit_callback])
    
    scene.AddInteraction(events=[no_quit_input],callbacks=[hide_text_quit_callback,
                                                      hide_container_quit_callback,
                                                      hide_yes_quit_callback, 
                                                      hide_no_quit_callback])
    
    scene.AddInteraction(events=[yes_quit_input],callbacks=[quit_callback])
    
    scene.AddInteraction(events=[ignore_input],callbacks=[show_text_ignore_callback, 
                                                       show_yes_ignore_callback, 
                                                       show_no_ignore_callback,
                                                       show_container_quit_callback
                                                  ])
    
    scene.AddInteraction(events=[no_ignore_input],callbacks=[hide_text_ignore_callback,
                                                      hide_yes_ignore_callback, 
                                                      hide_no_ignore_callback,
                                                      hide_container_quit_callback,
                                                      ])
    
    scene.AddInteraction(events=[yes_ignore_input],callbacks=[ 
                                                      hide_ignore_callback,
                                                      show_ignore_inactive_callback,
                                                      hide_text_ignore_callback,
                                                      hide_yes_ignore_callback,
                                                      hide_no_ignore_callback,
                                                      hide_container_quit_callback,
                                                      delete_point_graph_callback,
                                                      print_cancel_callback
                                                      
                                                      ])
    
    
    scene.AddInteraction(events=[outside_given_limit_input], callbacks=[show_warning_callback])
    scene.AddInteraction(events=[inside_given_limit_input], callbacks=[hide_warning_callback])
   
    scene.AddInteraction(events=[finish_test_input],callbacks=[ 
                                                            show_text_finish_callback, 
                                                            show_yes_finish_callback, 
                                                            show_no_finish_callback,
                                                            show_container_quit_callback
                                                      ])
    
    scene.AddInteraction(events=[no_finish_input],callbacks=[hide_text_finish_callback,
                                                      hide_yes_finish_callback, 
                                                      hide_no_finish_callback,
                                                      hide_container_quit_callback,
                                                      ])
    
    scene.AddInteraction(events=[yes_finish_input],callbacks=[ finish_test_callback,
                                                           save_point_graph_callback,
                                                           all_points_graph_callback ])
    
def create_words_UI(scene,my_world,phrase) :
    """
    This function is used to create a button for each word in the phrase
    """
    
# =============================================================================
#    MAIN PARAMETERS
# =============================================================================
    vertical_position = 0.3
    full_sentence = " ".join(phrase)
  
    words = full_sentence.split()
    number_words = len(words)
    list_numbers = []
    for i in range (number_words) : 
        list_numbers.append(i)
        
    first_horizontal_position= 0
           
    value =-1
# =============================================================================
#
#     BUTTONS ALL GOOD / ALL WRONG 
#
# =============================================================================
    
       
# =============================================================================
#         OBJECT CREATION    
# =============================================================================
    all_good_button = interface.all_good_button()
    all_wrong_button = interface.all_wrong_button()
    
# =============================================================================
#         INPUT CREATION
# =============================================================================
    all_good= PTVR.Data.Event.Button(id_object_button=all_good_button.id)
    all_wrong= PTVR.Data.Event.Button(id_object_button=all_wrong_button.id)
    
    scene.placeUI(all_good_button)
    scene.placeUI(all_wrong_button)
    
# =============================================================================
#         EVENT CREATION
# =============================================================================

    update_graph_callback = PTVR.Data.Callback.UpdateLastPoint()
 
    all_good_callbacks = []
    all_wrong_callbacks = []
    
# =============================================================================
#
#     END BUTTONS ALL GOOD / ALL WRONG     
#
# =============================================================================

   
    
    for y in range (len(phrase)) :
        
        ## Display the buttons on 3 lines, and centered in the container 
        word = phrase[y].split()
        
        if(len(word) == 5) :
            first_horizontal_position = -0.81
        elif(len(word) == 3) :
            first_horizontal_position = -0.65
        elif(len(word) == 4) :
            first_horizontal_position= -0.72
        elif(len(word) >= 6) :
            first_horizontal_position= -0.90
            
        horizontal_position= first_horizontal_position

        
        for j in range (len(word)) :
            
            value +=1    
            
# =============================================================================
#             OBJECT CREATION 
# =============================================================================

            good_button = interface.good_button(words[value],horizontal_position, vertical_position)
            wrong_button = interface.wrong_button(words[value],horizontal_position, vertical_position)
            
# =============================================================================
#             INPUT CREATION
# =============================================================================

            good_button_input = PTVR.Data.Event.Button(id_object_button=good_button.id)
            wrong_button_input = PTVR.Data.Event.Button(id_object_button=wrong_button.id)
    
# =============================================================================
#             callback CREATION
# =============================================================================
    
            green_visible_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=good_button.id, effect="activate")
            green_invisible_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=good_button.id, effect="deactivate")
        
            red_visible_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=wrong_button.id, effect="activate")
            red_invisible_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=wrong_button.id, effect="deactivate")
    
            save_text_callback = PTVR.Data.Callback.FillInResultsFileColumn(column_name=resultfile_misread_words,callback_name ="FillInResultsAdd_"+words[value], a_string = words[value]+" ", effect="activate") 
            unsave_text_callback = PTVR.Data.Callback.FillInResultsFileColumn(column_name=resultfile_misread_words, callback_name ="FillInResultsRemove_"+words[value],a_string = words[value]+" ", effect="deactivate") 
            
            all_good_callbacks.append(unsave_text_callback)
            all_good_callbacks.append(green_visible_callback)
            all_good_callbacks.append(red_invisible_callback)
            
            all_wrong_callbacks.append(save_text_callback)    
            all_wrong_callbacks.append(green_invisible_callback)
            all_wrong_callbacks.append(red_visible_callback)
            
# =============================================================================
#             OBJECT PLACEMENT
# =============================================================================
    
            scene.placeUI(good_button)
            scene.placeUI(wrong_button)
            
# =============================================================================
#             INTERACTIONS
# =============================================================================
    
            scene.AddInteraction(events=[good_button_input],
                         callbacks=[
                                save_text_callback,
                                green_invisible_callback,
                                red_visible_callback,
                                update_graph_callback])
            scene.AddInteraction(events=[wrong_button_input],
                         callbacks=[
                                unsave_text_callback,
                                red_invisible_callback,
                                green_visible_callback,
                                update_graph_callback
                                ])
            
            horizontal_position += 0.18
    
        vertical_position -= 0.15
        
    all_good_callbacks.append(update_graph_callback)
    all_wrong_callbacks.append(update_graph_callback)
    scene.AddInteraction(events=[all_good],callbacks=all_good_callbacks)
    scene.AddInteraction(events=[all_wrong],callbacks=all_wrong_callbacks)
    
    
def end_experiment(my_world,scene,smallest_logmar) : 
    
# =============================================================================
#     OBJET CREATION
# =============================================================================
    
    graph_UI= interface.create_graph_UI()
    
    popup_container = interface.pop_up_container()
    quit_button = interface.quit_button(is_active=True)
    yes_quit_button = interface.yes_button()
    no_quit_button = interface.no_button()
    
    validate_quit_text = interface.validate_end_quit_text()
    end_experiment_text = interface.end_experiment_text()
    
    informations_content_left = interface.informations_text_left()
    
    
    mnread_sentences = interface.number_sentences()
    mnread_sentences_description = interface.number_sentences_description()
    logmar = interface.logmar()
    logmar_description = interface.logmar_description()
    
    # timer = interface.timer()
    # timer_description = interface.timer_description()
    
    ## Distance head-text   
    # headset_screen_distance_content = interface.headset_screen_distance_content()
    # headset_screen_distance_description = interface.headset_screen_distance_description()
    
# =============================================================================
#     INPUT CREATION
# =============================================================================
    
    quit_input= PTVR.Data.Event.Button(id_object_button=quit_button.id)
    yes_quit_input= PTVR.Data.Event.Button(id_object_button=yes_quit_button.id)
    no_quit_input= PTVR.Data.Event.Button(id_object_button=no_quit_button.id)
    
    
    
# =============================================================================
#     callback CREATION
# =============================================================================
   
   
    ## Quit
    
    
    quit_callback = PTVR.Data.Callback.Quit()
    
    show_yes_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=yes_quit_button.id, effect="activate")
    hide_yes_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=yes_quit_button.id, effect="deactivate")
    
    show_no_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=no_quit_button.id, effect="activate")
    hide_no_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=no_quit_button.id, effect="deactivate")
    
    show_text_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=validate_quit_text.id, effect="activate")
    hide_text_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=validate_quit_text.id, effect="deactivate")
    
    show_container_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=popup_container.id, effect="activate")
    hide_container_quit_callback = PTVR.Data.Callback.ChangeObjectEnable(object_id=popup_container.id, effect="deactivate")  
    
    execute_post_treatment_script = PTVR.Data.Callback.ExecutePythonScript(script_path="Python_Scripts/The3DWorlds/MNRead/MNREAD_VR_Experiment",
                                                              script_name="result_file_post_treatment.py",
                                                              python_executable_complete_path = "C:/Program Files/Spyder/Python/python.exe",
                                                              )
    
# =============================================================================
#     OBJECT PLACEMENT
# =============================================================================
    
    # DO NOT change the order of placement, otherwise there might be positioning issues with the pop up container

    scene.placeUI(graph_UI)
    scene.placeUI(quit_button)
    scene.placeUI(end_experiment_text)
    scene.placeUI(informations_content_left)
    # scene.placeUI(timer)
    # scene.placeUI(headset_screen_distance_content)
    scene.placeUI(mnread_sentences)
    scene.placeUI(mnread_sentences_description)
    scene.placeUI(logmar)
    scene.placeUI(logmar_description)
    # scene.placeUI(timer_description)
    # scene.placeUI(headset_screen_distance_description)
    scene.placeUI(popup_container)
    scene.placeUI(validate_quit_text)
    scene.placeUI(yes_quit_button)
    scene.placeUI(no_quit_button)
    
    
# =============================================================================
#     INTERACTIONS
# =============================================================================
   
    scene.AddInteraction(events=[quit_input],callbacks=[
                                                  show_text_quit_callback, 
                                                  show_yes_quit_callback, 
                                                  show_no_quit_callback,
                                                  show_container_quit_callback,
                                                  ])
    
    scene.AddInteraction(events=[no_quit_input],callbacks=[hide_text_quit_callback,
                                                      hide_container_quit_callback,
                                                      hide_yes_quit_callback, 
                                                      hide_no_quit_callback])
    
    scene.AddInteraction(events=[yes_quit_input],callbacks=[quit_callback,
                                                         execute_post_treatment_script])
    
    
    
  