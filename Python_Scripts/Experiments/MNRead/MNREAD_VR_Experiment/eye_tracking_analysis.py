# -*- coding: utf-8 -*-
import pandas
import glob
import os
from pathlib import Path
from datetime import datetime
import math
from matplotlib import image
import numpy as np
import PTVR.VisualAngleCalculation as VAcalcul
import matplotlib.pyplot as plt
import cv2
from statistics import mean
from typing import Optional, Literal

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from matplotlib.text import Annotation
from matplotlib.transforms import Transform, Bbox
import matplotlib
import Algorithms.mnread_phrase_cutting as mnread
import PTVR.VisualAngleCalculation as VAcalcul

import result_file_post_treatment as filecleaning

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

text_description ="""
goal : Recreate gaze path on the mnread sentence 

how to use :  

predictions :   

Created on :  Thu Oct 20 14:51:14 2022
@author:jdelacha
"""

## Get the more recent csv file of the given folder

root_folder = str(Path(__file__).parents[4])
root_folder = root_folder.replace('\\','/' )
my_path = root_folder + "/PTVR_Operators/PTVR_Operators/Results/mnread_vr_experiment/"

list_of_cleaned_files = glob.glob(my_path + "*csv_cleaned.csv") # * means all if need specific format then *.csv
result_cleaned_file_name = max(list_of_cleaned_files, key=os.path.getctime)

path_to_png = result_cleaned_file_name


# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================



def text_with_autofit(
    ax,
    txt,
    xy,
    width, height,
    *,
    transform = None,
    ha= 'center',
    va = 'center',
    show_rect= False,
    **kwargs,
):
    if transform is None:
        transform = ax.transData

    #  Different alignments give different bottom left and top right anchors.
    x, y = xy
    xa0, xa1 = {
        'center': (x - width / 2, x + width / 2),
        'left': (x, x + width),
        'right': (x - width, x),
    }[ha]
    ya0, ya1 = {
        'center': (y - height / 2, y + height / 2),
        'bottom': (y, y + height),
        'top': (y - height, y),
    }[va]
    a0 = xa0, ya0
    a1 = xa1, ya1

    x0, y0 = transform.transform(a0)
    x1, y1 = transform.transform(a1)
    # rectangle region size to constrain the text in pixel
    rect_width = x1 - x0
    rect_height = y1 - y0

    fig: plt.Figure = ax.get_figure()
    dpi = fig.dpi
    rect_height_inch = rect_height / dpi
    # Initial fontsize according to the height of boxes
    fontsize = rect_height_inch * 72

    text: Annotation = ax.annotate(txt, xy, ha=ha, va=va, xycoords=transform, **kwargs)

    # Adjust the fontsize according to the box size.
    text.set_fontsize(fontsize)
    bbox: Bbox = text.get_window_extent(fig.canvas.get_renderer())
    adjusted_size = fontsize * rect_width / bbox.width
    text.set_fontsize(adjusted_size)

    if show_rect:
        rect = mpatches.Rectangle(a0, width, height, fill=False, ls='--')
        ax.add_patch(rect)

    return text







def plot_eyetracking_result(sentence,sentence_number,tangent_screen,screen_width,screen_height,logmar,distance):
    visual_angle_of_centered_object = VAcalcul.logmar_to_visual_angle_in_degrees(logmar)
    x_height_in_meters = VAcalcul.visual_angle_to_size_on_tangent_screen(visual_angle_of_centered_object = visual_angle_of_centered_object, 
                                                                         radial_distance = distance)
    textboxScale = np.array([(17.32 * x_height_in_meters), 1.0])
    
    list_cutted_phrases_from_mnread_phrase_cutting = mnread.mnread_phrase_cutting(sentence)
    phrase_with_back_to_line = str(list_cutted_phrases_from_mnread_phrase_cutting[0]) + "\n" + str(list_cutted_phrases_from_mnread_phrase_cutting[1]) + "\n" + str(list_cutted_phrases_from_mnread_phrase_cutting[2])
        
    
    tangent_screen = str(tangent_screen)
    tangent_screen = tangent_screen.replace('.', '')
    tangent_screen = tangent_screen[0:8] # pour contrecarer le formatage de l'id du screen. 
    list_of_files = glob.glob(my_path + "*"+str(tangent_screen)+"*.csv") # * means all if need specific format then *.csv

    result_file_name = max(list_of_files, key=os.path.getctime)
    
    data = pandas.read_csv(result_file_name, sep=";", encoding="utf_8")
    
    data_x_right = -data['x_right_eye_forward_direction_projection_on_screen']
    data_y_right = data['y_right_eye_forward_direction_projection_on_screen']
    
    data_x_left = -data['x_left_eye_forward_direction_projection_on_screen']
    data_y_left = data['y_left_eye_forward_direction_projection_on_screen']
    
    fig, ax = plt.subplots(1,1)
    ax.set_position([0,0,1,1])
    ## Axis setting
    #ax.set(xlim=(-(screen_width/2),(screen_width/2)), ylim = (-(screen_height/2), screen_height/2))# le graph fait la taille du screen
    ax.set(xlim=(-(textboxScale[0]/2),(textboxScale[0]/2)), ylim = (-(screen_height/2), screen_height/2))
    
    
    # In the box with the width of 0.4 and the height of 0.4 at (0.5, 0.5), add the text.
    #text_with_autofit(ax, phrase_with_back_to_line, (0.0, 0.0),screen_width, screen_height, show_rect=False)
    text_with_autofit(ax, phrase_with_back_to_line, (0.0, 0.0),textboxScale[0], screen_height, show_rect=False)
    #plt.text(0, 0, sentence, fontsize=10, ha='center',va='center', wrap=True,bbox=dict(facecolor='red', alpha=0.5))
    
    
    plt.plot(data_x_right,data_y_right,".", color='red',linestyle='solid') 
    plt.plot(data_x_left,data_y_left,".", color='blue',linestyle='solid') 
    plt.rcParams["font.family"] = "Times New Roman"
    
    plt.savefig(my_path+'gaze_path_sentence_'+str(tangent_screen)+'.png',dpi=600,bbox_inches='tight')
    #plt.savefig('gaze_path_sentence.png',format="png",dpi=600)
    print("fig saved")
    ## Show the entire figure
    plt.show()
    
def called_function(_result_cleaned_file_name):
    data_cleaned = pandas.read_csv(_result_cleaned_file_name, sep=";", encoding="utf_8")
    print(result_cleaned_file_name)
    
    for i in range(len(data_cleaned['sentence_displayed'])):
        plot_eyetracking_result(sentence =data_cleaned['sentence_displayed'][i],
                                sentence_number = data_cleaned['sentence_number'][i],
                                tangent_screen = data_cleaned['tangent_screen_id'][i],
                                screen_width = data_cleaned['tangent_screen_width_in_m'][i],
                                screen_height =data_cleaned['tangent_screen_height_in_m'][i],
                                logmar = data_cleaned['x_logmar'][i],
                                distance = data_cleaned['viewing_distance_in_m'][i])    

def main():
    data_cleaned = pandas.read_csv(result_cleaned_file_name, sep=";", encoding="utf_8")
    print(result_cleaned_file_name)
    
    for i in range(len(data_cleaned['sentence_displayed'])):
        plot_eyetracking_result(sentence =data_cleaned['sentence_displayed'][i],
                                sentence_number = data_cleaned['sentence_number'][i],
                                tangent_screen = data_cleaned['tangent_screen_id'][i],
                                screen_width = data_cleaned['tangent_screen_width_in_m'][i],
                                screen_height =data_cleaned['tangent_screen_height_in_m'][i],
                                logmar = data_cleaned['x_logmar'][i],
                                distance = data_cleaned['viewing_distance_in_m'][i])
    

  
if __name__ == "__main__":
            main()