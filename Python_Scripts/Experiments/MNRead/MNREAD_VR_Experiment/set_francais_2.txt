Mon chat aime bien manger mes plantes et renverser leur pot 
Un bel oiseau rouge a mangé près de ma fenêtre tôt ce matin 
On vous invite tous à la campagne pour ramasser des fraises 
Le petit chat de ma cousine attrape très souvent des mulots 
Le film d'animation présenté ce midi est vraiment admirable 
J'aime bien manger dans les restaurants japonais ou chinois
Trois de mes amies vont déménager cet été après les classes
Nous avons un très grand jardin rempli de belles jonquilles
Mes parents partent bientôt pour un très long voyage en mer
Les enfants de mon professeur de piano aiment la clarinette 
Ma mère vient tout juste de cueillir une pomme et une poire
Je suis bien content de déménager dans un très grand studio 
La belle demoiselle a une nouvelle robe rouge à pois jaunes
Les marins ont peur de se retrouver dans une grosse tempête
Le plus beau cheval dans une exposition reçoit une médaille
La vieille dame qui me connaissait bien dansait près de moi
Le garçon a dit que son ours en peluche aime bien le gâteau
La robe rose de ma petite soeur est trop pâle selon ma mère
Le jardinier de mon père est un bon ami du premier ministre 
