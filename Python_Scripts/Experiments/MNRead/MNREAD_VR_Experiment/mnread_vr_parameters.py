#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  6 12:48:45 2021

@author: johanna

Parameters used in mnread_vr_experiment """

import PTVR.Stimuli.Color as color
import Algorithms.mnread_phrases_frequences as phrases_frequences
import os
import pandas
# =============================================================================
# GENERAL TEST OPTIONS
# =============================================================================

## If sentences_testing_mode is True : 
    # the sentences have no decreasing size, 
    # the graph shows the sentence's index on x axis (instead of it's size in logmar)
sentences_testing_mode = False

## Add controllers input to use switch the sentences with the controllers
solo_testing_mode = False

## Use eyetracking to record eye gaze. 
record_eye_gaze = False

## Get the sentences a tag associated with each sentence 
    # MNREAD : standards mnread sentences 
    # GEN_GOOD : Alexandre's best generated sentences 
    # GEN_BAD : Alexandre's worst generated sentences 
    # With one sentence per line separated from the tag with a comma
use_tagged_sentences = False

take_picture = False 

## "True" if you want to simulate a scotomate, "False" if you don't
scotoma_visible = False

## "True"if you want a reticle
reticle_visible = False
# =============================================================================
# SUBJECT INFORMATIONS
# =============================================================================

subject_name = "Subject6"
height_of_subject_in_m = 1.18

# =============================================================================
# CHART INFORMATIONS
# =============================================================================

## Use the exact file name where your sentences are saved indicate path to access to the file from this script
## WARNING : the file must be in a .txt or .csv format with 1 sentence per line 
sentence_set = "set_francais_1.txt"
    
if (use_tagged_sentences):
    phrases_to_display = []
    tag = []
    abs_file_path = os.path.join(os.path.dirname(__file__) , sentence_set) 
    with open(abs_file_path, encoding='utf8') as sentences:
        phrases = sentences.readlines()
        #print(phrases)
    for i in range (len(phrases)):
        phrases[i] = phrases[i].rstrip("\n")
        
        couple = phrases[i].split(',')
        print(couple)
        phrases_to_display.append(couple[0])
        tag.append(couple[1])
        
    data = pandas.DataFrame(list(zip(phrases_to_display, tag)),
           columns =['sentences', 'tag'])
    
    data = data.sample(frac=1).reset_index(drop=True) ## Shuffle the rows 
    phrases_to_display = data['sentences']
    sentence_tag = data['tag']
else :
    abs_file_path = os.path.join(os.path.dirname(__file__) , sentence_set) #<-- absolute dir of the .txt
    with open(abs_file_path, encoding='utf8') as sentences:
        phrases_to_display = sentences.readlines()  
# =============================================================================
# SCOTOMA PARAMETERS
# =============================================================================

scotoma_image = "scotome.png" #"scotome.png" ,"S001_OS_2021-06-01_scotoma_scale3.png"
scotoma_x_angular_radius = 5
scotoma_y_angular_radius = 5

# =============================================================================
# RESULT FILE INFORMATIONS
# =============================================================================
# Indicate the column result
resultfile_sentence_display = "sentence_displayed"
resultfile_sentence_tag = "sentence_tag"
resultfile_visual_angle = "x_visual_angle_degrees"
resultfile_misread_words = "misread_words"
resultfile_logmar = "x_logmar"
resultfile_sentence_number = "sentence_number"
resultfile_viewing_distance = "viewing_distance_in_m"
resultfile_tangent_screen_id = "tangent_screen_id"
resultfile_tangent_screen_width_in_m = "tangent_screen_width_in_m"
resultfile_tangent_screen_height_in_m = "tangent_screen_height_in_m"

# =============================================================================
# EXPERIMENT INFORMATIONS
# =============================================================================

## Visual angle in degrees of the first phrase
visual_angle_of_centered_object = 0 

## Initial logmar
start_logmar = 1.3 ## Change to a value to specify the starting logmar. If None, it takes the visual angle mentionned above 

number_of_sentences_to_use = len(phrases_to_display) ## Number of sentences to display. Default is the complete .txt file passed to parameters above

## Distance in meters between the text and the camera
viewing_distance_in_m = 0.4

## If the distance is crossed, the operator has a feedback on the interface
limit_warning_area_radius = (25 * viewing_distance_in_m) / 100 # 25 % de 0.4m = 0.1 m = 10 cm

## decreasing size between each phrase /!\ in logMAR /!\
decreasing_size = 0.1 

## Font used to display the text.It is the mnread's standard font to use. Changing this parameter will occure imprecisions.
font_to_use = "TimesRoman"   

## Black letters on white background (BoW) or white letters on black background (WoB)
polarity = "BoW" 

# =============================================================================
# POLARITY TREATMENT
# =============================================================================
tangent_screen_color = color.RGBColor(r=1.0,g=1.0,b=1.0,a=1.0)
scene_background_color = color.RGBColor(r=0.8,g=0.8,b=0.8,a=1.0)

if (polarity == "WoB"):
    text_color = color.RGBColor(r=1.0,g=1.0,b=1.0,a=1.0) 
    ## Color of the screen behind the text
    tangent_screen_color = color.RGBColor(r=0.2,g=0.2,b=0.2,a=1.0)
    ## Color of the scene all around the patient 
    scene_background_color = color.RGBColor(r=0.1,g=0.1,b=0.1,a=1.0)
else : 
    text_color = color.RGBColor(r=0.0,g=0.0,b=0.0,a=1.0) 
# =============================================================================
#     
# 
# ## tangent screen invisible
# tangent_screen_color = color.RGBColor(r=1.0,g=1.0,b=1.0,a=1.0)
# scene_background_color = color.RGBColor(r=1.0,g=1.0,b=1.0,a=1.0)
# 
# if (polarity == "WoB"):
#     text_color = color.RGBColor(r=1.0,g=1.0,b=1.0,a=1.0) 
#     ## Color of the screen behind the text
#     tangent_screen_color = color.RGBColor(r=0.0,g=0.0,b=0.0,a=1.0)
#     ## Color of the scene all around the patient 
#     scene_background_color = color.RGBColor(r=0.0,g=0.0,b=0.0,a=1.0)
# else : 
#     text_color = color.RGBColor(r=0.0,g=0.0,b=0.0,a=1.0) 
# =============================================================================
