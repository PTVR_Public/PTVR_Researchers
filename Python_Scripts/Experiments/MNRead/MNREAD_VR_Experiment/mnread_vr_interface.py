# -*- coding: utf-8 -*-
import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import mnread_vr_parameters as parameters
import PTVR.Tools as tools
import Algorithms.mnread_phrase_cutting as mnread

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
 
text_description ="""
goal : This script contains all UI objects for the MNREAD vr experiment 

how to use :  Execute the script mnread_vr_experiment to launch the entire experiment. 

predictions : An interface allowing you to start the experiment should be first displayed. For each scene, an interface should be visible on the 
PC screen to interact with the experiment. The interface should be the same as showed in the tech note 

tech note : https://notes.inria.fr/qJt3btPVTnS5zXckWjzn2Q

@author:jdelacha
"""


light_gray_color = color.RGBColor(r=0.85,g=0.85,b=0.85,a=1.0)
light_blue_color = color.RGBColor(r=0.964,g=0.984,b=0.984,a=1.0)
background_color = light_blue_color
active_button_color = color.RGBColor(r=1.0,g=0.71,b=0.0,a=1.0)
inactive_button_color = color.RGBColor(r=0.5,g=0.5,b=0.5,a=1.0)
container_color = color.RGBColor(r=1,g=1,b=1,a=1.0)
titles_color = color.RGBColor(r=0.08,g=0.08,b=0.12,a=1.0)
content_color = color.RGBColor(r=0.08,g=0.08,b=0.12,a=1.0)
good_color = color.RGBColor(r=0.862,g=0.93,b=0.756,a=1.0)
wrong_color = color.RGBColor(r=1,g=0.66,b=0.647,a=1.0)
quit_ignore_color = color.RGBColor(r=0.658,g=0.9,b=0.956,a=1.0)
continue_stop_color = color.RGBColor(r=0.949,g=0.76,b=0.188,a=1.0)
graph_frame_color = color.RGBColor(r=1.0, g=1.0, b=1.0, a=1)
graph_text_color = color.RGBColor(r=0.08,g=0.08,b=0.12,a=1.0)


font_size_number = 50
font_size_titles = 40
font_size_content = 30
font_size_graph = 20
font_name = "LiberationSans"
button_height = 60.0
word_button_width_little = 70
word_button_width = 160
word_button_width_mid = 140
word_button_width_big = 280
side_containers_width = 350
side_containers_height = 450
content_width = side_containers_width - 20
resultfile_sentence_display = parameters.resultfile_sentence_display
resultfile_visual_angle = parameters.resultfile_visual_angle
resultfile_misread_words = parameters.resultfile_misread_words
resultfile_logmar = parameters.resultfile_logmar
result_file_sentence_number = parameters.resultfile_sentence_number
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

def create_background_image_UI(scene) :
    background_image =  PTVR.Stimuli.UIObjects.ImageUI(color=background_color,
                                                   image_type="background")
    scene.placeUI(background_image)
    
def create_graph_UI():
    """
    This function is used to create the template of the graph    
    """
    start_logmar = 0 
    if (parameters.start_logmar) :
        start_logmar = parameters.start_logmar
    else : 
        start_logmar = tools.visual_angle_in_degrees_to_logmar(parameters.visual_angle_of_centered_object)
        
    ## Standard graph
    
    graph = PTVR.Stimuli.UIObjects.GraphUI(position_in_current_CS=np.array([0.5,0,0]), # x= 0 is in center, x = 1 is on the right
                                        color=light_blue_color,
                                        dot_color = wrong_color,
                                        dot_connection_color = wrong_color,
                                        frame_color = graph_frame_color,
                                        text_color = graph_text_color,
                                        fontsize_in_points= font_size_graph,
                                        grid_color= quit_ignore_color,
                                        size = np.array([800, 400]),
                                        maximum_y_value=150, 
                                        minimum_x_value=-0.2,
                                        maximum_x_value = start_logmar + 0.1,
                                        graduations_x_range=0.1,
                                        graduations_y_range=40,
                                        x_is_invariant_culture=True,
                                        x_axis_name= "Print size (logMAR)",
                                        y_axis_name = "Reading speed (word/min)")
    graph.UseAppendStringAsValues(x_values_name = resultfile_logmar,
                                  y_values_name = resultfile_misread_words,
                                  operation_x="",
                                  operation_y="calculerReadingSpeed",
                                  )
    graph.CreateSecondXAxis(x_up_axis_name="Print size (degrees)", operation_type="conversionlogmardegrees")
    
    ## Graph when no decreasing size 
    if (parameters.sentences_testing_mode ) :
    
        graph = PTVR.Stimuli.UIObjects.GraphUI(#position_in_current_CS=np.array([500,25,0]),
                                           position_in_current_CS=np.array([0.5,0,0]),
                                           color=light_blue_color,
                                           dot_color = wrong_color,
                                           dot_connection_color = wrong_color,
                                           frame_color = graph_frame_color,
                                           text_color = graph_text_color,
                                           fontsize_in_points= font_size_graph,
                                           grid_color= quit_ignore_color,
                                           size = np.array([800, 400]),
                                           maximum_y_value=150, 
                                           minimum_x_value=0,
                                           maximum_x_value = parameters.number_of_sentences_to_use,
                                           graduations_x_range=parameters.number_of_sentences_to_use/10,
                                           graduations_y_range=40,
                                           x_is_invariant_culture=True,
                                           x_axis_name= "Sentence number" ,
                                           y_axis_name = "Reading speed")
        graph.UseAppendStringAsValues(x_values_name = result_file_sentence_number, y_values_name = resultfile_misread_words, operation_x="", operation_y="calculerReadingSpeed")

    return graph

# =============================================================================



#     BUTTONS 



# =============================================================================

def all_good_button() :
    all_good_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button="all_wellread",
                                                  position_in_current_CS=np.array([-0.54,-0.2,0.0]),
                                                  color = good_color,
                                                  height=button_height,
                                                  width = word_button_width)
    all_good_text = PTVR.Stimuli.UIObjects.TextUI(text="All good",font_name = font_name, fontsize_in_points = font_size_content)
    all_good_button.placeUIObject(all_good_text)
    return all_good_button

def all_wrong_button():
    all_wrong_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button="all_misread",
                                                   position_in_current_CS=np.array([-0.36,-0.2,0.0]),
                                                   color = wrong_color
                                                   , height=button_height,
                                                   width=word_button_width)
    all_wrong_text = PTVR.Stimuli.UIObjects.TextUI(text="All wrong",font_name = font_name,fontsize_in_points = font_size_content)
    all_wrong_button.placeUIObject(all_wrong_text)
    return all_wrong_button

def good_button(word, horizontal_position, vertical_position):
    good_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button=word+"_misread",
                                              position_in_current_CS=np.array([horizontal_position,vertical_position,0.0]),
                                              color = good_color,
                                              height=button_height,
                                              width = word_button_width)
    good_text = PTVR.Stimuli.UIObjects.TextUI(text=word,font_name = font_name,fontsize_in_points = font_size_content, vertical_alignment="Capline")
    good_button.placeUIObject(good_text)
    return good_button
    
def wrong_button(word, horizontal_position, vertical_position):
    wrong_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button=word+"_wellread",
                                               position_in_current_CS=np.array([horizontal_position,vertical_position,0.0]),
                                               color = wrong_color,
                                               height=button_height,
                                               width =word_button_width
                                               ,is_active=False)
    wrong_text = PTVR.Stimuli.UIObjects.TextUI(text=word,font_name = font_name,fontsize_in_points = font_size_content)
    wrong_button.placeUIObject(wrong_text)
    return wrong_button

def pop_up_container():
    container = PTVR.Stimuli.UIObjects.ImageUI(position_in_current_CS = np.array([0.0,0.0,0.0]),
                                                        color = container_color,
                                                        width = 1000,
                                                        height = 1000,
                                                        image_type="background",
                                                        is_active=False)
    return container

def start_button():
    start_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button="StartButton",
                                               position_in_current_CS=np.array([-0.08,-0.5,0.0]), 
                                               color = continue_stop_color, 
                                               height=button_height
                                               )
    start_button_text = PTVR.Stimuli.UIObjects.TextUI(text="Start",font_name = font_name,fontsize_in_points=font_size_content)
    start_button.placeUIObject(start_button_text)
    return start_button

def quit_button(is_active=False):
    quit_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button="QuitButton",
                                               position_in_current_CS=np.array([-0.8,-0.5,0.0]), 
                                               color = quit_ignore_color, 
                                               height=button_height,
                                               is_active = is_active
                                               )
    quit_button_text = PTVR.Stimuli.UIObjects.TextUI(text="Quit",font_name = font_name,fontsize_in_points=font_size_content)
    quit_button.placeUIObject(quit_button_text)
    
    return quit_button

def quit_button_inactive():
    quit_button_inactive = PTVR.Stimuli.UIObjects.ButtonUI(name_button="QuitButtonInactive",
                                               position_in_current_CS=np.array([-0.8,-0.5,0.0]), 
                                               color = light_gray_color, 
                                               height=button_height
                                               )
    quit_button_inactive_text = PTVR.Stimuli.UIObjects.TextUI(text="Quit",font_name = font_name,fontsize_in_points=font_size_content)
    quit_button_inactive.placeUIObject(quit_button_inactive_text)
    
    return quit_button_inactive

def ignore_button():
    ignore_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button="IgnoreButton",
                                               position_in_current_CS=np.array([-0.6,-0.5,0.0]), 
                                               color =quit_ignore_color, 
                                               height=button_height,
                                               is_active = False
                                               ,
                                               width = 180
                                               )
    ignore_button_text = PTVR.Stimuli.UIObjects.TextUI(text="Ignore Trial",font_name = font_name,fontsize_in_points=font_size_content)
    ignore_button.placeUIObject(ignore_button_text)
    
    return ignore_button

def ignore_button_inactive():
    ignore_button_inactive = PTVR.Stimuli.UIObjects.ButtonUI(name_button="IgnoreButtonInactive",
                                               position_in_current_CS=np.array([-0.6,-0.5,0.0]), 
                                               color =light_gray_color, 
                                               height=button_height,
                                               width = 180
                                               )
    ignore_button_inactive_text = PTVR.Stimuli.UIObjects.TextUI(text="Ignore Trial",font_name = font_name,fontsize_in_points=font_size_content)
    ignore_button_inactive.placeUIObject(ignore_button_inactive_text)
    
    return ignore_button_inactive

def finish_test_button():
    finish_test_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button="FinishTestButton",
                                               position_in_current_CS=np.array([-0.4,-0.5,0.0]), 
                                               color =quit_ignore_color, 
                                               height=button_height,
                                               is_active = False
                                               ,
                                               width = 180
                                               )
    finish_test_button_text = PTVR.Stimuli.UIObjects.TextUI(text="Finish Test",font_name = font_name,fontsize_in_points=font_size_content)
    finish_test_button.placeUIObject(finish_test_button_text)
    
    return finish_test_button

def finish_test_button_inactive():
    finish_test_button_inactive = PTVR.Stimuli.UIObjects.ButtonUI(name_button="FinishTestButtonInactive",
                                               position_in_current_CS=np.array([-0.4,-0.5,0.0]), 
                                               color =light_gray_color, 
                                               height=button_height,
                                               width = 180
                                               )
    finish_test_button_inactive_text = PTVR.Stimuli.UIObjects.TextUI(text="Finish Test",font_name = font_name,fontsize_in_points=font_size_content)
    finish_test_button_inactive.placeUIObject(finish_test_button_inactive_text)
    
    return finish_test_button_inactive

def yes_button():
    yes_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button="YesButton",
                                               position_in_current_CS=np.array([0.1,0.0,0.0]), 
                                               color =quit_ignore_color, 
                                               height=button_height,
                                               width=60.0,
                                               is_active=False
                                               )
    yes_button_text = PTVR.Stimuli.UIObjects.TextUI(text="Yes",font_name = font_name,fontsize_in_points=font_size_content)
    yes_button.placeUIObject(yes_button_text)
    
    return yes_button

def no_button():
    no_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button="NoButton",
                                               position_in_current_CS=np.array([-0.1,0.0,0.0]), 
                                               color =quit_ignore_color, 
                                               height=button_height,
                                               width=60.0,
                                               is_active=False
                                               )
    no_button_text = PTVR.Stimuli.UIObjects.TextUI(text="No",font_name = font_name,fontsize_in_points=font_size_content)
    no_button.placeUIObject(no_button_text)
    
    return no_button

def stop_button():
    stop_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button="Stop",
                                               position_in_current_CS=np.array([-0.08,-0.5,0.0]), 
                                               color = continue_stop_color, 
                                               height=button_height
                                               )
    stop_button_text = PTVR.Stimuli.UIObjects.TextUI(text="Stop",font_name = font_name,fontsize_in_points=font_size_content)
    stop_button.placeUIObject(stop_button_text)
    
    return stop_button

def continue_button():
    continue_button = PTVR.Stimuli.UIObjects.ButtonUI(name_button="Continue",
                                               position_in_current_CS=np.array([-0.08,-0.5,0.0]), 
                                               color = continue_stop_color, 
                                               height=button_height,
                                               is_active=False
                                               )
    continue_button_text = PTVR.Stimuli.UIObjects.TextUI(text="Continue",font_name = font_name,fontsize_in_points=font_size_content)
    continue_button.placeUIObject(continue_button_text)
    
    return continue_button


# =============================================================================



#     TEXTS 



# =============================================================================

def quit_button_description():
    
    quit_button_description = PTVR.Stimuli.UIObjects.TextUI(text="Quit the experiment",
                                                    fontsize_in_points = font_size_content/2,
                                                    font_name = font_name,
                                                    is_italic = True,
                                                    horizontal_alignment = "Left",
                                                    vertical_alignment= "Top",
                                                    textbox_width=160.0,
                                                    position_in_current_CS=np.array([-0.8,-0.62,0.0]),
                                                    color=content_color)
    return quit_button_description

def finish_button_description():
    
    quit_button_description = PTVR.Stimuli.UIObjects.TextUI(text="End the experiment and go to the end screen",
                                                    fontsize_in_points = font_size_content/2,
                                                    font_name = font_name,
                                                    is_italic = True,
                                                    horizontal_alignment = "Left",
                                                    vertical_alignment= "Top",
                                                    textbox_width=160.0,
                                                    position_in_current_CS=np.array([-0.4,-0.62,0.0]),
                                                    color=content_color)
    return quit_button_description

def ignore_button_description():
    
    ignore_button_description = PTVR.Stimuli.UIObjects.TextUI(text="Ignore the result of this trial",
                                                    fontsize_in_points = font_size_content/2,
                                                    font_name = font_name,
                                                    is_italic = True,
                                                    horizontal_alignment = "Left",
                                                    vertical_alignment= "Top",
                                                    textbox_width=160.0,
                                                    position_in_current_CS=np.array([-0.6,-0.62,0.0]),
                                                    color=content_color)
    return ignore_button_description

def start_button_description():
    
    start_button_description = PTVR.Stimuli.UIObjects.TextUI(text="Start the experiment",
                                                    fontsize_in_points = font_size_content/2,
                                                    font_name = font_name,
                                                    is_italic = True,
                                                    horizontal_alignment = "Left",
                                                    vertical_alignment= "Top",
                                                    textbox_width=160.0,
                                                    position_in_current_CS=np.array([-0.08,-0.62,0.0]),
                                                    color=content_color)
    return start_button_description

def stop_button_description():
    
    stop_button_description = PTVR.Stimuli.UIObjects.TextUI(text="Stop the timer once the patient has finished reading",
                                                    fontsize_in_points = font_size_content/2,
                                                    font_name = font_name,
                                                    is_italic = True,
                                                    horizontal_alignment = "Left",
                                                    vertical_alignment= "Top",
                                                    textbox_width=160.0,
                                                    position_in_current_CS=np.array([-0.08,-0.62,0.0]),
                                                    color=content_color)
    return stop_button_description

def continue_button_description():
    
    continue_button_description = PTVR.Stimuli.UIObjects.TextUI(text="Launch the next sentence",
                                                            is_active = False,
                                                    fontsize_in_points = font_size_content/2,
                                                    font_name = font_name,
                                                    is_italic = True,
                                                    horizontal_alignment = "Left",
                                                    vertical_alignment= "Top",
                                                    textbox_width=160.0,
                                                    position_in_current_CS=np.array([-0.08,-0.62,0.0]),
                                                    color=content_color)
    return continue_button_description

def validate_ignore_text():
    
    text_ignore_printed = PTVR.Stimuli.UIObjects.TextUI(text="Mark this trial as non-valid ? \n The current sentence will be indicated as ignored in the result file and the test will not be displayed on the graph",
                                                    is_active = False,
                                                    fontsize_in_points = font_size_content,
                                                    font_name = font_name,
                                                    textbox_width= 700,
                                                    position_in_current_CS=np.array([0.0,0.2,0.0]),
                                                    color=content_color)
    return text_ignore_printed

def same_size_description():
    
    same_size_description = PTVR.Stimuli.UIObjects.TextUI(text="Do you want to display the next sentence at the same size as the current sentence? \n Clicking yes will automatically launch the next sentence. ",
                                                    is_active = False,
                                                    fontsize_in_points = font_size_content,
                                                    font_name = font_name,
                                                    textbox_width= 700,
                                                    position_in_current_CS=np.array([0.0,0.2,0.0]),
                                                    color=content_color)
    return same_size_description

def validate_quit_text():
    text_quit_printed = PTVR.Stimuli.UIObjects.TextUI(text="Do you want to quit the experiment ? \n The application will close and you will not have access to the end of experience statistics",
                                                    is_active = False,
                                                    position_in_current_CS=np.array([0.0,0.2,0.0]),
                                                    fontsize_in_points = font_size_content,
                                                    font_name = font_name,
                                                    textbox_width= 700,
                                                    color=content_color)
  
    return text_quit_printed

def validate_end_quit_text():
    validate_end_quit_text = PTVR.Stimuli.UIObjects.TextUI(text="Do you want to close the application ?",
                                                    is_active = False,
                                                    position_in_current_CS=np.array([0.0,0.2,0.0]),
                                                    fontsize_in_points = font_size_content,
                                                    font_name = font_name,
                                                    textbox_width= 700,
                                                    color=content_color)
  
    return validate_end_quit_text

def validate_finish_text():
    validate_finish_text = PTVR.Stimuli.UIObjects.TextUI(text="Do you want to end the experiment ? \n You will be redirected to the end screen",
                                                    is_active = False,
                                                    position_in_current_CS=np.array([0.0,0.2,0.0]),
                                                    fontsize_in_points = font_size_content,
                                                    font_name = font_name,
                                                    textbox_width= 700,
                                                    color=content_color)
  
    return validate_finish_text
    
def informations_text_left():
    
    informations_content_update = PTVR.Stimuli.UIObjects.TextUI(text="Subject : "+ parameters.subject_name + "\n" +
                                                        "Sentence set : " + str(parameters.sentence_set) + "\n"+
                                                         "Distance : " + str(parameters.viewing_distance_in_m) + " meters"
                                                        ,
                                                        horizontal_alignment = "Left",
                                                        position_in_current_CS=np.array([0.7,0.68,0.0]),
                                                        textbox_width=content_width,
                                                        fontsize_in_points=font_size_content,
                                                        font_name = font_name,
                                                        color=content_color)
   
    return informations_content_update

def number_sentences(counter_sentences="-"):
    number_sentences = PTVR.Stimuli.UIObjects.TextUI(text=str(counter_sentences) +" / "+ str(parameters.number_of_sentences_to_use) 
                                                        ,
                                                        horizontal_alignment = "Left",
                                                        position_in_current_CS=np.array([-0.7,0.72,0.0]),
                                                        textbox_width=content_width,
                                                        fontsize_in_points=font_size_number,
                                                        font_name = font_name,
                                                        color=content_color)
   
    return number_sentences
def number_sentences_description():
    number_sentences_description = PTVR.Stimuli.UIObjects.TextUI(text=
                                                         "MNREAD sentence "
                                                        ,
                                                        horizontal_alignment = "Left",
                                                        vertical_alignment= "Top",
                                                        position_in_current_CS=np.array([-0.7,0.63,0.0]),
                                                        textbox_width=content_width,
                                                        fontsize_in_points=font_size_content,
                                                        font_name = font_name,
                                                        color=content_color)
   
    return number_sentences_description


def logmar(logmar="-"):
    logmar = PTVR.Stimuli.UIObjects.TextUI(text=str(logmar) + " logmar",
                                            horizontal_alignment = "Left",
                                            position_in_current_CS=np.array([-0.4,0.72,0.0]),
                                            textbox_width=content_width,
                                            fontsize_in_points=font_size_number,
                                            font_name = font_name,
                                            color=content_color)
   
    return logmar
def logmar_description( degrees="-"):
    logmar_description = PTVR.Stimuli.UIObjects.TextUI(text= str(degrees) + " degrees",
                                                        horizontal_alignment = "Left",
                                                        vertical_alignment= "Top",
                                                        position_in_current_CS=np.array([-0.4,0.63,0.0]),
                                                        textbox_width=content_width,
                                                        fontsize_in_points=font_size_content,
                                                        font_name = font_name,
                                                        color=content_color)
   
    return logmar_description



# def timer(timer_calculator = "-"):
#     timer = PTVR.Stimuli.UIObjects.TextUI(text= timer_calculator + " s.",                                        
#                                         horizontal_alignment = "Left",
#                                         position_in_current_CS=np.array([-0.1,0.72,0.0]),
#                                         textbox_width=content_width,
#                                         fontsize_in_points=font_size_number,
#                                         font_name = font_name,
#                                         color=content_color)
#     return timer
# def timer_description():
#     timer_description = PTVR.Stimuli.UIObjects.TextUI(text= "Reading time",                                        
#                                         horizontal_alignment = "Left",
#                                         vertical_alignment= "Top",
#                                         position_in_current_CS=np.array([-0.1,0.63,0.0]),
#                                         textbox_width=content_width,
#                                         fontsize_in_points=font_size_content,
#                                         font_name = font_name,
#                                         color=content_color)
#     return timer_description

# def headset_screen_distance_content(distance_head_text = "-"):
#     headset_screen_distance_content = PTVR.Stimuli.UIObjects.TextUI(text= distance_head_text + " m.",                                        
#                                         horizontal_alignment = "Left",
#                                         position_in_current_CS=np.array([0.2,0.72,0.0]),
#                                         textbox_width=content_width,
#                                         fontsize_in_points=font_size_number,
#                                         font_name = font_name,
#                                         color=content_color)
#     return headset_screen_distance_content

# def headset_screen_distance_description():
#     headset_screen_distance_description = PTVR.Stimuli.UIObjects.TextUI(text="Headset-text distance",                                        
#                                         horizontal_alignment = "Left",
#                                         vertical_alignment= "Top",
#                                         position_in_current_CS=np.array([0.2,0.63,0.0]),
#                                         textbox_width=content_width,
#                                         fontsize_in_points=font_size_content,
#                                         font_name = font_name,
#                                         color=content_color)
#     return headset_screen_distance_description

def distance_warning (is_active):
    distance_warning = PTVR.Stimuli.UIObjects.TextUI(text= "Distance not respected ",                                        
                                        horizontal_alignment = "Left",
                                        color = color.RGBColor(r=1.0),
                                        position_in_current_CS=np.array([0.2,0.57,0.0]),
                                        textbox_width=content_width,
                                        fontsize_in_points=font_size_content,
                                        font_name = font_name,
                                        is_active = is_active)
    return distance_warning

def end_experiment_text():
    text_end_experiment = PTVR.Stimuli.UIObjects.TextUI(text="End of the experiment",
                                                    position_in_current_CS=np.array([-0.43,0.0,0.0]),
                                                    textbox_width=content_width,
                                                    fontsize_in_points=font_size_content,
                                                    font_name = font_name,
                                                    color=content_color)
    return text_end_experiment

