# -*- coding: utf-8 -*-
import pandas
import glob
import os
from pathlib import Path
from datetime import datetime
import math
import numpy as np
#import eye_tracking_analysis as eyetracking
#import gaze_path_visualization as visualize
#import mnread_vr_parameters as parameters

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

text_description ="""
goal : Post treatment of the result file obtained after an experiment

how to use :  Call this script at the end of your PTVR experiment 

predictions :   

Created on :  Thu Oct  6 11:50:47 2022
@author:jdelacha
"""

## Get the more recent csv file of the given folder
root_folder = str(Path(__file__).parents[4])
root_folder = root_folder.replace('\\','/' )
my_path = root_folder + "/PTVR_Operators/Results/mnread_vr_experiment/"
# TO DO : ajouter qu'on ne traite que le _Main_ !!
list_of_files = glob.glob(my_path + "*Main*.csv") # * means all if need specific format then *.csv
#list_of_files = glob.glob(my_path + "*Subject6*.csv")
result_file_name = max(list_of_files, key=os.path.getctime)


columns_to_keep = ['callback_name','callback_timestamp','sentence_number',
                   'sentence_displayed','x_logmar','viewing_distance_in_m',
                   'misread_words','sentence_tag','tangent_screen_id',
                   'tangent_screen_width_in_m','tangent_screen_height_in_m'] #

final_columns = ['sentence_number','sentence_tag','sentence_displayed',
                 'tangent_screen_id','tangent_screen_width_in_m','tangent_screen_height_in_m',
                 'x_logmar','viewing_distance_in_m','misread_words','errors_number', 'is_outside_area_radius','trial_ignored']

path_to_new_file = result_file_name+ "_cleaned.csv"

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

def correct_date_format(string):
    string = datetime.strptime(string, "%Y-%m-%d-%H-%M-%S.%f")
    return string

def count_words(string): 
    if(isinstance(string, float)):
        return 0
    else :
        return len(str(string).split())

def main():
    result_file = pandas.read_csv(result_file_name, sep=";", usecols=columns_to_keep, encoding="utf_8")
    result_file = result_file.loc[(result_file['callback_name']=='EndCurrentScene') | (result_file['callback_name']=='AddNewPoint') | (result_file['callback_name']=='IgnoreTrial') | (result_file['callback_name']=='OutsideAreaLimit')]
    
    ## Management of the callback_timestamp column
    
    result_file['callback_timestamp'] = result_file['callback_timestamp'].apply(correct_date_format)
    
    result_file['reading_time'] = result_file['callback_timestamp'].diff().dt.total_seconds()
    
    result_file['is_outside_area_radius'] = "No"
    result_file['trial_ignored'] = "No"
    
    print(result_file)
    
    ## Delete results related to an Ignored trial 
    # if 'IgnoreTrial' in result_file.values:
    #     value = result_file[result_file['event_name']=='IgnoreTrial']['sentence_number'].item()
    #     result_file = result_file[result_file.sentence_number != value]
        
    if 'IgnoreTrial' in result_file.values:
        values = result_file[result_file['callback_name']=='IgnoreTrial']['sentence_number'].tolist()
        for value in values :
            result_file.loc[result_file.sentence_number == value, 'trial_ignored'] = "Yes"
    
    ## Print "Yes" in the column "is_outside_area_radius" if the patient has moved once or more outside the area limit 
    ## specified in the mnread_vr_parameters.py
    if 'OutsideAreaLimit' in result_file.values: 
        numbers = result_file[result_file['callback_name']=='OutsideAreaLimit']['sentence_number'].tolist()
        for number in numbers :
            result_file.loc[result_file.sentence_number == number, 'is_outside_area_radius'] = "Yes"
                                           
    
    ## add column errors_number
    result_file['errors_number'] = result_file['misread_words'].apply(count_words)
    
    
    ## keep only interested rows for reading_speed (ce sont celles avec AddNewPoint)
    
    temp_frame = pandas.DataFrame(result_file)  
    temp_frame = temp_frame.loc[(temp_frame['callback_name']=='AddNewPoint')]
    temp_frame = temp_frame[['reading_time','sentence_number']]
    
        
    ## On garde seulement les lignes qui nous interessent pour le nombre d'erreur (ce sont celles avec EndCurrentScene)
    
    result_file = result_file.loc[(result_file['callback_name']=='EndCurrentScene')]
    result_file.drop(index=result_file.index[0], axis=0, inplace=True)
    result_file.drop(columns=["reading_time"], axis=1, inplace=True)
    
    
    ## On crée le tableau final sur lequel on se basera pour extraire les paramètres MNREAD 
    result_file = result_file[final_columns]
    final_frame = pandas.merge(result_file,temp_frame,on=["sentence_number"])
    print(final_frame)
    
    final_frame.to_csv(path_to_new_file, sep=";", index = False, header=True)
    print("New csv written")
# =============================================================================
#     eyetracking.called_function(_result_cleaned_file_name = path_to_new_file)
# # =============================================================================
# #     if (parameters.record_eye_gaze):
# #         visualize.called_function(_result_cleaned_file_name = path_to_new_file)
# # =============================================================================
#     
#   
# =============================================================================
if __name__ == "__main__":
            main()