#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 21 09:11:56 2021

@author: johanna

This demo recreate the MNRead test by allowing the researcher to display several sentences at different sizes. When he pushes the button, a new phrase is displayed with its size decreased of X logMAR (this value is specified in mnread_vr_parameters).

"""

# from sys import exit
import PTVR.SystemUtils

import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

import PTVR.mnread_phrase_cutting as mnread
import PTVR.VisualAngleCalculation as VAcalcul
import Parameters.mnread_vr_parameters as parameters
#import mnread_vr_interface as interface
import PTVR.Data.Input as input
import PTVR.Data.Event as event

heightOfStaticPOV_m = 0
username = "User"

informations_text_size =  parameters.visual_angle_of_centered_object

def create_scene(phrase_to_display, visual_angle_of_centered_object, scene ,my_world, phrase_to_write= "",tag_duration = "",logmar_value = 0, is_mnread_sentence = False) :
    """ 
    
    This function is used to create each scene displayed 
    
    """
    
    
    ## The tangent screen is the screen behind the text
    tangent_screen = PTVR.Stimuli.Objects.FlatScreen(position_current_CS=(0,0,(parameters.viewing_distance_in_m)),
                                                   color=parameters.tangent_screen_color,
                                                   size_in_meters=np.array([parameters.tangent_screen_width,parameters.tangent_screen_height]))
    tangent_screen.SetTangentRotation()
    
    if(is_mnread_sentence) : 
        ## Text displayed on the screen    
        text = PTVR.Stimuli.Objects.Text(text=phrase_to_display,font_name=parameters.font_to_use,color= parameters.text_color,
                                       visual_angle_of_centered_x_height_deg = visual_angle_of_centered_object 
                                       )
        text.SetMnreadFormat()      
        scene.append_string(name ="Sentence displayed", value =phrase_to_write)
        scene.append_string(name ="Reading duration" , value ="start")
        scene.append_string(name ="Visual angle",value= str(visual_angle_of_centered_object))
        scene.append_string(name ="Mots mal lus",value= "")
        scene.append_string(name="LogMAR", value=str(logmar_value))
        
        ## Placement of the elements in the scene 
        tangent_screen.place_object_on_tangent_screen(text,eccentricity=0,halfMeridian=0,radialDistance=0)
       
        scene.place(tangent_screen)
        scene.place(text)
    
        ## If MNRead sentence, add in the result file the informations about the displayed phrase 
        print(phrase_to_write)
        
        
    else : 
        ## Text displayed on the screen    
        text = PTVR.Stimuli.Objects.Text(text=phrase_to_display,font_name=parameters.font_to_use,color= parameters.text_color
                                       ,visual_angle_of_centered_x_height_deg = visual_angle_of_centered_object)
        tangent_screen.place_object_on_tangent_screen(text,eccentricity=0,halfMeridian=0,radialDistance=0)
        ## Placement of the elements in the scene     
        scene.place(tangent_screen)
        scene.place(text)

    ## Add the scene to the experiment 
    InteractionTest(scene)
    my_world.add_scene(scene)

    



    

def sentences_sequence_mnread(my_world) :
    """ 
    
    When the researcher push a button on the controller, a MNRead phrase is displayed. Each new phrase has a decreasing size 
    
    """
    
    
    ## Variable with the current visual angle. Used to decrease the size of each sentence
    current_visual_angle = parameters.visual_angle_of_centered_object
    if (parameters.start_logmar) :
        print("There is a logmar")
        current_visual_angle = VAcalcul.logmar_to_visual_angle_in_degrees(parameters.start_logmar)
    
    for i in range (len(parameters.phrases_to_display)):
        
        print("Current VA: "+str(current_visual_angle))
   
        phrase_to_display = parameters.phrases_to_display[i].rstrip("\n")
   
        logmar_value = VAcalcul.visual_angle_in_degrees_to_logmar(current_visual_angle) ## conversion degrees to logmar 
        print("Current LogMar: "+str(logmar_value))
        ## Transformation of the phrase in order to have 3 lines following the MNRead's standards 
        list_cutted_phrases_from_mnread_phrase_cutting = mnread.mnread_phrase_cutting(phrase_to_display)
        phrase_with_back_to_line = list_cutted_phrases_from_mnread_phrase_cutting[0] + "\n" + list_cutted_phrases_from_mnread_phrase_cutting[1] + "\n" + list_cutted_phrases_from_mnread_phrase_cutting[2]
        
        ## Creation of the scene with the displayed phrase 
        t1 = PTVR.Stimuli.Scenes.VisualScene(experiment=exp)

        create_scene(phrase_to_display = phrase_with_back_to_line,
                     visual_angle_of_centered_object = current_visual_angle,
                     scene = t1,
                     my_world = my_world,
                     phrase_to_write = phrase_to_display,
                     tag_duration = "start",
                     logmar_value = logmar_value,
                     is_mnread_sentence=True)
        
        #interface.create_MainButtonsUI(t1,my_world, list_cutted_phrases_from_mnread_phrase_cutting, logmar_value)
        #interface.create_UIWords(t1,my_world,list_cutted_phrases_from_mnread_phrase_cutting)
        """
        ## Creation of the rest scene  T2
        t2 = PTVR.Stimuli.Scenes.VisualScene(experiment=exp)
        myEvent2= event.EndCurrentScene (t2)
        t2.AddInteraction(inputs= [myInputPress], events =[myEvent2])
        
        create_scene("Pause. \n Remettez-vous en position de confort.",parameters.visual_angle_of_centered_object,t2,my_world,phrase_to_display,"end")
        """
        ## Management of the current visual angle in order to decrease the size of the next phrase 
        current_visual_angle = VAcalcul.logmar_to_visual_angle_in_degrees(logmar_value - parameters.decreasing_size)
    
    
    ## Creation of the end scene 
    #interface.end_experiment(exp)
    
def InteractionTest(scene):
    myEventEnd= event.EndCurrentScene(scene)
    myInputTest = input.Keyboard(valid_responses=['y', 'n'])
    scene.AddInteraction(inputs=[myInputTest],events=[myEventEnd])
    
def main():
    
    # Set-up the experiment file
    POVatCalibration = PTVR.Stimuli.Objects.VirtualPoint(position_current_CS= np.array([0, heightOfStaticPOV_m, 0]))
    my_world = visual.The3DWorld()
    my_world.set_coordinate_system_from_a_virtual_point (POVatCalibration)

    #interface.start_experiment(exp)
    sentences_sequence_mnread(exp)

    # Write The3DWorld to file
    my_world.write()
    print("The experiment has been written.")
    
    


if __name__ == "__main__":
        main()

        PTVR.SystemUtils.LaunchThe3DWorld(username) 