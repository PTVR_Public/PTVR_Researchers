# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 10:13:12 2022

@author: jdelacha

Test MRS and CPS calculation algorithm 
"""


# from sys import exit
import PTVR.SystemUtils

import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

import PTVR.Data.Input as input
import PTVR.Data.Event as event
import PTVR.Stimuli.Color as color



x_column = 'x_values' 
y_column = 'y_values'

button_height = 60


def create_GraphUI():
   
    graph = PTVR.Stimuli.World.GraphUI(position_current_CS=np.array([0,300,0]),
                                       size = np.array([800, 400]),
                                       minimum_y_value = 0,
                                       maximum_y_value=120, 
                                       minimum_x_value = 0,
                                       maximum_x_value=1.3, 
                                       graduations_x_range=0.1,
                                       graduations_y_range=20,
                                       x_axis_name= "X",
                                       y_axis_name = "Y")
    graph.UseAppendStringAsValues(x_values_name = x_column, y_values_name = y_column, operation_x="", operation_y="")
    return graph


def setInteractions(scene) :
    graphUI = create_GraphUI() 
   
    showDot_button = PTVR.Stimuli.World.ButtonUI(nameButton="showDot",
                                               position_current_CS=np.array([-0.25,0.0,0.0]),
                                               color = color.RGBColor(r=1.0,g=0.71,b=0.0,a=1.0),
                                               height = button_height
                                               )
    showDot_button_text = PTVR.Stimuli.UIObjects.TextUI(text="Show dot")
    showDot_button.placeUIObject(showDot_button_text)
    
    
    nextScene_button = PTVR.Stimuli.World.ButtonUI(nameButton="nextScene",
                                               position_current_CS=np.array([0.0,-0.5,0.0]), 
                                               color = color.RGBColor(r=1.0,g=0.71,b=0.0,a=1.0),
                                               height = button_height
                                               )
    nextScene_button_text = PTVR.Stimuli.UIObjects.TextUI(text="Next Scene")
    nextScene_button.placeUIObject(nextScene_button_text)
    
    ##########################################################
    
    ########              INPUT CREATION             #########
    
    ##########################################################
    
    
    showDot= PTVR.Data.Input.Button(idObjectButton=showDot_button.id)
    
    
    continuer = PTVR.Data.Input.Button(idObjectButton=nextScene_button.id)
    
    
    ##########################################################
    
    ########              EVENT CREATION             #########
    
    ##########################################################
    
    
    
    eventAddNewPoint = event.AddNewPoint( graph_id=graphUI.id, is_invariant_culture=True)
    
    
    ##########################################################
    
    ########              PLACEMENT             #########
    
    ##########################################################
    
    
    scene.placeUI(showDot_button)
    
    scene.placeUI(nextScene_button)
    
    scene.placeUI(graphUI)
    
    
    ##########################################################
    
    ########              INTERACTIONS             #########
    
    ##########################################################
    
    scene.AddInteraction(inputs=[showDot],events=[ eventAddNewPoint,event.EndCurrentScene() ])
    
def lastscene(scene):
    graphUI = create_GraphUI() 
    stats = PTVR.Stimuli.UIObjects.TextUI(text="ACC : \n"+
                                        "RA : \n"+
                                        "CPS :\n"+
                                        "MRS :\n"
                                        ,
                                        horizontal_alignment = "Left",
                                        position_current_CS=np.array([0.9,-0.4,0.0]))
    result_button = PTVR.Stimuli.World.ButtonUI(nameButton="result_button",
                                               position_current_CS=np.array([0.0,0.0,0.0]),
                                               color = color.RGBColor(r=1.0,g=0.71,b=0.0,a=1.0),
                                               height = button_height
                                               )
    result_button_text = PTVR.Stimuli.UIObjects.TextUI(text="Result")
    result_button.placeUIObject(result_button_text)
    result= PTVR.Data.Input.Button(idObjectButton=result_button.id)
    
    eventMRSCPS = event.MRSandCPSCalcul()
    eventShowGraphPoints = event.ShowGraphPoints(graph_id=graphUI.id)
    eventDisplayStats = event.DisplayMNREADStats(text_object_id=stats.id)
    
    scene.placeUI(graphUI)
    scene.placeUI(result_button)
    scene.placeUI(stats)
    
    scene.AddInteraction(inputs=[result],events=[ eventShowGraphPoints,eventMRSCPS, eventDisplayStats])
    
    
def main():

    # Set-up the experiment file
    my_world = visual.The3DWorld() 
    #my_world.translate_coordinate_system ( )
    
    misreadWords = [[1.2,110],[1.1,108],[1.0,100],[0.9,111],[0.8,105],[0.7,75],[0.6,65],[0.5,30],[0.4,9],[0.3,0]]
    
    for i in range(0,len(misreadWords)) :
        scene = PTVR.Stimuli.Scenes.VisualScene() 
        my_world.add_scene(scene)
        
        scene.append_string(name =x_column, value = str(misreadWords[i][0]))
        scene.append_string(name =y_column,value= str(misreadWords[i][1]))
        setInteractions(scene)
    
    endscene = PTVR.Stimuli.Scenes.VisualScene() 
    lastscene(endscene)
    my_world.add_scene(endscene)
   
    # Write The3DWorld to file
    my_world.write()
print("The experiment has been written.")




if __name__ == "__main__":
    main()

    PTVR.SystemUtils.LaunchThe3DWorld(username="demo") 