# -*- coding: utf-8 -*-
"""


@author: jdelacha

Prevision : the text_with_mnreadformat_notworking should create a warning 

"""


# import os

# from sys import exit
import PTVR.SystemUtils
# import sys
import numpy as np
from math import pi
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Input as input
import PTVR.Stimuli.Color as color
import PTVR.PointingCursor.PointingCursor
import PTVR.VisualAngleCalculation as calcAng

username = ""

radialDistance_m = 3  # in meters

heightOfStaticPOV_m = 0

angularXHeight = 4

flatScreenHeight_deg = 20  # in degrees
flatScreenHeight_m = calcAng.visual_angle_to_size_on_tangent_screen(
    visual_angle_of_centered_object=flatScreenHeight_deg, radial_distance=radialDistance_m)  # in meters

flatScreenWidth_deg = 20  # in degrees
flatScreenWidth_m = calcAng.visual_angle_to_size_on_tangent_screen(
    visual_angle_of_centered_object=flatScreenWidth_deg, radial_distance=radialDistance_m)  # in meters


def main():

    # Creation of the visual scene
    POVatCalibration = PTVR.Stimuli.Objects.VirtualPoint(
        position_current_CS=np.array([0, heightOfStaticPOV_m, 0]))
    my_world = visual.The3DWorld()
    my_world.set_coordinate_system_from_a_virtual_point(
        POVatCalibration)  # new origin is now at POVatCalibration

    visScene = PTVR.Stimuli.Scenes.VisualScene()
    my_input = PTVR.Data.Input.Keyboard(valid_responses=['space'])
    my_event = PTVR.Data.Event.EndScene(id_next_scene=(visScene.id+1))

    visScene.AddInteraction(inputs=[my_input], events=[my_event])

    my_world.add_scene(visScene)

    # Creation of the tangent screen

    myTangentScreen = PTVR.Stimuli.Objects.FlatScreen(size_in_meters=np.array([flatScreenWidth_m, flatScreenHeight_m]),
                                                      getData=False,
                                                      color=color.RGBColor(r=0.8, g=0.8, b=0.0, a=1))
    myTangentScreen.SetTangentRotation()
    myTangentScreen.set_perimetric_coordinates(eccentricity=0.0, halfMeridian=0.0,
                                               radialDistance=radialDistance_m)

    # Creation of the text

    text_with_mnreadformat_working = PTVR.Stimuli.Objects.Text()
    text_with_mnreadformat_working.SetText(
        "Ma mère aime bien écouter le chant des cigales en plein été")
    text_with_mnreadformat_working.SetXheightAngularSize(angularXHeight)
    text_with_mnreadformat_working.SetMnreadFormat()

    text_with_mnreadformat_notworking = PTVR.Stimuli.Objects.Text(
        fontsize_in_points=4)
    text_with_mnreadformat_working.SetText(
        "Ma mère aime bien écouter le chant des cigales en plein été")
    text_with_mnreadformat_notworking.SetMnreadFormat()

    myTangentScreen.place_object_on_tangent_screen(text_with_mnreadformat_working, eccentricity=0,
                                                   halfMeridian=90, radialDistance=0.0)
    myTangentScreen.place_object_on_tangent_screen(text_with_mnreadformat_notworking, eccentricity=6,
                                                   halfMeridian=90, radialDistance=0.0)
    visScene.place(myTangentScreen)
    visScene.place(text_with_mnreadformat_working)
    visScene.place(text_with_mnreadformat_notworking)

    # Write The3DWorld to file
    my_world.write()
    print("The The3DWorld has been written.")


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld(username)
