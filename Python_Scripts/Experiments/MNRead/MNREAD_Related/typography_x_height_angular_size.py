#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 22 11:19:49 2021

@author: johanna
"""
"""

Demo which displays one letter and one cube. Both of them have the same size. Demo used to proove internal consistency in Unity.
Expectation: the cube should be slightly bigger than the text (due to perspective)
 
"""

import os
import subprocess

# from sys import exit
import PTVR.SystemUtils

import sys
from math import *
import math

import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Input as input
import PTVR.Stimuli.Color as color
import PTVR.VisualAngleCalculation as VAcalcul
import Parameters.single_letter_demo_parameters as parameters


username = "DemoName"
detailsThe3DWorld = "Demo which displays one letter and one cube. Both of them have the same size. Demo used to proove internal consistency in Unity. "
    
def single_letter_demo(exp) :
    """ Display one letter on front of the camera. A cube is set relative to the camera. """
   
    ## Creation of the scene
    t1 = PTVR.Stimuli.Scenes.VisualScene(experiment=exp)
    
    ## Elements in scene are relative to coordinates    
    coordinates = PTVR.Stimuli.World.EmptyVisualObject(
        position_current_CS= (0,0,0), 
        rotation = (0,0,0)
            
        )
    coordinates.isRelativeToHead = True
    t1.place(coordinates)
    
    ## Create and place the tangent screen which is behind the text
    physical_size = VAcalcul.visual_angle_to_size_on_tangent_screen(parameters.visual_angle_of_centered_object, parameters.viewing_distance_in_m)
    tangent_screen = PTVR.Stimuli.World.FlatScreen(position_current_CS=(0,0,(parameters.viewing_distance_in_m+0.1)),color=parameters.tangent_screen_color,size_in_meters=np.array([physical_size + physical_size*0.3,physical_size*2]))
    tangent_screen.SetTangentRotation()
    tangent_screen.set_parent(coordinates)
    t1.place(tangent_screen)
    
    ## Create and place the text
    text = PTVR.Stimuli.World.Text(text=parameters.letter_to_display,font_name=parameters.font_to_use,color= parameters.text_color,position_current_CS= (0,0, parameters.viewing_distance_in_m),visual_angle_of_centered_x_height_deg = parameters.visual_angle_of_centered_object)
    ## Create the cube isHeadPOVcontingent
    cube = PTVR.Stimuli.World.Cube(position_current_CS= text.position, size_in_meters = VAcalcul.visual_angle_to_size_on_tangent_screen(parameters.visual_angle_of_centered_object, parameters.viewing_distance_in_m) )
    cube.isHeadPOVcontingent = True
  
    
    text.set_parent(coordinates)
    t1.place(text)
    t1.place(cube) 
     
    return t1


def main():
    
    # Set-up the experiment file
    my_world = visual.The3DWorld(detailsThe3DWorld = detailsExperiment)

    
    # Add this scene to the experiment
    my_world.add_scene(single_letter_demo(my_world.)

    # Write The3DWorld to file
    my_world.write()
    print("The experiment has been written.")


if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld( username)