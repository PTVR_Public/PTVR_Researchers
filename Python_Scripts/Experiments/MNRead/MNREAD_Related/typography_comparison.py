# -*- coding: utf-8 -*-
"""

Typography Demo to visualize the difference of fontsize between differents fonts when they have the same physical height.

"""

import os
import subprocess

# from sys import exit
import PTVR.SystemUtils

import sys
from math import *
import math

import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Input as input
import PTVR.Stimuli.Color as color
import PTVR.VisualAngleCalculation as VAcalcul
import Parameters.typography_comparison_parameters as parameters

"""
    This function displays 1 letter et 1 tangent screen per font to compare. The tangents screens are placed around the camera. It also displays two guidelines to show the baseline and 
    the x-height of the letter.

    """

username = "DemoName"
detailsThe3DWorld="Typography Demo to visualize the difference of fontsize between differents fonts when they have the same physical height."

heightOfStaticPOV_m = 0

def typography_comparison(exp) :
    
    
    if (len(parameters.fonts_to_use) == 0):
        print("-----------\n Error : the demo is designed to display at least 1 font. Please write a font's name in fonts_to_use \n-----------")
        return -1;
    if (len(parameters.letter_to_display)!=1):
        print("-----------\n Error : the demo is designed to display only one letter per tangent screen. Please give only one letter in letter_to_display \n-----------")
        return -1
    
    
    
    ## Infornations about the tangent screen
    physical_size = VAcalcul.visual_angle_to_size_on_tangent_screen(parameters.visual_angle_of_centered_object, parameters.viewing_distance_in_m)
    ## Placement of tangents screens depending of the number of fonts
    if (len(parameters.fonts_to_use)%2)==0 :
        # Il y a un nombre pair de fonts
        coordinates_increment_for_rotation = float(  0 - ( (parameters.tangent_screen_height *100)*(len(parameters.fonts_to_use)/2) ) + ( parameters.tangent_screen_height *100/2 ) )
    else :
        # Il y a un nombre impair de fonts
        coordinates_increment_for_rotation = float(  0 - ( (parameters.tangent_screen_height *100)*(floor(len(parameters.fonts_to_use)/2)) )  ) 
             
    ## Creation of the scene    
    t1 = PTVR.Stimuli.Scenes.VisualScene(experiment=exp)
    
    ## Title and informations about the scene 
    title = "Demo Typographie" 
    subtitle = "Les lettres suivantes ont un angle visuel de " + str(parameters.visual_angle_of_centered_object) + " degrés, sont à " + str(parameters.viewing_distance_in_m) + " m de la caméra et ont une taille physique de " + str(VAcalcul.visual_angle_to_size_on_tangent_screen(parameters.visual_angle_of_centered_object,parameters.viewing_distance_in_m)) + " m."
    
    title_text = PTVR.Stimuli.World.Text(text=title,position_current_CS= ( 0,1, parameters.viewing_distance_in_m),visual_angle_of_centered_x_height_deg = parameters.visual_angle_of_centered_object)
    subtitle_text = PTVR.Stimuli.World.Text(text=subtitle,position_current_CS= ( 0,0.75, parameters.viewing_distance_in_m),visual_angle_of_centered_x_height_deg = parameters.visual_angle_of_centered_object)
    ## Starting position when placing the tangent screens
    start_coordinates_rotation = coordinates_increment_for_rotation
    
    
    for i in range (0, len(parameters.fonts_to_use)):
        
        if coordinates_increment_for_rotation >= 360+start_coordinates_rotation : # addition because start_coordinates_rotation is negative
            # If we have more than 15 fonts, warning message
            print("-----------\n There is no place left to display another letter (15 maximum). Excess letters have been ignored \n-----------")
            break
        ## Elements in scene are relative to coordinates  
        coordinates = PTVR.Stimuli.World.EmptyVisualObject(
            position_current_CS= (0,0,0), 
            rotation = (0,0,0)
            
            )
        coordinates.isRelativeToHead = True
        
        ## Creation of the card behind the letter 
        tangent_screen = PTVR.Stimuli.World.FlatScreen(position_current_CS=(0,0,(parameters.viewing_distance_in_m+0.1)),color=color.RGBColor(r=(163/255),g=(210/255),b=(202/255),a=1),size_in_meters=np.array([physical_size + physical_size*0.3,physical_size*2]))
        tangent_screen.set_parent(coordinates)
        
        text = PTVR.Stimuli.World.Text(text=parameters.letter_to_display,vertical_alignment = parameters.vertical_alignment, font_name=parameters.fonts_to_use[i],position_current_CS= (0,0, parameters.viewing_distance_in_m),visual_angle_of_centered_x_height_deg = parameters.visual_angle_of_centered_object)
        text.SetUnderlineInformations() ## Informations about the letter displayed (font name and font size) under the letter
        text.set_parent(coordinates)
        
        ## Creation of lines used as a marker 
        xheight_line = PTVR.Stimuli.World.Line(position_current_CS=(0,0,0), start_point= (-(parameters.tangent_screen_width*10/2),physical_size/2,0), end_point = (parameters.tangent_screen_width*10/2,physical_size/2,0), line_color=color.RGBColor(r=(240/255),g=(89/255),b=(69/255),a=(255/255)))
        base_line = PTVR.Stimuli.World.Line(position_current_CS=(0,0,0), start_point= (-(parameters.tangent_screen_width*10/2),-(physical_size/2),0), end_point = (parameters.tangent_screen_width*10/2,-(physical_size/2),0), line_color=color.RGBColor(r=(240/255),g=(89/255),b=(69/255),a=(255/255)))
       
        
       
       
        
        ## Ajustment of the placement
        coordinates.rotation = (0,coordinates_increment_for_rotation,0) 
        coordinates_increment_for_rotation += parameters.tangent_screen_height *100
      
        t1.place(coordinates)
        t1.place(tangent_screen)
        t1.place(text)
        xheight_line.set_parent(text)
        base_line.set_parent(text)
        t1.place(xheight_line)
        t1.place(base_line)
        
        
    my_world.add_scene(t1)
 

def main():
    

    # Set-up the experiment file
    my_world = visual.The3DWorld()
    typography_comparison(exp)

    # Write The3DWorld to file
    my_world.write()
    print("The experiment has been written.")


if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld(username)
