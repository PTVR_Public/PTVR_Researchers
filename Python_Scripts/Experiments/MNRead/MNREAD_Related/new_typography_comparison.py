
# -*- coding: utf-8 -*-
"""
Goal : visualize the difference of fontsize between differents fonts when they have the same physical height.
Expectations : the letter "x" is displayed around the user with different fonts

@author: jdelacha
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

import PTVR.VisualAngleCalculation as VAcalcul
import Parameters.typography_comparison_parameters as parameters

current_coordinate_system = np.array([0.0, 1.0, 0.0])


def create_scene(my_world, scene):

    legend = PTVR.Stimuli.World.Text(text="Les lettres suivantes ont un angle visuel de " +
                                     str(parameters.visual_angle_of_centered_object) +
                                     " degrés, sont à " + str(parameters.viewing_distance_in_m) +
                                     " m de la caméra et ont une taille physique de " +
                                     str(VAcalcul.visual_angle_to_size_on_tangent_screen(parameters.visual_angle_of_centered_object, parameters.viewing_distance_in_m)) +

                                     " m.",
                                     fontsize_in_postscript_points=1)
    legend.set_orientation(is_facing_head_pov=False, is_facing_origin=True)
    legend.set_perimetric_coordinates(
        eccentricity=20, halfMeridian=90, radialDistance=parameters.viewing_distance_in_m)
    scene.place(legend)
    ecc = [10, 10, 30, 30, 50, 50, 70, 70, 90, 90,
           110, 110, 130, 130, 150, 150, 170, 170, 190]
    halfm = 0
    for y in range(len(ecc)):
        if (y % 2 == 0):
            halfm = 0
        else:
            halfm = 180
        if (y < len(parameters.fonts_to_use)):
            myTangentScreen = PTVR.Stimuli.World.TangentScreen(size_in_meters=np.array([parameters.tangent_screen_width, parameters.tangent_screen_height]),
                                                               color=color.RGBColor(r=0.1, g=0.7, b=0.6, a=1))
            myTangentScreen.set_perimetric_coordinates(eccentricity=ecc[y], halfMeridian=halfm,
                                                       radialDistance=parameters.viewing_distance_in_m)
            text = PTVR.Stimuli.World.Text(text=parameters.letter_to_display, font_name=parameters.fonts_to_use[y],
                                           horizontal_alignment="Center", vertical_alignment="Middle", visual_angle_of_centered_x_height_deg=parameters.visual_angle_of_centered_object)
            description = PTVR.Stimuli.World.Text(text=parameters.fonts_to_use[y],
                                                  horizontal_alignment="Center",
                                                  vertical_alignment="Middle",
                                                  visual_angle_of_centered_x_height_deg=0.1*parameters.visual_angle_of_centered_object)
            print(parameters.fonts_to_use[y])
            myTangentScreen._place_object_on_tangent_screen(
                text, eccentricity=0, halfMeridian=0)
            myTangentScreen._place_object_on_tangent_screen(
                description, eccentricity=10, halfMeridian=-90)

            scene.place(myTangentScreen)
            scene.place(text)
            scene.place(description)
        else:
            break

    my_world.add_scene(scene)


def main():

    # Creation of the visual scene
    my_world = visual.The3DWorld()
    my_world.translate_coordinate_system_along_global(
        current_coordinate_system)

    scene = PTVR.Stimuli.Scenes.VisualScene(
        background_color=color.RGBColor(r=0.8, g=0.8, b=0.8, a=1.0))

    create_scene(my_world=my_world, scene=scene)

    # Write The3DWorld to file
    my_world.write()
    print("The The3DWorld has been written.")


if __name__ == "__main__":
    main()
    # PTVR.SystemUtils.LaunchThe3DWorld()
