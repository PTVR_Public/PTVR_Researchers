# -*- coding: utf-8 -*-
"""
Created on Fri Jun 25 09:37:44 2021

@author: Johanna Delachambre

This demo recreate the MNRead Carboard, by displaying 3 sentences at 3 different sizes.

"""

import os
import subprocess

# from sys import exit
import PTVR.SystemUtils

import sys
from math import *
import math
import random

import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Input as input
import PTVR.Stimuli.Color as color
import PTVR.mnread_phrase_cutting as mnread
import PTVR.VisualAngleCalculation as VAcalcul
import Parameters.cardboard_mnread_parameters as parameters


username = "DemoName"
detailsThe3DWorld = "This demo recreate the MNRead Carboard, by displaying 3 sentences at 3 different sizes."
heightOfStaticPOV_m = 0


def cardboard_mnread(exp):
    """Displays 3 phrases like in the board version of MNRead """
    # Creation of the scene
    POVatCalibration = PTVR.Stimuli.Objects.VirtualPoint(
        position_current_CS=np.array([0, heightOfStaticPOV_m, 0]))
    my_world.set_coordinate_system_from_a_virtual_point(POVatCalibration)
    t1 = PTVR.Stimuli.Scenes.VisualScene(experiment=exp)

    # The tangent screen is the screen behind the text
    tangent_screen = PTVR.Stimuli.Objects.FlatScreen(position_current_CS=(0, 0, (parameters.viewing_distance_in_m+0.1)),
                                                     color=parameters.tangent_screen_color, size_in_meters=np.array([parameters.tangent_screen_width, parameters.tangent_screen_height]))
    tangent_screen.SetTangentRotation()
    tangent_screen.set_perimetric_coordinates(eccentricity=0.0, halfMeridian=0.0,
                                              radialDistance=parameters.viewing_distance_in_m)
    t1.place(tangent_screen)

    # Variable with the current visual angle. Used to decrease the size of each sentence
    current_visual_angle_of_centered_object = parameters.visual_angle_of_centered_object
    # Position of the text in the y-axis
    y_text_position_current_CS = (VAcalcul.visual_angle_to_size_on_tangent_screen(
        current_visual_angle_of_centered_object, parameters.viewing_distance_in_m) * 10)

    for i in range(len(parameters.phrases_to_display)):

        # Transformation of the phrase in order to have 3 lines following the MNRead's standards
        list_cutted_phrases_from_mnread_phrase_cutting = mnread.mnread_phrase_cutting(
            parameters.phrases_to_display[i])
        phrase_with_back_to_line = list_cutted_phrases_from_mnread_phrase_cutting[0] + "\n" + \
            list_cutted_phrases_from_mnread_phrase_cutting[1] + \
            "\n" + list_cutted_phrases_from_mnread_phrase_cutting[2]

        text = PTVR.Stimuli.Objects.Text(text=phrase_with_back_to_line, horizontal_alignment=parameters.text_alignment, font_name=parameters.font_to_use, color=parameters.text_color, position_current_CS=(
            0, y_text_position, parameters.viewing_distance_in_m), visual_angle_of_centered_x_height_deg=current_visual_angle_of_centered_object)
        text.SetMnreadFormat()

        tangent_screen.place_object_on_tangent_screen(text)

        # Management of the current visual angle in order to decrease the size of the next phrase and management of the position in y-axis
        visual_angle_of_centered_object_in_logmar = VAcalcul.visual_angle_in_degrees_to_logmar(
            current_visual_angle_of_centered_object)
        current_visual_angle_of_centered_object = VAcalcul.logmar_to_visual_angle_in_degrees(
            visual_angle_of_centered_object_in_logmar - parameters.decreasing_size)
        y_text_position -= VAcalcul.visual_angle_to_size_on_tangent_screen(
            current_visual_angle_of_centered_object, parameters.viewing_distance_in_m)*10

        t1.place(text)
    my_world.add_scene(t1)


def main():

    # Set-up the experiment file
    my_world = visual.The3DWorld(detailsThe3DWorld=detailsExperiment)

    cardboard_mnread(exp)

    # Write The3DWorld to file
    my_world.write()
    print("The experiment has been written.")


if __name__ == "__main__":
    main()

    PTVR.SystemUtils.LaunchThe3DWorld(username)
