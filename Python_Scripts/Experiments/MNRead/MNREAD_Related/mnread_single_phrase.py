#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 22 10:13:21 2021

@author: johanna

This demo displays one phrase following MNRead's standards.

"""

import os
import subprocess

# from sys import exit
import PTVR.SystemUtils

import sys
from math import *
import math
import random

import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Input as input
import PTVR.Stimuli.Color as color
import PTVR.mnread_phrase_cutting as mnread
import PTVR.VisualAngleCalculation as VAcalcul
import Parameters.single_phrase_mnread_parameters as parameters


username = "DemoName"
detailsThe3DWorld= "This demo displays one phrase following MNRead's standards."



def single_phrase_mnread(exp) :
    """ Function used to display one phrase following MNRead's standards. """
    
    
    ## Transformation of the phrase in order to have 3 lines following the MNRead's standards 
    list_cutted_phrases_from_mnread_phrase_cutting = mnread.mnread_phrase_cutting(parameters.phrase_to_display)
    phrase_with_back_to_line = list_cutted_phrases_from_mnread_phrase_cutting[0] + "\n" + list_cutted_phrases_from_mnread_phrase_cutting[1] + "\n" + list_cutted_phrases_from_mnread_phrase_cutting[2] 
    
    ## Creation of the scene
    t1 = PTVR.Stimuli.Scenes.VisualScene(experiment=exp)
    ## Elements in scene are relative to coordinates   
    coordinates = PTVR.Stimuli.Objects.EmptyVisualObject(
        position_current_CS= (0,0,0), 
        rotation = (0,0,0)
            
        )
    coordinates.isRelativeToHead = True
    ## The tangent screen is the screen behind the text    
    tangent_screen = PTVR.Stimuli.Objects.FlatScreen(position_current_CS=(0,0,(parameters.viewing_distance_in_m+0.1)),color=parameters.tangent_screen_color,size_in_meters=np.array([parameters.tangent_screen_width,parameters.tangent_screen_height]))
    tangent_screen.SetTangentRotation()
    tangent_screen.set_parent(coordinates)
        
    text = PTVR.Stimuli.Objects.Text(text=phrase_with_back_to_line,font_name=parameters.font_to_use,color= parameters.text_color,position_current_CS= (0,0, parameters.viewing_distance_in_m),visual_angle_of_centered_x_height_deg = parameters.visual_angle_of_centered_object)
   # text.SetMnreadFormat() 
    text.set_parent(coordinates)
         
    t1.place(coordinates)
    t1.place(tangent_screen)
    t1.place(text)
                
        
    return t1

def main():
    

    # Set-up the experiment file
    my_world = visual.The3DWorld(detailsThe3DWorld = detailsExperiment)


    
    
    # Add this scene to the experiment

    my_world.add_scene(single_phrase_mnread(my_world.)

    # Write The3DWorld to file
    my_world.write()
    print("The experiment has been written.")


if __name__ == "__main__":
        main()
        
        PTVR.SystemUtils.LaunchThe3DWorld(username)