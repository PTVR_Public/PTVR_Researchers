# -*- coding: utf-8 -*-
"""

Création et test du cochage de mots mal lus
@author: jdelacha
"""



# from sys import exit
import PTVR.SystemUtils
#import sys
import numpy as np
from math import pi
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Input as input
import PTVR.Stimuli.Color as color
import PTVR.Data.Event as event

import PTVR.VisualAngleCalculation as VAcalcul
import Parameters.mnread_vr_parameters as parameters
import PTVR.mnread_phrase_cutting as mnread

username="Johanna"
heightOfStaticPOV_m = 0

phrases = mnread.mnread_phrase_cutting("Ma mère aime bien écouter le chant des cigales en plein été ")
mnread_phrase = "Ma mère aime bien écouter le chant des cigales en plein été "
words = mnread_phrase.split()

def main():
    
    ## Creation of the visual scene 
    POVatCalibration = PTVR.Stimuli.Objects.VirtualPoint (position_current_CS= np.array([0, heightOfStaticPOV_m, 0]))
    my_world = visual.The3DWorld()
    my_world.set_coordinate_system_from_a_virtual_point (POVatCalibration) # new origin is now at POVatCalibration 
    
    scene  = PTVR.Stimuli.Scenes.VisualScene (experiment=my_world,background_color=color.RGBColor(0.5,0.5,0.5,1)) 
    my_world.add_scene(scene)
    
    
    
    
    vertical_position_current_CS= 0.5
    horizontal_position_current_CS= -0.5
    
    virtualPoint = PTVR.Stimuli.Objects.VirtualPoint()
    virtualPoint.set_cartesian_coordinates(x=-0.2,y=-1)
    
    all_good = input.ButtonUI(position_current_CS=virtualPoint, color = color.RGBColor(r=0.0,g=1.0,b=0.0,a=1.0))
    all_good.placeUIElement( PTVR.Stimuli.UIObjects.TextUI(text="All good"))
    
    virtualPoint = PTVR.Stimuli.Objects.VirtualPoint()
    virtualPoint.set_cartesian_coordinates(x=0.2,y=-1)
    
    all_wrong = input.ButtonUI(position_current_CS=virtualPoint, color = color.RGBColor(r=1.0,g=0.0,b=0.0,a=1.0))
    all_wrong.placeUIElement( PTVR.Stimuli.UIObjects.TextUI(text="All wrong"))
    
    
    
    
    
    ##############################
    ##############################
    ### Test Event PrintResult ### 
    
    
    
    
    
    scene.append_string("Sentence displayed", mnread_phrase)
    scene.append_string("Reading duration" , "")
    scene.append_string("Visual angle", 2)
    scene.append_string("LogMAR", 1)
    
    virtualPoint = PTVR.Stimuli.Objects.VirtualPoint()
    virtualPoint.set_cartesian_coordinates(x=0,y=-1)
    eventPrint = event.PrintResult(effect = "activate")
    
    printButton = input.ButtonUI(position_current_CS=virtualPoint, color = color.RGBColor(r=0.0,g=0.0,b=1.0,a=1.0))
    printButton.placeUIElement( PTVR.Stimuli.UIObjects.TextUI(text="PRINT"))
    
    scene.AddInteraction(inputs=[printButton],events=[eventPrint])
    
    
    
    
    
    
    ##############################
    ##############################
    ##############################
    
    
    
    
    
    
   
    ## Input creation
    for y in range (len(phrases)) :
        
        words = phrases[y].split()
        
        
        for i in range (len(words)) :

            virtualPoint1 = PTVR.Stimuli.Objects.VirtualPoint()
            virtualPoint1.set_cartesian_coordinates(x=horizontal_position,y=vertical_position)
            ######### Inputs  #########
            
            UI_BUTTON_GOOD = input.ButtonUI(position_current_CS=virtualPoint1, color = color.RGBColor(r=0.0,g=1.0,b=0.0,a=1.0))
            UI_BUTTON_GOOD.placeUIElement( PTVR.Stimuli.UIObjects.TextUI(text=words[i]))
            
            UI_BUTTON_WRONG = input.ButtonUI(position_current_CS=virtualPoint1, color = color.RGBColor(r=1.0,g=0.0,b=0.0,a=1.0), is_active = False)
            UI_BUTTON_WRONG.placeUIElement( PTVR.Stimuli.UIObjects.TextUI(text=words[i]))
            
            ######### Events  #########
            
            event_isgood = PTVR.Data.Event.SaveText(word= words[i], effect="deactivate")
            event_iswrong = PTVR.Data.Event.SaveText(word= words[i], effect="activate")
            
            green_visible = PTVR.Data.Event.ChangeObjectEnable(object_id=UI_BUTTON_GOOD.id, effect="activate")
            green_invisible = PTVR.Data.Event.ChangeObjectEnable(object_id=UI_BUTTON_GOOD.id, effect="deactivate")
            
            red_visible = PTVR.Data.Event.ChangeObjectEnable(object_id=UI_BUTTON_WRONG.id, effect="activate")
            red_invisible = PTVR.Data.Event.ChangeObjectEnable(object_id=UI_BUTTON_WRONG.id, effect="deactivate")
    
            ######### Interactions  #########
            
            scene.AddInteraction(inputs=[UI_BUTTON_WRONG, all_good],events=[event_isgood,
                                                                             red_invisible,
                                                                             green_visible])
            scene.AddInteraction(inputs=[UI_BUTTON_GOOD, all_wrong],events=[event_iswrong,
                                                                           green_invisible,
                                                                           red_visible])
            
            
            
            horizontal_position += 0.3
            
        vertical_position -= 0.5 
        horizontal_position_current_CS= -0.5
     
    # Write The3DWorld to file

    my_world.write()
    print("The The3DWorld has been written.")


if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld(username)


