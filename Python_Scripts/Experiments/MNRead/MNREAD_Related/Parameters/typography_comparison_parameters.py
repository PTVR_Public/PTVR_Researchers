#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  6 16:07:24 2021

@author: johanna

parameters used in typography_comparison.py
"""

import PTVR.Stimuli.Color as color
import PTVR.VisualAngleCalculation as VAcalcul

## Text 


## Letter to display. The demo is made to display only one letter
letter_to_display = "x"

## Visual angle in degrees of the text
visual_angle_of_centered_object = 11.63 

## Distance in meters between the text and the camera
viewing_distance_in_m = 5

## Font used to display the text. You need to choose at least 1 font, and maximum 10 fonts
fonts_to_use = ["Arial","CourierNew","TimesNewRoman","georgia","impact","segoepr","TimesRoman","verdana","trebuc","lucon"]

vertical_alignment = "Middle" ## horizontal alignment of the text. Changing this parameter could occur errors 


## Tangent screen 

#Set the width and height of the screen. The value are set to be relatives to the text's size and to give the screen a card format.
tangent_screen_height = VAcalcul.visual_angle_to_size_on_tangent_screen(visual_angle_of_centered_object,viewing_distance_in_m) *2
tangent_screen_width = VAcalcul.visual_angle_to_size_on_tangent_screen(visual_angle_of_centered_object,viewing_distance_in_m)

## Demo


## Set to true if you want to have a title in the scene with recapitulate the variables used (visual angle, distance,....) and informations below the letter such as its font and the size in point 
is_typography_demo = True 

