#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  6 16:25:47 2021

@author: johanna
"""
""" Parameters used in single_letter_demo """

import PTVR.Stimuli.Color as color






## Text

## Letter to display. The demo is made to display only one letter
letter_to_display = "x"

## Visual angle in degrees of the text
visual_angle_of_centered_object = 11.63 

## Distance in meters between the text and the camera
viewing_distance_in_m = 5

## Font used to display the text. You need to choose at least 1 font, and maximum 15 fonts
font_to_use = "Arial"

## Color of the text
text_color = color.RGBColor(r=0.0,g=0.0,b=0.0,a=1.0) 





## Tangent screen 

## Color of the tangent screen
tangent_screen_color = color.RGBColor(r=(163/255),g=(210/255),b=(202/255),a=1) 

