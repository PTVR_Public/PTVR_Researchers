#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  6 16:31:44 2021

@author: johanna

Parameters used in cardboard_mnread.py

"""

import PTVR.Stimuli.Color as color
import PTVR.VisualAngleCalculation as VAcalcul

## Text

phrases_to_display = ["Ma mère aime bien écouter le chant des cigales en plein été",
                          "Le petit chat de ma cousine attrape très souvent des mulots",
                          "Nous avons un très grand jardin rempli de belles jonquilles"]
## Visual angle in degrees of the first phrase
visual_angle_of_centered_object = 3 

## Distance in meters between the text and the camera
viewing_distance_in_m = 0.71 

## decreasing size between each phrase /!\ in logMAR /!\
decreasing_size = 0.1 

## Font used to display the text.It is the mnread's standard font to use. Changing this parameter will occure errors
font_to_use = "TimesRoman"   

## horizontal alignment of the text. The "justified"parameter is mandatory to display a phrase following MNRead's standards
text_alignment = "justified" 

## Color of the text
text_color = color.RGBColor(r=0.0,g=0.0,b=0.0,a=1.0) 


## Tangent screen 

## Color of the screen behind the text
tangent_screen_color = color.RGBColor(r=1.0,g=1.0,b=1.0,a=1.0) 

## Set the width and height of the screen. The value are set to be relatives to the text's size. 
tangent_screen_width = 80 * VAcalcul.visual_angle_to_size_on_tangent_screen(visual_angle_of_centered_object, viewing_distance_in_m)

tangent_screen_height = 3 * 20 * VAcalcul.visual_angle_to_size_on_tangent_screen(visual_angle_of_centered_object, viewing_distance_in_m)



## Demo
## Set to true if you want to have the visual angle and the logmar displayed next to the phrase
is_mnread_demo = True 