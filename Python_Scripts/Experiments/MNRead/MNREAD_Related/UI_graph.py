# -*- coding: utf-8 -*-
"""
Demo to show how to use the GraphUI and create a simple graph with the X and Y axis, or a more complex one with a new X axis on the top and/or a new Y axis on the right

@author: jdelacha
"""



# from sys import exit
import PTVR.SystemUtils
#import sys
import numpy as np
from math import pi
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import PTVR.Data.Event as event

username="Johanna"
heightOfStaticPOV_m = 0
current_coordinate_system = np.array([0,heightOfStaticPOV_m,0]) # 1.2 is the height of the static POV when the user is sitting   


def main():
    
    ## Creation of the visual scene 
    my_world = visual.The3DWorld()
    my_world.translate_coordinate_system_along_global(current_coordinate_system)
    scene  = PTVR.Stimuli.Scenes.VisualScene () 
    my_world.add_scene(scene)
    
    points = [ [0.2,6] , [0.4,13] , [0.6,10] , [0.8,8] , [1,9] , [1.2,10] , [1.4,8] , [1.6,9] ]
    graph = PTVR.Stimuli.UIObjects.GraphUI()
    scene.placeUI(graph)
    
    
    parameter1 = 0.8
    parameter2 = 20
    
    eventUpdateGraph = event.UpdateGraph(graph_id=graph.id,parameter1=parameter1, parameter2=parameter2)
    change = event.ChangeObjectEnable(object_id=graph.id, effect="deactivate")
    print(graph.id)
    button = PTVR.Stimuli.UIObjects.ButtonUI(nameButton="update",position_current_CS=np.array([0.0,0.0,0.0]), color = color.RGBColor(r=0.0,g=0.0,b=0.0,a=1.0), height=80.0)
    button_text = PTVR.Stimuli.UIObjects.TextUI(text="Update")
    button.placeUIObject(button_text)
    update= PTVR.Data.Input.Button(idObjectButton=button.id)
    
    scene.placeUI(button)
    
    scene.AddInteraction(inputs=[update],events=[eventUpdateGraph])
    # Write The3DWorld to file
    my_world.write()
    print("The The3DWorld has been written.")


if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld(username)
