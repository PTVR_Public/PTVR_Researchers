# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Experiments\Augmented_Vision\
    2025_IEEEVR_paper.py

Goal : illustrates the scenes used in IEEEVR paper (2025):
    https://hal.science/hal-04918641
    In this paper, you will find all explanations on the different types
    of Augmented Vision that can be used in the present script.


a/ What you have to do before running the present script:
    
    You must download on your PC some files that contain complex 3D entities
   whose size in memory would be too large in the long run  to be stored in PTVR. 
   These files are called "asset bundles" in Unity and are stored in the following 
   URL: https://files.inria.fr/ptvr/
        (asset bundles from this URL are free)
        
    For the present script, you must download the asset bundle called
    "experiments/experiments_2024_augmented_vision" from the URL mentioned above.
    As each asset bundle consists of two files you must actually download the 
    following files:
    - experiments_2024_augmented_vision
    - experiments_2024_augmented_vision.manifest
    
    Once you have downloaded these files on your PC, you MUST store them
    in a directory called "/Asset_bundles". 
    In addition, this directory will have to be at the same level as 
    the "\PTVR_Researchers" directory.
	    For instance, if you installed PTVR such that it was in the following 
    directory:
	C:\my_directory\PTVR_Researchers
	then the directory containing ALL your asset bundles will now have to be in:
	C:\my_directory\Asset_bundles 

created by Carlos Aguilar (january 2025)
"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Forest, CustomObject, Sphere
from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ImageFromDrawing as IFD
import PTVR.Data.Callback as callback
import PTVR.Data.Event as event
import numpy as np
from PTVR.SystemUtils import LaunchThe3DWorld
import os

my_contingency = ImageContingency.HEADSET
# ImageContingency.HEADSET,
# ImageContingency.LEFT_EYE, ImageContingency.RIGHT_EYE, ImageContingency.GAZE,
# ImageContingency.LEFT_HAND, or ImageContingency.RIGHT_HAND.

# the simplest experiment
my_world = The3DWorld()


# Play around with the different True/False combinations below
flag_augmented_vision = True 
flag_smart_augmentation = True
flag_is_ROAV_a_frozen_image = True


if (flag_smart_augmentation):
    flag_ROAV_world_centered = True  # ROAV stands for Region Of Augmented Vision
else:
    flag_ROAV_world_centered = False

humans_list = ["Female1.prefab",
               "Female3.prefab",
               "Female2.prefab",
               "Male1.prefab",
               ]
my_asset_bundle_name = "experiments_2024_augmented_vision"


def set_environnement(my_scene, my_world):
    collider_size = np.array([0.001, 0.001, 0.001])

    room = CustomObject(
        asset_bundle_name=my_asset_bundle_name,
        asset_to_load="Assets/Prefabs/experiments_2024_augmented_vision/FreeRoom.prefab",
        # path defined in the '.manifest' file of the assetBundle above
        collider_size=collider_size,
        position_in_current_CS=np.array([-3.0, 0.0, 2.0])
    )
    my_scene.place(room, my_world)

    sofa = CustomObject(
        asset_bundle_name=my_asset_bundle_name,
        asset_to_load="Assets/Prefabs/experiments_2024_augmented_vision/Sofa.prefab",
        # path defined in the '.manifest' file of the assetBundle above
        object_size=np.array([0.7, 0.7, 0.7]),
        collider_size=collider_size,
        rotation_in_current_CS=np.array([0, 90, 0]),
        position_in_current_CS=np.array([-2.44, 0, 2.58])
    )
    my_scene.place(sofa, my_world)

    sofa_2 = CustomObject(
        asset_bundle_name=my_asset_bundle_name,
        asset_to_load="Assets/Prefabs/experiments_2024_augmented_vision/Sofa.prefab",
        # path defined in the '.manifest' file of the assetBundle above
        object_size=np.array([0.7, 0.7, 0.7]),
        collider_size=collider_size,
        rotation_in_current_CS=np.array([0, 180, 0]),
        position_in_current_CS=np.array([-0.11, 0.0, 5])
    )
    my_scene.place(sofa_2, my_world)

    table = CustomObject(
        asset_bundle_name=my_asset_bundle_name,
        asset_to_load="Assets/Prefabs/experiments_2024_augmented_vision/Table.prefab",
        # path defined in the '.manifest' file of the assetBundle above
        object_size=np.array([0.7, 0.7, 0.7]),
        collider_size=collider_size,
        rotation_in_current_CS=np.array([0, 0, 0]),
        position_in_current_CS=np.array([0, 0.0, 2.5])
    )
    my_scene.place(table, my_world)

    plant1 = CustomObject(
        asset_bundle_name=my_asset_bundle_name,
        asset_to_load="Assets/Prefabs/experiments_2024_augmented_vision/Plant 3.prefab",
        # path defined in the '.manifest' file of the assetBundle above
        object_size=np.array([0.7, 0.7, 0.7]),
        collider_size=collider_size,
        rotation_in_current_CS=np.array([0, 0, 0]),
        position_in_current_CS=np.array([-1.9, 0.0, 4.9])
    )
    my_scene.place(plant1, my_world)

    plant2 = CustomObject(
        asset_bundle_name=my_asset_bundle_name,
        asset_to_load="Assets/Prefabs/experiments_2024_augmented_vision/Plant 4.prefab",
        # path defined in the '.manifest' file of the assetBundle above
        object_size=np.array([0.7, 0.7, 0.7]),
        collider_size=collider_size,
        rotation_in_current_CS=np.array([0, 0, 0]),
        position_in_current_CS=np.array([1.895, 0.0, 4.9])
    )
    my_scene.place(plant2, my_world)

    pcTable = CustomObject(
        asset_bundle_name=my_asset_bundle_name,
        asset_to_load="Assets/Prefabs/experiments_2024_augmented_vision/PCTable.prefab",
        # path defined in the '.manifest' file of the assetBundle above
        collider_size=collider_size,
        object_size=np.array([0.7, 0.7, 0.7]),
        rotation_in_current_CS=np.array([0, 90, 0]),
        position_in_current_CS=np.array([3.6, 0.0, 3.06])
    )
    my_scene.place(pcTable, my_world)


def set_humans(my_scene, my_world):
    circle_list = []
    human_list = []

    positions_list = [np.array([-2.3, 0, 1.911]),
                      np.array([-2.22, 0, 2.981]),
                      np.array([-0.687, 0, 4.7]),
                      np.array([0.405, 0, 4.87])]

    rotations_list = [np.array([0, 90, 0]),
                      np.array([0, 90, 0]),
                      np.array([0, 180, 0]),
                      np.array([0, 180, 0])]

    for i in range(len(humans_list)):

        collider_center = np.array([0.0, 1.31, 0.0])
        collider_size = np.array([0.5, 0.5, 0.25])

        if (humans_list[i] == "Female3.prefab"):
            collider_center += np.array([0.0, 0.0, 0.0])
        if (humans_list[i] == "Female1.prefab"):
            collider_center += np.array([0.0, 0.0, 0.0])

        human = CustomObject(
            asset_bundle_name=my_asset_bundle_name,
            asset_to_load="Assets/Prefabs/experiments_2024_augmented_vision/" +
            humans_list[i],
            position_in_current_CS=positions_list[i],
            rotation_in_current_CS=rotations_list[i],
            collider_center=collider_center,
            collider_size=collider_size,
            display_collider=False
        )
        my_scene.place(human, my_world)

        start_scene_event = PTVR.Data.Event.Timer(
            delay_in_ms=0, timer_start="Start Scene", event_name="Timer")

        if (humans_list[i] == "Male1.prefab"):
            human_sit = PTVR.Data.Callback.AnimateObject(
                object_id=human.id,
                effect="activate",
                animation_string="sit_thought",
                play_until_the_end=True)

            my_scene.AddInteraction(events=[start_scene_event],
                                    callbacks=[human_sit])

        if (humans_list[i] == "Female1.prefab"):
            human_sit = PTVR.Data.Callback.AnimateObject(
                object_id=human.id,
                effect="activate",
                animation_string="sit_wait",
                play_until_the_end=True)

            my_scene.AddInteraction(events=[start_scene_event],
                                    callbacks=[human_sit])

        circle_for_yellow_sprite = IFD.ImageFromDrawing(
            line_width_as_ratio=0.04,
            image_file="Circle.png",
            fill=False,
            shape="ellipse",
        )

        if flag_smart_augmentation:
            circle_position = positions_list[i] + \
                collider_center
            yellow_circle_sprite = PTVR.Stimuli.Objects.Sprite(
                image_file="Circle.png",
                path_to_image_file=circle_for_yellow_sprite.image_path,  # current directory
                size_in_meters=np.array([0.5, 0.4, 0.2]),
                position_in_current_CS=circle_position,
                rotation_in_current_CS=np.array([0, 0, 0])
            )

            yellow_circle_sprite.rotate_to_look_at(my_world.headset.id)
            my_scene.place(yellow_circle_sprite, my_world)

            circle_list.append(yellow_circle_sprite.id)

        human_list.append(human.id)

    return human_list, circle_list


def set_photo(my_scene, my_world):

    #################### Photo #####################
    photo_sprite = PTVR.Stimuli.Objects.Sprite(
        image_file="Female1.png",
        path_to_image_file=os.getcwd(),  # current directory
        size_in_meters=np.array([0.25, 0.25, 0.5]),
        position_in_current_CS=np.array([0.0, 0.0, 0.0]),
        rotation_in_current_CS=np.array([45, 0, 0])
    )

    photo_sprite.set_parent(my_world.handControllerRight,
                            need_parent_information=False,
                            z_local=0.05, y_local=0.13)

    my_scene.place(photo_sprite, my_world)


def main():
    my_scene = VisualScene(skybox="BrightMorning",
                           is_right_hand_controller_visible=True,
                           augmented_vision=flag_augmented_vision)

    #################### Room/furniture #############
    set_environnement(my_scene, my_world)

    #################### Humans #####################
    human_list, circle_list = set_humans(my_scene, my_world)

    #################### Photo #####################
    set_photo(my_scene, my_world)

    ################### Cursor ######################

    circle_for_cursor = IFD.ImageFromDrawing(
        line_width_as_ratio=0.04,
        color=color.RGBColor(r=1, g=1, b=1, a=1),
        shape="ellipse",
    )

    if (not flag_smart_augmentation):
        my_augmented_camera = AugmentedVisionROICursor(
            image=circle_for_cursor,
            contingency_type=my_contingency,
            size_in_degrees=[7, 7],
            display_cursor_cone=False,
            # list_of_ROI_id=np.array(human_list),
            # list_of_ROI_circle_id=np.array(circle_list)
        )
    else:
        my_augmented_camera = AugmentedVisionROICursor(
            image=circle_for_cursor,
            contingency_type=my_contingency,
            size_in_degrees=[7, 7],
            display_cursor_cone=False,
            list_of_ROI_id=np.array(human_list),
            list_of_ROI_circle_id=np.array(circle_list)
        )

    my_scene.place_contingent_cursor(my_augmented_camera)

    event_up = event.Keyboard(valid_responses=["up"], mode="press")

    event_down = event.Keyboard(valid_responses=["down"], mode="press")

    trigger_button_on = event.HandController(
        valid_responses=['right_trigger'],
        mode="press")

    trigger_button_off = event.HandController(
        valid_responses=['right_trigger'],
        mode="release")

    augment = callback.AugmentVision(
        zooming_factor = 10,
        is_ROAV_world_centered = flag_ROAV_world_centered,
        is_ROAV_a_frozen_image = flag_is_ROAV_a_frozen_image,
        transition_duration_in_sec = 0.1)

    not_augment = callback.AugmentVision(effect="deactivate",
                                         transition_duration_in_sec=0.1)

    my_scene.AddInteraction(events=[event_up, trigger_button_on],
                            callbacks=[augment])
    my_scene.AddInteraction(events=[event_down, trigger_button_off],
                            callbacks=[not_augment])

    my_world.add_scene(my_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    LaunchThe3DWorld()  # Launch the Experiment with PTVR.
