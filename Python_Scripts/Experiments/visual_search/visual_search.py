# -*- coding: utf-8 -*-

'''
...\PTVR_Researchers\Python_Scripts\Experiments\visual_search.py
'''
from PTVR.Visual import The3DWorld# Used to create the experiment
from PTVR.SystemUtils import LaunchThe3DWorld # Used to launch the experiment
from PTVR.Stimuli.Scenes import VisualScene, CheckScene # Used to create the scene 
from PTVR.Stimuli.Objects import Cube, Sphere, AudioSource # Used to create visual object 
from PTVR.Stimuli.Color import RGBColor
from PTVR.Data.Event import PointedAt, HandController
from PTVR.Data.Callback import AddInteractionPassedToCallback, EndCurrentScene,ChangeObjectOutline,ChangeAudioSettings
from PTVR.Pointing.PointingCursor import  PointingLaser, LaserContingency
from numpy import array
import random  


# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Henry" # The subject's name
height_of_head =  1.2 # this value (in meters) is chosen so as to correspond
# to the height of the subject's headset during the experiment (seated or standing)
viewpoint_position = array ([0, height_of_head, 0])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint


###  Distractors
number_of_distractor = 512

radial_distance_min_max_values = array([2, 15])

# Color VisualSearch
color_base = RGBColor(r=0, g=1, b=0) # Green
color_distractor = RGBColor(r=1, g=0, b=0) # Red
color_target = RGBColor(r=1, g=0, b=0) # Red

#### POINTING LASER parameters
my_laser_color = RGBColor(r= 0.7)
hand = LaserContingency.RIGHT_HAND



# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================
my_world = The3DWorld()
sound_end_of_block = "applaudisement.mp3" # sound indicating end of block
sound_wrong_response = "buzzer.ogg"
handcontroller_id = my_world.handControllerRight.id
configurations_set = set()

def new_objects(random_value):
    if(random_value == 1):
        value = random.randint (0, 1)
        if(value == 1):
            color_object = color_base
        else:
            color_object = color_distractor
            
        my_object = Sphere(size_in_meters= array([0.2, 0.2, 0.2]), color = color_object)
    else:
        my_object = Cube(size_in_meters= array([0.2, 0.2, 0.2]), color = color_base)
    return my_object

def create_events_when_pointing_at_objects(my_object):
    reticle_pointing_at_object = PointedAt (target_id = my_object.id, 
                                            activation_cone_origin_id=handcontroller_id, 
                       activation_cone_radius_deg=1, activation_cone_depth=500, mode="press", 
                       event_name="object_is_pointed_at") 
    reticle_NOT_pointing_at_object = PointedAt (target_id = my_object.id, 
                                                activation_cone_origin_id=handcontroller_id, 
                        activation_cone_radius_deg=1, activation_cone_depth=500, mode="release", 
                        event_name="object_is_not_pointed_at") 
    return reticle_pointing_at_object, reticle_NOT_pointing_at_object

def create_callbacks_to_change_objects(my_object):
    callback_outline_on = ChangeObjectOutline(object_id=my_object.id, outline_color=RGBColor(r=1.0,g= 1.0, b=1.0,a= 1),
                                              effect="activate")
    callback_outline_off = ChangeObjectOutline(object_id=my_object.id, outline_color=RGBColor(r=1.0,g= 1.0, b=1.0,a= 1),
                                               effect="deactivate")
    return callback_outline_on,callback_outline_off 

def main():
    print("Creating an experiment to study visual search in various 3D regions...")
    
    my_initial_scene = CheckScene ( my_world = my_world,
                                     are_both_hand_controllers_visible= True,
                                    text_information = "In this experiment you have "\
                                    "to search for the red cube \n among over objects " \
                                    "by pointing at it and pressing the trigger.\n" \
                                    "When you are ready to start, please press the trigger.")
    my_world.add_scene (my_initial_scene)

    # Create the visual scene that will contain the distractors and the target
    my_scene = VisualScene (is_right_hand_controller_visible = True)
    
    # Create a pointer   
    my_laser_beam =  PointingLaser ( hand_laser = hand, 
                                     laser_color = my_laser_color,
                                     laser_width = 0.01 )
    my_scene.place_pointing_laser ( my_laser_beam )
    my_scene.AddPointingCursor(my_laser_beam)
    

    my_world.translate_coordinate_system_along_current(translation = viewpoint_position)
    
    audio_wrong = AudioSource(audioFile=sound_wrong_response, is_playing_directly=False)
    my_scene.place(audio_wrong,my_world)
    while len(configurations_set) < number_of_distractor+1:  
        
        radialdistance = random.uniform(radial_distance_min_max_values [0], 
                                        radial_distance_min_max_values [1])
        eccentricity = random.gauss(90, 40) 
        halfmeridian = random.randint(0, 360)
        configuration = (radialdistance, eccentricity, halfmeridian)
        if tuple(configuration) not in configurations_set:
                configurations_set.add(tuple(configuration))
                  
    configurations = [list(config) for config in configurations_set]
    target_configuration = random.choice(configurations)    
    print("my target is at np.array([radial_distance,eccentricity,half_meridian)) : " + 
          str(target_configuration))
    for config in configurations:
        radialdistance = config[0]
        eccentricity = config[1]
        halfmeridian = config[2]
        if(config != target_configuration):
            my_object = new_objects(random.randint(0, 1)) # Sphere or Cube 
        else: 
            my_object = Cube(size_in_meters= array([0.2, 0.2, 0.2]), color=color_target)
        my_object.set_perimetric_coordinates(radialDistance=radialdistance,
                                             eccentricity=eccentricity, halfMeridian=halfmeridian)
        my_scene.place(my_object, my_world)
        inside,outside = create_events_when_pointing_at_objects (my_object)
        outline_on,outline_off = create_callbacks_to_change_objects(my_object)  
        launchwrong_song = ChangeAudioSettings(audio_id=audio_wrong.id)
        press = HandController(valid_responses=['right_trigger'],mode="press",event_name="press")
        if(config != target_configuration):
            add = AddInteractionPassedToCallback(events=[press], callbacks=[launchwrong_song], 
                                                 effect="activate")
            remove = AddInteractionPassedToCallback(events=[press], callbacks=[launchwrong_song], 
                                                    effect="deactivate")
            my_scene.AddInteraction(events=[inside], callbacks=[add,outline_on])
            my_scene.AddInteraction(events=[outside], callbacks=[remove,outline_off])
             
        else: 
            finish = EndCurrentScene(callback_name = "target has been found")
            
            add = AddInteractionPassedToCallback(events=[press], callbacks=[finish], effect="activate")
            remove = AddInteractionPassedToCallback(events=[press], callbacks=[finish], effect="deactivate")
            # Register Events and Callbacks
            my_scene.AddInteraction(events=[inside], callbacks=[add,outline_on])
            my_scene.AddInteraction(events=[outside], callbacks=[remove,outline_off])
         

    my_world.add_scene(my_scene)
    
    my_world.reset_coordinate_system()
    
    calibration = CheckScene(my_world = my_world,text_information= "Congratulations!",are_both_hand_controllers_visible = True)
    audio = AudioSource(audioFile=sound_end_of_block)
    calibration.place(audio,my_world)
    my_world.add_scene(calibration)

    my_world.write() # create json file containing the parameters of the present experiment
    print("The .json file corresponding to this experiment has been created.")


if __name__ == "__main__":
        main()
        LaunchThe3DWorld(username) # Launch the3DWorld with PTVR.