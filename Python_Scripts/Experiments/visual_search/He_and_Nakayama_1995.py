#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Experiments\visual_search\
    He_and_Nakayama_1995.py
    
    Basic stimuli for Experiment 1 of He and Nakayama (1995). 
    Visual attention to surfaces in three-dimensional space. PNAS. 92 (24) 11155-11159
    https://doi.org/10.1073/pnas.92.24.11155    
    
Created On :(31 January 2024)
@author: E. Castet 
'''
from PTVR import SystemUtils, Visual
from PTVR.Stimuli import Scenes, Objects, Color as color
import numpy as np

head_height = 1.2 # height in meters of subject's headset during the experiment

# dimensions and positions of the rectangles (in meters)
rect_width = 0.1;  delta_x = 1.4 * rect_width       
rect_height = rect_width/4; delta_y = 1.2 * rect_width; 
x_table = np.linspace (-1.5 * delta_x, 1.5 * delta_x, 4)
y_table = np.linspace (-1.0 * delta_y, 1.0 * delta_y, 3)    
between_plane_distance = 0.04

def create_plane (rectangle_rgb, scene, world):
    i=0
    for row in range (0, 3):
        for column in range(0, 4):
            x = x_table [column]; y = y_table [row]
            my_color = rectangle_rgb [i] * 0.9; i+=1
            my_rectangle =  Objects.Cube ( 
                size_in_meters = np.array ( [rect_width, rect_height, 0.001] ),
                color =  color.RGBColor (r = my_color, g = my_color, b = my_color),
                position_in_current_CS = np.array ([x, y, 0]) )
            scene.place (my_rectangle, world)    
    
def main():
    my_world = Visual.The3DWorld (name_of_subject = "Lucy")
    my_scene = Scenes.VisualScene ( global_lights_intensity = 0 )     
    #  Translate Origin upward and forward to the center of the closest plane
    my_world.translate_coordinate_system_along_global ( 
        translation = np.array ([0, head_height*0.75 , 0.7]) ) 
    for plane in range (0, 3):  # Create the three fronto-parallel planes
        if plane == 1: # middle plane  : search plane with one target      
            rectangle_rgb = np.random.permutation ( np.array([1,1,1,1,1,1,1,1,1,1,1,0]) ) 
        else: # nearer and farther planes
            rectangle_rgb = np.random.permutation ( np.array([1,1,1,1,1,1,1,1,0,0,0,0]) )
        create_plane (rectangle_rgb, my_scene, my_world)
        my_world.translate_coordinate_system_along_global ( 
            translation = np.array ([0, rect_height, between_plane_distance]) )
    my_world.add_scene (my_scene)
    my_world.write() # create .json file 

if __name__ == "__main__":
        main()
        SystemUtils.LaunchThe3DWorld()
