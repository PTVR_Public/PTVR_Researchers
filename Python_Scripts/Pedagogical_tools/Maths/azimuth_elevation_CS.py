# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Pedagogical_tools\Maths\
    azimuth_elevation_CS.py

This demo illustrates how a PTVR script can be useful as a pedagogical tool.

Here a cube can be set in motion on a circle of constant azimuth or on a 
circle of constant elevation (by clicking on menus' buttons).

Through this active manipulation in 3D, the student can quickly learn the basic 
principles of the spherical Coordinate System called "azimuth / elevation".

created by Carlos Aguilar (january 2025)

"""

import PTVR.SystemUtils

import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.UIObjects
from PTVR.Stimuli.Objects import Sphere
import PTVR.Stimuli.Color as color
import PTVR.Data.Event as event
import PTVR.Stimuli.Objects
import PTVR.Data.Callback as callback
import PTVR.Data.Callbacks.CounterCallback
import PTVR.Data.Callbacks.MovementCallback
import PTVR.Data.Events.CounterEvent
from PTVR.Blocks import MenuWithPointAndClick as menuWithPointAndClick
from PTVR.Blocks import DrawCircle
from PTVR.Pointing.PointingCursor import PointingLaser, LaserContingency
import numpy as np

length_of_gizmo_axes = 1.0
width_of_gizmo_axes = 0.03

# Laser parameters
my_laser_color = color.RGBColor(r=0.7, g=0.5)

# Gizmos coordinates
x_gizmo = 0.0
y_gizmo = 0.0
z_gizmo = 0.0
fontsize_in_postscript_points = 150

initial_elevation = 0
initial_azimuth = 0
initial_radialDistance = 0.7
speed = 10


azimuth_reference_cardinal_direction = "north"  # "east" "west" "north" "south"
azimuth_rotation_direction = "clockwise"  # "counterclockwise" "clockwise"

translation_applied_for_new_CS = np.array([0, 0, 1.5])


def set_text(my_scene, my_world):
    text_x = PTVR.Stimuli.Objects.Text(
        text="z", fontsize_in_postscript_points=fontsize_in_postscript_points,
        color=color.RGBColor(r=0, g=0, b=1, a=1),
        position_in_current_CS=np.array(
            [x_gizmo, y_gizmo, z_gizmo+length_of_gizmo_axes + 0.02])
    )
    text_y = PTVR.Stimuli.Objects.Text(
        text="y", fontsize_in_postscript_points=fontsize_in_postscript_points,
        color=color.RGBColor(r=0, g=1, b=0, a=1),
        position_in_current_CS=np.array(
            [x_gizmo, y_gizmo+length_of_gizmo_axes + 0.02, z_gizmo])
    )
    text_z = PTVR.Stimuli.Objects.Text(
        text="x", fontsize_in_postscript_points=fontsize_in_postscript_points,
        color=color.RGBColor(r=1, g=0, b=0, a=1),
        position_in_current_CS=np.array(
            [x_gizmo + length_of_gizmo_axes + 0.02, y_gizmo, z_gizmo])
    )
    text = PTVR.Stimuli.Objects.Text(
        text="The azimuth / elevation Coordinate System:\n"
        + "Click on the buttons to change azimuth \n"
        + "and elevation of the cube.\n"   
        + "(a translation of the CS can be achieved in the script).\n"
        + "MOVE your head laterally to perceive the depth of the yellow sphere.",
 
        fontsize_in_postscript_points=fontsize_in_postscript_points,
        color=color.RGBColor(r=0, g=0, b=0, a=1),
        position_in_current_CS=np.array([x_gizmo + length_of_gizmo_axes - 0.2,
                                         y_gizmo + length_of_gizmo_axes + 1.2,
                                         z_gizmo + length_of_gizmo_axes/2])
    )

    text.rotate_to_look_at(id_object_to_lookAt=my_world.headset.id)
    my_scene.place(text, my_world)

    my_scene.place(text_x, my_world)
    my_scene.place(text_y, my_world)
    my_scene.place(text_z, my_world)


def set_azimuth(cube, circle, my_scene, my_world):
    color_vector_list = [
        color.RGBColor(r=0.5, g=0.5, b=0.5, a=0.5),
        color.RGBColor(r=0.5, g=0.5, b=0.5, a=0.5),
        color.RGBColor(r=0, g=0, b=1, a=0.7),
        color.RGBColor(r=0, g=1, b=0, a=0.7),
        color.RGBColor(r=1, g=0, b=0, a=0.7),
    ]

    button_names = ["reset", "stop", "azimuth+", "azimuth-"]
    callbacks_list = []

    for i in range(len(button_names)):
        if (button_names[i] == "reset"):
            move_azimuth = PTVR.Data.Callbacks.MovementCallback.SetAzimuthAndElevationInGlobalCS(
                object_id=cube.id,
                azimuth=initial_azimuth, elevation=initial_elevation, radialDistance=initial_radialDistance,
                azimuth_reference_cardinal_direction="north",
                azimuth_rotation_direction="clockwise",
                translation_applied_for_new_CS=translation_applied_for_new_CS
            )

            turn_circle = PTVR.Data.Callbacks.MovementCallback.SetCircleInAzimuthAndElevation(
                object_id=circle.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                object_to_track_id=cube.id,
                effect="deactivate")
        elif (button_names[i] == "stop"):
            move_azimuth = PTVR.Data.Callbacks.MovementCallback.MoveInAzimuthAndElevationCS(
                object_id=cube.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                movement_direction_in_azimuth_and_elevation=np.array(
                    [1, 0]),  #
                speed=speed,  # °/s
                movement_id=-1,
                azimuth_rotation_direction=azimuth_rotation_direction,
                radialDistance=initial_radialDistance,
                effect="deactivate")

            turn_circle = PTVR.Data.Callbacks.MovementCallback.SetCircleInAzimuthAndElevation(
                object_id=circle.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                object_to_track_id=cube.id,
                effect="deactivate")

        elif (button_names[i] == "azimuth+"):
            move_azimuth = PTVR.Data.Callbacks.MovementCallback.MoveInAzimuthAndElevationCS(
                object_id=cube.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                movement_direction_in_azimuth_and_elevation=np.array(
                    [1, 0]),  #
                speed=speed,  # °/s
                movement_id=-1,
                azimuth_rotation_direction=azimuth_rotation_direction,
                radialDistance=initial_radialDistance)

            turn_circle = PTVR.Data.Callbacks.MovementCallback.SetCircleInAzimuthAndElevation(
                object_id=circle.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                object_to_track_id=cube.id,
                movement_direction_in_azimuth_and_elevation=np.array(
                    [1, 0]),  #
                effect="activate")
        elif (button_names[i] == "azimuth-"):
            move_azimuth = PTVR.Data.Callbacks.MovementCallback.MoveInAzimuthAndElevationCS(
                object_id=cube.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                movement_direction_in_azimuth_and_elevation=np.array(
                    [-1, 0]),  #
                speed=speed,  # °/s
                movement_id=-1,
                azimuth_rotation_direction=azimuth_rotation_direction,
                radialDistance=initial_radialDistance)

            turn_circle = PTVR.Data.Callbacks.MovementCallback.SetCircleInAzimuthAndElevation(
                object_id=circle.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                object_to_track_id=cube.id,
                movement_direction_in_azimuth_and_elevation=np.array(
                    [-1, 0]),  #
                effect="activate")

        # change_color = callback.ChangeObjectColor(
        #     object_id=cube.id,
        #     new_color=color_vector_list[i])

        callbacks_list.append([move_azimuth, turn_circle])

    y_pos_button = -0.8
    x_canvas = 2
    z_canvas = 3
    rot = 35

    button_position_in_canvas = [
        np.array([0, y_pos_button + (i+1) * 0.22]) for i in range(len(button_names))]
    canvas_position_in_current_CS = np.array([x_canvas, 1.0, z_canvas])
    canvas_rotation_in_current_CS = np.array([0, rot, 0])
    button_size_in_meters = [np.array([0.5, 0.18])
                             for i in range(len(button_names))]
    canvas_color = color.RGBColor(r=1, g=0.5, b=0, a=0.7)

    menuWithPointAndClick(
        my_scene, my_world,
        valid_responses=["right_trigger"],
        button_names=button_names,
        canvas_text="canvas 1\nChange Azimuth :",
        callbacks_list=callbacks_list,
        canvas_position_in_current_CS=canvas_position_in_current_CS,
        canvas_rotation_in_current_CS=canvas_rotation_in_current_CS,
        canvas_text_fontsize_in_postscript_points=100,
        canvas_size_in_meters=np.array([1.2, 1.5, 1]),
        canvas_color=canvas_color,
        button_position_in_canvas_list=button_position_in_canvas,
        button_size_in_meters_list=button_size_in_meters,
        button_text_fontsize_in_postscript_points=100,

    )


def set_sphere_circles_and_gizmo(my_scene, my_world):
    gizmo = PTVR.Stimuli.Objects.Gizmo(
        length=length_of_gizmo_axes,  width=width_of_gizmo_axes,
        position_in_current_CS=np.array([x_gizmo, y_gizmo, z_gizmo])
    )
    my_scene.place(gizmo, my_world)

    # Iso- circles

    great_circle_about_y = DrawCircle(my_scene, my_world,
                                      position_in_current_CS=np.array(
                                          [x_gizmo, y_gizmo+0.01, z_gizmo]),
                                      circle_color=color.RGBColor(
                                          r=0.0, g=1.0, b=0.0, a=1),
                                      rotation_in_current_CS=np.array(
                                          [90, 0, 0]),
                                      line_width_as_ratio=0.02,
                                      circle_diameter=2*initial_radialDistance,
                                      image_file="great_circle_about_y.png"
                                      )

    great_circle_about_x = DrawCircle(my_scene, my_world,
                                      position_in_current_CS=np.array(
                                          [x_gizmo, y_gizmo, z_gizmo]),
                                      rotation_in_current_CS=np.array(
                                          [0, 90, 0]),
                                      circle_color=color.RGBColor(
                                          r=1.0, g=0.0, b=0, a=1),
                                      line_width_as_ratio=0.02,
                                      circle_diameter=2*initial_radialDistance,
                                      image_file="great_circle_about_x.png"
                                      )

    great_circle_about_z = DrawCircle(my_scene, my_world,
                                      position_in_current_CS=np.array(
                                          [x_gizmo, y_gizmo, z_gizmo]),
                                      rotation_in_current_CS=np.array(
                                          [0, 0, 0]),
                                      circle_color=color.RGBColor(
                                          r=0.0, g=0.0, b=1, a=1),
                                      line_width_as_ratio=0.02,
                                      circle_diameter=2*initial_radialDistance,
                                      image_file="great_circle_about_z.png"
                                      )
    # Creation of Sphere
    sphere_color = color.RGBColor(0.7, 0.7, 0, 0.1)
    sphere = Sphere(color=sphere_color,
                    position_in_current_CS=np.array(
                        [x_gizmo, y_gizmo, z_gizmo]),
                    size_in_meters=np.array([2*initial_radialDistance, 2*initial_radialDistance, 2*initial_radialDistance]))

    my_scene.place(sphere, my_world)


def set_elevation(cube, circle, my_scene, my_world):

    button_names = ["reset", "stop", "elevation+", "elevation-"]
    callbacks_list = []

    for i in range(len(button_names)):
        if (button_names[i] == "reset"):
            move_elevation = PTVR.Data.Callbacks.MovementCallback.SetAzimuthAndElevationInGlobalCS(
                object_id=cube.id,
                azimuth=initial_azimuth, elevation=initial_elevation, radialDistance=initial_radialDistance,
                azimuth_reference_cardinal_direction="north",
                azimuth_rotation_direction="clockwise",
                translation_applied_for_new_CS=translation_applied_for_new_CS
            )

            turn_circle = PTVR.Data.Callbacks.MovementCallback.SetCircleInAzimuthAndElevation(
                object_id=circle.id,
                object_to_track_id=cube.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                effect="deactivate")

        elif (button_names[i] == "stop"):
            move_elevation = PTVR.Data.Callbacks.MovementCallback.MoveInAzimuthAndElevationCS(
                object_id=cube.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                movement_direction_in_azimuth_and_elevation=np.array(
                    [1, 0]),  #
                speed=speed,  # °/s
                movement_id=-1,
                azimuth_rotation_direction=azimuth_rotation_direction,
                radialDistance=initial_radialDistance,
                effect="deactivate")

            turn_circle = PTVR.Data.Callbacks.MovementCallback.SetCircleInAzimuthAndElevation(
                object_id=circle.id,
                object_to_track_id=cube.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                effect="deactivate")

        elif (button_names[i] == "elevation+"):
            move_elevation = PTVR.Data.Callbacks.MovementCallback.MoveInAzimuthAndElevationCS(
                object_id=cube.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                movement_direction_in_azimuth_and_elevation=np.array(
                    [0, 1]),  #
                speed=speed,  # °/s
                movement_id=-1,
                azimuth_rotation_direction=azimuth_rotation_direction,
                radialDistance=initial_radialDistance)

            turn_circle = PTVR.Data.Callbacks.MovementCallback.SetCircleInAzimuthAndElevation(
                object_id=circle.id,
                object_to_track_id=cube.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                movement_direction_in_azimuth_and_elevation=np.array(
                    [0, 1]),  #
                effect="activate")

        elif (button_names[i] == "elevation-"):
            move_elevation = PTVR.Data.Callbacks.MovementCallback.MoveInAzimuthAndElevationCS(
                object_id=cube.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                movement_direction_in_azimuth_and_elevation=np.array(
                    [0, -1]),  #
                speed=speed,  # °/s
                movement_id=-1,
                azimuth_rotation_direction=azimuth_rotation_direction,
                radialDistance=initial_radialDistance)

            turn_circle = PTVR.Data.Callbacks.MovementCallback.SetCircleInAzimuthAndElevation(
                object_id=circle.id,
                object_to_track_id=cube.id,
                translation_applied_for_new_CS=translation_applied_for_new_CS,
                movement_direction_in_azimuth_and_elevation=np.array(
                    [0, -1]),  #
                effect="activate")

        # change_color = callback.ChangeObjectColor(
        #     object_id=cube.id,
        #     new_color=color_vector_list[i])

        callbacks_list.append([move_elevation, turn_circle])

    y_pos_button = -0.8

    button_position_in_canvas = [
        np.array([0, y_pos_button + (i+1) * 0.22]) for i in range(len(button_names))]
    canvas_position_in_current_CS = np.array([3, 1.0, 2])
    canvas_rotation_in_current_CS = np.array([0, 50, 0])

    button_size_in_meters = [np.array([0.55, 0.18])
                             for i in range(len(button_names))]
    canvas_color = color.RGBColor(r=1, g=0, b=1, a=0.7)

    menuWithPointAndClick(
        my_scene, my_world, valid_responses=["right_trigger"],
        button_names=button_names,
        canvas_text="canvas 2\nChange elevation :",
        callbacks_list=callbacks_list,
        canvas_position_in_current_CS=canvas_position_in_current_CS,
        canvas_rotation_in_current_CS=canvas_rotation_in_current_CS,
        canvas_text_fontsize_in_postscript_points=100,
        canvas_size_in_meters=np.array([1.2, 1.5, 1]),
        canvas_color=canvas_color,
        button_position_in_canvas_list=button_position_in_canvas,
        button_size_in_meters_list=button_size_in_meters,
        button_text_fontsize_in_postscript_points=100,
    )


def main():
    my_world = visual.The3DWorld()
    my_world.translate_coordinate_system_along_global(
        translation=translation_applied_for_new_CS)

    my_scene = PTVR.Stimuli.Scenes.VisualScene(
        background_color=color.RGBColor(0.5, 0.5, 0.5, 1.0),
        is_right_hand_controller_visible=True)

    ###### POINTER ##########################################
    my_laser_beam = PointingLaser (
                        laser_color = my_laser_color,
                        laser_width = 0.01)
    my_scene.place_pointing_laser (my_laser_beam)

    cube_size = 0.1
    cube = PTVR.Stimuli.Objects.Cube(
        size_in_meters=np.array([cube_size, cube_size, cube_size]),
        color=color.RGBColor(r=0.5, g=0.5, b=0.5, a=0.5),
        position_in_current_CS=np.array([x_gizmo, y_gizmo, z_gizmo]),
        rotation_in_current_CS=np.array([0, 0, 0]),
    )
    cube.set_azimuth_elevation_coordinates (
        radialDistance = initial_radialDistance,
        azimuth = initial_azimuth,
        elevation = initial_elevation,
        azimuth_reference_cardinal_direction = azimuth_reference_cardinal_direction,
        azimuth_rotation_direction = azimuth_rotation_direction)
    my_scene.place (cube, my_world)
    set_text (my_scene, my_world)

    # circle depending on the cube's position
    circle = DrawCircle (my_scene, my_world,
                position_in_current_CS = np.array([x_gizmo, y_gizmo, z_gizmo]),
                rotation_in_current_CS = np.array( [90, 0, 0]),
                line_width_as_ratio = 0.01,
                circle_diameter = 2 * initial_radialDistance
                )
    
    set_azimuth (cube, circle, my_scene, my_world)
    set_elevation (cube, circle, my_scene, my_world)
    set_sphere_circles_and_gizmo (my_scene, my_world)

    my_world.add_scene(my_scene)
    my_world.write()  # Write The3DWorld to .json file


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
