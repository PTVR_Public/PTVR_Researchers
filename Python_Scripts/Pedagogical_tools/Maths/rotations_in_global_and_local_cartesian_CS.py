# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Pedagogical_tools\Maths\
rotations_in_global_and_local_cartesian_CS.py

This demo illustrates how a PTVR script can be useful as a pedagogical tool.

Here a cube can be set in rotation either in the global rotation CS or
in the local CS (or both) by pointing (and clicking) at menu buttons.

Through this active manipulation in 3D, the student can quickly learn the basic 
principles of global and local rotations in a cartesian 
coordinate system (cartesian CS).

Notes:
    "CS" stands for Coordinate System
    
    "Local" CS : this is the CS of an OBJECT (here the cube)

created by Carlos Aguilar (january 2025)

"""



import PTVR.SystemUtils

import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import PTVR.Data.Event as event
import PTVR.Stimuli.Objects
import PTVR.Data.Callback as callback
import PTVR.Data.Callbacks.CounterCallback
import PTVR.Data.Callbacks.MovementCallback
import PTVR.Data.Events.CounterEvent
from PTVR.Blocks import MenuWithPointAndClick as menuWithPointAndClick
from PTVR.Pointing.PointingCursor import PointingLaser, LaserContingency
import numpy as np

length_of_gizmo_axes = 1.2
width_of_gizmo_axes = 0.02

# Laser parameters
my_laser_color = color.RGBColor(r=0.7, g=0.5)
hand = LaserContingency.RIGHT_HAND

# Display axes of Current CS
x_gizmo = 0.0
y_gizmo = 0.3
z_gizmo = 1.0
fontsize_in_postscript_points = 150


def set_text(cube, my_scene, my_world):
    text_x_cube = PTVR.Stimuli.Objects.Text(
        text="x", fontsize_in_postscript_points=fontsize_in_postscript_points,
        color=color.RGBColor(r=1, g=0, b=0, a=1),
        # position_in_current_CS=np.array([0.6, 0, ]

    )
    text_x_cube.rotate_to_look_at(id_object_to_lookAt=my_world.headset.id)
    text_x_cube.set_parent(cube, need_parent_information=False, x_local=0.3)

    my_scene.place(text_x_cube, my_world)

    text_y_cube = PTVR.Stimuli.Objects.Text(
        text="y", fontsize_in_postscript_points=fontsize_in_postscript_points,
        color=color.RGBColor(r=0, g=1, b=0, a=1),

    )
    text_y_cube.rotate_to_look_at(id_object_to_lookAt=my_world.headset.id)
    text_y_cube.set_parent(cube, need_parent_information=False, y_local=0.3)

    my_scene.place(text_y_cube, my_world)

    text_z_cube = PTVR.Stimuli.Objects.Text(
        text="z", fontsize_in_postscript_points=fontsize_in_postscript_points,
        color=color.RGBColor(r=0, g=0, b=1, a=1),

    )
    text_z_cube.rotate_to_look_at(id_object_to_lookAt=my_world.headset.id)
    text_z_cube.set_parent(cube, need_parent_information=False, z_local=0.3)

    my_scene.place(text_z_cube, my_world)

    text_x = PTVR.Stimuli.Objects.Text(
        text="z", fontsize_in_postscript_points=fontsize_in_postscript_points,
        color=color.RGBColor(r=0, g=0, b=1, a=1),
        position_in_current_CS=np.array(
            [x_gizmo, y_gizmo, z_gizmo+length_of_gizmo_axes + 0.02])
    )
    text_y = PTVR.Stimuli.Objects.Text(
        text="y", fontsize_in_postscript_points=fontsize_in_postscript_points,
        color=color.RGBColor(r=0, g=1, b=0, a=1),
        position_in_current_CS=np.array(
            [x_gizmo, y_gizmo+length_of_gizmo_axes + 0.02, z_gizmo])
    )
    text_z = PTVR.Stimuli.Objects.Text(
        text="x", fontsize_in_postscript_points=fontsize_in_postscript_points,
        color=color.RGBColor(r=1, g=0, b=0, a=1),
        position_in_current_CS=np.array(
            [x_gizmo + length_of_gizmo_axes + 0.02, y_gizmo, z_gizmo])
    )
    text = PTVR.Stimuli.Objects.Text(
        text="Click on the buttons to rotate \n"
        + "the cube about the selected \n"
        + " GLOBAL axis or LOCAL axis.",
        fontsize_in_postscript_points=fontsize_in_postscript_points,
        color=color.RGBColor(r=0, g=0, b=0, a=1),
        position_in_current_CS=np.array([x_gizmo + length_of_gizmo_axes,
                                         y_gizmo + length_of_gizmo_axes + 0.3,
                                         z_gizmo + length_of_gizmo_axes/2])
    )

    text.rotate_to_look_at(id_object_to_lookAt=my_world.headset.id)
    my_scene.place(text, my_world)

    my_scene.place(text_x, my_world)
    my_scene.place(text_y, my_world)
    my_scene.place(text_z, my_world)


def set_global_rotations(cube, my_scene, my_world):
    rotation_speed = 30
    rotation_vector_list = [
        np.array([0, 0, 0]),
        np.array([0, 0, 0]),
        np.array([0, 0, rotation_speed]),
        np.array([0, rotation_speed, 0]),
        np.array([rotation_speed, 0, 0])
    ]

    color_vector_list = [
        color.RGBColor(r=0.5, g=0.5, b=0.5, a=0.5),
        color.RGBColor(r=0.5, g=0.5, b=0.5, a=0.5),
        color.RGBColor(r=0, g=0, b=1, a=0.7),
        color.RGBColor(r=0, g=1, b=0, a=0.7),
        color.RGBColor(r=1, g=0, b=0, a=0.7),
    ]

    button_names = ["reset", "stop", "+z", "+y", "+x"]
    # button_names = ["reset"]
    callbacks_list = []
    global_coordinate_system = True

    for i in range(len(button_names)):
        if (button_names[i] == "reset"):
            rotate_cube = PTVR.Data.Callbacks.MovementCallback.SetOrientationInGlobalCS(
                object_id=cube.id,
                # Object facing the z axis
                rotation_in_global_CS=rotation_vector_list[i],
            )
        elif (button_names[i] == "stop"):
            rotate_cube = PTVR.Data.Callbacks.MovementCallback.Rotate(
                object_id=cube.id,
                rotation_vector=rotation_vector_list[i],
                global_coordinate_system=global_coordinate_system,
                effect="deactivate"
            )

        else:
            rotate_cube = PTVR.Data.Callbacks.MovementCallback.Rotate(
                object_id=cube.id,
                rotation_vector=rotation_vector_list[i],
                global_coordinate_system=global_coordinate_system
            )

        change_color = callback.ChangeObjectColor(
            object_id=cube.id,
            new_color=color_vector_list[i])

        callbacks_list.append([rotate_cube, change_color])

    y_pos_button = -0.8
    x_canvas = 2
    z_canvas = 3
    rot = 35

    button_position_in_canvas = [
        np.array([0, y_pos_button + (i+1) * 0.22]) for i in range(len(button_names))]
    canvas_position_in_current_CS = np.array([x_canvas, 1.0, z_canvas])
    canvas_rotation_in_current_CS = np.array([0, rot, 0])
    button_size_in_meters = [np.array([0.4, 0.18])
                             for i in range(len(button_names))]
    canvas_color = color.RGBColor(r=1, g=1, b=0, a=0.7)

    menuWithPointAndClick(
        my_scene, my_world,
        valid_responses=["right_trigger"],
        button_names=button_names,
        canvas_text="canvas 1\nRotate about global\nworld axis :",
        callbacks_list=callbacks_list,
        canvas_position_in_current_CS=canvas_position_in_current_CS,
        canvas_rotation_in_current_CS=canvas_rotation_in_current_CS,
        canvas_text_fontsize_in_postscript_points=100,
        canvas_size_in_meters=np.array([1.2, 1.5, 1]),
        canvas_color=canvas_color,
        button_position_in_canvas_list=button_position_in_canvas,
        button_size_in_meters_list=button_size_in_meters,
        button_text_fontsize_in_postscript_points=100,

    )


def set_local_rotations(cube, my_scene, my_world):

    rotation_speed = 30
    rotation_vector_list = [
        np.array([0, 0, 0]),
        np.array([0, 0, 0]),
        np.array([0, 0, rotation_speed]),
        np.array([0, rotation_speed, 0]),
        np.array([rotation_speed, 0, 0])
    ]

    color_vector_list = [
        color.RGBColor(r=0.5, g=0.5, b=0.5, a=0.5),
        color.RGBColor(r=0.5, g=0.5, b=0.5, a=0.5),
        color.RGBColor(r=0, g=0, b=1, a=0.7),
        color.RGBColor(r=0, g=1, b=0, a=0.7),
        color.RGBColor(r=1, g=0, b=0, a=0.7),
    ]

    button_names = ["reset", "stop", "+z", "+y", "+x"]

    callbacks_list = []
    global_coordinate_system = False

    for i in range(len(button_names)):
        if (button_names[i] == "reset"):
            rotate_cube = PTVR.Data.Callbacks.MovementCallback.SetOrientationInGlobalCS(
                object_id=cube.id,
                # Object facing the z axis
                rotation_in_global_CS=rotation_vector_list[i],
            )
        elif (button_names[i] == "stop"):
            rotate_cube = PTVR.Data.Callbacks.MovementCallback.Rotate(
                object_id=cube.id,
                rotation_vector=rotation_vector_list[i],
                global_coordinate_system=global_coordinate_system,
                effect="deactivate"
            )

        else:
            rotate_cube = PTVR.Data.Callbacks.MovementCallback.Rotate(
                object_id=cube.id,
                rotation_vector=rotation_vector_list[i],
                global_coordinate_system=global_coordinate_system
            )

        change_color = callback.ChangeObjectColor(
            object_id=cube.id,
            new_color=color_vector_list[i])

        callbacks_list.append([rotate_cube, change_color])

    y_pos_button = -0.8
    button_position_in_canvas = [
        np.array([0, y_pos_button + (i+1) * 0.22]) for i in range(len(button_names))]
    canvas_position_in_current_CS = np.array([3, 1.0, 2])
    canvas_rotation_in_current_CS = np.array([0, 50, 0])

    button_size_in_meters = [np.array([0.4, 0.18])
                             for i in range(len(button_names))]
    canvas_color = color.RGBColor(r=1, g=0, b=1, a=0.7)

    menuWithPointAndClick(
        my_scene, my_world, valid_responses=["right_trigger"],
        button_names=button_names,
        canvas_text="canvas 2\nRotate about local axis:",
        callbacks_list=callbacks_list,
        canvas_position_in_current_CS=canvas_position_in_current_CS,
        canvas_rotation_in_current_CS=canvas_rotation_in_current_CS,
        canvas_text_fontsize_in_postscript_points=100,
        canvas_size_in_meters=np.array([1.2, 1.5, 1]),
        canvas_color=canvas_color,
        button_position_in_canvas_list=button_position_in_canvas,
        button_size_in_meters_list=button_size_in_meters,
        button_text_fontsize_in_postscript_points=100,

    )


def main():
    my_world = visual.The3DWorld()

    my_scene = PTVR.Stimuli.Scenes.VisualScene(
        background_color=color.RGBColor(0.5, 0.5, 0.5, 1.0),
        is_right_hand_controller_visible=True)

    ###### POINTER ##########################################
    my_laser_beam = PointingLaser(hand_laser=hand,
                                  laser_color=my_laser_color,
                                  laser_width=0.01)
    my_scene.place_pointing_laser(my_laser_beam)

    gizmo = PTVR.Stimuli.Objects.Gizmo(
        length=length_of_gizmo_axes,  width=width_of_gizmo_axes,
        position_in_current_CS=np.array([x_gizmo, y_gizmo, z_gizmo])
    )
    my_scene.place(gizmo, my_world)

    cube = PTVR.Stimuli.Objects.Cube(
        size_in_meters=np.array([0.25, 0.25, 0.25]),
        color=color.RGBColor(r=0.5, g=0.5, b=0.5, a=0.5),
        position_in_current_CS=np.array([0.35, 0.8, 1.5]),
        rotation_in_current_CS=np.array([0, 0, 0]),
    )

    my_scene.place(cube, my_world)

    gizmo_cube = PTVR.Stimuli.Objects.Gizmo(
        length=0.25,  width=0.02,
        position_in_current_CS=np.array([0.35, 0.8, 1.5]),
    )

    gizmo_cube.set_parent(cube)
    my_scene.place(gizmo_cube, my_world)

    set_text(cube, my_scene, my_world)

    ############################## Global Rotations ##################################

    set_global_rotations(cube, my_scene, my_world)

    ############################ Local Rotations ##############################

    set_local_rotations(cube, my_scene, my_world)

    my_world.add_scene(my_scene)
    my_world.write()  # Write The3DWorld to .json file


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
