# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\events_and_callbacks_for_pointing\
    head_pointing_to_change_an_object.py
    
Goal of Demo:
    Illustrates the use of the 'PointedAt' event when a pointing "device" 
    (here the head) is used to point at a target ()).
    
    
What to do: 
    Move your head to point at the sphere WITHOUT ANY visible 
    head-contingent cursor.
    As soon as your head is accurately pointing at the sphere,
    the sphere's color changes to blue...
    By moving your head on and off the sphere, you can change the sphere's color
    as long as you want.
    
Emphasis: 
        Your head movements are not accompanied by movements of a cursor.     
        In other words, there is no head-contingent cursor in this demo.

Note: you can decrease the difficulty of the pointing task by increasing
        the pointing_activation_disk_radius_deg parameter.

'''

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import numpy as np
import PTVR.Stimuli.Color as color

pointing_activation_disk_radius_deg = 3 # radius of the pointing activation disk (in degrees)
# increase this parameter to make pointing easier

distance_to_sphere = 3 # in meters

def main():
    my_world = visual.The3DWorld ( )
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    my_target_sphere = PTVR.Stimuli.Objects.Sphere ( 
                        color = color.RGBColor ( 1, 0, 0, 1),
                        size_in_meters = 0.1,
                        position_in_current_CS = np.array ( [0, 1, distance_to_sphere] ) ) 

    # Interactions allowing us to point at objects
    ### Events and Callbacks must be created BEFORE interactions !
    ## Events
    target_is_pointed_at =  \
                event.PointedAt (
                    # What is the object that must be pointed at?
                    target_id = my_target_sphere.id, 
                    
                    # Id of the pointing device:
                    # Headset is the default (shown below for didactic emphasis)
                    activation_cone_origin_id = my_world.headset.id, 
                    # If you want gaze-contingent pointing instead, 
                    # uncomment the line below.                       
                    #activation_cone_origin_id = my_world.eyeCombined.id,                     
                    # In the Spyder IDE, press Ctrl-i while the mouse is on 
                    # 'PointedAt' to get the
                    # other contingency options (left eye, etc...)
                    
                    activation_cone_radius_deg = pointing_activation_disk_radius_deg, 
                    activation_cone_depth = 500, 
                    mode = "press")
                
    target_is_not_pointed_at = \
                event.PointedAt (
                    target_id = my_target_sphere.id,   
                    activation_cone_origin_id = my_world.headset.id, # i.e. -3
                    #activation_cone_origin_id = my_world.handControllerRight.id, # i.e. -2                                      
                    
                    activation_cone_radius_deg = pointing_activation_disk_radius_deg, 
                    activation_cone_depth = 500, 
                    mode = "release") 
    ## Callbacks
    change_object_color  = callback.ChangeObjectColor ( 
                                new_color = color.RGBColor ( b = 1 ), 
                                object_id = my_target_sphere.id)
    change_object_color_back = callback.ChangeObjectColor ( 
                                effect = "deactivate", 
                                object_id = my_target_sphere.id)
    ## Standard Interactions
    # When my_target_sphere is pointed at, its color is changed to Blue
    my_scene.AddInteraction ( events = [ target_is_pointed_at ],
                              callbacks = [ change_object_color ] )
    # When my_target_sphere is NOT pointed at, its color is changed back to Red (initial color) 
    my_scene.AddInteraction ( events = [ target_is_not_pointed_at ],
                              callbacks = [ change_object_color_back] )
    
    my_scene.place (my_target_sphere, my_world)  
    my_world.add_scene (my_scene)
    
    my_world.write() # Write The3DWorld to .json file
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()