# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\events_and_callbacks_for_pointing\
    head_pointing_to_end_trials.py

Description :
    Loop of trials with one scene per trial.
    You must point at the sphere (whose position changes across trials) with 
    your head to go to the next trial.
    
    
Note : you can change the cursor contingency by uncommenting a line of code
        (in 'ImageToContingentCursor()' )
        For instance, you can make it gaze-contingent.
        
        And independently of cursor contingency, you can also change the
        pointing contingency by uncommenting another line of code (in 
        'PointedAt()' ): for instance you can choose gaze-contingency instead
        of head-contingency.

        

"""

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import PTVR.Stimuli.Color as color

from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ReticleImageFromDrawing as RG

nb_of_trials = 4
   
pointing_activation_disk_radius_deg = 3 # radius of the pointing activation disk (in degrees)
# increase this parameter to make pointing easier

reticle_size_deg = 2 * pointing_activation_disk_radius_deg
# The reticle's diameter is thus equal to the diameter of the pointing 
# activation disk.

distance_to_sphere = 3 # in meters



def create_reticle (my_scene):
    # The RETICLE
    # This function creates the head-contingent reticle.
    # This reticle is a head-contingent visual feedback which is not responsible for
    # the pointing process. The latter is performed thanks to the 'pointedAt' events
    # created further below.
    #
    # internally create a PNG file to create a sprite
    reticle_2D_image = RG.ReticleImageFromDrawing ( )

    # transform 'reticle_2D_image' into a contingent cursor.
    my_reticle = ImageToContingentCursor (
                    # If you want to have gaze-contingency instead of 
                    # head contingency (default),
                    # then uncomment the code line below.
                    # contingency_type = ImageContingency.GAZE,
                    # In the Spyder IDE, press Ctrl-i while the mouse is on 
                    # 'ImageToContingentCursor' to get the
                    # other contingency options (left eye, etc...)
                    
                    image = reticle_2D_image,                    
                    size_in_degrees= [reticle_size_deg, reticle_size_deg],
                    distance_if_empty_cursor_cone = distance_to_sphere
                    )
    my_scene.place_contingent_cursor (my_reticle)
    # END of Reticle creation.
    #-------------------------

def main():

    my_world = visual.The3DWorld (  name_of_subject = "nabil" )
    my_world.translate_coordinate_system_along_global ( np.array ([0, 1.2, 0]) )
    text_1 = PTVR.Stimuli.Objects.Text ( text = "Point at sphere with your head to go to next trial",
                                           fontsize_in_postscript_points = 200 )
    text_1.set_cartesian_coordinates ( y = -2, z = 2 ) 
        
    for i in range (0, nb_of_trials):
        trial_scene = PTVR.Stimuli.Scenes.VisualScene (trial = i + 1)
        trial_text = PTVR.Stimuli.Objects.Text ( text = "trial : " + 
                                               str (trial_scene.trial) ,
                                               fontsize_in_postscript_points = 600  )
        trial_text.set_cartesian_coordinates ( z = 2 )
        
        my_target_sphere = PTVR.Stimuli.Objects.Sphere ( 
                            color = color.RGBColor ( 1, 0, 0, 1),
                            size_in_meters = 0.2 ) 
        my_target_sphere.set_perimetric_coordinates ( eccentricity = 20,
                                                      halfMeridian = i * 90,
                                                      radialDistance = 4    )
        
        # if you do not want to have a flat cursor to get visual feedback from your head's
        # motion and thus see if you're pointing correctly at the sphere,
        # then comment the following line.
        create_reticle (trial_scene)
        
        # Interactions allowing us to point at objects
        ### Events and Callbacks must be created BEFORE interactions !
        ## Events
        target_is_pointed_at =  \
                event.PointedAt (
                    # What is the object that must be pointed at?
                    target_id = my_target_sphere.id, 
                    
                    # Id of the pointing device:
                    # Headset is the default (shown below for didactic emphasis)
                    activation_cone_origin_id = my_world.headset.id, 
                    # If you want gaze-contingent pointing instead, 
                    # uncomment the line below.                       
                    #activation_cone_origin_id = my_world.eyeCombined.id,                     
                    # In the Spyder IDE, press Ctrl-i while the mouse is on 
                    # 'PointedAt' to get the
                    # other contingency options (left eye, etc...)
                                                
                    activation_cone_radius_deg = pointing_activation_disk_radius_deg )

        # Create interaction for the current scene
        my_callback_current_scene_end = callback.EndCurrentScene () 
        
        ## Standard Interaction
        trial_scene.AddInteraction ( events = [ target_is_pointed_at ],
                                     callbacks = [ my_callback_current_scene_end ] )
        
        trial_scene.place ( trial_text, my_world ) 
        trial_scene.place ( text_1, my_world ) 
        
        trial_scene.place (my_target_sphere, my_world)
        my_world.add_scene ( trial_scene )
        
    my_world.write() # Write experiment to .json file
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld ()