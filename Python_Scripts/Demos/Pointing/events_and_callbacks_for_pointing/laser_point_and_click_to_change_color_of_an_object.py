# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\events_and_callbacks_for_pointing\
    laser_point_and_click_to_change_color_of_an_object.py
    
Goal: This demo script illustrates the use of the "convenience function" called
    "PointAndClickEvent()".
    Thanks to this convenience function, you can easily point at an object and 
    click on it (here the click is a trigger press on the right handcontroller)
    thus creating several events returned by the function:
        a/ the first event is the key event: it is used in the present demo
        to create an interaction whose goal is to change an object's color.

        b/ the second event is not used here.
        
        b/ the third event is used in the present demo to reset the object's
        color when it is not pointed at.
    


Task : "point-and_click" the sphere to change its color.

        EC : QUESTION à Carlos: ++ pour Saroi 3
            pourquoi  le pointed_at et le non_pointed_at fonctionnent
            AVANT qu'on ait fait le pointed_at_and_clicked ?????'
            mais ne fonctionnent plus APRES le pointed_at_and_clicked ??
            
            Modifier/compléter ci-dessous selon réponse Carlos !
            
            
        And do not point at the sphere to reset its initial color (here red)
    
    TODOOOOO! si c'est normal, explicite qu'une fois que ya eu point and clicked,
    les events ne sont plus fonctionnels !!!!!!!
    
    
Meaning of "convenience function": A convenience function is a non-essential 
        subroutine in a programming library or framework which is intended to 
        EASE commonly performed tasks (https://en.wikipedia.org/wiki/Convenience_function).
        PTVR convenience functions are in: ...\PTVR_Researchers\PTVR\blocks.py

'''

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
from PTVR import Blocks as blocks

import numpy as np
import PTVR.Stimuli.Color as color
from PTVR.Pointing.PointingCursor import PointingLaser, LaserContingency

red =   color.RGBColor(1, 0, 0) # Initial colour
green = color.RGBColor(0, 1, 0) # colour when target is pointed at
blue =  color.RGBColor(0, 0, 1) # colour when target is NOT pointed at
purple = color.RGBColor(r=0.5, g=0.3, b=0.5)

def main():
    my_world = visual.The3DWorld()
    my_scene = PTVR.Stimuli.Scenes.VisualScene(
        are_both_hand_controllers_visible=True)

    my_laser_beam = PointingLaser(
        laser_color=color.RGBColor(r=0.7, g=0.5)
    )
    my_scene.place_pointing_laser(my_laser_beam)

    my_target_sphere = PTVR.Stimuli.Objects.Sphere(
        color = red,
        size_in_meters=0.5,
        position_in_current_CS=np.array([0, 1, 3]))

    # Events
    #
    my_activation_cone_origin_id = my_world.handControllerRight.id # -2 : my_world.handControllerRight.id
    my_activation_cone_radius_deg = 0.1  # usually 0.1 deg with a laser pointer,
    # but you can modify this value to see the effect on the accuracy of pointing.

    ###########################################################################
    # Get the following events that are returned by the function
    # to create the interactions shown further below:
    # a/ target_is_pointed_at_and_clicked
    #♦TODOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    
    # b/ target_is_not_pointed_at
    
    
    ########################
    # Point AND Click EVENT
    target_is_pointed_at_and_clicked, target_is_pointed_at, \
                                        target_is_not_pointed_at = \
        blocks.PointAndClickEvent (
            my_target_sphere.id, my_scene, my_world,
            the_activation_cone_origin_id = my_activation_cone_origin_id,

            the_activation_cone_radius_deg = my_activation_cone_radius_deg
        )

    # Callbacks
    #
    change_object_color_purple = callback.ChangeObjectColor(
        new_color = purple,
        object_id = my_target_sphere.id,
    )
    # change_object_color_back = callback.ChangeObjectColor(
    #     object_id = my_target_sphere.id,
    #     effect = "deactivate"
    # )
    change_object_color_green = callback.ChangeObjectColor(
        new_color = green,
        object_id = my_target_sphere.id
    )

    change_object_color_blue = callback.ChangeObjectColor(
        new_color = blue,
        object_id = my_target_sphere.id
    )
    
    # Interactions
    #
    # When my_target_sphere is pointed at AND
    # when the handcontroller's trigger is subsequently pressed,
    # then the target's colour is changed.
    my_scene.AddInteraction( events = [target_is_pointed_at_and_clicked],
                             callbacks = [change_object_color_purple]
                            )
    # When my_target_sphere is NOT pointed at (i.e. we do not care about
    # clicks here),
    # then the target's colour is reset
    
    my_scene.AddInteraction( events = [target_is_pointed_at],
                             callbacks = [change_object_color_green])
    
    my_scene.AddInteraction( events = [target_is_not_pointed_at],
                             callbacks = [change_object_color_blue])
    

    

    my_scene.place(my_target_sphere, my_world)
    my_world.add_scene(my_scene)

    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
