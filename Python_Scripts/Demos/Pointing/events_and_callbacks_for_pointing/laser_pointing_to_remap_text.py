# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\events_and_callbacks_for_pointing\
laser_pointing_to_remap_text.py 
  
Created on Wed Oct 23 10:46:05 2024
@author: Carlos Aguilar

What you see: when you point at the middle letter of the word "THERE",
                the letters are remapped upward.
                
            when you stop pointing at the middle letter, all the letters come
            back to their original position.
        
The Trick:
        It is necessary to use a trick (otherwise you will encounter
        the problem described below).
        This trick is to place a sphere at the same position (and with
        about the same size) as the letter that must be pointed at (here the 
        middle letter "E").
        This sphere is made invisible (see'is_visible = False') but it 
        is actively used as the target that must be pointed at to induce the 
        upward remapping. You can of course make this sphere visible
        (with 'is_visible = True') for instance when debugging your code.
        While the letters are remapped upwards, the sphere is still pointed at
        and nothing else occurs as long as the sphere is pointed at.
        However, when the sphere is not pointed at any more, this triggers the 
        "deactivate" callback which repositions the letters downward at their
        initial positions.

What problem would occur without the trick above:
        If the code was such that you had to point at the middle letter 
        (instead of the sphere), then
        as soon as the letter would be pointed at, it would be repositionned 
        upward which would instantly imply that the letter would not be
        pointed at any more, and this in turn would trigger the "deactivate" 
        callback whose goal is to reposition the letters DOWNWARD. 
        In other words, pointing at the middle letter would instantaneously
        cause a very rapid up and down shift of the letters !
        


"""

from PTVR.Visual import The3DWorld
from  PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere
import PTVR.Stimuli.Color as color
import numpy as np
from PTVR.Pointing.PointingCursor import  PointingLaser,LaserContingency
from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Data.Event as event
import PTVR.Data.Callbacks.CounterCallback
import PTVR.Data.Events.CounterEvent
import PTVR.Data.Callbacks.MovementCallback as movementCallbacks


text = "THERE"

def main():
    my_world = The3DWorld ()
    my_scene  = VisualScene (are_both_hand_controllers_visible = True,)
    
    my_laser_beam =  PointingLaser ( hand_laser = LaserContingency.RIGHT_HAND, 
                                     laser_color = color.RGBColor ( r = 0.7, g = 0.5 ),
                                     laser_width = 0.01 )          
    my_scene.place_pointing_laser ( my_laser_beam ) 
    
    left_x_pos = 0
    letters_list = []
    letters_position_list = []
    
    sphere_1 = Sphere (size_in_meters = np.array([0.1,0.1,0.1]), 
                       color = color.RGBColor (0.5,0,0),
                       is_visible = False, # or True if you want to see the sphere !
                       position_in_current_CS = np.array ([0.3, 1.05, 1]))    
    my_scene.place(sphere_1, my_world)
    
    for i in range(len(text)):
        
        x_pos_i = left_x_pos + i * 0.15
    
        my_text = PTVR.Stimuli.Objects.Text ( 
            text = text[i], 
            position_in_current_CS = np.array ( [ x_pos_i, 1, 1 ] ),
            vertical_alignment="baseline",
            fontsize_in_postscript_points = 300 )
        
        letters_position_list.append(np.array([ x_pos_i, 1, 1 ]))
        my_scene.place (my_text, my_world)
        
        letters_list.append(my_text)
       
    middle_letter_is_pointed_at = event.PointedAt ( 
                target_id = sphere_1.id, 
                activation_cone_origin_id = 
                    my_world.handControllerRight.id,
                activation_cone_radius_deg = 0.01 )           
    
    middle_letter_is_not_pointed_at = event.PointedAt ( 
                target_id = sphere_1.id, 
                activation_cone_origin_id = 
                    my_world.handControllerRight.id,
                activation_cone_radius_deg = 0.01, mode = "release" )
    
    
    ################################## Callbacks ##################################

    for i in range(len(text)):
        x_pos_i = left_x_pos + i * 0.15
        y_pos_i = 1 + (3 - (abs(2-i))) * 0.1
        print (y_pos_i)
        set_position_up = movementCallbacks.SetCartesianCoordinatesInGlobalCS(
                    object_id =  letters_list[i].id,
                    position_in_global_CS = np.array([x_pos_i, y_pos_i, 1]))
        
        set_position_down = movementCallbacks.SetCartesianCoordinatesInGlobalCS(
                    object_id =  letters_list[i].id,
                    position_in_global_CS = letters_position_list[i])

    ############################## Add Interactions ###############################
              
        my_scene.AddInteraction (events = [middle_letter_is_pointed_at],
                                  callbacks = [set_position_up])
        
        my_scene.AddInteraction (events = [middle_letter_is_not_pointed_at],
                                  callbacks = [set_position_down])
    
    my_world.add_scene (my_scene)
     
    my_world.write () # writes to .json file
    print (".json file has been created.")

if __name__ == "__main__":
        main()
        LaunchThe3DWorld() 


