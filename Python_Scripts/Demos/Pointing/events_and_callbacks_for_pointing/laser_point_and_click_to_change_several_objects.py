# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\events_and_callbacks_for_pointing\
    laser_point_and_click_to_change_several_objects.py
    

Goal: This demo script illustrates the use of the "convenience function" called
    "PointAndClickEvent()".
    Thanks to this convenience function, you can easily point at an object and 
    click on it (here the click is a trigger press on the right handcontroller)
    thus creating two events (returned by the function):
        a/ the first event is the key event: it is used in the present demo
        to create an interaction whose goal is to hide an object.
        Note that you need to create an interaction for each object.
        
        b/ the second event is not used in the present demo.
    


Task : hide all the spheres by "point-and_clicking" them.
        This is very useful when creating some games.
    
Meaning of "convenience function": A convenience function is a non-essential 
        subroutine in a programming library or framework which is intended to 
        EASE commonly performed tasks (https://en.wikipedia.org/wiki/Convenience_function).
        PTVR convenience functions are in: ...\PTVR_Researchers\PTVR\blocks.py

'''

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
from PTVR import Blocks as blocks

import numpy as np
import PTVR.Stimuli.Color as color
from PTVR.Pointing.PointingCursor import PointingLaser, LaserContingency


def main():
    my_world = visual.The3DWorld()
    my_scene = PTVR.Stimuli.Scenes.VisualScene(
        are_both_hand_controllers_visible=True)

    # Parameters defining the visual aspects of the pointer
    my_laser_beam = PointingLaser(hand_laser=LaserContingency.RIGHT_HAND,
                                  laser_color=color.RGBColor(r=0.7, g=0.5),
                                  laser_width=0.01)
    my_scene.place_pointing_laser(my_laser_beam)

    # Parameters defining the pointing process
    my_activation_cone_origin_id = my_world.handControllerRight.id

    # Create a set of objects, each associated with an interaction
    number_of_objects = 10
    for i in range(number_of_objects):
        my_sphere = PTVR.Stimuli.Objects.Sphere(
            size_in_meters = 0.1,
            position_in_current_CS = np.array ([ -number_of_objects/2 + i, 1, 3] ) )
        # Events
        #
        #  Event highlighted in the present demo
        target_is_pointed_at_and_clicked, target_is_not_pointed_at = \
            blocks.PointAndClickEvent (
                my_sphere.id, my_scene, my_world,
                the_activation_cone_origin_id = my_activation_cone_origin_id
            )
        # Callbacks
        hide_sphere = callback.ChangeObjectVisibility(
            object_id = my_sphere.id
        )
        # Interactions
        # When my_sphere is pointed at AND
        # the handcontroller's trigger is subsequently pressed,
        # the callback hides the target.
        my_scene.AddInteraction(
            events = [target_is_pointed_at_and_clicked],
            callbacks = [hide_sphere])

        my_scene.place(my_sphere, my_world)

    my_world.add_scene(my_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
