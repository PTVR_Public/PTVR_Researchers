# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\events_and_callbacks_for_pointing\
    head_pointing_to_position_an_object.py
    
Goal of Demo:
    Illustrates how to change an object's position by pointing at this object
    with a pointing "device" (head, gaze, handcontroller, ...).
    Here, the pointing device is the headset but this can be changed with
    one line of code ( see activation_cone_origin_id = my_world.xxxxxx.id )
        Example 1: if you want hand-controller contingency, use:
            activation_cone_origin_id = my_world.handControllerRight.id
        Example 2: if you want gaze contingency, use:
            activation_cone_origin_id = my_world.eyeCombined.id    
    
What to do: 
    Move your head to point at the sphere WITHOUT ANY visible 
    head-contingent cursor.
    As soon as your head is accurately pointing at the sphere,
    the sphere's position changes... and is thus not pointed at anymore, which    
    induces a color change of the sphere.
    
    After this interaction, pointing at the sphere has not visible effect 
    any longer as the callback sets the coordinates at the same position in 
    the global Coordinate System ( here np.array ([-1, y_of_sphere, z_of_sphere]) )
    
Emphasis: 
        Your head movements are not accompanied by movements of a cursor.     
        In other words, there is no head-contingent cursor in this demo.

Note: you can decrease the difficulty of the pointing task by increasing
        the pointing_activation_disk_radius_deg parameter.
        


'''

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import numpy as np
import PTVR.Stimuli.Color as color
import PTVR.Data.Callbacks.MovementCallback as movementCallbacks

pointing_activation_disk_radius_deg = 5 # radius of the pointing activation disk (in degrees)
# increase this parameter to make pointing easier

z_of_sphere = 2 # in meters
y_of_sphere = 1 # in meters

def main():
    my_world = visual.The3DWorld ( )
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    my_target_sphere = PTVR.Stimuli.Objects.Sphere ( 
                        color = color.RGBColor ( 1, 0, 0, 1),
                        size_in_meters = 0.1,
                        position_in_current_CS = np.array ( [0, y_of_sphere, z_of_sphere] ) ) 

    # Interactions allowing us to point at objects
    ### Events and Callbacks must be created BEFORE interactions !
    ## Events
    target_is_pointed_at =  \
                event.PointedAt (
                    # What is the object that must be pointed at?
                    target_id = my_target_sphere.id, 
                    
                    # Id of the pointing device:
                    # Headset is the default (shown below for didactic emphasis)
                    activation_cone_origin_id = my_world.headset.id, # i.e. -3                                     
                    activation_cone_radius_deg = pointing_activation_disk_radius_deg 
                    )
                
    target_is_not_pointed_at = \
                event.PointedAt (
                    target_id = my_target_sphere.id,   
                    activation_cone_origin_id = my_world.headset.id, # i.e. -3                                                         
                    activation_cone_radius_deg = pointing_activation_disk_radius_deg,  
                    mode = "release" ) 
    ## Callbacks
    position_object_to_the_left  = movementCallbacks.SetCartesianCoordinatesInGlobalCS ( 
                                    object_id = my_target_sphere.id,
                                    position_in_global_CS = np.array([-1, y_of_sphere, z_of_sphere])                          
                                    )  
    change_object_color  = callback.ChangeObjectColor( 
                                    object_id = my_target_sphere.id,
                                    new_color=color.RGBColor (g=1)        )                          

    ## Standard Interactions
    # When my_target_sphere is pointed at, its color is changed to Blue
    my_scene.AddInteraction ( events = [ target_is_pointed_at ],
                              callbacks = [ position_object_to_the_left ] )
    
    # # When my_target_sphere is NOT pointed at, its color is changed back to Red (initial color) 
    my_scene.AddInteraction ( events = [ target_is_not_pointed_at ],
                              callbacks = [ change_object_color] )
    
    my_scene.place (my_target_sphere, my_world)  
    my_world.add_scene (my_scene)
    
    my_world.write() # Write The3DWorld to .json file
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()