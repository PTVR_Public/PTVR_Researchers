# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\
    scotoma_in_forest_with_spheres.py

Goal of this demo: 
        Show how to create a Monocular (or binocular) STANDARD PTVR scotoma.        
        The scotoma can be head-, gaze- or eye- contingent, or hand-contingent.
        
        The scene is a forest* with trees at different distances to observe
        the scotoma's behaviour when its pointing cone encounters objects or
        nothing.
        * This forest is a PTVR object modified from the following free asset:
        https://assetstore.unity.com/packages/3d/environments/landscapes/low-poly-simple-nature-pack-162153
        
        Three spheres are added to the Forest.    
        Note that the scotoma and the 3 spheres subtend the same angular value
        for the 3 spheres distances.
        
          By default, a PTVR scotoma is always rendered through any object
        with its position calculated to be close to the object's center (See PTVR
        Documentation -> User Manual -> Pointing at an object). 
        Thus, the similarity in position (depth) of the object and the scotoma 
        render their binocular disparity similar thus avoiding double vision.
        This default behaviour occurs when 'is_distance_constant' is set to 
        False (in ImageToContingentCursor() )
          Another aspect of this default behaviour emerges when the scotoma's pointing 
        cone is empty, i.e. when no object lies within the pointing cone. In 
        this case, the scotoma is set to a distance that you can choose. This
        distance is set to 500 m by default ('distance_if_empty_cursor_cone'=500).
           
What you can play around with:
        You can set a constant distance for the scotoma (thus changing its
        default behaviour).
        For instance, if you want to have the scotoma at 2 meters from the head,
        gaze or eye you have to modify the two following parameters of 
        ImageToContingentCursor :
            is_distance_constant = True
            constant_distance = 2
            If you do as said above, then the scotoma will not be visible
        when it is behind the red sphere (the spheres are displayed at different 
        distances from the CS's origin).
        
Note 1: The scotoma belongs to the PTVR category called "flat cursors"
        (reminder: a reticle also belongs to the "flat cursors" category).

 Note 2: When visually controlling angular values (here of the scotoma
        and spheres ), remember that 
         angular values are accurate only when the eyes lie
         close to the current Coordinate System's origin. Move your head if it
         is not the case, you can use the Gizmo below your feet: if your head is
         aligned with the green axis (Y), it will be fine.

"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere, Forest, Gizmo
import PTVR.Stimuli.Color as color
import numpy as np
from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ScotomaImageFromDrawing as Scotoma

from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Tools as tools

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
subject_name = "eric"
height_of_viewpoint_in_m = 1.0 # this value is chosen so as to correspond to the height of the subject's headset during
if subject_name == "carlos": height_of_viewpoint_in_m = 1.3
if subject_name == "eric": height_of_viewpoint_in_m = 1.2
my_viewpoint_3d_vector = np.array ([0, height_of_viewpoint_in_m, 0]) 

my_contingency = ImageContingency.HEADSET 
    # ImageContingency.HEADSET, ImageContingency.LEFT_EYE, 
    # ImageContingency.RIGHT_EYE or ImageContingency.GAZE. 
    
my_eye_w_scotoma = "both" # "right" or  "left" or "both"

#####  SCOTOMA PARAMETERS ######
# Geometry and Display parameters 
scotoma_x_size_in_degrees = 10
scotoma_y_size_in_degrees = 10
sphere_diameter_deg = scotoma_x_size_in_degrees

###### SCENE OBJECTS #######
red_sphere_color = color.RGBColor(1,0,0)
red_sphere_radial_distance = 1 # meters
red_sphere_diameter_m = tools.visual_angle_to_size_on_perpendicular_plane (
    visual_angle_of_centered_object_in_deg = sphere_diameter_deg , 
    viewing_distance_in_m = red_sphere_radial_distance)

green_sphere_color = color.RGBColor(0,1,0)
green_sphere_radial_distance = 3 # meters
green_sphere_diameter_m = tools.visual_angle_to_size_on_perpendicular_plane (
    visual_angle_of_centered_object_in_deg = sphere_diameter_deg , 
    viewing_distance_in_m = green_sphere_radial_distance)

blue_sphere_color = color.RGBColor(0,0,1)
blue_sphere_radial_distance = 6 # meters
blue_sphere_diameter_m = tools.visual_angle_to_size_on_perpendicular_plane (
    visual_angle_of_centered_object_in_deg = sphere_diameter_deg , 
    viewing_distance_in_m = blue_sphere_radial_distance)
# =============================================================================
#                              END OF PARAMETERS                                 #
# =============================================================================
 

def create_scotoma ( scene_name ):
    # internally create a PNG file to create an image
    scotoma_2D_image = Scotoma.ScotomaImageFromDrawing ( )
    #scotoma_2D_image.Show() #  uncomment this line to show the image of the 
    # scotoma in a new window of your PC
    
    my_scotoma = ImageToContingentCursor (
                contingency_type = my_contingency,
                image = scotoma_2D_image,
                eye_with_contingent_cursor = my_eye_w_scotoma,
                size_in_degrees = [scotoma_x_size_in_degrees, scotoma_y_size_in_degrees] )
    scene_name.place_contingent_cursor (my_scotoma)
# END of create_scotoma ()

def create_spheres (scene_name):
    red_sphere = Sphere (color = red_sphere_color, size_in_meters = red_sphere_diameter_m)
    red_sphere.set_perimetric_coordinates (
                        radialDistance = red_sphere_radial_distance, 
                        eccentricity = 30, halfMeridian = 180 )
    green_sphere = Sphere (color = green_sphere_color, size_in_meters = green_sphere_diameter_m)
    green_sphere.set_perimetric_coordinates (
                        radialDistance = green_sphere_radial_distance, 
                        eccentricity = 0, halfMeridian = 0 )
    blue_sphere = Sphere (color = blue_sphere_color, size_in_meters = blue_sphere_diameter_m )
    blue_sphere.set_perimetric_coordinates (
                        radialDistance = blue_sphere_radial_distance, 
                        eccentricity = 30, halfMeridian = 0 )  
    scene_name.place (red_sphere, my_world)
    scene_name.place (green_sphere, my_world)
    scene_name.place (blue_sphere, my_world)    
# END of create_spheres ()    

my_world = The3DWorld ()
def main():
    my_scene = VisualScene (skybox= "BrightMorning") 
        # Night, BrightMorning, Cloudy, Afternoon, or Sunset
    
    # Gizmo at the origin of the Global Coordinate System
    my_gizmo = Gizmo ()
    my_scene.place (my_gizmo, my_world)
    
    my_forest = Forest ()
    my_scene.place (my_forest, my_world)
    
    # translate the global CS upwards
    # the Angular values of the subsequently created objects are correct
    # when the headset is close to my_viewpoint_3d_vector
    my_world.translate_coordinate_system_along_global (my_viewpoint_3d_vector) 

    create_scotoma (my_scene)    
    create_spheres (my_scene)
    
    my_world.add_scene (my_scene)
    my_world.write ()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
