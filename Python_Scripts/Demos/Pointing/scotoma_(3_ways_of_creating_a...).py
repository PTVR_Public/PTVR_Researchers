# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\
    scotoma_(3_ways_of_creating_a...).py

Goal: show three ways of creating a scotoma corresponding to three different goals.
        The 3 scotomas are simultaneously displayed close together with 
        different perimetric coordinates (wrt pointer). 
        The scotomas are Head-contingent. This contingency can be changed by modifying the
        the 'my_contingency' parameter.
     
Note : The scotoma belongs to the PTVR category called "flat cursors". 
     
"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere, Cube
import PTVR.Stimuli.Color as color
import numpy as np
from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ScotomaImageFromDrawing as Scotoma
import PTVR.Pointing.ImageFromDrawing as SFD
import PTVR.Pointing.ImageFromLoading as SFL

from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Tools as tools

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================


my_contingency = ImageContingency.GAZE 
    # ImageContingency.HEADSET, 
    # ImageContingency.LEFT_EYE, ImageContingency.RIGHT_EYE, ImageContingency.GAZE,
    # ImageContingency.LEFT_HAND, or ImageContingency.RIGHT_HAND. 

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

# the simplest experiment 
my_world = The3DWorld ()
def main():
    my_world.translate_coordinate_system_along_global ( np.array([0, 1.2, 0]))
    my_scene = VisualScene (skybox="Night")
    
    ##########
    # Goal 1 : very quickly create a red transparent standard scotoma
    my_scotoma_image = Scotoma.ScotomaImageFromDrawing (
                                scotoma_color=color.RGBColor(r=1, a=0.3) )
    my_scotoma_cursor = ImageToContingentCursor ( 
                            image =  my_scotoma_image,
                            contingency_type = my_contingency) 
    my_scene.place_contingent_cursor (my_scotoma_cursor)
    
    ##########
    # Goal 2 : create a scotoma with one of the preset shapes available
    # with ImageFromDrawing and change its perimetric coordinates wrt pointing
    # axis.
    my_scotoma_image_2 = SFD.ImageFromDrawing (
                                color = color.RGBColor ( b = 0.7),
                                shape = "rectangle", fill = True )
    my_scotoma_cursor_2 = ImageToContingentCursor ( 
                                image =  my_scotoma_image_2, 
                                contingent_cursor_ecc_hm = 
                                    np.array ( [ 7, 90 ]),  # 7° above 
                                contingency_type = my_contingency   )    
    my_scene.place_contingent_cursor (my_scotoma_cursor_2)
    
    ##########
    # Goal 3 : create a scotoma by loading a png file 
    # with ImageFromLoading (a disk scotoma with a central functional
    # island) and change its perimetric coordinates wrt pointing
    # axis.
    my_scotoma_image_3 = SFL.ImageFromLoading (
                                 image_file = "full_scotoma_hole.png" )
    my_scotoma_cursor_3 = ImageToContingentCursor ( 
                                image =  my_scotoma_image_3, 
                                contingent_cursor_ecc_hm = 
                                    np.array ( [ 7, 270 ]),  # 7° below,
                                contingency_type = my_contingency    )    
    my_scene.place_contingent_cursor (my_scotoma_cursor_3)    
    
    my_world.add_scene (my_scene)
    my_world.write ()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
