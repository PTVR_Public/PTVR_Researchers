# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\
    reticle_test_angular_size.py
    
(see also ...\PTVR_Researchers\Python_Scripts\Demos\Objects\angular_size_of_object.py )

Goal: show how to specify the angular size of a reticle and
        check this angular value against visual objects.

1st test: the angular size of the two objects is 2 deg of visual angle, 
    because the cube's size is set to 0.04 m at 1.15 m which 
    mathematically induces a 2 deg angular size.
    
2nd test:   the reticle, whose cursor cone diameter is set to 2 deg,
            is thus visually superimposed with the 2 deg-diameter sphere.

Note 1: When visually controlling angular values, remember that 
        angular values are accurate only when the eyes lie
        close to the current Coordinate System's origin. Move your head if it
        is not the case, you can use the Gizmo below your feet: if your head is
        aligned with the green axis (Y), i.e. just above the CS' origin, 
        it will be fine.
        

Note 2: In this demo, the distance between head and reticle is intentionnally
        constant and smaller than the distance between head and object.
        Make sure your head is close to the CS's origin, otherwise the
        reticle will disappear within the objects if your head is too close
        to the objects.
  
Note 3: The reticle belongs to the PTVR category called "flat cursors". 
"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere, Cube, Gizmo
import PTVR.Stimuli.Color as color
import numpy as np
from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ReticleImageFromDrawing as RG
from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Tools as tools

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

#####  RETICLE PARAMETERS ######
 
my_reticle_diameter_deg = 2 # diameter: 2 degrees

my_contingency = ImageContingency.HEADSET 
    # ImageContingency.HEADSET, 
    # ImageContingency.LEFT_EYE, ImageContingency.RIGHT_EYE, ImageContingency.GAZE,
    # ImageContingency.LEFT_HAND, or ImageContingency.RIGHT_HAND.  
my_eye_w_reticle = "both" # "right" or  "left" or "both"

###### Two objects having the same angular size but ... #######
###### constructed in different ways #######
# Red sphere
# 2 deg at 1.15 m correspond approx. to 0.04 m
sphere_diameter_in_deg = 2         # 2
sphere_radial_distance_in_m = 1.15 # 1.15

# Calculate size (in meters) from visual angle and viewing distance.
# Calculation below returns 0.04 m for sphere_diameter_in_m
sphere_diameter_in_m =  \
    tools.visual_angle_to_size_on_perpendicular_plane (
        visual_angle_of_centered_object_in_deg = sphere_diameter_in_deg, 
        viewing_distance_in_m = sphere_radial_distance_in_m )

# cube with size specified directly in meters
# to correspond to the angular calculation above 
cube_size_in_m = 0.04
cube_radial_distance_in_m = 1.15

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

# the simplest experiment 
my_world = The3DWorld ()
def main():
    my_scene = VisualScene ()
    
    # Gizmo at the origin of the Global Coordinate System
    my_gizmo = Gizmo ( )
    my_scene.place (my_gizmo, my_world)
    
    my_world.translate_coordinate_system_along_global ( np.array([0, 1.2, 0]))
    
    # internally create a PNG file to create an image
    reticle_2D_image = RG.ReticleImageFromDrawing ( )
    # reticle_2D_image.Show() #  uncomment this line to show the image of the 
    # reticle in a new window of your PC

    head_reticle_distance = sphere_radial_distance_in_m - (5*sphere_diameter_in_m)
    
    my_reticle = ImageToContingentCursor (
                image = reticle_2D_image,
                eye_with_contingent_cursor = my_eye_w_reticle,
                is_distance_constant = True, 
                constant_distance = head_reticle_distance,
                size_in_degrees= [my_reticle_diameter_deg, my_reticle_diameter_deg]  )
    
    my_scene.place_contingent_cursor (my_reticle)     
    
    ## Creation of objects placed at equivalent radial distances
    # sphere
    my_sphere = Sphere ( size_in_meters = sphere_diameter_in_m )
    my_sphere.set_perimetric_coordinates (eccentricity = 1, halfMeridian = 180,
                                          radialDistance = sphere_radial_distance_in_m)
    my_scene.place ( my_sphere, my_world) 
    
    # cube
    my_cube = Cube ( size_in_meters = cube_size_in_m )
    my_cube.set_perimetric_coordinates (eccentricity = 1, halfMeridian = 0,
                                          radialDistance = cube_radial_distance_in_m)    
    my_scene.place ( my_cube, my_world)   
    
    my_world.add_scene (my_scene)
    my_world.write ()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
