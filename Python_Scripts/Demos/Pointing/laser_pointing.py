# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\laser_pointing.py


Goal : illustrate the use of the Pointing Laser.
        

What you see: if you point at the red sphere (sphere_1) with the laser beam, 
            it will turn blue. This an INTERACTION between an EVENT and a
            CALLBACK.

"""



from PTVR.Visual import The3DWorld
from  PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere
import PTVR.Stimuli.Color as color
import numpy as np
from PTVR.Pointing.PointingCursor import  PointingLaser,LaserContingency
from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Data.Callback 
import PTVR.Data.Event
# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

#### Laser parameters
my_laser_color = color.RGBColor ( r = 0.7, g = 0.5 )
hand = LaserContingency.RIGHT_HAND

###### SCENE OBJECTS #######
sphere_size = np.array ([1,1,1])

sphere_1_color = color.RGBColor (1,0,0)
sphere_1_pos = np.array ([-2,0,3])
sphere_2_color = color.RGBColor (0,1,0)
sphere_2_pos = np.array ([0,0,5])
sphere_3_color = color.RGBColor (0,0,1)
sphere_3_pos = np.array ([2,0,7])
new_color = color.RGBColor (b = 1, g = 1)
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================
 
my_world = The3DWorld ()

def main():

    if(hand == LaserContingency.RIGHT_HAND):
        is_right_controller_visibility = True
        is_left_controller_visibility = False
        my_activation_cone_origin_id = my_world.handControllerRight.id
    if(hand == LaserContingency.LEFT_HAND):
        is_left_controller_visibility = True
        is_right_controller_visibility = False
        my_activation_cone_origin_id = my_world.handControllerLeft.id
        
    myScene = VisualScene (is_left_hand_controller_visible = is_left_controller_visibility,
                           is_right_hand_controller_visible = is_right_controller_visibility)
  
    # Create a pointer
    my_laser_beam =  PointingLaser ( hand_laser = hand, 
                                     laser_color = my_laser_color,
                                     laser_width = 0.01                                       )
    myScene.place_pointing_laser ( my_laser_beam )
    
    # Creation of Spheres
    sphere_1 = Sphere (size_in_meters = sphere_size, color = sphere_1_color,
                       position_in_current_CS = sphere_1_pos)
    sphere_2 = Sphere (size_in_meters = sphere_size, color = sphere_2_color,
                       position_in_current_CS = sphere_2_pos)
    sphere_3 = Sphere (size_in_meters = sphere_size, color = sphere_3_color,
                       position_in_current_CS = sphere_3_pos)
    
    myScene.place (sphere_1, my_world)
    myScene.place (sphere_2, my_world)
    myScene.place (sphere_3, my_world)
    
    ## Interactions
    # Events
    sphere_1_is_pointed_at = PTVR.Data.Event.PointedAt ( target_id = sphere_1.id, 
                                    activation_cone_origin_id = my_activation_cone_origin_id,
                                    activation_cone_radius_deg = 0.01)
    
    sphere_1_is_not_pointed_at = PTVR.Data.Event.PointedAt ( target_id = sphere_1.id, 
                                    activation_cone_origin_id = my_activation_cone_origin_id, 
                                    activation_cone_radius_deg = 0.01, mode = "release")
    # Callbacks
    change_sphere_1_color = PTVR.Data.Callback.ChangeObjectColor ( 
                                                    object_id = sphere_1.id, 
                                                    new_color = new_color )
    change_sphere_1_color_back = PTVR.Data.Callback.ChangeObjectColor ( 
                                                    object_id = sphere_1.id,
                                                    effect = "deactivate" )

    myScene.AddInteraction (events = [sphere_1_is_pointed_at],
                            callbacks = [change_sphere_1_color] )
    myScene.AddInteraction (events = [sphere_1_is_not_pointed_at],
                            callbacks = [change_sphere_1_color_back] )
    
    my_world.add_scene (myScene)
    my_world.write()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
 
