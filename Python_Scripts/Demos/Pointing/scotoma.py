# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\scotoma.py

Goal of this demo: 
        Show the simplest way of creating a standard  
        contingent scotoma.
        By default, the scotoma is head-contingent (i.e. it moves with
        your head) - this is also known as being head-controlled.
        
        You can make the scotoma contingent on gaze by setting:
            my_contingency = ImageContingency.GAZE
        
Note : The scotoma belongs to the PTVR category called "flat cursors". 

"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
#from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ScotomaImageFromDrawing as Scotoma
from PTVR.Pointing.PointingCursor import ImageToContingentCursor, ImageContingency
from PTVR.SystemUtils import LaunchThe3DWorld

my_contingency = ImageContingency.HEADSET
    # ImageContingency.HEADSET, 
    # ImageContingency.LEFT_EYE, ImageContingency.RIGHT_EYE, ImageContingency.GAZE,
    # ImageContingency.LEFT_HAND, or ImageContingency.RIGHT_HAND.
    
my_world = The3DWorld ()
def main():
    # the simplest scene
    my_scene = VisualScene (skybox = "Cloudy") # Night BrightMorning Afternoon Sunset

    # internally create a PNG file to create an image
    scotoma_2D_image = Scotoma.ScotomaImageFromDrawing ()
    # If you want to open a window on your PC showing the scotoma image 
    # that has just been created, comment out the following line.
    #reticle_2D_image.Show()
    
    # transform 'scotoma_2D_image' into a contingent cursor.
    my_scotoma = ImageToContingentCursor ( image = scotoma_2D_image,
                        contingency_type = my_contingency)
    
    my_scene.place_contingent_cursor (my_scotoma)
    
    my_world.add_scene (my_scene)
    my_world.write ()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
