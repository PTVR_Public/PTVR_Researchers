# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\
    scotoma_with_text_on_tangent_screen.py

Goal: simulates the effect of reading text with an artificial 
    gaze-contingent scotoma.
    
    
Modifying some parameters:
    You can increase the size of the characters by increasing 
    the angular_x_height variable. You will thus feel why increasing size
    in general helps people with some visual deficiencies.
    
    You can also decrease the masking strength of the scotoma by making it
    slightly transparent: try for instance 'alpha_transparency = 0.9'
    

"""
import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ScotomaImageFromDrawing as Scotoma
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

subject_name = "eric"
height_of_viewpoint_in_m = 1.0 # this value is chosen so as to correspond to the height of the subject's headset during
if subject_name == "carlos": height_of_viewpoint_in_m = 1.3
if subject_name == "eric": height_of_viewpoint_in_m = 1.2

angular_x_height = 0.6 # 0.6: angular size of characters
alpha_transparency = 1 # controls the scotoma's "transparency"
# 1 -> scotoma is absolute (i.e. full blindness in the scotoma)
# 0.9 -> scotoma is less absolute

height_of_head =  1.0 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_viewpoint_in_m , 0])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint

tangent_screen_position = np.array ([0, 0, 100])

my_contingency = ImageContingency.GAZE
    # ImageContingency.HEADSET, 
    # ImageContingency.LEFT_EYE, ImageContingency.RIGHT_EYE, ImageContingency.GAZE,
    # ImageContingency.LEFT_HAND, or ImageContingency.RIGHT_HAND.

def main():
    my_world = visual.The3DWorld ( )
    my_world.translate_coordinate_system_along_global (
                                translation = viewpoint_position )
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ( background_color = color.RGBColor(0.74,0.76,0.78,1.0)) 
    my_world.add_scene ( my_scene )
    
    #######
    # Text on Tangent Screen                
    my_tangent_screen = PTVR.Stimuli.Objects.TangentScreen (
                            color=color.RGBColor(r=0.6, g= 0.6, b=0.6),
                            position_in_current_CS = tangent_screen_position,
                            size_in_meters=np.array([40, 40]),
                            ) 
    my_scene.place (my_tangent_screen, my_world)       
        
    text_object_1 = PTVR.Stimuli.Objects.Text ( 
                                    text = 'What else !',
                                    is_bold = True,
                                    visual_angle_of_centered_x_height_deg = 
                                        angular_x_height) 
    text_object_1.set_perimetric_coordinates_on_screen (
                    tangentscreen = my_tangent_screen)      
    my_scene.place (text_object_1, my_world)
    
    #########    
    # Scotoma
    scotoma_2D_image = Scotoma.ScotomaImageFromDrawing (
        scotoma_color = color.RGBColor(r= 0, g=0, b=0, a = alpha_transparency))
    my_scotoma = ImageToContingentCursor ( image = scotoma_2D_image,
                        contingency_type = my_contingency,
                        size_in_degrees= [12, 12 ] )
    
    my_scene.place_contingent_cursor (my_scotoma)
    
    my_world.write()  # Write the experiment to .json file

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld()