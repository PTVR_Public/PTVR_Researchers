# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\
    reticle_in_forest_with_spheres.py

Goal of this demo: 
        Show how to create a Monocular (or binocular) STANDARD PTVR reticle. 
        Note that there is no pointing interaction between the reticle and the
        spheres or objects (i.e. no callback is called when an object is
                            pointed at).
        
        The reticle can be head-, gaze- or eye- contingent, or hand-contingent.
        
        The scene is a forest* with trees at different distances to observe
        the reticle's behaviour when its pointing cone encounters objects or
        nothing.
        
        * This forest is a PTVR object modified from the following free asset:
        https://assetstore.unity.com/packages/3d/environments/landscapes/low-poly-simple-nature-pack-162153
        
          By default, a PTVR reticle is always rendered through any object
        with its position calculated to be close to the object's center (See PTVR
        Documentation -> User Manual -> Pointing at an object). 
        Thus, the similarity in position of the object and the reticle render
        their binocular disparity similar thus avoiding double vision.
        This default behaviour occurs when 'is_distance_constant' is set to 
        False (in ImageToContingentCursor() )
          Another aspect of this default behaviour emerges when the reticle's pointing 
        cone is empty, i.e. when no object lies within the pointing cone. In 
        this case, the reticle is set to a distance that you can choose. This
        distance is set to 500 m by default ('distance_if_empty_cursor_cone'=500).
           
What you can play around with:
        You can set a constant distance for the reticle (thus changing its
        default behaviour).
        For instance, if you want to have the reticle at 2 meters from the head,
        gaze, eye, or hand, you have to modify the two following parameters of 
        ImageToContingentCursor :
            is_distance_constant = True
            constant_distance = 2
            If you do as defined above, then the reticle will not be visible
        when it is behind the red sphere (the spheres are displayed at different 
        distances from the CS's origin).
        
Note : The reticle belongs to the PTVR category called "flat cursors"
        (reminder: a scotoma also belongs to the "flat cursors" category).

"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere, Forest, Sprite
import PTVR.Stimuli.Color as color
import numpy as np
from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ReticleImageFromDrawing as RG

from PTVR.SystemUtils import LaunchThe3DWorld

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

my_contingency = ImageContingency.HEADSET 
    # ImageContingency.HEADSET, 
    # ImageContingency.LEFT_EYE, ImageContingency.RIGHT_EYE, 
    # ImageContingency.GAZE,
    # ImageContingency.LEFT_HAND, or ImageContingency.RIGHT_HAND. 
    
my_eye_w_reticle = "both" # "right" or  "left" or "both"

###### SCENE OBJECTS #######

sphere_1_color = color.RGBColor(1,0,0)
sphere_1_pos = np.array([-1,0,1])
sphere_2_color = color.RGBColor(0,1,0)
sphere_2_pos = np.array([0,0,3])
sphere_3_color = color.RGBColor(0,0,1)
sphere_3_pos = np.array([1,0,5])
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================
 

def create_reticle (scene_name):
    # internally create a PNG file to create an image
    reticle_2D_image = RG.ReticleImageFromDrawing ( 
                    reticle_inner_diameter_as_ratio = 0.3,
                    reticle_line_width_as_ratio=0.1 )
    #reticle_2D_image.Show() #  uncomment this line to show the image of the 
    # reticle in a new window of your PC
    
    my_reticle = ImageToContingentCursor (
                contingency_type = my_contingency,
                image = reticle_2D_image,
                eye_with_contingent_cursor = my_eye_w_reticle,
                is_distance_constant = False, # False by default
                constant_distance = 2    )    
    scene_name.place_contingent_cursor (my_reticle)  
# END of create_reticle ()    

def create_spheres (scene_name):
    sphere_1 = Sphere ( color = sphere_1_color,
                        position_in_current_CS = sphere_1_pos)
    sphere_2 = Sphere ( color = sphere_2_color,
                        position_in_current_CS = sphere_2_pos)
    sphere_3 = Sphere ( color = sphere_3_color,
                        position_in_current_CS = sphere_3_pos)
    scene_name.place (sphere_1, my_world)
    scene_name.place (sphere_2, my_world)
    scene_name.place (sphere_3, my_world)
# END of create_spheres () 

    
my_world = The3DWorld ()
def main():
    # the simplest scene
    my_scene = VisualScene (skybox= "Sunset", side_view_on_pc = False) 
        # Night, BrightMorning, Cloudy, Afternoon, or Sunset
    
    create_reticle (my_scene)
    create_spheres (my_scene)
    
    my_forest = Forest ()
    my_scene.place (my_forest, my_world)
    
    my_sprite = Sprite (image_file ="cat.png",
                    position_in_current_CS=np.array([3, 3, 0.0]) ,
                    rotation_in_current_CS=np.array([0.0, 90, 0.0]))
    my_scene.place (my_sprite, my_world)
    
    my_world.add_scene (my_scene)
    my_world.write ()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
