# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\reticle.py

Goal of this demo: 
        Show the simplest way of creating a standard PTVR 
        contingent reticle.
        By default, the reticle is head-contingent (i.e. it moves with
        your head) - this is also known as being head-controlled.
        
        You can make the reticle contingent on gaze by setting:
            my_contingency = ImageContingency.GAZE
        
Note : The reticle belongs to the PTVR category called "flat cursors". 

"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ReticleImageFromDrawing as RG

from PTVR.SystemUtils import LaunchThe3DWorld

my_contingency = ImageContingency.HEADSET
    # ImageContingency.HEADSET, 
    # ImageContingency.LEFT_EYE, ImageContingency.RIGHT_EYE, ImageContingency.GAZE,
    # ImageContingency.LEFT_HAND, or ImageContingency.RIGHT_HAND.

# the simplest experiment 
my_world = The3DWorld ()
def main():
    # the simplest scene
    my_scene = VisualScene (skybox = "Night") # Night BrightMorning Afternoon Sunset

    reticle_2D_image = RG.ReticleImageFromDrawing ( )    
    # If you want to open a window on your PC showing the reticle image 
    # that has just been created, comment out the following line.
    #reticle_2D_image.Show()
    
    # transform 'reticle_2D_image' into a contingent cursor.
    my_reticle = ImageToContingentCursor ( 
                        image = reticle_2D_image,
                        contingency_type = my_contingency)
    
    my_scene.place_contingent_cursor (my_reticle)
    
    my_world.add_scene (my_scene)
    my_world.write ()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
