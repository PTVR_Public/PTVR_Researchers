# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Pointing\
    reticle_(3_ways_of_creating_a...).py

Goal: show three ways of creating a reticle corresponding to three different goals.
        The 3 reticles are simultaneously displayed close together with 
        different perimetric coordinates. 
        They are Head-contingent. This contingency can be changed by modifying the
        'my_contingency' parameter.
     
Note 1 : The reticle belongs to the PTVR category called "flat cursors". 

Note 2: 
    when using ImageFromLoading (),
    the default is to use a file that is stored in: 
        ...\PTVR_Researchers\PTVR_Operators\resources\Images\
    However, you can use the 'path_to_image_folder' parameter to get files
    that are stored in paths outside of ...\PTVR_Researchers.
     
"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere, Cube
import PTVR.Stimuli.Color as color
import numpy as np
from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ReticleImageFromDrawing as RG
import PTVR.Pointing.ImageFromDrawing as SFD
import PTVR.Pointing.ImageFromLoading as SFL

from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Tools as tools

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
my_contingency = ImageContingency.HEADSET 
    # ImageContingency.HEADSET, 
    # ImageContingency.LEFT_EYE, ImageContingency.RIGHT_EYE, ImageContingency.GAZE,
    # ImageContingency.LEFT_HAND, or ImageContingency.RIGHT_HAND. 

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

# the simplest experiment 
my_world = The3DWorld ()
def main():
    my_world.translate_coordinate_system_along_global ( np.array([0, 1.2, 0]))
    my_scene = VisualScene ()
    
    ##########
    # Goal 1 : very quickly create the standard PTVR reticle
    my_reticle_image = RG.ReticleImageFromDrawing ()
    # If you want to open a window on your PC showing the reticle image 
    # that has just been created, comment out the following line.
    #my_reticle_image.Show()    
    my_reticle_cursor = ImageToContingentCursor ( 
                            image =  my_reticle_image,
                            contingency_type = my_contingency) 
    # you could also do the same as the 2 lines above with the following line:
    # my_reticle_cursor = ImageToContingentCursor( image = RG.ReticleImageFromDrawing () )    
    my_scene.place_contingent_cursor (my_reticle_cursor)
    
    ##########
    # Goal 2 : create a reticle with one of the preset shapes available
    # with ImageFromDrawing and change its perimetric coordinates wrt pointing
    #  axis.
    my_reticle_image_2 = SFD.ImageFromDrawing (
                                color = color.RGBColor ( g = 0.7),
                                shape = "rectangle", line_width_as_ratio = 0.05 )   
    my_reticle_cursor_2 = ImageToContingentCursor ( 
                                image =  my_reticle_image_2, 
                                contingent_cursor_ecc_hm = 
                                    np.array ( [ 5, 90 ]),  # 5° above 
                                contingency_type=my_contingency   )    
    my_scene.place_contingent_cursor (my_reticle_cursor_2)
    
    ##########
    # Goal 3 : create a reticle by loading a blue arrow png file 
    # with ImageFromLoading and change its perimetric coordinates wrt pointing
    # axis.
    my_reticle_image_3 = SFL.ImageFromLoading (
                    #path_to_image_folder = r"C:\Users\Eric_Castet",
                    # you can uncomment the line above to enter the path on
                    # your PC to your own image files.
                    image_file = "mouse_cursor_blue_arrow.png" )
    my_reticle_cursor_3 = ImageToContingentCursor ( 
                                image =  my_reticle_image_3, 
                                contingent_cursor_ecc_hm = 
                                    np.array ( [ 5, 270 ]),  # 5° below,
                                contingency_type = my_contingency    )    
    my_scene.place_contingent_cursor (my_reticle_cursor_3)    
    
    my_world.add_scene (my_scene)
    my_world.write ()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
