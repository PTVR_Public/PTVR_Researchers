 
# -*- coding: utf-8 -*-
"""
...\Demos\Experiment_building\30_simplest_yes_no_experiment.py

Description :
    Same as : 
    ...\Demos\Experiment_building\10_loop_of_trials_w_one_scene_per_trial.py
    Except that the subject has to press
    either the return key for "yes"
    or
    the space bar for  "no"
    to go to the next trial

"""

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import numpy as np
import random



nb_of_trials = 5
   
def main():

    my_world = visual.The3DWorld (name_of_subject = "toto" )

    for i in range (0, nb_of_trials):
        trial_scene = PTVR.Stimuli.Scenes.VisualScene (trial = i+1)
        trial_text = PTVR.Stimuli.Objects.Text ( 
            text = "trial : " + str (trial_scene.trial) + " / " + str (nb_of_trials) +
                "\nPress ENTER key for 'yes' or SPACE bar for 'no'",
            fontsize_in_postscript_points= 200,
            position_in_current_CS = np.array ( [0, 1, 1]) )
        # Create interaction for the scene         
        my_response_yes =  event.Keyboard ( valid_responses = ['enter'], mode = "press", 
                                               event_name = "yes" )     
        my_response_no  =  event.Keyboard ( valid_responses = ['space'], mode = "press", 
                                           event_name = "no"  )     
 
        STOP_current_scene = callback.EndCurrentScene ()    
        
        trial_scene.AddInteraction ( 
                events = [my_response_yes, my_response_no], 
                callbacks = [STOP_current_scene])       
        trial_scene.place ( trial_text,my_world)
        
        # the next two lines add my_variable in the results file
        my_variable = random.randrange(1,10)
        trial_scene.fill_in_results_file_column("my_variable", my_variable)  
        
        my_world.add_scene ( trial_scene )
        
    my_world.write() # Write experiment into .json file
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld ()