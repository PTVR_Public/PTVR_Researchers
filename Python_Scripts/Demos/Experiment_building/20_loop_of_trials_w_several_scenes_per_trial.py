 # -*- coding: utf-8 -*-
"""
...\Demos\Experiment_building\20_loop_of_trials_w_several_scenes_per_trial.py

Description :
    Loop of trials with several scenes per trial.
    Here, all the scenes of a given trial are given the same trial rank at the scene
    creation ( see the VisualScene () fonction ). This trial rank will be
    visible in the results file.
    
Note: 
    you can see in the console the path of the directory where the results are
automatically saved. Have a look at the results file (in csv format) in this 
directory.

"""

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback

nb_of_trials = 2
nb_of_scenes_per_trial = 3
   
def main():

    my_world = visual.The3DWorld ( name_of_subject = "toto" )

    for i in range (0, nb_of_trials):
        for j in range(0, nb_of_scenes_per_trial):
            
            # all the scenes of the ith trial are given the same trial rank 
            trial_scene = PTVR.Stimuli.Scenes.VisualScene (trial = i+1)
            
            trial_text = PTVR.Stimuli.Objects.Text ( text = "trial : " + 
                        str (trial_scene.trial) + ", Scene : " + str (j+1) + 
                        "\nPress space bar to continue",
                        fontsize_in_postscript_points= 200
                    )
            trial_text.set_cartesian_coordinates (x = -0.5 + j, y=1.2, z = 3 - (j * 0.1 * 2 )  )           
            
            # Create interaction for the scene
            my_event_space_bar_press =  event.Keyboard ( valid_responses = ['space'], 
                                                        mode = "press")     
            my_callback_current_scene_end = callback.EndCurrentScene (  )       
            trial_scene.AddInteraction ( events = [my_event_space_bar_press ], 
                                        callbacks = [my_callback_current_scene_end])
            
            trial_scene.place ( trial_text,my_world)
            
            my_world.add_scene ( trial_scene )
        
    my_world.write() # Write experiment to .json file
    
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld ()