# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Experiment_building\40_succession_of_blocks_w_trials.py

This demo shows how to display a succession of experimental blocks:
    you can modify 'number_of_blocks'.

Each block contains a succession of trials :
    you can modify the 'number_of_trials' in the blocks.
    
Each block is preceded by a scene which is used to wait for a subject's click
before launching the block of trials. This 'block launch scene' does not
count as a trial (check this in the results file).
    
This demo also illustrates (see the save_experiment_data() fonction) how to 
        save parameters of interest that are not automatically saved by PTVR.
        

Note: 
    you can see in the console the path of the directory where the results are
automatically saved. Have a look at the results file (in csv format) in this 
directory.        


"""
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects 
import PTVR.Stimuli.Color as color 
import PTVR.Data.Event as event 
import PTVR.Data.Callback as callback 
import PTVR.Pointing.PointingCursor
import numpy as np 

number_of_blocks = 2
number_of_trials  = 2 # in EACH block

headset_pos_y = 1.2
z_coordinate = 2

my_world = visual.The3DWorld ()

# event
click = event.Keyboard (valid_responses = ['space'], event_name="space bar press" )
# callback
end_scene = callback.EndCurrentScene ()

############################
def save_experiment_data(
          scene = "",
          block_i = "",          
          scene_description = ""):
    """
    For each scene, save two parameters that are not saved automatically by PTVR, 
    namely the rank of block and a scene description.
    """

    scene.fill_in_results_file_column (column_name = "block_i", value = block_i)
    scene.fill_in_results_file_column (column_name = "scene_description", 
                                       value = scene_description)   

# End of def save_experiment_data(scene,block_i,trial_i,scene_description):
#-----------------------------------------------------------------------    
    
##########################################
def create_block_launch_scene (block_i): # this scene does not count as a trial
    block_launch_scene = PTVR.Stimuli.Scenes.VisualScene ( 
        background_color = color.RGBColor (0, 0.7, 0, 1) )
    # Add Text
    block_text  = PTVR.Stimuli.Objects.Text ( 
        position_in_current_CS =   np.array ([0, headset_pos_y, z_coordinate] ),
        visual_angle_of_centered_x_height_deg = 2 )
    block_text_str = 'Block: '+ str (block_i) + '\n Press space bar \n to start\n the block of trials.'
    block_text.text = block_text_str
    block_launch_scene.place (block_text,my_world) 
    # Add Interaction
    block_launch_scene.AddInteraction (events = [click], callbacks = [end_scene])
    # Save Data of current Scene
    save_experiment_data (scene = block_launch_scene, block_i = block_i, 
                          scene_description ="create_block_launch_scene")
    
    my_world.add_scene ( block_launch_scene) 
# END of def create_block_launch_scene (block_i)
#--------------------------------------------------------------------   

###############################################
def  create_trial_scene ( block_i, trial_i ):
    trial_scene = PTVR.Stimuli.Scenes.VisualScene ( trial = trial_i, 
                            background_color = color.RGBColor (0, 0.0, 0.9, 1) )
    # Add Text
    block_text  = PTVR.Stimuli.Objects.Text ( 
        position_in_current_CS =   np.array ([0, headset_pos_y, z_coordinate] ),
        visual_angle_of_centered_x_height_deg = 2 )
    block_text_str = "Block:" + str (block_i) + "- Trial:" + str (trial_i) + '\n Press space bar \n to display\n next trial.'
    block_text.text = block_text_str
    trial_scene.place (block_text, my_world)
    # Add Interaction
    trial_scene.AddInteraction (events = [click], callbacks = [end_scene])
    # Save Data of current Scene
    save_experiment_data (scene = trial_scene, block_i = block_i, 
                          scene_description = "create_trial_scene")  
    
    my_world.add_scene (trial_scene)
# END of  create_trial_scene ( block_i, trial_i ):
#--------------------------------------------------------------------     

############
def main():
    for block_i in range (1, number_of_blocks+1):
       print("____________________________")    # console
       print("Block : " + str(block_i))         # console

       create_block_launch_scene (block_i = block_i) 
                                       
       for trial_i in range ( 1, number_of_trials+1 ):
           print("Trial : " + str (trial_i)) # console
           create_trial_scene ( block_i, trial_i )

    my_world.write() # Write experiment to .json file    
#-----------------------------------------------------
    
#   __main__ if statement allows to import this demo
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld() # Launch the Experiment with PTVR.