# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Experiment_building\Saving_results\
1_where_is_my_results_file_by_default.py

Goal of present Demo : 
        show where your results will be saved if you do not
        specify anything concerning the location of the results 
        files.

        As the name of the current script is :
            "1_where_is_my_results_file_by_default.py",   
        then your results will be saved in:
... \PTVR_Researchers\PTVR_Operators\Results\1_where_is_my_results_file_by_default\


Note 1: your script might be 
        either in one of PTVR's folders,
        notably in "...\PTVR_Researchers\Python_Scripts\...",
        OR 
        anywhere on your PC

Note 2: If you want to define the location where your results will be saved,
please see the demo "2_where_else_can_I_save_my_results_file.py".


"""


import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================


my_radial_distance = 2  # in meters
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================


def main():

    my_world = visual.The3DWorld()
    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    my_text = PTVR.Stimuli.Objects.Text(
        text="You have to close the PTVR window by hand\n" +
        "to check where your results have been saved. \n" +
        "\n Don't worry! Your results path is printed in the console :O)",
        position_in_current_CS=np.array([0, 1.2, my_radial_distance]),
        visual_angle_of_centered_x_height_deg=0.5
    )

    # my_text.set_perimetric_coordinates( radialDistance = my_radial_distance)
    my_scene.place(my_text, my_world)
    # must be after the lines of code with my_scene.place (object)
    my_world.add_scene(my_scene)

    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
