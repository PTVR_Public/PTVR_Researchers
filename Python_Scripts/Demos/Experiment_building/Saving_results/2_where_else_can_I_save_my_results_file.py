# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Experiment_building\Saving_results\
2_where_else_can_I_save_my_results_file.py

Goal of present Demo : 
    show where your results will be saved if you change 
    the "resultsPath" parameter when calling The3DWorld (),
    as for instance in the following code line : 
        PTVR.Visual.The3DWorld ( resultsPath = "C://great_folder//")
        

What you get with the present demo:
    As the name of the current script is :
        "2_where_else_can_I_save_my_results_file.py",    
    then your result files will be saved in the folder: 
            "C:/great_folder/2_where_else_can_I_save_my_results_file/"  
            
    And this will be true whatever the location of your python script on the PC.
  
Note:   the location of the results file is indicated in the console.
        
        
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

my_results_path = "C://great_folder//"    # !!!!!!!!!!!!!!!!!!!!!!

my_radial_distance = 2 # in meters 
   
def main():

    my_world = visual.The3DWorld ( resultsPath = my_results_path )
    text_always_on = "You have to close the PTVR window by hand\n" + \
        "to check where your results have been saved.\n" +  \
        "\n Don't worry! Your results path is printed in the console :O)"
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()    
    my_text = PTVR.Stimuli.Objects.Text (
        text = text_always_on,
        position_in_current_CS = np.array( [0, 1.2, my_radial_distance]),     
        visual_angle_of_centered_x_height_deg= 0.5)
 
    my_scene.place (my_text, my_world)   
    my_world.add_scene (my_scene) # must be after the lines of code with my_scene.place (object)

    my_world.write() 

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld ()