# -*- coding: utf-8 -*-
"""
...\Demos\Experiment_building\10_loop_of_trials_w_one_scene_per_trial.py

Description :
    Loop of trials with one scene per trial
    The user must press the space bar to go to the next trial.
    
    A trial rank is associated here with each scene when the scene is created
   ( see the VisualScene () fonction ). This trial rank will be
   visible in the results file.
    
Note: 
    you can see in the console the path of the directory where the results are
automatically saved. Have a look at the results file (in csv format) in this 
directory.

"""


import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback

nb_of_trials = 5
   
def main():

    my_world = visual.The3DWorld (  name_of_subject = "nabil" )

    for i in range (0, nb_of_trials):
        
        # a trial rank is associated with a scene in the line below !
        trial_scene = PTVR.Stimuli.Scenes.VisualScene (trial = i + 1)
        
        trial_text = PTVR.Stimuli.Objects.Text ( text = "trial : " + 
                                               str (trial_scene.trial) + " Press space bar to continue",
                                               fontsize_in_postscript_points = 200
                                              )
        trial_text.set_cartesian_coordinates ( z = 2 )

        # Create interaction for the scene
        my_event_space_bar_press =  event.Keyboard ( valid_responses = ['space'], 
                                                     mode = "press")     
        my_callback_current_scene_end = callback.EndCurrentScene ()       
        trial_scene.AddInteraction ( 
                events =    [ my_event_space_bar_press ], 
                callbacks = [ my_callback_current_scene_end ] )
        
        trial_scene.place ( trial_text, my_world ) 
        
        my_world.add_scene ( trial_scene )
        
    my_world.write() # Write experiment to .json file
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld ()