# -*- coding: utf-8 -*-
"""
...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Interactions_events_callbacks\callbacks\
pause_current_scene.py

Created on : Tue Nov 23 10:02:30 2021
@author: jtermozm
    
Goal : This demo shows that:
    a/ a scene can be paused thanks to the PauseCurrentScene callback.
    b/ an interaction in a scene is paused by default in synchrony with 
    the pause of this scene. This behaviour occurs when the callbacks of this interaction have 
    their parameter 'is_callback_pausable' set to TRUE (which is the default).

As an example, let's consider the following interaction:
a sound callback is triggered by an 's' keyboard event (fictitious interaction not implemented yet
                                                        in PTVR). 
By default, this interaction will be functional when the scene is NOT paused 
and will NOT be functional when the scene is paused.

Firstly, the present demo illustrates this default behaviour with the following interaction: 
the blue_text string variable can be changed by left and right arrows only 
when the scene is NOT PAUSED.
This behaviour occurs when the parameter 'is_callback_pausable' is set to TRUE 
for the two ChangeObjectText callbacks (that are triggered by the left and right 
                                        arrow events).


Secondly, the present demo illustrates how this default behaviour can be modified
by setting the parameter 'is_callback_pausable' to FALSE for the two ChangeObjectText callbacks 
triggered by the left and right arrow events.

You can set both parameters to TRUE or FALSE respectively by
setting the global variable is_callback_pausable to 1 or 0



"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import PTVR.Stimuli.Color as color
# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

is_callback_pausable = 1 # 1 or 0 to alternate between default and non default behaviours

if is_callback_pausable == 1: 
     boolean_value_of_updated_GV = True 
else:
    boolean_value_of_updated_GV = False 

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================   
def main():

    my_world = visual.The3DWorld (  name_of_subject = "toto" )
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()

    text_that_cannot_be_changed = PTVR.Stimuli.Objects.Text ( text = "This black text cannot be changed. It is used to give instructions.\n\
Press space bar to PAUSE the scene and release space bar to stop the pause.\n\n\
Press right arrow key to change the blue words into a new string of bold characters.\n\
Press left arrow key to change the blue words into a new string of non-bold characters. \n\
If you have set the variable 'is_callback_pausable' to 1, the interaction will work only if the scene is NOT paused\n\
If you have set the variable 'is_callback_pausable' to 0, the interaction will work EVEN if the scene is paused\n\
Current value of is_callback_pausable: " + str(is_callback_pausable) ,
                                            position_in_current_CS = np.array ([-0.5, 1.0, 1]),
                                            horizontal_alignment = "Left",
                                            visual_angle_of_centered_x_height_deg = 0.8     )    
    my_scene.place (text_that_cannot_be_changed,my_world) 
        
    ## 1st Interaction:  
    # if space bar is pressed,
    # then
    # Change Background Color to rose AND Suspend Current Scene  And change text 
    space_bar_press = event.Keyboard ( valid_responses= ['space'], mode = "press")   
    change_background_color_to_rose = callback.ChangeBackgroundColor (
                                           new_color = color.RGB255Color (234, 200, 200, 1), 
                                            effect = "activate")
    pause_scene = callback.PauseCurrentScene (effect = "activate") 
    
    display_state_of_current_scene = PTVR.Stimuli.Objects.Text ( 
            text = "State of current scene : NOT PAUSED" , 
            position_in_current_CS = np.array ( [0, 0.5, 1] ),  
            visual_angle_of_centered_x_height_deg = 1 )
    my_scene.place (display_state_of_current_scene, my_world)
    
    display_paused_callback = callback.ChangeObjectText (
                        display_state_of_current_scene.id, 
                        text = "State of current scene : PAUSED", 
                        is_callback_pausable = False)   
    
    my_scene.AddInteraction (   events = [ space_bar_press ],
                                callbacks = [ change_background_color_to_rose,
                                           pause_scene ,
                                           display_paused_callback ] ) 
    ## 2nd Interaction:  
    # if space bar is released,
    # then
    # Background Color comes back to initial color (here white)
    # AND pause of the Current Scene is stopped.
    space_bar_release = event.Keyboard (valid_responses = ['space'], mode = "release")
    change_background_color_to_initial = callback.ChangeBackgroundColor ( effect = "deactivate")
    stop_pause_of_scene = callback.PauseCurrentScene ( effect = "deactivate" )
    display_not_paused_callback = callback.ChangeObjectText (display_state_of_current_scene.id, 
                        text = "State of current scene : NOT PAUSED", 
                        is_callback_pausable = False) 
    my_scene.AddInteraction ( events = [space_bar_release], 
                              callbacks = [stop_pause_of_scene, 
                                           change_background_color_to_initial,
                                           display_not_paused_callback] )

    ## 3rd Interaction:  
    # if right arrow key is pressed,
    # then
    # blue_text becomes a string of bold characters    
    blue_text = PTVR.Stimuli.Objects.Text (    text = " This is the initial text " , 
                                                    position_in_current_CS = np.array ( [0, 0.3, 1] ),
                                                    color = color.RGBColor ( b = 1 ), # BLUE text
                                                    visual_angle_of_centered_x_height_deg = 1 )
    my_scene.place (blue_text,my_world)
    
    right_arrow_press  = event.Keyboard ( valid_responses = ['right'], mode = "press")
    replace_text_with_bold_characters = callback.ChangeObjectText ( 
                                    text_object_id = blue_text.id, 
                                    text = "This blue text is now bold (due to right arrow press)", 
                                    is_bold = True, effect = "activate", 
                                    is_callback_pausable = boolean_value_of_updated_GV # True is the default value: set here for didactic reasons
                                    ) # Note that you could change text color with
                                        # callback.ChangeObjectColor
    my_scene.AddInteraction (   events = [right_arrow_press],
                                callbacks = [replace_text_with_bold_characters])

    ## 4th Interaction:  
    # if  left arrow key is  pressed,
    # then
    # blue_text becomes a string of NON bold characters        
    left_arrow_press = event.Keyboard ( valid_responses = ['left'], mode = "press")
    replace_text_with_non_bold_characters =  callback.ChangeObjectText (
                                        text_object_id = blue_text.id, 
                                        text = "This blue text is NOT bold any more (due to left arrow press)",
                                        is_bold = False, effect = "activate",
                                        is_callback_pausable = boolean_value_of_updated_GV # True is the default value: set here for didactic reasons
                                        )
    my_scene.AddInteraction (   events = [ left_arrow_press ],
                                callbacks = [ replace_text_with_non_bold_characters ] )

    my_world.add_scene(my_scene)
    my_world.write() # Write The3DWorld to .json file
    
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()