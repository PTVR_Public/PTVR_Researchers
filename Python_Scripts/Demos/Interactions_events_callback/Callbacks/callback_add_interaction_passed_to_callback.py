# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Interactions_events_callback\Callbacks\
    callback_add_interaction_passed_to_callback.py
    
Goal of Demo:
    Illustrates the use of the AddInteractionPassedToCallback ( )
    
What to do: 
    move your head to point at the sphere !
    As soon as you are accurately pointing at the sphere,
    its color changes to blue...
    By moving your head on and off the sphere, you can change the sphere's color
    as long as you want.
    NOW ...
    if you maintain your head pointed at the sphere
    AND
    if a delay has elapsed,
   Then
       a new Empty scene is displayed
    
explanation: 
    What is the role of callback.AddInteractionPassedToCallback() ?
    
    It is a special callback that creates an interaction that will be passed as an 
    argument to 'callbacks' in my_scene.AddInteraction().
    Example:
    # First create this special callback:
    my_special_callback = callback.AddInteractionPassedToCallback ( 
                             events     = [ my_great_event ],
                             callbacks  = [ my_great_callback ] )
    And then create a standard interaction that uses the preceding callback as
    an argument:
    my_scene.AddInteraction ( 
                    events =    [ some_key_press ],
                    callbacks = [ my_special_callback ] # this callback is an interaction
                    )        
    Eventually, you have created nested interactions that do the following:
    if some_key_press occurs,
        then start the following interaction:
            if my_great_event occurs then call my_great_callback
            
    Example:
    if an object is correctly pointed at,
        then start the following interaction:
            if trigger is pressed on hand-controller then end the trial.        

'''

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import numpy as np
import PTVR.Stimuli.Color as color

my_delay_in_ms = 3000

def main():

    my_world = visual.The3DWorld ( )
    my_scene = PTVR.Stimuli.Scenes.VisualScene (trial = 1)
    my_target_disc = PTVR.Stimuli.Objects.Sphere ( color=color.RGBColor ( 1, 0, 0, 1),
                                                  size_in_meters = 0.1,
                                                 position_in_current_CS=np.array ( [0, 1, 1] ) ) 
    ### Events and Callbacks must be created BEFORE interactions !
    ## Events
    target_is_pointed_at =        event.PointedAt (activation_cone_radius_deg=1, 
                                                         activation_cone_depth=500, 
                                                         target_id = my_target_disc.id, 
                                                         mode="press")
    target_is_not_pointed_at =    event.PointedAt (activation_cone_radius_deg=1, 
                                                         activation_cone_depth=500, 
                                                         target_id = my_target_disc.id, 
                                                         mode="release") 
    ## Callbacks
    change_object_color  = callback.ChangeObjectColor ( 
                                new_color = color.RGBColor(b=1.0), 
                                object_id = my_target_disc.id)
    change_object_color_back = callback.ChangeObjectColor ( 
                                            effect = "deactivate", 
                                            object_id = my_target_disc.id)
    end_scene = callback.EndCurrentScene()
    
    delay_in_ms_is_exceeded =  event.Timer ( delay_in_ms = my_delay_in_ms, 
                                             timer_start = "Current Time" ) 
    # 'delay_in_ms' milliseconds have passed since the scene's start
    
    ## Callbacks that create a NEW interaction.
    # They are called 'Callbacks' because they will be passed as arguments to
    # 'callbacks' in my_scene.AddInteraction()
    end_scene_if_delay_is_exceeded =   \
                                callback.AddInteractionPassedToCallback ( 
                                    events = [ delay_in_ms_is_exceeded ],
                                    callbacks = [ end_scene ] )
    end_scene_if_delay_is_exceeded_STOP =  \
                                callback.AddInteractionPassedToCallback ( 
                                    events = [ delay_in_ms_is_exceeded ],
                                    callbacks = [ end_scene ], effect = "deactivate" )
    ## Standard Interactions
    # When target_point is pointed at,
    # the color of my_target_disc is changed to Blue
    # AND 
    # a new interaction occurs such that the scene is ended
    # when some delay is exceeded
    my_scene.AddInteraction ( events = [ target_is_pointed_at ],
                              callbacks = [ change_object_color, 
                                            end_scene_if_delay_is_exceeded ] )
    # When target_point is NOT pointed at, 
    # the color of target_disc is changed back to Red (initial color) and
    # the interaction that end the scene after some delay is deactivated.
    my_scene.AddInteraction ( events = [ target_is_not_pointed_at ],
                              callbacks = [ change_object_color_back, 
                                            end_scene_if_delay_is_exceeded_STOP ] )
    
    my_scene.place (my_target_disc, my_world)  
    my_world.add_scene (my_scene)
    
    my_scene_2 = PTVR.Stimuli.Scenes.VisualScene (trial = 1)
    my_world.add_scene (my_scene_2)
    my_world.write() # Write The3DWorld to .json file
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()