# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Interactions_events_callbacks\quit_PTVR.py

Goal : This demo shows how the 'quit' callback allows you to instantaneously quit PTVR.exe.

In the particular case of this script, this callback is triggered by pressing 
the space bar (a keyboard event)

Created on : Thu Jan 13 15:09:02 2022
@author: jtermozm
'''
import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
 
def main():

    my_world = visual.The3DWorld ()
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()

    my_event = event.Keyboard (valid_responses = ['space'])
    my_callback = callback.Quit()

    my_scene.AddInteraction (events= [my_event], callbacks =[my_callback] )
    
    #Display Information for callback
    my_text_press = PTVR.Stimuli.Objects.Text (text="Press space bar to Quit ", 
                                               position_in_current_CS = np.array([0.0,1.2,1.0]),visual_angle_of_centered_x_height_deg=1.0)
    my_scene.place (my_text_press,my_world)
  
    my_world.add_scene (my_scene)
    my_world.write() # Write The3DWorld to .json file
    
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()