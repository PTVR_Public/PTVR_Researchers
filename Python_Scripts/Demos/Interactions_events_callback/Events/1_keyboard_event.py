# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Interactions_events_callbacks\keyboard_event.py

Goal: This demo shows how to use the Keyboard event and how the modes "press" and "release" work. 

How to use:  
Pressing the 'space' bar or the 'q' key changes the background to Red. 
It also changes the displayed text.
Releasing the 'space' bar or the 'q' key changes the background back to white (default background_color of a VisualScene),
and goes back to the default text.
PTVR works with :
    - the Windows French (France) Keyboard or
    - the Windows English (UK) keyboard

Created on: Tue Nov 23 10:02:30 2021
@author: jtermozm

'''
import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import PTVR.Stimuli.Color as color
import numpy as np
# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================


text_always_on = """Press the SPACE bar or the 'q' key to change the background to red.\n 
As soon as you release the SPACE bar or the 'q' key, the background returns to white.
"""
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

def main():
    my_world = visual.The3DWorld ( name_of_subject = "toto" )
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()

    my_text = PTVR.Stimuli.Objects.Text ( text = "State of valid key: RELEASED",
                                        position_in_current_CS = np.array([0.0, 0.0, 0.25]))
    my_scene.place (my_text,my_world)
    
    ## Create event 
    my_event_press = event.Keyboard (   valid_responses = [ 'space', 'q'], mode = "press")
  
    my_event_release = event.Keyboard ( valid_responses = [ 'space', 'q'], mode = "release")
    
    ## Create callback ChangeBackgroundColor
    my_callback_change_background_color_activate = callback.ChangeBackgroundColor (   
                                                    new_color = color.RGBColor(r = 0.7), 
                                                    effect = "activate")
    my_callback_change_background_color_deactivate = callback.ChangeBackgroundColor ( effect = "deactivate")
    
    ## Create callback ChangeObjectText
    my_callback_change_object_text_activate = callback.ChangeObjectText   ( text = "State of valid key : PRESSED", 
                                                                      text_object_id = my_text.id,
                                                                      effect = "activate")
    my_callback_change_object_text_deactivate = callback.ChangeObjectText ( text_object_id = my_text.id,
                                                                      effect = "deactivate")

    ## Add TWO interactions (between callback and event) to my_scene
    my_scene.AddInteraction ( events = [ my_event_press ], 
                              callbacks = [ my_callback_change_background_color_activate, 
                                         my_callback_change_object_text_activate])
    my_scene.AddInteraction ( events = [ my_event_release], 
                              callbacks = [ my_callback_change_background_color_deactivate, 
                                         my_callback_change_object_text_deactivate])

    # Text always ON
    my_text_always_on = PTVR.Stimuli.Objects.Text ( text = text_always_on,
                                                  position_in_current_CS = np.array([0.0, 0.0, 0.3]), 
                                                  visual_angle_of_centered_x_height_deg = 1, align_vertical = "TOP")
    my_text_always_on.set_perimetric_coordinates ( radialDistance = 200, eccentricity = 9, halfMeridian=90)
    my_scene.place (my_text_always_on,my_world)
    
    my_world.add_scene(my_scene)
    my_world.write() # Write The3DWorld to .json file
    
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()