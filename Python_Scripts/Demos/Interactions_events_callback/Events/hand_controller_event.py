# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Interactions_events_callbacks\HandController_event.py

Goal: This demo shows how to use the  "HandController" event and how the modes "
    press" and "release" work with this event. 

How to use:  
Pressing the trigger of the right controller changes the background to Red. 
It also changes the displayed text.
Releasing the trigger changes the background back to white (default background_color of the VisualScene),
and goes back to the default text.

In this particular demo, the  "HandController" event is associated with the 
    callbacks "ChangeBackgroundColor" and "ChangeObjectText".

Created on: Tue Nov 23 10:02:30 2021
@author: jtermozm
'''


import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import PTVR.Stimuli.Color as color
import numpy as np
# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Doe"

text_always_on = """
If you can see your virtual right hand,
this means that you switched on at least one hand controller, 
you're good to go !
Press the trigger of the right controller to change the background to red.
As soon as you release the trigger, the background returns to white.

Your results are saved in : ...\PTVR_Researchers\PTVR_Operators\Results\hand_controller_event\

"""
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================
  
def main():

    my_world = visual.The3DWorld(name_of_subject = username)
    my_scene = PTVR.Stimuli.Scenes.VisualScene(is_right_hand_controller_visible = True)
    
    my_text = PTVR.Stimuli.Objects.Text(text= " State of right trigger: RELEASED",
                                      position_in_current_CS = np.array ( [0.0, 0.0, 0.3] ), 
                                      visual_angle_of_centered_x_height_deg = 1.5)
    my_text.set_perimetric_coordinates ( radialDistance =200, eccentricity=0,halfMeridian=0 )
    my_scene.place(my_text,my_world)
    
    ## Events
    #
    my_event_press = event.HandController (
        valid_responses = ['right_trigger'], mode="press"
        )
    my_event_release = event.HandController (
        valid_responses = ['right_trigger'], mode="release"
        )
    #
    # my_event_press = event.HandController (valid_responses = ['right_touch_press'],     mode="press")
    # my_event_release = event.HandController (valid_responses = ['right_touch_press'],   mode="release")    
    # #
    # my_event_press = event.HandController (valid_responses = ['right_grip'],     mode="press")
    # my_event_release = event.HandController (valid_responses = ['right_grip'],   mode="release")       

    ## Callbacks
    #
    my_callback_change_background_color_activate = \
        callback.ChangeBackgroundColor (  new_color=color.RGBColor(r = 0.7), 
        effect = "activate")
    my_callback_change_background_color_deactivate = \
        callback.ChangeBackgroundColor ( effect = "deactivate")
    
    my_callback_change_objet_text_activate = \
        callback.ChangeObjectText ( text = "State of right trigger: PRESSED", 
                                    text_object_id = my_text.id, 
                                    effect = "activate")
    my_callback_change_object_text_deactivate = \
        callback.ChangeObjectText ( text_object_id = my_text.id , 
                                    effect = "deactivate")    
    
    ## Links between events and callbacks
    # create first interaction
    # if you press trigger, this induces two callbacks: 
    #   a/ change the bckgnd color and 
    #   b/ change the text to "State of right trigger: PRESSED"
    my_scene.AddInteraction ( 
        events = [my_event_press],   
        callbacks = [ my_callback_change_background_color_activate,   
                      my_callback_change_objet_text_activate ] )
    # create second interaction
    # if you release trigger, this induces two callbacks:
    #   a/ Deactivate the change in bckgnd color and 
    #   b/ change the text back to its initial content ie ...
    # "State of right trigger: RELEASED".
    my_scene.AddInteraction ( 
        events= [my_event_release], 
        callbacks = [ my_callback_change_background_color_deactivate, 
                      my_callback_change_object_text_deactivate ] )
    
    # Text to explain task to subject
    my_text_always_on = PTVR.Stimuli.Objects.Text (
        text = text_always_on, 
        visual_angle_of_centered_x_height_deg = 1, align_vertical = "TOP"
        )
    my_text_always_on.set_perimetric_coordinates (
        radialDistance = 200, eccentricity = 10, halfMeridian = 90
        )
    my_scene.place (my_text_always_on,my_world)
    
    my_world.add_scene ( my_scene)
    my_world.write() # Write The3DWorld to .json file
    
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()