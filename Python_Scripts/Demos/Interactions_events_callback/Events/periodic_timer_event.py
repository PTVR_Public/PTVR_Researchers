# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Interactions_events_callback\Events\
periodic_timer_event.py

Goal : illustrate how to create a periodic Timer Event

What you see: a sphere bouncing back and forth between two spheres

"""


from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Forest, CustomObject, Sphere
import PTVR.Stimuli.Color as color
import numpy as np
from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import PTVR.Data.Callbacks.CounterCallback
import PTVR.Data.Callbacks.MovementCallback as movementCallback



my_world = The3DWorld()

def main():

    my_scene = VisualScene (skybox="BrightMorning", trial=1)
    my_forest = Forest(position_in_current_CS=np.array([0, 0.92, 4]))
    sphere_start_position = np.array([0, 0.25, 3])

    # Creation of Spheres
    sphere_1 = Sphere(size_in_meters=np.array([0.2, 0.2, 0.2]),
                      color=color.RGBColor(0.0, 1, 0),
                      position_in_current_CS=np.array([-4, 0.54, 6]))

    sphere_2 = Sphere(size_in_meters=np.array([0.2, 0.2, 0.2]),
                      color=color.RGBColor(0.0, 0, 1),
                      position_in_current_CS=sphere_start_position)

    sphere_3 = Sphere(size_in_meters=np.array([0.2, 0.2, 0.2]),
                      color=color.RGBColor(0.5, 0, 0),
                      position_in_current_CS=sphere_start_position)

    distance_two_spheres = np.linalg.norm(
        sphere_1.position_in_current_CS - sphere_3.position_in_current_CS)

    sphere_speed = 10  # m/s

    time_to_reach_sphere = 1000 * distance_two_spheres/sphere_speed  # in milliseconds

    my_scene.place(my_forest, my_world)
    my_scene.place(sphere_1, my_world)
    my_scene.place(sphere_2, my_world)
    my_scene.place(sphere_3, my_world)

    ###############################################################################
    ############################### Interactions ##################################
    ###############################################################################

    ################################## Events #####################################

    event_timer_1 = event.Timer ( delay_in_ms = 0,
                                  period_in_ms = 2*time_to_reach_sphere)

    event_timer_2 = event.Timer ( delay_in_ms = time_to_reach_sphere,
                                  period_in_ms = 2*time_to_reach_sphere)

    ################################ Callbacks #####################################
    my_callback_translation_go = movementCallback.MoveTowards(
        object_id = sphere_3.id,
        target_object_id = sphere_1.id,  
        speed = sphere_speed,
        effect="activate")

    my_callback_translation_come_back = movementCallback.MoveTowards(
        object_id = sphere_3.id,
        target_object_id = sphere_2.id,
        speed = sphere_speed,
        effect="activate")

    ################################# interactions #####################################
    my_scene.AddInteraction( events = [event_timer_1],
                             callbacks = [my_callback_translation_go])
    
    my_scene.AddInteraction( events = [event_timer_2],
                             callbacks = [ my_callback_translation_come_back])

    my_world.add_scene(my_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    LaunchThe3DWorld()  # Launch the Experiment with PTVR.
