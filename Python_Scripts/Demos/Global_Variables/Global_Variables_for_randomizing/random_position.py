# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Demos\Global_Variables\Global_Variables_for_randomizing\
    random_position.py
    
What you see:
        A sphere is set to a new random position (with a callback)      
        at random temporal intervals (triggered thanks to the RandomTimer event).   
    
Goal: this illustrates the use of the PTVR Global Variable when you want to 
        draw random values at runtime.
        

How it works:
        a callback (here RandomVector3) creates the global variable, 
        labels this global variable (here 'my_vector3_GV').
        draws a triplet of random values,
        and stores the triplet into the global variable.
        
        The triplet in the global variable is then available (thanks to its label)
        for other callbacks !
        
        In the present example, the triplet is used by a callback whose role is 
        to set a new position to a sphere.
        
        
Note: As the sphere positions are randomly drawn at runtime, the randomizing process,
         hence the change in position, can go on infinitely.     


"""

from PTVR.Visual import The3DWorld
from  PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere
import numpy as np
from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Data.Event
import PTVR.Data.Callback
import PTVR.Data.Callbacks.RandomCallback
import PTVR.Data.Callbacks.MovementCallback as movementCallbacks

limits = 1

my_world = The3DWorld ()

def main():                                        
    my_scene = VisualScene ()
    my_sphere = Sphere()
    my_scene.place (my_sphere, my_world)
    
    ######################### Events ############################################
    # This event is triggered infinitely at random temporal intervals. Each
    # interval is randomly drawn between 200 and 500 ms (uniform distribution).    
    my_event = PTVR.Data.Event.RandomTimer (
        random_interval_in_ms_min_max = np.array([200, 500])   )
    
    ####################### Callbacks #####################################                                                                                 
    # This callback creates and updates a Global Variable which here is a
    # 3D vector.
    create_random_vector3 = \
        PTVR.Data.Callbacks.RandomCallback.RandomVector3(
                label_of_updated_GV = "my_vector3_GV", # GV for Global Variable
                x_min_max = np.array([-limits, limits]),
                y_min_max = np.array([-limits, limits]),
                z_min_max = np.array([1, 3]) )
        
    # This 3D vector global variable (called "my_vector3_GV") is then used
    # by the following callback that changes the sphere's position
    change_position = movementCallbacks.SetCartesianCoordinatesInGlobalCS(
                                object_id = my_sphere.id, 
                                label_of_updated_GV_to_use = "my_vector3_GV" ) 
                
    ############################ Add Interactions ######################
    my_scene.AddInteraction (events = [my_event],
                            callbacks = [ create_random_vector3, 
                                          change_position ]       )     
    my_world.add_scene (my_scene)   
    my_world.write()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
 
