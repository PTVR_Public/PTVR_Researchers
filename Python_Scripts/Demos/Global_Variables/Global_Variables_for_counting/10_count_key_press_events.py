# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\10_count_key_press_events.py


Goal : 
       Show how to use a PTVR Global Variable (GV) to count how many times a key
       has been pressed on the keyboard. Using a GV as a COUNTER 
       of events is very useful for many purposes 
       (for instance when programming games).
       
What you see: 
        By pressing the "up" key on your keyboard, a GV counter is 
        increased (by one).
        The value of the counter is displayed on a Flat Screen.

Note: 
    Note the "trick" to set the GV initial value to 1 (see my_GV_initial_value)

Note: 
    Global Variable COUNTERS are created/updated with the CounterGVupdate
    callback.
    The values contained in these COUNTERS are displayed with the
    DisplayCounter callback.
        

'''

import PTVR.SystemUtils
import numpy as np
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import PTVR.Data.Callbacks.CounterCallback


def main():
    my_world = visual.The3DWorld ()
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    
    my_screen = PTVR.Stimuli.Objects.FlatScreen (
        size_in_meters = 0.6,
        color=color.RGBColor (r=1, g= 1, b=1),
        position_in_current_CS = np.array ([-0.6, 1.2 , 3]) )    
    my_scene.place (my_screen, my_world) 
    
    my_GV_initial_value = 1 
    
    
    my_text = PTVR.Stimuli.Objects.Text ( 
                        text = "Initial Value in COUNTER\n%d"%my_GV_initial_value,
                        # use %f if my_GV_initial_value is a float
                        visual_angle_of_centered_x_height_deg = 0.7
                        )     
    my_text.set_cartesian_coordinates_on_screen ( 
                        my_2D_screen = my_screen, 
                        z_local = -0.001
                        )    
    my_scene.place (my_text, my_world)   
    
    # Event       
    up_is_pressed = event.Keyboard ( valid_responses = [ "up"], mode = "press")
    # Callbacks
    increase_score = PTVR.Data.Callbacks.CounterCallback.CounterGVupdate(
                operation = "+",
                global_variable_initial_value = my_GV_initial_value,
                factor = 1.0,
                label_of_updated_GV = "Value_in_counter"
                )    
    display_variable = PTVR.Data.Callbacks.CounterCallback.DisplayCounter(
                text_id = my_text.id,
                label_of_updated_GV_to_use = "Value_in_counter")
    # Interaction          
    my_scene.AddInteraction ( events = [ up_is_pressed ], 
                              callbacks = [ increase_score,
                                            display_variable ]
                              )           
    my_world.add_scene(my_scene)          
    my_world.write() 
        
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()