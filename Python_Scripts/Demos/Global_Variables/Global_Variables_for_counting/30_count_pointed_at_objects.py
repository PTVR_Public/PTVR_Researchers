# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Global_Variables\Global_Variables_for_counting\
    30_count_pointed_at_objects.py

Goal : 
       Show how to use a PTVR Global Variable (GV) to count the number of objects 
       that have been pointed at (only once) during a scene. 
       Using a Global Variable as a counter of Events is very useful for 
       programming games.
       
What you see:
       In each scene, a set of n_objects static objects is presented (here black
                                                               spheres).
          The goal of the game is to point at objects.
       Everytime you point at an object, a Global Variable, used as a
       counter, is updated (here increased by one) until five different objects
       have been pointed at within a scene 
       (see "score_to_be_reached_in_each_scene" variable), in which case the 
       next scene is presented.
          Pointing at an object more than once is not useful here as any object is 
       only allowed to increase the Global Variable counter once.
       
          When the next scene is presented, the Global Variable counter is
       reset to zero.

Note: 
    In this demo, there is ONE COUNTER associated with each of several objects 
    in a scene. 
    This kind of counter is created with the CounterGVupdateFromObject
    callback.


Created in July 2024
@author: Carlos Aguilar  
"""

from PTVR.Visual import The3DWorld
from  PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere, Forest

import PTVR.Stimuli.Color as color
import numpy as np
from PTVR.Pointing.PointingCursor import  PointingLaser,LaserContingency
from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Data.Event
import PTVR.Data.Callbacks.CounterCallback
import PTVR.Data.Events.CounterEvent

import random
import os
# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

my_laser_color = color.RGBColor ( r = 0.7, g = 0.5 )

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================
 
my_world = The3DWorld ()

n_scenes = 3
n_objects = 16
score_to_be_reached_in_each_scene = 5 # here : score to reach in order
# to start the next scene
my_label_of_counter = "counter_to_end_scene" 

def main():
    end_current_scene = PTVR.Data.Callback.EndCurrentScene() 
    
    scene_score_reached = PTVR.Data.Events.CounterEvent.TriggerWhenValueIsReached ( 
                        value_to_be_reached = score_to_be_reached_in_each_scene,
                        label_of_updated_GV_to_use = my_label_of_counter,
                        global_variables_to_reset = np.array([my_label_of_counter])
                        ) 
    
    for j in range(n_scenes): # each scene is a trial        
        objects_list = []
          
        text = "Trial\n%d\nScore to end scene\n0.0"%(j+1) # 
              
        my_screen = PTVR.Stimuli.Objects.FlatScreen (size_in_meters = 0.6,
                color=color.RGBColor (r=1, g= 1, b=1),
                position_in_current_CS = np.array ([-0.6, 1.2 , 3]) )
        
        text_object = PTVR.Stimuli.Objects.Text ( text = text,
                                        visual_angle_of_centered_x_height_deg = 0.7
                                        ) 
        text_object.set_cartesian_coordinates_on_screen ( my_2D_screen = my_screen, 
                                                          z_local = -0.001    )
        my_laser_beam =  PointingLaser ( hand_laser = LaserContingency.RIGHT_HAND, 
                                         laser_color = my_laser_color,
                                         laser_width = 0.01 )
        my_scene = VisualScene ( trial = j+1,
                                are_both_hand_controllers_visible = True,
                                skybox= "BrightMorning" )    
        my_scene.place (my_screen, my_world)         
        my_scene.place (text_object, my_world)                       
        my_scene.place_pointing_laser ( my_laser_beam )   
        
        my_forest = Forest (position_in_current_CS = np.array ([0, 0, 4]))
        my_scene.place (my_forest, my_world)        
        
        # Set of Objects (here spheres)
        for i in range(n_objects):
            x_pos = random.random() * 7 - 3.5
            z_pos = 2.5 + random.random() * 3

            object_i = Sphere (
                position_in_current_CS = np.array ([x_pos, -0.92, z_pos]),
                size_in_meters = np.array ([0.1, 0.1, 0.1])  )
        
            objects_list.append(object_i)
            my_scene.place (object_i, my_world)
      
            ## Interactions
            # Events
            object_is_pointed_at = PTVR.Data.Event.PointedAt ( 
                                        target_id = objects_list[i].id, 
                                        activation_cone_origin_id = 
                                            my_world.handControllerRight.id,
                                        activation_cone_radius_deg = 0.01)            
            
            # Callbacks
            #
            callback_list = []
            
            increase_score = \
                PTVR.Data.Callbacks.CounterCallback.CounterGVupdateFromObject(
                                operation = "+",
                                factor = 1.0,
                                label_of_updated_GV = my_label_of_counter,
                                update_the_global_variable_only_once = True,            
                                object_id = objects_list[i].id )
            callback_list.append ( increase_score )    
                
            display_counter = PTVR.Data.Callbacks.CounterCallback.DisplayCounter(
                                text_id = text_object.id,
                                label_of_updated_GV_to_use = my_label_of_counter, 
                                display_trial = True  )
            callback_list.append ( display_counter ) 
            
            # Interaction            
            my_scene.AddInteraction (
                            events      = [ object_is_pointed_at ],
                            callbacks   = callback_list 
                            )
            # BEWARE: do not write : callbacks   = [callback_list] 
            # using square brackets would create an error as callback_list is
            # already a python list.
       
        # END of for i in range(len(objects_list)):  
            
        # For each scene,    
        # when 'counter_to_end_scene' reaches score_to_be_reached_in_each_scene,
        # then the current scene is ended which automatically displays the next 
        # scene.
        my_scene.AddInteraction (events = [scene_score_reached],
                                callbacks = [end_current_scene] )            
        my_world.add_scene (my_scene)
    # END of for j in range(n_scenes):
        
    my_world.write()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
 
