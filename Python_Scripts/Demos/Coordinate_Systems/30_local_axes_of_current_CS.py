# -*- coding: utf-8 -*-
"""
...\Demos\Coordinate_Systems\30_local_axes_of_current_CS.py

Goal: to help you visualize the local axes of the successive "current" 
    Coordinate Systems (CS)
    as you apply successive coordinate transformations within the script.
 
What you see: Three different local Coordinate Systems (CS) represented by
            red / green / blue gizmos*.
            
In the console: you can check the global coordinates of each local axis.
            
*For more details on the PTVR Gizmo Object, see in directory...
...\Demos\Objects\...
    notably the script gizmo_to_draw_local_axes.py

"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

height_of_head =  1.1 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head, 0.7])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint  

def main():
    
    my_world = visual.The3DWorld (name_of_subject="Michelle")
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()  
    
    ###################################    
    # First Coordinate Transformation #
    # Translation to a point straight ahead of the approximate 
    # position of the head (with a seated subject)
    my_world.translate_coordinate_system_along_global (viewpoint_position)
    
    gizmo_1 = PTVR.Stimuli.Objects.Gizmo ( length = 0.1, width = 0.01 ) # Display axes of Current CS
    
    my_scene.place (gizmo_1, my_world)
    my_text = PTVR.Stimuli.Objects.Text (text = 'First\n Coordinate\n Transformation', 
                                         fontsize_in_postscript_points = 30, horizontal_alignment = "Center",
                                         position_in_current_CS = np.array([0.1, 0.1, 0.0])) 
    my_scene.place (my_text, my_world) 
    
    ####################################
    # Second Coordinate Transformation #  
    # Translation to the LEFT
    my_world.translate_coordinate_system_along_global (np.array ([-0.4, 0, 0]))
    gizmo_2 = PTVR.Stimuli.Objects.Gizmo ( length = 0.1, width = 0.01 ) # Display axes of Current CS
    
    my_scene.place (gizmo_2, my_world)
    my_text = PTVR.Stimuli.Objects.Text (text = 'Second\n Coordinate\n Transformation', 
                                         fontsize_in_postscript_points = 30, horizontal_alignment = "Center",
                                         position_in_current_CS = np.array([0.1, 0.1, 0.0])) 
    my_scene.place (my_text, my_world) 
    
    ####################################
    # Third Coordinate Transformation #  
    # Three successive Rotations about current CS's axes
    my_world.rotate_coordinate_system_about_current_y (135)
    my_world.rotate_coordinate_system_about_current_z (-30)
    my_world.rotate_coordinate_system_about_current_x (40)
    gizmo_3 = PTVR.Stimuli.Objects.Gizmo ( length = 0.1, width = 0.01 ) # Display axes of Current CS
    my_scene.place (gizmo_3, my_world)
    
    my_text = PTVR.Stimuli.Objects.Text (text = 'Third\n Coordinate\n Transformation', 
                                         fontsize_in_postscript_points = 30, horizontal_alignment = "Center",
                                         position_in_current_CS = np.array([0.1, 0.1, 0.0])) 
    my_scene.place (my_text, my_world) 
    
    X_axis_of_text_in_global_coord, Y_axis_of_text_in_global_coord, \
        Z_axis_of_text_in_global_coord = my_text.get_axes() 
        
    # In the console:
    # For each local axis of the Text Object, its three GLOBAL coordinates
    # are shown.
    print ( " Local X axis of object (in global coord.):\n " + str( X_axis_of_text_in_global_coord ) )
    print ( " Local Y axis of object (in global coord.):\n " + str( Y_axis_of_text_in_global_coord ) )    
    print ( " Local Z axis of object (in global coord.):\n " + str( Z_axis_of_text_in_global_coord ) ) 

    my_world.add_scene (my_scene)
      
    my_world.write () # writes the experiment to .json file
    print (".json file has been created.")

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld() 
