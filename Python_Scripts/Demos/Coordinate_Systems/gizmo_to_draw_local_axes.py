# -*- coding: utf-8 -*-
"""
...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Objects\gizmo_to_draw_local_axes.py
Based on :
...\PTVR\PTVR_Researchers\Python_Scripts\Debugs\Objects\gizmo_object.py
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

description = """
Goal :
    To illustrate the use of the "Gizmo" object which draws the 3 axes 
    either of the current CS or of an Object (including Tangent Screen and Text objects).
    
    Step 1 Show Axes of current Coordinate System (CS)
    
    Step 2 Show Cube and its Axes
    
    Step 3 Show Tangent Screen and its Axes
    
    Step 4 Show Text and its axes
"""

height_of_head =  1.2 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head , 0])  

length_of_gizmo_axes = 0.2
width_of_gizmo_axes = 0.01

def gizmo_information_to_string(origin , rightward_axis , upward_axis , forward_axis ):
    my_string = "origin : " + str(origin.round(2)) + "\n" + " rightward axis : " + str(rightward_axis.round(2)) + "  \n  upward axis : " + str(upward_axis.round(2)) + "  \n  forward axis : "+ str(forward_axis.round(2))
    return my_string 

def main():
    my_world = visual.The3DWorld (detailsThe3DWorld = description )
    my_world.translate_coordinate_system_along_current(viewpoint_position) 
    my_world.rotate_coordinate_system_about_global_y (90) 
    
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ( background_color = color.RGBColor(0.74,0.76,0.78,1.0)) 
    
    ##########
    # Step 1 #
    ##########
    
    # Display axes of Current CS
    gizmo = PTVR.Stimuli.Objects.Gizmo ( length = length_of_gizmo_axes, 
                                         width = width_of_gizmo_axes )
    my_scene.place (gizmo, my_world)
    
    # Get origin and axes of Current CS
    current_CS_origin = my_world.get_position()
    current_CS_rightward_axis = my_world.get_rightward_axis()
    current_CS_upward_axis = my_world.get_upward_axis()
    current_CS_forward_axis = my_world.get_forward_axis()
  
    # Display information 
    gizmo_information = gizmo_information_to_string (current_CS_origin, current_CS_rightward_axis,current_CS_upward_axis,current_CS_forward_axis)
    text = PTVR.Stimuli.Objects.Text ( text = "current CS:\n" + gizmo_information, fontsize_in_postscript_points=50,
                                       position_in_current_CS = np.array([0.0, -0.15, 0.0]))
    my_scene.place (text, my_world)
    
    ##########
    # Step 2 #
    ##########
    
    # Display Cube
    my_cube = PTVR.Stimuli.Objects.Cube ( size_in_meters=np.array ([0.05, 0.05, 0.05]),
                                          position_in_current_CS=np.array ([-1.0, 0.0, 1.0]))
    my_scene.place (my_cube, my_world)
    
    ########
    # TRICK to display axes of Cube - thanks to parenting.
    gizmo = PTVR.Stimuli.Objects.Gizmo ( length = length_of_gizmo_axes, 
                                         width = width_of_gizmo_axes )
    gizmo.set_parent (parent = my_cube)
    my_scene.place (gizmo, my_world)
    
    # Get origin and axes of Cube
    cube_origin = my_cube.get_position()
    cube_rightward_axis = my_cube.get_rightward_axis()
    cube_upward_axis = my_cube.get_upward_axis()
    cube_forward_axis = my_cube.get_forward_axis()
  
    # Note : we could have written :
    # cube_rightward_axis, cube_upward_axis, cube_forward_axis = my_cube.get_axes()
    
    # Display information
    gizmo_information = gizmo_information_to_string (cube_origin, cube_rightward_axis,cube_upward_axis,cube_forward_axis)
    text = PTVR.Stimuli.Objects.Text(text =  gizmo_information, visual_angle_of_centered_x_height_deg=1)
    text.set_parent (my_cube, y_local = -0.15)
    my_scene.place (text, my_world)
    
    ##########
    # Step 3 #
    ##########
    
    # Display Tangent Screen
    my_tangent_screen = PTVR.Stimuli.Objects.TangentScreen(size_in_meters=np.array([0.5, 0.5, 0.5]),position_in_current_CS=np.array([0.0, 1.5, 1.0]))
    my_scene.place(my_tangent_screen,my_world)
    
    # Display axes of Tangent Screen
    gizmo = PTVR.Stimuli.Objects.Gizmo ( length = length_of_gizmo_axes, 
                                         width = width_of_gizmo_axes )
    gizmo.set_parent(parent = my_tangent_screen)
    my_scene.place(gizmo,my_world)
    
    # Get origin and axes of Cube
    tangent_screen_origin = my_tangent_screen.get_position()
    tangent_screen_rightward_axis = my_tangent_screen.get_rightward_axis()
    tangent_screen_upward_axis = my_tangent_screen.get_upward_axis()
    tangent_screen_forward_axis = my_tangent_screen.get_forward_axis()
    
    # Display information
    gizmo_information = gizmo_information_to_string(tangent_screen_origin, tangent_screen_rightward_axis,tangent_screen_upward_axis,tangent_screen_forward_axis)
    text = PTVR.Stimuli.Objects.Text(text = gizmo_information,visual_angle_of_centered_x_height_deg=1)
    text.set_parent(my_tangent_screen, y_local = -0.5)
    my_scene.place(text,my_world)

    ##########
    # Step 4 #
    ##########
    # Display Text
    my_text = PTVR.Stimuli.Objects.Text(text ="Ceci est un Texte. " ,position_in_current_CS=np.array([-1.0, 0.0, -1.0]),visual_angle_of_centered_x_height_deg=1)
    my_text.rotate_about_object_y(180)
    my_scene.place( my_text,my_world)
    
    # Display axes of Tangent Screen
    gizmo = PTVR.Stimuli.Objects.Gizmo ( length = length_of_gizmo_axes, 
                                         width = width_of_gizmo_axes )
    gizmo.set_parent(parent =  my_text)
    my_scene.place(gizmo,my_world)
    
    # Get origin and axes of Cube
    my_text_origin = my_text.get_position()
    my_text_rightward_axis = my_text.get_rightward_axis()
    my_text_upward_axis  = my_text.get_upward_axis()
    my_text_forward_axis = my_text.get_forward_axis()

    # Display information
    gizmo_information = gizmo_information_to_string(my_text_origin, my_text_rightward_axis,my_text_upward_axis,my_text_forward_axis)
    text = PTVR.Stimuli.Objects.Text(text = gizmo_information,visual_angle_of_centered_x_height_deg=1)
    text.set_parent(my_text,y_local = -0.15)
    my_scene.place(text,my_world)


    my_world.add_scene ( my_scene )
  
    my_world.write()  # Write The3DWorld to .json file

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld()