#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""""
...\PTVR_Researchers\Python_Scripts\Demos\Coordinate_Systems\
25_be_cautious_w_SEVERAL_coordinate_transformations.py


Goal: illustrates a common mistake that you must avoid when using several
    coordinate transformations.
        
What you see :
    If the code is correct (ie with is_code_correct = True),
    you see a tilted cylinder at the bottom
    and another tilted cylinder at the top. 
    
    
    If the code is not correct (ie with is_code_correct = False),
    you see both cylinders superimposed at the top.

Explanation: when you want objects to be in a given Coordinate System, you have
        to place them (with the place() function ) BEFORE the next 
        coordinate transformation.
        
    In pseudo-code this means the following:
        
        ----------------------------------------
        A/ The following pseudo-code is correct:
        
        (Let's call the current CS "CS_1")
            
        Create an object_1
        Place object_1 (its coordinates will be specified in CS_1)
        
        Do the next coordinate transformation
        (Let's call the current CS "CS_2")
    
        Create an object_2
        Place object_2 (its coordinates will be specified in CS_2)

             
        --------------------------------------------
        B/ The following pseudo-code is NOT correct:
        (Let's call the current CS "CS_1")            
            
        Create an object_1
        
        Do the next coordinate transformation
        (Let's call the current CS "CS_2")
    
        Place object_1 -> its coordinates will be specified in CS_2 !!!!!!!
            ( we did not want to place object_1 in CS_2 !!!!! )         
    
        Create an object_2
        Place object_2 (its coordinates will be specified in CS_2)             
                 
                 


@author: E.Castet (28 october 2024)
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes as scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import coordinate_system_drawing_utils as draw_CS

height_of_point_P = 1.2 # in meters: corresponds to headset height when subject is seated
cylinders_coordinates = np.array ( [0, 0, 3 ] )

upward_translation = np.array ( [0, height_of_point_P, 0 ] )  # translation
# used for coordinate transformation


def draw_cylinder_with_identical_position_coordinates ( rotation ):
    ##########################
    cylinder = PTVR.Stimuli.Objects.Cylinder ( 
                        position_in_current_CS =  cylinders_coordinates, 
                        size_in_meters = 0.08,
                        rotation_in_current_CS = rotation ) 
    return cylinder

def main():
    my_world = visual.The3DWorld ()
    my_scene  = scenes.VisualScene ()      

    # Try with True or False
    is_code_correct = True 
       
    # Bottom cylinder
    my_rotation = np.array ( [0, 0, 45 ] )  # counterClockwise as seen from the origin
    cylinder_1 = draw_cylinder_with_identical_position_coordinates ( my_rotation )
    
    # Placing cylinder_1 immediately after its creation is correct 
    if is_code_correct == True:
        my_scene.place (cylinder_1, my_world)  
    
    # Coordinate Transformation
    my_world.translate_coordinate_system_along_global ( upward_translation )
    
    # Top cylinder    
    my_rotation = np.array ( [0, 0, -45 ] )  # Clockwise as seen from the origin 
    cylinder_2 = draw_cylinder_with_identical_position_coordinates ( my_rotation )
    
    # A common mistake is to place cylinder_1 after the coordinate transformation,
    # in which case both cylinders will be placed in the 
    # upward-translated Coordinate System (and will be therefore superimposed).
    if is_code_correct == False:
        my_scene.place (cylinder_1, my_world)  
        
    my_scene.place (cylinder_2, my_world)  

    
    my_world.add_scene (my_scene)
    my_world.write() # Write The3DWorld to .json file
# end of main()

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld ( )
