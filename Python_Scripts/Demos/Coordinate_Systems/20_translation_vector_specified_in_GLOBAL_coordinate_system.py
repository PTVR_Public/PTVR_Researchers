#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""""
...\PTVR_Researchers\Python_Scripts\Demos\Coordinate_Systems\
translation_vector_specified_in_GLOBAL_coordinate_system.py

Goal:   This demo illustrates how to translate a coordinate system
        when the Translations axes are aligned with the axes of the GLOBAL CS,
        as explained in the User Manual's Page ->
    Section : 'Translation vector specified in GLOBAL CS'

What you see:
This demo shows the content of a 3D figure that you can find in the Documentation:
    User Manual
        -> Placing objects after coordinate transformations 
            -> Transformations defined in GLOBAL CS 
                -> Translations 
or directly:                
https://ptvr_public.gitlabpages.inria.fr/PTVR_Researchers/
shifting_the_origin_of_room_calibration_c_s_global.html

@author: E.Castet (26 march 2022)
last checked: 16 May 2022
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import coordinate_system_drawing_utils as draw_CS

height_of_point_P = 1.2 # in meters: corresponds to headset height when subject is seated
global_coordinates_of_orange_point = np.array ( [0, height_of_point_P, 0 ] )  
global_coordinates_of_point_P = np.array ( [0, height_of_point_P , 1.2 ] )  

def main():
    my_world = visual.The3DWorld (name_of_subject = "toto")
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()      
    draw_CS.draw_current_cartesian_CS (my_world,my_scene, axis_length=10, 
                                       CS_axes_text=" Global ")  # draw GLOBAL Axes
    #########
    # STEP 1 (figure 1): creates a pink Point P in the global coordinate system (World space).
    # Create a pink sphere approximately in front of your head (about 1.2 meters when seated).
    # Remember that at this stage the current coordinate system is the Global CS.
    my_pink_point_P = PTVR.Stimuli.Objects.Sphere ( 
                            position_in_current_CS = global_coordinates_of_point_P, 
                            color = color.RGBColor (r = 1, g=0.2, b=1), size_in_meters = 0.1 ) 
    my_scene.place (my_pink_point_P,my_world)  
    my_pink_point_P_text = PTVR.Stimuli.Objects.Text( text = 'P point', horizontal_alignment ="Left",
            position_in_current_CS = np.array ( [0.6, 1.2, 1.2 ]), 
            fontsize_in_postscript_points= 400,             
            color= color.RGBColor(r = 1, g=0.2, b= 1) ) 
    my_scene.place (my_pink_point_P_text,my_world)
      
    #`create orange point
    my_orange_point = PTVR.Stimuli.Objects.Sphere ( 
                        position_in_current_CS = global_coordinates_of_orange_point, 
                        color = color.RGBColor (r = 1, g=0.5, b=0), size_in_meters = 0.05) 
    my_scene.place (my_orange_point, my_world) 
    
    ##########  
    # STEP 2 : translate / Shift CURRENT coordinate system
    my_world.translate_coordinate_system_along_global( global_coordinates_of_orange_point)
    draw_CS.draw_current_cartesian_CS (my_world,my_scene, axis_length=1.2,  
                                       CS_axes_text = "   Current ")    
    ########## 
    # STEP 3 (figure 2): creates a black cylinder M.
    # The cylinder M has the same global location as point P (hence their superimposition) 
    # but here...
    # the cylinder's location is defined in the current system obtained after the 
    # upward translation in step 2    
    my_cylinder_M = PTVR.Stimuli.Objects.Cylinder ( 
                        position_in_current_CS = np.array ( [0, 0, 1.2 ] ) , 
                        color = color.RGBColor (r = 0, g=0, b=0), size_in_meters = 0.08 ) 
    my_scene.place (my_cylinder_M,my_world)  
    my_black_cylinder_M_text = PTVR.Stimuli.Objects.Text( 
            text = 'M cylinder', horizontal_alignment = "Left",
            position_in_current_CS = np.array ( [0.45, 0.2, 1.2 ]), 
            fontsize_in_postscript_points= 400 )
    my_scene.place (my_black_cylinder_M_text,my_world)     
    
    ############################################
    # text to the left : explanations for Users
    my_text = PTVR.Stimuli.Objects.Text (
        text = '1/ This demo script is an immersion within a figure that\n'
                'you can find in the Documentation to get all explanations ->\n'
                'See the web link in the header of the current script.\n\v'
                '   Briefly, you can see here an upward translation\n'
                'of the global coordinate system.\n'  , 
        fontsize_in_postscript_points = 100,                
        position_in_current_CS = np.array ([-0.7, 0, 0.7]), 
        rotation_in_current_CS = np.array ([0, -45, 0])
        )            
    my_scene.place (my_text, my_world)      
    
    my_world.add_scene (my_scene)
    my_world.write() # Write The3DWorld to .json file
# end of main()

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld ( )
