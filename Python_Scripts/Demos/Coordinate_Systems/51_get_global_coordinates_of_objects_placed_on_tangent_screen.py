# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\DEMOS\Coordinate_Systems\
get_global_coordinates_of_objects_placed_on_tangent_screen.py

GOAL : 
    - to show that getting the GlOBAL position of an object is the same 
    whether the object is directly placed in the 3D world (see previous demo) or 
    whether the object is placed on a Tangent Screen (present demo).
        In both cases, this is achieved with :
            my_object.get_position ()
    
    - to check if returned global coordinates are correct after 
        coordinate transformations.
    
@author: eric castet    
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import PTVR.Pointing.PointingCursor

PWD = PTVR.SystemUtils.PWD

username = "Julia" # do not comment out this line : username is used to create the name of your results file.

my_viewing_distance_m = 0.57 # in meters

height_of_headset = 1.2 # in meters
if username == "eric": height_of_headset = 1.2

head_viewpoint_position = np.array ([0, height_of_headset, 0] ) 

#----------------

def place_object_on_TS (my_world, my_scene, TS_position):
    # a/ create a Tangent Screen
    # b/ place an object with LOCAL coordinates on this TS 
    # c/ and get the GLOBAL coordinates of this object
    my_tangent_screen = PTVR.Stimuli.Objects.TangentScreen (
        size_in_meters = 0.1 , color = color.RGBColor(r=1, g= 1, b=1) )  
    # position of TS in the CURRENT CS
    my_tangent_screen.set_cartesian_coordinates (
            x = TS_position [0], 
            y = TS_position [1], 
            z = TS_position [2])     
    my_scene.place (my_tangent_screen, my_world)  
    
    # create black sphere at the center of the TS 
    local_position_in_2D_cartesian_coord = \
        np.array ([0,0]) # You play around with these values 
        
    black_sphere_at_center_of_TS = PTVR.Stimuli.Objects.Sphere ( size_in_meters = 0.01 ) 
    black_sphere_at_center_of_TS.set_cartesian_coordinates_on_screen (
            my_2D_screen = my_tangent_screen,
            x_local = local_position_in_2D_cartesian_coord [0], 
            y_local = local_position_in_2D_cartesian_coord [1])
    my_scene.place (black_sphere_at_center_of_TS, my_world)
    
    #####################################################
    #   Get GLOBAL position of BLACK sphere             #
    black_sphere_position_in_global_coord =  \
        black_sphere_at_center_of_TS.get_position ()
    #####################################################    
    print ("\nGET black sphere's global cartesian coordinates: " +
           str (round(black_sphere_position_in_global_coord[0],2))+", " + str (round(black_sphere_position_in_global_coord[1],2))+", "+str (round(black_sphere_position_in_global_coord[2],2)) + "\n"  )
    
    my_text = "Global position of black sphere:\n" + \
                str (round(black_sphere_position_in_global_coord[0],2))+", " + str (round(black_sphere_position_in_global_coord[1],2))+", "+str (round(black_sphere_position_in_global_coord[2],2)) + "\n"
    my_text_above_TS = PTVR.Stimuli.Objects.Text (
        text = my_text, position_in_current_CS = 
                  np.array ([
                    TS_position [0], 
                    TS_position [1] + 0.08, # place text above TS
                    TS_position [2]] ) 
            )
    my_scene.place (my_text_above_TS, my_world)
    
def main():
    my_world = visual.The3DWorld(name_of_subject = username)  
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()

    # A/
    # NO transformation yet
    # TS global position : in front of my FEET at my_viewing_distance_m meters.
    my_TS_position_in_current_CS = np.array ([0, 0, my_viewing_distance_m])
    place_object_on_TS (my_world, my_scene, my_TS_position_in_current_CS)
    
   
    # B/
    # First Coordinate Transformation
    my_world.translate_coordinate_system_along_current (head_viewpoint_position)        
    # TS global position : in front of my HEAD at  my_viewing_distance_m meters.
    my_TS_position_in_current_CS = np.array ([0, 0, my_viewing_distance_m])
    place_object_on_TS (my_world, my_scene, my_TS_position_in_current_CS)
    
    # C/
    # Second Coordinate Transformation
    my_world.rotate_coordinate_system_about_current_y(-90)       
    # TS global position : to your head's left at my_viewing_distance_m meters.
    my_TS_position_in_current_CS = np.array ([0, 0, my_viewing_distance_m])
    place_object_on_TS (my_world, my_scene, my_TS_position_in_current_CS)
    
    
    # D/
    # Third Coordinate Transformation
    my_world.rotate_coordinate_system_about_current_y (-90)       
    # TS global position : behind your head at my_viewing_distance_m meters.
    my_TS_position_in_current_CS = np.array ([0, 0, my_viewing_distance_m])
    place_object_on_TS (my_world, my_scene, my_TS_position_in_current_CS)
    
    # E/
    # 4th Coordinate Transformation
    my_world.rotate_coordinate_system_about_current_y (-90)       
    # TS global position : to your head's right at my_viewing_distance_m meters.
    my_TS_position_in_current_CS = np.array ([0, 0, my_viewing_distance_m])
    place_object_on_TS (my_world, my_scene, my_TS_position_in_current_CS)

    
    my_world.add_scene (my_scene)
    my_world.write() 

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld()
        
