# -*- coding: utf-8 -*-
"""
...\Demos\Coordinate_Systems\axes_of_an_object.py

Goal:   - to help visualize the axes of an object (here a Text)
         as you modify the object's orientation defined in the current CS.
         
         - to illustrate the use of:
             my_object.get_axes ()
     
What you see:
    A text whose orientation in the current CS can be modified by you.
    (you can play around with rotation_about_X, rotation_about_Y and rotation_about_Z)
   
    The global coordinates of each object's axis are shown in the console, 
    and 
    The Coordinate System (CS) of the object is represented by a
                red / green / blue gizmo.
    
For more details on the PTVR Gizmo Object, see in directory...
...\Demos\Objects\...
    notably the script gizmo_to_draw_local_axes.py

"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

height_of_head =  1.0 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head, 0])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint  

text_position_wrt_viewpoint = np.array ( [ 0, 0, 0.7 ] )

rotation_about_X = 60 
rotation_about_Y = 0 
rotation_about_Z = -40 

def main():
    
    my_world = visual.The3DWorld (name_of_subject="Michelle")
    my_world.translate_coordinate_system_along_global (viewpoint_position)
    
    my_scene  = PTVR.Stimuli.Scenes.VisualScene () 
    
    my_sphere = PTVR.Stimuli.Objects.Sphere (
                    size_in_meters = 0.02,
                    position_in_current_CS = text_position_wrt_viewpoint )
    my_text = PTVR.Stimuli.Objects.Text ( 
                text = 'rotation of Text Object: ', visual_angle_of_centered_x_height_deg=2,
                position_in_current_CS = text_position_wrt_viewpoint )
    
    my_text.rotate_about_current_x (rotation_about_X)
    my_text.rotate_about_current_y (rotation_about_Y)
    my_text.rotate_about_current_z (rotation_about_Z)
    
    my_scene.place (my_sphere, my_world) 
    my_scene.place (my_text, my_world)  
        
    ####################    
    # In the headset:  #
    # Each axis of the Text Object is displayed with the 
    # red / green / blue gizmo.
    my_gizmo = PTVR.Stimuli.Objects.Gizmo ( length = 0.1, width = 0.01 )
    my_gizmo.set_parent (parent = my_text)
    my_scene.place (my_gizmo, my_world)
    
    X_axis_of_text_in_global_coord, Y_axis_of_text_in_global_coord, \
        Z_axis_of_text_in_global_coord = my_text.get_axes()     
  
    ####################    
    # In the console:  #
    # For each axis of the Text Object, its three GLOBAL coordinates
    # are displayed in the console.
    print ( " Local X axis of object (in global coord.):\n " + str( X_axis_of_text_in_global_coord ) )
    print ( " Local Y axis of object (in global coord.):\n " + str( Y_axis_of_text_in_global_coord ) )    
    print ( " Local Z axis of object (in global coord.):\n " + str( Z_axis_of_text_in_global_coord ) ) 
          
    # some predictions:
    # if rotation_current_CS = np.array([0, 0, 0]) :
      # X axis of object: [1  0  0]
      # Y axis of object: [0  1  0]
      # Z axis of object: [0  0  1]
      
    # if rotation_current_CS = np.array([0, 90, 0]) : 
      # X axis of object: [0  0  -1]    # local X is now global -Z
      # Y axis of object: [0  1  0]     # local Y is now global Y
      # Z axis of object: [1  0  0]     # local Z is now global X
      
    
    
    my_world.add_scene (my_scene)
      
    my_world.write () # writes to .json file
    print (".json file has been created.")

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld() 
