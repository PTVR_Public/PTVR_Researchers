#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""""
coordinate_system_drawing_utils.py

goal: utility to display axes of coordinate system



@author: E. Castet (02 november 2021)
last checked EC : 7 december 2022

"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Objects
import PTVR.Stimuli.Color as color



def draw_current_cartesian_CS (world, scene, axis_length, CS_axes_text):
    """
    Draws the axes of the current cartesian coordinate system with the PTVR/Unity color code 
        (x: red, y:green, z:blue).

    Parameters
    ----------
    scene : VisualScene scene where objects will be placed.
    axis_length : Length (in meters) of the 3 axes of the coordinate system.
    CS_axes_text : text for each axis' label (e.g. "global X" at the tip of the X axis)
    Returns
    -------
    None.

    """    
    myFontSize = 150
    delta_position_for_axis_label = 0.1
    
    # X AXIS
    XaxisPlus = PTVR.Stimuli.Objects.LineTool ( color=color.RGBColor(r =1.0, g=0.0, b=0.0, a=1.0), width = 0.025,                            
                                              start_point = np.array ( [0, 0, 0] ), end_point = np.array ( [axis_length, 0, 0] ))    
    scene.place (XaxisPlus, world) 
    smallest_x = 2.0
    
    if (axis_length >= 2):
        myText_X_2m = PTVR.Stimuli.Objects.Text (
            text = f'X = {smallest_x} meters', fontsize_in_postscript_points = myFontSize, color= color.RGBColor(r = 1.0, g= 0, b= 0, a=1.0), 
            position_in_current_CS = np.array ([smallest_x, 0, -0.3]),
            rotation_in_current_CS = np.array ( [0, 90, 0])  # rotation about Y axis      # hotfix: ne fonctionne plus le 9 mai) 
                    
             ) 
        #myText_X_2m.rotate_about_current_y (90) # pour compenser le hotfix ci-dessus
        scene.place (myText_X_2m, world) 
     
    if (axis_length >= 4):
        myText_4m = PTVR.Stimuli.Objects.Text (
            text = f'X = {smallest_x * 2} meters', fontsize_in_postscript_points = myFontSize, color= color.RGBColor(r = 1.0),
            position_in_current_CS = np.array ([smallest_x*2, 0, -0.3]),
            rotation_in_current_CS = np.array ( [0, 90, 0])  # rotation about Y axis     # hotfix: ne fonctionne plus le 9 mai)                             
             ) 
        #myText_4m.rotate_about_current_y (90) # pour compenser le hotfix ci-dessus
        scene.place (myText_4m, world)     

    X_axis_label = PTVR.Stimuli.Objects.Text (
        text = CS_axes_text + ' X (aka rightward)', visual_angle_of_centered_x_height_deg = 1.5, horizontal_alignment = "Left", color= color.RGBColor(r = 1.0),
        position_in_current_CS = np.array ([axis_length + delta_position_for_axis_label, 0, -0.5]),
        rotation_in_current_CS = np.array ( [0, 90, 0]) # rotation about Y axis  # hotfix: ne fonctionne plus le 9 mai)     

         )
    #X_axis_label.rotate_about_current_y (90) # pour compenser le hotfix ci-dessus
    scene.place (X_axis_label, world) 
    
    # Y AXIS 
    YaxisPlus = PTVR.Stimuli.Objects.LineTool ( color=color.RGBColor(r=0.0, g=1.0, b=0.0, a=1.0), width = 0.025,                            
                                              start_point = np.array ( [0, 0, 0] ), end_point = np.array ( [0, axis_length, 0] ))
    scene.place (YaxisPlus, world)     
    
    if (axis_length >= 2):
        myText_Y_2m = PTVR.Stimuli.Objects.Text (
            text = 'Y = 2 meters', fontsize_in_postscript_points = myFontSize, color=color.RGBColor ( g= 1.0 ),
            position_in_current_CS = np.array([0.30, 2, 0.0]),
            rotation_in_current_CS = np.array( [-45, 0, 0] ) # hotfix: ne fonctionne plus le 9 mai)    

             )    
        
        #myText_Y_2m.rotate_about_current_x (-45) # pour compenser le hotfix ci-dessus
        scene.place (myText_Y_2m, world)
         
    Y_axis_label = PTVR.Stimuli.Objects.Text (
        text = CS_axes_text + ' Y (aka upward)',  visual_angle_of_centered_x_height_deg = 1.5, horizontal_alignment = "Left", color= color.RGBColor(g = 1.0),
        position_in_current_CS = np.array ([0.5, axis_length + delta_position_for_axis_label, 0]),
        rotation_in_current_CS = np.array( [-45, 0, 0])  # hotfix: ne fonctionne plus le 9 mai)    
         )
    #Y_axis_label.rotate_about_current_x (-45) # pour compenser le hotfix ci-dessus
    scene.place (Y_axis_label, world) 
       
    # Z AXIS
    ZaxisPlus = PTVR.Stimuli.Objects.LineTool (color = color.RGBColor(r =0.0, g=0.0 ,b=1.0, a=1.0), 
                                         width = 0.025,                            
        start_point = np.array ( [0, 0, 0] ), end_point = np.array ( [0, 0, axis_length] ))     
    scene.place (ZaxisPlus, world) 
    smallest_z = 2

    if (axis_length >= 2):
        myText_Z_2m = PTVR.Stimuli.Objects.Text (
            text = f'Z = {smallest_z} meters',  fontsize_in_postscript_points = myFontSize, color=color.RGBColor (b = 1.0), 
            position_in_current_CS = np.array ([0.5, 0, smallest_z] )
             )
        scene.place (myText_Z_2m, world) 
    if (axis_length >= 4):
        myText_Z_4m = PTVR.Stimuli.Objects.Text (
            text = f'Z = {smallest_z*2} meters', fontsize_in_postscript_points = myFontSize, color=color.RGBColor (b = 1.0),
            position_in_current_CS = np.array ( [0.5, 0, smallest_z*2] )
             )
        scene.place (myText_Z_4m, world) 
    if (axis_length >= 10):    
        myText_Z_10m = PTVR.Stimuli.Objects.Text (
            text = f'Z = {smallest_z*4} meters', fontsize_in_postscript_points = myFontSize, color=color.RGBColor (b = 1.0),
            position_in_current_CS = np.array([0.5, 0, smallest_z*4] )
             )  
        scene.place (myText_Z_10m, world) 

    Z_axis_label = PTVR.Stimuli.Objects.Text (
        text = CS_axes_text + ' Z (aka forward)', visual_angle_of_centered_x_height_deg = 1.5, horizontal_alignment = "Left", color= color.RGBColor(b = 1.0),
        position_in_current_CS = np.array ([0.5, 0, axis_length + delta_position_for_axis_label]),
        rotation_in_current_CS = np.array ( [0, 0, 0]) 
         )
    scene.place (Z_axis_label, world) 

    
# end of def draw_current_cartesian_CS (scene)    
