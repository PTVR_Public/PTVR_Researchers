# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\DEMOS\Coordinate_Systems\
50_get_global_coordinates_of_objects.py

GOAL : 
    - to show how to get the GlOBAL position of any object after some
    coordinate transformations.
    This is illustrated with:
    a cube
    and
    a Tangent Screen

        In all cases, this is achieved with :
            my_object.get_position ()
    
    - to check if returned global coordinates are correct after 
        coordinate transformations.
    
@author: eric castet    
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects

import PTVR.Stimuli.Color as color


PWD = PTVR.SystemUtils.PWD

username = "Julia" # do not comment out this line : username is used to create the name of your results file.

my_viewing_distance_m = 0.57 # in meters

height_of_headset = 1.2 # in meters
if username == "eric": height_of_headset = 1.2

head_viewpoint_position = np.array ([0, height_of_headset, 0] ) 

#----------------

def create_cube (my_world, my_scene, a_position):
    # a/ create a Tangent Screen
    # b/ and get the GLOBAL coordinates of this object
    my_cube = PTVR.Stimuli.Objects.Cube(
        size_in_meters = 0.05 , color = color.RGBColor(r=1, g= 1, b=1) )  
    # position of TS in the CURRENT CS
    my_cube.set_cartesian_coordinates (
            x = a_position [0], 
            y = a_position [1], 
            z = a_position [2])     
    my_scene.place (my_cube, my_world)  
    
    #####################################################
    #   Get GLOBAL position of TS         #
    cube_position_in_global_coord =  \
        my_cube.get_position ()
    #####################################################    
    print ("\nGET CUBE global cartesian coordinates: " +
           str (round(cube_position_in_global_coord[0],2))+", " + str (round(cube_position_in_global_coord[1],2))+", "+str (round(cube_position_in_global_coord[2],2)) + "\n"  )
    
    my_text = "Global position of CUBE:\n" + \
                str (round(cube_position_in_global_coord[0],2))+", " + str (round(cube_position_in_global_coord[1],2))+", "+str (round(cube_position_in_global_coord[2],2)) + "\n"
    my_text_above_cube = PTVR.Stimuli.Objects.Text (
        text = my_text, position_in_current_CS = 
                  np.array ([
                    a_position [0], 
                    a_position [1] + 0.06, # place text above cube
                    a_position [2]] ) 
            )
    my_scene.place (my_text_above_cube, my_world)
    
# END of create_cube ()   
#------------------------------------------------

def create_TS (my_world, my_scene, a_position):
    # a/ create a Tangent Screen
    # b/ and get the GLOBAL coordinates of this object
    my_tangent_screen = PTVR.Stimuli.Objects.TangentScreen (
        size_in_meters = 0.1 , color = color.RGBColor(r=1, g= 1, b=1) )  
    # position of TS in the CURRENT CS
    my_tangent_screen.set_cartesian_coordinates (
            x = a_position [0], 
            y = a_position [1], 
            z = a_position [2])     
    my_scene.place (my_tangent_screen, my_world)  
    
    #####################################################
    #   Get GLOBAL position of TS         #
    TS_position_in_global_coord =  \
        my_tangent_screen.get_position ()
    #####################################################    
    print ("\nGET TS global cartesian coordinates: " +
           str (round(TS_position_in_global_coord[0],2))+", " + str (round(TS_position_in_global_coord[1],2))+", "+str (round(TS_position_in_global_coord[2],2)) + "\n"  )
    
    my_text = "Global position of TS:\n" + \
                str (round(TS_position_in_global_coord[0],2))+", " + str (round(TS_position_in_global_coord[1],2))+", "+str (round(TS_position_in_global_coord[2],2)) + "\n"
    my_text_above_TS = PTVR.Stimuli.Objects.Text (
        text = my_text, position_in_current_CS = 
                  np.array ([
                    a_position [0], 
                    a_position [1] + 0.08, # place text above TS
                    a_position [2]] ) 
            )
    my_scene.place (my_text_above_TS, my_world)
    
# END of create_TS ()   
#------------------------------------------------
    
def main():
    my_world = visual.The3DWorld(name_of_subject = username)  
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()

    # A/
    # NO transformation yet
    # TS global position : in front of my FEET at my_viewing_distance_m meters.
    my_TS_position_in_current_CS = np.array ([0, 0, my_viewing_distance_m])
    create_TS (my_world, my_scene, my_TS_position_in_current_CS)

    my_cube_position_in_current_CS = np.array ([0, 0.2, my_viewing_distance_m])
    create_cube (my_world, my_scene, my_cube_position_in_current_CS)
    
   
    # B/
    # First Coordinate Transformation
    my_world.translate_coordinate_system_along_current (head_viewpoint_position)        
    # TS global position : in front of my HEAD at  my_viewing_distance_m meters.
    my_TS_position_in_current_CS = np.array ([0, 0, my_viewing_distance_m])
    create_TS (my_world, my_scene, my_TS_position_in_current_CS)

    my_cube_position_in_current_CS = np.array ([0, 0.2, my_viewing_distance_m])
    create_cube (my_world, my_scene, my_cube_position_in_current_CS)
  
    
    # C/
    # Second Coordinate Transformation
    my_world.rotate_coordinate_system_about_current_y(-90)       
    # TS global position : to your head's left at my_viewing_distance_m meters.
    my_TS_position_in_current_CS = np.array ([0, 0, my_viewing_distance_m])
    create_TS (my_world, my_scene, my_TS_position_in_current_CS)

    my_cube_position_in_current_CS = np.array ([0, 0.2, my_viewing_distance_m])
    create_cube (my_world, my_scene, my_cube_position_in_current_CS)
    
    
    # D/
    # Third Coordinate Transformation
    my_world.rotate_coordinate_system_about_current_y (-90)       
    # TS global position : behind your head at my_viewing_distance_m meters.
    my_TS_position_in_current_CS = np.array ([0, 0, my_viewing_distance_m])
    create_TS (my_world, my_scene, my_TS_position_in_current_CS)
    
    my_cube_position_in_current_CS = np.array ([0, 0.2, my_viewing_distance_m])
    create_cube (my_world, my_scene, my_cube_position_in_current_CS)

    # E/
    # 4th Coordinate Transformation
    my_world.rotate_coordinate_system_about_current_y (-90)       
    # TS global position : to your head's right at my_viewing_distance_m meters.
    my_TS_position_in_current_CS = np.array ([0, 0, my_viewing_distance_m])
    create_TS (my_world, my_scene, my_TS_position_in_current_CS)

    my_cube_position_in_current_CS = np.array ([0, 0.2, my_viewing_distance_m])
    create_cube (my_world, my_scene, my_cube_position_in_current_CS)

    
    my_world.add_scene (my_scene)
    my_world.write() 

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld()
        
