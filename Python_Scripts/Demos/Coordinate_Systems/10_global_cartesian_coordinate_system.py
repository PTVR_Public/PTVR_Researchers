#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""""
...\PTVR_Researchers\Python_Scripts\Demos\Coordinate_Systems\
1_global_cartesian_coordinate_system.py


Immerse yourself in the ...
Global (aka as World) coordinate system of PTVR (and of Unity).

This is probably the most important demo to start learning the 3D geometry of PTVR !

@author: E. Castet (02 november 2021)

"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import coordinate_system_drawing_utils as CSdraw 
PWD = PTVR.SystemUtils.PWD
subject_id = "castet"

height_of_viewpoint_in_m = 1.0 # this value is chosen so as to correspond to the height of the subject's headset during
# an experiment (either seated or standing)
if subject_id == "castet": height_of_viewpoint_in_m = 1.2
my_viewpoint_3d_vector = np.array ([0, height_of_viewpoint_in_m, 0])  

my_font_size = 100

def main():
    my_world = visual.The3DWorld (name_of_subject = subject_id)    
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()  

    CSdraw.draw_current_cartesian_CS (my_world, my_scene, axis_length= 10, CS_axes_text= "Global") # in coordinate_system_drawing_utils.py
    
    # text to the left
    my_text_welcome = PTVR.Stimuli.Objects.Text (
        text = '1/ Welcome to the PTVR GLOBAL (aka WORLD)\n Coordinate System !\n'\
                ' Look around ! \n Look at your feet to see the Origin!\n\v'\
                'The global coordinate system is created just after you have\n' \
                'correctly performed the Room Calibration process.', 
        fontsize_in_postscript_points = my_font_size,                
        position_in_current_CS = np.array ([-1.5, height_of_viewpoint_in_m, 1.5]), 
        rotation_in_current_CS = np.array ([0, -45, 0])
        )    
    my_scene.place (my_text_welcome, my_world)  
    
    # text to the right    
    text_to_the_right = '2/ This is a left-handed cartesian coordinate system (as in UNITY) \n with the Z axis (BLUE) pointing in the direction\n' \
        'that was FORWARD (local direction wrt headset)\n at the time of room calibration.\n\n' \
        'Note that only the positive parts of the axes are shown here.'
    my_text_left_handed = PTVR.Stimuli.Objects.Text (
        text=text_to_the_right,  fontsize_in_postscript_points = my_font_size,
        position_in_current_CS = np.array ([1.5, height_of_viewpoint_in_m, 1.5]) ,
        rotation_in_current_CS = np.array ([0, 45, 0]) ) 
    my_scene.place (my_text_left_handed,  my_world) 
    
    # text at Origin
    my_text_origin = PTVR.Stimuli.Objects.Text (
        text = 'Global Origin', fontsize_in_postscript_points = my_font_size * 2,
        position_in_current_CS = np.array ( [0, 0, -0.06] ) , 
        rotation_in_current_CS = np.array ( [60, 0, 0] ) 
         )        
    my_scene.place (my_text_origin, my_world) 

                
    my_world.add_scene (my_scene)
    my_world.write() # write the experiment to .JSON file
# end of main()


if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld()
