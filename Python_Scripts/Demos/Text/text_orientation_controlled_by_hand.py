# -*- coding: utf-8 -*-
"""
...\Demos\Text\text_orientation_controlled_by_hand.py

Goal:
       illustrates the use of 
       rotate_to_look_at ()
       and
       rotate_to_look_at_in_opposite_direction ()
       
What you see:
   text orientation is controlled by (or contingent on ) your hand controller.
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

height_of_head =  1.0 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head , 0])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint

def main():
    my_world = visual.The3DWorld (name_of_subject="Michelle")
    my_world.translate_coordinate_system_along_global (viewpoint_position)
    my_scene  = PTVR.Stimuli.Scenes.VisualScene (is_right_hand_controller_visible = True)    

    # To the left
    # Text #1 defined with postscript points
    my_text_1 = PTVR.Stimuli.Objects.Text ( text = 'Change my orientation \n with hand controller !', 
                                        position_in_current_CS = np.array ( [ -0.3, 0, 1 ] ),
                                        fontsize_in_postscript_points = 60  )
    my_text_1.rotate_about_current_y (30)
    my_text_1.rotate_to_look_at_in_opposite_direction (my_world.handControllerRight.id)
    my_scene.place (my_text_1, my_world)
     
    # To the right
    # Text #2 defined with angular size
    my_text_2 = PTVR.Stimuli.Objects.Text ( text = 'Change my orientation \n with hand controller !', 
                                        position_in_current_CS = np.array ( [ 0.3, 0, 1 ] ),
                                       visual_angle_of_centered_x_height_deg = 1.0 )
    my_text_2.rotate_about_current_z (45)
    my_text_2.rotate_to_look_at ( my_world.handControllerRight.id ) # works fine on 16 March 2023
    my_scene.place (my_text_2, my_world)    
    
    my_world.add_scene (my_scene)
     
    my_world.write () # writes the experiment to .json file
    print (".json file has been created.")

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld() 
