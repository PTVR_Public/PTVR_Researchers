#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" ...\demos\text\text_height_at_different_radial_distances.py

Goal: illustrates the control of the angular size of text (x-height).

Predictions:  
    a/ When the vertical size of strings of text placed at different radial distances 
    from head_viewpoint_position is defined by setting a constant vertical
    ANGULAR SIZE (x-height)  , ...
    
    ... then these strings of texts should subtend the same angular size.
    
Observations in the present demo: ( Close one eye to perform this test ) .
    A text at 1 meter is visually coinciding with a text at 3 meters because
    their X-HEIGHT in degrees of visual angle is set to a constant value 
    (here 3 degrees of visual angle subtended at head_viewpoint_position) 
    
There are 3 spatial configurations, each demonstrating an important point indicated
 in the legend:
    
1. straight ahead i.e. forward

2. 45° to the left

3. 45° to the right

Note : When visually controlling angular values, remember that :
        a/ angular values are accurate only when the eyes lie
        close to the current Coordinate System's origin. Move your head if it
        is not the case, you can use the Gizmo below your feet: if your head is
        aligned with the green axis (Y), it will be fine.
        b/ the control of angular values is also simpler when you close one eye.

@author: E. Castet (18 november 2021)
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import PTVR.Tools as tools
# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Eric"

# this value is chosen so as to correspond to the height of the subject's headset during
height_of_head_viewpoint = 1.0
# an experiment (either seated or standing)

if username == "Eric":
    height_of_head_viewpoint = 1.2

head_viewpoint_position = np.array([0, height_of_head_viewpoint, 0])

x_height_in_degrees = 3

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================


def main():
    my_world = visual.The3DWorld(name_of_subject=username)

    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    # Gizmo at the origin of the Global Coordinate System
    my_gizmo = PTVR.Stimuli.Objects.Gizmo()
    my_scene.place(my_gizmo, my_world)

    my_world.translate_coordinate_system_along_global(
        translation=head_viewpoint_position)
    current_cs_pos_x, current_cs_pos_y, current_cs_pos_z, current_cs_axis_x, current_cs_axis_y, current_cs_axis_z = my_world.get_coordinate_system()
    current_cs_origin = np.array(
        [current_cs_pos_x, current_cs_pos_y, current_cs_pos_z])

    # my_origin = PTVR.Stimuli.Objects.Sphere(size_in_meters=0.01)
    # my_scene.place(my_origin,my_world)

    ##############################
    # Configuration straight ahead
    # the 2 strings of text are written with the same font (namely the PTVR default font 'Arial' )

    # Near text
    distance_origin_cube = 1.0  # in meters

    my_near_text_forward = PTVR.Stimuli.Objects.Text(text='x x',
                                                     position_in_current_CS=np.array(
                                                         [0, 0, distance_origin_cube]),
                                                     visual_angle_of_centered_x_height_deg=x_height_in_degrees)
    my_scene.place(my_near_text_forward, my_world)

    # Near cube
    # cube_size_in_meters = 1
    cube_size_in_meters = tools.visual_angle_to_size_on_perpendicular_plane(
        x_height_in_degrees, distance_origin_cube)
    my_near_cube_forward = PTVR.Stimuli.Objects.Cube(size_in_meters=cube_size_in_meters,
                                                     position_in_current_CS=np.array(
                                                         [0, 0, distance_origin_cube]),
                                                     color=color.RGBColor(r=1, g=0, b=0, a=1))

    my_scene.place(my_near_cube_forward, my_world)

    # Far text
    distance_origin_cube = 3  # in meters
    my_far_text_forward = PTVR.Stimuli.Objects.Text(text='x x',
                                                    horizontal_alignment='center', color=color.RGBColor(r=0.0, g=1.0, b=0.0, a=1.0),
                                                    position_in_current_CS=np.array(
                                                        [0, 0, distance_origin_cube]),
                                                    visual_angle_of_centered_x_height_deg=x_height_in_degrees)
    my_scene.place(my_far_text_forward, my_world)

    # Far cube
    cube_size_in_meters = tools.visual_angle_to_size_on_perpendicular_plane(
        x_height_in_degrees, distance_origin_cube)
    my_far_cube_forward = PTVR.Stimuli.Objects.Cube(size_in_meters=cube_size_in_meters,
                                                    position_in_current_CS=np.array(
                                                        [0, 0, distance_origin_cube]),
                                                    color=color.RGBColor(r=0, g=1, b=0, a=1))
    my_scene.place(my_far_cube_forward, my_world)

    # Far legend
    myLegendStraightAhead = PTVR.Stimuli.Objects.Text(
        text=f'Forward configuration. \n Angular size of x-height \n is {
            x_height_in_degrees} degrees\n for both x strings',
        visual_angle_of_centered_x_height_deg=0.5)
    myLegendStraightAhead.set_perimetric_coordinates(
        radialDistance=distance_origin_cube,
        eccentricity=10, halfMeridian=90)
    my_scene.place(myLegendStraightAhead, my_world)

    # legend above and still straight ahead
    myLegendAbove_pos = tools.global_to_local_cartesian_coordinates(
        point_in_global_cs=my_far_cube_forward.get_position(),
        local_cs_origin=current_cs_origin,
        local_cs_axis_X=current_cs_axis_x,
        local_cs_axis_Y=current_cs_axis_y,
        local_cs_axis_Z=current_cs_axis_z) + np.array([0, 1, 0])

    myLegendAbove = PTVR.Stimuli.Objects.Text(
        text='Task in the middle and left spatial configurations :\n close one eye and move your head\n until you visually align \
the near and far x strings.\n Once this is achieved,\n your headset is at the origin of the current Coordinate System !',
        visual_angle_of_centered_x_height_deg=0.6, position_in_current_CS=myLegendAbove_pos)

    my_scene.place(myLegendAbove, my_world)

    ##############################
    # Configuration 45° to the left
    # the 2 strings of text are written with different font (namely the PTVR default font 'Arial' )

    # Near text
    distance_origin_cube = 1.0  # in meters
    my_near_text_left = PTVR.Stimuli.Objects.Text(text='x x',
                                                  visual_angle_of_centered_x_height_deg=x_height_in_degrees)
    my_near_text_left.set_perimetric_coordinates(
        radialDistance=distance_origin_cube, eccentricity=45, halfMeridian=180)
    # my_near_text.is_facing_origin = True # not functional in July 2022
    my_near_text_left.rotate_to_look_at(my_world.originCurrentCS.id)
#    my_near_text.set_orientation(is_facing_origin= True) # easier than next commented line
    # my_near_text.rotate_about_current_y (-45) # same value as eccentricity in the line above
    my_scene.place(my_near_text_left, my_world)

    # Near cube
    cube_size_in_meters = tools.visual_angle_to_size_on_perpendicular_plane(
        x_height_in_degrees, distance_origin_cube)
    my_near_cube_left = PTVR.Stimuli.Objects.Cube(size_in_meters=cube_size_in_meters,
                                                  color=color.RGBColor(r=1, g=0, b=0, a=1))
    my_near_cube_left.set_perimetric_coordinates(radialDistance=distance_origin_cube,
                                                 eccentricity=45, halfMeridian=180)

    my_near_cube_left.rotate_to_look_at(my_world.headset.id)
    # my_near_cube.rotate_about_current_y (-45) # same value as eccentricity in the line above
    my_scene.place(my_near_cube_left, my_world)

    # Far text (note the different font)
    distance_origin_cube = 3  # in meters
    my_far_text_left = PTVR.Stimuli.Objects.Text(text='x x',
                                                 font_name='CourierNew', color=color.RGBColor(r=0.0, g=1.0, b=0.0, a=1.0),
                                                 visual_angle_of_centered_x_height_deg=x_height_in_degrees)
    my_far_text_left.set_perimetric_coordinates(radialDistance=distance_origin_cube,
                                                eccentricity=45, halfMeridian=180)

    # easier than next commented line
    my_far_text_left.rotate_to_look_at(my_world.originCurrentCS.id)
    # my_far_text.rotate_about_current_y (-45) # same value as eccentricity in the line above
    my_scene.place(my_far_text_left, my_world)

    # Far cube
    cube_size_in_meters = tools.visual_angle_to_size_on_perpendicular_plane(
        x_height_in_degrees, distance_origin_cube)
    my_far_cube_left = PTVR.Stimuli.Objects.Cube(size_in_meters=cube_size_in_meters, position_in_current_CS=np.array([0.0, 0.0, distance_origin_cube]),
                                                 color=color.RGBColor(r=0, g=1, b=0, a=1))
    my_far_cube_left.set_perimetric_coordinates(radialDistance=distance_origin_cube,
                                                eccentricity=45, halfMeridian=180)

    # easier than next commented line
    my_far_cube_left.rotate_to_look_at(my_world.originCurrentCS.id)
    # my_far_cube.rotate_about_current_y (-45) # same value as eccentricity in the line above
    my_scene.place(my_far_cube_left, my_world)
    sphere_test = PTVR.Stimuli.Objects.Sphere(size_in_meters=0.05)
    sphere_test.set_perimetric_coordinates(radialDistance=distance_origin_cube,
                                           eccentricity=45, halfMeridian=180)
    my_scene.place(sphere_test, my_world)

    # Far legend of leftward configuration
    my_legend_to_the_left_pos = tools.global_to_local_cartesian_coordinates(point_in_global_cs=my_far_cube_left.get_position(
    ), local_cs_origin=current_cs_origin, local_cs_axis_X=current_cs_axis_x, local_cs_axis_Y=current_cs_axis_y, local_cs_axis_Z=current_cs_axis_z) + np.array([0, 0.5, 0])

    my_legend_to_the_left = PTVR.Stimuli.Objects.Text(
        text=f'Configuration on the left.\n Both x strings have different fonts\n but the same angular x-height ({
            x_height_in_degrees} degrees)',
        visual_angle_of_centered_x_height_deg=0.5, position_in_current_CS=my_legend_to_the_left_pos)
    my_legend_to_the_left.rotate_to_look_at_in_opposite_direction(
        my_world.originCurrentCS.id)  # easier than next commented line
    # my_legend_to_the_left.rotate_about_current_y (-45) # same value as eccentricity in the line above
    my_scene.place(my_legend_to_the_left, my_world)

    #######################################
    # Configuration 45° to the RIGHT
    # the vertical height of both strings of text is
    # constant in postscript points (hence in meters)

    # Near text
    distance_origin_cube = 1.0  # in meters
    my_font_size_in_postscript_points = 360

    my_near_text_right = PTVR.Stimuli.Objects.Text(text='x x',
                                                   font_name='TimesNewRoman',
                                                   fontsize_in_postscript_points=my_font_size_in_postscript_points)
    my_near_text_right.set_perimetric_coordinates(
        radialDistance=distance_origin_cube, eccentricity=45, halfMeridian=0)

    # same value as eccentricity in the line above
    my_near_text_right.rotate_about_current_y(45)
    my_scene.place(my_near_text_right, my_world)

    # Near cube
    # cube_size_in_meters = 0.05 # in meters
    # I want my cube to have the same height in meters as the height of my x characters
    my_cube_size_in_meters = my_font_size_in_postscript_points * 0.00035 * 0.60

    # my_cube_size_in_meters = my_font_size_in_postscript_points * 0.00035 * 0.45
    # la valeur 0.45 pour Times New Roman semble trop petite de Bigelow
    # Bigelow : Table 1 p. 2 and  formula (2) in p.3
    # il semble qu'il faille utiliser 0.60 plutôt que 0.45
    # en fait, il faudrait que je refasse les mesures de Bigelow moi-même

    my_near_cube_right = PTVR.Stimuli.Objects.Cube(size_in_meters=my_cube_size_in_meters,
                                                   color=color.RGBColor(r=1, g=0, b=0, a=1))
    my_near_cube_right.set_perimetric_coordinates(
        radialDistance=distance_origin_cube, eccentricity=45, halfMeridian=0)
    my_near_cube_right.rotate_to_look_at(
        my_world.headset.id)  # easier than next commented line
    # my_near_cube.rotate_about_current_y (45) # same value as eccentricity in the line above
    my_scene.place(my_near_cube_right, my_world)

    # Far text
    distance_origin_cube = 3  # in meters
    my_far_text_right = PTVR.Stimuli.Objects.Text(text="x x",
                                                  font_name='TimesNewRoman',
                                                  fontsize_in_postscript_points=my_font_size_in_postscript_points, color=color.RGBColor(r=0.0, g=1.0, b=0.0, a=1.0))
    my_far_text_right.set_perimetric_coordinates(
        radialDistance=distance_origin_cube, eccentricity=45, halfMeridian=0)

    # same value as eccentricity in the line above
    my_far_text_right.rotate_about_current_y(45)
    my_scene.place(my_far_text_right, my_world)

    # Far cube
    my_far_cube_right = PTVR.Stimuli.Objects.Cube(size_in_meters=my_cube_size_in_meters, color=color.RGBColor(r=0, g=1, b=0, a=1),
                                                  # distance_origin_cube]) )
                                                  position_in_current_CS=np.array([0.0, 0.0, 3.3333333]))
    my_far_cube_right.set_perimetric_coordinates(
        radialDistance=distance_origin_cube, eccentricity=45, halfMeridian=0)

    # same value as eccentricity in the line above
    my_far_cube_right.rotate_about_current_y(45)
    my_scene.place(my_far_cube_right, my_world)

    # Far legend
    my_legend_to_the_right_pos = tools.global_to_local_cartesian_coordinates(point_in_global_cs=my_far_cube_right.get_position(
    ), local_cs_origin=current_cs_origin, local_cs_axis_X=current_cs_axis_x, local_cs_axis_Y=current_cs_axis_y, local_cs_axis_Z=current_cs_axis_z) + np.array([0, 0.5, 0])

    my_legend_to_the_right = PTVR.Stimuli.Objects.Text(
        text=f'Configuration on the right.\nBoth x strings have the same text-height in postscript points (hence in meters) \n as \
defined by font_size = {my_font_size_in_postscript_points}',
        visual_angle_of_centered_x_height_deg=0.5, position_in_current_CS=my_legend_to_the_right_pos)

    # same value as eccentricity in the line above
    my_legend_to_the_right.rotate_about_current_y(45)
    my_scene.place(my_legend_to_the_right, my_world)

    my_world.add_scene(my_scene)
    my_world.write()
# end of main()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
