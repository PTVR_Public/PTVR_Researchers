# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Text\local_z_of_text_on_flat_and_tangent_screens.py.py

Goal : to show the different meanings of the "z_local" parameter when placing text on 
    Flat or Tangent Screens.
    See comments below the local_Z_of_text variable.

What you see: 
    Flat Screens (on the left) and Tangent Screens (on the right).
    Play around with the local_Z_of_text variable and try different signs.
    Beware: when local_Z_of_text is positive, it will be hidden behind the
    Tangent Screens.
    
Remember:
    Note 1:
    When using the set_cartesian_coordinates_on_screen () function, 
    Do not set its z_local parameter to zero as it places the object (to place ON screen)
        right ON the screen and produces flicker (this is a Unity constraint).
        This is why the default value of z_local is -0.001.
        
    Note 2: the term "local" in local_z is a shortcut to say that  
    the z coordinate is defined in the Coordinate System (CS) of the flat
    or tangent screen.

"""
import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

local_Z_of_text = -0.001 # KEY VARIABLE to play around with !!!!
# negative value induces ...
# -> For a Flat Screen
# the text has a negative coordinate in the Flat Screen Coordinate System (CS)
# -> Special meaning For a Tangent Screen
# the text is in front of the TS, i.e. between the TS and the origin of the current CS
#
# If you want text to appear ON the screen, then choose -0.OO1, ie the default
# value of z_local in set_cartesian_coordinates_on_screen ()

angular_x_height = 0.8
height_of_head =  1.0 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head , 0])  

def draw_screen (is_flat, my_text, x_coord_of_screen, z_coord_of_screen, my_scene, my_world):
    # Flat Screen
    if is_flat: 
        my_screen = PTVR.Stimuli.Objects.FlatScreen (size_in_meters = 0.25,
                color=color.RGBColor (r=1, g= 1, b=1),
                position_in_current_CS = np.array ([x_coord_of_screen, 0 , z_coord_of_screen]) )
    # Tangent Screen
    else : 
        my_screen = PTVR.Stimuli.Objects.TangentScreen ( size_in_meters = 0.25,
                color=color.RGBColor (r=1, g= 1, b=1),
                position_in_current_CS = np.array ([x_coord_of_screen, 0 , z_coord_of_screen]) )
    my_scene.place (my_screen, my_world)       
        
    text_object = PTVR.Stimuli.Objects.Text ( text = my_text,
                                    visual_angle_of_centered_x_height_deg = angular_x_height) 

    # # Place 2nd object (In local cartesian coordinates) below the 1st one.
    text_object.set_cartesian_coordinates_on_screen ( my_2D_screen = my_screen, 
                                                      z_local = local_Z_of_text                                                     
                                                     )       
    my_scene.place (text_object, my_world)

    
    
#- fin de draw_screen()
#-----------------------

def main():
    my_world = visual.The3DWorld ( )
    my_world.translate_coordinate_system_along_global (translation = viewpoint_position)

    my_scene  = PTVR.Stimuli.Scenes.VisualScene ( background_color = color.RGBColor(0.74,0.76,0.78,1.0)) 
    my_world.add_scene ( my_scene )

    axis = PTVR.Stimuli.Objects.Gizmo()
    my_scene.place(axis, my_world)

    
    for z_coord_of_screen in np.arange (0.5, -0.6, -0.5):
        # draw Flat Screens
        is_flat = 1 
        x_coord_of_screen = -0.7
        text = "Flat Screen"
        draw_screen (is_flat, text, x_coord_of_screen, z_coord_of_screen, my_scene, my_world)
        
        # draw Tangen Screens
        is_flat = 0 
        x_coord_of_screen = 0.7
        text = "Tangent Screen"
        draw_screen (is_flat, text, x_coord_of_screen, z_coord_of_screen, my_scene, my_world)    

        
    my_world.write()  # Write the experiment to .json file

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld()