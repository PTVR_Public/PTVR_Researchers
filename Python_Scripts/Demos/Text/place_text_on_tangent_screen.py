# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Text\place_text_on_tangent_screen.py

Goal: illustrates how to place text on a Tangent Screen (TS) 
            by using spatial coordinates defined in the 
            coordinate system (CS) of the Tangent Screen (TS).

What you see: a TS in front of your head (when you sit on a chair),
            and "hello world" written on this tangent screen ...
            either in TS perimetric coordinates (for "Hello")
            or in TS cartesian coordinates (for "World!")


"""
import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

viewing_distance_m = 2 # in meters
angular_x_height = 2
height_of_head =  1.0 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head , 0])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint

tangent_screen_position = np.array ([0, 0, 1])


def main():
    my_world = visual.The3DWorld ( )
    my_world.translate_coordinate_system_along_global (translation = viewpoint_position)

    my_scene  = PTVR.Stimuli.Scenes.VisualScene ( background_color = color.RGBColor(0.74,0.76,0.78,1.0)) 
    my_world.add_scene ( my_scene )
                   
    my_tangent_screen = PTVR.Stimuli.Objects.TangentScreen (
                                        color=color.RGBColor(r=1, g= 1, b=1),
                                        position_in_current_CS = tangent_screen_position
                                        )
    my_scene.place (my_tangent_screen, my_world)       
        
    text_object_1 = PTVR.Stimuli.Objects.Text ( text = 'Hello',
                                    visual_angle_of_centered_x_height_deg = angular_x_height) 
    text_object_2 = PTVR.Stimuli.Objects.Text (text = 'WORLD !',
                                    visual_angle_of_centered_x_height_deg = angular_x_height)         
        
    # In TS perimetric coordinates
    # text_object_1.set_perimetric_coordinates_on_screen (
    #                 tangentscreen = my_tangent_screen)  
    # line above is equivalent to its wrapper function below
    text_object_1.set_perimetric_coordinates_on_screen (
                    tangentscreen = my_tangent_screen)      
    
    # # Place 2nd object (In TS cartesian coordinates) below the 1st one.
    text_object_2.set_cartesian_coordinates_on_screen(
                    my_2D_screen = my_tangent_screen, y_local = -0.3)   
    text_object_2.rotate_about_object_z (30)
    
    my_scene.place (text_object_1, my_world)
    my_scene.place (text_object_2, my_world)
        
    my_world.write()  # Write the experiment to .json file

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld()