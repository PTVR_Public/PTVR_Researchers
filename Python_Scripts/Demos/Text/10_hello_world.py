# -*- coding: utf-8 -*-
"""
...\Demos\Text\hello_world.py

Goal : should be obvious :0)


"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

def main():
    my_world = visual.The3DWorld (name_of_subject="Jane Doe")

    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()
    my_text = PTVR.Stimuli.Objects.Text ( text = 'Hello World !', 
                                        position_in_current_CS = np.array ( [ 0, 1, 1 ] ),
                                        fontsize_in_postscript_points = 60
                                    )
    my_scene.place (my_text, my_world)
    my_world.add_scene (my_scene)
     
    my_world.write () # writes to .json file
    print (".json file has been created.")

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld() 
