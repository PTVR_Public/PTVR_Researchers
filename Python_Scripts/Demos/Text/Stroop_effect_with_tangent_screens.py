# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Text\Stroop_effect_with_tangent_screens.py
@author: jdelacha

Goal :  Illustrates the easiness of placing text of different fonts while having
         the same angular size on different Tangent Screens located at differents positions.
"""
import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

viewing_distance_m = 2  # in meters
angular_x_height = 2
# this value is chosen so as to correspond (approximately)
height_of_head = 1.0
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array([0, height_of_head, 0])


def main():
    my_world = visual.The3DWorld()
    my_world.translate_coordinate_system_along_global(
        translation=viewpoint_position)

    my_scene = PTVR.Stimuli.Scenes.VisualScene(
        background_color=color.RGBColor(0.74, 0.76, 0.78, 1.0))
    my_world.add_scene(my_scene)

    text_displayed = ["Blue", 'Green', 'Yellow', 'Pink',
                      'Red', 'Orange', 'Grey', 'Black', 'Purple']
    fonts = ["Arial", 'CourierNew', 'georgia', 'impact', 'LiberationSans', 'segoepr', 'TimesRoman',
             'trebuc', 'verdana']
    colors = [color.RGBColor(r=1), color.RGBColor(b=1), color.RGBColor(r=1),
              color.RGBColor(b=1), color.RGBColor(
                  g=1), color.RGBColor(r=0.8, b=0.8),
              color.RGBColor(r=1, g=0.9, b=0.1), color.RGBColor(r=1), color.RGBColor(b=1)]
    x_position = 1.5
    y_position = 1.6

    screen_positions = [np.array([-x_position, y_position, viewing_distance_m]),
                        np.array([0.0, y_position, viewing_distance_m]),
                        np.array([x_position, y_position, viewing_distance_m]),

                        np.array([-x_position, 0.0, viewing_distance_m]),
                        np.array([0.0, 0.0, viewing_distance_m]),
                        np.array([x_position, 0.0, viewing_distance_m]),

                        np.array(
                            [-x_position, -y_position, viewing_distance_m]),
                        np.array([0.0, -y_position, viewing_distance_m]),
                        np.array(
                            [x_position, -y_position, viewing_distance_m]),
                        ]
    for i in range(len(text_displayed)):
        my_tangent_screen = PTVR.Stimuli.Objects.TangentScreen(
            color=color.RGBColor(r=1, g=1, b=1),
            position_in_current_CS=screen_positions[i])
        my_scene.place(my_tangent_screen, my_world)

        text_object = PTVR.Stimuli.Objects.Text(
            text=text_displayed[i], color=colors[i],
            font_name=fonts[i],
            visual_angle_of_centered_x_height_deg=angular_x_height)
        text_object_2 = PTVR.Stimuli.Objects.Text(
            text=text_displayed[i], color=colors[i],
            font_name=fonts[i-1],
            visual_angle_of_centered_x_height_deg=angular_x_height)

        # In TS perimetric coordinates
        text_object.set_perimetric_coordinates_on_screen(
            tangentscreen=my_tangent_screen,
            eccentricity_local_deg=0,
            half_meridian_local_deg=0)

        # In TS cartesian coordinates
        text_object_2.set_cartesian_coordinates_on_screen(
            my_2D_screen=my_tangent_screen,
            y_local=0.2, z_local=-0.001)

        my_scene.place(text_object, my_world)
        my_scene.place(text_object_2, my_world)

    my_world.write()  # Write The3DWorld to .json file


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
