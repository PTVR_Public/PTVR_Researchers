# -*- coding: utf-8 -*-
"""
...\Demos\Text\20_text_orientation.py

Goal:
    View the effect of varying the Text object's parameter : "rotation_in_current_CS"  
    (i.e. this varies the text's orientation by applying rotations
    in the current CS)
    Most important goal is to show that the default orientation, (i.e.
    when rotation_in_current_CS = np.array([0.0, 0.0, 0.0])  ) 
    is such that it can be READ when it is placed, loosely speaking,
    straight-ahead (in front) of your head.
What you see:
    Straight ahead of your head: a column of text with increasing values
    of the parameter "rotation_in_current_CS" 
    
    To the left of your head: a column of text with increasing values
    of the "rotation_in_current_CS" parameter
    
    To the right of your head: a column of text with increasing values
    of the "rotation_in_current_CS" parameter
    
    These three columns simply aim at making it easier to read the texts when
    the viewpoint is at "viewpoint_position".

"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import PTVR.Tools as tools

height_of_head =  1.2 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head , 0])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint

y = 0
text_above = 'Effect of varying the Text object\'s parameter \'rotation_in_current_CS\'\n \
                i.e. effect of varying text\'s orientation in the CURRENT Coordinate System'
 
def draw_text (rotation, my_world, my_scene):  
    global y
    y = y - 0.1
    my_text = PTVR.Stimuli.Objects.Text ( 
                        text = 'rotation about Y: ' + str (rotation), 
                        visual_angle_of_centered_x_height_deg=2,
                        position_in_current_CS = np.array ( [ 0, y, 1 ] ),
                        rotation_in_current_CS = np.array ([0, rotation, 0])  )
    my_scene.place (my_text, my_world)
    
def main():
    global y
    y=0.2
    my_world = visual.The3DWorld (name_of_subject="Michelle")
    my_world.translate_coordinate_system_along_global (viewpoint_position)
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()    
    
    # Text for instructions
    my_text_above = PTVR.Stimuli.Objects.Text ( visual_angle_of_centered_x_height_deg = 2,
        text = text_above, position_in_current_CS = np.array ( [ 0, y+0.2, 1 ] )   )
    my_scene.place (my_text_above, my_world)
    
    # draw text in front (or straight-ahead) and at different y values wrt current CS  
    # text column straight-ahead of your head 
    y=0.2
    for rotation in range (0, 360, 45):
        draw_text (rotation, my_world, my_scene)
        
        
    # Change current CS to translate text column to the LEFT of your head
    my_world.translate_coordinate_system_along_global (np.array ( [ -1, 0, 0 ] ))
    y= 0.2
    for rotation in range (0, 360, 45):
        draw_text (rotation, my_world, my_scene)        
        
    # Change current CS to translate text column to the RIGHT of your head
    my_world.translate_coordinate_system_along_global (np.array ( [ 2, 0, 0 ] ))
    y= 0.2
    for rotation in range (0, 360, 45):
        draw_text (rotation, my_world, my_scene)    
        
    my_world.add_scene (my_scene)
      
    my_world.write () # writes the experiment to .json file
    print (".json file has been created.")

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld() 
