# -*- coding: utf-8 -*-
"""
...\Demos\Text\Beware_rotate_to_look_at_with_text.py

Goal:

    Most important goal is to show that the default orientation, (i.e.
    when rotation_current_CS = np.array([0.0, 0.0, 0.0])  ) 
    is such that it can be READ when it is placed, loosely speaking,
    straight-ahead (in front) of your head.
    
    Second goal: illustrates that the behaviors of rotate_to_look_at () and
    rotate_to_look_at_in_opposite_direction () when applied TO TEXT 
    are NOT A BUG 
    although they produce a counterintuitive effect.
    
    
What you see:
    The text in front of your head has a zero orientation (0,0,0) and you can read it.

"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

height_of_head =  1.0 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head , 0])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint

def main():
    my_world = visual.The3DWorld (name_of_subject="Michelle")
    my_world.translate_coordinate_system_along_global (viewpoint_position)
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()    
    
    my_text_bug = PTVR.Stimuli.Objects.Text ( 
                                text = 'Counter-intuitive in left and right columms\n \
                                but this is not a bug', 
                                color = color.RGBColor ( r = 1 ),
                                position_in_current_CS = np.array ([0, 0.3, 1]),
                                fontsize_in_postscript_points = 60  )
    my_scene.place (my_text_bug, my_world)
    
    
    # Straight-ahead
    # Text #0 defined with postscript points
    my_text_0 = PTVR.Stimuli.Objects.Text ( text = 'Default Orientation !', 
                                position_in_current_CS = np.array ([0, 0, 1]),
                                fontsize_in_postscript_points = 60  )
    my_scene.place (my_text_0, my_world)
    

    # To the left
    # Text #1 defined with postscript points
    my_text_1 = PTVR.Stimuli.Objects.Text ( text = 'rotate to look at ', 
                                        position_in_current_CS = np.array ( [ -0.8, 0, 1 ] ),
                                        fontsize_in_postscript_points = 60  )
    my_text_1.rotate_to_look_at ( my_world.originCurrentCS.id ) 
    my_scene.place (my_text_1, my_world)
     
    # To the right
    # Text #2 defined with angular size
    my_text_2 = PTVR.Stimuli.Objects.Text ( text = 'rotate_to_look_at_in_opposite_direction', 
                                        position_in_current_CS = np.array ( [ 0.8, 0, 1 ] ),
                                       visual_angle_of_centered_x_height_deg = 1.0 )
    my_text_2.rotate_to_look_at_in_opposite_direction (my_world.originCurrentCS.id )
    my_scene.place (my_text_2, my_world)    
    
    my_world.add_scene (my_scene)
     
    my_world.write () # writes the experiment to .json file
    print (".json file has been created.")

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld() 
