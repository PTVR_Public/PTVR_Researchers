# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Objects\
    angular_size_of_object.py

See general help on angular values in the PTVR Documentation:    
https://ptvr_public.gitlabpages.inria.fr/PTVR_Researchers/visual_angles_in_v_s.html

Goal: shows how to create an object whose size is specified in degrees
    of visual angle, say 2° as in the present manuscript.
    You know that this object is at the center of a tangent screen.
    You also know that the viewing distance of this object is 1.15 meters.
    
    In order to measure the corresponding object's size in meters, you can 
    either use trigonometric calculation or use the following PTVR fonction:
     tools.visual_angle_to_size_on_perpendicular_plane ()
    More details on this calculation can be found here:
     https://ptvr_public.gitlabpages.inria.fr/PTVR_Researchers/how_to_avoid_approximations.html   
    
    When you run the following:
    tools.visual_angle_to_size_on_perpendicular_plane (
        visual_angle_of_centered_object_in_deg = 2, 
        viewing_distance_in_m = 1.15 )    
    the returned value is 0.04 meters.
    In other words the size of the object (here a sphere's diameter) must
    be 0.04 meters to subtend 2 degrees of visual angle at a viewing distance of 1.15 meters.
    
"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere, Cube, Gizmo
import PTVR.Stimuli.Color as color
import numpy as np

from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Tools as tools

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

# Task: you want to create a sphere with the following constraints:
sphere_diameter_in_deg = 2         # 2 degrees
sphere_radial_distance_in_m = 1.15 # 1.15 meters

# Therefore, you must calculate size (in meters) from the sphere's visual angle 
# and from its viewing distance.
# This calculation is done with the following fonction:
    # tools.visual_angle_to_size_on_perpendicular_plane ()
# The Calculation done by the next line of code returns 0.04,
# in other words the size of the sphere must be 0.04 meters to subtend
# 2 degrees of visual angle at a viewing distance of 1.15 meters.
sphere_diameter_in_m =  \
    tools.visual_angle_to_size_on_perpendicular_plane (
        visual_angle_of_centered_object_in_deg = sphere_diameter_in_deg, 
        viewing_distance_in_m = sphere_radial_distance_in_m )

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================


my_world = The3DWorld ()

def main():
    my_scene = VisualScene ()
    
    my_world.translate_coordinate_system_along_global ( np.array([0, 1.2, 0]))
    
    my_sphere = Sphere ( size_in_meters = sphere_diameter_in_m,
                        position_in_current_CS=np.array([0.0, 0.0, 0.2]))

    my_scene.place ( my_sphere, my_world)    
    
    my_world.add_scene (my_scene)
    my_world.write ()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
