# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Objects\
10_primitive_shapes.py

Goal : display some of the primitive PTVR shapes.
                     
Note : the 'size_in_meters' parameter corresponds to the side of an object (e.g.
        a cube) or to the diameter of a sphere.
                                                
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects


def main():

    my_world = visual.The3DWorld ()
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    
    my_cube = PTVR.Stimuli.Objects.Cube ( 
        position_in_current_CS = np.array ( [ -1, 1.2, 2] )  )

    my_sphere = PTVR.Stimuli.Objects.Sphere (
        position_in_current_CS = np.array ( [ 1, 1.2, 2] )  )

    my_cylinder = PTVR.Stimuli.Objects.Cylinder (
        position_in_current_CS = np.array ( [ 1, -0.5, 2] )  )
    
    my_scene.place (my_cube, my_world)
    my_scene.place (my_sphere, my_world)
    my_scene.place (my_cylinder, my_world)
    
    my_world.add_scene (my_scene)   
   
    my_world.write () 
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()