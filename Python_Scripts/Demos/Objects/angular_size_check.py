# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Objects\
    angular_size_check.py


Goal : this script shows a trick to check that the angular size of an object
        is correct. You might want to do that for instance because you have 
        doubts on some fonctions that were used to create this object.
        
        The trick is to use the perimetric coordinate system.
        Say you want your object to be 10 degrees of visual angle.
        
        You actually create two identical objects with this size and you set
        their eccentricity to 1° (ie half their angular size)
        
        The prediction is that both objects should be abutting with each other when
        they are on opposite half-meridians (say 0° and 180°)
    
"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere, Cube, Gizmo
import PTVR.Stimuli.Color as color
import numpy as np

from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Tools as tools

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

# Task: you want to create a sphere with the following constraints:
sphere_diameter_in_deg = 2         # 2 degrees
sphere_radial_distance_in_m = 1.15 # 1.15 meters

# Therefore, you must calculate size (in meters) from the sphere's visual angle 
# and from its viewing distance.
# This calculation is done with the following fonction:
    # tools.visual_angle_to_size_on_perpendicular_plane ()
# The Calculation done by the next line of code returns 0.04,
# in other words the size of the sphere must be 0.04 meters to subtend
# 2 degrees of visual angle at a viewing distance of 1.15 meters.
sphere_diameter_in_m =  \
    tools.visual_angle_to_size_on_perpendicular_plane (
        visual_angle_of_centered_object_in_deg = sphere_diameter_in_deg, 
        viewing_distance_in_m = sphere_radial_distance_in_m )

sphere_1_half_meridian = 45 # half-meridian of sphere 1
sphere_2_half_meridian = sphere_1_half_meridian + 180 # half-meridian of sphere 2

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================


my_world = The3DWorld ()

def main():
    my_scene = VisualScene ()
    
    my_world.translate_coordinate_system_along_global ( np.array([0, 1.2, 0]))
    
    my_sphere_1 = Sphere ( size_in_meters = sphere_diameter_in_m )
    my_sphere_1.set_perimetric_coordinates ( 
                                eccentricity = sphere_diameter_in_deg/2, 
                                halfMeridian = sphere_1_half_meridian,
                                radialDistance = sphere_radial_distance_in_m)
    
    my_sphere_2 = Sphere ( size_in_meters = sphere_diameter_in_m )
    my_sphere_2.set_perimetric_coordinates ( 
                                eccentricity = sphere_diameter_in_deg/2, 
                                halfMeridian = sphere_2_half_meridian,
                                radialDistance = sphere_radial_distance_in_m)    

    my_scene.place ( my_sphere_1, my_world)    
    my_scene.place ( my_sphere_2, my_world)    
    
    my_world.add_scene (my_scene)
    my_world.write ()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
