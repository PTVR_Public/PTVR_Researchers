# -*- coding: utf-8 -*-
"""
...PTVR_Researchers\Python_Scripts\Demos\Objects\
Sprite_10_2D_object_from_png_file.py

Goal:
    Discover the PTVR Sprite object (the 'Sprite' name is used in Unity): 
    "In computer graphics, a sprite is a two-dimensional bitmap that is integrated 
    into a larger scene, most often in a 2D video game".
    (https://en.wikipedia.org/wiki/Sprite_(computer_graphics).
        
    More concretely, it is a PTVR 2D object (similar to other PTVR objects such
    as a sphere for example) that you create from 
    an image file (PTVR currently only uses png and jpg image files).
    
    
About the 'path_to_image_file' parameter of Sprite():
    if 'path_to_image_file' is set to "" (default), then the 'image_file' parameter
    corresponds to a file stored on you PC in:
    ...\PTVR_Researchers\PTVR_Operators\resources\Images
    or in one of its sub-directories.
    
    if 'path_to_image_file' is set to say "C:", then the 'image_file' parameter
    corresponds to a file stored on you PC in "C:"
    
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.Color as color

text_above = "The yellow and orange sprites have a 0NE meter size.\n"\
                "The black cube has a 0NE meter size. \n "\
                    "The light blue cube has a TWO meters size. \n "

def main():
    my_world = visual.The3DWorld (name_of_subject="Michelle")
    #my_world.translate_coordinate_system_along_global (viewpoint_position)
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()    
    
    # Text for instructions
    my_text_above = PTVR.Stimuli.Objects.Text ( visual_angle_of_centered_x_height_deg = 2,
        text = text_above, position_in_current_CS = np.array ( [ 0, 1.2, 1 ] )   )
    my_scene.place (my_text_above, my_world)
    
    ## ALL objects have a 1 meter size !
    
    # Default size in meters of yellow sprite is 1 meter
    # and size of imported image is 100 pixels
    my_yellow_sprite = PTVR.Stimuli.Objects.Sprite ( 
                        image_file = "yellow_square_100pix.png", 
                        position_in_current_CS = np.array ( [ -1, -0.5, 2 ] ),
                        rotation_in_current_CS = np.array ([90, 0, 0]) )
    
    # Default size in meters of cube is 1 meter
    my_reference_cube = PTVR.Stimuli.Objects.Cube (        
                        position_in_current_CS = np.array ( [ 0, -1, 2 ] ) ,
                        rotation_in_current_CS = np.array ([0, 0, 0])  )
     
    my_scene.place (my_yellow_sprite, my_world)
    my_scene.place (my_reference_cube, my_world)
    
    my_world.rotate_coordinate_system_about_global_y (90)
    
    # Default size in meters of orange sprite is 1 meter ....
    # and size of imported image is 200 pixels
    my_orange_sprite = PTVR.Stimuli.Objects.Sprite ( 
                        image_file = "orange_square_200pix.png", 
                        position_in_current_CS = np.array ( [ -0.3, 0, 1.5 ] ),
                        rotation_in_current_CS = np.array ([90, 0, 0]) )    
    # Default size in meters of cube is 2 meters
    my_reference_cube_2 = PTVR.Stimuli.Objects.Cube ( 
                        size_in_meters = 2, color = color.RGBColor(r=0.2, g = 0.2, b = 0.5),
                        position_in_current_CS = np.array ( [ -0.3, -1.01, 1.5 ] ) ,
                        rotation_in_current_CS = np.array ([0, 0, 0])  )    
    
    my_scene.place (my_orange_sprite, my_world)    
    my_scene.place (my_reference_cube_2, my_world)

    my_world.add_scene (my_scene)     
    my_world.write () # writes the experiment to .json file
    print (".json file has been created.")

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld() 
