# -*- coding: utf-8 -*-
"""
...PTVR_Researchers\Python_Scripts\Demos\Objects\
Sprite_40_hundreds_of_sprites_in_a_scene.py

Goal: shows that hundreds of PTVR sprites of the same image (even with a high
    spatial resolution) can be displayed within a scene, and 
    across scenes, without any timing issues.
 
   
Note:    If the images are all different from each other (not shown here),
     then timing issues will arise above a certain number of images 
     but this would be the same in Unity.
    
    

created by Carlos Aguilar (february 2025) 
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import random
import math


n_scenes = 6

n_animals_x = 30
n_animals_y = 30

x_start = -n_animals_x/2
z_start = 4
y_start = -n_animals_y/2
separation_between_animals = 0.1
scene_duration = 6000
list_images_names = ["dog.png", "cat.png"]


def main():
    my_world = visual.The3DWorld(name_of_subject="Michelle")

    for j in range(n_scenes):
        my_scene = PTVR.Stimuli.Scenes.VisualScene()
        animal_name = list_images_names[j % 2]
        for k in range(n_animals_x):
            x = x_start + k*(1+separation_between_animals)
            for i in range(n_animals_y):
                y = y_start + i*(1+separation_between_animals)
                z = 5  
                cube = PTVR.Stimuli.Objects.Cube(
                    position_in_current_CS=np.array([x, y, z]),
                    rotation_in_current_CS=np.array([0, 0, 0]))
                
                cat_on_cube = PTVR.Stimuli.Objects.Sprite(
                    image_file = animal_name,  # cat.png or dog.png
                    size_in_meters=np.array([1, 1, 1]),
                    position_in_current_CS=np.array([x, y, z - 0.5]),
                    rotation_in_current_CS=np.array([0, 0, 0]))

                my_scene.place(cube, my_world)
                my_scene.place(cat_on_cube, my_world)

        t = event.Timer(delay_in_ms=scene_duration)
        end_scene = callback.EndCurrentScene()

        my_scene.AddInteraction(events=[t], callbacks=[end_scene])

        my_world.add_scene(my_scene)

    my_world.write()  # writes the experiment to .json file
    print(".json file has been created.")


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
