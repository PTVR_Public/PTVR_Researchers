# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Objects\Light_object.py

Goal of this demo: 
        Create a Light object and discover its effects on a cube.
        You can observe the Light's effects by changing the relative positions
        of the ligth and of the cube, or by changing the arguments to orientation,
        intensity, lightRange, spotAngle, etc... of the Light object.
        
Note 1: You can also play around with global_lights_intensity (while keeping
        is_active of the Light to False).        
        
Note 2: For the VisualScene, the argument to global_lights_intensity is
        set to 0 in the present script (by default it is set to 1). In this case, 
        there are no luminous sources coming from above.
        This allows you to observe the effects
        of the Light object without interference from other light sources.
        The consequence of the null global_lights_intensity
        is that the sides of the cube will not have different
        luminances if you set 'is_active' to 0 in the Light object.
        
Note 3: You can of course add new Light objects around the cube and observe
        the shading effects.
                                                                     
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Color as color
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

def main():

    my_world = visual.The3DWorld (name_of_subject = "Ignace")
    my_scene = PTVR.Stimuli.Scenes.VisualScene ( 
                    global_lights_intensity = 0,
                    background_color = color.RGBColor(0.2, 0.2, 0.2) )
    
    #################################################
    # You can change the cube's position or orientation here !
    my_cube = PTVR.Stimuli.Objects.Cube (
                    position_in_current_CS = np.array ( [ 0, 0.5, 2] ),
                    rotation_in_current_CS = np.array([45, 45, 0]),
                    size_in_meters = 0.5 )
    
    # You can play around with the arguments of the light
    my_light =  PTVR.Stimuli.Objects.Light(
                    is_active = True,
                    position_in_current_CS = np.array ([5.0, 2.5, 0]),
                    rotation_in_current_CS = np.array ([25.0, 157.0, 0.0]),
                    intensity = 3.0, # no max value provided
                    lightRange = 15.0, # in meters
                    spotAngle = 90.0   ) # spotlight cone angle in deg
    
    my_light.rotate_to_look_at (my_cube.id)
    
    my_scene.place (my_cube, my_world)
    my_scene.place (my_light, my_world)
    
    my_world.add_scene (my_scene)   
   
    my_world.write () 
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()