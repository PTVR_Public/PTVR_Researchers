# -*- coding: utf-8 -*-
"""
...\Demos\Objects\LineTool_object.py

goal: draw a yellow line (actually a segment) with LineTool()

    A gizmo, i.e. a small x, y, z Coordinate Systeme (CS) is also drawn.


Note: the coordinates passed to LineTool are coordinates in the CURRENT CS
    

"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

height_of_head =  1.2 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head , 0])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint  
viewing_distance = 1

def main():
    
    my_world = visual.The3DWorld (name_of_subject="Michelle")    
    my_world.translate_coordinate_system_along_global (viewpoint_position)  
    my_world.rotate_coordinate_system_about_global_y (45) 
    my_world.rotate_coordinate_system_about_current_x (45)
    
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()    

    # Display axes of Current CS
    gizmo = PTVR.Stimuli.Objects.Gizmo( length=0.25, width=0.01)
    my_scene.place(gizmo, my_world)
    
    # yellow line : forward in the current CS, i.e. along local Z, 
    # and slightly tilted above the local Z axis
    # i.e. end point : [0, 0.1, 1 ]
    
    
    yellow_line = PTVR.Stimuli.Objects.LineTool ( 
            color = color.RGB255Color (r = 255, g=255), width = 0.01,
            end_point =  np.array ([0, 0.1, 1 ])  ) # coordinates in current CS   
    my_scene.place (yellow_line, my_world)  
              
    my_world.add_scene (my_scene)
      
    my_world.write () # writes to .json file
    print (".json file has been created.")

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld() 
