# -*- coding: utf-8 -*-
"""
...PTVR_Researchers\Python_Scripts\Demos\Objects\
Sprite_15_2D_object_from_YOUR_png_file.py

Goal:
    Discover the PTVR Sprite object (the 'Sprite' name is used in Unity): 
    "In computer graphics, a sprite is a two-dimensional bitmap that is integrated 
    into a larger scene, most often in a 2D video game".
    (https://en.wikipedia.org/wiki/Sprite_(computer_graphics).
        
Your task: choose an image file you would like to use to create a sprite. Use 
the path of this file on your PC to set the 'the_path_on_your_PC' variable.

What you see:  The orientation of the sprite is tilted by 45° about the Y axis.
     
        
About the 'path_to_image_file' parameter of Sprite():
    if 'path_to_image_file' is set to "" (default), then the 'image_file' parameter
    corresponds to a file stored on you PC in:
    ...\PTVR_Researchers\PTVR_Operators\resources\Images
    or in one of its sub-directories.
    
    if 'path_to_image_file' is set to say "C:", then the 'image_file' parameter
    corresponds to a file stored on you PC in "C:"
    
    
    
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects

# Where is the PNG image file you want to use to create a sprite ?
the_path_on_your_PC = r"C:\Users\Eric_Castet\Documents\images_for_PTVR"

def main():
    my_world = visual.The3DWorld (name_of_subject="Michelle")
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()    
       
    your_sprite = PTVR.Stimuli.Objects.Sprite (         
                        ######################################
                        path_to_image_file = the_path_on_your_PC,
                        ######################################
                        image_file = "photo_1.png", 
                        position_in_current_CS = np.array ( [ 0, 1, 0.7 ] ),
                        rotation_in_current_CS = np.array ([0, 45, 0])
                        )        
    my_scene.place (your_sprite, my_world)
 
    my_world.add_scene (my_scene)     
    my_world.write () # writes the experiment to .json file

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld() 
