# -*- coding: utf-8 -*-
"""
...PTVR_Researchers\Python_Scripts\Demos\Objects\
Sprite_20_letters_from_png_file.py

Goal:
    Discover the PTVR Sprite object (the 'Sprite' name is used in Unity): 
    "In computer graphics, a sprite is a two-dimensional bitmap that is integrated 
    into a larger scene, most often in a 2D video game".
    (https://en.wikipedia.org/wiki/Sprite_(computer_graphics).
     
    Here, the sprites are created from a unique file representing the C letter.
    (PTVR currently only uses png and jpg image files).
        
    View the effect of varying the Sprite object's parameter : "rotation_in_current_CS"  
    (i.e. this varies the sprite's orientation by applying rotations
    in the current CS)
.
What you see:
    Straight ahead of your head: a column of text with increasing values
    of the Sprite's parameter called "rotation_in_current_CS" 
    
    To the left of your head: a column of text with increasing values
    of the Sprite's parameter called "rotation_in_current_CS" 
    
    To the right of your head: a column of text with increasing values
    of the Sprite's parameter called "rotation_in_current_CS"    
    
    These three columns simply aim at making it easier to view the different
    Sprites' orientations from different viewing angles.
   
About the 'path_to_image_file' parameter of Sprite():
    if 'path_to_image_file' is set to "" (default), then the 'image_file' parameter
    corresponds to a file stored on you PC in:
    ...\PTVR_Researchers\PTVR_Operators\resources\Images
    or in one of its sub-directories.
    
    if 'path_to_image_file' is set to say "C:", then the 'image_file' parameter
    corresponds to a file stored on you PC in "C:"
    
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects

height_of_head =  1.2 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head , 0])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint

# the image is e PTVR_Operators/resources/Images
png_file_of_sprite = "C.png" # ex: "C.png", "E.png", 
# "plus.png", "scotome.png"
y = 0
text_above = 'Effect of varying the Sprite object\'s parameter \'rotation_in_current_CS\'\n \
                i.e. effect of varying Sprite\'s orientation in the CURRENT Coordinate System'
 
def draw_text (rotation, my_world, my_scene):  
    global y
    y = y - 0.1
    my_sprite = PTVR.Stimuli.Objects.Sprite ( 
                            size_in_meters = 0.05,
                            image_file = png_file_of_sprite, 
                            position_in_current_CS = np.array ( [ 0, y, 1 ] ),
                            rotation_in_current_CS = np.array ([0, rotation, 0]) )
    my_scene.place (my_sprite, my_world)
    
def main():
    global y
    y=0.2
    my_world = visual.The3DWorld (name_of_subject="Michelle")
    my_world.translate_coordinate_system_along_global (viewpoint_position)
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()    
    
    # Text for instructions
    my_text_above = PTVR.Stimuli.Objects.Text ( visual_angle_of_centered_x_height_deg = 2,
        text = text_above, position_in_current_CS = np.array ( [ 0, y+0.2, 1 ] )   )
    my_scene.place (my_text_above, my_world)
    
    # draw text in front (or straight-ahead) and at different y values wrt current CS  
    # text column straight-ahead of your head 
    y=0.2
    for rotation in range (0, 360, 45):
        draw_text (rotation, my_world, my_scene)
        
        
    # Change current CS to translate text column to the LEFT of your head
    my_world.translate_coordinate_system_along_global (np.array ( [ -1, 0, 0 ] ))
    y= 0.2
    for rotation in range (0, 360, 45):
        draw_text (rotation, my_world, my_scene)        
        
    # Change current CS to translate text column to the RIGHT of your head
    my_world.translate_coordinate_system_along_global (np.array ( [ 2, 0, 0 ] ))
    y= 0.2
    for rotation in range (0, 360, 45):
        draw_text (rotation, my_world, my_scene)    
        
    my_world.add_scene (my_scene)
      
    my_world.write () # writes the experiment to .json file
    print (".json file has been created.")

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld() 
