# -*- coding: utf-8 -*-
"""
...PTVR_Researchers\Python_Scripts\Demos\Objects\
Sprite_30_cats_on_cubes.py

Goal:
    Discover the PTVR Sprite object (the 'Sprite' name is used in Unity): 
    "In computer graphics, a sprite is a two-dimensional bitmap that is integrated 
    into a larger scene, most often in a 2D video game".
    (https://en.wikipedia.org/wiki/Sprite_(computer_graphics).
        
    Here the Sprites are created from a unique file representing a cat.
   (PTVR currently only uses png and jpg image files).
    
About the 'path_to_image_file' parameter of Sprite():
    if 'path_to_image_file' is set to "" (default), then the 'image_file' parameter
    corresponds to a file stored on you PC in:
    ...\PTVR_Researchers\PTVR_Operators\resources\Images
    or in one of its sub-directories.
    
    if 'path_to_image_file' is set to say "C:", then the 'image_file' parameter
    corresponds to a file stored on you PC in "C:"
    
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects

def main():
    my_world = visual.The3DWorld (name_of_subject="Michelle")
    my_scene  = PTVR.Stimuli.Scenes.VisualScene ()    
    
    # Default size in meters of cube is 1 meter
    first_cube = PTVR.Stimuli.Objects.Cube (        
                        position_in_current_CS = np.array ( [ 1, 0, 3 ] ) ,
                        rotation_in_current_CS = np.array ([0, 0, 0])  )
    # 3 cats on differents sides of first cube (1 meter size)
    first_cat_on_cube_1 = PTVR.Stimuli.Objects.Sprite ( 
                        image_file = "cat.png", 
                        size_in_meters=np.array([1,1,1]),
                        position_in_current_CS = np.array ( [ 1, 0.501, 3 ] ),
                        rotation_in_current_CS = np.array ([90, 0, 0]) ) 
    second_cat_on_cube_1 = PTVR.Stimuli.Objects.Sprite ( 
                        image_file = "cat.png", 
                        size_in_meters=np.array([1,1,1]),
                        position_in_current_CS = np.array ( [ 1, 0, 2.49] ),
                        rotation_in_current_CS = np.array ([0, 0, 0]) )    
    third_cat_on_cube_1 = PTVR.Stimuli.Objects.Sprite ( 
                        image_file = "cat.png", 
                        size_in_meters=np.array([1,1,1]),
                        position_in_current_CS = np.array ( [ 0.49, 0, 3] ),
                        rotation_in_current_CS = np.array ([0, 90, 0]) )    
    # Cube size: 2 meters
    second_cube = PTVR.Stimuli.Objects.Cube (        
                        size_in_meters=np.array([2,2,2]),
                        position_in_current_CS = np.array ( [ -1, 0.5, 4 ] ) ,
                        rotation_in_current_CS = np.array ([0, 0, 0])  )
    # One cat on second cube
    cat_on_cube_2 = PTVR.Stimuli.Objects.Sprite ( 
                        image_file = "cat.png", 
                        size_in_meters=np.array([2,2,2]),
                        position_in_current_CS = np.array ( [ -1, 0.5, 2.99] ),
                        rotation_in_current_CS = np.array ([0, 0, 0]) )
    # Cube size: 2 meters
    third_cube = PTVR.Stimuli.Objects.Cube (        
                        size_in_meters=np.array([10,10,10]),
                        position_in_current_CS = np.array ( [ -10, 0, 16 ] ) ,
                        rotation_in_current_CS = np.array ([0, 0, 0])  )
    # one cat on third cube
    cat_on_cube_3 = PTVR.Stimuli.Objects.Sprite ( 
                        image_file = "cat.png", 
                        size_in_meters=np.array([10,10,10]),
                        position_in_current_CS = np.array ( [ -10, 0, 10.99] ),
                        rotation_in_current_CS = np.array ([0, 0, 0]) )
    
    fourth_cube = PTVR.Stimuli.Objects.Cube (        
                        size_in_meters=np.array([2,2,2]),
                        position_in_current_CS = np.array ( [ -4, 0.5, 2 ] ) ,
                        rotation_in_current_CS = np.array ([0, 0, 0])  )
   
    my_scene.place (first_cube, my_world)    
    my_scene.place (first_cat_on_cube_1, my_world)
    my_scene.place (second_cat_on_cube_1, my_world)
    my_scene.place (third_cat_on_cube_1, my_world)
    
    my_scene.place (second_cube, my_world)
    my_scene.place (cat_on_cube_2, my_world)

    my_scene.place (third_cube, my_world)
    my_scene.place (cat_on_cube_3, my_world)

    my_scene.place (fourth_cube, my_world)
    
    my_world.add_scene (my_scene)
      
    my_world.write () # writes the experiment to .json file
    print (".json file has been created.")

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld() 
