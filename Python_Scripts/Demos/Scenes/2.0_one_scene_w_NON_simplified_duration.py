# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Scenes\2.0_one_scene_w_NON_simplified_duration.py

Description :   displays one scene whose duration is defined by explicitely creating an
                    interaction corresponding to this scene. 
                It requires therefore to define an event, a callback and their interaction.

"""


Created on : Thu Jan 13 15:09:02 2022
@author: jtermozm

'''

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
 
# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Julia" # subject's name
 
my_scene_duration_in_ms = 10000 #in Ms
# 100 ms corresponds to 9 frames with a refresh rate of 90 Hz (HTC Vive Pro)

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================
   
def main():
 
    my_world = visual.The3DWorld (name_of_subject = username)
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    
    # display Scene information on subject's screen
    my_text = PTVR.Stimuli.Objects.Text ( text = "This single scene lasts " + str( my_scene_duration_in_ms) + " ms\n" + 
                                        "and then the script will QUIT PTVR (ie the PTVR window will close itself)\n" +
                                        "Or you can quit by closing yourself the PTVR window (mouse click in the top right corner)",                              
                                        position_in_current_CS = np.array([0, 1, 1]), visual_angle_of_centered_x_height_deg = 1)
    my_scene.place(my_text,my_world)
 
    my_event = event.Timer (delay_in_ms = my_scene_duration_in_ms)
    my_callback = callback.EndCurrentScene() 

    my_scene.AddInteraction ( events = [my_event], callbacks = [my_callback] )
  
    my_world.add_scene (my_scene)
    my_world.write() # Write The3DWorld to .json file
    
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()