# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Scenes\cycle_of_scenes_w_NON_simplified_duration.py
based on ...\Debugs\event\event_timed.py

Goal : displays a cyclic presentation of N scenes whose durations are NOT defined 
            by a simplified interaction.

See the PTVR documentation : 
    Section : User Manual -> Interactions between events and callbacks -> 
                                    Simplified Interactions


"""


Created on: Tue Nov 23 10:02:30 2021
@author: jtermozm
'''

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import numpy as np

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Martin"

number_of_scenes = 3

my_scene_duration_in_ms = 500 
# 100 ms corresponds to 9 frames with a refresh rate of 90 Hz (HTC Vive Pro)


text_always_on = """
The duration of each scene is defined by the 'Timer' event (called 'my_event'  in the code).
The scene duration is set to """ + str(my_scene_duration_in_ms) + ' ms' + """.
When the last scene (""" + '#' + str(number_of_scenes - 1) + """ here) has been presented, the first scene (#0 here) 
is displayed to create an infinite loop.\n
Close the PTVR window (the one with a dragonfly logo) to quit the program (mouse click in the top right corner).
"""
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

def NewScene (my_world, id_scene):
   my_scene = PTVR.Stimuli.Scenes.VisualScene (trial=1)
   #display the Scene Number
   my_text = PTVR.Stimuli.Objects.Text ( text="Current Scene : " + str ( id_scene),
                                       visual_angle_of_centered_x_height_deg = 1.5)
   my_text.set_perimetric_coordinates ( radialDistance = 200, eccentricity = 0, halfMeridian = 0)
   my_scene.place (my_text,my_world)
   
   my_event = event.Timer(delay_in_ms = my_scene_duration_in_ms)
   my_callback = callback.EndCurrentScene() 
   
   # if this is the last scene of the 'for loop', 
   # change the interaction for this specific scene so that when it ends, the next scene
   # that will be presented will the first scene of the my_world.riment (here my_world.id_scene[0]),
   # thus producing a cyclic presentation of the scenes 
   if (id_scene == number_of_scenes - 1 ):
       my_callback.SetNextScene ( id_next_scene = my_world.id_scene[0] )    
       
   my_scene.AddInteraction(events= [my_event],callbacks =[my_callback]) 
   my_text_always_on = PTVR.Stimuli.Objects.Text ( text=text_always_on,
                                                 visual_angle_of_centered_x_height_deg = 1, align_vertical = "TOP")
   my_text_always_on.set_perimetric_coordinates ( radialDistance = 20, eccentricity = 9, halfMeridian = -90)
   my_scene.place (my_text_always_on,my_world)
    
   my_world.add_scene(my_scene)
   pass

def main():
    my_world = visual.The3DWorld (name_of_subject = username)
    for i in range ( number_of_scenes ):
        NewScene(my_world =  my_world, id_scene= i )
   
    my_world.write() # Write The3DWorld to .json file
    
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()