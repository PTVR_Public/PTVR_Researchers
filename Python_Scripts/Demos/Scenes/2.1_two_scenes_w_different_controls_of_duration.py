# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Scenes\
    2.1_two_scenes_w_different_controls_of_duration.py

Decription : displays two scenes with different controls of duration:
        1/ the first scene waits until the 'q' key is pressed on the keyboard.
        This keypress induces the end of the scene.

        2/ the second scene's duration is defined by explicitely creating an interaction. 
        It requires therefore to define an event, a callback and their interaction.

'''

from PTVR import SystemUtils, Visual
from PTVR.Stimuli import Scenes, Objects
from PTVR.Data import Event, Callback
import numpy as np 
 
scene_duration = 2000 # in ms
   
def main(): 
    my_world = Visual.The3DWorld (name_of_subject = "Julia")
    
    # Scene 1
    my_scene_1 = Scenes.VisualScene (trial=1)   
    my_text = Objects.Text (text = "Press 'q' to stop the scene",
                            position_in_current_CS = np.array([0, 1, 0.5]))
    my_scene_1.place (my_text, my_world)  
    
    # Create interaction for scene 1
    q_press =   Event.Keyboard (valid_responses = ['q'], mode = "press")
    end_scene = Callback.EndCurrentScene() 
    my_scene_1.AddInteraction (events = [q_press], callbacks = [end_scene])
    
    # Scene 2
    my_scene_2 = Scenes.VisualScene (trial=1)
    my_text = Objects.Text (
                text = "Scene's duration: " + str(scene_duration) + " ms",
                position_in_current_CS = np.array([0, 1, 0.5]))
    my_scene_2.place (my_text, my_world)
    
    # Create interaction for scene 2
    duration_exceeded = Event.Timer (delay_in_ms = scene_duration)
    my_scene_2.AddInteraction (events = [duration_exceeded], callbacks = [end_scene])
    
    my_world.add_scene (my_scene_1)    
    my_world.add_scene (my_scene_2)
    my_world.write() # create .json file
    
if __name__ == "__main__":
            main()
            SystemUtils.LaunchThe3DWorld()