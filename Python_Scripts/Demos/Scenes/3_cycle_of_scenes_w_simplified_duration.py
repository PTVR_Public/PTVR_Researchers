# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Scenes\cycle_of_scenes_w_simplified_duration.py


GOAL : displays a cyclic presentation of N scenes whose durations are defined 
        by a simplified interaction.


Created on Fri Jan  7 16:06:22 2022
@author: jtermozm

'''

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Martin"

number_of_scenes = 3
my_scene_duration_in_ms= 100 #in Ms
# 100 ms corresponds to 9 frames with a refresh rate of 90 Hz (HTC Vive Pro)

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

def new_scene(my_world,  id_scene):
   my_scene = PTVR.Stimuli.Scenes.SimplifiedTimerScene (scene_duration_in_ms = my_scene_duration_in_ms)
   
   # display Scene Number on subject's screen
   my_text = PTVR.Stimuli.Objects.Text ( text = " Current Scene " + str ( id_scene) + "\n" 
                                       "You can quit by closing yourself the PTVR window (mouse click in the top right corner)",
                                       position_in_current_CS = np.array ([0, 1, 1]), visual_angle_of_centered_x_height_deg = 1)
   my_scene.place(my_text,my_world)
   
   # if this is the last scene of the 'for loop', 
   # change the interaction for this specific scene so that when it ends, the next scene
   # that will be presented will the first scene of the my_world.riment (here my_world.id_scene[0]),
   # thus producing a cyclic presentation of the scenes 
   if ( id_scene == number_of_scenes - 1 ):
       my_scene.SetNextScene ( id_next_scene = my_world.id_scene [0] )
   my_world.add_scene(my_scene) 
   pass

def main():
    my_world = visual.The3DWorld (name_of_subject = username)
    
    for i in range (number_of_scenes):
       new_scene (my_world = my_world, id_scene = i)     
   
    my_world.write() # Write The3DWorld to .json file
    
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()