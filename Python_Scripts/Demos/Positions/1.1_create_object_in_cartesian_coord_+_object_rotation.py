# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Objects\
1.1_create_object_in_cartesian_coord_+_object_rotation.py

Goal of this demo: create an object (here a cube) and specify its position
with CARTESIAN coordinates in the global (aka world) Coordinate System (CS).
You can specify the object's position' in different ways by uncommenting some lines of code.

Prediction: with coordinates x=0, y=1.2 and z=2:
A black cube should be placed approximately in front of your head (y= 1.2 meters if you're seated) 
and at a viewing distance of 2 meters (z=2).

Note 1: the default orientation of the cube is the horizontal plane (XY plane) in the global CS.

Note 2: you can modify the object rotation by modifying the angle_deg parameter in
  my_cube.rotate_about_current_y (angle_deg = ... ) or
  my_cube.rotate_about_global_y (angle_deg = ... ), 
  etc... (see cheatsheet 'orientation' in the Documentation)
                                                                     
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

username = "castet"

def main():
    my_world = visual.The3DWorld (name_of_subject = username)
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    
    my_cube = PTVR.Stimuli.Objects.Cube (position_in_current_CS = np.array ( [ 0.0, 1.2, 2.0] ) )
    # uncomment line below if you want to change cube's position (viewing distance: 10 m)
    # my_cube.position_in_current_CS =  np.array ( [ 0.0, 1.2, 10.0] )
    # uncomment line below if you want to change cube's position by using the fonction set_cartesian_coordinates
    # my_cube.set_cartesian_coordinates (x = 0.0, y = 1.2, z = 50) # viewing distance is now 50 meters
    
    #####################################################
    # You can change the rotation of the object here !
    my_cube.rotate_about_current_y ( angle_deg = 45) # not really necessary, it is just to show rapidly to the user
    # that the object is a cube and not a square, without the user being obliged to move sideways.
    #####################################################
    
    my_scene.place (my_cube,my_world)
    my_world.add_scene (my_scene)   
   
    my_world.write () 
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()