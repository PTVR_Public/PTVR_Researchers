 
# -*- coding: utf-8 -*-
"""
...\Demos\Experiment_building\iso_eccentric_stimuli.py

Description :
    In each trial, a single scene displays...
    8 iso-eccentric stimuli having the same radial distance
    and varying in half-meridian.
    In other words, they appear as a circle of discs.
    As they have the same eccentricity AND radial distance, they all subtend
    the same angular value at the origin of the current Coordinate System.
    
    In each trial, one of these 8 stimuli has a slightly higher size
    
"""

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import numpy as np
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback

nb_of_trials = 5
nb_of_spheres = 8

def main():
    my_world = visual.The3DWorld (name_of_subject = "Naomi" )
    # Translate the Coordinate System upwards by headset height
    # (around 1.2 meters when subject is seated)
    my_world.translate_coordinate_system_along_global (np.array ( [0, 1.2, 0]))
    
    for i in range (0, nb_of_trials): 
        trial_scene = PTVR.Stimuli.Scenes.VisualScene (trial = i+1)
      
        random_index = np.random.randint (nb_of_spheres)
        
        for j in range (0, nb_of_spheres):       
            my_stim = PTVR.Stimuli.Objects.Sphere (
                size_in_meters = np.array([0.05, 0.05, 0.05]) )            
            if j == random_index : 
                my_stim = PTVR.Stimuli.Objects.Sphere (
                    size_in_meters = np.array([0.07, 0.07, 0.07]) )                   

            # else :      my_stim.rotate_about_object_z(0)
            
            my_stim.set_perimetric_coordinates (radialDistance = 2,
                                                eccentricity = 5, 
                                                halfMeridian = j * 45)
            
            trial_scene.place (my_stim, my_world)
        
        # Create interaction for the scene
        my_event_space_bar_press =  event.Keyboard ( valid_responses = ['space'], mode = "press")     
        my_callback_current_scene_end = callback.EndCurrentScene ()       
        trial_scene.AddInteraction ( 
                events = [ my_event_space_bar_press ], 
                callbacks = [ my_callback_current_scene_end ] )
        
        my_world.add_scene (trial_scene)
        
    my_world.write() # Write experiment into .json file
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld ()