# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Positions\
1.0_create_object_in_cartesian_coord.py

Goal of this demo: create an object (here a cube) and specify its position
with CARTESIAN coordinates in the global (aka world) Coordinate System (CS).
You can specify the object's position' in different ways by uncommenting some lines of code.

Prediction: with coordinates x=0, y=1.2 and z=2:
    A black cube should be placed approximately in front of your head 
    (y= 1.2 meters if you're seated) 
     and at a viewing distance of 2 meters (z=2).

Note 1: the default orientation of the cube is along the 
horizontal plane (XY plane) in the global CS.

Note 2: See the demo 1.1_create_object_in_cartesian_coord_+_object_rotation.py 
to change the object's orientation
                                                                     
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

subject_id = "Ignace"

def main():

    my_world = visual.The3DWorld (name_of_subject = subject_id)
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    
    #################################################
    # You can change the cube's position here !
    my_cube = PTVR.Stimuli.Objects.Cube (
        position_in_current_CS = np.array ( [ 0, 1.2, 2] ) )
    # uncomment line below if you want to change cube's position (viewing distance: 10 m)
    # my_cube.position_current_CS =  np.array ( [ 0.0, 1.2, 10.0] )
    # uncomment line below if you want to change cube's position by using the fonction set_cartesian_coordinates
    # my_cube.set_cartesian_coordinates (x = 0.0, y = 1.2, z = 50) # viewing distance is now 50 meters
    ###################################################
    
    my_scene.place (my_cube,my_world)
    my_world.add_scene (my_scene)   
   
    my_world.write () 
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()