# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Positions
2_create_object_in_perimetric_coord.py

Goal of this demo: 
    Create two objects (here cubes) and specify their position
with PERIMETRIC coordinates in the global (aka world) Coordinate System.
    The cubes only differ by their half-meridian

   Reminder : the origin of the Coordinate System (CS) is at your feet.
                                                                     
"""

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

def main():

    my_world = visual.The3DWorld()
    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    my_cube_1 = PTVR.Stimuli.Objects.Cube(size_in_meters = 0.1)
    my_cube_1.set_perimetric_coordinates (radialDistance = 1, eccentricity = 30,
                                         halfMeridian = 45)

    my_cube_2 = PTVR.Stimuli.Objects.Cube(size_in_meters = 0.1)
    my_cube_2.set_perimetric_coordinates(radialDistance = 1, eccentricity = 30,
                                         halfMeridian = 135)
    my_scene.place(my_cube_1, my_world)
    my_scene.place(my_cube_2, my_world)
    my_world.add_scene(my_scene)

    my_world.write()

if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
