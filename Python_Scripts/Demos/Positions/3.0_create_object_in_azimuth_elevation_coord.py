# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Positions
3.0_create_object_in_azimuth_elevation_coord.py

Goal of this demo: 
    Create 3 objects (here cubes) and specify their position
with AZIMUTH / ELEVATION coordinates in the global Coordinate System (CS) .
    The cubes only differ by their azimuth.

   Reminder : the origin of the Coordinate System (CS) is at your feet
               (this is the default).
                                                                     
"""

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

def main():

    my_world = visual.The3DWorld()
    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    my_cube_1 = PTVR.Stimuli.Objects.Cube (size_in_meters = 0.1)
    my_cube_1.set_azimuth_elevation_coordinates (radialDistance = 2, azimuth = 0,
                                         elevation = 45)

    my_cube_2 = PTVR.Stimuli.Objects.Cube (size_in_meters = 0.1)
    my_cube_2.set_azimuth_elevation_coordinates (radialDistance = 2, azimuth = 30,
                                         elevation = 45)

    my_cube_3 = PTVR.Stimuli.Objects.Cube (size_in_meters = 0.1)
    my_cube_3.set_azimuth_elevation_coordinates (radialDistance = 2, azimuth = 60,
                                         elevation = 45)
    
    my_scene.place(my_cube_1, my_world)
    my_scene.place(my_cube_2, my_world)
    my_scene.place(my_cube_3, my_world)
    my_world.add_scene(my_scene)

    my_world.write()

if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
