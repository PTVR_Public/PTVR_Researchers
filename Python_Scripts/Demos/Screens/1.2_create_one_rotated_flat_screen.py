# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\DEMOS\Screens\
1.2_create_one_rotated_flat_screen.py

@author: eric castet

Animation demo to show:
a/ that the  orientation of a flat screen can be modified 
    (this in contrast with the orientation of a tangent screen whose orientation cannot be modified 
    by fonctions such as rotate_about_global_x ()  )
"""

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import PTVR.Pointing.PointingCursor

PWD = PTVR.SystemUtils.PWD

username = "Ignace" # do not comment : username is used (among other things) to create the name of your results file.

flat_screen_radial_distance_m = 3 # in meters

def main():
    my_world = visual.The3DWorld(name_of_subject = username)
    
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    
    # WARNING : the flat screen is in front of your feet ! 
    my_flat_screen_white = PTVR.Stimuli.Objects.FlatScreen (color = color.RGBColor (r=1, g=1, b=1)  )   
    my_flat_screen_white.set_cartesian_coordinates (x = 0, y =  0, z = flat_screen_radial_distance_m)
    
    #######################################################################################
    # Modify the angle_deg parameter here if you want !
    my_flat_screen_white.rotate_about_global_x (angle_deg = 45)
    #######################################################################################    
 
    my_scene.place (my_flat_screen_white,my_world)
    
    my_world.add_scene(my_scene)
    my_world.write() 

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld()
        
