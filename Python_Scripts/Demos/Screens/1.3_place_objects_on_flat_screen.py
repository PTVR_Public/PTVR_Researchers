# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\DEMOS\Screens\
1.3_place_objects_on_flat_screen.py

@author: eric castet

Animation demo to show:
a/ that the PTVR function used to place objects on a flat screen 
    ( set_cartesian_coordinates_on_screen ()  )
     only accepts a cartesian coordinate system (in meter units)

b/ as a reminder, that a FLAT screen DOES NOT automatically adapt its orientation as a 
    function of its position
    (this is in contrast with the tangent screen and this is also shown in 
    1_create_flat_screen.py) so that you can change the orientation of a flat screen at will.
    
    Play around by rotating the flat screens with for instance 
    my_flat_screen_xxxx.rotate_about_object_z ( any angle in degrees).

"""


import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

import PTVR.Stimuli.Color as color
import PTVR.Pointing.PointingCursor


PWD = PTVR.SystemUtils.PWD

# do not comment : username is used (among other things) to create the name of your results file.
username = "castet"

flat_screen_radial_distance_m = 3  # in meters
flat_screen_eccentricity = 30
flat_screen_halfmeridian = 180

height_of_headset = 1.2  # in meters
if username == "castet":
    height_of_headset = 1.2

viewpoint_position = np.array([0, height_of_headset, 0])

my_world = visual.The3DWorld(name_of_subject=username)


def place_objects_w_cartesian_CS(my_scene, my_screen):
    for i in np.arange(0, 0.6, 0.1):
        my_object = PTVR.Stimuli.Objects.Cube(
            size_in_meters=0.05, color=color.RGBColor(r=0.5, b=0.9))
        # cartesian_1 = my_object.get_current_CS_cartesian_coordinates()  Remove
        # print ("cartesian_1:", cartesian_1)

        # CARTESIAN Coordinates
        my_object.set_cartesian_coordinates_on_screen(my_2D_screen=my_screen,
                                                      x_local=i, y_local=i)

        my_scene.place(my_object, my_world)

# ---------------------------------------------------------------------


# -------------------------------------------------------------------------------


def main():

    # Coordinate TRANSFORMATIONS
    my_world.translate_coordinate_system_along_global(viewpoint_position)
    # my_world.rotate_coordinate_system_about_global_y (90)
    # my_world.rotate_coordinate_system_about_current_y (90)

    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    my_flat_screen_white = PTVR.Stimuli.Objects.FlatScreen(
        color=color.RGBColor(r=1, g=1, b=1))
    my_flat_screen_white.set_cartesian_coordinates(
        x=0, y=0, z=flat_screen_radial_distance_m)
    my_flat_screen_white.rotate_about_object_z(20)

    # # PERIMETRIC Coordinates
    my_flat_screen_yellow = PTVR.Stimuli.Objects.FlatScreen(
        color=color.RGBColor(r=1, g=1))
    my_flat_screen_yellow.set_perimetric_coordinates(
        eccentricity=flat_screen_eccentricity,
        halfMeridian=flat_screen_halfmeridian, radialDistance=flat_screen_radial_distance_m)
    my_flat_screen_yellow.rotate_about_object_z(20)

    my_flat_screen_orange = PTVR.Stimuli.Objects.FlatScreen(
        color=color.RGBColor(r=1, g=0.5))
    my_flat_screen_orange.set_perimetric_coordinates(
        eccentricity=flat_screen_eccentricity,
        halfMeridian=180 + flat_screen_halfmeridian, radialDistance=flat_screen_radial_distance_m)
    my_flat_screen_orange.rotate_about_object_z(20)
    my_flat_screen_orange.rotate_about_object_x(-45)

    # CARTESIAN Coordinates
    my_flat_screen_green = PTVR.Stimuli.Objects.FlatScreen(
        color=color.RGBColor(g=1))
    my_flat_screen_green.set_cartesian_coordinates(
        x=0, y=- flat_screen_radial_distance_m, z=flat_screen_radial_distance_m)

    my_flat_screen_blue = PTVR.Stimuli.Objects.FlatScreen(
        color=color.RGBColor(b=1))
    my_flat_screen_blue.set_cartesian_coordinates(
        x=0, y=flat_screen_radial_distance_m, z=flat_screen_radial_distance_m)

    my_scene.place(my_flat_screen_white, my_world)
    my_scene.place(my_flat_screen_yellow, my_world)
    my_scene.place(my_flat_screen_orange, my_world)
    my_scene.place(my_flat_screen_green, my_world)
    my_scene.place(my_flat_screen_blue, my_world)

    place_objects_w_cartesian_CS(
        my_scene=my_scene, my_screen=my_flat_screen_white)
    place_objects_w_cartesian_CS(
        my_scene=my_scene, my_screen=my_flat_screen_yellow)
    place_objects_w_cartesian_CS(
        my_scene=my_scene, my_screen=my_flat_screen_orange)
    place_objects_w_cartesian_CS(
        my_scene=my_scene, my_screen=my_flat_screen_green)
    place_objects_w_cartesian_CS(
        my_scene=my_scene, my_screen=my_flat_screen_blue)

    my_world.add_scene(my_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
