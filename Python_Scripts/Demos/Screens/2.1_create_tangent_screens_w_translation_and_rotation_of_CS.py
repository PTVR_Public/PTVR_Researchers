# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\DEMOS\Screens\
2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

"CS" in the title stands for Coordinate System (not to be confused with Current Coordinate System)

@author: eric castet

Animation demo to show:
a/ how a TANGENT screen adapts its orientation as a function of its position
    in order to "look at" the ORIGIN of the CURRENT coordinate system.
    This origin is often called in PTVR code a VIEWPOINT (see PTVR documentation) 
    
a2/ The local rotation of an object (blue cylinder) placed at the origin of the current CS (Coordinate System).
    This cylinder is pointing along the Z axis of the current CS (i.e. it points in the locally forward direction)
    thanks to a rotation about the X axis of the current CS.

b/ that point a/ is checked with the cartesian and the perimetric coordinate systems used for different tangent screens

c/ that point a/ is checked with different coordinate transformations 
(you have to modify yourself the rotation_deg parameter in the line of code: 
       my_world.rotate_coordinate_system_about_current_x ( rotation_deg = ... )
       (you can also add rotations about other axes, as for instance 
        my_world.rotate_coordinate_system_about_current_y () or
        my_world.rotate_coordinate_system_about_current_z() )



"""


import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

import PTVR.Stimuli.Color as color
import PTVR.Pointing.PointingCursor


PWD = PTVR.SystemUtils.PWD

# do not comment this line : username is used (among other things) to create the name of your results file.
username = "toto"

tangent_screen_radial_distance_m = 3  # in meters
tangent_screen_eccentricity = 30
tangent_screen_halfmeridian = 180

height_of_headset = 1.2  # in meters
if username == "castet":
    height_of_headset = 1.2

viewpoint_position = np.array([0, height_of_headset, 0])


def main():
    my_world = visual.The3DWorld(name_of_subject=username)

    #######################################################################################
    # Modify the rotation_deg parameters below if you want test your own transformations!
    # Coordinate TRANSFORMATIONS
    my_world.translate_coordinate_system_along_global(
        translation=viewpoint_position)
    my_world.translate_coordinate_system_along_global(
        translation=np.array([-2, 0, 0]))

    my_world.rotate_coordinate_system_about_global_x(rotation_deg=0)
    my_world.rotate_coordinate_system_about_global_y(rotation_deg=-45)
    my_world.rotate_coordinate_system_about_global_z(rotation_deg=0)

    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    # Blue cylinder to visualize the origin AND the Z axis of the current CS
    # The cylinder is at the origin of the current CS and pointing along the Z axis of the current CS
    # ( i.e. it is pointing in the locally forward direction)
    my_origin = PTVR.Stimuli.Objects.Cylinder(size_in_meters=0.1 * np.array([1.0, 1.0, 1.0]),
                                              color=color.RGBColor(b=1),
                                              rotation_in_current_CS=np.array(
                                                  # rotation about X
                                                  [90, 0.0, 0.0])
                                              )
    my_scene.place(my_origin, my_world)

    # Straight ahead in the current CS
    my_tangent_screen_white = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(r=1, g=1, b=1))
    my_tangent_screen_white.set_cartesian_coordinates(
        x=0, y=0, z=tangent_screen_radial_distance_m)
    my_scene.place(my_tangent_screen_white, my_world)

    # PERIMETRIC Coordinates
    my_tangent_screen_yellow = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(r=1, g=1))
    my_tangent_screen_yellow.set_perimetric_coordinates(
        eccentricity=tangent_screen_eccentricity,
        halfMeridian=tangent_screen_halfmeridian, radialDistance=tangent_screen_radial_distance_m)
    my_scene.place(my_tangent_screen_yellow, my_world)

    my_tangent_screen_orange = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(r=1, g=0.5))
    my_tangent_screen_orange.set_perimetric_coordinates(
        eccentricity=tangent_screen_eccentricity,
        halfMeridian=180 + tangent_screen_halfmeridian, radialDistance=tangent_screen_radial_distance_m)
    my_scene.place(my_tangent_screen_orange, my_world)

    # CARTESIAN Coordinates
    my_tangent_screen_green = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(g=1))
    my_tangent_screen_green.set_cartesian_coordinates(x=0, y=- tangent_screen_radial_distance_m,
                                                      z=tangent_screen_radial_distance_m)
    my_scene.place(my_tangent_screen_green, my_world)

    my_tangent_screen_blue = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(b=1))
    my_tangent_screen_blue.set_cartesian_coordinates(x=0, y=tangent_screen_radial_distance_m,
                                                     z=tangent_screen_radial_distance_m)
    my_scene.place(my_tangent_screen_blue, my_world)

    my_world.add_scene(my_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
