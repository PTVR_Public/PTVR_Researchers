#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Screens\
    spherical_array_of_flat_screens_around_current_CS_origin.py


Enjoy being near the center of a sphere of flat screens !

You can try and move your head to be at a viewpoint around which a spherical array of 
screens is displayed.

This viewpoint (identified by a red sphere) is at the current coordinate system's ORIGIN.

At some point, your head will be exactly at the center of this spherical array 
when your head position coincides with the position of the current 
coordinate system's ORIGIN (i.e. at the viewpoint).

Note that the orientation of the flat screens is not specified in the 
uncommented code. This allows you to observe that the default ORIENTATION 
of FLAT screens (0, 0, 0) is such that these screens 
lie in planes parallel to the XY plane and 
their horizontal borders are parallel to the XZ plane

Of course, you can uncomment my_flat_screen.rotate_about_global_y (50) and
thus play around with different orientations around the y axis.


Created on: 27 september 2021
@author:  E. Castet 

'''

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import PTVR.Tools as tools

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Mary"
radial_distance_of_object = 0.7  # in meters
# distance between each object and origin of current Coordinate System

height_of_head = 1.3  # in meters : this value is chosen so as to correspond to
# the height of the subject's headset during an experiment (either seated or standing)
if username == "Eric":
    height_of_head = 1.2

viewpoint_position = np.array([0, height_of_head, 0])
# the viewpoint is identified by a red cube

flatscreen_height_deg = 2  # in degrees
flatScreen_height = tools.visual_angle_to_size_on_perpendicular_plane(
    visual_angle_of_centered_object_in_deg=flatscreen_height_deg, viewing_distance_in_m=radial_distance_of_object)  # in meters

flatScreenWidth_deg = 5  # in degrees
flatscreen_width = tools.visual_angle_to_size_on_perpendicular_plane(
    visual_angle_of_centered_object_in_deg=flatScreenWidth_deg, viewing_distance_in_m=radial_distance_of_object)  # in meters
# =============================================================================
#                              END of PARAMETERS                                 #
# =============================================================================


def main():
    my_world = visual.The3DWorld(name_of_subject=username)
    my_world.translate_coordinate_system_along_global(
        translation=viewpoint_position)

    vis_scene = PTVR.Stimuli.Scenes.VisualScene()

    # Red Sphere at the current Coordinate System's Origin
    my_sphere = PTVR.Stimuli.Objects.Sphere(
        size_in_meters=0.05, color=color.RGBColor(r=0.8))
    vis_scene.place(my_sphere, my_world)

    for ecc_i in range(10, 170, 20):
        for HM_j in range(0, 360, 45):
            my_flat_screen = PTVR.Stimuli.Objects.FlatScreen(
                size_in_meters=np.array([flatscreen_width, flatScreen_height]),
                color=color.RGBColor(r=0.8, g=0.8, b=0.0, a=1))
            my_flat_screen.set_perimetric_coordinates(
                eccentricity=ecc_i,
                halfMeridian=HM_j, radialDistance=radial_distance_of_object)
            # Orientation of flat screens
            # my_flat_screen.rotate_about_global_y (50)
            # my_flat_screen.rotate_about_global_z(45)
            # you could also run sequentially the two rotations above

            vis_scene.place(my_flat_screen, my_world)

    my_world.add_scene(vis_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
