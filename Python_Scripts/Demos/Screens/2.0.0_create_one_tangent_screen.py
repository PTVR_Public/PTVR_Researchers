# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\DEMOS\Screens\2.0.0_create_one_tangent_screen.py

Goal : show ...
a/ how to create and place one Tangent Screen (TS)
b/ how the orientation of the TS automatically changes when you only 
    modify its position ...
c/ ... however you cannot change its orientation yourself.

Note:  the origin of the current Coordinate System is approximately at your head level.

@author: eric castet
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import PTVR.Pointing.PointingCursor

PWD = PTVR.SystemUtils.PWD

username = "Eric" # do not comment : username is used (among other things) to create the 
# name of your results file.

tangent_screen_eccentricity = 30 # in degrees of visual angle
tangent_screen_half_meridian = 270 # in degrees
# some examples with cardinal directions
# 0 -> TS to the right of your "straigh ahead" with an eccentricity of tangent_screen_eccentricity  
# 90 -> TS above your "straigh ahead" with an eccentricity of tangent_screen_eccentricity  
# 180 -> TS to the left of your "straigh ahead" with an eccentricity of tangent_screen_eccentricity 
# 270 -> TS below  your "straigh ahead" with an eccentricity of tangent_screen_eccentricity

tangent_screen_radial_distance_m = 3 # in meters

height_of_headset = 1.2 # in meters
if username == "Eric": height_of_headset = 1.2 # determines the height of the CS origin 
# when Eric is seated
if username == "toto": height_of_headset = 1.0  # determines the height of the CS origin
# when toto is seated

viewpoint_position = np.array ( [0, height_of_headset, 0] ) 

def main():
    my_world = visual.The3DWorld (name_of_subject = username)
    my_world.translate_coordinate_system_along_global ( translation = viewpoint_position ) 
    
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    
    my_tangent_screen_white = PTVR.Stimuli.Objects.TangentScreen ( 
        size_in_meters = np.array ( [ 1, 0.5] ),      
        color=color.RGBColor (r=1, g= 1, b=1) )
    #######################################################################################
    # Modify the position parameters here if you want !
    # you can change the perimetric coordinates below, to see the effect on the TS orientation
    # Note that the origin of the current Coordinate System is approximately at your head.
    my_tangent_screen_white.set_perimetric_coordinates (
        eccentricity = tangent_screen_eccentricity,
        halfMeridian = tangent_screen_half_meridian,
        radialDistance = tangent_screen_radial_distance_m    )
    
    my_scene.place (my_tangent_screen_white, my_world)
    
    my_world.add_scene(my_scene)
    my_world.write() 

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld()
        
