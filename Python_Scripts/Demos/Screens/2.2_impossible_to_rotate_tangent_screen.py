# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Screens\
2.2_impossible_to_rotate_tangent_screen.py

@author: eric castet

Animation demo to show...
 that the  orientation of a tangent screen is not allowed by PTVR to be modified by rotate functions 
     such as rotate_about_global_x()  (see PTVR documentation)

    (this in contrast with the orientation of flat screens)
"""

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
import PTVR.Pointing.PointingCursor

PWD = PTVR.SystemUtils.PWD

username = "Ignace" # do not comment : username is used (among other things) to create the name of your results file.

flat_screen_radial_distance_m = 3 # in meters

def main():
    my_world = visual.The3DWorld (name_of_subject = username)
    
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    
    # WARNING : the screen is in front of your feet ! 
    my_tangent_screen_white = PTVR.Stimuli.Objects.TangentScreen (color = color.RGBColor (r=1, g=1, b=1)  )   
    my_tangent_screen_white.set_cartesian_coordinates (x = 0, y =  0, z = flat_screen_radial_distance_m)
    
    #######################################################################################
    # the following line of code will have no effect  
    # as TANGENT screens are not allowed to be modified by fonctions such as rotate_about_global_z()
    my_tangent_screen_white.rotate_about_global_z (angle_deg = 45)
    #######################################################################################    
 
    my_scene.place (my_tangent_screen_white,my_world)
    
    my_world.add_scene(my_scene)
    my_world.write() 

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld()
        
