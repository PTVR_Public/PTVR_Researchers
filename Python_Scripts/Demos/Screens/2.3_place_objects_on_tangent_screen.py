# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\DEMOS\Screens\
2.3_place_objects_on_tangent_screen.py

@author: eric castet

Animation demo to show:
        
How to place objects on a TANGENT screen with ...
    - either a 2D spherical coordinate system ( here a perimetric system) : ...
        set_perimetric_coordinates_on_screen (.
    - or a 2D cartesian coordinate system (in meter units) : ...
        set_cartesian_coordinates_on_screen ()

Note: the origin of the Coordinate System (CS) is at the center of the black point
        in front of you.


"""


import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

import PTVR.Stimuli.Color as color


PWD = PTVR.SystemUtils.PWD

# do not comment out: username is used (among other things) to create the name of your results file.
username = "Eric"

tangent_screen_radial_distance_m = 1  # in meters
tangent_screen_eccentricity = 60
tangent_screen_halfmeridian = 180

height_of_headset = 1.2  # in meters
if username == "Eric":
    height_of_headset = 1.2
viewpoint_Z = 0.7
head_viewpoint_position = np.array([0, height_of_headset, viewpoint_Z])


def place_objects_w_cartesian_CS(my_world, my_scene, my_screen):
    for i in np.arange(0, 0.6, 0.1):
        my_object = PTVR.Stimuli.Objects.Cube(
            size_in_meters=0.05, color=color.RGBColor(r=0.5, b=0.9))
        # CARTESIAN Coordinates
        my_object.set_cartesian_coordinates_on_screen(my_2D_screen=my_screen,
                                                      x_local=i, y_local=i)
        my_scene.place(my_object, my_world)
# ---------------------------------------------------------------------


def place_objects_w_perimetric_CS(my_world, my_scene, my_tangent_screen, my_ecc):
    for HM_j in range(0, 360, 45):
        my_object = PTVR.Stimuli.Objects.Cube(
            size_in_meters=0.1, color=color.RGBColor(r=1))
        # PERIMETRIC Coordinates
        my_object.set_perimetric_coordinates_on_screen(
            tangentscreen = my_tangent_screen,
            eccentricity_local_deg = my_ecc,
            half_meridian_local_deg = HM_j)

        my_scene.place(my_object, my_world)
# ---------------------------------------------------------------------


def main():
    my_world = visual.The3DWorld(name_of_subject=username)

    # Coordinate TRANSFORMATIONS
    my_world.translate_coordinate_system_along_global(head_viewpoint_position)
    # my_world.rotate_coordinate_system_about_global_y (90)

    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    my_viewpoint = PTVR.Stimuli.Objects.Sphere(
        size_in_meters=np.array([0.1, 0.1, 0.1]))
    # the default position of an object being np.array([0.0, 0.0, 0.0]),
    # my_viewpoint will be at the ORIGIN of the CURRENT coordinate system.

    # Place white TS straight-ahead with CARTESIAN Coordinates
    my_tangent_screen_white = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(r=1, g=1, b=1))
    my_tangent_screen_white.set_cartesian_coordinates(x=0.0, y=0,
                                                      z=tangent_screen_radial_distance_m)

    # Place other TS with PERIMETRIC Coordinates
    my_tangent_screen_yellow = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(r=1, g=1))
    my_tangent_screen_yellow.set_perimetric_coordinates(
        eccentricity=tangent_screen_eccentricity,
        halfMeridian=tangent_screen_halfmeridian,
        radialDistance=tangent_screen_radial_distance_m)
    my_tangent_screen_orange = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(r=1, g=0.5))
    my_tangent_screen_orange.set_perimetric_coordinates(
        eccentricity=tangent_screen_eccentricity,
        halfMeridian=180 + tangent_screen_halfmeridian,
        radialDistance=tangent_screen_radial_distance_m)

    # Place other TS with CARTESIAN Coordinates
    my_tangent_screen_green = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(g=1))
    my_tangent_screen_green.set_cartesian_coordinates(x=0, y=- tangent_screen_radial_distance_m,
                                                      z=tangent_screen_radial_distance_m)

    my_tangent_screen_blue = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(b=1))
    my_tangent_screen_blue.set_cartesian_coordinates(x=0, y=tangent_screen_radial_distance_m,
                                                     z=tangent_screen_radial_distance_m)

    my_scene.place(my_viewpoint, my_world)

    my_scene.place(my_tangent_screen_white, my_world)
    my_scene.place(my_tangent_screen_yellow, my_world)
    my_scene.place(my_tangent_screen_orange, my_world)
    my_scene.place(my_tangent_screen_green, my_world)
    my_scene.place(my_tangent_screen_blue, my_world)

    # place objects with 2D cartesian coordinates on the tangent screens
    place_objects_w_cartesian_CS(my_world=my_world, my_scene=my_scene,
                                 my_screen=my_tangent_screen_white)
    place_objects_w_cartesian_CS(my_world=my_world, my_scene=my_scene,
                                 my_screen=my_tangent_screen_yellow)
    place_objects_w_cartesian_CS(my_world=my_world, my_scene=my_scene,
                                 my_screen=my_tangent_screen_orange)
    place_objects_w_cartesian_CS(my_world=my_world, my_scene=my_scene,
                                 my_screen=my_tangent_screen_green)
    place_objects_w_cartesian_CS(my_world=my_world, my_scene=my_scene,
                                 my_screen=my_tangent_screen_blue)

    # place objects with 2D perimetric coordinates on the tangent screens
    place_objects_w_perimetric_CS(my_world=my_world, my_scene=my_scene,
                                  my_tangent_screen=my_tangent_screen_white, my_ecc=9.0)
    place_objects_w_perimetric_CS(my_world=my_world, my_scene=my_scene,
                                  my_tangent_screen=my_tangent_screen_yellow, my_ecc=9.0)
    place_objects_w_perimetric_CS(my_world=my_world, my_scene=my_scene,
                                  my_tangent_screen=my_tangent_screen_orange, my_ecc=9.0)
    place_objects_w_perimetric_CS(my_world=my_world, my_scene=my_scene,
                                  my_tangent_screen=my_tangent_screen_green, my_ecc=9.0)
    place_objects_w_perimetric_CS(my_world=my_world, my_scene=my_scene,
                                  my_tangent_screen=my_tangent_screen_blue, my_ecc=9.0)

    my_world.add_scene(my_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
