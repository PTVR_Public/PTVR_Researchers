# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\DEMOS\Screens\
2.0_create_tangent_screens_w_translation_of_CS.py

"CS" in the title stands for Coordinate System (not to be confused with Current System)

@author: eric castet


Animation demo to show:
a/ how a TANGENT screen adapts its orientation as a function of its position
    in order to face the ORIGIN of the CURRENT coordinate system.
    This origin is often called in PTVR code a VIEWPOINT (see PTVR documentation)
    
a2/ The local rotation of an object (black cylinder) placed at the origin of the current CS (Coordinate System)
    This cylinder is pointing along the current CS Z axis (locally forward)
    (by rotation about the current CS X axis)       

b/ Point a/ is checked with the cartesian and the perimetric coordinate systems

c/ Point a/ is checked with different coordinate transformations.
(you have to modify yourself the translation parameter in the line of code: 
    my_world.translate_coordinate_system_along_global ( translation = viewpoint_position)
    
    And you can also add more translations (see code below)


"""


import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

import PTVR.Stimuli.Color as color
import PTVR.Pointing.PointingCursor


PWD = PTVR.SystemUtils.PWD

# do not comment : username is used (among other things) to create the name of your results file.
username = "castet"

Tangent_screen_radial_distance_m = 3  # in meters
Tangent_screen_eccentricity = 30
Tangent_screen_halfmeridian = 180

height_of_headset = 1.2  # in meters
if username == "castet":
    height_of_headset = 1.2

viewpoint_position = np.array([0, height_of_headset, 0])


def main():
    my_world = visual.The3DWorld(name_of_subject=username)

    #######################################################################################
    # Modify the translation parameter here if you want !
    # Coordinate TRANSFORMATIONS
    # my_world.translate_coordinate_system_along_global ( translation = viewpoint_position )
    my_world.translate_coordinate_system_along_global(
        translation=np.array([1, 1.65, 0]))
    #######################################################################################

    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    # a cylinder at the origin of the current CS and pointing along the current CS Z axis (localyl forward)
    my_origin = PTVR.Stimuli.Objects.Cylinder(size_in_meters=0.1 * np.array([1.0, 1.0, 1.0]),
                                              rotation_in_current_CS=np.array([90, 0.0, 0.0]))

    my_tangent_screen_white = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(r=1, g=1, b=1))
    my_tangent_screen_white.set_cartesian_coordinates(
        x=0, y=0, z=Tangent_screen_radial_distance_m)

    # PERIMETRIC Coordinates
    my_tangent_screen_yellow = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(r=1, g=1))
    my_tangent_screen_yellow.set_perimetric_coordinates(
        eccentricity=Tangent_screen_eccentricity,
        halfMeridian=Tangent_screen_halfmeridian, radialDistance=Tangent_screen_radial_distance_m)
    my_tangent_screen_orange = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(r=1, g=0.5))
    my_tangent_screen_orange.set_perimetric_coordinates(
        eccentricity=Tangent_screen_eccentricity,
        halfMeridian=180 + Tangent_screen_halfmeridian, radialDistance=Tangent_screen_radial_distance_m)

    # CARTESIAN Coordinates
    my_tangent_screen_green = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(g=1))
    my_tangent_screen_green.set_cartesian_coordinates(
        x=0, y=- Tangent_screen_radial_distance_m, z=Tangent_screen_radial_distance_m)

    my_tangent_screen_blue = PTVR.Stimuli.Objects.TangentScreen(
        color=color.RGBColor(b=1))
    my_tangent_screen_blue.set_cartesian_coordinates(
        x=0, y=Tangent_screen_radial_distance_m, z=Tangent_screen_radial_distance_m)

    my_scene.place(my_origin, my_world)
    my_scene.place(my_tangent_screen_white, my_world)
    my_scene.place(my_tangent_screen_yellow, my_world)
    my_scene.place(my_tangent_screen_orange, my_world)
    my_scene.place(my_tangent_screen_green, my_world)
    my_scene.place(my_tangent_screen_blue, my_world)

    my_world.add_scene(my_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
