# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Motion_within_a_scene\
    move_with_periodic_function.py


Goal :  Illustrates the use of the Move_with_periodic_function callback.

What you see: A cube moving back and forth in front of you (along X axis).
            The cube starts moving at the same time as the scene starts by default, 
            but you can adjust delay_before_motion_starts_in_ms to add a delay after
            scene start.
            The cube motion lasts until you close the PTVR window yourself.
            
"""

import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callbacks.MovementCallback as movementCallback
import numpy as np

delay_before_motion_starts_in_ms = 0  # wrt to scene start

motion_amplitude_in_m = 1  # spatial period in meters of the periodic motion


def main():
    my_world = visual.The3DWorld(name_of_subject="carlos")
    my_world.translate_coordinate_system_along_global(
        np.array([0, 1.2, 0]))

    my_scene = PTVR.Stimuli.Scenes.VisualScene()
    my_cube = PTVR.Stimuli.Objects.Cube(size_in_meters=0.2)
    my_cube.set_cartesian_coordinates(x=- motion_amplitude_in_m/2, y=0, z=1)

    my_scene.place(my_cube, my_world)

    # Create one event and one callback
    # Create Event defining when cube motion will start (wrt to scene start)
    my_delay_event = event.Timer(delay_in_ms=delay_before_motion_starts_in_ms,
                                 timer_start="Start Scene")

    # Create Callback to define the motion of the cube    
    my_motion_callback = movementCallback.MoveWithPeriodicFunction (
        my_cube.id,
        temporal_period_in_sec=10,
        end_position=np.array(
            [motion_amplitude_in_m, 0, 0]),
    )
    # Add one interaction (between callback and event) to my_scene
    # First interaction : when my_delay_event is triggered, my_motion_callback
    # is called. In other words, when some delay has elapsed since the scene start,
    # then the cube is set in motion.
    my_scene.AddInteraction(events=[my_delay_event],
                            callbacks=[my_motion_callback])

    my_world.add_scene(my_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
