# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Motion_within_a_scene\
    cube_revolves_around_static_sphere.py
    
    
Goal :  Illustrates the use of the RevolveAround callback.

What you see: A cube revolving around a static sphere.
            The cube starts moving at the same time as the scene starts.
            The cube motion lasts until you close the PTVR window yourself.

Note : see 30_boy_asset_w_walk_and_run_animation.py (in another directory) 
        for a more complex demo using revolution movements.
        
"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere, Cube
import PTVR.Stimuli.Color as color
import numpy as np
from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Data.Event as event
import PTVR.Data.Callbacks.MovementCallback as movementCallbacks


my_world = The3DWorld()

def main():
    my_scene = VisualScene()

    # Sphere used to specify the revolution center of the cube.
    sphere_1 = Sphere ( size_in_meters=np.array([0.2, 0.2, 0.2]),
                        color=color.RGBColor(0.5, 0, 0),
                        position_in_current_CS=np.array([0, 0.54, 8.8]))
    my_scene.place(sphere_1, my_world)
    
    # Revolving cube   
    cube = Cube ( size_in_meters = np.array ([0.2, 0.2, 0.2]),
                  color = color.RGBColor (0.5, 0.2, 0),
                  position_in_current_CS = np.array([0, 0.54, 7] ) )
    my_scene.place(cube, my_world)

    ###############################################################################
    ############################### Interactions ##################################
    ###############################################################################

    ################################## Events #####################################
    event_timer_1 = event.Timer() # Starting 0 ms after scene start

    ################################## Callbacks ##################################
    cube_revolves_around = movementCallbacks.RevolveAround(
        object_id = cube.id,
        revolution_center_id = sphere_1.id,
        revolution_axis = np.array([1.0, 1.0, 0.0]),
        revolution_speed = 90,  # in degrees per second.
    )
    
    # Add interaction (between event and callback) to my_scene
    #
    my_scene.AddInteraction( events = [event_timer_1],
                             callbacks = [cube_revolves_around ] 
                             )
    my_world.add_scene(my_scene)
    my_world.write()

if __name__ == "__main__":
    main()
    LaunchThe3DWorld()  # Launch the Experiment with PTVR.
