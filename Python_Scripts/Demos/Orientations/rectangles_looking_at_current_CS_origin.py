#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Rotations\rectangles_looking_at_current_CS_origin.py

Goal: Illustrates the use of my_object.rotate_to_look_at (my_world.originCurrentCS.id).

All "rectangles" (organized in a spherical array) are looking at the origin of 
the current coordinate system.
 
Created On :(27 september 2021)
@author: E. Castet 
'''
import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

username = "Lucy"
radial_distance_of_object = 0.8  # in meters (0.8 is nice)
# distance between each object and origin of current Coordinate System
# this value is chosen so as to correspond (approximately) to the
height_of_head = 1.0
# height of the subject's headset during an experiment (either seated or standing)


if username == "Eric":
    height_of_head = 1.2
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================


def main():
    my_world = visual.The3DWorld(name_of_subject=username)

    #  Translate Origin upward to the approximate location of the head during the experiment
    my_world.translate_coordinate_system_along_global(
        translation=np.array([0, height_of_head, 0]))

    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    # Red cube at the current Coordinate System's Origin to identify the viewpoint
    my_cube = PTVR.Stimuli.Objects.Cube(
        size_in_meters=0.1, color=color.RGBColor(r=1))
    my_scene.place(my_cube, my_world)

    for ecc_i in range(10, 170, 15):
        for HM_j in range(0, 360, 45):
            my_rectangle = PTVR.Stimuli.Objects.Cube(
                size_in_meters=np.array([0.05, 0.05, 0.35]),
                color=color.RGBColor(r=0.4, g=0.2, b=0.05))
            my_rectangle.set_perimetric_coordinates(eccentricity=ecc_i, halfMeridian=HM_j,
                                                    radialDistance=radial_distance_of_object)
            # KEY fonction for "looking at" the origin !!
            my_rectangle.rotate_to_look_at(my_world.originCurrentCS.id)
            my_scene.place(my_rectangle, my_world)

    my_world.add_scene(my_scene)
    my_world.write()
# end of main()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
