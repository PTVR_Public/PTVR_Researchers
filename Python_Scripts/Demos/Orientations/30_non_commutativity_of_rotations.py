# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Orientations\
30_non_commutativity_of_rotations.py

Goal : 
    Illustrate that rotation is a non commutative operation.

What you see: 
    The cylinder in the middle is at its reference orientation.
    
    For the left cylinder, two successive rotations have been applied in the 
    following order: 1/ about X global axis and 2/ about Z global axis.
    
    For the right cylinder, two successive rotations have been applied in the 
    opposite order: 1/ about X global axis and 2/ about X global axis.    
    
    You can see that the left and right cylinder do not have the same
    orientation, thus showing the non commutativity of rotation.

                                                
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

height_of_head =  1.0 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head, 0])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint 

size_of_cylinder = np.array ([0.3, 0.6, 0.3])

def main():

    my_world = visual.The3DWorld ()
    #my_world.translate_coordinate_system_along_global (viewpoint_position)
    my_scene = PTVR.Stimuli.Scenes.VisualScene (global_lights_intensity = 2)
    
    # The middle cylinder shows the reference orientation of the PTVR Cylinder 
    # object (ie upward).
    # The reference orientation is obtained when 'rotation_in_current_CS' is
    # set to np.array([0.0, 0.0, 0.0]) (i.e. the default).
    # Note: at this stage of the script, the current Coordinate System 
    # is the Global CS.
    my_middle_cylinder = PTVR.Stimuli.Objects.Cylinder (
        position_in_current_CS = np.array ( [ 0, 0, 2] ),
        #rotation_in_current_CS = np.array ([0, 0, 45]),
        size_in_meters = size_of_cylinder
        )
    my_scene.place (my_middle_cylinder, my_world) 
   
    # Left cylinder
    # First rotation is about X
    # Second rotation is about Z
    my_left_cylinder = PTVR.Stimuli.Objects.Cylinder (
        position_in_current_CS = np.array ( [ -0.8 , 0, 2 ] ), 
        size_in_meters = size_of_cylinder
        ) 
    my_left_cylinder.rotate_about_global_x (90)
    my_left_cylinder.rotate_about_global_z (90)    
    my_scene.place (my_left_cylinder, my_world)
    
    # Right cylinder
    # First rotation is about Z
    # Second rotation is about X
    my_right_cylinder = PTVR.Stimuli.Objects.Cylinder (
        position_in_current_CS = np.array ( [ 0.8 , 0, 2 ] ),
        size_in_meters = size_of_cylinder
        )    
    my_right_cylinder.rotate_about_global_z (90)
    my_right_cylinder.rotate_about_global_x (90)        
    my_scene.place (my_right_cylinder, my_world)            
    
    my_world.add_scene (my_scene)      
    my_world.write () 
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()