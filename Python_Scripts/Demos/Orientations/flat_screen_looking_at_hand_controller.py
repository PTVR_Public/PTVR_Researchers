# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Orientations\flat_screen_looking_at_hand_controller.py

Goal :   Illustrate the use of my_object.rotate_to_look_at ( my_world.handControllerRight.id )  
        and of my_object.rotate_to_look_at_in_opposite_direction (  my_world.handControllerRight.id )

What you see:
    (do not forget to switch on your hand-controller)
    When you move your hand-controller, the flat screen in front of you changes 
    its orientation by looking at your right hand-controller.

'''

import PTVR.Visual as visual # Used to create the experiment
import PTVR.SystemUtils # Used to launch the experiment
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects # Used to create the scene , visual object , ui object
import numpy as np # np.array is internally used in PTVR.(eg position)

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Henry"
height_of_head = 1.0 # in meters : this value is chosen so as to correspond 
# (approximately) to the height of the subject's headset during
# an experiment (either seated or standing).
if username == "Eric": height_of_head = 1.2
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

def main():
    my_world = visual.The3DWorld ()
    #  Translate UP and forward the CS
    my_world.translate_coordinate_system_along_global ( translation = np.array ([0, height_of_head, 1]))  
    
    my_scene = PTVR.Stimuli.Scenes.VisualScene (is_right_hand_controller_visible = True)

    
    my_flat_screen = PTVR.Stimuli.Objects.FlatScreen()   
    #my_flat_screen.rotate_to_look_at ( my_world.handControllerRight.id )
    
    my_flat_screen.rotate_to_look_at (  my_world.handControllerRight.id )
    
    my_scene.place (my_flat_screen, my_world)
    
    my_world.add_scene (my_scene)
    my_world.write() # Write the experiment to .JSON file    
    
#   __main__ if statement allows to import this demo
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld() # Launch the Experiment with PTVR.