#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Rotations\eyes_looking_at_you.py

Goal: Illustrates the use of my_object.rotate_to_look_at (my_world.headset.id).

What you see:
    Enjoy being near the center of a sphere of eyes that look at YOU 
    (actually at your headset) when you move your head ! 

Note: The eyes can also look at the red point in front of you,
     or at your moving handcontroller,
     and so on... 
     (this is achieved by modifying the argument passed to 'id_object_to_lookAt'
     in rotate_to_look_at () )

'''

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Simon"
radial_distance_of_object = 0.7  # in meters
# distance between each object and origin of current Coordinate System
height_of_head = 1.0  # in meters
# this value is chosen so as to correspond (approximately) to the height of the subject's
# headset during an experiment (either seated or standing)
if username == "Eric":
    height_of_head = 1.2

sphere_diameter = 0.5  # 0.5 is nice

# =============================================================================
#                              END of PARAMETERS                                 #
# =============================================================================


def main():
    my_world = visual.The3DWorld(name_of_subject=username)

    #  Translate UP the global CS so that the CS's origin is at your eye level
    my_world.translate_coordinate_system_along_global(
        np.array([0, height_of_head, 0]))

    my_sphere_in_front_of_me = PTVR.Stimuli.Objects.Sphere(
        position_in_current_CS=np.array(
            [0.0, 0.0, 0.8 * radial_distance_of_object]),
        size_in_meters=0.05, color=color.RGB255Color(r=255))
    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    for ecc_i in range(10, 170, 15):
        for HM_j in range(0, 360, 45):

            # the globe of the eye
            my_sphere = PTVR.Stimuli.Objects.Sphere(size_in_meters=np.array([sphere_diameter, sphere_diameter, sphere_diameter]),
                                                    color=color.RGB255Color(r=251, g=251, b=229, a=1))
            my_sphere.set_perimetric_coordinates(eccentricity=ecc_i, halfMeridian=HM_j,
                                                 radialDistance=radial_distance_of_object)

            # the iris of the eye
            inner_sphere = PTVR.Stimuli.Objects.Sphere(
                size_in_meters=(sphere_diameter/2))
            inner_sphere.set_parent(
                my_sphere, x_local=0, y_local=0, z_local=0.015)
            # my_sphere is now the inner_sphere's parent

            # Scaled resizing of my_sphere and its children (here the inner_sphere)
            my_sphere.resize_with_its_children(0.1, 0.1, 0.1)

            # KEY fonction for "looking at" some location
            my_sphere.rotate_to_look_at(
                id_object_to_lookAt=my_world.headset.id)

            # alternative object to look at
            # my_sphere.rotate_to_look_at ( id_object_to_lookAt =
            #                                   my_sphere_in_front_of_me.id)

            # Play around with ...
            # possible arguments to id_object_to_lookAt:
            # my_object.id (here my_sphere_in_front_of_me.id),
            # my_world.headset.id,
            # my_world.HandControllerRight.id, my_world.HandControllerLeft.id,
            # my_world.originGlobalCS.id, my_world.originCurrentCS.id

            my_scene.place(my_sphere_in_front_of_me, my_world)
            my_scene.place(my_sphere, my_world)
            my_scene.place(inner_sphere, my_world)

    my_world.add_scene(my_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
