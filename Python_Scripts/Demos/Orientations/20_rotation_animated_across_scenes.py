# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Orientations\
    1_rotation_animated_across_scenes_axis.py

Goal :  Illustrates the use of my_object.rotate_about_current_z (rot) 

What you see:
    A cube at 2 m in front of you at the floor level changes its orientation 
    across scenes, i.e. it is rotating across scenes

"""


import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

orientation_change = 10 # change it to modidfy speed of rotation 

def main():
    my_world = visual.The3DWorld (name_of_subject = "Ali")

    # Object created once with its rotation updated in each step of the loop
    # This is more efficient than creating the object in each step of the loop
    my_cube = PTVR.Stimuli.Objects.Cube ( size_in_meters = np.array([0.4, 0.2, 0.2]) )
    my_cube.set_cartesian_coordinates (x=0, y=0, z=2)
        
    for rot in np.arange (0, 360, orientation_change): # np.arange is used to create a range of FLOATS
        my_scene = PTVR.Stimuli.Scenes.SimplifiedTimerScene ( 
            scene_duration_in_ms = 200,
            global_lights_intensity=1.5)
        
        my_cube.rotate_about_current_z (orientation_change) # rotation around current CS Z axis

        my_scene.place (my_cube,my_world)
        my_world.add_scene (my_scene)
    # If you do not want a cyclic animation, comment out the following line 
    # which restarts the loop at the firt scene.
    my_scene.SetNextScene ( my_world.id_scene[0] ) 
    
    my_world.write()

if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()


