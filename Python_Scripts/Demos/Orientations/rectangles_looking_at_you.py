#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Rotations\rectangles_looking_at_you.py

Goal: Illustrates the use of my_object.rotate_to_look_at (my_world.headset.id)

What you see:
    Not very sexy but important to understand what is going on.
    
    Enjoy being near the center of a sphere of 3D rectangles that are 
    "looking at" you (at your headset actually) when you move your head.
    
    Note that this head-contingency prevents you from seeing the sides of the 3D rectangles.
    In other words, you see them as 2D SQUARES.
    If you want to see the sides of the rectangles, i.e. if you want to see their
    3D structure, remove the head-contingency by 
    commenting out my_rectangle.rotate_to_look_at (my_world.headset.id).
    After commenting out this line of code, note that all rectangles will have their DEFAULT 
    orientation (in short, the rectangles' long axes will be parallel to the 
                 global Z axis)
                 
Created On :(27 september 2021)
@author: E. Castet                 
'''

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================

username = "Julie"
radial_distance_of_object = 0.8  # in meters
# distance between each object and origin of current Coordinate System
height_of_head = 1.0  # in meters : this value is chosen so as to correspond
# (approximately) to the height of the subject's headset during
# an experiment (either seated or standing).
if username == "Eric":
    height_of_head = 1.2

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================


def main():
    my_world = visual.The3DWorld(name_of_subject=username)

    #  Translate UP the global CS so that the CS's origin is at your eye level
    my_world.translate_coordinate_system_along_global(
        translation=np.array([0, height_of_head, 0]))

    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    for ecc_i in range(10, 170, 15):
        for HM_j in range(0, 360, 45):
            my_rectangle = PTVR.Stimuli.Objects.Cube(
                size_in_meters=np.array([0.1, 0.1, 0.15]))
            my_rectangle.set_perimetric_coordinates(
                eccentricity=ecc_i, halfMeridian=HM_j,
                radialDistance=radial_distance_of_object)

            # KEY Function : comment out the line below to see the rectangles' 3D structure
            my_rectangle.rotate_to_look_at(my_world.headset.id)

            my_scene.place(my_rectangle, my_world)

    my_world.add_scene(my_scene)
    my_world.write()
# end of main()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
