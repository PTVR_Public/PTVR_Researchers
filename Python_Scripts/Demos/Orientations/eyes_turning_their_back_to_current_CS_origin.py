#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Rotations\eyes_turning_their_back_to_current_CS_origin.py

Goals :   Illustrate the use of : 
            my_object.rotate_to_look_at_in_opposite_direction ( my_world.originCurrentCS.id)  
       
            my_object.set_parent () 
            
What you see :
    To the left:
    All "eyes" (organized in a spherical array) are looking at the origin of 
the current coordinate system (identified by a red cube).

    To the right:
    Same thing, except that all eyes are now turning their back to the origin of 
the current coordinate system.
'''

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Jennifer"
radial_distance_of_object = 0.4  # in meters
# distance between each eye and origin of current Coordinate System

height_of_head = 1.0  # in meters
# this value is chosen so as to correspond (approximately )to the height of the subject's
# headset during an experiment (either seated or standing)

if username == "Eric":
    height_of_head = 1.2

distance_between_viewpoints = 1
viewpoint_1_position = np.array(
    [-distance_between_viewpoints/2, height_of_head, 1])
# viewpoints are identified by a red cube

sphere_diameter = 0.4  # 0.4 is nice

counter = 0

# =============================================================================
#                              END of PARAMETERS                                 #
# =============================================================================


def draw_objects(my_world, my_scene):

    global counter
    counter += 1

    # Red cube at the current Coordinate System's Origin to identify the viewpoint
    my_cube = PTVR.Stimuli.Objects.Cube(
        size_in_meters=0.04, color=color.RGBColor(r=0.8))
    # Note that, when creating an object, here the cube, default position in the current CS
    # is the triplet np.array([0.0, 0.0, 0.0])

    my_scene.place(my_cube, my_world)

    for ecc_i in range(10, 170, 15):
        for HM_j in range(0, 360, 45):

            # the globe of the eye
            my_sphere = PTVR.Stimuli.Objects.Sphere(
                size_in_meters=np.array(
                    [sphere_diameter, sphere_diameter, sphere_diameter]),
                color=color.RGB255Color(r=251, g=251, b=229, a=1))
            my_sphere.set_perimetric_coordinates(eccentricity=ecc_i, halfMeridian=HM_j,
                                                 radialDistance=radial_distance_of_object)

            # the iris of the eye
            inner_sphere = PTVR.Stimuli.Objects.Sphere(
                size_in_meters=(sphere_diameter/2))
            inner_sphere.set_parent(
                my_sphere, x_local=0, y_local=0, z_local=0.015)
            # my_sphere is now the inner_sphere's parent

            # scaled resizing of my_sphere and its children (here the inner_sphere)
            my_sphere.resize_with_its_children(0.1, 0.1, 0.1)

            # LOOK AT or Turn your back to ...
            if counter == 1:  # if counter == 1 -> first time -> to the left
                my_sphere.rotate_to_look_at(my_world.originCurrentCS.id)
            else:  # if counter == 2 -> second time -> to the right
                my_sphere.rotate_to_look_at_in_opposite_direction(
                    my_world.originCurrentCS.id)

            my_scene.place(my_sphere, my_world)
            my_scene.place(inner_sphere, my_world)
# End of draw_objects (my_world, my_scene)
# --------------------------------------


def main():
    my_world = visual.The3DWorld(name_of_subject=username)
    my_scene = PTVR.Stimuli.Scenes.VisualScene()

    # To the left: All eyes look at the current CS origin
    # Translate the global Coordinate System to the left
    my_world.translate_coordinate_system_along_global(
        translation=viewpoint_1_position)

    draw_objects(my_world, my_scene)

    # To the right: All eyes turn their back to the current CS origine
    # Translate current CS to the right (by distance_between_viewpoints)
    my_world.translate_coordinate_system_along_global(
        translation=np.array([distance_between_viewpoints, 0, 0]))
    draw_objects(my_world, my_scene)

    my_world.add_scene(my_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
