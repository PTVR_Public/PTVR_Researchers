# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Orientations\
10_reference_orientation_versus_rotations.py

Goals : 
    a/ illustrate that each primitive PTVR object has a reference orientation.
    Here you can see that the cylinder's reference orientation is vertical
    (as shown by the cylinder in the middle).
    
    b/ illustrate that rotations applied to an object are defined with 
    respect to the object's reference orientation

                     
Note : 
                                                
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

height_of_head =  1.0 # this value is chosen so as to correspond (approximately)
# to the height of the subject's headset during an experiment (either seated or standing)
viewpoint_position = np.array ([0, height_of_head, 0])  
# Recall: a viewpoint is the origin of the current CS used to draw
# the stimuli to be "seen" from this viewpoint 

size_of_cylinder = np.array ([0.3, 0.6, 0.3])

def main():

    my_world = visual.The3DWorld ()
    #my_world.translate_coordinate_system_along_global (viewpoint_position)
    my_scene = PTVR.Stimuli.Scenes.VisualScene (global_lights_intensity = 2)
    
    # The middle cylinder shows the reference orientation of the PTVR Cylinder 
    # object (ie upward).
    # The reference orientation is obtained when 'rotation_in_current_CS' is
    # set to np.array([0.0, 0.0, 0.0]) (i.e. the default).
    # Note: at this stage of the script, the current Coordinate System 
    # is the Global CS.
    my_middle_cylinder = PTVR.Stimuli.Objects.Cylinder (
        position_in_current_CS = np.array ( [ 0, 0, 2] ),
        rotation_in_current_CS = np.array ([0.0, 0.0, 0.0]),
        size_in_meters = size_of_cylinder
        )
    my_scene.place (my_middle_cylinder, my_world) 
   
    # The left cylinder is rotated by +50 degrees about global X
    # with respect to the Cylinder's reference orientation.
    my_left_cylinder = PTVR.Stimuli.Objects.Cylinder (
        position_in_current_CS = np.array ( [ -0.8 , 0, 2 ] ), 
        rotation_in_current_CS = np.array ([ 50, 0, 0]),
        size_in_meters = size_of_cylinder
        ) 
    my_scene.place (my_left_cylinder, my_world)

    # The right cylinder is rotated by -50 degrees about global X
    # with respect to the Cylinder's reference orientation.    
    my_right_cylinder = PTVR.Stimuli.Objects.Cylinder (
        position_in_current_CS = np.array ( [ 0.8 , 0, 2 ] ),
        rotation_in_current_CS = np.array ([ -50, 0, 0]),
        size_in_meters = size_of_cylinder
        )    
    my_scene.place (my_right_cylinder, my_world)    

    my_text = PTVR.Stimuli.Objects.Text ( 
        text = 'Reference orientation of \n a PTVR cylinder is vertical \n as shown by the middle cylinder.', 
        visual_angle_of_centered_x_height_deg = 1,
        position_in_current_CS =  np.array ( [ 0 , 1.5, 2 ] ))
    my_scene.place (my_text, my_world)        
    
    my_world.add_scene (my_scene)      
    my_world.write () 
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()