# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Dichoptic_and_Monocular_Viewing\
alternate_background_color_of_masked_eye.py

Goal: 

    Press Trigger of right hand-controller to change background color of the 
    'masked' eye.
    

    
Created on: Fri Mar 10 09:16:11 2023
@author: jtermozm
"""
import PTVR.Visual # Used to create the experiment
import PTVR.SystemUtils # Used to launch the experiment
import PTVR.Stimuli.Scenes # Used to create the scene 
import PTVR.Stimuli.Objects # Used to create visual object , ui objec

import PTVR.Stimuli.Color as color # Used for color in PTVR's objects
import PTVR.Data.Event as event # Used for PTVR's event 
import PTVR.Data.Callback as callback # Used for PTVR's callback 
from PTVR.Pointing.PointingCursor import MaskedEye
import numpy as np # np.array is internally used in PTVR.(eg position)

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Doe" #The subject's name
   
#the description of the experiment, what it does. It'll be integrated in the graphical interface.
text_description ="""

"""
my_event_true =  event.HandController(valid_responses=['right_trigger'],mode="press")
my_event_false = event.HandController(valid_responses=['right_trigger'],mode="release")

my_masked_eye = MaskedEye.RIGHT_EYE
my_masked_eye_background_color = color.RGBColor(0.6, 0.6, 0.6, 1)

default_color_background = color.RGB255Color(r= 200, g= 200, b= 200,a= 1.0)

new_color_background = color.RGB255Color(r=0,g= 0,b= 0,a= 1.0)
which_eye = "Right"#"Left","Right","Both"
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

def main():
    my_world = PTVR.Visual.The3DWorld(detailsThe3DWorld=text_description)
    my_world.set_monocular(masked_eye = my_masked_eye,masked_eye_background_color = my_masked_eye_background_color)
    my_scene = PTVR.Stimuli.Scenes.VisualScene ( background_color=default_color_background )
    my_cube = PTVR.Stimuli.Objects.Cube(position_in_current_CS = np.array([0,0,5]))
    my_scene.place(my_cube,my_world)
    ##callback ChangeBackgroundColor Activate
    my_callback_activate = callback.ChangeBackgroundColor(new_color=new_color_background,which_eye=which_eye ,effect = "activate")

    my_scene.AddInteraction(events= [my_event_true],callbacks =[my_callback_activate])
    
    ##callback ChangeBackgroundColor Deactivate
    my_callback_deactivate = callback.ChangeBackgroundColor(effect = "deactivate")

    my_scene.AddInteraction(events= [my_event_false],callbacks =[my_callback_deactivate])
    my_world.add_scene(my_scene)
    my_world.write() # Write the Experiment to .json file    
    
#   __main__ if statement allows to import this demo
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld(username) # Launch the3DWorld with PTVR.