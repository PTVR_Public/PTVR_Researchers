# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Dichoptic_and_Monocular_Viewing\
Monocular_viewing.py

Goal: 
    Test the renderer of a cube on only one eye
    
Created on: Fri Mar 10 09:16:11 2023
@author: jtermozm
"""
import PTVR.Visual # Used to create the experiment
import PTVR.SystemUtils # Used to launch the experiment
import PTVR.Stimuli.Scenes # Used to create the scene 
import PTVR.Stimuli.Objects # Used to create visual object , ui objec

import PTVR.Stimuli.Color as color # Used for color in PTVR's objects

import numpy as np # np.array is internally used in PTVR.(eg position)
from PTVR.Pointing.PointingCursor import MaskedEye
# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Johnny" #The subject's name
   
my_masked_eye = MaskedEye.RIGHT_EYE
my_masked_eye_background_color = color.RGBColor(0.6, 0.6, 0.6, 1)
my_background_color = color.RGBColor(0.6, 0.6, 0.6, 1)
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================
##change logique choix 

### masked_eye 
def main():
    my_world = PTVR.Visual.The3DWorld ()
    my_world.set_monocular(masked_eye = my_masked_eye,masked_eye_background_color = my_masked_eye_background_color)
    my_scene = PTVR.Stimuli.Scenes.VisualScene (background_color = my_background_color)
    my_cube = PTVR.Stimuli.Objects.Cube(position_in_current_CS = np.array([0,0,5]))
    my_scene.place(my_cube,my_world)
    my_world.add_scene(my_scene)
    my_world.write() # Write the Experiment to .json file    
    
#   __main__ if statement allows to import this demo
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld(username) # Launch the3DWorld with PTVR.