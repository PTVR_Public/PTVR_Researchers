# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Serious_games\
    dancing_rabbit_in_room_w_interactions.py

Goal: this script illustrates :
        a/ the use of CustomObject() to import realistic scenes (here a room),
            characters, animal (here a standing rabbit), etc...
        b/ the use of callbacks allowing to move (and animate) the imported
        CustomObjects within a single scene (i.e. without the need to have 
        several scenes).
        c/ the basic ideas for creating serious games 
        (here for low vision children) with PTVR.
    
Task: point at the motionless rabbit with the hand-controller 
                and see what happens !

a/ What you have to do before running the present script:
    
    You must download on your PC some files that contain complex 3D entities
   whose size in memory would be too large in the long run  to be stored in PTVR. 
   These files are called "asset bundles" in Unity and are stored in the following 
   URL: https://files.inria.fr/ptvr/
        (asset bundles from this URL are free)
        
    For the present script, you must download the "animals_cartoons" and the
    "environments_freeroom" asset bundles.
    As each asset bundle consists of two files you must actually download the 
    following files:
    - animals_cartoons
    - animals_cartoons.manifest
    - environments_freeroom
    - environments_freeroom.manifest
    
    Once you have downloaded these files on your PC, you MUST store them
    in a directory called "/Asset_bundles". 
    In addition, this directory will have to be at the same level as 
    the "\PTVR_Researchers" directory.
	    For instance, if you installed PTVR such that it was in the following 
    directory:
	C:\my_directory\PTVR_Researchers
	then the directory containing ALL your asset bundles will now have to be in:
	C:\my_directory\Asset_bundles 
      
        
b/ Attributions and explanations about asset bundles: 
    The asset bundles mentioned above were created by PTVR in two steps:
        1/ The following free assets (not to be confused with asset bundles)
            were downloaded from the Unity asset store:
            For the rabbit:
            https://assetstore.unity.com/packages/3d/characters/low-poly-dancing-rabbit-22788      
            For the room:
            https://assetstore.unity.com/packages/3d/environments/apartment-kit-124055
    
        2/ The rabbit asset was then modified by PTVR developers to incorporate it into 
        an asset BUNDLE called "animals_cartoons". 
        This transformation process was done within the Unity software in a few 
        minutes.
        The same was done for the room.
    
Created by Carlos Aguilar
November 2023
                                                              
"""
import os
import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
from PTVR.Pointing.PointingCursor import PointingLaser, LaserContingency
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
from PTVR.Stimuli.Objects import CustomObject

# Decrease the following parameter to make the pointing task more difficult
my_activation_cone_radius_deg = 1.0

asset_bundle_name_for_room = "environments_freeroom"
asset_bundle_name_for_rabbit = "animals_cartoons"

def main():
    my_world = visual.The3DWorld ()

    my_scene = PTVR.Stimuli.Scenes.VisualScene(
        is_right_hand_controller_visible = True,
        skybox = "BrightMorning",  # "BrightMorning", "Afternoon", "Sunset", "Night", "Cloudy"
        global_lights_intensity = 0)

    # Create a laser pointer
    my_laser_beam = PointingLaser (hand_laser = LaserContingency.RIGHT_HAND,
                                  laser_color = color.RGBColor(g=0.7))
    my_scene.place_contingent_cursor (my_laser_beam)

    ###################### Room and other objects ###################
    my_room = CustomObject(
        asset_bundle_name = asset_bundle_name_for_room, # name of asset bundle
        asset_to_load = "Assets/Prefabs/environments_freeroom/FreeRoom.prefab", 
            # Path defined in the '.manifest' file of the asset bundle above.
        position_in_current_CS = np.array([-3.0, 0.0, 2.0])
        )
    sofa = CustomObject(
        asset_bundle_name = asset_bundle_name_for_room, # name of asset bundle        
        asset_to_load = "Assets/Prefabs/environments_freeroom/Sofa.prefab", 
            # Path defined in the '.manifest' file of the asset bundle above.        
        position_in_current_CS = np.array([0.08, 0.0, 4.95])
        )
    sofa.rotate_about_current_y(180)

    pcTable = CustomObject(
        asset_bundle_name = asset_bundle_name_for_room, # name of asset bundle
        asset_to_load = "Assets/Prefabs/environments_freeroom/PCTable.prefab", 
            # Path defined in the '.manifest' file of the asset bundle above.
        position_in_current_CS=np.array([-8.55, 0.0, 0.46])
        )
    pcTable.rotate_about_current_y(90)

    table = CustomObject(
        asset_bundle_name = asset_bundle_name_for_room, # name of asset bundle
        asset_to_load = "Assets/Prefabs/environments_freeroom/Table.prefab", 
            # Path defined in the '.manifest' file of the asset bundle above.
        position_in_current_CS = np.array([4.457, 0.0, 2.595])
        )
    table.rotate_about_current_y(-90)

    my_scene.place (my_room, my_world)
    my_scene.place (sofa, my_world)
    my_scene.place (pcTable, my_world)
    my_scene.place (table, my_world)

    ################################  Rabbit creation  ##############################
    # my_world.translate_coordinate_system_along_global ( np.array([0.0, 1.0, 0.0]) )
    # my_world.rotate_coordinate_system_about_global_y (30)
    rabbit = CustomObject(
        asset_bundle_name = asset_bundle_name_for_rabbit, # name of asset bundle
        asset_to_load = "Assets/Prefabs/animals_cartoons/Rabbit.prefab",  
            # Path defined in the '.manifest' file of the asset bundle above.
        position_in_current_CS = np.array([-0.5, 0.0, 3.0]),
        object_size = np.array([1.0, 1.0, 1.0]),
        # object_size is defined as a RATIO wrt the model of the custom object
        # (the model is in the asset bundle)
        # see details by hovering the mouse over 'CustomObject' (in the Spyder IDE).
        collider_size=np.array([0.25, 1.0, 0.25]),
        # collider_size is defined in meters.
        collider_center=np.array([0.0, 0.5, 0.0]),
        # collider_center is defined IN METERS wrt collider box,
        # (the collider origin being at the collider's bottom)
    )
    rabbit.rotate_about_current_y(180)
    my_scene.place (rabbit, my_world)
    ############################## End of Rabbit creation #####################

    ############### Create Interactions ###################
    # Events
    rabbit_is_pointed_at = PTVR.Data.Event.PointedAt(
        target_id =rabbit.id,
        activation_cone_origin_id = my_world.handControllerRight.id,
        activation_cone_radius_deg = my_activation_cone_radius_deg)
    rabbit_is_not_pointed_at = PTVR.Data.Event.PointedAt(
        target_id = rabbit.id,
        activation_cone_origin_id = my_world.handControllerRight.id,
        activation_cone_radius_deg = my_activation_cone_radius_deg,
        mode="release")

    # Callbacks
    play_rabbit_animation = PTVR.Data.Callback.AnimateObject(
        object_id = rabbit.id, effect = "activate")
    stop_rabbit_animation = PTVR.Data.Callback.AnimateObject(
        object_id = rabbit.id, effect = "deactivate")

    # Interactions
    my_scene.AddInteraction( events = [rabbit_is_pointed_at], 
                             callbacks = [ play_rabbit_animation] )
    my_scene.AddInteraction( events = [rabbit_is_not_pointed_at], 
                             callbacks = [stop_rabbit_animation] )

    my_world.add_scene(my_scene)
    my_world.write()

if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
