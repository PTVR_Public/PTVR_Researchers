# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Assets_to_import_into_PTVR\
    immerse_yourself_in_a_rotating_room.py

Goal: 
    The main goal is quite the same as that of the 
    "10_import_a_rabbit_asset_into_PTVR.py" script.

    Here, a second goal is to have fun feeling the strange unusual sensation
    that a room is rotating around you ! (press the left arrow
                                          key on your PC keyboard)
    
    This room is called "FreeRoom.prefab" in the 
    environments_freeroom asset bundle 
    (you can find this name in the environments_freeroom.manifest file). 

    See detailed explanations about importing asset bundles into PTVR in the 
     "10_import_a_rabbit_asset_into_PTVR.py" script.      
    
Attributions:
    The asset bundle mentioned above was created by PTVR in two steps:
        1/ The FreeRoom ASSET, aka prefab, (not to be confused with the asset 
                                            bundle)
            was freeely downloaded from the Unity asset store:
            https://assetstore.unity.com/packages/3d/environments/apartment-kit-124055        
    
        2/ This asset was then modified by PTVR developers to incorporate it into 
        an asset BUNDLE called "environments_freeroom". This transformation process 
        was done within the Unity software in a few minutes.
    
                                                              
"""
import os
import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Data.Callbacks.MovementCallback as movementCallbacks

import PTVR.Stimuli.Color as color
from PTVR.Stimuli.Objects import CustomObject

import PTVR.Data.Event as event

def main():
    my_world = visual.The3DWorld ()
    my_scene = PTVR.Stimuli.Scenes.VisualScene (
        skybox ="BrightMorning" # "BrightMorning", "Afternoon", "Sunset", "Night", "Cloudy"
        ) 
    
    ###################### Room and other objects ###################
    my_room  = CustomObject(
        asset_bundle_name = "environments_freeroom", # name of assetBundle
        asset_to_load = "Assets/Prefabs/environments_freeroom/FreeRoom.prefab", 
            # Path defined in the '.manifest' file of the asset bundle above.
        position_in_current_CS=np.array([-3.0, 0.0, 2.0])
        )    
    my_scene.place (my_room, my_world)
  
    press_left_key = event.Keyboard (   valid_responses = [ "left"], mode = "press")
    press_left_key_stop = event.Keyboard ( valid_responses = [ "left"], mode = "release")
    
    rotate_room_left = movementCallbacks.Rotate (
        object_id = my_room.id,    
        #left_rotate =True,
        rotation_vector=np.array([0,10,0]) # degree per second A VERIFIER ?
        )
    rotate_room_left_deactivate = movementCallbacks.Rotate (
        object_id = my_room.id,
        effect = "deactivate" )    
    
    
    # Interactions
    my_scene.AddInteraction( events = [press_left_key], 
                            callbacks = [rotate_room_left])
    my_scene.AddInteraction( events = [press_left_key_stop], 
                            callbacks = [rotate_room_left_deactivate])
    
    my_world.add_scene (my_scene)     
    my_world.write () 
    
if __name__ == "__main__":
                main()
                PTVR.SystemUtils.LaunchThe3DWorld()