# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Assets_to_import_into_PTVR\
30_boy_asset_w_walk_and_run_animations.py

    
Goal: this script illustrates :
        How to animate an asset (here the boy asset) containing SEVERAL
        animations. 
        
        In the boy asset used here, the available animations are:
            "walk", "run", "idle", "pickup" and "victory".
            
        These animations are displayed above the CustomObject 
        (here the boy) if the 'display_animations' parameter is set to True.
            
        The present script only uses "walk" and "run".
        
        This boy asset is in the "humans_cartoons" asset bundle that should 
        be stored on your PC before running the experiment.
        
        Reminder: Explanations on how to import free asset bundles into PTVR
            from https://files.inria.fr/ptvr/
            are in the "10_import_a_rabbit_asset_into_PTVR.py" script.
            Explanations on the basics of animating an asset are in the
            "20_animate_rabbit_asset.py script".
            

What you see: 
        A boy revolving around a tree.
        For the first half of the revolution around the tree, 
        the bow walks ("walk" animation of the boy asset).
        For the second half, he runs ("run" animation of the boy asset).

Note 1: When there are SEVERAL ANIMATIONS in an asset (as here), it is convenient
        to use their names (that should be provided by the person who created
        the asset bundle) instead of their index number.
        Here for instance, you will see how to pass "walk" or "run" as an
        argument to 'animation_string' in the AnimateObject() callback. 
        

Note 2: Attributions and explanations about asset bundles: 
    The "humans_cartoons" asset bundle was created by PTVR in two steps:
        1/ The ASSET (not to be confused with the asset bundle)
        was freeely downloaded from the Unity asset store:
        https://assetstore.unity.com/packages/3d/characters/humanoids/character-pack-free-sample-79870      
    
        2/ This asset was then modified by PTVR developers to incorporate it into 
        an asset BUNDLE called "humans_cartoons". This process was done within the 
        Unity software in a few minutes.    

"""

from PTVR.Visual import The3DWorld
from  PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Forest, CustomObject, Sphere
import PTVR.Stimuli.Color as color
import numpy as np

from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Data.Event as event
import PTVR.Data.Callbacks.MovementCallback as movementCallbacks
import PTVR.Data.Callback

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================
 
my_world = The3DWorld ()

def main():
    my_scene = VisualScene ( skybox= "BrightMorning" )

    # PTVR object Forest was initially an asset that we incorporated into PTVR. It
    # can thus now be created very easily without the need to import it with 
    # CustomObject ().
    my_forest = Forest (position_in_current_CS = np.array ([0, 0.92, 4])) 
    
    
    # Sphere used to specify the revolution center of the boy.
    # Its position was chosen by trial and error to coincide with that of a tree
    sphere_1 = Sphere (size_in_meters = np.array([0.2,0.2,0.2]), 
                       color = color.RGBColor (0.5,0,0),                 
                       position_in_current_CS = np.array ([-2.4, 0.54, 8.8]))
    
    my_scene.place (my_forest, my_world)        
    my_scene.place (sphere_1, my_world)

        
    boy_start_position = np.array([-3.5,0, 7])

    boy = CustomObject(
        asset_bundle_name = "humans_cartoons", # name of assetBundle
        asset_to_load = "Assets/Prefabs/humans_cartoons/Boy.prefab", 
            # Path defined in the '.manifest' file of the asset bundle above.
        position_in_current_CS = boy_start_position,
        display_animations = True
        )      
    my_scene.place (boy, my_world)
    
    ###############################################################################
    ############################### Interactions ##################################
    ###############################################################################
    
    ################################## Events #####################################
    period_of_event_in_ms = 4000 
    
    event_timer_1 = event.Timer (delay_in_ms = 0,
                                 period_in_ms = period_of_event_in_ms) # 
    # Starting 0 ms after scene start, this event is triggered periodically 
    # with a period of period_of_event_in_ms.    
    
    event_timer_2 = event.Timer (delay_in_ms = period_of_event_in_ms/2, 
                                 period_in_ms = period_of_event_in_ms)
    # With a delay after scene start of half the period of timer_1,
    # this event is triggered periodically with the same period as timer_1.
    # i.e. this event is in antiphase wrt timer_1.


    ################################## Callbacks ##################################       
    boy_walks = PTVR.Data.Callback.AnimateObject (
                                object_id = boy.id, 
                                animation_string = "walk" )
    
    boy_runs = PTVR.Data.Callback.AnimateObject (
                                object_id = boy.id, 
                                animation_string = "run" )    
    
    boy_revolves_around = movementCallbacks.RevolveAround( 
                     object_id = boy.id,
                     revolution_center_id = sphere_1.id,  
                     revolution_axis = np.array ([0.0, 1.0, 0.0]),
                     revolution_speed = 90, # in degrees per second.
                     # With 90 °/s, a full revolution takes 4000 ms
                     # which is the period of the timers declared above.
                     face_movement_direction = True )
    
    ## Add interactions (between event and callback) to my_scene
    #
    # Everytime the periodic event_timer_1 is triggered (here every 4 seconds),
    # the boy starts a/ revolving around the tree (movement callback and b/
    # walking (asset animation callback)
    my_scene.AddInteraction ( events = [ event_timer_1 ], 
                              callbacks = [ 
                                  boy_revolves_around,                                   
                                  boy_walks ] 
                              )    
    # Everytime the antiphase event_timer_2 is triggered, i.e. every half revolution,
    # the boy starts running (which automatically stop the previous walk animation)
    my_scene.AddInteraction ( events = [ event_timer_2 ], 
                              callbacks = [ boy_runs ]) 
        
    my_world.add_scene (my_scene)   
    my_world.write()

if __name__ == "__main__":
            main()
            LaunchThe3DWorld() # Launch the Experiment with PTVR.          
 
