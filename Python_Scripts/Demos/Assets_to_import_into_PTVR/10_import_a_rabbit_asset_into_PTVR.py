# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Assets_to_import_into_PTVR\
    10_import_a_rabbit_asset_into_PTVR.py

Goal: this script illustrates :
        a/ How to use a single function (namely CustomObject() ) to import a 
        complex 3D animal/character, here a rabbit, into PTVR. In the video game 
        terminology, this amounts to importing an "ASSET" into PTVR.
            See the more general definition of an "asset" (aka "game asset") in 
            many video game web sites, for example:
                https://vionixstudio.com/2021/12/04/what-is-an-asset-in-game-development/
                or https://retrostylegames.com/blog/what-are-assets-in-game-design/
        "What are game assets? A game asset is any resource that is used in 
        the development of a video game – from 3D models and textures to sound 
        effects and music. Game assets are created by different professionals, 
        including artists, animators, sound designers, and programmers. The 
        specific assets that are needed for a game will vary depending on the 
        genre, style, and scope of the project.
        Once the assets have been created, they are imported into the game engine 
        and used to build the game world."
        

a/ What you have to do before running the present script:
    
    You must download on your PC some files whose size in memory would be too 
    large in the long run to be stored in PTVR. These files are called 
    "asset bundles" in Unity. You can import free asset bundles into PTVR
    from https://files.inria.fr/ptvr/.
        
    For the present script, you must download the "animals_cartoons" asset bundle.
    As each asset bundle consists of two files you must actually download the 
    following files:
    - animals_cartoons
    - animals_cartoons.manifest
    
    Once you have downloaded these 2 files on your PC, you MUST store them
    in a directory called "/Asset_bundles". 
	In addition, this directory will have to be at the same level as 
    the "\PTVR_Researchers" directory.
	
	For instance, if you installed PTVR such that it was in the following directory:
	C:\my_directory\PTVR_Researchers
	then the directory containing ALL your asset bundles will now have to be in:
	C:\my_directory\Asset_bundles        
        
b/ what is done in the script to "import" the rabbit.
    In the "animals_cartoons" asset bundle, there is an asset or prefab called 
    Rabbit.prefab. You can check this fact by opening with a text editor
    the animals_cartoons.manifest file.
    Having the name of the asset bundle and the name of the prefab contained
    in the asset bundle, you can then simply use the following code:
    CustomObject (
        asset_bundle_name = "animals_cartoons",
        asset_to_load = "Assets/Prefabs/animals_cartoons/Rabbit.prefab"   
        ) 
    

c/ Attributions and explanations about asset bundles: 
    The asset bundle mentioned above was created by PTVR in two steps:
        1/ The rabbit ASSET, or prefab, (not to be confused with the asset bundle)
            was freeely downloaded from the Unity asset store:
            https://assetstore.unity.com/packages/3d/characters/low-poly-dancing-rabbit-22788        
    
        2/ This asset was then modified by PTVR developers to incorporate it into 
        an asset BUNDLE called "animals_cartoons". This transformation process 
        was done within the Unity software in a few minutes.
     
    
Created by Carlos Aguilar
November 2023
                                                              
"""
import os
import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
from PTVR.Pointing.PointingCursor import  PointingLaser,LaserContingency
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
from PTVR.Stimuli.Objects import CustomObject



def main():
    my_world = visual.The3DWorld ()
    
    my_scene = PTVR.Stimuli.Scenes.VisualScene (
        skybox ="BrightMorning" ) # "BrightMorning", "Afternoon", "Sunset", "Night", "Cloudy" 
    
    ###################### Rabbit asset ###################
    rabbit = CustomObject(
        asset_bundle_name = "animals_cartoons", # name of assetBundle
        asset_to_load = "Assets/Prefabs/animals_cartoons/Rabbit.prefab", 
            # Path defined in the '.manifest' file of the asset bundle above.
        position_in_current_CS = np.array([0, 0, 1]),
        )
    rabbit.rotate_about_current_y(180)
    
    my_scene.place (rabbit, my_world)    
    ############################## End of Rabbit creation #####################
        
    my_world.add_scene (my_scene)     
    my_world.write () 
    
if __name__ == "__main__":
                main()
                PTVR.SystemUtils.LaunchThe3DWorld()