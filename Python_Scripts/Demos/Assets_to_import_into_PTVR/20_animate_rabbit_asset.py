# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Assets_to_import_into_PTVR\
    20_animate_rabbit_asset.py

Goal: this script illustrates :
        How to animate an asset (here the rabbit asset) contained in an 
        asset bundle which has been imported with CustomObject () into PTVR.       
        
        This is done thanks to the AnimateObject() callback which is 
        called in the present case by a Timer event.
        
        Reminder: Explanations on how to import free asset bundles into PTVR
            from https://files.inria.fr/ptvr/
            are in the "10_import_a_rabbit_asset_into_PTVR.py" script. 
        
Note 1: Note that the rabbit asset contains only ONE animation (the 
        rabbit is dancing).
        In general, the information about the presence (and the number) of
        animations should be provided along with the asset bundle by 
        the person who created the asset bundle.
        
        In any case, this information is displayed above the CustomObject 
        (here the rabbit) if the 'display_animations' parameter is set to True.
        
        
Note 2: When there is only one animation (as here), 
        knowing the animation's name is not very useful as you can simply pass the 
        number 0 as an argument to 'animation_index' in the AnimateObject() 
        callback.

Note 3: See in the "30_boy_asset_w_walk_and_run_animation.py" script how to deal 
        with an asset that contains more than one animation.
        
Note 4: Attributions and explanations about asset bundles: 
    The asset bundle used here was created by PTVR in two steps:
        1/ The rabbit ASSET (not to be confused with the asset bundle)
            was freeely downloaded from the Unity asset store.
            The URL of this asset is:
                https://assetstore.unity.com/packages/3d/characters/low-poly-dancing-rabbit-22788        
    
        2/ This asset was then modified by PTVR developers to incorporate it into 
        an asset BUNDLE called "animals_cartoons". This process was done within 
        the Unity software in a few minutes.


    
Created by Carlos Aguilar
November 2023
                                                              
"""
import os
import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
from PTVR.Pointing.PointingCursor import  PointingLaser,LaserContingency
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color
from PTVR.Stimuli.Objects import CustomObject

def main():
    my_world = visual.The3DWorld ()
    
    my_scene = PTVR.Stimuli.Scenes.VisualScene (
        skybox ="BrightMorning" ) # "BrightMorning", "Afternoon", "Sunset", "Night", "Cloudy" 
    
    ###################### Rabbit asset ###################
    rabbit = CustomObject(
        asset_bundle_name = "animals_cartoons", # name of assetBundle
        asset_to_load = "Assets/Prefabs/animals_cartoons/Rabbit.prefab", 
            # Path defined in the '.manifest' file of the asset bundle above.
        position_in_current_CS = np.array([0, 0, 1]),
        
        display_animations = True
        )
    rabbit.rotate_about_current_y(180)
    
    my_scene.place (rabbit, my_world)    
    ############################## End of Rabbit creation #####################

    ############### Create Interaction ###################
    ##### Here an animation of the rabbit asset ##########
    
    ## Event
    my_delay_after_scene_start =  PTVR.Data.Event.Timer ( delay_in_ms = 3000 )

    ## Callback
    play_rabbit_animation = PTVR.Data.Callback.AnimateObject(
                        object_id = rabbit.id, 
                        animation_index = 0, # the argument is zero by default,
                        # it is shown here only for didactic purposes. 
                        )
   # Note that it is not necessary to change the default argument (zero) 
   # passed to 'animation index" as there is ONLY ONE animation in the 
   # rabbit asset.

    ## Interactions
    # Wait 3 seconds after the scene starts, and then play the rabbit animation.
    my_scene.AddInteraction ( 
                events = [my_delay_after_scene_start], 
                callbacks = [play_rabbit_animation] )
        
    my_world.add_scene (my_scene)     
    my_world.write () 
    
if __name__ == "__main__":
                main()
                PTVR.SystemUtils.LaunchThe3DWorld()