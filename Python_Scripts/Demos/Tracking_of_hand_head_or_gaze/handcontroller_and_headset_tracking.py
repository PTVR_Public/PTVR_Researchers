# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Tracking_of_hand_head_or_gaze\
handcontroller_and_headset_tracking.py

This script is a mix of handcontroller_tracking.py and headset_tracking.py.

Your task:
    Thanks to the laser beam attached to your handset and thanks to the 
    reticle controlled by your head, you can point at the tangent screen (TS) 
    in front of you. These two pointers are independent.
    While you're pointing at the TS with these two different pointers, 
    everytime you press the handcontroller trigger, the spatial coordinates
    of the two points of collision between these pointers and TS will be displayed 
    in the screens adjacent to the TS.


Created by Carlos Aguilar (January 2025)
"""

from PTVR.Visual import The3DWorld
from PTVR.Stimuli.Scenes import VisualScene
from PTVR.Stimuli.Objects import Sphere
import PTVR.Stimuli.Color as color
import numpy as np

from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ReticleImageFromDrawing as RG
import PTVR.Pointing.ImageFromDrawing as SFD
import PTVR.Pointing.ImageFromLoading as SFL
from PTVR.SystemUtils import LaunchThe3DWorld
import PTVR.Data.Callback
import PTVR.Data.Event
from PTVR.Blocks import DrawCircle


my_world = The3DWorld(
    output_hand_controller=True,  # True by default
    output_headset=True,  # True by default
    hand_controller_save_sampling_period=11,  
    hand_controller_events_are_recorded=True  # e.g. "Trigger of the hc"
)

viewpoint_position = np.array([0, 1.2, 0])

TS_viewing_distance = 8  # orthogonal distance between current origin (where
# the head should be) and the Tangent Screen (TS).

TS_size_deg = 10
TS_size_m = \
    PTVR.Tools.visual_angle_to_size_on_perpendicular_plane(
        visual_angle_of_centered_object_in_deg=TS_size_deg,
        viewing_distance_in_m=TS_viewing_distance
    )

circle_diameter_deg = TS_size_deg
circle_diameter_m = \
    PTVR.Tools.visual_angle_to_size_on_perpendicular_plane(
        visual_angle_of_centered_object_in_deg=circle_diameter_deg,
        viewing_distance_in_m=TS_viewing_distance
    )

reticle_diameter_deg = 2


my_contingency = ImageContingency.HEADSET


def set_headset(my_scene):
    my_reticle_image = RG.ReticleImageFromDrawing()
    my_reticle_cursor = ImageToContingentCursor(
        image=my_reticle_image,
        contingency_type=my_contingency,
        size_in_degrees=[reticle_diameter_deg, reticle_diameter_deg]
    )
    my_scene.place_contingent_cursor(my_reticle_cursor)

    # text to the right of TS
    text_for_GV_perimetric_coord = PTVR.Stimuli.Objects.Text(
        text="Headset collision\n in TS perimetric CS\n\necc=0\nhm=0",
        position_in_current_CS=np.array([TS_size_m * 1.5,
                                         TS_size_m * 1.3,
                                         TS_viewing_distance]),
        visual_angle_of_centered_x_height_deg=0.8,
        use_collider=True
    )
    my_scene.place(text_for_GV_perimetric_coord, my_world)

    # text to the right of TS
    text_for_GV_perimetric_global_coord = PTVR.Stimuli.Objects.Text(
        text="Headset collision\n in global perimetric CS\n\necc=0\nhm=0",
        position_in_current_CS=np.array([-TS_size_m * 1.5,
                                         TS_size_m * 1.3,
                                         TS_viewing_distance]),
        visual_angle_of_centered_x_height_deg=0.8,
        use_collider=True
    )
    my_scene.place(text_for_GV_perimetric_global_coord, my_world)

    trigger_button = PTVR.Data.Event.HandController(
        valid_responses=['right_trigger'], mode="press")

    display_spherical_callback = PTVR.Data.Callback.DisplayVector3GV(
        text_id=text_for_GV_perimetric_coord.id,
        label_of_GV_to_use="headset_collision_in_TS_perimetric",
        text_to_display="Headset collision\n in TS perimetric CS\n\n",
        vector_components_names=np.array(["ecc", "hm"])
    )

    display_global_spherical_callback = PTVR.Data.Callback.DisplayVector3GV(
        text_id=text_for_GV_perimetric_global_coord.id,
        label_of_GV_to_use="headset_collision_global_perimetric",
        text_to_display="Headset collision\n in global perimetric CS\n\n",
        vector_components_names=np.array(["ecc", "hm"])
    )

    my_scene.AddInteraction(
        events=[trigger_button],
        callbacks=[
            display_spherical_callback,
            display_global_spherical_callback]
    )


def set_handcontroller(my_scene):
    # Leaser beam contingent on handcontroller
    my_laser_beam = PointingLaser(
        hand_laser=LaserContingency.RIGHT_HAND,
        laser_color=color.RGBColor(r=0.7, g=0.5))
    my_scene.place_pointing_laser(my_laser_beam)

    # text to the right of TS
    text_for_GV_perimetric_coord = PTVR.Stimuli.Objects.Text(
        text="Handcontroller collision\n in TS perimetric CS\n\necc=0\nhm=0",
        position_in_current_CS=np.array([TS_size_m * 1.5,
                                         -TS_size_m * 1.3,
                                         TS_viewing_distance]),
        visual_angle_of_centered_x_height_deg=0.8,
        use_collider=True
    )
    my_scene.place(text_for_GV_perimetric_coord, my_world)

    # text to the right of TS
    text_for_GV_perimetric_global_coord = PTVR.Stimuli.Objects.Text(
        text="Handcontroller collision\n in global perimetric CS\n\necc=0\nhm=0",
        position_in_current_CS=np.array([-TS_size_m * 1.5,
                                         -TS_size_m * 1.3,
                                         TS_viewing_distance]),
        visual_angle_of_centered_x_height_deg=0.8,
        use_collider=True
    )
    my_scene.place(text_for_GV_perimetric_global_coord, my_world)

    trigger_button = PTVR.Data.Event.HandController(
        valid_responses=['right_trigger'], mode="press")

    display_spherical_callback = PTVR.Data.Callback.DisplayVector3GV(
        text_id=text_for_GV_perimetric_coord.id,
        label_of_GV_to_use="hc_collision_in_TS_perimetric",
        text_to_display="Handcontroller collision\n in TS perimetric CS\n\n",
        vector_components_names=np.array(["ecc", "hm"])
    )

    display_global_spherical_callback = PTVR.Data.Callback.DisplayVector3GV(
        text_id=text_for_GV_perimetric_global_coord.id,
        label_of_GV_to_use="hc_collision_global_perimetric",
        text_to_display="Handcontroller collision\n in global perimetric CS\n\n",
        vector_components_names=np.array(["ecc", "hm"])
    )

    my_scene.AddInteraction(
        events=[trigger_button],
        callbacks=[
            display_spherical_callback,
            display_global_spherical_callback]
    )


def main():
    # you can play around with the rotation value of the
    # current coordinate system
    my_world.rotate_coordinate_system_about_global_y(0)  # 0 by default

    my_scene = VisualScene(is_right_hand_controller_visible=True)

    my_sphere = Sphere(
        position_in_current_CS=np.array([0, -1, TS_viewing_distance]),
        size_in_meters=0.5)
    my_scene.place(my_sphere,  my_world)

    # TS in the middle
    my_tangent_screen_white = PTVR.Stimuli.Objects.TangentScreen(
        position_in_current_CS=np.array([0, 0, TS_viewing_distance]),
        size_in_meters=np.array([TS_size_m, TS_size_m]),
        color=color.RGBColor(r=0.5, g=0.5, b=0.5)
    )
    my_scene.place(my_tangent_screen_white, my_world)

    set_headset(my_scene)

    set_handcontroller(my_scene)

    # Iso-eccentricity circle
    DrawCircle(my_scene, my_world,
               position_in_current_CS=np.array(
                   [0, 0, TS_viewing_distance - 0.01]),
               circle_diameter=circle_diameter_m
               )

    my_world.add_scene(my_scene)
    my_world.write()


if __name__ == "__main__":
    main()
    LaunchThe3DWorld()  # Launch the Experiment with PTVR.
