# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 15:54:34 2025

@author: chopi
"""


import numpy as np
import pandas as pd


path_file = "C:\\PTVR\\Private\\PTVR_Researchers\\PTVR_Operators\\Results\\handcontroller_tracking\\"


path_file = "C:\\Users\Eric_Castet\Documents\Private\\PTVR_Researchers\\PTVR_Operators\\Results\\handcontroller_tracking\\"


#path_file = "C:\\Users\Eric_Castet\Documents\Private\\PTVR_Researchers\\PTVR_Operators\\Results\\saroi_exp_3_v1\\"

df = pd.read_csv(
    path_file + "handcontroller_tracking_hand_controller_DummyUser_2025-01-31-13-07-40.705.csv",
    sep=";")


timestamp_ms = df["timestamp_ms"]


print("Duplicated in timestamp_ms? : ", timestamp_ms.duplicated().any())


timestamp = df["timestamp"]


print("Duplicated in timestamp ? : ", timestamp.duplicated().any())
