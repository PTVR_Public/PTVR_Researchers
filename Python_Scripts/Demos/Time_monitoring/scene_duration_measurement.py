# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Time_monitoring\
    scene_duration_measurement.py

GOAL: measures the actual duration of each scene from a .csv results file
        and creates a distribution of these durations.

        BEWARE : just after you launch the script, the explorer file window might
        be HIDDEN behind the Spyder window (or your IDE) : 
        
        AND, to create a graph in an independent window, go to :
            Tools -> Preferences -> Ipython Console -> Graphics tab ->
                Graphics Backend submenu -> set Backend to Automatic.
        
Created on Thu Jul 20 11:12:42 2023
@author: chopi
"""

import pandas as pd
import numpy as np
from matplotlib.ticker import PercentFormatter
import matplotlib.pyplot as plt
import tkinter as tk
from tkinter import filedialog


# Window to open a file
root = tk.Tk()
root.withdraw()

file_path = filedialog.askopenfilename()

t_scene = pd.read_csv(file_path, sep=";")


def scene_duration(df, filter=150):
    
    dt = round(df["scene_end_timestamp"] - df["scene_start_timestamp"])
    
    if (dt > filter):
        return -1
    
    return dt


t_scene = t_scene.loc[:, ~t_scene.columns.str.contains('^Unnamed')]

t_scene["scene_duration"] = t_scene.apply(scene_duration, args=(200,), axis=1)

t_scene = t_scene[t_scene["scene_duration"] != -1]

t_scene_timer = t_scene[t_scene["event_name"] == "Timer"]

start_bin = min(t_scene_timer["scene_duration"]) - 0.5#round(t_scene_timer["scene_duration"].mean()) - 3.5

diff_end_start = 2 + (max(t_scene_timer["scene_duration"]) - min(t_scene_timer["scene_duration"]))

bins_ = [start_bin + 1*i for i in range(diff_end_start)]


# An "interface" to matplotlib.axes.Axes.hist() method
W =  np.ones(len(t_scene_timer["scene_duration"]))/(len(t_scene_timer["scene_duration"]))
n, bins, patches = plt.hist(x=t_scene_timer["scene_duration"], weights=W, bins=bins_, color='red',
                            alpha=0.7, rwidth=1, edgecolor='black', linewidth=1.2)


number_of_scenes = len(t_scene_timer["scene_id"].unique())

plt.grid(axis='y', alpha=0.75)
fs = 22
fs_xticks = 400/(diff_end_start)
plt.xticks(range(int(start_bin),int(start_bin)+diff_end_start), fontsize=fs_xticks)
plt.yticks(fontsize=fs)
plt.xlabel('Value', fontsize=fs)
plt.ylabel('Percentage', fontsize=fs)
plt.title('Distribution of Scene Duration\nNumber of scenes : %d'%number_of_scenes, fontsize=fs)
plt.text(23, 45, r'$\mu=15, b=3$')
#plt.grid(visible=True, axis="both")
plt.gca().yaxis.set_major_formatter(PercentFormatter(1))

plt.subplots_adjust(top=0.925,
bottom=0.075,
left=0.075,
right=0.97,
hspace=0.2,
wspace=0.2)

plt.show ()
