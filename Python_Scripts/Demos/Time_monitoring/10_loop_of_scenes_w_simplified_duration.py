# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Time_monitoring\
    10_loop_of_scenes_w_simplified_duration.py

Goal: displays ONE for LOOP of X scenes whose durations are defined by a 
        simplified interaction.
        This is mainly to check that the measure of the actual scene duration is correct.
        This measure comes from the timestamps recorded in the results file.
        
See the PTVR documentation : 
    Section : User Manual -> Interactions between Events and Callbacks -> Simplified Interactions

'''

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes

import PTVR.Stimuli.Objects


# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Martin" # subject's name
   
nb_of_scenes = 500
my_scene_duration_in_ms = 100 # in Ms
# 100 ms corresponds to 9 frames with a refresh rate of 90 Hz (HTC Vive Pro)

# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================
   
def main():

   my_world = visual.The3DWorld (name_of_subject = username)
   
   
   for i in range (0, nb_of_scenes):
   
       my_scene = PTVR.Stimuli.Scenes.SimplifiedTimerScene (scene_duration_in_ms = my_scene_duration_in_ms)
    
       # display Scene information on subject's screen
       my_text = PTVR.Stimuli.Objects.Text ( text = "This single scene (number %d) lasts "%(i+1) + str( my_scene_duration_in_ms) + " ms\n" + 
                                           "and then the script will QUIT PTVR (ie the PTVR window will close itself) \n" + 
                                           "Or you can quit by closing yourself the PTVR window (mouse click in the top right corner)",
                                           position_in_current_CS = np.array ([0, 1, 1]), visual_angle_of_centered_x_height_deg = 1)
       my_scene.place(my_text,my_world)
       
    
       my_world.add_scene (my_scene)
   my_world.write() # Write The3DWorld to .json file
    
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()