#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\Colors\color_discrimination.py

Goal : Do you perceive the color difference between the two colors?

        You can play around with the red_increment variable.
'''

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Stimuli.Color as color

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Mary"

height_of_head = 1.0 # # in meters : this value is chosen so as to correspond to 
# the height of the subject's headset during an experiment (either seated or standing)
if username == "Eric": 
    height_of_head = 1.2

red_increment = 0.08
    
def main():
    my_world = visual.The3DWorld ( name_of_subject = username )   
    vis_scene  = PTVR.Stimuli.Scenes.VisualScene ()  
    
    # Sphere to the left !
    my_sphere = PTVR.Stimuli.Objects.Sphere (
        position_in_current_CS = np.array ( [-0.04, height_of_head, 0.1] ),
        size_in_meters = 0.05, 
        color = color.RGBColor (    r = 0.8, 
                                    g = 0.5, 
                                    b = 0.0, a = 1 )  ) 
        # ( r=0.8, g=0.5, b=0.0) -> ORANGE ) 
    vis_scene.place (my_sphere, my_world)

    # Sphere to the right !    
    my_sphere = PTVR.Stimuli.Objects.Sphere ( 
        position_in_current_CS = np.array ( [0.04, height_of_head, 0.1] ),
        size_in_meters = 0.05, 
        color = color.RGBColor (    r = 0.8 + red_increment ,
                                    g = 0.5, 
                                    b = 0.0, a = 1 )  ) 
    vis_scene.place (my_sphere, my_world)  

    my_world.add_scene (vis_scene)
    my_world.write() 

if __name__ == "__main__":
        main()
        PTVR.SystemUtils.LaunchThe3DWorld( )
