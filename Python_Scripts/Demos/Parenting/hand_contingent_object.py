# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Parenting\
hand_contingent_object.py

Goal of this demo: Illustrates a simple code where the right hand-controller
    becomes the parent of an object.
    In effect, this renders the object HAND-CONTINGENT (neuroscience terminology)
    or HAND-CONTROLLED (gaming terminology). 

What you experience: move your hand and see the object move along with your hand.

Note: you can make the object HEAD-contingent by substituting 
    my_world.headset.id to my_world.handControllerRight.id
                                                          
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

subject_id = "Ignace"

def main():

    my_world = visual.The3DWorld (name_of_subject = subject_id)
    my_scene = PTVR.Stimuli.Scenes.VisualScene ( is_right_hand_controller_visible = True )
    
    my_object = PTVR.Stimuli.Objects.Cube ( size_in_meters = 0.5,
        position_in_current_CS = np.array ( [ 0, 0, 2] ) )
        # this position becomes the relative position of the object 
        # wrt the hand-controller as soon as the latter becomes the 
        # object's parent in the next code line.
    
    ## PARENTING
    # HAND-CONTINGENT or HAND-CONTROLLED
    my_object.set_parent_id ( my_world.handControllerRight.id )
    # HEAD-CONTINGENT or HEAD-CONTROLLED
    #my_object.set_parent_id ( my_world.headset.id )    
    
    my_scene.place (my_object, my_world)
    my_world.add_scene (my_scene)   
   
    my_world.write () 
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()