# -*- coding: utf-8 -*-
"""
...\PTVR\PTVR_Researchers\Python_Scripts\Demos\Motion_across_scenes\
moving_tangent_screen_with_moving_object_on_it.py

Created on Thu Sep 30 08:57:37 2021
@author: eric castet   

Goal : illustrates how to create motion by placing objects across scenes
        at different perimetric coordinates on a Tangent Screen whose
        coordinates also change.

Note: the Tangent Screen can be static by setting is_screen_moving to False

"""


import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

import PTVR.Stimuli.Color as color
import PTVR.Pointing.PointingCursor
import PTVR.Tools as tools


###################################################################################
#                             PARAMETERS
###################################################################################
PWD = PTVR.SystemUtils.PWD

# The 3 lines below are the 3 perimetric coordinates of the static tangent screen
# in the current Coordinate system.
# In the present script, the Origin of the current Coordinate system is represented
# by a dark sphere  (see fixationPointSize_deg)
tangent_screen_radial_distance_m = 500
tangent_screen_eccentricity = 30
tangent_screen_half_meridian = 90

subject_id = "Manolo"
height_of_headset = 1.2  # in meters
if subject_id == "Manolo":
    height_of_headset = 1.2

scene_duration = 200  # in ms

is_screen_moving = True
bgColor = color.RGBColor(0.5, 0.5, 0.5, 1)

# The new origin of the coordinate system (a small black sphere) will be
# created before drawing the stimuli.
# This is to let you visualize from the outside that the tangent screen
# (whatever its position) is tangent to a notional sphere around the
# origin of the current coordinate system (CS).
new_origin_distance = 0.8  # from the static headset position
new_origin_position = np.array(
    [0, height_of_headset, new_origin_distance])  # Global coordinates

objectSize_deg = 1.0  # moving red cube
objectSize_m = tools.visual_angle_to_size_on_perpendicular_plane(
    visual_angle_of_centered_object_in_deg=objectSize_deg,
    viewing_distance_in_m=tangent_screen_radial_distance_m)  # in meters


fixationPointSize_deg = 1.0  # will be drawn at the new origin position
fixationPointSize_m = tools.visual_angle_to_size_on_perpendicular_plane(
    visual_angle_of_centered_object_in_deg=fixationPointSize_deg,
    viewing_distance_in_m=new_origin_distance)  # in meters

# Angular values measured at the new origin created in main () with
# my_world.translate_coordinate_system_along_global (new_origin_position)
ScreenHeight_deg = 30.0  # in degrees
ScreenHeight_m = tools.visual_angle_to_size_on_perpendicular_plane(
    visual_angle_of_centered_object_in_deg=ScreenHeight_deg, viewing_distance_in_m=tangent_screen_radial_distance_m)  # in meters

ScreenWidth_deg = 30.0  # in degrees
ScreenWidth_m = tools.visual_angle_to_size_on_perpendicular_plane(
    visual_angle_of_centered_object_in_deg=ScreenWidth_deg, viewing_distance_in_m=tangent_screen_radial_distance_m)  # in meters

# ---------------         END of Parameters
# -------------------------------------------------------------------------------------


def create_scene(halfMeridian0fObject):

    global tangent_screen_eccentricity
    global tangent_screen_half_meridian

    myScene = PTVR.Stimuli.Scenes.SimplifiedTimerScene(
        scene_duration_in_ms=scene_duration)

    # myTangentScreen and myObject are created for each scene of the loop.
    # The position of the objects is modified in each scene.
    myTangentScreen = PTVR.Stimuli.Objects.TangentScreen(color=color.RGBColor(r=1, g=1, b=0, a=1),
                                                         size_in_meters=np.array([ScreenWidth_m, ScreenHeight_m]))
    myObject = PTVR.Stimuli.Objects.Cube(size_in_meters=objectSize_m,
                                         color=color.RGBColor(r=1, g=0, b=0, a=1))
    fixationPoint = PTVR.Stimuli.Objects.Sphere(size_in_meters=fixationPointSize_m,
                                                color=color.RGBColor(r=0, g=0, b=0, a=1))

    myTangentScreen.set_perimetric_coordinates(
        eccentricity=tangent_screen_eccentricity,
        halfMeridian=tangent_screen_half_meridian,
        radialDistance=tangent_screen_radial_distance_m)

    myObject.set_perimetric_coordinates_on_screen(myTangentScreen,
                                                  eccentricity_local_deg=ScreenWidth_deg * 0.25,
                                                  half_meridian_local_deg=halfMeridian0fObject
                                                  )
    myScene.place(myTangentScreen, my_world)
    myScene.place(fixationPoint, my_world)
    myScene.place(myObject, my_world)

    my_world.add_scene(myScene)
    return myScene


my_world = visual.The3DWorld(name_of_subject=subject_id)


def main():
    global tangent_screen_eccentricity
    global tangent_screen_half_meridian

    my_world.translate_coordinate_system_along_global(new_origin_position)
    # my_world.rotate_coordinate_system_about_global_y (90)

    halfMeridian0fObject = 0
    for i in np.arange(0, 360, 1.0):  # np.arange used to create a range of FLOATS
        if is_screen_moving:
            tangent_screen_half_meridian = tangent_screen_half_meridian + 1.0
        halfMeridian0fObject = 2 * i
        # Half-Meridian of Tangent Screen
        myScene = create_scene(halfMeridian0fObject=halfMeridian0fObject)
    # end of for loop

    # to restart the loop at the firt scene
    myScene.SetNextScene(my_world.id_scene[0])

    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
