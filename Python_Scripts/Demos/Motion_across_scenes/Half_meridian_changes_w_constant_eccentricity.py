# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Motion_across_scenes\
    Half_meridian_changes_w_constant_eccentricity.py


Goal :  Illustrates the use of my_object.set_perimetric_coordinates() used
        across different scenes.

What you see:
    A cube moves across scenes on a circular trajectory by changing its
    half-meridian (with constant eccentricity and radial distance)

"""


import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import numpy as np

scene_duration_in_ms = 200

half_meridian_change = 3  # in degrees

radial_distance_m = 1.5
eccentricity = 20


def main():
    my_world = visual.The3DWorld(name_of_subject="carlos")
    my_world.translate_coordinate_system_along_global(
        np.array([0.0, 1.2, 0]))

    # Object created once with its half-meridian updated in each step of the loop
    my_cube = PTVR.Stimuli.Objects.Cube(size_in_meters=0.2)
    my_cube.set_cartesian_coordinates(x=0, y=1, z=1)

    for HM in np.arange(0, 360, half_meridian_change):
        my_scene = PTVR.Stimuli.Scenes.SimplifiedTimerScene(
            scene_duration_in_ms=scene_duration_in_ms)

        my_cube.set_perimetric_coordinates(
            eccentricity=eccentricity,
            halfMeridian=HM,
            radialDistance=radial_distance_m)

        my_scene.place(my_cube, my_world)
        my_world.add_scene(my_scene)
    # If you do not want a cyclic animation, comment out the following line
    # which restarts the loop at the firt scene.
    my_scene.SetNextScene(my_world.id_scene[0])

    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
