# -*- coding: utf-8 -*-
'''
...\PTVR_Researchers\Python_Scripts\Demos\eye_tracking\
gaze_cursor_pointing_with_recording.py

Created on: Tue Mar 08 2022
@author: jtermozm

Goal: collect gaze/eye data.

The subject points his/her gaze at the target ( pointing is visualized by the 
                                        gaze-contingent reticle).
Once the subject is ok with his/her pointing, he/she presses the hand-controller 
trigger, thus triggering the presentation of the next target.

The eccentric targets have a constant eccentricity and varying halfMeridians.

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Recording of gaze/eye data is in :
...\PTVR_Researchers\PTVR_Operators\Results\gaze_cursor_pointing_with_recording\


'''
import PTVR.Tools as tools
import PTVR.SystemUtils
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import PTVR.Stimuli.Color as color

import PTVR.Pointing.ReticleImageFromDrawing as RG
from PTVR.Pointing.PointingCursor import *

import numpy as np
# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Edgar"

record_gaze = True
my_contingency = ImageContingency.GAZE
# ImageContingency.HEADSET, ImageContingency.LEFT_EYE,
# ImageContingency.RIGHT_EYE or ImageContingency.GAZE.

gaze_calibration_already_done = True

height_of_headset = 1.2

target_eccentricity = 13  # in deg
distance_xp = 500  # in meters

### Reticle Parameters ###
reticle_2D_image = RG.ReticleImageFromDrawing()

my_reticle_diameter_deg = 12


tangent_screen_size_deg = 55
tangent_screen_size_meters = tools.visual_angle_to_size_on_perpendicular_plane(
    visual_angle_of_centered_object_in_deg=tangent_screen_size_deg,
    viewing_distance_in_m=distance_xp)

my_tangent_screen = PTVR.Stimuli.Objects.TangentScreen(position_in_current_CS=np.array([0, 0, distance_xp]),
                                                       size_in_meters=tangent_screen_size_meters,
                                                       color=color.RGBColor(
                                                           r=0.0, g=0.0, b=0.0, a=1),
                                                       output_gaze=record_gaze)
number_of_targets = 8

my_world = visual.The3DWorld()


################################################################
#                                                               #
################################################################
def launch_gaze_calibration(my_world):
    my_scene = PTVR.Stimuli.Scenes.VisualScene()
    my_event = event.HandController(valid_responses=['right_trigger'])
    calibration = callback.CalibrationEyesTracking()
    my_text = PTVR.Stimuli.Objects.Text(
        text="Press right trigger to launch gaze calibration (be patient during loading)",
        visual_angle_of_centered_x_height_deg=1, position_in_current_CS=np.array([0, 0, 1]))
    my_scene.place(my_text, my_world)
    end_scene = callback.EndCurrentScene()
    my_scene.AddInteraction(events=[my_event], callbacks=[
                            calibration, end_scene])
    my_world.add_scene(my_scene)

################################################################
# scene_with_target = Scene with a target of given eccentricity and halfMeridian on TangentScreen#
################################################################


def scene_with_target(my_world, ecc, hm, scene_rank):
    if scene_rank == 1:
        # show the hand-controller to check if you switched it ON .
        my_scene = PTVR.Stimuli.Scenes.VisualScene(trial=scene_rank,
                                                   are_both_hand_controllers_visible=True)
        instructions = PTVR.Stimuli.Objects.Text(text="Point your head at the target and \n"
                                                 "press the hand-controller's trigger \n"
                                                 "to display the next target",
                                                 visual_angle_of_centered_x_height_deg=2,
                                                 rotation_in_current_CS=np.array([45, 0, 0]))
        instructions.set_perimetric_coordinates(eccentricity=40,
                                                halfMeridian=270,
                                                radialDistance=2)
        my_scene.place(instructions, my_world)

    else:
        my_scene = PTVR.Stimuli.Scenes.VisualScene(trial=scene_rank)

    # Set peripheral Target on TangentScreen
    my_target = PTVR.Stimuli.Objects.Sphere(
        size_in_meters=2*distance_xp *
        np.tan(my_reticle_diameter_deg/2*np.pi/180),
        color=color.RGB255Color(r=255))

    my_target.set_perimetric_coordinates_on_screen(
        tangentscreen=my_tangent_screen,
        eccentricity_local_deg=ecc, half_meridian_local_deg=hm)
    my_scene.place(my_tangent_screen, my_world)
    my_scene.place(my_target, my_world)

    my_reticle = ImageToContingentCursor(
        contingency_type=my_contingency,
        image=reticle_2D_image,
        size_in_degrees=[my_reticle_diameter_deg, my_reticle_diameter_deg])
    my_scene.place_contingent_cursor(my_reticle)

    # Interaction
    my_event_next_scene = event.HandController(
        valid_responses=['right_trigger'])
    my_callback_next_scene = callback.EndCurrentScene()

    my_scene.AddInteraction(events=[my_event_next_scene],
                            callbacks=[my_callback_next_scene])

    # Writing target position into the Main .csv results file.
    my_scene.fill_in_results_file_column(column_name="distance_exp",
                                         value=distance_xp)
    my_scene.fill_in_results_file_column(column_name="target_eccentricity",
                                         value=str(ecc))
    my_scene.fill_in_results_file_column(column_name="target_half_meridian",
                                         value=str(hm))
    my_world.add_scene(my_scene)

#############
#############


def main():
    my_world = visual.The3DWorld(name_of_subject=username)
    my_world.translate_coordinate_system_along_global(
        translation=np.array([0.0, height_of_headset, 0.0]))

    if gaze_calibration_already_done == False:  # perform gaze calibration if it
        # has not been performed yet.
        launch_gaze_calibration(my_world)

    # HalfMeridian Increment
    hm_step = 360 / number_of_targets

    for count in range(1, number_of_targets+1):
        print(count)
        # Scene 1  = Target At Center
        scene_with_target(my_world=my_world,
                          ecc=0, hm=0, scene_rank=count)
        # Scene 2 = Target in Periphery
        scene_with_target(my_world=my_world,
                          ecc=target_eccentricity,
                          hm=hm_step * count, scene_rank=count)

    scene_with_target(my_world=my_world, ecc=0, hm=0,
                      scene_rank=count + 1)
    my_world.write()


if __name__ == "__main__":
    main()
    PTVR.SystemUtils.LaunchThe3DWorld()
