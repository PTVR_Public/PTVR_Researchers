# -*- coding: utf-8 -*-
""""
callback_calibration_eyes_tracking.py

Goal : This demo illustrates the use of the callback CalibrationEyesTracking.
This will allow you to include (if needed) an eyetracker calibration within your scripts. 
For instance, this could be included at the beginning of each block.
  
How to use : 
    Press the space bar to launch an eyetracker calibration (we assume that Sranipall software has 
                                                              already been installed )
To install the Sranipal application:
 see section 2/ Eyetracker Installation
 in https://ptvr_public.gitlabpages.inria.fr/PTVR_Researchers/setup_h_m_d.html

Predictions : 
    After pressing the space bar, eyetracker calibration should start and, once finished,
    you should come back to your running script.

Created on : Thu Jan 13 15:09:02 2022
@author: jtermozm

"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual
import PTVR.Stimuli.Objects
import PTVR.Stimuli.Scenes
import PTVR.Data.Event as event
import PTVR.Data.Callback as callback
import PTVR.Stimuli.Color as color

from PTVR.Pointing.PointingCursor import *
import PTVR.Pointing.ReticleImageFromDrawing as RG
   
reticle_size_deg = 2 # The reticle's diameter
distance_to_text = 3 # in meters


def create_reticle (my_scene):
    # Create the gaze-contingent reticle.
    # This reticle is only a gaze-contingent feedback which is not responsible for
    # any pointing process. 
    #
    # internally create a PNG file to create a sprite
    reticle_2D_image = RG.ReticleImageFromDrawing ( )

    # transform 'reticle_2D_image' into a contingent cursor.
    my_reticle = ImageToContingentCursor (
                    contingency_type=ImageContingency.GAZE,
                    image = reticle_2D_image,                    
                    size_in_degrees= [reticle_size_deg, reticle_size_deg],
                    distance_if_empty_cursor_cone = distance_to_text
                    )
    my_scene.place_contingent_cursor (my_reticle)
    # END of Reticle creation.
    #-------------------------

def main():

    my_world = PTVR.Visual.The3DWorld ()
    my_scene = PTVR.Stimuli.Scenes.VisualScene ()
    
    # Create interaction : if you press space bar, this launches an eyetracker calibration
    my_event = event.Keyboard (valid_responses = ['space'])    
    my_callback = callback.CalibrationEyesTracking()
    my_scene.AddInteraction (events = [my_event], callbacks = [my_callback])
    
    # if you do not want to have a flat cursor to get visual feedback from your gaze
    # position, then comment the following line.
    create_reticle (my_scene)
    
    my_text = PTVR.Stimuli.Objects.Text (text = "If Gaze calibration is correct,\n reticle size should be the same as that of the o.", font_name='Arial', 
                                        position_in_current_CS = np.array([0, 1.2, 5]), color = color.RGBColor(r=0.7, g=0, b=0.6, a=1),
                                        visual_angle_of_centered_x_height_deg = reticle_size_deg)
    my_scene.place (my_text, my_world)
    
    my_world.add_scene (my_scene)
    my_world.write () # Write the Experiment to .json file
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()