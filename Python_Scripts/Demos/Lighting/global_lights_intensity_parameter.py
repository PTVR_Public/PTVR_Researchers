# -*- coding: utf-8 -*-
"""
...\PTVR_Researchers\Python_Scripts\Demos\Lighting\
    global_lights_intensity_parameter.py

Goal: Discover the effects of the 'global_lights_intensity' parameter of 
    the VisualScene object (the values vary from 0 to any positive value - the absence
                     of a max value is inherited from Unity and we left it 
                     that way for the time being).
    

Explanations about the 'global_lights_intensity' parameter: 
    If this parameter is set to zero, all the sides of an object appear to
    have the same color and luminance. This is because all the sides of a cube
    do have the same color/luminance (say (0,0,0) for a black object).
    The reason is that  PTVR objects are EMITTING objects 
    (in the future, other lighting options will be available). 
    This EMISSION mode means that these objects do not need any source of 
    lighting in the virtual world to be seen. 
    The downside is that the whole surface of an emitting object
    will have the same color / luminance. For instance, all the sides of a black
    cube will be physically black.
    
    This can be modified by ADDING a light source in the virtual world. In this
    case, Unity internally calculates the consequences of this light source
    on the emitting objects, which does concomitantly CHANGE the physical color 
    of the object's sides depending on the geometrical relationships between the 
    light source and the object.
    
    There are two main ways to add a lighting source in a scene:
        a/ you can give a positive value to the 'global_lights_intensity' parameter.
    This will create several light sources high above the ground so that it
    will provide a sort of diffuse white light allowing users to perceive that different
    sides of an object are differently lighted.
        This lighting mode is chosen by default when you create a scene by
    setting the 'global_lights_intensity' parameter to 1 by default. 
    
        b/ you can create a PTVR Light object (or several of these objects) at
        any position in the scene. To learn how to create a PTVR Light Object, 
        search for the "Light_object.py" demo in the \Python_Scripts\Demos\ folder.
    
    
Prediction #1:
        As the 'global_lights_intensity' parameter is set to zero in the present demo,
    the objects' sides will not be discernable. For example, the black cube will appear 
    as an object being homogeneously black.
        However the sides will become discernable if you set this parameter to 1 !

Prediction #2:     
    If you set the 'global_lights_intensity' parameter to a positive value: say 1... 
    you might then note an adverse scintillating effect at the white rectangle's borders. 
    This effect should disappear ...
    if you set back the 'global_lights_intensity' parameter to zero 
    or
    if you reduce the object's level of whiteness ( RGBColor (r=0.7, g=0.7, b=0.7) ).
    
    The origin of this adverse visual effect seems to be in Unity, and we do not 
    currently understand this interaction between a fully white object and 
    light sources.
                                                                     
"""

import PTVR.SystemUtils
import numpy as np
import PTVR.Visual as visual
import PTVR.Stimuli.Scenes
import PTVR.Stimuli.Color as color
import PTVR.Stimuli.Objects
import PTVR.Stimuli.UIObjects

def main():

    my_world = visual.The3DWorld (name_of_subject = "Ignace")
    my_scene = PTVR.Stimuli.Scenes.VisualScene ( 
                    global_lights_intensity = 0) # !!!!!!!!!!!!!
    
    #########
    my_black_cube = PTVR.Stimuli.Objects.Cube (
                    position_in_current_CS = np.array ( [0.2, 1.2, 0.7] ),
                    rotation_in_current_CS = np.array([45, 45, 0]),
                    size_in_meters = 0.1 )
    #########
    my_white_cube = PTVR.Stimuli.Objects.Cube (
                    position_in_current_CS = np.array ( [ -0.2, 1.2, 0.7] ),
                    color = color.RGBColor (r=1, g=1, b=1),
                    #color = color.RGBColor (r=0.7, g=0.7, b=0.7), # reduce whiteness                             
                    size_in_meters = np.array([0.1, 0.05, 0.001]) )    
  
    my_scene.place (my_black_cube, my_world)
    my_scene.place (my_white_cube, my_world)
    
    my_world.add_scene (my_scene)   
   
    my_world.write () 
    
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchThe3DWorld()